var resp_bpjs = {};
var tampungshiftsekarang;
var tampungshiftsekarang;
var dsKtpKelurahan;
var dataSource_viDaftar;
var selectKecamatanpasien;
var datasource_gettempat;
var datagetappto;
var dsKelurahan;
var tmp_cari_rujukan = true;
var selectSetHubunganKeluargaPj = 1;
var jeniscus;

var status_kepesertaanPasienBPJS;
var dataKepesertaanBPJS;
var str_tglkunjungan;
var str_kdpasien;

var dataSourceFKTL;
var dataSource_BookingOnline;
var autocomdiagnosa_master;
var dsDataStoreGridDiagnosa = new Ext.data.JsonStore();
var DataStoreListDiagnosa;
var dataSourceFKTP;
var dspenunjang;
var tmp_nomor_skdp = "";
var tmp_sub_unit_rehab = "";
var kodeDokterDefault;
var dsComboCustomer;
var status_pulang_sep;
var jenis_kontrol_insert_rencana_kontrol;
var selectTujuanKunjSEP;
var selectFlagProcedure;
var selectKdPenunjang;
var selectAssementPel;
var select_dokterKontrol;
var select_nama_dokterKontrol;
var kodePenerimaanPasien = 1;
var janganDuluCetakSEP = 'ya';
var sendDataArrayFKTLRWJ = [];
var sendDataArrayHistorySEPRWJ = [];
var noSEPHistorySEPRWJ;
var barisNoSEPHistorySEP;
var select_namapoliKontrol;
var select_poliKontrol;
var sendDataArrayFKTPRWJ = [];
var jenis_pelayanan_insert_rujukan_balik;
var select_programPRB;
var select_dokterPRB;
var select_nama_dokterPRB;
var dsDataGrdObat_PRB = new Ext.data.ArrayStore({
	id: 0,
	fields: ['kode', 'nama', 'jml', 'signa'],
	data: []
});

var dsDataRujukanInternal = new Ext.data.ArrayStore({
	id: 0,
	fields: ['nosep', 'nosurat', 'tglrujukinternal', 'tujuanrujuk', 'nmdokter', 'diagppk'],
	data: []
});


var dsDokterPJBPJS;
var dsDokterPJBPJSKunj2;
// ---------------------------------

var dataSourceHistorySEP;
var caranerima;
var PILIHKABUPATEN;
var loket_terpilih;
var booking_online = null;
var booking_online_status = false;
var dataSourceHistoryPenyakit;
var rowSelected_viDaftarHistory;
var CurrentHistorydatapasienRWJ = {
	data: Object,
	details: Array,
	row: 0
};
var tipe_faskes = 2;
var tipe_rujukan_last;
var rujukan_tipe;
var kode_faskes_insert_rujukanTL;
var kode_poli_insert_rujukan_faskesTL;
var kode_diagnosa_insert_rujukan_faskesTL;
var rujuk_balik = 0;
var partial = 0;
var tipe_penuh = 0
var jenis_pelayanan_insert_rujukan;
var select_kd_dpjp;
var selectPelayananApprovalSEP;
var tmp_data_approval_sep;
var asal_rujukan = '1';
var dataSourceRujukanByNoka;
var Field = ['NO_KUNJUNGAN', 'NO_KARTU', 'NAMA_PESERTA', 'TGL_KUNJUNGAN', 'PPK_PERUJUK', 'SPESIALIS'];
dataSourceRujukanByNoka = new Ext.data.ArrayStore({
	fields: Field
});

var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];
	ds_customer_viDaftar = new WebApp.DataStore({ fields: Field_poli_viDaftar });

var kd_dokter_dpjp_insert;
var is_rujukan_rs = 'false';
var tmp_katarak_update;
var tmp_diagnosa_update;
var dataSourceListDiagosa;
var polipilihanpasien_bpjs;
var tmp_dokter_dpjp;
var tmp_poli_tujuan_update;
var tmp_tgl_rujukan;
var data_array_sep;
var tmp_kd_dokter_dpjp;
var kode_dpjp_kunj2;
var tmp_poli_tujuan_kunj2;
var flagging_pasien_lama_beda_poli;
var tmp_tgl_rujukan;
var tmp_suplesi = 0;
var select_kd_dokter_bpjs = '';
var tmp_katarak = 0;
var tmp_cob = 0;
var tmp_penjamin_1 = 0;
var tmp_penjamin_2 = 0;
var tmp_penjamin_3 = 0;
var tmp_penjamin_4 = 0;
var tmp_lakaLantas = 0;
var selectProvinsiLokasiLaka = 0;
var selectKabupatenLokasiLaka = 0;
var selectKecamatannLokasiLaka = 0;
var tmp_diagnosa;
var tmp_eksekutif = 0;
var tmp_ppk_rujukan;
var tmp_kelas_rawat;
var tmp_poli_tujuan;
var dsPilihLoket;
var timeInterval;
var nik_pasien = 0;
var dataSourceHistorySEP;
var Pendidikanpasien = undefined;
var PekerjaanPasien = undefined;
var PendidikanAyah = undefined;
var PekerjaanAyah = undefined;
var PendidikanIbu = undefined;
var PekerjaanIbu = undefined;
var PendidikanSuamiIstri = undefined;
var PekerjaanSuamiIstri = undefined;
var namacbokelpasien = 'umum';
var selectCount_viDaftar = 100;
var NamaForm_viDaftar = "Pendaftaran Rawat Jalan";
var mod_name_viDaftar = "viDaftar";
var cboAsuransi;
var dsprinter_pendaftaranrwj;
var koderujuk = 0;
var now = new Date();
var now_viDaftar = new Date();
var xmlHttp;

/* SatuSehat 29-12-2023 */
var formLookup_UpdateSATUSEHAT;
var name_diagnosa;
var ihs_number;
var dsConditionSatusehat = new WebApp.DataStore({
	fields: [
		"fullUrl",
		"category_code",
		"category_display",
		"category_system",
		"clinicalStatus_code",
		"clinicalStatus_display",
		"clinicalStatus_system",
		"code_code",
		"code_display",
		"code_system",
		"encounter_display",
		"encounter_reference",
		"id",
		"meta_lastUpdated",
		"meta_versionId",
		"resourceType",
		"subject_display",
		"subject_reference",
		"search"
	],
});
var dataSource_HistorySatusehat;
var Field = ['class_code', 'class_display', 'id_ecounter', 'identifier', 'location_display', 'location_reference','last_updated','version_id','participant_display','participant_reference','participant_type_code','participant_type_display','period_start','resource_type','service_provider','status','status_history_period_start', 'status_history_status','subject_display','subject_reference'
];
dataSource_HistorySatusehat = new Ext.data.ArrayStore({
	fields: Field
});
/* ==================== */
function srvTime(){
    try {
        //FF, Opera, Safari, Chrome
        xmlHttp = new XMLHttpRequest();
    }
    catch (err1) {
        //IE
        try {
            xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (err2) {
            try {
                xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (eerr3) {
                //AJAX not supported, use CPU time.
                alert("AJAX not supported");
            }
        }
    }
console.log(xmlHttp);

    xmlHttp.open('HEAD',window.location.href.toString(),false);
    xmlHttp.setRequestHeader("Content-Type", "text/html");
    xmlHttp.send('');
    return xmlHttp.getResponseHeader("Date");
}

var st = srvTime();
var Today_Pendaftaran = new Date(st);
console.log(Today_Pendaftaran);
// var Today_Pendaftaran = new Date;
var h = now_viDaftar.getHours();
var m = now_viDaftar.getMinutes();
var s = now_viDaftar.getSeconds();
var year = now_viDaftar.getFullYear();
var nowSerahIjazah_viDaftar = new Date();
var urutJenjang_viDaftar;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftar;
var rowSelected_viDaftar;
var tmp_tempat;
var setLookUps_viDaftar;
var formLookups_viDataBPJS;
var ds_KABUPATEN_viKasirRwj;
var polipilihanpasien;
var ds_KECAMATAN_viKasirRwj;
var tampung;
var BlnIsDetail;
var selectPropinsiKtp;
var SELECTDATASTUDILANJUT;
var kelurahanpasien;
var dataKabupaten;
var datapasienvariable = {};
datapasienvariable.kd_pasien;
datapasienvariable.urut;
datapasienvariable.tglkunjungan;
datapasienvariable.kd_unit;
var selectKabupatenKTP;
var tmpnotransaksi;
var tmpkdkasir;
var selectkelurahanktp;
var selectPoliPendaftaran;
var selectKelompokPoli;
var selectKecamatanktp;
var selectPendidikanPendaftaran;
var selectPekerjaanPendaftaran;
var selectPendaftaranStatusMarital;
var selectAgamaPendaftaran;
var mNoKunjungan_viKasir = '1';
var selectSetJK;
var radios;
var radios1;
var radios2;
var radios3;
var radios4;
var pasienrujukan = false;
var selectSetPerseorangan;
var selectSetAsuransi;
var selectSetGolDarah;
var selectSetSatusMarital;
var kelompokpasien;
var selectSetRujukanDari;
var selectSetNamaRujukan;
var dsAgamaRequestEntry;
var selectAgamaRequestEntry;
var dsPropinsiRequestEntry;
var selectPropinsiRequestEntry;
var dsKecamatanKtp;
var dsKecamatanRequestEntry;
var dsKecamatanPenanggungJawab;
var dsKelurahanPenanggungJawab;
var dsKelurahanKtpPenanggungJawab;
var dsPendidikanRequestEntry;
var dsPendidikanAyahRequestEntry;
var dsPendidikanIbuRequestEntry;
var dsPendidikanSuamiIstriRequestEntry;
var dsPekerjaanRequestEntry;
var dsPekerjaanAyahRequestEntry;
var dsPekerjaanIbuRequestEntry;
var dsPekerjaanSuamiIstriRequestEntry;
var dsPekerjaanPenanggungJawabRequestEntry;
var dsDokterRequestEntry;
var dsKabupatenKtp;
var dsKabupatenRequestEntry;
var dsKabupatenpenanggungjawab;
var dsKabupatenKtppenanggungjawab;
var selectSetWarga;
var selectSetHubunganKeluarga;
var txtedit = "Tambah Data";
var selectPropinsipenanggungjawab;
var selectKabupatenRequestEntry;
var selectKecamatanRequestEntry;
var carapenerimaanpasien = "99";
var tmpcriteriaRWJ = "";
var PasienSetSatusMarital = 0;
var NoAsuransiPasien;
var autocomdiagnosa;
var selectSukuRequestEntry;
var CurrentData_viDaftar = {
	data: Object,
	details: Array,
	row: 0
};
var kelasbpjs;
var jenisbpjs;
var dataSource_medrec_viDaftar;
var LookupGrdDataHistoryKunjungan_PendaftaranRWJ;
var LookupDataSourceHistoryKunjungan_PendaftaranRWJ;
//TAMBAHAN UNTUK BPJS VERSI 2
var str_kdUnit;
var str_kdPasien;
var jenis_pelayanan;
var no_surat_kontrol;
var selectTujuanKunjSEP;
var selectFlagProcedure;
var selectKdPenunjang;
var selectPengajuanApprovalSEP;
var sep_rwi='';
var validasi_finger;
var pesan_finger;
var btn_pascaRWI=false;
var nama_rujukan_pbr;
var kd_rujukan_pbr;
var kunjungan_pertama;
var selectAssementPel;
var jenis_kontrol_insert_rencana_kontrol;
var noSuratKontrol;
var rwi_kode_poli; 
var rwi_nama_poli ;
var rwi_kode_dokter ;
var rwi_nama_dokter ;
var rwi_diag_awal;
var rwi_kd_prov ;
var rwi_kd_kab ;
var rwi_kd_kec ;
var rwi_no_tlp ;
var rwi_kd_dpjp ;
var noka_buat_surat;
var sep_buat_surat;
var tgl_kontrol;
var kd_dpjp_kontrol;
var status_kepesertaanPasienBPJS; 
var tmp_sex;
var poli_rujukan_asal;
var dsDataRujukanInternal = new Ext.data.ArrayStore({
	id: 0,
	fields: ['nosep','nosepref', 'nosurat', 'tglrujukinternal', 'tujuanrujuk', 'nmdokter', 'diagppk'],
	data: []
});
var dsDataGrdObat_PRB = new Ext.data.ArrayStore({
	id: 0,
	fields: ['kode', 'nama', 'jml', 'signa'],
	data: []
});
//----------------------------
var lakalantas_checkbox_1_RWJ = false;
var lakalantas_checkbox_2_RWJ = false;
var lakalantas_checkbox_3_RWJ = false;
var lakalantas_checkbox_4_RWJ = false;
var txtLokasi_Lakalantas = "-";

clearInterval(timeInterval);
clearInterval();
function daftarRWJgetdatabpjs() {
	var contentType = "application/json; charset=utf-8";
	if ((Ext.getCmp('txtNoAskes').getValue().trim() != '' && Ext.getCmp('cboAsuransi').getValue() == '0000000043') ||
		(Ext.getCmp('txtNoAskes').getValue().trim() != '' && Ext.getCmp('cboAsuransi').getValue() == '0000000044') ||
		(Ext.getCmp('txtNoAskes').getValue().trim() != '' && Ext.getCmp('cboAsuransi').getValue() == 'BPJS PBI') ||
		(Ext.getCmp('txtNoAskes').getValue().trim() != '' && Ext.getCmp('cboAsuransi').getValue() == 'BPJS NON PBI')) {
		var e = false;
		loadMask.show();
		if (Ext.getCmp('cboPoliklinikRequestEntry').getValue() == '') {
			e = true;
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Poliklinik Harus dipilih.');
		}
		if (Ext.getCmp('txtDiagnosa_RWJ').getValue() == '') {
			e = true;
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Diagnosa Harus dipilih.');
		}
		if (e == false) {
			Ext.Ajax.request({
				method: 'POST',
				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs",
				params: {
					klinik: polipilihanpasien,
					kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
					kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
					tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
					no_kartu: Ext.getCmp('txtNoAskes').getValue()
				},

				success: function (o) {
					loadMask.hide();
					var cst = Ext.decode(o.responseText);
					if (cst.data.metaData.code == '200') {
						if (cst.poli != null) {
							var catatan = "";
							if (Ext.getCmp('txtcatatan').getValue() != "" || Ext.getCmp('txtcatatan').getValue() != null) {
								catatan = Ext.getCmp('txtcatatan').getValue();
							}

							var telepon = '-';
							var tmp_telepon = Ext.getCmp('txttlpnPasien').getValue();
							if (tmp_telepon.length > 0) {
								telepon = Ext.getCmp('txttlpnPasien').getValue();
							}

							var tmp_cbo_lakalantas = 0;
							var lokasi_Lakalantas = "-";
							var penjamin = "-";
							if (Ext.getCmp('cblakalantas').getValue() === true) {
								tmp_cbo_lakalantas = 1;

								if (Ext.getCmp('cblakalantas').getValue() === true) {
									lakalan = 1;

									penjamin = "";
									if (lakalantas_checkbox_1_RWJ === true || lakalantas_checkbox_1_RWJ == 'true') {
										penjamin += "1,";
									}

									if (lakalantas_checkbox_2_RWJ === true || lakalantas_checkbox_2_RWJ == 'true') {
										penjamin += "2,";
									}

									if (lakalantas_checkbox_3_RWJ === true || lakalantas_checkbox_3_RWJ == 'true') {
										penjamin += "3,";
									}

									if (lakalantas_checkbox_4_RWJ === true || lakalantas_checkbox_4_RWJ == 'true') {
										penjamin += "4,";
									}

									penjamin = penjamin.substring(0, (penjamin.length - 1));
									if (txtLokasi_Lakalantas != "" || txtLokasi_Lakalantas != null) {
										lokasi_Lakalantas = txtLokasi_Lakalantas;
									}
								}
							}
							var p = cst.data.response.peserta;
							console.log(p);
							Ext.getCmp('txtNamaPeserta').setValue(p.nama);
							// kelasbpjs = p.kelasTanggungan.nmKelas;
							// jenisbpjs = p.jenisPeserta.nmJenisPeserta;

							var lakalan = 2;
							if (Ext.getCmp('cblakalantas').getValue() === true) {
								lakalan = 1;
							} else {
								lakalan = 2;
							}

							var datanya = '<data>' +
								'<request>' +
								'<t_sep>' +
								'<noKartu>' + p.noKartu + '</noKartu>' +
								'<tglSep>' + new Date().format('Y-m-d') + '</tglSep>' +
								// '<tglSep>2018-03-28</tglSep>' +
								'<ppkPelayanan>' + cst.kd_rs + '</ppkPelayanan>' +
								'<jnsPelayanan>2</jnsPelayanan>' +
								'<klsRawat>' + p.hakKelas.code + '</klsRawat>' +
								'<noMR>' + Ext.getCmp('txtNoRequest').getValue() + '</noMR>' +
								'<rujukan>' +
								'<asalRujukan>1</asalRujukan>' +
								'<ppkRujukan>' + p.provUmum.kdProvider + '</ppkRujukan>' +
								'<noRujukan>0</noRujukan>' +
								'<tglRujukan>' + new Date().format('Y-m-d') + '</tglRujukan>' +
								'</rujukan>' +
								'<catatan>dari WS ,' + catatan + '</catatan>' +
								'<diagAwal>' + autocomdiagnosa_master + '</diagAwal>' +
								'<poli>' +
								'<tujuan>' + cst.poli + '</tujuan>' +
								'<eksekutif>0</eksekutif>' +
								'</poli>' +
								'<cob>' +
								'<cob>0</cob>' +
								'</cob>' +
								'<jaminan>' +
								'<lakaLantas>' + tmp_cbo_lakalantas + '</lakaLantas>' +
								'<penjamin>' + penjamin + '</penjamin>' +
								'<lokasiLaka>' + lokasi_Lakalantas + '</lokasiLaka>' +
								'</jaminan>' +
								'<noTelp>' + telepon + '</noTelp>' +
								'<user>Test WS</user>' +
								'</t_sep>' +
								'</request>' +
								'</data>';

							var param = { data: datanya };
							loadMask.show();
							$.ajax({
								type: 'POST',
								dataType: 'JSON',
								crossDomain: true,
								url: baseURL + "index.php/rawat_jalan/functionRWJ/createSEP/",
								data: param,
								success: function (resp1) {
									loadMask.hide();
									if (resp1.metaData.code == '200') {
										Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
										Ext.getCmp('txtNoSJP').focus();
									} else {
										Ext.MessageBox.alert('Gagal', resp1.metaData.message);
									}
								},
								error: function (jqXHR, exception) {
									loadMask.hide();
									Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
									Ext.Ajax.request({
										url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
										params: {
											respon: jqXHR.responseText,
											keterangan: 'Error create SEP'
										}
									});
								}
							});
						} else {
							loadMask.hide();
							Ext.MessageBox.alert('Gagal', 'Poliklinik ' + Ext.getCmp('cboPoliklinikRequestEntry').getRawValue() + ' tidak terdaftar didata BPJS, Harap Hubungi Admin.');
						}
					} else {
						loadMask.hide();
						Ext.MessageBox.alert('Gagal', cst.data.metadata.message);
					}
				}, error: function (jqXHR, exception) {
					Ext.MessageBox.alert('Gagal', 'Nomor Asuransi tidak ditemukan.');
					Ext.getCmp('txtNoSJP').setValue('');
					Ext.getCmp('txtNamaPeserta').setValue('');
					loadMask.hide();
					Ext.Ajax.request({
						url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
						params: {
							respon: jqXHR.responseText,
							keterangan: 'Error search SEP'
						}
					});
				}
			});
		}
	}
}
CurrentPage.page = dataGrid_viDaftar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
Ext.getCmp('txtNoMedrec').focus(true, 1000);
function refreshAntrianRWJ() {
	// console.log(Ext.getCmp('cboPilihGrupLoket'));
	if (Ext.getCmp('cboPilihGrupLoket') === undefined) {
		clearInterval(timeInterval);
		clearInterval();
	} else {
		var grupLoket = Ext.getCmp('cboPilihGrupLoket').getValue();
		if (grupLoket === undefined || grupLoket === '' || grupLoket === 'Pilih Loket...') {
			grupLoket = 'kosong';
		} else {
			grupLoket = Ext.getCmp('cboPilihGrupLoket').getValue();
		}
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/functionRWJ/getAntrian",
			params: { grupLoket: grupLoket },
			failure: function (o) {
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.semuanomor === '') {
					Ext.getCmp('txtTotalAntrian').setValue('0');
				} else {
					Ext.getCmp('txtTotalAntrian').setValue(cst.semuanomor);
				}
				if (cst.semuanomorterpanggil === '') {
					Ext.getCmp('txtSudahPanggil').setValue('0');
				} else {
					Ext.getCmp('txtSudahPanggil').setValue(cst.semuanomorterpanggil);
				}
				if (cst.semuanomorbelumterpanggil === '') {
					Ext.getCmp('txtAntrianPanggil').setValue('0');
				} else {
					Ext.getCmp('txtAntrianPanggil').setValue(cst.semuanomorbelumterpanggil);
				}
			}
		});
	}
}
function dataGrid_BookingOnline() {
	booking_online = null;
	var FieldMaster_viDaftar = ['KD_PASIEN', 'NAMA_PASIEN', 'NAMA_DOKTER', 'NAMA_UNIT', 'KD_UNIT', 'TGL_KUNJUNGAN', 'TGL_BOOKING', 'URUT_MASUK', 'ANTRIAN', 'CUSTOMER', 'KD_CUSTOMER', 'KD_DOKTER', 'JENIS_CUST', 'NOMOR_SKDP'];
	dataSource_BookingOnline = new WebApp.DataStore({ fields: FieldMaster_viDaftar });

	dataSource_BookingOnline.load({
		params: {
			Skip: 5,
			Take: 100,
			Sort: '',
			Sortdir: 'ASC',
			target: 'BookingOnlineRWJ_List',
			param: "bo.kunjungan = ~false~ AND bo.tgl_kunjungan = ~" + now.format('Y-m-d') + "~",
		},
	});
	var grData_viDaftar = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		store: dataSource_BookingOnline,
		autoScroll: true,
		columnLines: true,
		style: 'padding: 0px 4px 4px 4px',
		flex: 2,
		trackMouseOver: true,
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					booking_online = dataSource_BookingOnline.getAt(row);
					console.log(booking_online);
				}
			}
		}),
		listeners: {
			rowdblclick: function (sm, ridx, cidx) {
				booking_online = dataSource_BookingOnline.getAt(ridx);
				Ext.getCmp('FormBookingOnline_viDaftar').close();

				Ext.Ajax.request({
					url: baseURL + "index.php/main/getDataKunjungan",
					params: {
						command: booking_online.data.KD_PASIEN
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						cst.KD_UNIT = booking_online.data.KD_UNIT;
						cst.NAMA_UNIT = booking_online.data.NAMA_UNIT;
						setLookUp_viDaftar(cst);
					}
				});
			}
		},
		colModel: new Ext.grid.ColumnModel([
			{
				id: 'colNM_ANTRIAN_viDaftar',
				header: 'Urut',
				dataIndex: 'ANTRIAN',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 15,
				filter: {},
			}, {
				id: 'colNRM_viDaftar',
				header: 'No.Medrec',
				dataIndex: 'KD_PASIEN',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				width: 45,
				filter: {},
			},
			{
				id: 'colNM_PASIEN_viDaftar',
				header: 'Pasien',
				dataIndex: 'NAMA_PASIEN',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				width: 100,
				filter: {},
			}, {
				id: 'colNM_UNIT_viDaftar',
				header: 'Unit',
				dataIndex: 'NAMA_UNIT',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50,
				filter: {},
			},/*{
				id: 'colNM_TGL_BOOKING_viDaftar',
				header: 'Tgl. Booking',
				dataIndex: 'TGL_BOOKING',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50
			},{
				id: 'colNM_TGL_KUNJUNGAN_viDaftar',
				header: 'Jadwal Kunjungan',
				dataIndex: 'TGL_KUNJUNGAN',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50
			},*/{
				id: 'colNM_DOKTER_viDaftar',
				header: 'Dokter',
				dataIndex: 'NAMA_DOKTER',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 100,
				filter: {},
			}, {
				id: 'col_CUSTOMER_viDaftar',
				header: 'Customer',
				dataIndex: 'CUSTOMER',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50,
				filter: {},
			}, {
				id: 'col_NoSurat_viDaftar',
				header: 'No Surat',
				dataIndex: 'NOMOR_SKDP',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50,
				filter: {},
			},
		]),
		viewConfig: {
			forceFit: true
		}
	});
	return grData_viDaftar;
}
function dataGrid_viDaftar(mod_id) {
	timeInterval = setInterval(function () {
		refreshAntrianRWJ();
	}, 4000);
	var FieldMaster_viDaftar = [
		'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
		'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
		'NAMA_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KELURAHAN', 'KABUPATEN', 'KECAMATAN', 'PROPINSI',
		'KD_KABUPATEN', 'KD_KECAMATAN', 'KD_PROPINSI', 'KD_PENDIDIKAN', 'KD_PEKERJAAN', 'KD_PENDIDIKAN_AYAH', 'PENDIDIKAN_AYAH',
		'KD_PEKERJAAN_AYAH', 'PEKERJAAN_AYAH',
		'KD_PENDIDIKAN_IBU', 'PENDIDIKAN_IBU', 'KD_PEKERJAAN_IBU', 'PEKERJAAN_IBU', 'KD_PENDIDIKAN_SUAMIISTRI', 'PENDIDIKAN_SUAMIISTRI',
		'KD_PEKERJAAN_SUAMIISTRI', 'PEKERJAAN_SUAMIISTRI', 'KD_AGAMA',
		'ALAMAT_KTP', 'NAMA_AYAH', 'NAMA_IBU', 'NAMA_SUAMIISTRI', 'KD_KELURAHAN_KTP', 'KD_POS_KTP', 'KD_POS', 'KD_KELUHAN',
		'PROPINSIKTP', 'KABUPATENKTP', 'KECAMATANKTP', 'KELURAHANKTP', 'KELURAHAN', 'KEL_KTP', 'KEC_KTP', 'NO_ASURANSI',
		'KAB_KTP', 'PRO_KTP', 'EMAIL_PASIEN', 'HP_PASIEN', 'TLPN_PASIEN', 'KARTU'
	];
	dataSource_viDaftar = new WebApp.DataStore({ fields: FieldMaster_viDaftar });
	var FieldMedrec_viDaftar = [
		'KD_PASIEN', 'URUT', 'KD_UNIT', 'POLI', 'TGL', 'DOKTER', 'TGL_KELUAR', 'TGL_MASUK', 'CUSTOMER'
	];
	dataSource_medrec_viDaftar = new WebApp.DataStore({ fields: FieldMedrec_viDaftar });
	var grData_viDaftar = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		store: dataSource_viDaftar,
		autoScroll: true,
		columnLines: true,
		style: 'padding: 0px 4px 4px 4px',
		flex: 2,
		trackMouseOver: true,
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowSelected_viDaftar = undefined;
					rowSelected_viDaftar = dataSource_viDaftar.getAt(row);
					CurrentData_viDaftar.row = row;
					CurrentData_viDaftar.data = rowSelected_viDaftar;
					Ext.getCmp('btntambah_viDaftar').hide()
					Ext.getCmp('btnEdit_viDaftar').show()
					var tmptampung = rowSelected_viDaftar.data.KD_PASIEN;
					var tmpkriteria2 = " P.KD_PASIEN = '" + tmptampung + "'";
					datarefresh_medrec_viDaftar(tmpkriteria2);
				}
			}
		}),
		listeners: {
			rowdblclick: function (sm, ridx, cidx) {
				rowSelected_viDaftar = dataSource_viDaftar.getAt(ridx);
				if (rowSelected_viDaftar !== undefined) {
					Ext.Ajax.request({
						url: baseURL + "index.php/main/getDataKunjungan",
						params: {
							command: rowSelected_viDaftar.data.KD_PASIEN
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							setLookUp_viDaftar(cst);
						}
					});
					Ext.Ajax.request({
						url: baseURL + "index.php/main/getcurrentshift",
						params: {
							command: '0'
						},
						failure: function (o) {
							var cst = Ext.decode(o.responseText);
						},
						success: function (o) {
							tampungshiftsekarang = o.responseText;
						}
					});
				} else {
					setLookUp_viDaftar();
					Ext.Ajax.request({
						url: baseURL + "index.php/main/getcurrentshift",
						params: {
							command: '0'
						},
						failure: function (o) {
							var cst = Ext.decode(o.responseText);
						},
						success: function (o) {
							tampungshiftsekarang = o.responseText;
						}
					});
				}
			},
			containerclick: function () {
				Ext.getCmp('btntambah_viDaftar').show()
				Ext.getCmp('btnEdit_viDaftar').hide()
			}
		},
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({ width: 30 }),
			{
				id: 'colKartuPasien',
				header: 'Kartu',
				dataIndex: 'KARTU',
				width: 10,
				align: 'center',
				renderer: function (value, metaData, record, rowIndex, colIndex, store) {
					switch (value) {
						case 't':
							metaData.css = 'StatusHijau';
							break;
						case 'f':
							metaData.css = 'StatusMerah';
							break;
					}
					return '';
				}
			}, {
				id: 'colNRM_viDaftar',
				header: 'No.Medrec',
				dataIndex: 'KD_PASIEN',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				width: 15
			}, {
				id: 'colNMPASIEN_viDaftar',
				header: 'Nama',
				dataIndex: 'NAMA',
				hideable: false,
				menuDisabled: true,
				sortable: true,
				width: 50
			}, {
				id: 'colTGL_LAHIR_viDaftar',
				header: 'Tgl Lahir',
				dataIndex: 'TGL_LAHIR',
				width: 32,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'colALAMAT_viDaftar',
				header: 'Alamat',
				dataIndex: 'ALAMAT',
				width: 50,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'colTglKunj_viDaftar',
				header: 'Nama Ibu',
				dataIndex: 'NAMA_IBU',
				width: 20,
				sortable: true,
				hideable: false,
				menuDisabled: true,
			}
		]),
		tbar: {
			xtype: 'toolbar',
			id: 'toolbar_viDaftar',
			items: [
				{
					xtype: 'button',
					text: 'Kunjungan',
					hidden: true,
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					id: 'btnEdit_viDaftar',
					handler: function (sm, row, rec) {
						if (rowSelected_viDaftar != undefined) {
							Ext.Ajax.request({
								url: baseURL + "index.php/main/getDataKunjungan",
								params: {
									command: rowSelected_viDaftar.data.KD_PASIEN
								},
								success: function (o) {
									var cst = Ext.decode(o.responseText);
									setLookUp_viDaftar(cst);
								}
							});
						} else {
							setLookUp_viDaftar();
							if (Ext.get('txtNamaKeluarga').getValue() != "") {
								Ext.get('txtNamaKeluarga').getValue(Ext.get('txtNamaKeluarga').getValue());
							} else {
								Ext.get('txtNamaKeluarga').getValue('');
							}
							if (Ext.get('txtAlamatPasien').getValue() != "") {
								Ext.getCmp('txtAlamat').setValue(Ext.get('txtAlamatPasien').getValue());
							} else {
								Ext.getCmp('txtAlamat').setValue('');
							}
							if (Ext.get('txtNamaPasien').getValue() != "") {
								Ext.getCmp('txtNama').setValue(Ext.get('txtNamaPasien').getValue());
							} else {
								Ext.getCmp('txtNama').setValue('');
							}
						}
					}
				}, {
					xtype: 'button',
					text: 'Pasien Baru',
					iconCls: 'Edit_Tr',
					hidden: false,
					tooltip: 'Edit Data',
					id: 'btntambah_viDaftar',
					handler: function (sm, row, rec) {

						setLookUp_viDaftar();
						Ext.Ajax.request({
							url: baseURL + "index.php/main/getcurrentshift",
							params: {
								command: '0'
							},
							failure: function (o) {
								var cst = Ext.decode(o.responseText);
							},
							success: function (o) {
								tampungshiftsekarang = o.responseText;
							}
						});
						if (Ext.get('txtNamaKeluarga').getValue() != "") {
							var tmpTanggalLahir = ShowDate(Ext.get('txtNamaKeluarga').getValue())
							Ext.get('txtNamaKeluarga').getValue(tmpTanggalLahir);
						} else {
							Ext.get('txtNamaKeluarga').getValue('');
						}
						if (Ext.get('txtAlamatPasien').getValue() != "") {
							Ext.getCmp('txtAlamat').setValue(Ext.get('txtAlamatPasien').getValue());
						} else {
							Ext.getCmp('txtAlamat').setValue('');
						}
						if (Ext.get('txtNamaPasien').getValue() != "") {
							var nama = Ext.get('txtNamaPasien').getValue();
							Ext.getCmp('txtNama').setValue(nama);
						} else {
							Ext.getCmp('txtNama').setValue('');
						}
					}
				}, {
					xtype: 'button',
					text: 'Booking Online',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					id: 'btnBookingOnline_viDaftar',
					handler: function (sm, row, rec) {
						var Form_BookingOnline = new Ext.Window({
							id: 'FormBookingOnline_viDaftar',
							name: 'FormBookingOnline_viDaftar',
							title: 'Booking Online',
							closeAction: 'destroy',
							width: 920,
							layout: 'fit',
							height: 500,
							resizable: false,
							constrain: true,
							autoScroll: false,
							iconCls: 'Edit_Tr',
							modal: true,
							items: [
								dataGrid_BookingOnline(),
							],
						});
						Form_BookingOnline.show();
					}
				},
				//hani 10-06-2022 //SATUSEHAT
                {
                    xtype: 'button',
                    text: 'History SATUSEHAT',
                    hidden: false,
                    iconCls: 'Edit_Tr',
                    tooltip: 'Edit Data',
                    id: 'btnHistorySatusehat_viDaftar',
                    handler: function(sm, row, rec) {
                        var Form_HistorySatusehat = new Ext.Window({
                            id: 'FormHistorySatusehat_viDaftar',
                            name: 'FormHistorySatusehat_viDaftar',
                            title: 'History SATUSEHAT',
                            closeAction: 'destroy',
                            width: 920,
							layout: 'absolute',
                            // layout: 'fit',
							maximized: true,
                            height: 500,
                            resizable: false,
                            // constrain: true,
                            autoScroll: false,
                            iconCls: 'Edit_Tr',
                            modal: true,
							html: '<img src="' + baseURL + 'ui/images/icons/16x16/header.png" style="width: 1366px; height: 80px;">',
                            items: [
								// imageheader(),
                                dataGrid_HistorySatusehat(),
                            ],
                        });
                        dataHistorySATUSEHAT_viDaftar(ihs_number);
                        Form_HistorySatusehat.show();
                    }
                },
                //===============
			]
		},
		bbar: bbar_pagingPendaftaran(mod_name_viDaftar, 5, dataSource_viDaftar),
		viewConfig: {
			forceFit: true
		}
	});
	var grData_viDaftar2 = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		store: dataSource_medrec_viDaftar,
		id: 'gridmedrec',
		autoScroll: true,
		columnLines: true,
		style: 'padding: 4px;',
		trackMouseOver: true,
		flex: 1,
		plugins: [new Ext.ux.grid.FilterRow()],
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowSelected_viDaftarHistory = dataSource_medrec_viDaftar.getAt(row);
					CurrentHistorydatapasienRWJ.row = row;
					CurrentHistorydatapasienRWJ.data = rowSelected_viDaftarHistory;
					datapasienvariable.kd_pasien = CurrentHistorydatapasienRWJ.data.data.KD_PASIEN;
					datapasienvariable.urut = CurrentHistorydatapasienRWJ.data.data.URUT;
					datapasienvariable.tglkunjungan = CurrentHistorydatapasienRWJ.data.data.TGL;
					datapasienvariable.kd_unit = CurrentHistorydatapasienRWJ.data.data.KD_UNIT;
				}
			}
		}),
		listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				console.log(ridx);
					rowSelected_viDaftarHistory = dataSource_medrec_viDaftar.getAt(ridx);
					CurrentHistorydatapasienRWJ.row = ridx;
					CurrentHistorydatapasienRWJ.data = rowSelected_viDaftarHistory;

					Ext.Ajax.request({
						method: 'POST',
						url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/GetConditionSubject",
						params: {
							ecounter: CurrentHistorydatapasienRWJ.data.data.ECOUNTER_ID
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							console.log(cst);
							dsConditionSatusehat.removeAll();
							var recs = [],
								recType = dsConditionSatusehat.recordType;

							for (
								var i = 0; i < cst.length; i++
							) {
								recs.push(new recType(cst[i]));
								console.log(cst[i].id);

								recs[i].data.category_code = cst[i].category_code;
								recs[i].data.category_display = cst[i].category_display;
								recs[i].data.category_system = cst[i].category_system
								recs[i].data.clinicalStatus_code =cst[i].clinicalStatus_code;
								recs[i].data.clinicalStatus_display =cst[i].clinicalStatus_display;
								recs[i].data.clinicalStatus_system =cst[i].clinicalStatus_system;
								recs[i].data.code_code = cst[i].code_code;
								recs[i].data.code_display = cst[i].code_display;
								recs[i].data.code_system = cst[i].code_system;
								recs[i].data.encounter_display = cst[i].encounter_display;
								recs[i].data.encounter_reference = cst[i].encounter_reference;
								recs[i].data.meta_lastUpdated = cst[i].meta_lastUpdated;
								recs[i].data.meta_versionId = cst[i].meta_versionId;
								recs[i].data.subject_display = cst[i].subject_display;
								recs[i].data.subject_reference = cst[i].subject_reference;
								recs[i].data.fullUrl = cst[i].fullUrl;
								recs[i].data.id = cst[i].id;
								recs[i].data.resourceType = cst[i].resourceType;
								recs[i].data.search = cst[i].search;							
							}
							dsConditionSatusehat.add(recs);
							console.log(dsConditionSatusehat);							
							SetLookUpCondition(dsConditionSatusehat);
						}, error: function (jqXHR, exception) {
							Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
						}
					});
				
			},
		},
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				id: 'col_KdPasienHistory_viDaftar',
				header: 'Kd Pasien',
				dataIndex: 'KD_PASIEN',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_KdUnitHistory_viDaftar',
				header: 'Kd Unit',
				dataIndex: 'KD_UNIT',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_UrutHistory_viDaftar',
				header: 'Urut',
				dataIndex: 'URUT',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_Poli_viDaftar',
				header: 'Unit',
				dataIndex: 'POLI',
				width: 300,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_Tanggal_viDaftar',
				header: 'Tanggal Kunjungan',
				dataIndex: 'TGL',
				width: 150,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_Dokter_viDaftar',
				header: 'Dokter',
				dataIndex: 'DOKTER',
				width: 150,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_TglKeluar_viDaftar',
				header: 'Tanggal Keluar',
				dataIndex: 'TGL_KELUAR',
				width: 150,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_Customer_viDaftar',
				header: 'Cara Bayar',
				dataIndex: 'CUSTOMER',
				width: 200,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}
		]),
		tbar: [
			{
				xtype: 'button',
				text: 'Hapus Kunjungan',
				iconCls: 'remove',
				// hidden: true,
				tooltip: 'Hapus Kunjungan',
				id: 'btnhapuskunjungan_viDaftar',
				handler: function (sm, row, rec) {
					Ext.MessageBox.confirm('Hapus Kunjungan', "Yakin Akan Hapus Kunjungan ini ?", function (btn) {
						if (btn === 'yes') {
							loadMask.show();
							Ext.Ajax.request({
								url: baseURL + "index.php/main/Controller_kunjungan/delete_kunjungan",
								params: {
									kd_pasien: rowSelected_viDaftarHistory.data.KD_PASIEN,
									kd_unit: rowSelected_viDaftarHistory.data.KD_UNIT,
									tgl_masuk: rowSelected_viDaftarHistory.data.TGL_MASUK,
									urut_masuk: rowSelected_viDaftarHistory.data.URUT,
									asal: 'rawat_jalan',
								},
								failure: function (o) {
									var cst = Ext.decode(o.responseText);
									loadMask.hide();
									ShowPesanInfo_viDaftar(cst.message, 'Hapus Data Kunjungan');
								},
								success: function (o) {
									var cst = Ext.decode(o.responseText);
									loadMask.hide();
									ShowPesanInfo_viDaftar(cst.message, 'Hapus Data Kunjungan');
									var tmpkriteria2 = " P.KD_PASIEN = '" + rowSelected_viDaftarHistory.data.KD_PASIEN + "'";
									datarefresh_medrec_viDaftar(tmpkriteria2);
								}
							});
						}
					});
				}
			}
		]
	}
	)
	var top_satu_rwj = new Ext.FormPanel({
		// labelAlign: 'top',
		flex: 0,
		bodyStyle: 'padding:4px',
		border: false,
		labelWidth: 60,
		height: 150,
		items: [
			{
				layout: 'column',
				border: false,
				items: [
					{
						columnWidth: .4,
						layout: 'form',
						border: false,
						items: [
							{
								xtype: 'textfield',
								fieldLabel: 'Mr / NIK',
								name: 'txtNoMedrec',
								id: 'txtNoMedrec',
								enableKeyEvents: true,
								style: 'text-align: right',
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										var tmpNoIIMedrec = Ext.get('txtNoMedrec').getValue()
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9) {
											if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10) {
												var tmpgetNoIIMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue())
												Ext.getCmp('txtNoMedrec').setValue(tmpgetNoIIMedrec);
												var tmpkriteria = getCriteriaFilter_viDaftar();
												datarefresh_viDaftar(tmpkriteria, 'Y');
												var tmpkriteria2 = getCriteriaFilter_viDaftar2();
												datarefresh_medrec_viDaftar(tmpkriteria2);
											} else {
												if (tmpNoIIMedrec.length === 10) {
													tmpkriteria = getCriteriaFilter_viDaftar();
													datarefresh_viDaftar(tmpkriteria, 'Y');
													var tmpkriteria2 = getCriteriaFilter_viDaftar2();
													datarefresh_medrec_viDaftar(tmpkriteria2);
													Ext.Ajax.request({
														url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien2",
														params: { part_number_nik: Ext.get('txtNoMedrec').getValue() },
														failure: function (o) {
															ShowPesanError_viDaftar('Pencarian error ! ', 'Rawat Jalan');
														},
														success: function (o) {
															var cst = Ext.decode(o.responseText);
															if (cst.success === true) {
																Ext.getCmp('txtNoMedrec').setValue(cst.nomedrec);
															}
														}
													})
												} else {
													Ext.getCmp('txtNoMedrec').setValue('');
												}
											}
										}
									}
								}
							},
							//hani 2023-03-07 //SATUSEHAT
							{
								xtype: 'textfield',
								fieldLabel: 'IHS Number',
								name: 'txtSearchIHSNumber',
								id: 'txtSearchIHSNumber',
								enableKeyEvents: true,
								style: 'text-align: right',
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										var tmpSearchIHSNumber = Ext.get('txtSearchIHSNumber').getValue();
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9) {
											Ext.Ajax.request({
												url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/cekIHSNumber",
												params: { ihs_number: tmpSearchIHSNumber},
												failure: function (o) {
													ShowPesanError_viDaftar('Pencarian error ! ', 'Rawat Jalan');
												},
												success: function (o) {
													var cst = Ext.decode(o.responseText);
													console.log(cst);
													setLookUp_PatientSATUSEHAT(cst)
													// if (cst.success === true) {
													// 	Ext.getCmp('txtNoMedrec').setValue(cst.nomedrec);
													// }
												}
											});
										}
									}
								}
							}, 
							//========================================================================= 
							{
								xtype: 'textfield',
								fieldLabel: 'Keluarga',
								name: 'txtNamaKeluargacari',
								id: 'txtNamaKeluargacari',
								enableKeyEvents: true,
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										var tmptanggal = Ext.get('txtNamaKeluargacari').getValue()
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9) {
											tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
											var tmpkriteria2 = getCriteriaFilter_viDaftar2();
											datarefresh_medrec_viDaftar(tmpkriteria2);
										}
									}
								}
							}, {
								xtype: 'textfield',
								fieldLabel: 'HP/Telp ',
								name: 'txtHPcari',
								id: 'txtHPcari',
								enableKeyEvents: true,
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										var tmptanggal = Ext.get('txtHPcari').getValue()
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9) {
											tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
											var tmpkriteria2 = getCriteriaFilter_viDaftar2();
											datarefresh_medrec_viDaftar(tmpkriteria2);
										}
									}
								}
							}, {
								xtype: 'displayfield',
								value: '[ENTER] Untuk Mencari.'
							}
						]
					}, {
						columnWidth: .4,
						layout: 'form',
						border: false,
						items: [
							{
								xtype: 'textfield',
								fieldLabel: 'Nama',
								name: 'txtNamaPasien',
								id: 'txtNamaPasien',
								enableKeyEvents: true,
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										if (Ext.EventObject.getKey() === 13) {
											var tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
											var tmpkriteria2 = getCriteriaFilter_viDaftar2();
											datarefresh_medrec_viDaftar(tmpkriteria2);
										}
									}
								}
							}, {
								xtype: 'textfield',
								fieldLabel: 'Alamat',
								name: 'txtAlamatPasien',
								enableKeyEvents: true,
								id: 'txtAlamatPasien',
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										if (Ext.EventObject.getKey() === 13) {
											var tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
											var tmpkriteria2 = getCriteriaFilter_viDaftar2();
											datarefresh_medrec_viDaftar(tmpkriteria2);
										}
									}
								}
							}, {
								xtype: 'textfield',
								fieldLabel: 'No. Ass',
								name: 'txtAsuransiPasien',
								id: 'txtAsuransiPasien',
								enableKeyEvents: true,
								anchor: '98%',
								listeners: {
									'specialkey': function () {
										if (Ext.EventObject.getKey() === 13) {
											var tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
											var tmpkriteria2 = getCriteriaFilter_viDaftar2();
											datarefresh_medrec_viDaftar(tmpkriteria2);
										}
									}
								}
							}
						]
					}, {
						// columnWidth: .15,
						xtype: 'fieldset',
						width: 150,
						labelWidth: 40,
						title: 'Panggil Antrian',
						layout: 'form',
						style: 'padding:4px;',
						border: true,
						items: [
							mComboPilihGrupLoketRWJ(),
							mComboPilihLoketRWJ(),
							{
								xtype: 'button',
								text: 'Panggil',
								anchor: '100%',
								id: 'btnAntrianDepan',
								handler: function () {
									var loket = Ext.getCmp('cboPilihLoket').getValue();
									var grup = Ext.getCmp('cboPilihGrupLoket').getValue();
									var txtPrioritas = false;
									if (Ext.getCmp('txtAntrianPrioritas').getValue() === '0' || Ext.getCmp('txtAntrianPrioritas').getValue() !== '') {
										txtPrioritas = true;
									} else {
										txtPrioritas = false;
									}
									if (grup === undefined || grup === '' || grup === 'Pilih Grup...') {
										ShowPesanWarning_viDaftar('Pilih grup terlebih dahulu', 'Antrian');
									} else if (loket === undefined || loket === '' || loket === 'Pilih Loket...') {
										ShowPesanWarning_viDaftar('Pilih loket terlebih dahulu', 'Antrian');
									} else {
										if (Ext.getCmp('txtAntrianPanggil').getValue() === '0' || Ext.getCmp('txtAntrianPrioritas').getValue() !== '') {
											var param = {
												nomor_antri: Ext.getCmp('txtAntrianPrioritas').getValue(),
												loket: loket_terpilih,
												prioritas: true,
												grup: Ext.getCmp('cboPilihGrupLoket').getValue(),
												txt_prioritas: txtPrioritas,
											};
											$.ajax({
												type: 'POST',
												dataType: 'JSON',
												crossDomain: true,
												url: baseURL + "index.php/rawat_jalan/functionRWJ/simpanDisplayAntrian",
												data: param,
												success: function (resp1) {
													loadMask.hide();
												},
												error: function (jqXHR, exception) {
													loadMask.hide();
												}
											});
										} else {
											var param = {
												nomor_antri: Ext.getCmp('txtAntrianPanggil').getValue(),
												loket: loket_terpilih,
												prioritas: false,
												grup: Ext.getCmp('cboPilihGrupLoket').getValue(),
												txt_prioritas: txtPrioritas,
											}
											$.ajax({
												type: 'POST',
												dataType: 'JSON',
												crossDomain: true,
												url: baseURL + "index.php/rawat_jalan/functionRWJ/simpanDisplayAntrian",
												data: param,
												success: function (resp1) {
													loadMask.hide();
												},
												error: function (jqXHR, exception) {
													loadMask.hide();
												}
											});
										}
									}
								}
							},{
								xtype: 'button',
								text: 'Panggil Ulang',
								anchor: '100%',
								id: 'btnPanggilUlang',
								handler: function () {
									var loket = Ext.getCmp('cboPilihLoket').getValue();
									var grup = Ext.getCmp('cboPilihGrupLoket').getValue();
									var txtPrioritas = false;
									if (Ext.getCmp('txtAntrianPrioritas').getValue() === '0' || Ext.getCmp('txtAntrianPrioritas').getValue() !== '') {
										txtPrioritas = true;
									} else {
										txtPrioritas = false;
									}
									if (grup === undefined || grup === '' || grup === 'Pilih Grup...') {
										ShowPesanWarning_viDaftar('Pilih grup terlebih dahulu', 'Antrian');
									} else if (loket === undefined || loket === '' || loket === 'Pilih Loket...') {
										ShowPesanWarning_viDaftar('Pilih loket terlebih dahulu', 'Antrian');
									} else {
										
											var param = {
												nomor_antri: Ext.getCmp('txtSudahPanggil').getValue(),
												loket: loket_terpilih,
												prioritas: true,
												grup: Ext.getCmp('cboPilihGrupLoket').getValue(),
												txt_prioritas: txtPrioritas,
											}
											$.ajax({
												type: 'POST',
												dataType: 'JSON',
												crossDomain: true,
												url: baseURL + "index.php/rawat_jalan/functionRWJ/PanggilUlangAntrian",
												data: param,
												success: function (resp1) {
													loadMask.hide();
												},
												error: function (jqXHR, exception) {
													loadMask.hide();
												}
											});
										
									}
								}
							},
							{
								xtype: 'button',
								text: 'History Panggilan',
								anchor: '100%',
								id: 'btnHistoryPanggilan',
								handler: function () {
									var loket = Ext.getCmp('cboPilihLoket').getValue();
									var grup = Ext.getCmp('cboPilihGrupLoket').getValue();
									
									if (grup === undefined || grup === '' || grup === 'Pilih Grup...') {
										ShowPesanWarning_viDaftar('Pilih grup terlebih dahulu', 'Antrian');
									} else {
										HistoryPanggilan(grup);
									}
								}
							},
						]
					}, {
						// columnWidth: .40,
						title: 'Antrian',
						style: 'padding-left:4px;',
						bodyStyle: 'padding:4px;',
						width: 300,
						// height:200,
						layout: 'column',
						items: [
							{
								columnWidth: .25,
								xtype: 'textfield',
								name: 'txtTotalAntrian',
								id: 'txtTotalAntrian',
								readOnly: true,
								style: 'font-weight:bold; font-size: 42px;text-align: center',
								// columnWidth:1,
								height: 70,
								enableKeyEvents: true,
								anchor: '100%',
								listeners: {
									'specialkey': function () {
										if (Ext.EventObject.getKey() === 13) {
											var tmpkriteria = getCriteriaFilter_viDaftar();
											datarefresh_viDaftar(tmpkriteria);
										}
									}
								}
							}, {
								columnWidth: .25,
								layout: 'column',
								border: false,
								items: [
									{
										xtype: 'textfield',
										readOnly: true,
										name: 'txtSudahPanggil',
										id: 'txtSudahPanggil',
										style: 'font-weight:bold; font-size: 42px;text-align: center',
										columnWidth: 1,
										height: 70,
										enableKeyEvents: true,
										anchor: '100%',
										listeners: {
											'specialkey': function () {
												if (Ext.EventObject.getKey() === 13) {
													var tmpkriteria = getCriteriaFilter_viDaftar();
													datarefresh_viDaftar(tmpkriteria);
												}
											}
										}
									}
								]
							}, {
								columnWidth: .25,
								layout: 'column',
								border: false,
								items: [
									{
										xtype: 'textfield',
										name: 'txtAntrianPanggil',
										id: 'txtAntrianPanggil',
										style: 'font-weight:bold; font-size: 42px;text-align: center',
										columnWidth: 1,
										height: 70,
										enableKeyEvents: true,
										anchor: '100%',
										listeners: {
											'specialkey': function () {
												if (Ext.EventObject.getKey() === 13) {
													var tmpkriteria = getCriteriaFilter_viDaftar();
													datarefresh_viDaftar(tmpkriteria);
												}
											}
										}
									},
									{
										xtype: 'button',
										columnWidth: 1,
										text: 'Lewati',
										id: 'btnAntrianUlangi',
										hidden: false,
										handler: function () {
											var loket = Ext.getCmp('cboPilihLoket').getValue();
											var grup = Ext.getCmp('cboPilihGrupLoket').getValue();
											var txtPrioritas = false;
											if (Ext.getCmp('txtAntrianPrioritas').getValue() === '0' || Ext.getCmp('txtAntrianPrioritas').getValue() !== '') {
												txtPrioritas = true;
											} else {
												txtPrioritas = false;
											}
											if (grup === undefined || grup === '' || grup === 'Pilih Grup...') {
												ShowPesanWarning_viDaftar('Pilih grup terlebih dahulu', 'Antrian');
											} else {
												if (Ext.getCmp('txtAntrianPanggil').getValue() === '0' || Ext.getCmp('txtAntrianPrioritas').getValue() !== '') {
													var param = {
														nomor_antri: Ext.getCmp('txtAntrianPrioritas').getValue(),
														loket: loket_terpilih,
														prioritas: true,
														grup: Ext.getCmp('cboPilihGrupLoket').getValue(),
														txt_prioritas: txtPrioritas,
													};
													$.ajax({
														type: 'POST',
														dataType: 'JSON',
														crossDomain: true,
														url: baseURL + "index.php/rawat_jalan/functionRWJ/simpanDisplayAntrian",
														data: param,
														success: function (resp1) {
															loadMask.hide();
														},
														error: function (jqXHR, exception) {
															loadMask.hide();
														}
													});
												} else {
													var param = {
														nomor_antri: Ext.getCmp('txtAntrianPanggil').getValue(),
														loket: loket_terpilih,
														prioritas: false,
														grup: Ext.getCmp('cboPilihGrupLoket').getValue(),
														txt_prioritas: txtPrioritas,
													}
													$.ajax({
														type: 'POST',
														dataType: 'JSON',
														crossDomain: true,
														url: baseURL + "index.php/rawat_jalan/functionRWJ/LewatiAntrian",
														data: param,
														success: function (resp1) {
															loadMask.hide();
														},
														error: function (jqXHR, exception) {
															loadMask.hide();
														}
													});
												}
											}
										}
									}
								]
							}, {
								columnWidth: .25,
								layout: 'column',
								border: false,
								items: [
									{
										xtype: 'textfield',
										columnWidth: 1,
										name: 'txtAntrianPrioritas',
										id: 'txtAntrianPrioritas',
										style: 'font-weight:bold; font-size: 42px;text-align: center',
										height: 70,
										enableKeyEvents: true
									},
								]
							}
						]
					}
				]
			}
		]
	});
	var FrmTabs_viDaftar = new Ext.Panel({
		id: mod_id,
		region: 'center',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		title: NamaForm_viDaftar,
		border: false,
		closable: true,
		iconCls: 'Studi_Lanjut',
		items: [top_satu_rwj, grData_viDaftar2, grData_viDaftar],
		listeners: {
			'afterrender': function () {
				datarefresh_viDaftar(tmpcriteriaRWJ);
			}
		}
	});
	return FrmTabs_viDaftar;
}
function setLookUp_viDaftar(rowdata) {
	var lebar = 900;
	setLookUps_viDaftar = new Ext.Window({
		id: 'SetLookUps_viDaftar',
		name: 'SetLookUps_viDaftar',
		title: NamaForm_viDaftar,
		closeAction: 'destroy',
		// width: lebar,
		layout: 'fit',
		// height: 620,
		maximized: true,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: getFormItemEntry_viDaftar(lebar, rowdata),
		listeners: {
			activate: function () {
				Ext.getCmp('txtNama').focus(true, 10);
				Ext.getCmp('txtJamKunjung').hide();
				Ext.getCmp('dptTanggal').hide();
				Ext.getCmp('cboPerseorangan').hide();
				Ext.getCmp('cboAsuransi').hide();
				Ext.getCmp('cboPerusahaanRequestEntry').hide();
			},
			afterShow: function () {
				this.activate();
			},
			deactivate: function () {
				// alert();
				// rowSelected_viDaftar = undefined;
				datarefresh_viDaftar(tmpcriteriaRWJ);
				mNoKunjungan_viKasir = '';
				Ext.getCmp('txtNoMedrec').setValue('');
				Ext.getCmp('txtNamaKeluargacari').setValue('');
				Ext.getCmp('txtAlamatPasien').setValue('');
				Ext.getCmp('txtNamaPasien').setValue('');
				Ext.getCmp('btntambah_viDaftar').show();
				Ext.getCmp('btnEdit_viDaftar').hide();
				Ext.getCmp('txtNoMedrec').focus();
			}
		}
	});
	setLookUps_viDaftar.show();
	if (rowdata == undefined) {
		autocomdiagnosa_master = '';
		dataaddnew_viDaftar();
		getIdGetDataSettingPendaftaran();
	} else {
		autocomdiagnosa_master = '';
		datainit_viDaftar(rowdata);
	}
	//Ext.getCmp('txtNama').focus();
}
function refreshComboGrupLoketRWJ() {
	dsPilihGrupLoket.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboPilihGrupList',
			param: ''
		}
	});
}
function mComboPilihGrupLoketRWJ() {
	var Field = ['ID_GROUP', 'NAMA_GROUP', 'ID_NAMA'];
	dsPilihGrupLoket = new WebApp.DataStore({ fields: Field });
	refreshComboGrupLoketRWJ();
	var cboPilihGrupLoketRWJ = new Ext.form.ComboBox({
		id: 'cboPilihGrupLoket',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Grup...',
		fieldLabel: 'Grup',
		align: 'Right',
		width: 120,
		tabIndex: 58,
		store: dsPilihGrupLoket,
		valueField: 'ID_GROUP',
		displayField: 'ID_NAMA',
		anchor: '95%'
	});
	return cboPilihGrupLoketRWJ;
}
//hani 2023-10-09 //HISTORY PANGGILAN ANTRIAN
var dsHistoryPanggilan
function HistoryPanggilan(data) {
	//console.log(dataRowIndexDetail);
	//
	// dsHistoryPanggilan.removeAll();
	loaddatastoreHistoryPanggilan(data);
	varHistoryPanggilan = new Ext.Window
		(
			{
				id: 'idFormHistoryPanggilan',
				title: 'History Panggilan',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle: 'padding: 3px;',
				items: [
					GridHistoryPanggilan(),
					// paneltotal
				],
				listeners:
				{
					activate: function () {

					},
					afterShow: function () {
						this.activate();

					},
					deactivate: function () {

					},
					close: function () {
						/* if(FocusExitResepRWJ == false){
							var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
							AptResepRWJ.form.Grid.a.startEditing(line, 4);	
						} */
					}
				}
			}
		);
	varHistoryPanggilan.show();
};


function GridHistoryPanggilan() {
	var rowLine = 0;
	var rowData = "";
	var currentRowSelectionListKunjunganResepRWJ;
	var Field = ['id_group', 'no_antrian','kode_loket','jam_panggilan_terakhir','qty_panggilan'];
	dsHistoryPanggilan = new WebApp.DataStore({ fields: Field });
	//loaddatastoreProdukComponent();

	var cm = new Ext.grid.ColumnModel({
		// specify any defaults for each column
		defaults: {
			sortable: false // columns are not sortable by default           
		},
		columns: [
			{
				header: 'ID Group',
				dataIndex: 'id_group',
				width: 25,
			},
			{
				header: 'No. Antrian',
				dataIndex: 'no_antrian',
				width: 25,
			},
			{
				header: 'Kode Loket',
				dataIndex: 'kode_loket',
				width: 25,
			},
			{
				header: 'Jam Panggilan Terakhir',
				dataIndex: 'jam_panggilan_terakhir',
				width: 50,
			},
			{
				header: 'Jumlah Panggilan',
				dataIndex: 'qty_panggilan',
				width: 50,
			},
		]
	});

	gridHistoryPanggilan = new Ext.grid.EditorGridPanel
		(
			{
				title: '',
				id: 'idGridHistoryPanggilan',
				store: dsHistoryPanggilan,
				clicksToEdit: 1,
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows: true,
				trackMouseOver: true,
				height: 250,
				width: 560,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
					(
						{
							singleSelect: true,
							listeners:
							{
							}
						}
					),
				sm: new Ext.grid.RowSelectionModel
					(
						{
							singleSelect: true,
							listeners:
							{
								rowselect: function (sm, row, rec) {
									rowLine = row;
									currentRowSelectionListKunjunganResepRWJ = undefined;
									currentRowSelectionListKunjunganResepRWJ = dsDataStoreGridDiagnosa.getAt(row);
								}
							}
						}
					),
				cm: cm,
				viewConfig: {
					forceFit: true
				},
				listeners: {
					'keydown': function (e, d) {
						if (e.getKey() == 13) {
							// autocomdiagnosa_master = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
							// tmp_diagnosa = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
							// Ext.getCmp('txtDiagnosa_RWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit + " - " + currentRowSelectionListKunjunganResepRWJ.data.penyakit);
							// varPilihDiagnosaLookUp_PendaftaranRWJ.close();
							// Ext.getCmp('txtNoAskes').focus(true, 10);
							// console.log(tmp_diagnosa);
						}
					}
				}
			}
		);
	return gridHistoryPanggilan;
};


function loaddatastoreHistoryPanggilan(params) {
	// console.log(params);
	// console.log(params);
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/HistoryPanggilan",
		params: {grup : params },
		success: function (response) {
			var cst = Ext.decode(response.responseText);
			// PilihDiagnosaLookUp_PendaftaranRWI();
			if (cst['listData'].length > 0) {
				for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
					var recs = [], recType = dsHistoryPanggilan.recordType;
					var o = cst['listData'][i];
					recs.push(new recType(o));
					dsHistoryPanggilan.add(recs);
				}

				// Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getView().refresh();
				// Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getSelectionModel().selectRow(0);
				// Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getView().focusRow(0);
			} else {
				varHistoryPanggilan.close();
				// Ext.MessageBox.alert('Informasi', 'Tindakan ' + params.text + ' tidak ada', function () {
				// 	Ext.getCmp('txtDiagnosa_RWJ').focus(true, 10);
				// });
			}
		},
	});
}

//=================================================================
function formLookup_viDataBPJS(rowdata) {
	var lebar = 900;
	formLookups_viDataBPJS = new Ext.Window({
		id: 'formLookup_viDataBPJS',
		name: 'formLookup_viDataBPJS',
		title: 'Data BPJS',
		closeAction: 'destroy',
		width: lebar,
		height: 450,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: getFormItemEntry_viDataBPJS(lebar, rowdata),
		listeners: {
			activate: function () {
				Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').focus();
			},
			afterShow: function () {
				this.activate();
			}
		}
	});
	formLookups_viDataBPJS.show();
	Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').setValue(Ext.getCmp('txtNoAskes').getValue());
	Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').setValue(Ext.getCmp('txtNoAskes').getValue());
	Ext.getCmp('txtNoBpjsHistorySEP').setValue(Ext.getCmp('txtNoAskes').getValue());
	Ext.getCmp('txtNoBpjsFKTP').setValue(Ext.getCmp('txtNoAskes').getValue());
}
function getFormItemEntry_viDataBPJS(lebar, rowdata) {
	var pbar1 = new Ext.ProgressBar({
		text: 'Initializing...'
	});
	var pnlFormDataBasic_viDataBPJS = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		autoScroll: true,
		width: '100%',
		height: 420,
		border: false,
		items: [
			getPenelItemDataKunjungan_viDataBPJS_prwj(lebar)
		],
		fileUpload: true,
	});
	return pnlFormDataBasic_viDataBPJS;
}
function load_data_printer_pendaftaranrwj(param) {
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/printer",
		params: {
			command: param
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsprinter_pendaftaranrwj.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsprinter_pendaftaranrwj.add(recs);
			}
		}
	});
}
function mCombo_printer_pendaftaranrwj() {
	var Field = ['kd_pasien', 'nama', 'kd_dokter', 'kd_unit', 'kd_customer', 'no_transaksi', 'kd_kasir', 'id_mrresep', 'urut_masuk', 'tgl_masuk', 'tgl_transaksi'];
	dsprinter_pendaftaranrwj = new WebApp.DataStore({ fields: Field });
	load_data_printer_pendaftaranrwj();
	var cbo_printer_pendaftaranrwj = new Ext.form.ComboBox({
		id: 'cbopasienorder_printer_pendaftaranrwj',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: 'Pilih Printer',
		fieldLabel: '',
		align: 'Right',
		width: 100,
		store: dsprinter_pendaftaranrwj,
		valueField: 'name',
		displayField: 'name'
	});
	return cbo_printer_pendaftaranrwj;
}
function printRWJStatusPasien() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: dataRWJcetakstatuspasien(),
		failure: function (o) {
			ShowPesanError_viDaftar('Data tidak berhasil di Cetak ', 'Cetak Data');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				ShowPesanInfo_viDaftar('Status Pasien Segera di Cetak', 'Cetak Data');
			} else if (cst.success === false && cst.pesan === 0) {
				ShowPesanWarning_viDaftar('Data tidak berhasil di Cetak ' + cst.pesan, 'Cetak Data');
			} else {
				ShowPesanError_viDaftar('Data tidak berhasil di Cetak ' + cst.pesan, 'Cetak Data');
			}
		}
	});
}
function printRWJStatusPasienBaru() {
	var win = window.open(baseURL + "index.php//rawat_jalan/statuspasienprinting/printTracerBaru/" + Ext.get('txtNoRequest').getValue(), '_blank');
	win.focus();
}
function printRWJBuktiDaftar() {
	var win = window.open(baseURL + "index.php//rawat_jalan/statuspasienprinting/printBukti/" + Ext.get('txtNoRequest').getValue(), '_blank');
	win.focus();
}
function dataRWJcetakstatuspasien() {
	var paramsstatuspasienpendaftaran = {
		Table: 'statuspasienprinting',
		NoMedrec: Ext.get('txtNoRequest').getValue(),
		NamaPasien: Ext.get('txtNama').getValue(),
		Alamat: Ext.get('txtAlamat').getValue(),
		Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
		TanggalMasuk: Ext.get('dptTanggal').getValue(),
		KdDokter: kodeDokterDefault//Ext.getCmp('cboDokterRequestEntry').getValue()
	};
	return paramsstatuspasienpendaftaran
}
function getFormItemEntry_viDaftar(lebar, rowdata) {
	var pbar1 = new Ext.ProgressBar({
		text: 'Initializing...'
	});
	var pnlFormDataBasic_viDaftar = new Ext.FormPanel({
		// title: '',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		bodyStyle: 'padding:4px;',
		// anchor: '100%',
		autoScroll: true,
		width: lebar,
		// height: 960,
		border: false,
		items: [
			getItemPanelInputBiodata_viDaftar(),
			getPenelItemDataKunjungan_viDaftar_prwj(lebar),
			getItemTxtNoreg_viDaftar()
		],
		// fileUpload: true,
		tbar: [
			{
				xtype: 'button',
				text: 'Add',
				iconCls: 'add',
				id: 'btnAdd_viDaftar',
				handler: function () {
					dataaddnew_viDaftar();
				}
			}, {
				xtype: 'tbseparator'
			}, {
				xtype: 'button',
				text: 'Save',
				iconCls: 'save',
				id: 'btnSimpan_viDaftar',
				handler: function () {
					// if (true) {}
					// console.log(selectSukuRequestEntry);
					// if ( Ext.getCmp('cboSukuRequestEntry').getValue() == "" ) {
					// 	ShowPesanInfo_viDaftar('Suku harap diisi', 'Peringatan');
					// 	Ext.getCmp('cboSukuRequestEntry').focus(true);
					// }else{
					// 	if (polipilihanpasien == '220' && tmp_sub_unit_rehab == "") {
					// 		ShowPesanError_viDaftar('Kunjungan Rehab medik harap mengisi sub unit', 'Simpan Kunjungan');
					// 	}else{
					// 		datasave_viDaftar(addNew_viDaftar,false, false);
					// 	}
					// 	Ext.getCmp('txtNama').focus(true);
					// }
					// 
					loadMask.show();
					if (polipilihanpasien == '230' && tmp_sub_unit_rehab == "") {
						loadMask.hide();
						ShowPesanError_viDaftar('Kunjungan Rehab medik harap mengisi sub unit', 'Simpan Kunjungan');
					} else if(Ext.isEmpty(Ext.getCmp('cboSukuRequestEntry').getValue())) {
						console.log(Ext.getCmp('cboSukuRequestEntry').getValue());
						loadMask.hide();
						ShowPesanInfo_viDaftar('Suku harap diisi', 'Peringatan');
						Ext.getCmp('cboSukuRequestEntry').focus(true);
					} else {
						if (ValidasiEntry_viDaftar('Simpan Data', false) == 1) {
							console.log("save");

							Ext.Ajax.request({
								url: baseURL + "index.php/rawat_jalan/Controller_kunjungan/save",
								params: dataparam_viDaftar(),
								failure: function (o) {
									loadMask.hide();
									tmp_sub_unit_rehab = "";
								console.log(o);
								ShowPesanInfo_viDaftar(o.statusText, 'Informasi');

							//Ext.getCmp('btnSimpan_viDaftar').enable();
								},
								success: function (o) {
									loadMask.hide();
									Ext.getCmp('btnSimpan_viDaftar').enable();
									console.log(o);

									// Ext.getCmp('btnSimpan_viDaftar').disable();
									var cst = Ext.decode(o.responseText);
									ShowPesanInfo_viDaftar(cst.message, 'Informasi');
									tmp_sub_unit_rehab = "";
									Ext.getCmp('txtNoRequest').setValue(cst.kd_pasien);
									resp_bpjs.kd_pasien = cst.kd_pasien;
									resp_bpjs.kd_unit = cst.kd_unit;
									resp_bpjs.tgl_masuk = cst.tgl_masuk;
									resp_bpjs.urut_masuk = cst.urut_masuk;
									printtracer();
								}
							});
						}
					}
				}
			}, {
				xtype: 'button',
				text: 'Save & Close',
				hidden: true,
				iconCls: 'saveexit',
				id: 'btnSimpanExit_viDaftar',
				handler: function () {
					var x = datasave_viDaftar(addNew_viDaftar, false, false);
					datarefresh_viDaftar(tmpcriteriaRWJ);
					if (x === undefined) {
						setLookUps_viDaftar.close();
					}
				}
			}, {
				xtype: 'tbseparator'
			}, {
				xtype: 'button',
				text: 'Print Data Pasien ',
				iconCls: 'print',
				id: 'btnPrintdatarwj',
				handler: function () {
					if (Ext.get('txtNoRequest').getValue() == 'Automatic from the system...') {
						return;
					} else {
						window.open(baseURL + "index.php/main/formulir/cetak/" + Ext.get('txtNoRequest').getValue() + "/" + Ext.get('cboPoliklinikRequestEntry').getValue());
					}
				}
			},
			mCombo_printer_pendaftaranrwj(), {
				xtype: 'splitbutton',
				text: 'Cetak',
				iconCls: 'print',
				id: 'btnPrint_viDaftar',
				handler: function () {
					GetPrintKartu();
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							xtype: 'button',
							text: 'Print Bill',
							hidden: true,
							id: 'btnPrintBill',
							handler: function () {
								if (Ext.getCmp('cbopasienorder_printer_pendaftaranrwj').getValue() === '' || Ext.getCmp('cbopasienorder_printer_pendaftaranrwj').getValue() === undefined) {
									ShowPesanWarning_viDaftar('Pilih printer terlebih dahulu !', 'Cetak Data');
								} else {
									printbill();
								}
							}
						}, {
							xtype: 'button',
							text: 'Print ',
							id: 'btnPrint',
							handler: function () {
								var criteria = 'kd_pasien = ' + Ext.get('txtNoRequest').getValue() + ' And kd_unit = ' + Ext.getCmp('cboPoliklinikRequestEntry').getValue();
								ShowReport('0', 1010204, criteria);
							}
						}, {
							xtype: 'button',
							text: 'Print Kartu Plastik [F8]',
							id: 'btnPrint',
							handler: function () {
								var url = baseURL + "index.php/rawat_jalan/cetakkartupasien/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Kartu Pasien',
									width: 700,
									height: 400,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						}, {
							xtype: 'button',
							text: 'SEP [F6]',
							id: 'btnPrintSEP',
							handler: function () {
								//cekKunjunganPasienLaluCetakBpjs();
								// _bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(),undefined,Ext.getCmp('txtcatatan').getValue());

								var url = baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/true";
								new Ext.Window({
									title: 'SEP',
									width: 900,
									height: 600,
									constrain: true,
									modal: false,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
									tbar: [
										{
											xtype: 'button',
											text: '<i class="fa fa-print"></i> Cetak',
											handler: function () {
												window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/false", "_blank");
											}
										}
									]
								}).show();
							}
						}, {
							xtype: 'button',
							text: 'Status Pasien',
							id: 'btnStatusPasienRWJ',
							handler: function () {
								printRWJStatusPasien();
							}
						}, {
							xtype: 'button',
							text: 'Status Pasien Baru',
							id: 'btnStatusPasienRWJBaru',
							handler: function () {
								printRWJStatusPasienBaru();
							}
						}, {
							xtype: 'button',
							text: 'Bukti Pendaftaran',
							id: 'btnBuktiDaftarRWJBaru',
							handler: function () {
								window.open(baseURL + "index.php/laporan/lap_bukti_daftar/cetak/" + Ext.get('txtNoRequest').getValue() + "/" + polipilihanpasien + "/" + now.format("Y-m-d") + "/0", "_blank");
								// printRWJBuktiDaftar();
							}
						}, {
							xtype: 'button',
							text: 'Kartu Pasien',
							id: 'btnPrintTracer',
							hidden: false,
							handler: function () {
								// printRWJKartuPasien();
								var url = baseURL + "index.php/rawat_jalan/kartupasienprinting/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Kartu Pasien',
									width: 800,
									height: 500,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						}, {
							xtype: 'button',
							text: 'Label Barcode [F7]',
							id: 'btnPrintLabelPasien',
							handler: function () {
								// window.print();
								// window.open(baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak_/" + Ext.get('txtNoRequest').getValue());
								var url = baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Label Barcode',
									width: 900,
									height: 500,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						},
						{
							xtype: 'button',
							text: 'Kartu Kunjungan',
							id: 'btnPrintKartuKunjungan',
							handler: function () {
								// window.print();
								// window.open(baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak_/" + Ext.get('txtNoRequest').getValue());
								var url = baseURL + "index.php/rawat_jalan/cetakkartukunjungan/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Kartu Kunjungan',
									width: 900,
									height: 500,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						},
						{
							xtype: 'button',
							text: 'Data Umum Pasien',
							id: 'btnPrintDataUmumPasien',
							handler: function () {
								// window.print();
								// window.open(baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak_/" + Ext.get('txtNoRequest').getValue());
								var url = baseURL + "index.php/rawat_jalan/cetakDataUmumPasien/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Data Umum',
									width: 900,
									height: 500,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						},
						{
							xtype: 'button',
							text: 'General Consent',
							id: 'btnPrintGeneralConsent',
							handler: function () {
								// window.print();
								// window.open(baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak_/" + Ext.get('txtNoRequest').getValue());
								var url = baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak/" + Ext.get('txtNoRequest').getValue();
								new Ext.Window({
									title: 'Persetujuan Umum / General Consent',
									width: 900,
									height: 500,
									constrain: true,
									modal: true,
									html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
								}).show();
							}
						},
					]
				})
			}, {
				xtype: 'button',
				text: 'Data BPJS',
				id: 'btnDataBpjs',
				handler: function () {
					formLookup_viDataBPJS();
				}
			}, '-', {
				xtype: 'button',
				text: 'Update',
				iconCls: 'save',
				id: 'btnUpdate_viDaftar',
				handler: function () {
					// DISINI
					/*Ext.Ajax.request({
						url: baseURL + "index.php/main/Controller_kunjungan/update_kunjungan",
						method:'POST',
						params: dataparam_viDaftar(),
						failure: function(o){	
							var cst = Ext.decode(o.responseText);
							Ext.MessageBox.alert('Update', cst.message);
						},
						success: function(o){
							var cst = Ext.decode(o.responseText);
							if (cst.status === true){
								Ext.MessageBox.alert('Update','Data berhasil perbarui');
							}else{
								Ext.MessageBox.alert('Update', cst.message);
							}
						}
					});*/
					if (Ext.getCmp('txtNoRequest').getValue() == "" || Ext.getCmp('txtNoRequest').getValue() == undefined || Ext.getCmp('txtNoRequest').getValue() == 'undefined') {
						ShowPesanWarning_viDaftar('Simpan kunjungan terlebih dahulu', 'Update Kunjungan');
					} else {
						UpdateKunjungan_RWJIGD.function.dialog(Ext.getCmp('txtNoRequest').getValue(), "2");
					}
				}
			},'-', {
				xtype: 'button',
				text: 'Bridging SATUSEHAT',
				id: 'btnBridgingSATUSEHAT',
				handler: function () {
					cariSATUSEHAT(Ext.getCmp('txtnikPasien').getValue());
				}
			},'-', {
				xtype: 'button',
				text: 'Update SATUSEHAT',
				id: 'btnUpdateSATUSEHAT',
				handler: function () {
					formLookup_UpdateSATUSEHAT();
				}
			}
		]
	});
	return pnlFormDataBasic_viDaftar;
}
// SATUSEHAT 
// Hani 2022-10-18

function save_historyConditon(recs){
	console.log(recs);
	var parameter = {recs};
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/save_historyCondition",
		params: parameter
	});
}
function SetLookUpCondition(rowdata) {
        pnlTRConditionSATUSEHAT = new Ext.Window({
            title: "List Condition",
            id: "tabConditionSatuSehat",
            layout: "column",
            height: 350,
            width: 750,
            border: false,
            resizable: false,
            plain: true,
            closeAction: "destroy",
            modal: true,
            items: [Getdetail_Condition(rowdata)],
        });

        pnlTRConditionSATUSEHAT.show();
    }

function Getdetail_Condition(rowdata) {

	gridGetdetail_ConditionSatusehat = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dsConditionSatusehat,
		height: 290,
		anchor: "100%",
		border: false,
		columnLines: true,
		cellWrap: true,
		frame: false,
		width: "100%",
		autoScroll: true,
		scrollable: true,
		viewConfig: {
			getRowClass: function(record, index, rowParams) {
				return (Math.floor(index / 5.0) % 2 == 0) ? 'rowClass1' : 'rowClass2';
			}
		},
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function(sm, row, rec) {
					
				},
			},
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: "ID CONDITION",
				dataIndex: "id",
				width: 150,
				menuDisabled: true,
				hidden: false,
			},
			{
				header: "KODE DIAGNOSA",
				dataIndex: "code_code",
				width: 100,
				menuDisabled: true,
				hidden: false,
			},
			{
				header: "DIAGNOSA",
				dataIndex: "code_display",
				width: 200,
				menuDisabled: true,
				hidden: false,
			},
			{
				header: "TGL DIAGNOSA",
				dataIndex: "meta_lastUpdated",
				width: 200,
				menuDisabled: true,
				hidden: false,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
			{
				header: "NAMA PATIENT",
				dataIndex: "subject_display",
				width: 100,
				menuDisabled: true,
				hidden: false,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
			{
				header: "IHS NUMBER",
				dataIndex: "subject_reference",
				width: 100,
				menuDisabled: true,
				hidden: false,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
				{
				header: "KATEGORI DIAGNOSA",
				dataIndex: "category_display",
				width: 100,
				menuDisabled: true,
				hidden: false,
			},
			{
				header: "STATUS",
				dataIndex: "clinicalStatus_display",
				width: 100,
				menuDisabled: true,
				hidden: false,
			},
			{
				header: "fullUrl",
				dataIndex: "fullUrl",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			{
				header: "category_code",
				dataIndex: "category_code",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
		
			{
				header: "category_system",
				dataIndex: "category_system",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			{
				header: "clinicalStatus_code",
				dataIndex: "clinicalStatus_code",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			
			{
				header: "clinicalStatus_system",
				dataIndex: "clinicalStatus_system",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			
			{
				header: "code_system",
				dataIndex: "code_system",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			{
				header: "encounter_reference",
				dataIndex: "encounter_reference",
				width: 100,
				menuDisabled: true,
				hidden: true,
			},
			
			
			{
				header: "meta_versionId",
				dataIndex: "meta_versionId",
				width: 100,
				menuDisabled: true,
				hidden: true,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
			{
				header: "resourceType",
				dataIndex: "resourceType",
				width: 100,
				menuDisabled: true,
				hidden: true,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
			
			{
				header: "search",
				dataIndex: "search",
				width: 100,
				menuDisabled: true,
				hidden: true,
				renderer: function(value, metadata) {
					metadata.attr = 'style="white-space: normal;"';
					return value;
				},
			},
		]),
		listeners: {
			rowdblclick: function(sm, ridx, cidx) {
				rowSelected_ObatInfocomSub = dsConditionSatusehat.getAt(ridx);
				console.log(rowSelected_ObatInfocomSub);
				Ext.getCmp("txtSubFormCode_SetupObatAlkesL").setValue(
					rowSelected_ObatInfocomSub.data.subFormCode
				);
				Ext.getCmp("txtSubFormName_SetupObatAlkesL").setValue(
					rowSelected_ObatInfocomSub.data.subFormName
				);
				pnlTRListObatInfocomSub.close();
			},
		},
		renderer: function(value, metadata) {
			metadata.attr = 'style="white-space: normal;"';
			return value;
		},
		tbar: [
			{ xtype: "tbspacer", height: 3, width: 10 },
			{
				x: 420,
				y: 40,
				xtype: "button",
				text: "Simpan",
				iconCls: "add",
				hideLabel: false,
				hidden: true,
				handler: function(sm, row, rec) {
					//total_pasien_order_mng()
					SaveObatInfocom("ConditionSatusehat");
				},
			},
		],
	});

	return gridGetdetail_ConditionSatusehat;
}

function formLookup_UpdateSATUSEHAT() {
	var lebar = 900;
	formLookup_UpdateSATUSEHAT = new Ext.Window({
		id: 'formLookup_UpdateSATUSEHAT',
		name: 'formLookup_UpdateSATUSEHAT',
		title: 'Data SATUSEHAT',
		closeAction: 'destroy',
		width: lebar,
		height: 450,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: getFormItemEntry_viDataSATUSEHAT(lebar),
		listeners: {
			activate: function () {
				// Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').focus();
			},
			// afterShow: function () {
			// 	this.activate();
			// }
		}
	});
	formLookup_UpdateSATUSEHAT.show();
	Ext.getCmp('txtNoEcounterID').setValue(Ext.getCmp('txtEncounterID1').getValue());
	Ext.getCmp('txtNoIhsNumber').setValue(Ext.getCmp('txtIHSNumber').getValue());
	Ext.getCmp('txtNoIhsNumberGetCondition').setValue(Ext.getCmp('txtEncounterID1').getValue());
	
	
	
}
function getFormItemEntry_viDataSATUSEHAT(lebar, rowdata) {
	var pbar1 = new Ext.ProgressBar({
		text: 'Initializing...'
	});
	var pnlFormDataBasic_viDataBPJS = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		autoScroll: true,
		width: '100%',
		height: 420,
		border: false,
		items: [
			getPenelItemDataKunjungan_viDataSATUSEHAT(lebar)
		],
		fileUpload: true,
	});
	return pnlFormDataBasic_viDataBPJS;
}

function getPenelItemDataKunjungan_viDataSATUSEHAT(lebar) {
	var items = {
		xtype: 'tabpanel',
		plain: true,
		activeTab: 0,
		height: 400,
		width: '100%',
		defaults: {
			bodyStyle: 'padding:10px',
			autoScroll: true
		},
		items: [
			DataPanel1DataSATUSEHAT(),
			DataPanel2DataSATUSEHAT(),
			DataPanel3DataSATUSEHAT(),
			// DataPanel4DataSATUSEHAT(),
		]
	}
	return items;
}
function DataPanel1DataSATUSEHAT() {

	var items = {
		title: 'PUT Ecounter',
		layout: 'form',
		tabIndex: 19,
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'Ecounter ID',
				tabIndex: 37,
				name: 'txtNoEcounterID',
				id: 'txtNoEcounterID',
				width: 250,
				hidden: false,
				listeners: {
					'render': function (c) {
						
					}
				}
			}, {
				xtype: 'button',
				text: 'Update',
				iconCls: 'find',
				id: 'btnUpdateEcounter',
				handler: function () {
						UpdateEcounterFinishing();
				}
			}, {
				xtype: 'textarea',
				width: '100%',
				height: 230,
				name: 'textAreaEcounter',
				id: 'textAreaEcounter',
				readOnly: true
			}
		],
		buttons:
			[
			

			]
	};
	return items;
}
function UpdateEcounterFinishing() {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/UpdateEcounterFinish",
		params: {
			ecounter: Ext.getCmp('txtNoEcounterID').getValue(),
			ihsnumber : Ext.getCmp('txtIHSNumber').getValue(),
			condition : Ext.getCmp('txtIdDiagnosa_RWJ').getValue(),
			name_condition : name_diagnosa
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			var txt = _bpjs.generateObjectToString(cst.response);
			Ext.getCmp('textAreaEcounter').setValue(txt);
			// Ext.getCmp('txtIHSNumber').setValue(cst.response.entry[0].resource.id);
			// createEcounterID(cst.response.entry[0].resource.id)
			// Ext.getCmp('txtEncounterID').setValue(cst.encounterID);
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}

function DataPanel2DataSATUSEHAT() {

	var items = {
		title: 'GET Ecounter',
		layout: 'form',
		tabIndex: 19,
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'IHS Number',
				tabIndex: 37,
				name: 'txtNoIhsNumber',
				id: 'txtNoIhsNumber',
				width: 250,
				hidden: false,
				listeners: {
					'render': function (c) {
						
					}
				}
			}, {
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				id: 'btnGETEcounter',
				handler: function () {
						GetEcounterSubject();
				}
			}, {
				xtype: 'textarea',
				width: '100%',
				height: 230,
				name: 'textAreaGETEcounter',
				id: 'textAreaGETEcounter',
				readOnly: true
			}
		],
		buttons:
			[
			

			]
	};
	return items;
}

function GetEcounterSubject() {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/GetEcounterSubject",
		params: {
			ecounter: Ext.getCmp('txtNoEcounterID').getValue(),
			ihs_number : Ext.getCmp('txtIHSNumber').getValue(),
			condition : Ext.getCmp('txtIdDiagnosa_RWJ').getValue(),
			name_condition : name_diagnosa
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			// var txt = _bpjs.generateObjectToString(cst.response);
			var txt = JSON.stringify(cst,null,2);
			Ext.getCmp('textAreaGETEcounter').setValue(txt);
			// Ext.getCmp('txtIHSNumber').setValue(cst.response.entry[0].resource.id);
			// createEcounterID(cst.response.entry[0].resource.id)
			// Ext.getCmp('txtEncounterID').setValue(cst.encounterID);
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}

function DataPanel3DataSATUSEHAT() {

	var items = {
		title: 'GET Condition',
		layout: 'form',
		tabIndex: 19,
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'Ecounter ID',
				tabIndex: 37,
				name: 'txtNoIhsNumberGetCondition',
				id: 'txtNoIhsNumberGetCondition',
				width: 250,
				hidden: false,
				listeners: {
					'render': function (c) {
						
					}
				}
			}, {
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				id: 'btnGETCondition',
				handler: function () {
						GetConditionSubject();
				}
			}, {
				xtype: 'textarea',
				width: '100%',
				height: 230,
				name: 'textAreaGETCondition',
				id: 'textAreaGETCondition',
				readOnly: true
			}
		],
		buttons:
			[
			

			]
	};
	return items;
}

function GetConditionSubject() {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/GetConditionSubject",
		params: {
			ecounter: Ext.getCmp('txtNoEcounterID').getValue(),
			ihsnumber : Ext.getCmp('txtIHSNumber').getValue(),
			condition : Ext.getCmp('txtIdDiagnosa_RWJ').getValue(),
			name_condition : name_diagnosa
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			// var txt = _bpjs.generateObjectToString(cst.response);
			var txt = JSON.stringify(cst,null,2);
			Ext.getCmp('textAreaGETCondition').setValue(txt);
			// Ext.getCmp('txtIHSNumber').setValue(cst.response.entry[0].resource.id);
			// createEcounterID(cst.response.entry[0].resource.id)
			// Ext.getCmp('txtEncounterID').setValue(cst.encounterID);
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}

function cariSATUSEHAT(nik) {
		loadMask.show();
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/cariNIK",
		params: {
			nik: nik,
			kd_pasien: Ext.getCmp('txtNoRequest').getValue()
		},
		success: function (o) {
			console.log(o);
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if(cst.success){
				Ext.getCmp('txtIHSNumber').setValue(cst.entry[0].resource.id);
				LookUpConsent_SATUSEHAT(cst.entry[0].resource.id);
				// createEcounterID(cst.entry[0].resource.id)
				// Ext.getCmp('txtEncounterID').setValue(cst.encounterID);
			}else{
				ShowPesanWarning_viDaftar(cst.pesan,'Warning');
			}
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}
function LookUpConsent_SATUSEHAT(patient_id) {
	// console.log(tmp_data_approval_sep);
loadMask.hide();
	var lebar = 690;
	formLookups_ConsentSATUSEHAT = new Ext.Window({
		id: 'LookUpConsentSATUSEHAT',
		name: 'LookUpConsentSATUSEHAT',
		title: 'Consent SATUSEHAT',
		closeAction: 'destroy',
		width: lebar,
		height: 250, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: [
			panel_form_lookup_ConsentSATUSEHAT(patient_id)], //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		},
		fbar: [
			{
				xtype: 'button',
				text: "Simpan",
				id: 'btn_consent',
				hidden: false,
				handler: function () {
					
					var param = {
						patient_id: Ext.getCmp('txt_patient_id').getValue(),
						consent: consent
					}
					$.ajax({
						type: 'POST',
						dataType: 'JSON',
						url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/PostConsent/",
						data: param,
						success: function (cst) {
							if (cst.metaData.code == "200") {
								Ext.MessageBox.alert('Sukses', 'SPRI berhasil dihapus');
								formLookups_Con.close();

							} else {
								Ext.MessageBox.alert('Gagal', cst.metaData.message);
							}
						},
						error: function (jqXHR, exception) {
							loadMask.hide();
							Ext.MessageBox.alert('Gagal', 'Delete SPRI gagal.');
							//dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
							Ext.Ajax.request({
								url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
								params: {
									respon: jqXHR.responseText,
									keterangan: 'Error hapus SPRI'
								}
							});
						}
					});
						
				}
			},
			
		]
	});
	formLookups_ConsentSATUSEHAT.show();
}

function panel_form_lookup_ConsentSATUSEHAT(patient_id) {
	

	var formPanelConsentSATUSEHAT = new Ext.form.FormPanel({
		id: "myformpanel_ConsentSATUSEHAT",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .5,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 200,
		//anchor: '100% 100%',
		items: [
				{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'Patient ID'
			},
			{
				x: 100,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 5,
				xtype: 'textfield',
				id: 'txt_patient_id',
				name: 'txt_patient_id',
				width: 220,
				// disabled: true,
				allowBlank: false,
				readOnly: true,
				value:patient_id,
				tabIndex: 1
			},
			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'ID Consent'
			},
			{
				x: 100,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 30,
				xtype: 'textfield',
				id: 'tgl_consent_id',
				name: 'tgl_consent_id',
				// format: 'd/M/Y',
				readOnly: false,
				// value: now,
				width: 200,
			},
			{
				x: 10,
				y: 105,
				xtype: 'checkbox',
				boxLabel: '',
				name: 'cbo_consent',
				id: 'cbo_consent',
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							consent = 1;
						} else {
							consent = 0;
						}
					}
				}
			},
			{
				x: 30,
				y: 105,
				xtype: 'label',
				text: 'Saya menjamin bahwa pasien sudah membaca dan menandatangani consent pembukaan data dari SATUSEHAT',
				style: 'font-weight:bold; font-style: italic; text-decoration: underline;',
			},
		],

	});

	// if (rowdata != undefined) {
		// Ext.Ajax.request({
		// 	url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_history_rencana_kontrol",
		// 	params: { no_sep: tmp_data_approval_sep.peserta.noKartu, jenis_kontrol: 1 },
		// 	failure: function (o) {
		// 	},
		// 	success: function (o) {
		// 		var cst = Ext.decode(o.responseText);
		// 		console.log(cst);
		// 		if (cst.sudah_ada == true) {
		// 			Ext.getCmp('btn_simpan_spri').hide();
		// 			Ext.getCmp('btn_update_spri').show();
		// 			Ext.getCmp('btn_hapus_spri').show();
		// 			Ext.getCmp('btn_cetak_spri').show();
		// 			Ext.getCmp('txt_no_kartu_insert_spri').setValue(tmp_data_approval_sep.peserta.noKartu);
		// 			Ext.getCmp('txt_nama_peserta_insert_spri').setValue(tmp_data_approval_sep.peserta.nama);
		// 			Ext.getCmp('txt_no_kontrol_insert_spri').setValue(cst.listData.noSuratKontrol);
		// 			Ext.getCmp('txt_sex_insert_spri').setValue(tmp_data_approval_sep.peserta.sex);
		// 			Ext.getCmp('tgl_insert_spri').setValue(cst.listData.tglRencanaKontrol);
		// 			Ext.getCmp('tgl_surat_insert_spri').setValue(cst.listData.tglTerbitKontrol).disable();
		// 			jenis_kontrol_insert_rencana_kontrol = cst.listData.jnsKontrol;
		// 			select_poliKontrol = cst.listData.poliTujuan;
		// 			select_namapoliKontrol = cst.listData.namaPoliTujuan;
		// 			select_dokterKontrol = cst.listData.kodeDokter;
		// 			select_nama_dokterKontrol = cst.listData.namaDokter;
		// 			loaddatastoreDokterKontrol(select_poliKontrol);
		// 			Ext.getCmp('cboPoliKontrol_ugd').setValue(select_namapoliKontrol);
		// 			Ext.getCmp('cboDokterKontrol_ugd').setValue(select_nama_dokterKontrol);
		// 			if (jenis_kontrol_insert_rencana_kontrol == 1) {
		// 				// Ext.getCmp('combo_kontrol_insert_rencanan_kontrol_igd').setValue('SPRI').disable();
		// 				// loaddatastorePoliKontrol();
		// 				Ext.getCmp('btn_update_spri').setText('Update SPRI');
		// 				Ext.getCmp('btn_hapus_spri').setText('Hapus SPRI');
		// 				Ext.getCmp('btn_cetak_spri').setText('Cetak SPRI');
		// 			}
		// 			// loaddatastorePoliKontrol();

		// 		} else {
		// 			Ext.getCmp('btn_update_spri').hide();
		// 			Ext.getCmp('btn_hapus_spri').hide();
		// 			Ext.getCmp('btn_cetak_spri').hide();
		// 			Ext.getCmp('btn_simpan_spri').show();
		// 			Ext.getCmp('txt_no_kartu_insert_spri').setValue(tmp_data_approval_sep.peserta.noKartu);
		// 			Ext.getCmp('txt_nama_peserta_insert_spri').setValue(tmp_data_approval_sep.peserta.nama);
		// 			Ext.getCmp('txt_no_kontrol_insert_spri').setValue("");
		// 			Ext.getCmp('txt_sex_insert_spri').setValue(tmp_data_approval_sep.peserta.sex);
		// 			Ext.getCmp('tgl_surat_insert_spri').disable();
		// 			loaddatastorePoliKontrol();
		// 		}
		// 	}
		// });
	// }
	return formPanelConsentSATUSEHAT;
}
function createEcounterID(ihs_number) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/createEcounterID",
		params: {
			ihs_number: ihs_number,
			kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
			kd_dokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
			kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
			tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
		},
		success: function (o) {
			console.log(o);
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if(cst.success){
				// Ext.getCmp('txtIHSNumber').setValue(cst.response.entry[0].resource.id);
				createConditionDiagnosis(cst.response.id,ihs_number)
				Ext.getCmp('txtEncounterID1').setValue(cst.response.id);
			}else{
				ShowPesanWarning_viDaftar('Kunjungan Belum Ada','Warning');
			}
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}

function createConditionDiagnosis(ecounter, ihsnumber) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/createConditionDiagnosis",
		params: {
			ecounter: ecounter,
			ihsnumber : ihsnumber,
			code_icd10 :tmp_diagnosa,
			// display_icd10 : name_diagnosa,
			name_patient :Ext.getCmp('txtNama').getValue(),
			kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
			kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
			tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
		},
		success: function (o) {
			console.log(o);
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			autocomdiagnosa_master = cst.response.code.coding[0].code;
			tmp_diagnosa = cst.response.code.coding[0].code;
			name_diagnosa = cst.response.code.coding[0].display;
			Ext.getCmp('txtDiagnosa_RWJ').setValue(cst.response.code.coding[0].code + " - " + cst.response.code.coding[0].display);
			Ext.getCmp('txtIdDiagnosa_RWJ').setValue(cst.response.id);
			ShowPesanInfo_viDaftar('Bridging SATUSEHAT Berhasil !', 'Informasi');

		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}

function dataGrid_HistorySatusehat() {
    // var FieldMaster_viDaftar = ['TMP_NORM', 'NOMOR_KARTU', 'NIK', 'NOMOR_KK', 'NAMA', 'JENIS_KELAMIN', 'TANGGAL_LAHIR', 'NO_HP', 'ALAMAT', 'KODEPROP', 'NAMAPROP', 'KODEDATI2', 'NAMADATI2', 'KODEKEC', 'NAMAKEC', 'KODEKEL', 'NAMAKEL', 'RW', 'RT', 'IS_AKTIF', 'KD_PASIEN', 'KODEBOOKING', 'TGL_TRANSAKSI', 'NOMOR_ANTRIAN', 'NAMA_UNIT', 'KD_UNIT'];

    // dataSource_HistorySatusehat = new WebApp.DataStore({ fields: FieldMaster_viDaftar });

    var grData_viDaftar = new Ext.grid.EditorGridPanel({
        x: 0,
		y:100,
		xtype: 'editorgrid',
        store: dataSource_HistorySatusehat,
        autoScroll: true,
        columnLines: true,
        style: 'padding: 0px 4px 4px 4px, font-size: 20px',
		style: 'font-weight:bold; font-size: 42px;text-align: center',
        flex: 2,
		height:545,
        trackMouseOver: true,
		cls: 'custom-header-style',
		// bodyStyle: 'font-size:20px',
        plugins: [new Ext.ux.grid.FilterRow()],
        listeners: {
            rowdblclick: function(sm, ridx, cidx) {
                // rowSelected_viDaftar = dataSource_HistorySatusehat.getAt(ridx);
				rowSelected_viDaftarHistory = dataSource_HistorySatusehat.getAt(ridx);
					CurrentHistorydatapasienRWJ.row = ridx;
					CurrentHistorydatapasienRWJ.data = rowSelected_viDaftarHistory;
							console.log(rowSelected_viDaftarHistory);							

					Ext.Ajax.request({
						method: 'POST',
						url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/GetConditionSubject",
						params: {
							ecounter: CurrentHistorydatapasienRWJ.data.data.id_ecounter
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							console.log(cst);
							dsConditionSatusehat.removeAll();
							var recs = [],
								recType = dsConditionSatusehat.recordType;

							for (
								var i = 0; i < cst.length; i++
							) {
								recs.push(new recType(cst[i]));
								console.log(cst[i].id);

								recs[i].data.category_code = cst[i].category_code;
								recs[i].data.category_display = cst[i].category_display;
								recs[i].data.category_system = cst[i].category_system
								recs[i].data.clinicalStatus_code =cst[i].clinicalStatus_code;
								recs[i].data.clinicalStatus_display =cst[i].clinicalStatus_display;
								recs[i].data.clinicalStatus_system =cst[i].clinicalStatus_system;
								recs[i].data.code_code = cst[i].code_code;
								recs[i].data.code_display = cst[i].code_display;
								recs[i].data.code_system = cst[i].code_system;
								recs[i].data.encounter_display = cst[i].encounter_display;
								recs[i].data.encounter_reference = cst[i].encounter_reference;
								recs[i].data.meta_lastUpdated = cst[i].meta_lastUpdated;
								recs[i].data.meta_versionId = cst[i].meta_versionId;
								recs[i].data.subject_display = cst[i].subject_display;
								recs[i].data.subject_reference = cst[i].subject_reference;
								recs[i].data.fullUrl = cst[i].fullUrl;
								recs[i].data.id = cst[i].id;
								recs[i].data.resourceType = cst[i].resourceType;
								recs[i].data.search = cst[i].search;							
							}
							dsConditionSatusehat.add(recs);
							console.log(dsConditionSatusehat);							
							SetLookUpCondition(dsConditionSatusehat);
						}, error: function (jqXHR, exception) {
							Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
						}
					});
			}
        },
		
        colModel: new Ext.grid.ColumnModel([
		{
            id: 'colNM_id_ecounter_viDaftar',
            header: 'ID KUNJUNGAN',
            dataIndex: 'id_ecounter',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 300,
			renderer: renderColumnWithStyle,
			headerCssClass: 'custom-header-style'
			

        }, {
            id: 'colNM_identifier_viDaftar',
            header: 'NO. MEDREC',
            dataIndex: 'identifier',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 100,
			renderer: renderColumnWithStyle

        },  {
            id: 'col_subject_display_viDaftar',
            header: 'NAMA PATIENT',
            dataIndex: 'subject_display',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 100,
			renderer: renderColumnWithStyle

        }, {
            id: 'col_period_start_viDaftar',
            header: 'TGL KUNJUNGAN',
            dataIndex: 'period_start',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 130,
			renderer: renderColumnWithStyle

        },{

            id: 'colNM_location_display_viDaftar',
            header: 'LOKASI BERKUNJUNG',
            dataIndex: 'location_display',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 750,
			renderer: renderColumnWithStyle

        }, {
            id: 'colNM_participant_display_viDaftar',
            header: 'NAMA DOKTER',
            dataIndex: 'participant_display',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 100,
			renderer: renderColumnWithStyle

        },{
            id: 'colNRM_location_reference_viDaftar',
            header: 'ID LOKASI',
            dataIndex: 'location_reference',
            sortable: true,
            hideable: false,
            menuDisabled: true,
            width: 150,

			renderer: renderColumnWithStyle
        },{
            id: 'col_subject_reference_viDaftar',
            header: 'IHS NUMBER',
            dataIndex: 'subject_reference',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 150,

			renderer: renderColumnWithStyle
        }, {
            id: 'col_status_viDaftar',
            header: 'STATUS',
            dataIndex: 'status',
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 150,
			renderer: renderColumnWithStyle

        },
		{
            id: 'colNM_class_code_viDaftar',
            header: 'class_code',
            dataIndex: 'class_code',
            hideable: false,
            menuDisabled: true,
            sortable: true,
			hidden: true,
            width: 150,

        }, {
            id: 'colNM_class_display_viDaftar',
            header: 'class_display',
            dataIndex: 'class_display',
            hideable: false,
			hidden: true,
            menuDisabled: true,
            sortable: true,
            width: 150,

        },  {
            id: 'colNM_last_updated_viDaftar',
            header: 'last_updated',
            dataIndex: 'last_updated',
            sortable: true,
            hideable: false,
			hidden: true,
            menuDisabled: true,
            width: 100,

        }, {
            id: 'colNM_version_id_viDaftar',
            header: 'version_id',
            dataIndex: 'version_id',
			hidden: true,
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_participant_reference_viDaftar',
            header: 'participant_reference',
            dataIndex: 'participant_reference',
			hidden: true,
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_participant_type_code_viDaftar',
            header: 'participant_type_code',
            dataIndex: 'participant_type_code',
            hideable: false,
            menuDisabled: true,
			hidden: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_participant_type_display_viDaftar',
            header: 'participant_type_display',
            dataIndex: 'participant_type_display',
			hidden: true,
            hideable: false,
            menuDisabled: true,
			hidden: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_resource_type_viDaftar',
            header: 'resource_type',
            dataIndex: 'resource_type',
            hideable: false,
            menuDisabled: true,
			hidden: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_service_provider_viDaftar',
            header: 'service_provider',
            dataIndex: 'service_provider',
            hideable: false,
            menuDisabled: true,
			hidden: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_status_history_period_start_viDaftar',
            header: 'status_history_period_start',
            dataIndex: 'status_history_period_start',
            hideable: false,
			hidden: true,
            menuDisabled: true,
            sortable: true,
            width: 150,

        }, {
            id: 'col_status_history_status_viDaftar',
            header: 'status_history_status',
            dataIndex: 'status_history_status',
			hidden: true,
            hideable: false,
            menuDisabled: true,
            sortable: true,
            width: 150,

        }, 
	]),
        viewConfig: {
            // forceFit: true
        }
    });
    return grData_viDaftar;
}

function renderColumnWithStyle(value, metadata, record, rowIndex, colIndex, store) {
    // Apply custom styles to the cell
	if (rowIndex % 2 === 0) {
        // return "Genap";
    	metadata.attr = 'style="background-color: #92c992; "';//font-size:15px;
    } else {
    	metadata.attr = 'style="background-color: #609b60; "';//font-size:15px;
        // return "Ganjil";
    }
	// console.log(record)
	// console.log(rowIndex)
	// console.log(colIndex)
    return value; // Return the original value for the cell
}

function dataHistorySATUSEHAT_viDaftar(ihs_number) {
	console.log('ihs' + ihs_number);
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionSATUSEHAT/GetEcounterSubject",
		params: {
			ihs_number: ihs_number
		},
		success: function (o) {
			loadMask.hide();
			var cst = Ext.decode(o.responseText);

			// if (cst != '' && cst.success == true) {
			// 	form_rujukan_multi_bpjs_RWJ(true);
				var res = cst;
				var recs = [],
					sendDataHistory = []
				
					// console.log(cst.entry);
					// console.log(cst.entry.entry.total);

				for (var i = 0; i < cst.length; i++) {
					console.log(res[i]);
					sendDataHistory.push(
						[res[i].class_code,
						res[i].class_display,
						res[i].id,
						res[i].identifier_value,
						res[i].location_display,
						res[i].location_reference,
						res[i].last_updated,
						res[i].version_id,
						res[i].participant_individual_display,
						res[i].participant_individual_reference,
						res[i].participant_type_coding_code,
						res[i].participant_type_coding_display,
						res[i].period_start,
						res[i].resource_type,
						res[i].service_provider_reference,
						res[i].status,
						res[i].status_history_period_start,
						res[i].status_history_status,
						res[i].subject_display,
						res[i].subject_reference
					]);
				}
				// console.log(sendDataHistory);
				dataSource_HistorySatusehat.removeAll();

				dataSource_HistorySatusehat.loadData(sendDataHistory);
				
			// } else {
			// 	loadMask.hide();
			// 	Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
			// 	//form_rujukan_multi_bpjs_RWJ.close();
			// }
		}, error: function (jqXHR, exception) {
			Ext.getCmp('txtNoAskes').setValue('Terjadi kesalahan dari server');
		}
	});
}

function setLookUp_PatientSATUSEHAT(rowdata) {
	var lebar = 450;
	setLookUps_PatientSATUSEHAT = new Ext.Window({
		id: Nci.getId(),
		title: 'Cari ID Patient dari SATUSEHAT',
		closeAction: 'destroy',
		width: 450,
		constrain: true,
		// height: 500,
		autoHeight: true,
		resizable: false,
		autoScroll: false,
		border: true,
		constrainHeader: true,
		iconCls: 'capsule',
		modal: true,
		items: getFormItemEntry_PatientSATUSEHAT(lebar, rowdata),
		listeners: {
			activate: function () {

			},
			afterShow: function () {
				this.activate();
			},
			deactivate: function () {
				rowSelected_PatientSATUSEHAT = undefined;
			},
			close: function () {
				shortcut.remove('lookup');
			},
		}
	});
	setLookUps_PatientSATUSEHAT.show();

	if (rowdata == undefined) {

	} else {
		datainit_PatientSATUSEHAT(rowdata);
	}
}

function getFormItemEntry_PatientSATUSEHAT(lebar, rowdata) {
	var pnlFormDataBasic_PatientSATUSEHAT = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items: [getItemPanelInputBiodata_PatientSATUSEHAT(lebar)],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: [{
					xtype: 'button',
					text: 'Add Kunjungan',
					iconCls: 'add',
					id: 'btnAdd_PatientSATUSEHAT',
					handler: function () {
						dataaddnew_PatientSATUSEHAT();
					}
				},

			]
		}
	})

	return pnlFormDataBasic_PatientSATUSEHAT;
}

function getItemPanelInputBiodata_PatientSATUSEHAT(lebar) {
	var items = {
		title: '',
		layout: 'column',
		//height:150,
		items: [{
				// columnWidth: .50,
				layout: 'form',
				labelWidth: 100,
				bodyStyle: 'padding: 10px',
				border: false,
				items: [{
						xtype: 'textfield',
						fieldLabel: 'IHS Number ',
						name: 'txtIHSNumber_PatientSATUSEHAT',
						id: 'txtIHSNumber_PatientSATUSEHAT',
						readOnly: true,
						tabIndex: 0,
						width: 100
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Pasien ',
						name: 'txtNamaPasien_PatientSATUSEHAT',
						id: 'txtNamaPasien_PatientSATUSEHAT',
						tabIndex: 1,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'NIK ',
						name: 'txtNIK_PatientSATUSEHAT',
						id: 'txtNIK_PatientSATUSEHAT',
						tabIndex: 5,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Tgl Lahir ',
						name: 'txtTglLahir_PatientSATUSEHAT',
						id: 'txtTglLahir_PatientSATUSEHAT',
						tabIndex: 1,
						// hidden: true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Bahasa',
						name: 'txttmpBahasa_PatientSATUSEHAT',
						id: 'txttmpBahasa_PatientSATUSEHAT',
						tabIndex: 1,
						// hidden: true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Jenis Kelamin',
						name: 'txtJenisKelamin_PatientSATUSEHAT',
						id: 'txtJenisKelamin_PatientSATUSEHAT',
						tabIndex: 1,
						// hidden: true,
						width: 180
					},
					{
						xtype : 'textfield',
						fieldLabel : 'Kota',
						id : 'txt_City',
						width : 100,
						// style: 'text-align: right'
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Alamat',
						name: 'txtAlamat_PatientSATUSEHAT',
						id: 'txtAlamat_PatientSATUSEHAT',
						tabIndex: 1,
						// hidden: true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Kewarganegaraan',
						name: 'txtKodePos_PatientSATUSEHAT',
						id: 'txtKodePos_PatientSATUSEHAT',
						tabIndex: 1,
						// hidden: true,
						width: 180
					},
					{
						xtype : 'checkbox',
						fieldLabel : 'Aktif',
						// boxLabel : 'Aktif',
						id : 'cbo_Aktif'
					},
					
				]
			},

			// {
			// 	columnWidth: .50,
			// 	layout: 'form',
			// 	labelWidth: 100,
			// 	bodyStyle: 'padding: 10px',
			// 	border: false,
			// 	items: [
			// 		{
			// 			xtype: 'numberfield',
			// 			fieldLabel: 'Fraction',
			// 			name: 'txtFraction_PatientSATUSEHAT',
			// 			id: 'txtFraction_PatientSATUSEHAT',
			// 			tabIndex: 9,
			// 			width: 180
			// 		},
			// 		{
			// 			xtype: 'textfield',
			// 			fieldLabel: 'Golongan ',
			// 			name: 'txttmpGolongan_PatientSATUSEHAT',
			// 			id: 'txttmpGolongan_PatientSATUSEHAT',
			// 			tabIndex: 1,
			// 			// hidden: true,
			// 			width: 180
			// 		},
			// 		{
			// 			xtype: 'textfield',
			// 			fieldLabel: 'Satuan Besar ',
			// 			name: 'txttmpSatbesar_PatientSATUSEHAT',
			// 			id: 'txttmpSatbesar_PatientSATUSEHAT',
			// 			tabIndex: 1,
			// 			// hidden: true,
			// 			width: 180
			// 		},
			// 		{
			// 			xtype: 'textfield',
			// 			fieldLabel: 'Satuan kecil ',
			// 			name: 'txttmpSatkecil_PatientSATUSEHAT',
			// 			id: 'txttmpSatkecil_PatientSATUSEHAT',
			// 			tabIndex: 1,
			// 			// hidden: true,
			// 			width: 180
			// 		},
			// 		{
			// 			xtype: 'textfield',
			// 			fieldLabel: 'Kode Pabrik ',
			// 			name: 'txttmpKdPabrik_PatientSATUSEHAT',
			// 			id: 'txttmpKdPabrik_PatientSATUSEHAT',
			// 			tabIndex: 1,
			// 			// hidden: true,
			// 			width: 180
			// 		},
			// 		{
			// 			xtype: 'textfield',
			// 			fieldLabel: 'Katagori ',
			// 			name: 'txttmpKdkatagori_PatientSATUSEHAT',
			// 			id: 'txttmpKdkatagori_PatientSATUSEHAT',
			// 			tabIndex: 1,
			// 			// hidden: true,
			// 			width: 180
			// 		}
			// 	]
			// }

		]
	};
	return items;
};

function datainit_PatientSATUSEHAT(rowdata) {
	console.log(rowdata.birthDate);


	Ext.getCmp('txtIHSNumber_PatientSATUSEHAT').setValue(rowdata.id);
	Ext.getCmp('txtNamaPasien_PatientSATUSEHAT').setValue(rowdata.name[0].text);
	Ext.getCmp('txtNIK_PatientSATUSEHAT').setValue(rowdata.identifier[1].value);
	Ext.getCmp('txtTglLahir_PatientSATUSEHAT').setValue(rowdata.birthDate);
	Ext.getCmp('txttmpBahasa_PatientSATUSEHAT').setValue(rowdata.communication[0].language.text);
	Ext.getCmp('txtJenisKelamin_PatientSATUSEHAT').setValue(rowdata.gender);
	Ext.getCmp('txt_City').setValue(rowdata.address[0].city);
	Ext.getCmp('txtAlamat_PatientSATUSEHAT').setValue(rowdata.address[0].line[0]);
	Ext.getCmp('txtKodePos_PatientSATUSEHAT').setValue(rowdata.extension[0].valueCode);
	Ext.getCmp('cbo_Aktif').setValue(rowdata.active);
	

};
//==================================================================

function printRWJKartuPasien() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: dataRwjcetakKartuPasien(),
		failure: function (o) {
			Ext.MessageBox.alert('Data tidak berhasil di Cetak ', 'Cetak Data');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				Ext.MessageBox.alert('Kartu Pasien Segera di Cetak', 'Cetak Data');
			} else if (cst.success === false && cst.pesan === 0) {
				Ext.MessageBox.alert('Data tidak berhasil di Cetak ' + cst.pesan, 'Cetak Data');
			} else {
				Ext.MessageBox.alert('Data tidak berhasil di Cetak ' + cst.pesan, 'Cetak Data');
			}
		}
	});
}
function dataRwjcetakKartuPasien() {
	var paramskartupasienpendaftaran = {
		Table: 'kartupasienprinting',
		NoMedrec: Ext.get('txtNoRequest').getValue(),
		NamaPasien: Ext.get('txtNama').getValue(),
		Alamat: Ext.get('txtAlamat').getValue(),
		Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
		TanggalMasuk: Ext.get('dptTanggal').getValue(),
		KdDokter: kodeDokterDefault//Ext.getCmp('cboDokterRequestEntry').getValue(),
	}
	return paramskartupasienpendaftaran
}
function getParamHitungUmur(tgl) {
	return tgl;
}
function getParamHitungUmurSQL(tgl) {
	return tgl;
}
function getParamBalikanHitungUmur(tgl) {
	console.log(typeof(tgl));
	var tglPisah=tgl.split("/");
	var tglAsli;
	console.log(tgl);
	console.log(tglPisah);
	if(tglPisah[0].length<2){
		tglPisah[0] = "0"+tglPisah[0];
	}
	console.log(tglPisah[0]);
	console.log(tglPisah[1]);
	console.log(tglPisah[2]);
	if (tglPisah[1]==="01" || tglPisah[1]==="1"){
		tglAsli=tglPisah[0]+"/"+"Jan"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="02" || tglPisah[1]==="2"){
		tglAsli=tglPisah[0]+"/"+"Feb"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="03" || tglPisah[1]==="3"){
		console.log('cekcek');
		tglAsli=tglPisah[0]+"/"+"Mar"+"/"+tglPisah[2];
		console.log(tglAsli);
	}else if (tglPisah[1]==="04" || tglPisah[1]==="4"){
		tglAsli=tglPisah[0]+"/"+"Apr"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="05" || tglPisah[1]==="5"){
		tglAsli=tglPisah[0]+"/"+"May"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="06" || tglPisah[1]==="6"){
		tglAsli=tglPisah[0]+"/"+"Jun"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="07" || tglPisah[1]==="7"){
		tglAsli=tglPisah[0]+"/"+"Jul"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="08" || tglPisah[1]==="8"){
		tglAsli=tglPisah[0]+"/"+"Aug"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="09" || tglPisah[1]==="9"){
		tglAsli=tglPisah[0]+"/"+"Sep"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="10"){
		tglAsli=tglPisah[0]+"/"+"Oct"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="11"){
		tglAsli=tglPisah[0]+"/"+"Nov"+"/"+tglPisah[2];
	}else if (tglPisah[1]==="12"){
		tglAsli=tglPisah[0]+"/"+"Dec"+"/"+tglPisah[2];
	}else{
		tglAsli=tgl;
	}
	console.log(tglAsli);
	Ext.getCmp('dtpTanggalLahir').setValue(tglAsli);
}
function getItemTxtNoreg_viDaftar() {
	var items = {
		columnWidth: .40,
		layout: 'form',
		labelWidth: 120,
		labelStyle: 'font-weight:bold;',
		width: 270,
		border: false,
		hidden: true,
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'No. Reg',
				labelStyle: 'font-weight:bold; font-size: 24px;',
				style: 'font-weight:bold; font-size: 20px;',
				name: 'txtNoRegRWJ',
				id: 'txtNoRegRWJ',
				width: 100,
				height: 30,
				emptyText: '',
				hidden: true,
				readOnly: true,
				anchor: '95%'
			}
		]
	}
	return items;
}
//##tambah validasi kunjungan untuk pasien yang akan generate SEP
function cekKunjunganPasienLaluCetakBpjs() {
	if (Ext.getCmp('txtNoRequest').getValue() == " " || Ext.getCmp('txtNoRequest').getValue() == "Automatic from the system..."
		|| Ext.getCmp('txtNoRequest').getValue() == "") {
		datasave_viDaftar(addNew_viDaftar, true, false);
	}
	else {
		Ext.Ajax.request({
			method: 'POST',
			url: baseURL + "index.php/rawat_jalan/functionRWJ/cekKunjunganPasien",
			params: dataparam_viDaftar(),
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success == true) {
					//	datasave_viDaftar(addNew_viDaftar,true);
					//_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(),undefined,Ext.getCmp('txtcatatan').getValue());
					var url = baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/true";
					new Ext.Window({
						title: 'SEP',
						width: 900,
						height: 600,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/false", "_blank");
								}
							}
						]
					}).show();
				} else {
					//_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(),undefined,Ext.getCmp('txtcatatan').getValue());
					var url = baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/true";
					new Ext.Window({
						title: 'SEP',
						width: 900,
						height: 600,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue() + "/false", "_blank");
								}
							}
						]
					}).show();
				}

			}
		});
	}

}
function getItemPanelInputBiodata_viDaftar() {
	var items = {
		layout: 'column',
		border: false,
		labelAlign: 'top',
		height: 220,
		items: [
			{
				columnWidth: .20,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpKecamatan',
						id: 'txtTmpKecamatan',
						emptyText: '',
						hidden: true,
						anchor: '95%'
					}, {
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpKabupaten',
						id: 'txtTmpKabupaten',
						emptyText: '',
						hidden: true,
						anchor: '95%'
					}, {
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpPropinsi',
						id: 'txtTmpPropinsi',
						emptyText: '',
						hidden: true,
						anchor: '95%'
					}, {
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpPendidikan',
						id: 'txtTmpPendidikan',
						emptyText: '',
						hidden: true,
						value: 1,
						anchor: '95%'
					}, {
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpPekerjaan',
						id: 'txtTmpPekerjaan',
						emptyText: '',
						hidden: true,
						value: 1,
						anchor: '95%'
					}, {
						xtype: 'textfield',
						fieldLabel: '',
						name: 'txtTmpAgama',
						id: 'txtTmpAgama',
						emptyText: '',
						hidden: true,
						anchor: '95%'
					}
				]
			}, {
				columnWidth: .10,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'No. Medrec ',
						name: 'txtNoRequest',
						id: 'txtNoRequest',
						emptyText: 'Automatic from the system...',
						readOnly: true,
						anchor: '99%'
					}
				]
			}, {
				columnWidth: .25,
				layout: 'form',
				border: false,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Nama ',
						name: 'txtNama',
						id: 'txtNama',
						tabIndex: 1,
						emptyText: ' ',
						disabled: false,
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
							},
							keypress: function (c, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('txtNamaKeluarga').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .17,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Keluarga ',
						name: 'txtNamaKeluarga',
						id: 'txtNamaKeluarga',
						tabIndex: 2,
						emptyText: ' ',
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
							},
							keypress: function (c, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('cboAgamaRequestEntry').focus();
									//Ext.getCmp('cboPoliklinikRequestEntry').focus(true,10);
								}
							},

							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .12,
				layout: 'form',
				border: false,
				labelWidth: 90,
				anchor: '95%',
				items: [mComboAgamaRequestEntry()]
			}, {
				columnWidth: .060,
				layout: 'form',
				border: false,
				labelWidth: 0,
				items: [mComboGolDarah_Daftar()]
			}, {
				columnWidth: .090,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboJK_Daftar()]
			}, {
				columnWidth: .10,
				layout: 'form',
				border: false,
				labelWidth: 90,
				anchor: '99%',
				items: [mComboSatusMarital_Daftar()]
			}, {
				columnWidth: .1,
				layout: 'form',
				border: false,
				labelWidth: 90,
				anchor: '95%',
				items: [
					{
						xtype: 'checkbox',
						boxLabel: 'WNA',
						name: 'cbWni',
						id: 'cbWni',
						tabIndex: 7
					}
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Tempat Lahir ',
						name: 'txtTempatLahir_Daftar',
						id: 'txtTempatLahir_Daftar',
						tabIndex: 7,
						emptyText: ' ',
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							keypress: function (c, e) {
								if (e.getKey() == 13) {
									Ext.getCmp('dtpTanggalLahir').focus();
								}
							},
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
							}
						}
					}
				]
			}, {
				columnWidth: .12,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Lahir ',
						name: 'dtpTanggalLahir',
						id: 'dtpTanggalLahir',
						tabIndex: 9,
						format: "d/M/Y",
						selectOnFocus: true,
						forceSelection: true,
						value: Today_Pendaftaran,
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) {
									var tmptanggal = Ext.get('dtpTanggalLahir').getValue();
									Ext.Ajax.request({
										url: baseURL + "index.php/main/GetUmur",
										params: {
											TanggalLahir: getParamHitungUmur(tmptanggal)
										},
										success: function (o) {
											var tmphasil = o.responseText;
											var tmp = tmphasil.split(' ');
											if (tmp.length == 6) {
												Ext.getCmp('txtThnLahir').setValue(tmp[0]);
												Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
												Ext.getCmp('txtHariLahir').setValue(tmp[4]);
												getParamBalikanHitungUmur(tmptanggal);
											} else if (tmp.length == 4) {
												if (tmp[1] == 'years' && tmp[3] == 'day') {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue(tmp[2]);
												} else if (tmp[1] == 'years' && tmp[3] == 'days') {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue(tmp[2]);
												} else if (tmp[1] == 'year' && tmp[3] == 'days') {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue(tmp[2]);
												} else {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
													Ext.getCmp('txtHariLahir').setValue('0');
												}
												getParamBalikanHitungUmur(tmptanggal);
											} else if (tmp.length == 2) {
												if (tmp[1] == 'year') {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue('0');
												} else if (tmp[1] == 'years') {
													Ext.getCmp('txtThnLahir').setValue(tmp[0]);
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue('0');
												} else if (tmp[1] == 'mon') {
													Ext.getCmp('txtThnLahir').setValue('0');
													Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
													Ext.getCmp('txtHariLahir').setValue('0');
												} else if (tmp[1] == 'mons') {
													Ext.getCmp('txtThnLahir').setValue('0');
													Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
													Ext.getCmp('txtHariLahir').setValue('0');
												} else {
													Ext.getCmp('txtThnLahir').setValue('0');
													Ext.getCmp('txtBlnLahir').setValue('0');
													Ext.getCmp('txtHariLahir').setValue(tmp[0]);
												}
												getParamBalikanHitungUmur(tmptanggal);
											} else if (tmp.length == 1) {
												Ext.getCmp('txtThnLahir').setValue('0');
												Ext.getCmp('txtBlnLahir').setValue('0');
												Ext.getCmp('txtHariLahir').setValue('1');
												getParamBalikanHitungUmur(tmptanggal);
											} else {
												alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
											}
										}
									});
								}
							},
							keypress: function (c, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('cboPendidikanRequestEntry').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .039,
				layout: 'form',
				border: false,
				labelWidth: 10,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Thn ',
						name: 'txtThnLahir',
						id: 'txtThnLahir',
						tabIndex: 10,
						emptyText: ' ',
						anchor: '99%',
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) {
									if (Ext.getCmp('txtThnLahir').getValue().length == 0) {
										var tmptahuns = 0;
										Ext.getCmp('txtThnLahir').setValue(0);
									} else {
										var tmptahuns = Ext.getCmp('txtThnLahir').getValue();
									}
									if (Ext.getCmp('txtBlnLahir').getValue().length == 0) {
										var tmpbulans = 0;
										Ext.getCmp('txtBlnLahir').setValue(0);
									} else {
										var tmpbulans = Ext.getCmp('txtBlnLahir').getValue();
									}
									if (Ext.getCmp('txtHariLahir').getValue().length == 0) {
										var tmpharis = 0;
										Ext.getCmp('txtHariLahir').setValue(0);
									} else {
										var tmpharis = Ext.getCmp('txtHariLahir').getValue();
									}
									Ext.Ajax.request({
										url: baseURL + "index.php/main/GetTglLahir",
										params: {
											tmptahun: tmptahuns,
											tmpbulan: tmpbulans,
											tmphari: tmpharis,
										},
										success: function (o) {
											var tmphasil = o.responseText;
											console.log(tmphasil);
											Ext.getCmp('dtpTanggalLahir').setValue(tmphasil);
										}
									});
								}
							},
							'render': function (c) {
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13)
										Ext.getCmp('txtNamaAyah').focus();
								}, c);
							}
						}
					}
				]
			}, {
				columnWidth: .037,
				layout: 'form',
				border: false,
				labelWidth: 10,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Bln ',
						name: 'txtBlnLahir',
						id: 'txtBlnLahir',
						tabIndex: 11,
						emptyText: ' ',
						anchor: '95%',
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) {
									if (Ext.getCmp('txtThnLahir').getValue().length == 0) {
										var tmptahuns = 0;
										Ext.getCmp('txtThnLahir').setValue(0);
									} else {
										var tmptahuns = Ext.getCmp('txtThnLahir').getValue();
									}
									if (Ext.getCmp('txtBlnLahir').getValue().length == 0) {
										var tmpbulans = 0;
										Ext.getCmp('txtBlnLahir').setValue(0);
									} else {
										var tmpbulans = Ext.getCmp('txtBlnLahir').getValue();
									}
									if (Ext.getCmp('txtHariLahir').getValue().length == 0) {
										var tmpharis = 0;
										Ext.getCmp('txtHariLahir').setValue(0);
									} else {
										var tmpharis = Ext.getCmp('txtHariLahir').getValue();
									}
									Ext.Ajax.request({
										url: baseURL + "index.php/main/GetTglLahir",
										params: {
											tmptahun: tmptahuns,
											tmpbulan: tmpbulans,
											tmphari: tmpharis,
										},
										success: function (o) {
											var tmphasil = o.responseText;
											Ext.getCmp('dtpTanggalLahir').setValue(tmphasil);
										}
									});
								}

							},
							'render': function (c) {
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13)
										Ext.getCmp('txtNamaAyah').focus();
								}, c);
							}
						}
					}
				]
			}, {
				columnWidth: .039,
				layout: 'form',
				border: false,
				labelWidth: 10,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Hari ',
						name: 'txtHariLahir',
						id: 'txtHariLahir',
						tabIndex: 12,
						emptyText: ' ',
						anchor: '95%',
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) {
									if (Ext.getCmp('txtThnLahir').getValue().length == 0) {
										var tmptahuns = 0;
										Ext.getCmp('txtThnLahir').setValue(0);
									} else {
										var tmptahuns = Ext.getCmp('txtThnLahir').getValue();
									}
									if (Ext.getCmp('txtBlnLahir').getValue().length == 0) {
										var tmpbulans = 0;
										Ext.getCmp('txtBlnLahir').setValue(0);
									} else {
										var tmpbulans = Ext.getCmp('txtBlnLahir').getValue();
									}
									if (Ext.getCmp('txtHariLahir').getValue().length == 0) {
										var tmpharis = 0;
										Ext.getCmp('txtHariLahir').setValue(0);
									} else {
										var tmpharis = Ext.getCmp('txtHariLahir').getValue();
									}
									Ext.Ajax.request({
										url: baseURL + "index.php/main/GetTglLahir",
										params: {
											tmptahun: tmptahuns,
											tmpbulan: tmpbulans,
											tmphari: tmpharis,
										},
										success: function (o) {
											var tmphasil = o.responseText;
											Ext.getCmp('dtpTanggalLahir').setValue(tmphasil);
										}

									});
								}
							},
							'render': function (c) {
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13)
										Ext.getCmp('cboPendidikanRequestEntry').focus();
								}, c);
							}
						}
					}
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboPendidikanRequestEntry()]
			}, {
				columnWidth: .17,
				layout: 'form',
				border: false,
				labelWidth: 100,
				items: [mComboPekerjaanRequestEntry()]
			}, {
				columnWidth: .25,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Alamat ',
						name: 'txtAlamat',
						id: 'txtAlamat',
						tabIndex: 17,
						emptyText: ' ',
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keypress: function (c, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('cboPropinsiRequestEntry').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
								Ext.getCmp('txtAlamatktp').setValue(a.getValue().toUpperCase());
							},
						}
					}
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboPropinsiRequestEntry()]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboKabupatenRequestEntry()]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboKecamatanRequestEntry()]
			}, {
				columnWidth: .13,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mCombokelurahan()]
			}, {
				columnWidth: .060,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Kd. Pos ',
						name: 'txtpos',
						id: 'txtpos',
						tabIndex: 22,
						emptyText: ' ',
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keypress: function (c, e) {
								if (e.keyCode == 13)
									Ext.getCmp('txttlpnPasien').focus();
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
								Ext.getCmp('txtposktp').setValue(a.getValue().toUpperCase());
							}
						}
					}
				]
			}, {
				columnWidth: .25,
				layout: 'form',
				border: false,
				hidden: true,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Alamat Ktp ',
						name: 'txtAlamatktp',
						id: 'txtAlamatktp',
						tabIndex: 23,
						emptyText: ' ',
						enableKeyEvents: true,
						anchor: '99%',
						listeners: {
							keypress: function (c, e) {
								if (e.keyCode == 13)
									Ext.getCmp('cboPropinsiKtp').focus(false);
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
							}
						}
					}
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				hidden: true,
				labelWidth: 90,
				items: [mComboPropinsiKtp()]
			}, {
				columnWidth: .15,
				layout: 'form',
				hidden: true,
				border: false,
				labelWidth: 90,
				items: [mComboKtpKabupaten()]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				hidden: true,
				labelWidth: 90,
				items: [mComboKecamatanktp()]
			}, {
				columnWidth: .13,
				layout: 'form',
				border: false,
				hidden: true,
				labelWidth: 90,
				items: [mComboKtpkelurahan()]
			}, {
				columnWidth: .060,
				layout: 'form',
				border: false,
				hidden: true,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Kd. Pos ',
						name: 'txtpos',
						id: 'txtposktp',
						tabIndex: 28,
						emptyText: ' ',
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {
								a.setValue(a.getValue().toUpperCase());
							},
							keypress: function (c, e) {
								if (e.keyCode == 13)
									Ext.getCmp('txttlpnPasien').focus();
							}
						}
					}
				]
			},
			{
				columnWidth: .12,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'datefield',
						fieldLabel: 'Tgl. Kunjungan ',
						name: 'dtpTanggalKunjungan',
						id: 'dtpTanggalKunjungan',
						tabIndex: 9,
						readOnly: false,
						format: "d/M/Y",
						value: Today_Pendaftaran,
						anchor: '99%'
					}
				]
			}, {
				columnWidth: .12,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'No Telepon',
						tabIndex: 9,
						name: 'txttlpnPasien',
						id: 'txttlpnPasien',
						maxLength: 30,
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {

								a.setValue(a.getValue().toUpperCase());
							},
							keypress: function (c, e) {
								if (e.keyCode == 13)
									Ext.getCmp('txtnikPasien').focus();
							}
						}
					},
				]
			}, {
				columnWidth: .12,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'NIK',
						tabIndex: 9,
						name: 'txtnikPasien',
						id: 'txtnikPasien',
						maxLength: 30,
						anchor: '99%',
						enableKeyEvents: true,
						listeners: {
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							blur: function (a) {

								a.setValue(a.getValue().toUpperCase());
							},
							keypress: function (c, e) {
								if (e.keyCode == 13)
									Ext.getCmp('txtNamaIbu').focus();
							}
						}
					},
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Ibu ',
						name: 'txtNamaIbu',
						id: 'txtNamaIbu',
						tabIndex: 14,
						emptyText: ' ',
						anchor: '99%',
						listeners: {
							'render': function (c) {
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13) Ext.getCmp('cboPoliklinikRequestEntry').focus();
								}, c);
							}
						}
					}
				]
			}, {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					mComboSukuRequestEntry(),
				]
			}, {
				columnWidth: .25,
				layout: 'column',
				height: 90,
				xtype: 'fieldset',
				title: 'SATUSEHAT',
				items: [
						{ xtype: "tbspacer", height: 10, width: 10 },
						{
							
							columnWidth: .50,
							layout: 'form',
							border: false,
							labelWidth: 90,
							items: [{
								x: 10,
								y: 10,
									xtype: 'textfield',
									fieldLabel: 'IHS Number ',
									name: 'txtIHSNumber',
									id: 'txtIHSNumber',
									tabIndex: 24,
									emptyText: ' ',
									anchor: '99%',
								},
							]
						},
						{ xtype: "tbspacer", height: 10, width: 10 },
						{
							columnWidth: .50,
							layout: 'form',
							border: false,
							labelWidth: 140,
							items: [
								{
									xtype: 'textfield',
									fieldLabel: 'Encounter ID ',
									name: 'txtEncounterID1',
									id: 'txtEncounterID1',
									tabIndex: 24,
									emptyText: ' ',
									labelWidth: 140,
									anchor: '99%',
								}
							]
						},
						{ xtype: "tbspacer", height: 10, width: 10 },
						]
					},
			/* , {
				columnWidth: .15,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
					}
				]
			} */
		]
	};
	return items;
}
var autohideandshowunitperawat;
function getItemDataKunjungan_viDaftar1() {
	var items = {
		layout: 'column',
		border: false,
		labelAlign: 'top',
		items: [
			{
				columnWidth: .15,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items: [
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						name: 'dtpTanggal',
						id: 'dptTanggal',
						format: 'd/M/Y',
						value: now_viDaftar,
						anchor: '95%',
						hidden: true
					}
				]
			}, {
				columnWidth: .10,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'textfield',
						id: 'txtJamKunjung',
						value: h + ':' + m + ':' + s,
						width: 120,
						anchor: '95%',
						hidden: true
					}
				]
			}, {
				columnWidth: 100,
				layout: 'form',
				border: false,
				labelWidth: 90,
				anchor: '100%',
				items: [{
					layout: 'column',
					border: false,
					width: 500,
					columns: 3,
					items: [{
						xtype: 'checkbox',
						boxLabel: 'Non Kunjungan ',
						hidden: true,
						name: 'cbnonkunjungan',
						id: 'cbnonkunjungan'
					}, {
						xtype: 'checkbox',
						boxLabel: 'Lakalantas ',
						name: 'cblakalantas',
						id: 'cblakalantas',
						listeners: {
							check: function (checkbox, isChecked) {
								if (isChecked === true) {
									formLookup_Lakalantas_RWJ();
								}
							}
						}
					},
					new Ext.form.RadioGroup({
						id: 'rb_induk_unit_rehabmedik',
						hidden: true,
						column: 3,
						items: [
							{
								id: 'rb_unit_Fisio',
								name: 'rb_unit_rehabMedik',
								boxLabel: 'Fisiotherapi',
								inputValue: "Fisiotherapi"
							}, {
								boxLabel: 'Terapi Wicara',
								id: 'rb_unit_TW',
								name: 'rb_unit_rehabMedik',
								inputValue: "Terapi Wicara"
							}, {
								boxLabel: 'Konsultasi dokter',
								id: 'rb_unit_Konsul',
								name: 'rb_unit_rehabMedik',
								inputValue: "Konsultasi Dokter"
							}
						],
						listeners: {
							change: function (a, b) {
								tmp_sub_unit_rehab = b.inputValue;
							}
						}
					}),
					]
				}]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				anchor: '95%',
				items: [
					// mComboJamKlinik(),
					mComboPoliklinik(),
					mComboDokterRequestEntry(),
					{
						xtype: 'tbtext',
						text: 'Jumlah Pasien yg Sudah Mendaftar : '
					}, 
					{
						xtype: 'textfield',
						label: 'Jumlah Antrian yg sudah dipanggil',
						id: 'txtJmlAntrianPanggilan',
						readOnly: true,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					}, 
				]
			}, {
				columnWidth: .30,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'combo',
						fieldLabel: 'Kelompok Pasien',
						id: 'kelPasien',
						editable: true,
						store: new Ext.data.ArrayStore({
							id: 0,
							fields: [
								'Id',
								'displayText'
							],
							data: [[0, 'Perseorangan'], [1, 'Perusahaan'], [2, 'Asuransi'], [3, 'Dinas Sosial']]
						}),
						valueField: 'Id',
						displayField: 'displayText',
						mode: 'local',
						width: 100,
						triggerAction: 'all',
						emptyText: 'Pilih Salah Satu...',
						selectOnFocus: true,
						tabIndex: 30,
						anchor: '99%',
						disabled: true,
						enableKeyEvents: true,
						listeners: {
							'select': function (a, b, c) {
								Combo_Select_prwj(b.data.displayText);
								if (b.data.displayText == 'Perseorangan') {
									Ext.getCmp('cboRujukanDariRequestEntry').show();
									Ext.getCmp('cboRujukanRequestEntry').show();
									jeniscus = '0';
									// RefreshDatacombo(jeniscus);
									selectSetAsuransi = '0000000001';
								} else if (b.data.displayText == 'Perusahaan') {
									Ext.getCmp('cboRujukanDariRequestEntry').show();
									Ext.getCmp('cboRujukanRequestEntry').show();
									jeniscus = '1';
									// RefreshDatacombo(jeniscus)
								} else if (b.data.displayText == 'Asuransi') {
									Ext.getCmp('cboRujukanDariRequestEntry').show();
									Ext.getCmp('cboRujukanRequestEntry').show();
									jeniscus = '2';
									// RefreshDatacombo(jeniscus)
								}
								console.log(jeniscus)
								RefreshDatacombo(jeniscus);
								// loadComboCustomer(jeniscus);
								// Ext.getCmp('cboCustomer').setValue("");
								Ext.getCmp('txtNoAskes').setValue(NoAsuransiPasien);
								kelompokpasien = b.data.id;
								// Ext.getCmp('cboAsuransi').focus(false,500);
							},
							keypress: function (c, e) {
								if (e.keyCode == 13 || e.keyCode == 9) {
									Ext.getCmp('cboCustomer').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							},
							'render': function (c) {
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13) //atau Ext.EventObject.ENTER
										Ext.getCmp('cboCustomer').focus();
								}, c);
							}
						}
					}
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboCustomer()]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboPerseorangan()]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboPerusahaan()]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [mComboAsuransi()]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 100,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Diagnosa',
						bodystyle: 'padding-top:100px;',
						maxLength: 200,
						name: 'txtDiagnosa_RWJ',
						id: 'txtDiagnosa_RWJ',
						width: 100,
						anchor: '95%',
						enableKeyEvents: true,
						listeners: {
							keydown: function (c, e) {
								if (e.keyCode == 13) {
									params = {
										text: Ext.getCmp('txtDiagnosa_RWJ').getValue(),
									};
									// loaddatastoreListDiagnosa(params);
									PilihDiagnosaLookUp_PendaftaranRWJ(params);
								}
							}
						}
						//disabled:true,
					},
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. Asuransi',
						maxLength: 200,
						name: 'txtNoAskes',
						id: 'txtNoAskes',
						width: 100,
						hidden: true,
						enableKeyEvents: true,
						anchor: '95%',
						tabIndex: 33,
						value: '',
						listeners: {
							specialkey: function (text, e) {
								str_tglkunjungan = Ext.getCmp('dtpTanggalKunjungan').getValue().format("Y-m-d");
								var dtpTanggalKunjungan = Ext.getCmp('dtpTanggalKunjungan').getValue();
								if (e.getKey() == 13) {
									Ext.Ajax.request({
										url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
										params: {
											select: " count(*) as count ",
											where: " kd_pasien = '" + Ext.getCmp('txtNoRequest').getValue() + "' AND kd_unit = '" + polipilihanpasien + "' AND tgl_masuk = '" + dtpTanggalKunjungan.format("Y-m-d") + "'",
											table: " kunjungan ",
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											if (cst[0].count > 0 || cst[0].count != '0') {
												Ext.Ajax.request({
													method: 'POST',
													url: baseURL + "index.php/rawat_jalan/functionRWJ/getSepRWI",
													params: {
														kd_pasien: Ext.getCmp('txtNoRequest').getValue(), 
														noka: Ext.getCmp('txtNoAskes').getValue(), 
														no_rujukan: Ext.getCmp('txtNoRujukan').getValue()
													},
													success: function (o) {
														var cst = Ext.decode(o.responseText);
														console.log(cst);
														if (cst != '' && cst.success === true) {
															sep_rwi = cst.sep_rwi;
															noSuratKontrol = cst.noSuratKontrol;
															rwi_kode_poli = cst.kode_poli;
															tmp_poli_tujuan_kunj2 = cst.kode_poli;
															tmp_poli_tujuan = rwi_kode_poli;
															rwi_nama_poli = cst.nama_poli;
															rwi_kode_dokter = cst.kode_dokter;
															rwi_nama_dokter = cst.nama_dokter;
															rwi_diag_awal = cst.diag_awal.trim();
															console.log(sep_rwi);
															if (cst != '' && cst.sep_rwi !== '') {
																Ext.MessageBox.confirm('BPJS', "Konsultasi Pasca Rawat Inap ?", function (btn) {
																	if (btn === 'yes') {
																		btn_pascaRWI = true;
																		tmp_cari_rujukan = true;
																		if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
																			// Ext.Ajax.request({
																			// 	method: 'POST',
																			// 	url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi",
																			// 	params: {
																			// 		no_kartu: Ext.getCmp('txtNoAskes').getValue()
																			// 	},
																			// 	success: function (o) {
																			// 		loadMask.hide();
																			// 		var cst = Ext.decode(o.responseText);
																			// 		console.log(cst);
																		
																					// if (cst != '' && cst.success === true) {
																					// 	form_rujukan_multi_bpjs_RWJ(true);
																					// 	var res = cst;
																					// 	console.log(res);
																					// 	var recs = [],

																					// 		sendDataArrayRujukanByNoka = []

																					// 	for (var i = 0; i < cst.totalrecords; i++) {
																					// 		sendDataArrayRujukanByNoka.push(
																					// 			[cst.listData[i].noKunjungan,
																					// 			cst.listData[i].peserta.noKartu,
																					// 			cst.listData[i].peserta.nama,
																					// 			cst.listData[i].tglKunjungan,
																					// 			cst.listData[i].provPerujuk.nama,
																					// 			cst.listData[i].poliRujukan.nama]);

																					// 	}

																					// 	dataSourceRujukanByNoka.removeAll();
																					// 	dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					// } else {
																					// 	loadMask.hide();
																					// 	console.log(cst);
																					// 	Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																					Ext.Ajax.request({
																						method: 'POST',
																						url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjsDenganSEP",
																						params: {
																							no_sep: sep_rwi
																						},
																						success: function (o) {
																							loadMask.hide();
																							formLookup_rujukan_bpjs_RWJ();
																							// generate_surat_kontrol(true);
																							var cst_rwi = Ext.decode(o.responseText);
																							console.log(cst);
																							if (cst_rwi.data.metaData.code == '200') {
																								
																								Ext.Ajax.request({
																									method: 'POST',
																									url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
																									params: {
																										no_rujukan: sep_rwi,
																										kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
																										noka: Ext.getCmp('txtNoAskes').getValue(),
																										kd_unit: polipilihanpasien,
																										sep_rwi : sep_rwi
																									},
																									failure: function (o) {
																										ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																									},
																									success: function (o) {
																										var cst = Ext.decode(o.responseText);
																										kunjungan_pertama = cst.kunjungan_pertama;
																											if (cst.kode_poli != null | cst.kode_poli != '') {
																												// Ext.getCmp('txt_poli').setValue(cst.kode_poli);																								
																											}
																											// rwi_kode_poli = cst.kode_poli;
																											tmp_poli_tujuan = rwi_kode_poli;
																											// rwi_nama_poli = cst.nama_poli;
																											// rwi_kode_dokter = cst.kode_dokter;
																											// rwi_nama_dokter = cst.nama_dokter;
																											// rwi_diag_awal = cst.diag_awal;
																											// tmp_diagnosa = rwi_diag_awal;
																											// rwi_kd_prov = cst.kd_prov;
																											// rwi_kd_kab = cst.kd_kab;
																											// rwi_kd_kec = cst.kd_kec;
																											// rwi_no_tlp = cst.no_tlp;
																											// rwi_kd_dpjp = cst.kd_dpjp;
																											loaddatastoreDokterPJBPJS('RWJ', rwi_kode_poli);

																										if (cst.kunjungan_pertama == false) {
																											generate_surat_kontrol(false);
																											loaddatastoreUnitBPJS();
																											console.log("rwi1"+rwi_kode_poli);
																											console.log('false');
																											get_dokter_pertama(cst_rwi.data.response.noRujukan, Ext.getCmp('txtNoRequest').getValue());
																											Ext.getCmp('txt_poli').enable();
																											// Ext.getCmp('txt_no_surat_kontrol').enable();
																											// Ext.getCmp('cboDokterPJBPJSKunj2').hide();
																											Ext.Ajax.request({
																												method: 'POST',
																												url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
																												params: {
																													select: " * ",
																													where: " kd_unit = '" + polipilihanpasien + "' ",
																													table: " map_unit_bpjs ",
																												},
																												failure: function (o) {
																													ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																												},
																												success: function (o) {
																													var cst = Ext.decode(o.responseText);
																													tmp_poli_tujuan = rwi_kode_poli;
																													tmp_poli_tujuan_kunj2 = rwi_kode_poli;
																													loaddatastoreDokterPJBPJS('RWJ', rwi_kode_poli);
																													// loaddatastoreDokterPJBPJSKunj2(cst[0].unit_bpjs);

																													Ext.Ajax.request({
																														method: 'POST',
																														url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
																														params: {
																															select: " * ",
																															where: " kd_unit = '" + polipilihanpasien + "' ",
																															table: " unit ",
																														},
																														failure: function (o) {
																															ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																														},
																														success: function (o) {
																															var cst = Ext.decode(o.responseText);
																															// Ext.getCmp('txt_poli').setValue(cst[0].nama_unit);
																														}
																													});
																												}
																											});
																										}
																										else {
																											generate_surat_kontrol(true);
																											console.log("rwi"+rwi_kode_poli);
																											Ext.Ajax.request({
																												method: 'POST',
																												url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
																												params: {
																													kd_poli_bpjs: rwi_kode_poli
																												},
																												failure: function (o) {
																													ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																												},
																												success: function (o) {
																													var cst = Ext.decode(o.responseText);
																													if (cst.success === true) {
																														console.log(cst.listData);
																														Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
																														polipilihanpasien = cst.listData[0].kd_unit;
																														tmp_poli_tujuan_kunj2 = cst.listData[0].unit_bpjs;
																														tmp_poli_tujuan = cst.listData[0].unit_bpjs;
																													}
																													else {
																														ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
																													}
																												}
																											});
																											// console.log(Ext.getCmp('cboDokterRequestEntry'));
																											// Ext.getCmp('txt_poli').setValue(rwi_nama_poli);																								
																											//Ext.getCmp('txt_poli').disable();
																											selectTujuanKunjSEP = 0;
																											Ext.getCmp('cboTujuanKunj').setValue('Normal');
																											Ext.getCmp('cboTujuanKunj').disable();
																										}
																									}

																								});

																								var res = cst_rwi;
																								Ext.getCmp('chk_rujukan_rs').setValue(true);
																								console.log(rwi_kode_poli);
																								Ext.getCmp('txt_no_surat_kontrol').setValue(noSuratKontrol);
																								Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
																								Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.diagnosa);
																								Ext.getCmp('txtNoRujukan').setValue(sep_rwi);
																								Ext.getCmp('txtNamaPeserta').setValue(res.data.response.peserta.nama)
																								tmp_ppk_rujukan = res.data.response.noRujukan.substr(0, 8);
																								tmp_kelas_rawat=res.data.response.peserta.hakKelas;
																								tmp_kelas_rawat = 3;
																								tmp_diagnosa = rwi_diag_awal;
																								kd_dokter_dpjp_insert = rwi_kode_dokter;
																								tmp_poli_tujuan = rwi_kode_poli;
																								tmp_tgl_rujukan = res.data.response.tglSep;
																								Ext.getCmp('tgl_rujukan').setValue(res.data.response.tglSep);
																								Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.peserta.nama);
																								Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.peserta.noKartu);
																								Ext.getCmp('cboDokterPJBPJSKunj2').setValue(rwi_nama_dokter)
																								Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.peserta.hakKelas);
																								Ext.getCmp('txt_perujuk').setValue(res.data.response.noRujukan.substr(0, 8));
																								console.log(rwi_nama_poli);
																								Ext.getCmp('txt_poli').setValue(rwi_nama_poli);
																								Ext.getCmp('txt_no_rujukan').setValue(sep_rwi);
																								Ext.getCmp('txt_kd_diagnosa').setValue(rwi_kode_poli);
																								Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.diagnosa);
																								Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
																								response_bpjs = cst_rwi.data.response;
																								// loaddatastoreUnitBPJS();
																								// Ext.Ajax.request({
																								// 	method: 'POST',
																								// 	url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
																								// 	params: {
																								// 		kd_prov_rujukan: 0
																								// 	},
																								// 	failure: function (o) {
																								// 		ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
																								// 	},
																								// 	success: function (o) {
																								// 		var cst = Ext.decode(o.responseText);
																								// 		if (cst.success === true) {
																								// 			console.log(cst);
																								// 			Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
																								// 			Ext.getCmp('cboRujukanDariRequestEntry').setValue(cst.listData[0].cara_penerimaan);
																								// 			koderujuk = cst.listData[0].kd_rujukan;
																								// 		}
																								// 		else {
																								// 			ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
																								// 		}
																								// 	}

																								// });

																							}else{
																								ShowPesanWarning_viDaftar('SEP tidak ditemukan', 'Warning');
																							}
																					// }
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																				}
																			});
																			// }
																			// }
																		// });

																		}else if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi_rs",
																				params: {
																					no_kartu: Ext.getCmp('txtNoAskes').getValue()
																				},
																				success: function (o) {
																					loadMask.hide();
																					var cst = Ext.decode(o.responseText);
																					console.log(cst);

																					if (cst != '' && cst.success == true) {
																						form_rujukan_multi_bpjs_RWJ(true);
																						var res = cst;
																						var recs = [],
																							sendDataArrayRujukanByNoka = []

																						for (var i = 0; i < cst.totalrecords; i++) {
																							sendDataArrayRujukanByNoka.push(
																								[cst.listData.noKunjungan,
																								cst.listData.peserta.noKartu,
																								cst.listData.peserta.nama,
																								cst.listData.tglKunjungan,
																								cst.listData.provPerujuk.nama,
																								cst.listData.poliRujukan.nama]);
																						}
																						dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					} else {
																						loadMask.hide();
																						Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																						//form_rujukan_multi_bpjs_RWJ.close();
																					}
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('txtNoAskes').setValue('Terjadi kesalahan dari server');
																				}
																			});

																		} else {
																			ShowPesanWarning_viDaftar('No Asuransi Belum Diisi', 'No Rujukan');
																		}
																		
																	} else {
																		btn_pascaRWI = false;
																		sep_rwi = '';
																		Ext.MessageBox.confirm('Pencarian', "Cari Rujukan  ?", function (btn) {
																			if (btn === 'yes') {
																				tmp_cari_rujukan = true;
																				if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
																					Ext.Ajax.request({
																						method: 'POST',
																						url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi",
																						params: {
																							no_kartu: Ext.getCmp('txtNoAskes').getValue()
																						},
																						success: function (o) {
																							loadMask.hide();
																							var cst = Ext.decode(o.responseText);
																							console.log(cst);
																							if (cst != '' && cst.success === true) {
																								form_rujukan_multi_bpjs_RWJ(true);
																								var res = cst;
																								console.log(res);
																								var recs = [],

																									sendDataArrayRujukanByNoka = []

																								for (var i = 0; i < cst.totalrecords; i++) {
																									sendDataArrayRujukanByNoka.push(
																										[cst.listData[i].noKunjungan,
																										cst.listData[i].peserta.noKartu,
																										cst.listData[i].peserta.nama,
																										cst.listData[i].tglKunjungan,
																										cst.listData[i].provPerujuk.nama,
																										cst.listData[i].poliRujukan.nama]);

																								}

																								dataSourceRujukanByNoka.removeAll();
																								dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																							} else {
																								loadMask.hide();
																								console.log(cst);
																								Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																							}
																						}, error: function (jqXHR, exception) {
																							Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																						}
																					});

																				}
																				else if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
																					Ext.Ajax.request({
																						method: 'POST',
																						url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi_rs",
																						params: {
																							no_kartu: Ext.getCmp('txtNoAskes').getValue()
																						},
																						success: function (o) {
																							loadMask.hide();
																							var cst = Ext.decode(o.responseText);
																							console.log(cst);

																							if (cst != '' && cst.success == true) {
																								form_rujukan_multi_bpjs_RWJ(true);
																								var res = cst;
																								var recs = [],
																									sendDataArrayRujukanByNoka = []

																								for (var i = 0; i < cst.totalrecords; i++) {
																									sendDataArrayRujukanByNoka.push(
																										[cst.listData.noKunjungan,
																										cst.listData.peserta.noKartu,
																										cst.listData.peserta.nama,
																										cst.listData.tglKunjungan,
																										cst.listData.provPerujuk.nama,
																										cst.listData.poliRujukan.nama]);
																								}
																								dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																							} else {
																								loadMask.hide();
																								Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																								//form_rujukan_multi_bpjs_RWJ.close();
																							}
																						}, error: function (jqXHR, exception) {
																							Ext.getCmp('txtNoAskes').setValue('Terjadi kesalahan dari server');
																						}
																					});

																				} else {
																					ShowPesanWarning_viDaftar('No Asuransi Belum Diisi', 'No Rujukan');
																				}
																			} else {
																				tmp_cari_rujukan = false;
																				Ext.Ajax.request({
																					method: 'POST',
																					url: baseURL + "index.php/rawat_jalan/functionRWJ/get_rujukan_history",
																					params: {
																						no_kartu: Ext.getCmp('txtNoAskes').getValue()
																					},
																					success: function (o) {
																						loadMask.hide();
																						var cst = Ext.decode(o.responseText);
																						if (cst != '' && cst.success === true) {
																							form_rujukan_multi_bpjs_RWJ(false);
																							var res = cst;
																							var recs = [],

																								sendDataArrayRujukanByNoka = []

																							for (var i = 0; i < cst.totalrecords; i++) {
																								sendDataArrayRujukanByNoka.push(
																									[cst.listData[i].noKunjungan,
																									cst.listData[i].peserta.noKartu,
																									cst.listData[i].peserta.nama,
																									cst.listData[i].tglKunjungan,
																									cst.listData[i].provPerujuk.nama,
																									cst.listData[i].poliRujukan.nama]);

																							}

																							dataSourceRujukanByNoka.removeAll();
																							dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);
																							console.log(dataSourceRujukanByNoka);

																						} else {
																							loadMask.hide();
																							console.log(cst);
																							Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																							//	form_rujukan_multi_bpjs_RWJ.close();
																						}
																					}, error: function (jqXHR, exception) {
																						Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																					}
																				});
																			}
																		});
																	}

																});
															} else {
                                								btn_pascaRWI = false;
																sep_rwi = '';
																// validasi finger print 1
																console.log('ckckck');
																console.log(polipilihanpasien);
																Ext.MessageBox.confirm('Pencarian', "Cari Rujukan  ?", function (btn) {
																	if (btn === 'yes') {
																		tmp_cari_rujukan = true;
																		if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi",
																				params: {
																					no_kartu: Ext.getCmp('txtNoAskes').getValue()
																				},
																				success: function (o) {
																					loadMask.hide();
																					var cst = Ext.decode(o.responseText);
																					console.log(cst);
																					if (cst != '' && cst.success === true) {
																						form_rujukan_multi_bpjs_RWJ(true);
																						var res = cst;
																						console.log(res);
																						var recs = [],

																							sendDataArrayRujukanByNoka = []

																						for (var i = 0; i < cst.totalrecords; i++) {
																							sendDataArrayRujukanByNoka.push(
																								[cst.listData[i].noKunjungan,
																								cst.listData[i].peserta.noKartu,
																								cst.listData[i].peserta.nama,
																								cst.listData[i].tglKunjungan,
																								cst.listData[i].provPerujuk.nama,
																								cst.listData[i].poliRujukan.nama]);

																						}

																						dataSourceRujukanByNoka.removeAll();
																						dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					} else {
																						loadMask.hide();
																						console.log(cst);
																						Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																					}
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																				}
																			});

																		}
																		else if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi_rs",
																				params: {
																					no_kartu: Ext.getCmp('txtNoAskes').getValue()
																				},
																				success: function (o) {
																					loadMask.hide();
																					var cst = Ext.decode(o.responseText);
																					console.log(cst);

																					if (cst != '' && cst.success == true) {
																						form_rujukan_multi_bpjs_RWJ(true);
																						var res = cst;
																						var recs = [],
																							sendDataArrayRujukanByNoka = []

																						for (var i = 0; i < cst.totalrecords; i++) {
																							sendDataArrayRujukanByNoka.push(
																								[cst.listData.noKunjungan,
																								cst.listData.peserta.noKartu,
																								cst.listData.peserta.nama,
																								cst.listData.tglKunjungan,
																								cst.listData.provPerujuk.nama,
																								cst.listData.poliRujukan.nama]);
																						}
																						dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					} else {
																						loadMask.hide();
																						Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																						//form_rujukan_multi_bpjs_RWJ.close();
																					}
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('txtNoAskes').setValue('Terjadi kesalahan dari server');
																				}
																			});

																		} else {
																			ShowPesanWarning_viDaftar('No Asuransi Belum Diisi', 'No Rujukan');
																		}
																	} else {
																		tmp_cari_rujukan = false;
																		Ext.Ajax.request({
																			method: 'POST',
																			url: baseURL + "index.php/rawat_jalan/functionRWJ/get_rujukan_history",
																			params: {
																				no_kartu: Ext.getCmp('txtNoAskes').getValue()
																			},
																			success: function (o) {
																				loadMask.hide();
																				var cst = Ext.decode(o.responseText);
																				if (cst != '' && cst.success === true) {
																					form_rujukan_multi_bpjs_RWJ(false);
																					var res = cst;
																					var recs = [],

																						sendDataArrayRujukanByNoka = []

																					for (var i = 0; i < cst.totalrecords; i++) {
																						sendDataArrayRujukanByNoka.push(
																							[cst.listData[i].noKunjungan,
																							cst.listData[i].peserta.noKartu,
																							cst.listData[i].peserta.nama,
																							cst.listData[i].tglKunjungan,
																							cst.listData[i].provPerujuk.nama,
																							cst.listData[i].poliRujukan.nama]);

																					}

																					dataSourceRujukanByNoka.removeAll();
																					dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);
																					console.log(dataSourceRujukanByNoka);

																				} else {
																					loadMask.hide();
																					console.log(cst);
																					Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																					//	form_rujukan_multi_bpjs_RWJ.close();
																				}
																			}, error: function (jqXHR, exception) {
																				Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																			}
																		});
																	}
																});
															}
														} else {
															// cek validasi finger 2
															console.log('ekwksk');
															console.log(polipilihanpasien);
															Ext.Ajax.request({
															url: baseURL + "index.php/rawat_jalan/functionRWJ/getValidasiFinger",
															params: {
																	noka : Ext.getCmp('txtNoAskes').getValue(), 
																	poli_tujuan: polipilihanpasien, 
																	tgl_sep:Ext.getCmp('dtpTanggalKunjungan').getValue() 
																},
															failure: function(o){
																var cst = Ext.decode(o.responseText);
															},	    
															success: function(o) {
																var cst = Ext.decode(o.responseText);
																console.log(cst.validasi);
																if(cst.pesan != '' && cst.validasi == false) {
																	// pesan_finger = cst.pesan;
																	ShowPesanWarning_viDaftar(cst.pesan, 'Warning');
																	// validasi_finger = "gagal";
																}else{
																	Ext.MessageBox.confirm('Pencarian', "Cari Rujukan  ?", function (btn) {
																	if (btn === 'yes') {
																		tmp_cari_rujukan = true;
																		if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi",
																				params: {
																					no_kartu: Ext.getCmp('txtNoAskes').getValue()
																				},
																				success: function (o) {
																					loadMask.hide();
																					var cst = Ext.decode(o.responseText);
																					console.log(cst);
																					if (cst != '' && cst.success === true) {
																						form_rujukan_multi_bpjs_RWJ(true);
																						var res = cst;
																						console.log(res);
																						var recs = [],

																							sendDataArrayRujukanByNoka = []

																						for (var i = 0; i < cst.totalrecords; i++) {
																							sendDataArrayRujukanByNoka.push(
																								[cst.listData[i].noKunjungan,
																								cst.listData[i].peserta.noKartu,
																								cst.listData[i].peserta.nama,
																								cst.listData[i].tglKunjungan,
																								cst.listData[i].provPerujuk.nama,
																								cst.listData[i].poliRujukan.nama]);

																						}

																						dataSourceRujukanByNoka.removeAll();
																						dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					} else {
																						loadMask.hide();
																						console.log(cst);
																						Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																					}
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																				}
																			});

																		}
																		else if (Ext.getCmp('txtNoAskes').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_nokartu_multi_rs",
																				params: {
																					no_kartu: Ext.getCmp('txtNoAskes').getValue()
																				},
																				success: function (o) {
																					loadMask.hide();
																					var cst = Ext.decode(o.responseText);
																					console.log(cst);

																					if (cst != '' && cst.success == true) {
																						form_rujukan_multi_bpjs_RWJ(true);
																						var res = cst;
																						var recs = [],
																							sendDataArrayRujukanByNoka = []
																						for (var i = 0; i < cst.totalrecords; i++) {
																							console.log(cst);
																							sendDataArrayRujukanByNoka.push(
																								[
																									cst.listData.noKunjungan,
																									cst.listData.peserta.noKartu,
																									cst.listData.peserta.nama,
																									cst.listData.tglKunjungan,
																									cst.listData.provPerujuk.nama,
																									cst.listData.poliRujukan.nama
																									/* cst.listData.noKunjungan,
																									cst.listData.peserta.noKartu,
																									cst.listData.peserta.nama,
																									cst.listData.tglKunjungan,
																									cst.listData.provPerujuk.nama,
																									cst.listData.poliRujukan.nama */
																								]
																							);
																						}
																						dataSourceRujukanByNoka.removeAll();
																						dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);

																					} else {
																						loadMask.hide();
																						Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																						//form_rujukan_multi_bpjs_RWJ.close();
																					}
																				}, error: function (jqXHR, exception) {
																					Ext.getCmp('txtNoAskes').setValue('Terjadi kesalahan dari server');
																				}
																			});

																		} else {
																			ShowPesanWarning_viDaftar('No Asuransi Belum Diisi', 'No Rujukan');
																		}
																	} else {
																		tmp_cari_rujukan = false;
																		Ext.Ajax.request({
																			method: 'POST',
																			url: baseURL + "index.php/rawat_jalan/functionRWJ/get_rujukan_history",
																			params: {
																				no_kartu: Ext.getCmp('txtNoAskes').getValue()
																			},
																			success: function (o) {
																				loadMask.hide();
																				var cst = Ext.decode(o.responseText);
																				if (cst != '' && cst.success === true) {
																					form_rujukan_multi_bpjs_RWJ(false);
																					var res = cst;
																					var recs = [],

																						sendDataArrayRujukanByNoka = []

																					for (var i = 0; i < cst.totalrecords; i++) {
																						sendDataArrayRujukanByNoka.push(
																							[cst.listData[i].noKunjungan,
																							cst.listData[i].peserta.noKartu,
																							cst.listData[i].peserta.nama,
																							cst.listData[i].tglKunjungan,
																							cst.listData[i].provPerujuk.nama,
																							cst.listData[i].poliRujukan.nama]);

																					}

																					dataSourceRujukanByNoka.removeAll();
																					dataSourceRujukanByNoka.loadData(sendDataArrayRujukanByNoka);
																					console.log(dataSourceRujukanByNoka);

																				} else {
																					loadMask.hide();
																					console.log(cst);
																					Ext.MessageBox.alert('Gagal', cst.listData.metaData.message);
																					//	form_rujukan_multi_bpjs_RWJ.close();
																				}
																			}, error: function (jqXHR, exception) {
																				Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
																			}
																		});
																	}
																});
																}
																// return cst;
															}
															});
														}
													}
												});
											} else {
												ShowPesanInfo_viDaftar('Pasien belum berkunjung ke rumah sakit', 'Informasi');
											}
										}
									});
								}

								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					}, {
						xtype: 'textfield',
						fieldLabel: 'Nama Peserta',
						maxLength: 200,
						name: 'txtNamaPeserta',
						id: 'txtNamaPeserta',
						width: 100,
						hidden: true,
						enableKeyEvents: true,
						anchor: '95%',
						tabIndex: 32,
						listeners: {
							keypress: function (text, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('txtNoSJP').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. SEP',
						maxLength: 200,
						name: 'txtNoSJP',
						id: 'txtNoSJP',
						width: 100,
						hidden: true,
						anchor: '95%',
						// disabled:true,
						enableKeyEvents: true,
						tabIndex: 33,
						listeners: {
							keyup: function (text, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('txtNama').focus();
								}
							},
							keydown: function (text, e) {
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					}, {
						xtype: 'textfield',
						fieldLabel: 'Catatan',
						maxLength: 200,
						name: 'txtcatatan',
						id: 'txtcatatan',
						width: 100,
						//hidden: true,
						enableKeyEvents: true,
						anchor: '95%',
						tabIndex: 33,
						listeners: {
							keydown: function (text, e) {
								if (e.keyCode == 13) {
									//Ext.getCmp('txtNama').focus();
								}
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			}, {
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No NIK',
						maxLength: 200,
						name: 'txtnik',
						id: 'txtnik',
						width: 100,
						hidden: true,
						enableKeyEvents: true,
						anchor: '95%',
						tabIndex: 33,
						listeners: {
							keydown: function (text, e) {
								if (e.keyCode == 13) {
									Ext.getCmp('txtNama').focus();
								}
								var nav = navigator.platform.match("Mac");
								if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
									e.preventDefault();
									Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
								} else if (e.keyCode == 117) {
									e.preventDefault();
									cekKunjunganPasienLaluCetakBpjs();
								}
							}
						}
					}
				]
			},
			{
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'label',
						text: ' '
					},
					{
						xtype: 'textfield',
						fieldLabel: 'No. Rujukan',
						maxLength: 200,
						name: 'txtNoRujukan',
						id: 'txtNoRujukan',
						width: 100,
						hidden: false,
						enableKeyEvents: true,
						anchor: '95%',
						tabIndex: 33,
						value: '',
						listeners: {
							keydown: function (text, e) {
								if (e.getKey() == 13) {
									loadMask.show();
									console.log(Ext.getCmp('txtNoRujukan').getValue());
									if (Ext.getCmp('txtNoRujukan').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_norujukan",
											params: {
												no_rujukan: Ext.getCmp('txtNoRujukan').getValue()
											},
											success: function (o) {
												loadMask.hide();
												formLookup_rujukan_bpjs_RWJ();
												var cst = Ext.decode(o.responseText);
												console.log(cst);
												if (cst.data.metaData.code == '200') {
													if (cst.data.response.rujukan.peserta.statusPeserta.kode != 0) {
														ShowPesanWarning_viDaftar("Status Kartu Tidak Aktif Atau Dalam PENANGGUHAN PESERTA", 'Generate SEP')
													} else {


														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
															params: {
																no_rujukan: Ext.getCmp('txtNoRujukan').getValue(),
																kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
																noka: Ext.getCmp('txtNoAskes').getValue(),
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																console.log(cst.kunjungan_pertama);
																kunjungan_pertama = cst.kunjungan_pertama;
																console.log("cek1");
																if (cst.kunjungan_pertama == false) {
																	generate_surat_kontrol(false);
																	loaddatastoreUnitBPJS();
																	console.log('bukan');
																	get_dokter_pertama(Ext.getCmp('txtNoRujukan').getValue(), Ext.getCmp('txtNoRequest').getValue());
																	Ext.getCmp('txt_poli').enable();
																	Ext.getCmp('txt_no_surat_kontrol').enable();
																	Ext.Ajax.request({
																		method: 'POST',
																		url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
																		params: {
																			select: " * ",
																			where: " kd_unit = '" + polipilihanpasien + "' ",
																			table: " map_unit_bpjs ",
																		},
																		failure: function (o) {
																			ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																		},
																		success: function (o) {
																			var cst = Ext.decode(o.responseText);
																			tmp_poli_tujuan = cst[0].unit_bpjs;
																			loaddatastoreDokterPJBPJS2('RWJ', cst[0].unit_bpjs);
																			// loaddatastoreDokterPJBPJSKunj2(cst[0].unit_bpjs);

																			Ext.Ajax.request({
																				method: 'POST',
																				url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
																				params: {
																					select: " * ",
																					where: " kd_unit = '" + polipilihanpasien + "' ",
																					table: " unit ",
																				},
																				failure: function (o) {
																					ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																				},
																				success: function (o) {
																					var cst = Ext.decode(o.responseText);
																					Ext.getCmp('txt_poli').setValue(cst[0].nama_unit);
																				}
																			});
																		}
																	});
																}
																else if (cst.kunjungan_pertama == true) {
																	generate_surat_kontrol(true);
																	Ext.Ajax.request({
																		method: 'POST',
																		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
																		params: {
																			kd_poli_bpjs: res.data.response.rujukan.poliRujukan.kode
																		},
																		failure: function (o) {
																			ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																		},
																		success: function (o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success === true) {
																				console.log(cst.listData[0].nama_unit);
																				Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
																				polipilihanpasien = cst.listData[0].kd_unit;
																			}
																			else {
																				ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
																			}
																		}
																	});

																	// console.log(Ext.getCmp('cboDokterRequestEntry'));
																	Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
																	Ext.getCmp('txt_poli').disable();
																	selectTujuanKunjSEP = 0;
																	Ext.getCmp('cboTujuanKunj').setValue('Normal');
																	Ext.getCmp('cboTujuanKunj').disable();
																}
															}

														});
														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
															params: {
																kd_poli_bpjs: cst.data.response.rujukan.poliRujukan.kode
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) {
																	console.log(cst.listData[0].nama_unit);
																	Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
																	polipilihanpasien_bpjs = cst.listData[0].unit_bpjs;
																	console.log(polipilihanpasien);
																}
																else {
																	ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
																}
															}

														});

														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
															params: {
																kd_prov_rujukan: cst.data.response.rujukan.provPerujuk.kode
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) {
																	console.log(cst);
																	Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
																	koderujuk = cst.listData[0].kd_rujukan;
																}
																else {
																	ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
																}
															}

														});


														var res = cst;
														Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
														Ext.getCmp('txtNoAskes').setValue(res.data.response.rujukan.peserta.noKartu);
														Ext.getCmp('txtNamaPeserta').setValue(res.data.response.rujukan.peserta.nama);
														tmp_ppk_rujukan = res.data.response.rujukan.provPerujuk.kode;
														tmp_kelas_rawat = res.data.response.rujukan.peserta.hakKelas.kode;
														tmp_diagnosa = res.data.response.rujukan.diagnosa.kode;
														tmp_poli_tujuan = res.data.response.rujukan.poliRujukan.kode;
														tmp_tgl_rujukan = res.data.response.rujukan.tglKunjungan;
														loaddatastoreDokterPJBPJS(tmp_poli_tujuan, cst.data.response.rujukan.poliRujukan.kode);
														if (res.data.response.rujukan.peserta.cob.noAsuransi === '') {
															Ext.getCmp('cbo_peserta_cob').checked(false);
														} else {
															if (res.data.response.rujukan.peserta.mr.noTelepon == '' || res.data.response.rujukan.peserta.mr.noTelepon == null) {
																Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
															} else {
																Ext.getCmp('txt_no_tlp').setValue(res.data.response.rujukan.peserta.mr.noTelepon);
															}
														}
														Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
														Ext.getCmp('tgl_rujukan').setValue(res.data.response.rujukan.tglKunjungan);
														Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.rujukan.peserta.nama);
														Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.rujukan.peserta.noKartu);
														Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.rujukan.peserta.hakKelas.keterangan)
														Ext.getCmp('txt_perujuk').setValue(res.data.response.rujukan.provPerujuk.nama);
														Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
														Ext.getCmp('txt_no_rujukan').setValue(res.data.response.rujukan.noKunjungan);
														Ext.getCmp('txt_kd_diagnosa').setValue(res.data.response.rujukan.diagnosa.kode);
														Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.rujukan.diagnosa.nama);

														Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
														response_bpjs = cst.data.response;



													}

												} else {
													loadMask.hide();
													console.log(cst.data.metaData.code);
													Ext.MessageBox.alert('Gagal', cst.data.metaData.message);
													formLookups_rujukan_bpjs.close();
												}
											}, error: function (jqXHR, exception) {
												Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
											}
										});

									}
									else if (Ext.getCmp('txtNoRujukan').getValue() != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_norujukan_RS",
											params: {
												no_rujukan: Ext.getCmp('txtNoRujukan').getValue()
											},
											success: function (o) {
												loadMask.hide();
												formLookup_rujukan_bpjs_RWJ();
												var cst = Ext.decode(o.responseText);
												console.log(cst);
												if (cst.data.metaData.code == '200') {
													if (cst.data.response.rujukan.peserta.statusPeserta.kode != 0) {
														ShowPesanWarning_viDaftar("Status Kartu Tidak Aktif Atau Dalam PENANGGUHAN PESERTA", 'Generate SEP')
													} else {

														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
															params: {
																kd_prov_rujukan: cst.data.response.rujukan.provPerujuk.kode
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) {
																	console.log(cst);
																	Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
																	koderujuk = cst.listData[0].kd_rujukan;
																}
																else {
																	ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
																}
															}

														});
														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
															params: {
																no_rujukan: Ext.getCmp('txtNoRujukan').getValue(),
																kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
																noka: Ext.getCmp('txtNoAskes').getValue(),
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																console.log(cst.kunjungan_pertama);
																kunjungan_pertama = cst.kunjungan_pertama;
																console.log("cek2");
																if (cst.kunjungan_pertama == false) {
																	generate_surat_kontrol(false);
																	loaddatastoreUnitBPJS();
																	console.log('bukan');
																	get_dokter_pertama(Ext.getCmp('txtNoRujukan').getValue(), Ext.getCmp('txtNoRequest').getValue());
																	Ext.getCmp('txt_poli').enable();
																	Ext.getCmp('txt_no_surat_kontrol').enable();

																}
																else if (cst.kunjungan_pertama == true) {
																	generate_surat_kontrol(true);
																	console.log('iya');
																	Ext.getCmp('txt_poli').disable();
																	selectTujuanKunjSEP = 0;
																	Ext.getCmp('cboTujuanKunj').setValue('Normal');
																	Ext.getCmp('cboTujuanKunj').disable();
																}
															}

														});
														Ext.Ajax.request({
															method: 'POST',
															url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
															params: {
																kd_poli_bpjs: cst.data.response.rujukan.poliRujukan.kode
															},
															failure: function (o) {
																ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
															},
															success: function (o) {
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) {
																	console.log(cst.listData[0].nama_unit);
																	Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
																	polipilihanpasien_bpjs = cst.listData[0].unit_bpjs;
																	console.log(polipilihanpasien);
																}
																else {
																	ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
																}
															}

														});




														var res = cst;
														Ext.getCmp('txtNoAskes').setValue(res.data.response.rujukan.peserta.noKartu);
														Ext.getCmp('txtNamaPeserta').setValue(res.data.response.rujukan.peserta.nama);
														tmp_ppk_rujukan = res.data.response.rujukan.provPerujuk.kode;
														tmp_kelas_rawat = res.data.response.rujukan.peserta.hakKelas.kode;
														tmp_diagnosa = res.data.response.rujukan.diagnosa.kode;
														tmp_poli_tujuan = res.data.response.rujukan.poliRujukan.kode;
														tmp_tgl_rujukan = res.data.response.rujukan.tglKunjungan;
														loaddatastoreDokterPJBPJS(tmp_poli_tujuan, cst.data.response.rujukan.poliRujukan.kode);
														if (res.data.response.rujukan.peserta.cob.noAsuransi === '') {
															Ext.getCmp('cbo_peserta_cob').checked(false);
														} else {
															if (res.data.response.rujukan.peserta.mr.noTelepon == '' || res.data.response.rujukan.peserta.mr.noTelepon == null) {
																Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
															} else {
																Ext.getCmp('txt_no_tlp').setValue(res.data.response.rujukan.peserta.mr.noTelepon);
															}
														}
														Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
														Ext.getCmp('tgl_rujukan').setValue(res.data.response.rujukan.tglKunjungan);
														Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.rujukan.peserta.nama);
														Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.rujukan.peserta.noKartu);
														Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.rujukan.peserta.hakKelas.keterangan)
														Ext.getCmp('txt_perujuk').setValue(res.data.response.rujukan.provPerujuk.nama);
														Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
														Ext.getCmp('txt_no_rujukan').setValue(res.data.response.rujukan.noKunjungan);
														Ext.getCmp('txt_kd_diagnosa').setValue(res.data.response.rujukan.diagnosa.kode);
														Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.rujukan.diagnosa.nama);

														Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
														response_bpjs = cst.data.response;



													}

												} else {
													loadMask.hide();
													console.log(cst.data.metaData.code);
													Ext.MessageBox.alert('Gagal', cst.data.metaData.message);
													formLookups_rujukan_bpjs.close();
												}
											}, error: function (jqXHR, exception) {
												Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
											}
										});
									}
									else {
										ShowPesanWarning_viDaftar('No Rujukan Belum Diisi', 'Pendaftaran RWJ');
									}

								}

							}
						}
					}
				]
			},
			{
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth: 90,
				items: [
					{
						xtype: 'checkbox',
						boxLabel: 'Rujukan Rumah Sakit (FKTL)',
						name: 'chk_rujukan_rs',
						id: 'chk_rujukan_rs',
						listeners: {
							check: function (checkbox, isChecked) {
								if (isChecked === true) {
									is_rujukan_rs = 'true';
									asal_rujukan = '2';
								} else {
									is_rujukan_rs = 'false';
									asal_rujukan = '1';
								}
							}
						}
					},

				]
			},
		]
	};
	return items;
}
function RefreshDatacombo(jeniscus) {
	var criteria = "";
	if (jeniscus.length > 0) {
		criteria = 'jenis_cust=~' + jeniscus + '~';
	} else {
		criteria = 'jenis_cust=~0~';
	}
	// var Field = ['KD_CUSTOMER', 'CUSTOMER'];
	// ds_customer_viDaftar = new WebApp.DataStore({ fields: Field });
	var Field = ['KD_CUSTOMER', 'CUSTOMER'];
	dsComboCustomer = new WebApp.DataStore({ fields: Field });
	dsComboCustomer.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'customer',
			Sortdir: 'ASC',
			target: 'ViewComboKontrakCustomer',
			param: criteria
		}
	})
	console.log(dsComboCustomer);

	return dsComboCustomer;
}
function getPenelItemDataKunjungan_viDaftar_prwj(lebar) {
	var items = {
		xtype: 'tabpanel',
		plain: true,
		flex: 1,
		activeTab: 0,
		// height: 370,
		defaults: {
			bodyStyle: 'padding:4px',
			autoScroll: true
		},
		items: [
			DataPanel1(),
			DataPanel4(),
			DataPanel3(),
			DataPanel5(),
			DataPanel6(),
			DataPanel7()
		],
		listeners: {
			tabchange: function (tabPanel, tab) {
				if (tab.title == "History Kunjungan") {
					getLookupDataHistoryKunjungan_PendaftaranRWJ(Ext.getCmp('txtNoRequest').getValue());
					// console.log("Cek");
				}
			}
		}
	}
	return items;
}
function DataPanel7() {
	var FormDepanHasilLab = new Ext.Panel({
		id: 'FormDepanHasilLab',
		layout: 'fit',
		title: 'History Kunjungan',
		border: false,
		items: [
			HistoryKunjunganPasienLookup_PendaftaranRWJ()
		]
	});
	return FormDepanHasilLab
}

function HistoryKunjunganPasienLookup_PendaftaranRWJ() {
	var FieldMedrec_viDaftar = [
		'KD_PASIEN', 'URUT', 'KD_UNIT', 'POLI', 'TGL', 'DOKTER', 'TGL_KELUAR'
	];
	LookupDataSourceHistoryKunjungan_PendaftaranRWJ = new WebApp.DataStore({ fields: FieldMedrec_viDaftar });
	LookupGrdDataHistoryKunjungan_PendaftaranRWJ = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		title: '',
		store: LookupDataSourceHistoryKunjungan_PendaftaranRWJ,
		id: 'gridhostorykunjunganpasienpendaftaranrwj',
		autoScroll: true,
		columnLines: true,
		border: true,
		trackMouseOver: true,
		height: 300,
		plugins: [new Ext.ux.grid.FilterRow()],
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					/* rowSelected_viDaftarHistory = LookupDataSourceHistoryKunjungan_PendaftaranRWJ.getAt(row);
					CurrentHistorydatapasienLookupPendaftaranRWJ.row = row;
					CurrentHistorydatapasienLookupPendaftaranRWJ.data = rowSelected_viDaftarHistory; */

				}
			}
		}),
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				id: 'col_KdPasienHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Kd Pasien',
				dataIndex: 'KD_PASIEN',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_KdUnitHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Kd Unit',
				dataIndex: 'KD_UNIT',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_UnitHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Urut',
				dataIndex: 'URUT',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				hidden: true,
				width: 50
			}, {
				id: 'col_UnitHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Unit',
				dataIndex: 'POLI',
				width: 300,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_TglHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Tanggal Kunjungan',
				dataIndex: 'TGL',
				width: 200,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_DokterHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Dokter',
				dataIndex: 'DOKTER',
				width: 150,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}, {
				id: 'col_TglKeluarHistoryKunjuganLookUp_PendaftaranRWJ',
				header: 'Tanggal Keluar',
				dataIndex: 'TGL_KELUAR',
				width: 200,
				hideable: false,
				menuDisabled: true,
				sortable: true
			}
		]),
		viewConfig: {
			forceFit: true
		},
	}
	);
	return LookupGrdDataHistoryKunjungan_PendaftaranRWJ;
}
function DataPanel6() {
	dataSourceHistoryPenyakit = new Ext.data.ArrayStore({
		fields: ['kd_penyakit', 'penyakit', 'tgl_masuk', 'stat_diag']
	});
	var gridListHistoryPenyakit = new Ext.grid.GridPanel({
		store: dataSourceHistoryPenyakit,
		id: 'rwj.riwayat.penyakit',
		cm: new Ext.grid.ColumnModel([
			{
				//xtype:'datecolumn',
				header: 'Tanggal Masuk',
				dataIndex: 'tgl_masuk',
				sortable: false,
				//format:'d M Y',
				width: 100
			}, {
				header: 'Kode Penyakit',
				dataIndex: 'kd_penyakit',
				sortable: false,
				width: 100
			}, {
				header: 'Penyakit',
				dataIndex: 'penyakit',
				sortable: false,
				flex: 1,
			}, {
				header: 'Status Diagnosa',
				dataIndex: 'stat_diag',
				sortable: false,
				width: 100
			}
		]),
		viewConfig: {
			forceFit: true
		},
	});
	return {
		title: 'Riwayat Penyakit',
		layout: 'fit',
		items: [
			gridListHistoryPenyakit
		]
	};
}
function getPenelItemDataKunjungan_viDataBPJS_prwj(lebar) {
	var items = {
		xtype: 'tabpanel',
		plain: true,
		activeTab: 0,
		height: 400,
		width: '100%',
		defaults: {
			bodyStyle: 'padding:10px',
			autoScroll: true
		},
		items: [
			DataPanel1DataBPJS(),
			DataPanel2DataBPJS(),
			DataPanel3DataBPJS(),
			DataPanel4DataBPJS(),
			DataPanel5DataBPJS(),
			DataPanel6DataBPJS(),
			DataPanel7DataBPJS(),
		]
	}
	return items;
}
function subdatapanel1_rwj() {
	var items = {
		// xtype: 'fieldset',
		// title: 'Anamnese & Alergi',
		layout: 'form',
		bodyStyle: 'padding:4px;',
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'Anamnese ',
				name: 'txtAnamnese_prwj',
				id: 'txtAnamnese_prwj',
				emptyText: ' ',
				tabIndex: 33,
				anchor: '100%',
				enableKeyEvents: true,
				listeners: {
					keyDown: function (text, e) {
						if (e.keyCode == 13 || e.keyCode == 9) {
							Ext.getCmp('txtAlergi_prwj').focus();
						}
						var nav = navigator.platform.match("Mac");
						if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
							e.preventDefault();
							Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
						} else if (e.keyCode == 117) {
							e.preventDefault();
							cekKunjunganPasienLaluCetakBpjs();
						}
					}
				}
			}, {
				xtype: 'textfield',
				fieldLabel: 'Alergi ',
				name: 'txtAlergi_prwj',
				id: 'txtAlergi_prwj',
				emptyText: ' ',
				tabIndex: 34,
				enableKeyEvents: true,
				anchor: '100%',
				listeners: {
					keyDown: function (text, e) {
						if (e.keyCode == 13 || e.keyCode == 9) {
							Ext.getCmp('cboPoliklinikRequestEntry').focus();
							Ext.getCmp('cboRujukanDariRequestEntry').focus();
						}
						var nav = navigator.platform.match("Mac");
						if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
							e.preventDefault();
							Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
						} else if (e.keyCode == 117) {
							e.preventDefault();
							cekKunjunganPasienLaluCetakBpjs();
						}
					}
				}
			}
		]
	};
	return items;
}
function DataPanel1() {
	var items = {
		title: 'Kunjungan',
		layout: 'form',
		tabIndex: 19,
		items: [
			subdatapanel1_rwj(),
			DataPanel2(),
			getItemDataKunjungan_viDaftar1(),
		]
	};
	return items;
}
function cariPesertaBPJSKepesertaanRWJ() {
	if (Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').getValue().trim() === '') {
		Ext.MessageBox.alert('Perhatian', 'No. BPJS harap diisi !');
	} else {
		Ext.Ajax.request({
			method: 'POST',
			url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs",
			params: {
				klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
				kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
				no_kartu: Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').getValue()
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.data != null) {
					if (cst.data.metaData.code == '200') {
						tmp_data_approval_sep = cst.data.response;
						var txt = _bpjs.generateObjectToString(cst.data.response);
						Ext.getCmp('textAreaKepesertaan').setValue(txt);
						status_kepesertaanPasienBPJS = cst.data.response.peserta.statusPeserta.keterangan;
						dataKepesertaanBPJS = cst.data;
						tmp_sex = cst.data.response.peserta.sex;
					} else {
						var txt = _bpjs.generateObjectToString(cst.data);
						Ext.getCmp('textAreaKepesertaan').setValue(txt);
					}
				} else {
					if (cst.message != undefined) {
						Ext.getCmp('textAreaKepesertaan').setValue(cst.message);
					}
				}
			}, error: function (jqXHR, exception) {
				Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
			}
		});
	}
}

function cariPesertaNoSEPKepesertaanRWJ() {
	if (Ext.getCmp('txtNoSEPKepesertaanDataBPJS').getValue().trim() === '') {
		Ext.MessageBox.alert('Perhatian', 'No. SEP harap diisi !');
	} else {
		Ext.Ajax.request({
			method: 'POST',
			url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjsDenganSEP",
			params: {
				klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
				kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
				no_sep: Ext.getCmp('txtNoSEPKepesertaanDataBPJS').getValue()
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.data != null) {
					var txt = _bpjs.generateObjectToString(cst.data.response);
					Ext.getCmp('textAreaKepesertaan').setValue(txt);
				} else {
					if (cst.message != undefined) {
						Ext.getCmp('textAreaKepesertaan').setValue(cst.message);
					}
				}
			}, error: function (jqXHR, exception) {
				Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
			}
		});
	}
}

function cariPesertaNOKTPKepesertaanRWJ() {
	if (Ext.getCmp('txtNIKKepesertaanDataBPJS').getValue().trim() === '') {
		Ext.MessageBox.alert('Perhatian', 'NIK / No KTP harap diisi !');
	} else {
		Ext.Ajax.request({
			method: 'POST',
			url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjsDenganNIK",
			params: {
				klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
				kd_unit: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
				tgl_masuk: Ext.getCmp('dtpTanggalKunjungan').getValue(),
				no_kartu: Ext.getCmp('txtNIKKepesertaanDataBPJS').getValue()
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.data != null) {
					var txt = _bpjs.generateObjectToString(cst.data.response);
					Ext.getCmp('textAreaKepesertaan').setValue(txt);
					tmp_data_approval_sep = cst.data.response;
					status_kepesertaanPasienBPJS = cst.data.response.peserta.statusPeserta.keterangan;
					dataKepesertaanBPJS = cst.data;
					tmp_sex = cst.data.response.peserta.sex;
				} else {
					if (cst.message != undefined) {
						Ext.getCmp('textAreaKepesertaan').setValue(cst.message);
					}
				}
			}, error: function (jqXHR, exception) {
				Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
			}
		});
	}
}
function DataPanel1DataBPJS() {
	radios1 = new Ext.form.RadioGroup({
		id: 'radios1',
		column: 3,
		items: [
			{
				id: 'rb_no_bpjs_kepesertaan',
				name: 'DataKepesertaan',
				boxLabel: 'No. BPJS',
				checked: true,
				inputValue: "bpjs"
			}, {
				boxLabel: 'No. SEP',
				id: 'rb_no_sep_kepesertaan',
				name: 'DataKepesertaan',
				inputValue: "nosep"
			}, {
				boxLabel: 'NIK / No. KTP',
				id: 'rb_nik_kepesertaan',
				name: 'DataKepesertaan',
				inputValue: "noktp"
			}
		],
		listeners: {
			change: function () {
				var kepesertaan1;
				kepesertaan1 = radios1.getValue().inputValue;
				if (kepesertaan1 === 'bpjs') {
					Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').show();
					Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').focus();
					Ext.getCmp('txtNoSEPKepesertaanDataBPJS').hide();
					Ext.getCmp('txtNIKKepesertaanDataBPJS').hide();
				} else if (kepesertaan1 === 'nosep') {
					Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').hide();
					Ext.getCmp('txtNoSEPKepesertaanDataBPJS').show();
					Ext.getCmp('txtNoSEPKepesertaanDataBPJS').focus();
					Ext.getCmp('txtNIKKepesertaanDataBPJS').hide();
				} else if (kepesertaan1 === 'noktp') {
					Ext.getCmp('txtNoBPJSKepesertaanDataBPJS').hide();
					Ext.getCmp('txtNoSEPKepesertaanDataBPJS').hide();
					Ext.getCmp('txtNIKKepesertaanDataBPJS').show();
					Ext.getCmp('txtNIKKepesertaanDataBPJS').focus();
				}
			}
		}
	});
	var items = {
		title: 'Kepesertaan',
		layout: 'form',
		tabIndex: 19,
		items: [
			radios1,
			{
				xtype: 'textfield',
				fieldLabel: 'No. BPJS ',
				tabIndex: 37,
				name: 'txtNoBPJSKepesertaanDataBPJS',
				id: 'txtNoBPJSKepesertaanDataBPJS',
				width: 250,
				hidden: false,
				listeners: {
					'render': function (c) {
						c.getEl().on('dblclick', function (e) {
							c.setValue(Ext.getCmp('txtNoAskes').getValue());
						}, c);
						c.getEl().on('keypress', function (e) {
							if (e.getKey() == 13)
								cariPesertaBPJSKepesertaanRWJ();
						}, c);
					}
				}
			}, {
				xtype: 'textfield',
				fieldLabel: 'No. SEP ',
				tabIndex: 38,
				name: 'txtNoSEPKepesertaanDataBPJS',
				id: 'txtNoSEPKepesertaanDataBPJS',
				width: 250,
				hidden: true
			}, {
				xtype: 'textfield',
				fieldLabel: 'NIK / No. KTP ',
				tabIndex: 39,
				width: 250,
				name: 'txtNIKKepesertaanDataBPJS',
				id: 'txtNIKKepesertaanDataBPJS',
				hidden: true
			}, {
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				id: 'btnCariKepesertaan',
				handler: function () {
					if (radios1.getValue().inputValue === 'bpjs') {
						cariPesertaBPJSKepesertaanRWJ();
					} else if (radios1.getValue().inputValue === 'nosep') {
						cariPesertaNoSEPKepesertaanRWJ();
					} else if (radios1.getValue().inputValue === 'noktp') {
						cariPesertaNOKTPKepesertaanRWJ();
					}
				}
			}, {
				xtype: 'textarea',
				width: '100%',
				height: 230,
				name: 'textAreaKepesertaan',
				id: 'textAreaKepesertaan',
				readOnly: true
			}
		],
		buttons:
		[
			{
				text: "Pengajuan SEP",
				id: 'approval_sep',
				handler: function () {
					LookUpAprovalSEP();
					//formLookups_rujukan_bpjs.close();	
				}
			},
			{
				text: "Approval FingerPrint",
				id:'approval_finger_act',
				handler: function()
				{
					LookUpAprovalFinger();
				}
			},
			{
				xtype: 'button',
				text: 'Pembuatan SPRI',
				id: 'btnInsertSPRI',
				handler: function () {
				console.log(status_kepesertaanPasienBPJS);
				console.log(dataKepesertaanBPJS);
				if(status_kepesertaanPasienBPJS == 'AKTIF'){
					LookUpPembuatanSPRI();
				}else{
					ShowPesanError_viDaftar('Status Kepesertaan ' + status_kepesertaanPasienBPJS, 'Pembuatan SPRI');
				}
				}
			},	
		]
	};
	return items;
}
//Vclaim 2.0 // hani 16-02-2022 START
function LookUpPembuatanSPRI() {
	// console.log(tmp_data_approval_sep);

	var lebar = 690;
	formLookups_insert_spri = new Ext.Window({
		id: 'LookUpPembuatanSPRI',
		name: 'LookUpPembuatanSPRI',
		title: 'Pembuatan SPRI',
		closeAction: 'destroy',
		width: lebar,
		height: 250, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: [
			panel_form_lookup_insert_spri()], //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		},
		fbar: [
			{
				xtype: 'button',
				text: "Simpan SPRI",
				id: 'btn_simpan_spri',
				hidden: false,
				handler: function () {
					ajax_insert_rencana_kontrol();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Update SPRI",
				id: 'btn_update_spri',
				hidden: false,
				handler: function () {
					ajax_update_rencana_kontrol();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Hapus SPRI",
				id: 'btn_hapus_spri',
				hidden: false,
				handler: function () {
					Ext.MessageBox.confirm('Hapus SPRI', "Yakin Akan Hapus SPRI '" + Ext.getCmp('txt_no_kontrol_insert_spri').getValue() + "'  ?", function (btn) {
						if (btn === 'yes') {
							var param = {
								no_kontrol: Ext.getCmp('txt_no_kontrol_insert_spri').getValue()
							}
							$.ajax({
								type: 'POST',
								dataType: 'JSON',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rencana_kontrol/",
								data: param,
								success: function (cst) {
									if (cst.metaData.code == "200") {
										Ext.MessageBox.alert('Sukses', 'SPRI berhasil dihapus');
										formLookups_insert_spri.close();

									} else {
										Ext.MessageBox.alert('Gagal', cst.metaData.message);
									}
								},
								error: function (jqXHR, exception) {
									loadMask.hide();
									Ext.MessageBox.alert('Gagal', 'Delete SPRI gagal.');
									//dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
									Ext.Ajax.request({
										url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
										params: {
											respon: jqXHR.responseText,
											keterangan: 'Error hapus SPRI'
										}
									});
								}
							});
						}
					});
				}
			},
			{
				xtype: 'button',
				text: "Cetak SPRI",
				id: 'btn_cetak_spri',
				hidden: true,
				handler: function () {
					var jenis = "SPRI";
					var no_surat = Ext.getCmp('txt_no_kontrol_insert_spri').getValue();
					console.log(no_surat);
					console.log(jenis);
					var url = baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + noka_buat_surat + "/" + no_surat + "/" + jenis + "/true";
					new Ext.Window({
						title: jenis,
						width: 900,
						height: 500,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + noka_buat_surat + "/" + no_surat + "/" + jenis + "/false", "_blank");
								}
							}
						]
					}).show();
					// formLookups_insert_spri.close();
				}
			}
		]
	});
	formLookups_insert_spri.show();
}

function panel_form_lookup_insert_spri() {
	// loaddatastoreDokterKontrol();
	// console.log(rowdata);
	noka_buat_surat =  tmp_data_approval_sep.peserta.noKartu;
	sep_buat_surat = '';
	tgl_kontrol = now_viDaftar;
	jenis_kontrol_insert_rencana_kontrol = 1;

	var formPanelInsertSPRI = new Ext.form.FormPanel({
		id: "myformpanel_insert_spri",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .5,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 200,
		//anchor: '100% 100%',
		items: [
				{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'No Surat SPRI'
			},
			{
				x: 100,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_kontrol_insert_spri',
				name: 'txt_no_kontrol_insert_spri',
				width: 220,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'Tgl Kontrol'
			},
			{
				x: 100,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 30,
				xtype: 'datefield',
				id: 'tgl_insert_spri',
				name: 'tgl_insert_spri',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,
			},
			{
				x: 320,
				y: 30,
				xtype: 'label',
				text: 'Tgl Surat'
			},
			{
				x: 410,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 420,
				y: 30,
				id: 'tgl_surat_insert_spri',
				name: 'tgl_surat_insert_spri',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 220,

			},

			{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'No Kartu'
			},
			{
				x: 100,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 55,
				xtype: 'textfield',
				id: 'txt_no_kartu_insert_spri',
				name: 'txt_no_kartu_insert_spri',
				width: 170,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 280,
				y: 55,
				xtype: 'textfield',
				id: 'txt_sex_insert_spri',
				name: 'txt_sex_insert_spri',
				width: 30,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Nama'
			},
			{
				x: 100,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 80,
				xtype: 'textfield',
				id: 'txt_nama_peserta_insert_spri',
				name: 'txt_nama_peserta_insert_spri',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 320,
				y: 105,
				xtype: 'label',
				text: 'Unit/Poli'
			},
			{
				x: 410,
				y: 105,
				xtype: 'label',
				text: ':'
			},
			mCombPoliKontrol(),
			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Dokter'
			},
			{
				x: 100,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			mCombDokterKontrol(),
		],

	});

	// if (rowdata != undefined) {
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_history_rencana_kontrol",
			params: { 
          noka: tmp_data_approval_sep.peserta.noKartu,
          jenis_kontrol: 1
         },
			failure: function (o) {
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				console.log(cst);
				if (cst.sudah_ada == true) {
					Ext.getCmp('btn_simpan_spri').hide();
					Ext.getCmp('btn_update_spri').show();
					Ext.getCmp('btn_hapus_spri').show();
					Ext.getCmp('btn_cetak_spri').show();
					Ext.getCmp('txt_no_kartu_insert_spri').setValue(tmp_data_approval_sep.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_spri').setValue(tmp_data_approval_sep.peserta.nama);
					Ext.getCmp('txt_no_kontrol_insert_spri').setValue(cst.listData.noSuratKontrol);
					Ext.getCmp('txt_sex_insert_spri').setValue(tmp_data_approval_sep.peserta.sex);
					Ext.getCmp('tgl_insert_spri').setValue(cst.listData.tglRencanaKontrol);
					Ext.getCmp('tgl_surat_insert_spri').setValue(cst.listData.tglTerbitKontrol).disable();
					jenis_kontrol_insert_rencana_kontrol = cst.listData.jnsKontrol;
					select_poliKontrol = cst.listData.poliTujuan;
					select_namapoliKontrol = cst.listData.namaPoliTujuan;
					select_dokterKontrol = cst.listData.kodeDokter;
					select_nama_dokterKontrol = cst.listData.namaDokter;
					loaddatastoreDokterKontrol(select_poliKontrol);
					Ext.getCmp('cboPoliKontrol_ugd').setValue(select_namapoliKontrol);
					Ext.getCmp('cboDokterKontrol_ugd').setValue(select_nama_dokterKontrol);
					if (jenis_kontrol_insert_rencana_kontrol == 1) {
						// Ext.getCmp('combo_kontrol_insert_rencanan_kontrol_igd').setValue('SPRI').disable();
						// loaddatastorePoliKontrol();
						Ext.getCmp('btn_update_spri').setText('Update SPRI');
						Ext.getCmp('btn_hapus_spri').setText('Hapus SPRI');
						Ext.getCmp('btn_cetak_spri').setText('Cetak SPRI');
					}
					// loaddatastorePoliKontrol();

				} else {
					Ext.getCmp('btn_update_spri').hide();
					Ext.getCmp('btn_hapus_spri').hide();
					Ext.getCmp('btn_cetak_spri').hide();
					Ext.getCmp('btn_simpan_spri').show();
					Ext.getCmp('txt_no_kartu_insert_spri').setValue(tmp_data_approval_sep.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_spri').setValue(tmp_data_approval_sep.peserta.nama);
					Ext.getCmp('txt_no_kontrol_insert_spri').setValue("");
					Ext.getCmp('txt_sex_insert_spri').setValue(tmp_data_approval_sep.peserta.sex);
					Ext.getCmp('tgl_surat_insert_spri').disable();
					loaddatastorePoliKontrol();
				}
			}
		});
	// }
	return formPanelInsertSPRI;
}
//Vclaim 2.0 // hani 16-02-2022 END
function DataPanel2() {
	radios = new Ext.form.RadioGroup({
		id: 'radiosa',
		layout: 'column',
		columnWidth: .2,
		items: [
			{
				id: 'rb_datang',
				name: 'rb',
				boxLabel: 'Datang Sendiri',
				checked: true,
				inputValue: "true"
			}, {
				boxLabel: 'Rujukan',
				id: 'rb_Rujukan',
				name: 'rb',
				inputValue: "false"
			}
		],
		listeners: {
			change: function () {
				//alert(radios.getValue().inputValue);
				pasienrujukan = radios.getValue().inputValue;
				if (radios.getValue().inputValue === false) {
					caranerima = 99;
					kdrujukannya = 0;

				} else {
					if (Ext.getCmp('rb_datang').getValue() == true) {
						carapenerimaanpasien = 99;
						koderujuk = 0;
						caranerima = carapenerimaanpasien;
						kdrujukannya = koderujuk;
					}
				}
				if (pasienrujukan === 'true') {
					Ext.getCmp('cboRujukanDariRequestEntry').disable();
					Ext.getCmp('cboRujukanRequestEntry').enable();
					Ext.getCmp('txtNamaPerujuk').enable();
					Ext.getCmp('txtAlamatPerujuk').enable();
					Ext.getCmp('txtKotaPerujuk').enable();
					var kriteria = "cara_penerimaan <> ~99~";
					/*carapenerimaanpasien=99;
					koderujuk=0;*/
					// dataCboRujukanDariRWJ(caranerima);
					// loaddatastorerujukan(kodePenerimaanPasien);
				} else {
					Ext.getCmp('cboRujukanDariRequestEntry').setValue('');
					Ext.getCmp('cboRujukanDariRequestEntry').disable();
					Ext.getCmp('cboRujukanRequestEntry').disable();
					Ext.getCmp('cboRujukanRequestEntry').setValue('');
					/*carapenerimaanpasien=99;
					koderujuk=0;*/
					Ext.getCmp('txtNamaPerujuk').disable();
					Ext.getCmp('txtAlamatPerujuk').disable();
					Ext.getCmp('txtKotaPerujuk').disable();
					var kriteria = "cara_penerimaan = ~99~";
					// dataCboRujukanDariRWJ(caranerima);
				}
			}
		}
	});
	var columnCombo = {
		layout: 'column',
		border: false,
		items: [
			{
				columnWidth: .5,
				layout: 'form',
				border: false,
				items: [mcomborujukandari()]
			}, {
				columnWidth: .5,
				layout: 'form',
				border: false,
				items: [mComboRujukan()]
			},
		]
	};
	var items = {
		xtype: 'fieldset',
		title: 'Rujukan',
		items: [
			radios,
			columnCombo,
			{
				xtype: 'textfield',
				fieldLabel: 'Nama ',
				tabIndex: 37,
				name: 'txtNamaPerujuk',
				id: 'txtNamaPerujuk',
				anchor: '95%',
				disabled: true,
				hidden: true,
			}, {
				xtype: 'textfield',
				fieldLabel: 'Alamat ',
				tabIndex: 38,
				name: 'txtAlamatPerujuk',
				id: 'txtAlamatPerujuk',
				anchor: '95%',
				disabled: true,
				hidden: true,
			}, {
				xtype: 'textfield',
				fieldLabel: 'Kota ',
				tabIndex: 39,
				anchor: '95%',
				name: 'txtKotaPerujuk',
				id: 'txtKotaPerujuk',
				disabled: true,
				hidden: true,
			}
		]
	};
	return items;
}
function cariPesertaNoBpjsRujukanFKTLRWJ(nomornya, tipe, tglRujuk, start, limit) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionRWJ/cariRujukanFKTL",
		params: {
			tipe: tipe,
			tgl_rujuk: tglRujuk,
			startnya: start,
			limitnya: limit,
			klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
			no_kartu: nomornya,
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			dataSourceFKTL.removeAll();
			if (tipe == 'bpjs') {
				if (cst.data.metaData.code === '200') {
					dataSourceFKTL.loadData([], false);
					for(var i=0, iLen = cst.data.response.rujukan.length; i < iLen; i++) {
						dataSourceFKTL.add(new dataSourceFKTL.recordType({
							NO_KUNJUNGAN : cst.data.response.rujukan[i].noKunjungan
						}));
					}
				} else {
					Ext.getCmp('textArearujukfktl').setValue(cst.data.metaData.message);
				}
			} else {
				if (cst.data.metaData.code === '200') {
					sendDataArrayFKTLRWJ = []
					var arraynya = [cst.data.response.rujukan.noKunjungan];
					for (var i = 0; i < arraynya.length; i++) {
						sendDataArrayFKTLRWJ.push(arraynya);
						console.log(sendDataArrayFKTLRWJ);
					}
					dataSourceFKTL.loadData(sendDataArrayFKTLRWJ);
				} else {
					Ext.getCmp('textArearujukfktl').setValue(cst.data.metaData.message);
				}
			}
		}, error: function (jqXHR, exception) {
			Ext.getCmp('textArearujukfktl').setValue('Terjadi kesalahan dari server');
		}
	});
}

function DataPanel2DataBPJS() {
	radios2 = new Ext.form.RadioGroup({
		id: 'radios2',
		column: 3,
		items: [
			{
				id: 'rb_no_bpjs_rujukfktl',
				name: 'Datarujukfktl',
				boxLabel: 'No. BPJS',
				checked: true,
				inputValue: "bpjs"
			}, {
				boxLabel: 'No. Rujukan',
				id: 'rb_no_rujukan_rujukfktl',
				name: 'Datarujukfktl',
				inputValue: "norujukan"
			},
			// {
			//     boxLabel: 'Tgl Rujuk',
			//     id: 'rb_tgl_rujukfktl',
			//     name: 'Datarujukfktl',
			//     inputValue: "tglrujuk"
			// }
		],
		listeners: {
			change: function () {
				var kepesertaan2;
				kepesertaan2 = radios2.getValue().inputValue;
				if (kepesertaan2 === 'bpjs') {
					Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').show();
					Ext.getCmp('txtStartrujukfktlDataBPJS').hide();
					Ext.getCmp('labelTextRujukanFKTL').setText('No. BPJS');
					Ext.getCmp('txtLimitrujukfktlDataBPJS').hide();
					Ext.getCmp('labelTextStartTglRujukFKTL').hide();
					Ext.getCmp('labelTextLimitTglRujukFKTL').hide();
					Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').focus();
					Ext.getCmp('txtNoRujukanrujukfktlDataBPJS').hide();
					Ext.getCmp('txtTglrujukfktlDataBPJS').hide();
				} else if (kepesertaan2 === 'norujukan') {
					Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').hide();
					Ext.getCmp('txtStartrujukfktlDataBPJS').hide();
					Ext.getCmp('labelTextRujukanFKTL').setText('No. Rujukan');
					Ext.getCmp('txtLimitrujukfktlDataBPJS').hide();
					Ext.getCmp('labelTextStartTglRujukFKTL').hide();
					Ext.getCmp('labelTextLimitTglRujukFKTL').hide();
					Ext.getCmp('txtNoRujukanrujukfktlDataBPJS').show();
					Ext.getCmp('txtNoRujukanrujukfktlDataBPJS').focus();
					Ext.getCmp('txtTglrujukfktlDataBPJS').hide();
				} else if (kepesertaan2 === 'tglrujuk') {
					Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').hide();
					Ext.getCmp('txtNoRujukanrujukfktlDataBPJS').hide();
					Ext.getCmp('labelTextRujukanFKTL').setText('Tgl. Rujuk');
					Ext.getCmp('txtStartrujukfktlDataBPJS').show();
					Ext.getCmp('labelTextStartTglRujukFKTL').show();
					Ext.getCmp('labelTextLimitTglRujukFKTL').show();
					Ext.getCmp('txtLimitrujukfktlDataBPJS').show();
					Ext.getCmp('txtTglrujukfktlDataBPJS').show();
					Ext.getCmp('txtTglrujukfktlDataBPJS').focus();
				}
			}
		}
	});
	var Field = ['NO_KUNJUNGAN'];
	dataSourceFKTL = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListFKTL = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceFKTL,
		columnLines: false,
		autoScroll: false,
		width: 220,
		height: 250,
		x: 10,
		y: 80,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectRujukanFKTLRWJ = dataSourceFKTL.getAt(row);
					loadMask.show();
					Ext.Ajax.request({
						method: 'POST',
						url: baseURL + "index.php/rawat_jalan/functionCariBpjs/cariRujukanFKTLDetail",
						params: {
							no_rujuk: rowselectRujukanFKTLRWJ.data.NO_KUNJUNGAN
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							loadMask.hide();
							if (cst.success === true) {
								if (cst.data.response == null) {
									Ext.getCmp('textArearujukfktl').setValue('Data Tidak Ada');
								} else {
									var txt = _bpjs.generateObjectToString(cst.data.response.rujukan);
									//formLookup_insert_rujukan(cst);
									Ext.getCmp('textArearujukfktl').setValue(txt);
								}
							} else if (cst.success === false) {
								Ext.getCmp('textArearujukfktl').setValue('Terjadi kesalahan dari server');
							}
						}, error: function (jqXHR, exception) {
							Ext.getCmp('textArearujukfktl').setValue('Terjadi kesalahan dari server');
						}
					});
				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colNoKunjunganFKTL',
				header: 'NO KUNJUNGAN',
				dataIndex: 'NO_KUNJUNGAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			}
		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		title: 'PPK Rujukan FKTL',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					radios2,
					{
						x: 10,
						y: 20,
						id: 'labelTextRujukanFKTL',
						xtype: 'label',
						text: 'No BPJS '
					}, {
						x: 110,
						y: 20,
						xtype: 'label',
						text: ' : '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. BPJS ',
						x: 120,
						y: 20,
						name: 'txtNoBPJSrujukfktlDataBPJS',
						id: 'txtNoBPJSrujukfktlDataBPJS',
						width: 250,
						hidden: false
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. SEP ',
						x: 120,
						y: 20,
						name: 'txtNoRujukanrujukfktlDataBPJS',
						id: 'txtNoRujukanrujukfktlDataBPJS',
						width: 250,
						hidden: true
					}, {
						xtype: 'datefield',
						fieldLabel: 'Tgl Rujuk ',
						x: 120,
						y: 20,
						width: 100,
						value: now_viDaftar,
						format: 'd/M/Y',
						name: 'txtTglrujukfktlDataBPJS',
						id: 'txtTglrujukfktlDataBPJS',
						hidden: true
					}, {
						x: 230,
						y: 20,
						id: 'labelTextStartTglRujukFKTL',
						xtype: 'label',
						text: 'Start ',
						hidden: true
					}, {
						xtype: 'numberfield',
						fieldLabel: 'Start ',
						x: 260,
						y: 20,
						name: 'txtStartrujukfktlDataBPJS',
						id: 'txtStartrujukfktlDataBPJS',
						width: 50,
						style: 'text-align: right',
						hidden: true
					}, {
						x: 330,
						y: 20,
						id: 'labelTextLimitTglRujukFKTL',
						xtype: 'label',
						text: 'Limit ',
						hidden: true
					}, {
						xtype: 'numberfield',
						fieldLabel: 'Limit ',
						x: 360,
						y: 20,
						name: 'txtLimitrujukfktlDataBPJS',
						id: 'txtLimitrujukfktlDataBPJS',
						width: 50,
						style: 'text-align: right',
						hidden: true
					}, {
						xtype: 'button',
						text: 'tambah',
						hidden: true,
						iconCls: 'find',
						x: 110,
						y: 50,
						id: 'btnTambahFKTL',
						handler: function () {
							sendDataArrayFKTLRWJ = []
							var arraynya = ["0001519275587"];
							sendDataArrayFKTLRWJ.push(['0001519275587']);
							dataSourceFKTL.removeAll();
							dataSourceFKTL.loadData(sendDataArrayFKTLRWJ);
						}
					},
					/*{
						xtype: 'button',
						text: 'tambah',
						iconCls: 'add',
						x: 450,
						y:20,
						id: 'btnTambahFKTL',
						handler: function (){
							formLookup_insert_rujukan();
						}
					},*/{
						xtype: 'button',
						text: 'Cari',
						iconCls: 'find',
						x: 10,
						y: 50,
						id: 'btnCarirujukfktl',
						handler: function () {
							var start = Ext.getCmp('txtStartrujukfktlDataBPJS').getValue();
							var limit = Ext.getCmp('txtLimitrujukfktlDataBPJS').getValue();
							if (radios2.getValue().inputValue === 'bpjs') {
								var nomorbpjs = Ext.getCmp('txtNoBPJSrujukfktlDataBPJS').getValue();
								if (nomorbpjs.trim() === '') {
									Ext.MessageBox.alert('Perhatian', 'No BPJS harap diisi !');
								} else {
									cariPesertaNoBpjsRujukanFKTLRWJ(nomorbpjs, 'bpjs', Ext.getCmp('txtTglrujukfktlDataBPJS').getValue().format('Y-m-d'), start, limit);
								}
							} else if (radios2.getValue().inputValue === 'norujukan') {
								var nomorrujukan = Ext.getCmp('txtNoRujukanrujukfktlDataBPJS').getValue();
								if (nomorrujukan.trim() === '') {
									Ext.MessageBox.alert('Perhatian', 'No Rujukan harap diisi !');
								} else {
									cariPesertaNoBpjsRujukanFKTLRWJ(nomorrujukan, 'norujukan', Ext.getCmp('txtTglrujukfktlDataBPJS').getValue().format('Y-m-d'), start, limit);
								}
							} else if (radios2.getValue().inputValue === 'tglrujuk') {
								cariPesertaNoBpjsRujukanFKTLRWJ(0, 'tglrujuk', Ext.getCmp('txtTglrujukfktlDataBPJS').getValue().format('Y-m-d'), start, limit);
							}
						}
					}, {
						xtype: 'textarea',
						width: 594,
						height: 250,
						x: 245,
						y: 80,
						name: 'textArearujukfktl',
						id: 'textArearujukfktl',
						readOnly: true
					},
					gridListFKTL
				]
			}
		]
	};
	return items;
}
function DataPanel4() {
	var items = {
		title: 'Kontak',
		layout: 'form',
		items: [
			{
				xtype: 'textfield',
				fieldLabel: 'Handphone ',
				tabIndex: 38,
				name: 'txtHandphonePasien',
				id: 'txtHandphonePasien',
				width: 500
			}, {
				xtype: 'textfield',
				fieldLabel: 'Email ',
				tabIndex: 39,
				name: 'txtEmailPasien',
				id: 'txtEmailPasien'
			}
		]
	};
	return items;
}
function cariPesertaNoBpjsRujukanFKTPRWJ(nomornya, tipe, tglRujuk, start, limit) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionRWJ/cariRujukanFKTP",
		params: {
			tipe: tipe,
			tgl_rujuk: tglRujuk,
			startnya: start,
			limitnya: limit,
			klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
			no_kartu: nomornya
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			dataSourceFKTP.removeAll();
			if (cst.data.metaData.code === '200') {
				// console.log(cst.data.response.rujukan);
				// Ext.getCmp('textArearujukfktp').setValue(_bpjs.generateObjectToString(cst.data.response.rujukan));
				sendDataArrayFKTPRWJ = []
				var arraynya = [cst.data.response.rujukan.noKunjungan];
				for (var i = 0; i < arraynya.length; i++) {

					sendDataArrayFKTPRWJ.push(arraynya);
					console.log(sendDataArrayFKTPRWJ);

				}
				dataSourceFKTP.loadData(sendDataArrayFKTPRWJ);
			} else {
				Ext.getCmp('textArearujukfktp').setValue(cst.data.metaData.message);
			}
		}, error: function (jqXHR, exception) {
			Ext.getCmp('textArearujukfktp').setValue('Terjadi kesalahan dari server');
		}
	});
}

function DataPanel4DataBPJS() {
	radios4 = new Ext.form.RadioGroup({
		id: 'radios4',
		column: 3,
		items: [
			{
				id: 'rb_no_bpjs_rujukfktp',
				name: 'Datarujukfktp',
				boxLabel: 'No. BPJS',
				checked: true,
				inputValue: "bpjs"
			}, {
				boxLabel: 'No. Rujukan',
				id: 'rb_no_rujukan_rujukfktp',
				name: 'Datarujukfktp',
				inputValue: "norujukan"
			},
			// {
			//     boxLabel: 'Tgl Rujuk',
			//     id: 'rb_tgl_rujukfktp',
			//     name: 'Datarujukfktp',
			//     inputValue: "tglrujuk"
			// }
		],
		listeners: {
			change: function () {
				var kepesertaan4;
				kepesertaan4 = radios4.getValue().inputValue;
				if (kepesertaan4 === 'bpjs') {
					Ext.getCmp('txtNoBpjsFKTP').show();
					Ext.getCmp('txtStartFKTP').hide();
					Ext.getCmp('labelTextRujukanFKTP').setText('No. BPJS');
					Ext.getCmp('txtLimitFKTP').hide();
					Ext.getCmp('StartFKTP').hide();
					Ext.getCmp('limitFKTP').hide();
					Ext.getCmp('txtNoBpjsFKTP').focus();
					Ext.getCmp('txtNoRujukanrujukfktpDataBPJS').hide();
					Ext.getCmp('txtTglFKTP').hide();
				} else if (kepesertaan4 === 'norujukan') {
					Ext.getCmp('txtNoBpjsFKTP').hide();
					Ext.getCmp('txtStartFKTP').hide();
					Ext.getCmp('labelTextRujukanFKTP').setText('No. Rujukan');
					Ext.getCmp('txtLimitFKTP').hide();
					Ext.getCmp('StartFKTP').hide();
					Ext.getCmp('limitFKTP').hide();
					Ext.getCmp('txtNoRujukanrujukfktpDataBPJS').show();
					Ext.getCmp('txtNoRujukanrujukfktpDataBPJS').focus();
					Ext.getCmp('txtTglFKTP').hide();
				} else if (kepesertaan4 === 'tglrujuk') {
					Ext.getCmp('txtNoBpjsFKTP').hide();
					Ext.getCmp('txtNoRujukanrujukfktpDataBPJS').hide();
					Ext.getCmp('labelTextRujukanFKTP').setText('Tgl. Rujuk');
					Ext.getCmp('txtStartFKTP').show();
					Ext.getCmp('StartFKTP').show();
					Ext.getCmp('limitFKTP').show();
					Ext.getCmp('txtLimitFKTP').show();
					Ext.getCmp('txtTglFKTP').show();
					Ext.getCmp('txtTglFKTP').focus();
				}
			}
		}
	});
	var Field = ['NO_KUNJUNGAN'];
	dataSourceFKTP = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListFKTP = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceFKTP,
		columnLines: false,
		autoScroll: false,
		width: 220,
		height: 250,
		x: 10,
		y: 80,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectRujukanFKTPRWJ = dataSourceFKTP.getAt(row);
					loadMask.show();
					Ext.Ajax.request({
						method: 'POST',
						url: baseURL + "index.php/rawat_jalan/functionCariBpjs/cariRujukanFKTPDetail",
						params: {
							no_rujuk: rowselectRujukanFKTPRWJ.data.NO_KUNJUNGAN
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							loadMask.hide();
							if (cst.success === true) {
								if (cst.data.response == null) {
									Ext.getCmp('textArearujukfktp').setValue('Data Tidak Ada');
								} else {
									var txt = _bpjs.generateObjectToString(cst.data.response.rujukan);
									//formLookup_insert_rujukan(cst);
									Ext.getCmp('textArearujukfktp').setValue(txt);
								}
							} else if (cst.success === false) {
								Ext.getCmp('textArearujukfktp').setValue('Terjadi kesalahan dari server');
							}
						}, error: function (jqXHR, exception) {
							Ext.getCmp('textArearujukfktp').setValue('Terjadi kesalahan dari server');
						}
					});
				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colNoKunjunganFKTP',
				header: 'NO KUNJUNGAN',
				dataIndex: 'NO_KUNJUNGAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			}
		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					radios4,
					{
						x: 10,
						y: 20,
						id: 'labelTextRujukanFKTP',
						xtype: 'label',
						text: 'No BPJS '
					}, {
						x: 110,
						y: 20,
						xtype: 'label',
						text: ' : '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. BPJS ',
						x: 120,
						y: 20,
						name: 'txtNoBpjsFKTP',
						id: 'txtNoBpjsFKTP',
						width: 250,
						hidden: false
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. SEP ',
						x: 120,
						y: 20,
						name: 'txtNoRujukanrujukfktpDataBPJS',
						id: 'txtNoRujukanrujukfktpDataBPJS',
						width: 250,
						hidden: true
					}, {
						xtype: 'datefield',
						fieldLabel: 'Tgl Rujuk ',
						x: 120,
						y: 20,
						width: 100,
						value: now_viDaftar,
						format: 'd/M/Y',
						name: 'txtTglFKTP',
						id: 'txtTglFKTP',
						hidden: true
					}, {
						x: 230,
						y: 20,
						id: 'StartFKTP',
						xtype: 'label',
						text: 'Start ',
						hidden: true
					}, {

						xtype: 'numberfield',
						fieldLabel: 'Start ',
						x: 260,
						y: 20,
						name: 'txtStartFKTP',
						id: 'txtStartFKTP',
						width: 50,
						style: 'text-align: right',
						hidden: true
					}, {
						x: 330,
						y: 20,
						id: 'limitFKTP',
						xtype: 'label',
						text: 'Limit ',
						hidden: true
					}, {
						xtype: 'numberfield',
						fieldLabel: 'Limit ',
						x: 360,
						y: 20,
						name: 'txtLimitFKTP',
						id: 'txtLimitFKTP',
						width: 50,
						style: 'text-align: right',
						hidden: true
					}, {
						xtype: 'button',
						text: 'tambah',
						iconCls: 'find',
						hidden: true,
						x: 110,
						y: 50,
						id: 'btnTambahFKTP',
						handler: function () {
							sendDataArrayFKTPRWJ = []
							var arraynya = ["0001519275587"];
							sendDataArrayFKTPRWJ.push(['0001519275587']);
							dataSourceFKTP.removeAll();
							dataSourceFKTP.loadData(sendDataArrayFKTPRWJ);
						}
					}, {
						xtype: 'button',
						text: 'Cari',
						iconCls: 'find',
						x: 10,
						y: 50,
						id: 'btnCariFKTP',
						handler: function () {
							var start = Ext.getCmp('txtStartFKTP').getValue();
							var limit = Ext.getCmp('txtLimitFKTP').getValue();
							if (radios4.getValue().inputValue === 'bpjs') {
								var nomorbpjs = Ext.getCmp('txtNoBpjsFKTP').getValue();
								if (nomorbpjs.trim() === '') {
									Ext.MessageBox.alert('Perhatian', 'No BPJS harap diisi !');
								} else {
									cariPesertaNoBpjsRujukanFKTPRWJ(nomorbpjs, 'bpjs', 'bpjs', Ext.getCmp('txtTglFKTP').getValue().format('Y-m-d'), start, limit);
								}
							} else if (radios4.getValue().inputValue === 'norujukan') {
								var nomorrujukan = Ext.getCmp('txtNoRujukanrujukfktpDataBPJS').getValue();
								if (nomorrujukan.trim() === '') {
									Ext.MessageBox.alert('Perhatian', 'No Rujukan harap diisi !');
								} else {
									cariPesertaNoBpjsRujukanFKTPRWJ(nomorrujukan, 'norujukan', Ext.getCmp('txtTglFKTP').getValue().format('Y-m-d'), start, limit);
								}
							} else if (radios4.getValue().inputValue === 'tglrujuk') {
								cariPesertaNoBpjsRujukanFKTPRWJ(0, 'tglrujuk', Ext.getCmp('txtTglFKTP').getValue().format('Y-m-d'), start, limit);
							}
						}
					}, {
						xtype: 'textarea',
						width: 594,
						height: 250,
						x: 245,
						y: 80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					},
					gridListFKTP
				]
			}
		]
	};
	return items;
}
function DataPanel3() {
	var items = {
		title: 'Penanggung Jawab',
		layout: 'column',
		items: [
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'Nama ',
						name: 'txtNamaPenanggungjawab',
						id: 'txtNamaPenanggungjawab',
						width: 200,
						tabIndex: 40
					}, {
						xtype: 'textfield',
						fieldLabel: 'Tempat Lahir ',
						name: 'txtTempatPenanggungjawab',
						id: 'txtTempatPenanggungjawab',
						width: 200,
						tabIndex: 41
					}, {
						xtype: 'datefield',
						fieldLabel: 'Tanggal Lahir ',
						name: 'dtpPenanggungJawabTanggalLahir',
						id: 'dtpPenanggungJawabTanggalLahir',
						tabIndex: 42,
						format: "d/M/Y",
						value: Today_Pendaftaran,
						anchor: '95%'
					}, {
						columnWidth: .050,
						layout: 'form',
						border: false,
						labelWidth: 100,
						anchor: '95%',
						items: [
							{
								xtype: 'checkbox',
								boxLabel: 'WNI',
								name: 'cbWniPenanggungJawab',
								id: 'cbWniPenanggungJawab',
								tabIndex: 42,
							}
						]
					},
					mComboStatusMaritalPenanggungjawab(),
					mComboPropinsiPenanggungJawab(),
					mComboKabupatenpenanggungjawab(),
					mComboKecamatanPenanggungJawab(),
					mCombokelurahanPenanggungJawab(),
					{
						xtype: 'textfield',
						fieldLabel: 'Alamat ',
						name: 'txtAlamatPenanggungjawab',
						id: 'txtAlamatPenanggungjawab',
						width: 300,
						tabIndex: 48
					}, {
						xtype: 'textfield',
						fieldLabel: 'Kode Pos ',
						name: 'txtKdPosPerusahaanPenanggungjawab',
						id: 'txtKdPosPerusahaanPenanggungjawab',
						width: 50,
						tabIndex: 49
					}, {
						xtype: 'textfield',
						fieldLabel: 'Tlp ',
						name: 'txtTlpPenanggungjawab',
						id: 'txtTlpPenanggungjawab',
						width: 100,
						tabIndex: 50
					}, {
						xtype: 'textfield',
						fieldLabel: 'No Handphone ',
						name: 'txtHpPenanggungjawab',
						id: 'txtHpPenanggungjawab',
						width: 100,
						tabIndex: 51
					}
				]
			}, {
				columnWidth: .50,
				layout: 'form',
				labelWidth: 100,
				border: false,
				items: [
					{
						xtype: 'textfield',
						fieldLabel: 'No KTP ',
						name: 'txtNoKtpPenanggungjawab',
						id: 'txtNoKtpPenanggungjawab',
						width: 200,
						tabIndex: 51
					},
					mComboPropinsiKTPPenanggungJawab(),
					mComboKabupatenKtppenanggungjawab(),
					mComboKecamatanKtpPenanggungJawab(),
					mCombokelurahanKtpPenanggungJawab(),
					{
						xtype: 'textfield',
						fieldLabel: 'Alamat KTP',
						name: 'txtalamatktpPenanggungjawab',
						id: 'txtalamatktpPenanggungjawab',
						width: 300,
						tabIndex: 52
					}, {
						xtype: 'textfield',
						fieldLabel: 'Kode Pos KTP ',
						name: 'txtKdPosKtpPerusahaanPenanggungjawab',
						id: 'txtKdPosKtpPerusahaanPenanggungjawab',
						width: 50,
						tabIndex: 53
					},
					mComboHubunganKeluarga(),
					mComboPendidikanPenanggungJawab(),
					mComboPekerjaanPenanggungJawabRequestEntry(),
					mComboPerusahaanPenanggungJawab(),
					{
						xtype: 'textfield',
						fieldLabel: 'Email ',
						name: 'txtEmailPenanggungjawab',
						id: 'txtEmailPenanggungjawab',
						width: 300,
						tabIndex: 60
					},
					mComboPenanggungjawabJK()
				]
			}
		]
	};
	return items;
}
function cariPesertaHistoryBPJSRWJ(nomornya) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionRWJ/cariHistorySEP",
		params: {
			klinik: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
			no_kartu: nomornya
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.data != null) {
				dataSourceHistorySEP.loadData([], false);
				for (var i = 0, iLen = cst.data.response.list.length; i < iLen; i++) {
					var lis = cst.data.response.list[i];
					dataSourceHistorySEP.add(new dataSourceHistorySEP.recordType({
						NO_SEP: lis.noSEP,
						TGL: lis.tglSEP
					}));
				}
			} else {
				if (cst.message != undefined) {
					Ext.MessageBox.alert('Gagal', cst.message);
				}
			}
		}, error: function (jqXHR, exception) {
			Ext.MessageBox.alert('Gagal', 'Terjadi kesalahan dari server');
		}
	});
}
function DataPanel3DataBPJS() {
	dataSourceHistorySEP = new Ext.data.ArrayStore({
		fields: ['NO_SEP', 'TGL']
	});
	var gridListHistorySEP = new Ext.grid.EditorGridPanel({
		store: dataSourceHistorySEP,
		stripeRows: true,
		id: 'rwj.bpjs.gridhistory',
		columnLines: false,
		autoScroll: true,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		width: 220,
		height: 230,
		x: 10,
		y: 50,
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					console.log(rec.data);
					noSEPHistorySEPRWJ = rec.data.NO_SEP;
					console.log('NO SEP :' + rec.data.NO_SEP);
					barisNoSEPHistorySEP = row;
					Ext.Ajax.request({
						method: 'POST',
						url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjsDenganSEP",
						params: {
							no_sep: rec.data.NO_SEP
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							if (cst.data != null) {
								data_array_sep = cst.data;
								console.log(cst.data);
								var txt = _bpjs.generateObjectToString(cst.data.response);
								Ext.getCmp('textAreaHistorySEP').setValue(txt);
								console.log(txt);
							} else {
								if (cst.message != undefined) {
									Ext.getCmp('textAreaHistorySEP').setValue(cst.message);
									// Ext.Ajax.request({
									// 	method:'POST',
									// 	url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_nomor_sep",
									// 	params: {	
									// 		no_sep 		: rec.data.NO_SEP
									// 	},
									// 	success: function (o) {
									// 		//var cst = Ext.decode(o.responseText);
									// 		//dataSourceHistorySEP.removeAll();
									// 		//gridListHistorySEP.getView().refresh();
									// 		cariPesertaHistoryBPJSRWJ(Ext.getCmp('txtNoBpjsHistorySEP').getValue());

									// 	},error: function (jqXHR, exception) {
									// 	  // Ext.getCmp('textAreaHistorySEP').setValue('Terjadi kesalahan dari server');
									// 	}
									// });	
								}
							}
						}, error: function (jqXHR, exception) {
							Ext.getCmp('textAreaHistorySEP').setValue('Terjadi kesalahan dari server');
						}
					});
				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			{
				id: 'colNoSepHistorySEP',
				header: 'NO SEP',
				dataIndex: 'NO_SEP',
				sortable: false,
				width: 200
			}, {
				id: 'colNoSepHistorySEP',
				header: 'Tanggal',
				dataIndex: 'TGL',
				hidden: true,
				sortable: false,
				width: 200
			}
		]),
		viewConfig: {
			forceFit: true
		},
	});
	var items = {
		title: 'History SEP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					{
						x: 10,
						y: 5,
						id: 'labelTextRujukanHistorySEP',
						xtype: 'label',
						text: 'No BPJS '
					}, {
						x: 110,
						y: 5,
						xtype: 'label',
						text: ' : '
					}, {
						xtype: 'textfield',
						fieldLabel: 'No. BPJS ',
						x: 120,
						y: 5,
						name: 'txtNoBpjsHistorySEP',
						id: 'txtNoBpjsHistorySEP',
						width: 250,
						listeners: {
							'render': function (c) {
								c.getEl().on('dblclick', function (e) {
									c.setValue(Ext.getCmp('txtNoAskes').getValue());
								}, c);
								c.getEl().on('keypress', function (e) {
									if (e.getKey() == 13)
										cariPesertaHistoryBPJSRWJ(c.getValue());
								}, c);
							}
						}
					}, {
						xtype: 'button',
						text: 'Cari',
						iconCls: 'find',
						x: 410,
						y: 5,
						id: 'btnCariHistorySEP',
						handler: function () {
							var nosepnya = Ext.getCmp('txtNoBpjsHistorySEP').getValue();
							if (nosepnya.trim() === '') {
								Ext.MessageBox.alert('Perhatian', 'No SEP harap diisi !');
							} else {
								cariPesertaHistoryBPJSRWJ(nosepnya);
							}
						}
					}, {
						xtype: 'button',
						text: 'tambah',
						hidden: true,
						iconCls: 'find',
						x: 410,
						y: 25,
						id: 'btnTambahHistorySEP',
						handler: function () {
							sendDataArrayHistorySEPRWJ = []
							var arraynya = ["0001519275587"];
							for (var i = 0; i < 9; i++) {
								sendDataArrayHistorySEPRWJ.push(['000151927558' + i]);
							}
							dataSourceHistorySEP.removeAll();
							dataSourceHistorySEP.loadData(sendDataArrayHistorySEPRWJ);
						}
					}, {
						xtype: 'textarea',
						width: 594,
						height: 230,
						x: 245,
						y: 50,
						name: 'textAreaHistorySEP',
						id: 'textAreaHistorySEP'
					},
					gridListHistorySEP
				]
			}

		],
		bbar: {
			xtype: 'toolbar',
			items: [
				{
					xtype: 'button',
					text: 'Cetak SEP',
					id: 'btnCetakSEPHistorySEP',
					handler: function () {
						// _bpjs.cetakSep(noSEPHistorySEPRWJ,undefined,Ext.getCmp('txtcatatan').getValue());
						//cekKunjunganPasienLaluCetakBpjs(noSEPHistorySEPRWJ);

						// _bpjs.cetakSep(noSEPHistorySEPRWJ,undefined,Ext.getCmp('txtcatatan').getValue());
						// _bpjs.cetakSep(noSEPHistorySEPRWJ);
						// cekKunjunganPasienLaluCetakBpjs();
						//cekKunjunganPasienLaluCetakBpjs(noSEPHistorySEPRWJ);
						var note = "";
						// note=catatan.replace("/","~~_~~");
						/*var url = baseURL + "index.php/rawat_jalan/functionRWJ/cetaksep/" +noSEPHistorySEPRWJ+"/"+"Cetak ulang";
						new Ext.Window({
							title: 'SEP',
							width: 900,
							height: 500,
							constrain: true,
							modal: true,
							listeners:{
								deactivate: function (){
									if(callback != undefined){
										callback();
									}
								}
							},
							tbar:[
							{
								xtype 		: 'button',
								text 		: 'Cetak',
								iconCls 	: 'print',
								hidden:false,
								handler: function (){
									// Ext.Ajax.request({
									// 	method:'POST',
									// 	url: baseURL + "index.php/rawat_jalan/functionRWJ/cetaksepdirect",
									// 	params: {
									// 		noSep: noSep,
									// 		catatan: "Cetak Ulang"
									// 	},
									// 	success: function (o) {
									// 	},error: function (jqXHR, exception) {
									// 	}
									// });
									window.open(baseURL + "index.php/laporan/lap_bpjs/cetak_sep/"+noSEPHistorySEPRWJ, '_blank');
								}
							}
							],
							html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
						}).show();*/
						var url = baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistorySEPRWJ + "/true";
						new Ext.Window({
							title: 'SEP',
							width: 900,
							height: 500,
							constrain: true,
							modal: false,
							html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
							tbar: [
								{
									xtype: 'button',
									text: '<i class="fa fa-print"></i> Cetak',
									handler: function () {
										window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistorySEPRWJ + "/false", "_blank");
									}
								}
							]
						}).show();
					}
				},
				{
					xtype: 'button',
					text: 'Histori Rujukan Internal',
					id: 'btnHistoriRujukanInternal',
					handler: function () {
						console.log(noSEPHistorySEPRWJ);
						if (noSEPHistorySEPRWJ == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Histori Rujukan Internal');
						} else {
							console.log(data_array_sep);
							formLookup_histori_rujukan_internal(data_array_sep);
						}


					}
				},
				{
					xtype: 'button',
					text: 'Hapus SEP',
					id: 'btnDeleteSEPHistorySEPs',
					handler: function () {
						Ext.MessageBox.confirm('Hapus SEP', "Yakin Akan Hapus SEP '" + noSEPHistorySEPRWJ + "' Sekarang ?", function (btn) {
							if (btn === 'yes') {
								var param = {
									sep: noSEPHistorySEPRWJ,
									tglsep: data_array_sep.response.tglSep,
									kd_pasien: data_array_sep.response.peserta.noMr,
									norujukan: data_array_sep.response.noRujukan,
									kddpjp: data_array_sep.response.dpjp.kdDPJP
								}
								$.ajax({
									type: 'POST',
									dataType: 'JSON',
									url: baseURL + "index.php/rawat_jalan/functionRWJ/hapusSEP/",
									data: param,
									success: function (cst) {
										if (cst.metaData.code == "200") {
											//   winDelete.close();
											Ext.MessageBox.alert('Sukses', 'SEP berhasil dihapus');
											cariPesertaHistoryBPJSRWJ(Ext.getCmp('txtNoBpjsHistorySEP').getValue());
											Ext.getCmp('textAreaHistorySEP').setValue('');
										} else {
											Ext.MessageBox.alert('Gagal', cst.metaData.message);
										}
									},
									error: function (jqXHR, exception) {
										loadMask.hide();
										Ext.MessageBox.alert('Gagal', 'Delete SEP gagal.');
										dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
										Ext.Ajax.request({
											url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
											params: {
												respon: jqXHR.responseText,
												keterangan: 'Error create SEP'
											}
										});
									}
								});
							}
						});
					}
				}, {
					xtype: 'button',
					text: 'Pulangkan',
					id: 'btnPulangkanHistorySEP',
					handler: function () {
						console.log(noSEPHistorySEPRWJ);
						if (noSEPHistorySEPRWJ == undefined) {
							ShowPesanError_viDaftar('Harap Pilih No SEP', 'Pulang SEP');
						} else {
							if (data_array_sep.response.jnsPelayanan == 'Rawat Inap') {
								formLookup_pulang_sep(data_array_sep);
							} else {
								ShowPesanWarning_viDaftar('Jenis Pelayanan Pasien Harus Rawat Inap', 'Warning');
							}
						}
						// var dtpPulang=null;
						// var winDelete=new Ext.Window({
						// 	title:'SEP Pulang',
						// 	items:[
						// 		new Ext.Panel({
						// 			layout:'column',
						// 			border:false,
						// 			items:[
						// 				new Ext.form.DisplayField({
						// 					value:'Tanggal Pulang : &nbsp;',
						// 					style:{'margin':'12px'}
						// 				}),
						// 				dtpPulang=new Ext.form.DateField({
						// 					value:new Date,
						// 					format: "d-M-Y",
						// 					style:{'margin':'10px'}
						// 				}),
						// 			]
						// 		})
						// 	],
						// 	fbar:[
						// 		new Ext.Button({
						// 			text:'Pulangkan',
						// 			handler:function(){
						// 				Ext.MessageBox.confirm('Hapus SEP', "Yakin Akan Pulangkan No SEP '"+noSEPHistorySEPRWJ+"'?", function (btn) {
						// 					if (btn === 'yes') {
						// 						var dtFromat=cTglAsli = new Date(dtpPulang.getValue()),
						// 						cTglAsli = cTglAsli.format('Y-m-d');
						// 						var param={
						// 							sep:noSEPHistorySEPRWJ,
						// 							tgl:cTglAsli
						// 						} 
						// 						loadMask.show();
						// 						$.ajax({
						// 							type: 'POST',
						// 							dataType: 'JSON',
						// 							url: baseURL + "index.php/rawat_jalan/functionRWJ/updatePulangSEP/",
						// 							data: param,
						// 							success: function (cst) {
						// 								loadMask.hide();
						//                                 if (cst.metaData.code == "200") {
						//                                     winDelete.close();
						//                                     Ext.MessageBox.alert('Sukses','Berhasil Pulang');
						//                                 }else{
						//                                     Ext.MessageBox.alert('Gagal',cst.metaData.message);
						//                                 }
						// 							},
						// 							error: function (jqXHR, exception) {
						// 								loadMask.hide();
						// 								Ext.MessageBox.alert('Gagal', 'Pulang Pasien duPulangkan.');
						// 								dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
						// 								Ext.Ajax.request({
						// 									url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
						// 									params: {
						// 										respon: jqXHR.responseText,
						// 										keterangan: 'Error create SEP'
						// 									}
						// 								});
						// 							}
						// 						}); 
						// 					}
						// 				});
						// 			}
						// 		}),
						// 		new Ext.Button({
						// 			text:'Batal',
						// 			handler:function(){
						// 				winDelete.close();
						// 			}
						// 		})
						// 	]
						// }).show();
					}
				},
				{
					xtype: 'button',
					text: 'Update SEP',
					id: 'btnUpdateSEPHistorySEPs',
					handler: function () {
						/*Ext.Ajax.request({
						url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_history_sep",
							params: {no_kartu   :Ext.getCmp('txtNoBpjsHistorySEP').getValue() },
							failure: function(o){
								var cst = Ext.decode(o.responseText);
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
							//	loaddatastoreDokterPJBPJS(tmp_poli_tujuan);
								// loaddatastoreUnitBPJS();
								// if(cst.listData[0].katarak==1){
								// 	Ext.getCmp('cbo_katarak').setValue(true);
								// 	tmp_katarak_update=1;
								// }else{
								// 	Ext.getCmp('cbo_katarak').setValue(false);
								// }
								// Ext.getCmp('txt_kd_diagnosa').setValue(cst.listData[0].diag_awal);
								// Ext.getCmp('txt_no_catatan').setValue(cst.listData[0].catatan);
								// Ext.getCmp('txt_no_tlp').setValue(cst.listData[0].no_tlp);
								// Ext.getCmp('txt_no_surat_kontrol').setValue(cst.listData[0].no_surat_dpjp);
								// Ext.getCmp('cboDokterPJBPJS').setValue(cst.listData[0].nama).disable();
								// tmp_kelas_rawat=cst.listData[0].kelas_rawat;
								// tmp_dokter_dpjp=cst.listData[0].kd_dpjp;
								// tmp_diagnosa=cst.listData[0].diag_awal;
								// tmp_tgl_rujukan=cst.listData[0].tgl_rujukan;
								// tmp_poli_tujuan_update=cst.listData[0].poli_tujuan;
								// tmp_diagnosa_update=cst.listData[0].diag_awal;
								// tmp_ppk_rujukan=cst.listData[0].ppk_rujukan;
								// console.log(tmp_tgl_rujukan);
								
							}
						});*/

						console.log(rowSelected_viDaftar);
						dialog_update_sep_modal("RWJ");
						init_data(Ext.getCmp('txtNoBpjsHistorySEP').getValue(), data_array_sep, rowSelected_viDaftar, "RWJ");

						// formLookup_rujukan_bpjs_RWJ(data_array_sep);
						// datainit_viUpdateSep(data_array_sep);

					}
				},
				{
					xtype: 'button',
					text: 'Pembuatan Rujukan',
					id: 'btnInsertRujukanHistorySEPs',
					handler: function () {
						console.log(noSEPHistorySEPRWJ);
						if (noSEPHistorySEPRWJ == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rujukan');
						} else {

							formLookup_insert_rujukan(data_array_sep);
						}


					}
				},
				{
					xtype: 'button',
					text: 'Pembuatan Rujukan Balik',
					id: 'btnInsertRujukanBalikHistorySEPs',
					handler: function () {
						console.log(noSEPHistorySEPRWJ);
						if (noSEPHistorySEPRWJ == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rujukan Balik');
						} else {
							console.log(data_array_sep);
							formLookup_insert_rujukan_balik(data_array_sep);
						}


					}
				},
				{
					xtype: 'button',
					text: 'Pembuatan Rencana Kontrol',
					id: 'btnInsertRencanaKontrolHistorySEPs',
					handler: function () {
						console.log(noSEPHistorySEPRWJ);
						if (noSEPHistorySEPRWJ == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rencana Kontrol');
						} else {
							console.log(data_array_sep);
							formLookup_insert_rencana_kontrol(data_array_sep,'RENCANA KONTROL');
						}
					}
				}
			]
		}
	};
	return items;
}

// ------------------------------------------hani 28-1-2022----------------------------------------------------------
var rowselectMonitoringPeserta;
var noSEPHistoryMonitoring;
var data_array_sep_monitoring;
function DataPanel5DataBPJS(){
	var Field = ['diagnosa','jnsPelayanan','kelasRawat','namaPeserta','noKartu','noSep','poli','tglPlgSep','tglSep','ppkPelayanan'];
	dataSourceMonitoringPeserta = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListMonitoringPelayananPeserta = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceMonitoringPeserta,
		columnLines: false,
		autoScroll: false,
		width : 820,
		height: 280, 
		x:10,
		y:60,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectRujukanRWJ = dataSourceMonitoringPeserta.getAt(row);
				    console.log(rowselectMonitoringPeserta);
				}
			}
		}),
		listeners:
		{
			rowclick: function (sm, ridx, cidx)
			{
                console.log("satu klik");
                rowselectMonitoringPeserta = dataSourceMonitoringPeserta.getAt(ridx);
				noSEPHistoryMonitoring = rowselectMonitoringPeserta.data.noSep;
                Ext.Ajax.request({
                    method: 'POST',
                    url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjsDenganSEP",
                    params: {
                        klinik: 1,
                        no_sep: rowselectMonitoringPeserta.data.noSep
                    },
                    success: function (o) {
                        var cst = Ext.decode(o.responseText);
                        if (cst.data != null) {
                            var txt = _bpjs.generateObjectToString(cst.data.response);
                            tmp_no_sep_for_update = cst.data.response.noSep;
                            data_array_sep_monitoring = cst.data;
                        }else{
                            data_array_sep = '';
                            ShowPesanError_viDaftar(cst.message, 'Data History Pelayanan');
                        }
                    }, error: function (jqXHR, exception) {
                        Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
                    }
                });				
			},
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header:'No.'
			}),{
				id: 'colDiagnosaMonKunj',
				header: 'DIAGNOSA',
				dataIndex: 'diagnosa',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colJnsRawatMonKunj',
				header: 'PELAYANAN',
				dataIndex: 'jnsPelayanan',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colKelasRawatMonKunj',
				header: 'KELAS RAWAT',
				dataIndex: 'kelasRawat',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNamaMonKunj',
				header: 'NAMA',
				dataIndex: 'namaPeserta',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNoKartuMonKunj',
				header: 'NO KARTU',
				dataIndex: 'noKartu',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNoSEPMonKunj',
				header: 'NO SEP',
				dataIndex: 'noSep',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colppkPelayananMonKunj',
				header: 'PPK Pelayanan',
				dataIndex: 'ppkPelayanan',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colPoliMonKunj',
				header: 'POLI',
				dataIndex: 'poli',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colTglPulangMonKunj',
				header: 'TGL PULANG SEP',
				dataIndex: 'tglPlgSep',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'coltglSEPMonKunj',
				header: 'TGL SEP',
				dataIndex: 'tglSep',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			}
			]),
		viewConfig: {
			forceFit: true
		}
	}); 
	var items ={
		title: 'Data History Pelayanan',
		layout: 'column',
		border: false,
		autoScroll: false,
		items:[
		{
			layout: 'absolute',
			bodyStyle: 'padding: 0px 0px 0px 0px',
			border: false,
			autoScroll: false,
			width: '100%',
			height: 350,
			anchor: '100% 100%',
			items:[
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'No BPJS'
			},{
				x: 60,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'textfield',
				fieldLabel: '',
				x: 70,
				y: 10,
				name: 'txt_no_kartu_monitoring_peserta',
				id: 'txt_no_kartu_monitoring_peserta',
				maskRe: /[0-9.]/,
				width: 120,
				value: Ext.getCmp('txtNoAskes').getValue(),
				hidden:false
			},{
				x: 200,
				y: 10,
				xtype: 'label',
				text: 'Tanggal Awal'
			},{
				x: 270,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'datefield',
				fieldLabel: '',
				x: 280,
				y: 10,
				name: 'dt_tgl_awal_monitoring_peserta',
				id: 'dt_tgl_awal_monitoring_peserta',
				width: 120,
				value:now_viDaftar,
				format:'d-M-Y',
				hidden:false
			},{
				x: 410,
				y: 10,
				xtype: 'label',
				text: 'Tanggal Akhir'
			},{
				x: 480,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'datefield',
				fieldLabel: '',
				x: 490,
				y: 10,
				name: 'dt_tgl_akhir_monitoring_peserta',
				id: 'dt_tgl_akhir_monitoring_peserta',
				width: 120,
				value:now_viDaftar,
				format:'d-M-Y',
				hidden:false
			},{
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				x: 650,
				y: 10,
				id: 'btnCariMonitoring_peserta',
				handler: function (){
					//didieu
					cariHistoryPelayanan();
				}
			},
			gridListMonitoringPelayananPeserta
			]
		}
		],
        bbar: {
            xtype: 'toolbar',
            items: [
                {
					xtype: 'button',
					text: 'Cetak SEP',
					id: 'btnCetakSEPMonitoring',
					handler: function (){
						var note="";
						console.log(noSEPHistoryMonitoring);
						console.log(data_array_sep_monitoring);
						if (noSEPHistoryMonitoring == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Histori Rujukan Internal');
						} else {
							if (data_array_sep_monitoring.response.jnsPelayanan == "Rawat Inap") {
								var url = baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistoryMonitoring+"/"+ data_array_sep_monitoring.response.kontrol.kdDokter +"/true";
							} else {
								var url = baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistoryMonitoring+"/"+ data_array_sep_monitoring.response.dpjp.kdDPJP +"/true";
							}               
							new Ext.Window({
								title: 'SEP',
								width: 900,
								height: 500,
								constrain: true,
								modal: false,
								html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
								tbar 	: [
									{
										xtype 	: 'button',
										text 	: '<i class="fa fa-print"></i> Cetak',
										handler : function(){
											if (data_array_sep_monitoring.response.jnsPelayanan == "Rawat Inap") {
												window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistoryMonitoring+"/"+ data_array_sep_monitoring.response.kontrol.kdDokter +"/false", "_blank");
											} else {
												window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + noSEPHistoryMonitoring+"/"+ data_array_sep_monitoring.response.dpjp.kdDPJP +"/false", "_blank");
											}
										}
									}
								]
							}).show();
						}
					}
				},
               {
					xtype: 'button',
					text: 'Histori Rujukan Internal',
					id: 'btnHistoriRujukanInternalMonitoring',
					handler: function () {
						console.log(noSEPHistoryMonitoring);
						if (noSEPHistoryMonitoring == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Histori Rujukan Internal');
						} else {
							console.log(data_array_sep_monitoring);
							formLookup_histori_rujukan_internal(data_array_sep_monitoring);
						}


					}
				},
        {
            xtype: 'button',
            text: 'Hapus SEP',
            id: 'btnDeleteSEPMonitoring',
            handler: function () {
                Ext.MessageBox.confirm('Hapus SEP', "Yakin Akan Hapus SEP '" + noSEPHistoryMonitoring + "' Sekarang ?", function (btn) {
                    if (btn === 'yes') {
                        var param = {
                            sep: noSEPHistoryMonitoring,
                            tglsep: data_array_sep_monitoring.response.tglSep,
                            kd_pasien: data_array_sep_monitoring.response.peserta.noMr,
                            norujukan: data_array_sep_monitoring.response.noRujukan,
                            kddpjp: data_array_sep_monitoring.response.dpjp.kdDPJP
                                }
                        $.ajax({
                            type: 'POST',
                            dataType: 'JSON',
                            url: baseURL + "index.php/rawat_jalan/functionRWJ/hapusSEP/",
                            data: param,
                            success: function (cst) {
                                if (cst.metaData.code == "200") {
                                    // winDelete.close();
                                    Ext.MessageBox.alert('Sukses', 'Berhasil Dihapus');
                                    cariHistoryPelayanan();
                                } else {
                                    Ext.MessageBox.alert('Gagal', cst.metaData.message);
                                }
                            },
                            error: function (jqXHR, exception) {
                                loadMask.hide();
                                Ext.MessageBox.alert('Gagal', 'Delete SEP gagal.');
                                dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
                                Ext.Ajax.request({
                                    url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                                    params: {
                                        respon: jqXHR.responseText,
                                        keterangan: 'Error create SEP'
                                    }
                                });
                            }
                        });
                    }
                });
            }
        },
        {
            xtype: 'button',
            text: 'Pulangkan',
            id: 'btnPulangkanMonitoring',
            handler: function () {
                console.log(noSEPHistoryMonitoring);
                if (noSEPHistoryMonitoring == undefined) {
                    ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pulang SEP');
                } else {
                    if (data_array_sep_monitoring.response.jnsPelayanan == 'Rawat Inap') {
                        formLookup_pulang_sep(data_array_sep_monitoring);
                    } else {
                        ShowPesanWarning_viDaftar('Jenis Pelayanan Pasien Harus Rawat Inap', 'Warning');
                    }
                }
            }
        },
        {
					xtype: 'button',
					text: 'Update SEP',
					id: 'btnUpdateSEPMonitoring',
					handler: function () {
						if (noSEPHistoryMonitoring == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Histori Rujukan Internal');
						} else {
						console.log(rowSelected_viDaftar);
						dialog_update_sep_modal("RWJ");
						init_data(Ext.getCmp('txtNoBpjsHistorySEP').getValue(), data_array_sep_monitoring, rowSelected_viDaftar, "RWJ");
						}
						// formLookup_rujukan_bpjs_RWJ(data_array_sep);
						// datainit_viUpdateSep(data_array_sep);

					}
				},
        {
					xtype: 'button',
					text: 'Pembuatan Rujukan',
					id: 'btnInsertRujukanMonitoring',
					handler: function () {
						console.log(noSEPHistoryMonitoring);
						if (noSEPHistoryMonitoring == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rujukan');
						} else {

							formLookup_insert_rujukan(data_array_sep_monitoring);
						}
					}
				},
        {
					xtype: 'button',
					text: 'Pembuatan Rujukan Balik',
					id: 'btnInsertRujukanBalikMonitoring',
					handler: function () {
						console.log(noSEPHistoryMonitoring);
						if (noSEPHistoryMonitoring == undefined) {
							ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rujukan Balik');
						} else {
							console.log(data_array_sep_monitoring);
							formLookup_insert_rujukan_balik(data_array_sep_monitoring);
						}
					}
				},
        {
            xtype: 'button',
            text: 'Pembuatan Rencana Kontrol',
            id: 'btnInsertRencanaKontrolMonitoring',
            handler: function () {
				    console.log("sini");
                console.log(data_array_sep_monitoring);
                console.log(data_array_sep_monitoring.response);
                if (data_array_sep_monitoring.response.noSep == undefined) {
                    ShowPesanWarning_viDaftar('Harap Pilih No SEP', 'Pembuatan Rencana Kontrol');
                } else {
                    console.log(data_array_sep_monitoring);
                    formLookup_insert_rencana_kontrol(data_array_sep_monitoring,'RENCANA KONTROL');
                }
            }
        }
        ]
    }
        
	};
	return items;
}
function cariHistoryPelayanan(){
	if(Ext.getCmp('txt_no_kartu_monitoring_peserta').getValue() ==''){
		ShowPesanWarning_viDaftar('Nomor Kartu Harus Di isi !','Data Kunjungan');
	}else if(Ext.getCmp('dt_tgl_awal_monitoring_peserta').getValue() ==''){
		ShowPesanWarning_viDaftar('Tanggal Awal Harus Di isi !','Data Kunjungan');
	}else if(Ext.getCmp('dt_tgl_akhir_monitoring_peserta').getValue() ==''){
		ShowPesanWarning_viDaftar('Tanggal Akhir Harus Di isi !','Data Kunjungan');
	}
	else{
		var tglawal = Ext.getCmp('dt_tgl_awal_monitoring_peserta').getValue();
		var tglakhir = Ext.getCmp('dt_tgl_akhir_monitoring_peserta').getValue();
		Ext.Ajax.request({
			method:'POST',
			url: baseURL + "index.php/rawat_jalan/functionCariBpjs/cari_data_peserta_monitoring",
			params: {	
				no_kartu: Ext.getCmp('txt_no_kartu_monitoring_peserta').getValue(),
				tgl_awal : tglawal.format('Y-m-d'),
				tgl_akhir : tglakhir.format('Y-m-d')
			},
			success: function(imprt) 
			{
				var cst = Ext.decode(imprt.responseText);
				// console.log(cst.data.metaData.code);
				if(cst.data != null){
					loadMask.hide();
					// var cst1 = Ext.decode(cst.data);
				// console.log(cst1);
					if (cst.data.metaData.code == 200) {					
						var data = cst.data.response.histori;

						dataSourceMonitoringPeserta.loadData([],false);
						for (var i = 0; i < data.length; i++) {					
							dataSourceMonitoringPeserta.add(new dataSourceMonitoringPeserta.recordType(data[i]));
						}
					}else{
						rowselectMonitoringPeserta = '';
						ShowPesanWarning_viDaftar(cst.data.metaData.message,'Get History Pelayanan');

						dataSourceMonitoringPeserta.loadData([],false);
					}
				}else{
					loadMask.hide();
					ShowPesanWarning_viDaftar(cst.message,'Get History Pelayanan');
					dataSourceMonitoringPeserta.loadData([],false);      
				}
			},
			error: function (jqXHR, exception) {
				loadMask.hide();
				ShowPesanWarning_viDaftar('Terjadi kesalahan dari server','Get History Pelayanan');
				dataSourceMonitoringPeserta.loadData([],false);
			}
		});
	}
}
// --------------------------------------------------------------------------------------------------
// -----------------------------Data Nomor Surat Kontrol Berdasarakan Noka Bpjs--------------------------------------
// Egi 22 Febuari 2022
var rowselectDataSuratKontrol;
var jenis_filter_rencana_kontrol;
function DataPanel6DataBPJS(){
	var Field = ['noSuratKontrol','jnsPelayanan','jnsKontrol','namaJnsKontrol','tglRencanaKontrol','tglTerbitKontrol','noSepAsalKontrol',
    'poliAsal','namaPoliAsal','poliTujuan','namaPoliTujuan','tglSEP','kodeDokter','namaDokter','noKartu','nama','terbitSEP'];
	dataSourceSuratKontrol = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListDataSuratKontrol = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceSuratKontrol,
		columnLines: false,
		autoScroll: false,
		width : 820,
		height: 280, 
		x:10,
		y:60,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
				rowselectDataSuratKontrol = dataSourceSuratKontrol.getAt(row);
				console.log(rowselectDataSuratKontrol);
				}
			}
		}),
		listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowselectDataSuratKontrol = dataSourceSuratKontrol.getAt(ridx);
				console.log(rowselectDataSuratKontrol);
				
			},
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header:'No.'
			}),{
				id: 'colSuratKontrol',
				header: 'No Surat',
				dataIndex: 'noSuratKontrol',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colJnsPelayanan',
				header: 'Jenis Pelayanan',
				dataIndex: 'jnsPelayanan',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colJnsKontrol',
				header: 'Jenis Kontrol',
				dataIndex: 'namaJnsKontrol',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colTglRencanaKontrol',
				header: 'Tgl Rencana Kontrol',
				dataIndex: 'tglRencanaKontrol',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colTglTerbitKontrol',
				header: 'Tgl Terbit',
				dataIndex: 'tglTerbitKontrol',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNoKartu',
				header: 'No Kartu',
				dataIndex: 'noKartu',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colSepAsal',
				header: 'Sep Asal',
				dataIndex: 'noSepAsalKontrol',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNamaPoliTujuan',
				header: 'Poli Tujuan',
				dataIndex: 'namaPoliTujuan',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colNamaDokter',
				header: 'Dokter',
				dataIndex: 'namaDokter',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},{
				id: 'colTerbitSep',
				header: 'Terbit Sep',
				dataIndex: 'terbitSEP',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			}
			]),
		viewConfig: {
			forceFit: true
		}
	}); 
	var items ={
		title: 'Data Nomor Surat Kontrol',
		layout: 'column',
		border: false,
		autoScroll: false,
		items:[
		{
			layout: 'absolute',
			bodyStyle: 'padding: 0px 0px 0px 0px',
			border: false,
			autoScroll: false,
			width: '100%',
			height: 350,
			anchor: '100% 100%',
			items:[
			{
				x: 10,
				y: 10,
				xtype: 'label',
				text: 'No BPJS'
			},{
				x: 60,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'textfield',
				fieldLabel: '',
				x: 70,
				y: 10,
				name: 'txt_no_kartu_surat_kontrol',
				id: 'txt_no_kartu_surat_kontrol',
				maskRe: /[0-9.]/,
				width: 120,
				value: Ext.getCmp('txtNoAskes').getValue(),
				hidden:false
			},{
				x: 200,
				y: 10,
				xtype: 'label',
				text: 'Bulan'
			},{
				x: 240,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'datefield',
				fieldLabel: '',
				x: 250,
				y: 10,
				name: 'dt_bulan_surat_kontrol',
				id: 'dt_bulan_surat_kontrol',
				width: 120,
				value:now_viDaftar,
				format:'M',
				hidden:false
			},{
				x: 380,
				y: 10,
				xtype: 'label',
				text: 'Tahun'
			},{
				x: 420,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'datefield',
				fieldLabel: '',
				x: 430,
				y: 10,
				name: 'dt_tahun_surat_kontrol',
				id: 'dt_tahun_surat_kontrol',
				width: 120,
				value:now_viDaftar,
				format:'Y',
				hidden:false
			},{
				x: 560,
				y: 10,
				xtype: 'label',
				text: 'Jenis'
			},{
				x: 600,
				y: 10,
				xtype: 'label',
				text: ' : '
			},
            mCombJenisFilterKontrol(),
			{
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				x: 740,
				y: 10,
				id: 'btnCariSuratKontrol',
				handler: function (){
                    cariDataSuratKontrol();
				}
			},
			gridListDataSuratKontrol
			]
		}
		],
        bbar: {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Hapus Surat Kontrol',
                    id: 'btnDeleteSEPHistorySEPs',
                    handler: function () {
                        var data = rowselectDataSuratKontrol.data;
                        console.log(data);
                        Ext.MessageBox.confirm('Hapus Surat Kontrol', "Yakin Akan Hapus No Surat '" + data.noSuratKontrol + "' Sekarang ?", function (btn) {
                            if (btn === 'yes') {
                                var param = {
                                    no_kontrol: data.noSuratKontrol,
                                }
                                $.ajax({
                                    type: 'POST',
                                    dataType: 'JSON',
                                    url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rencana_kontrol/",
                                    data: param,
                                    success: function (cst) {
                                        if (cst.metaData.code == "200") {
                                            // winDelete.close();
                                            Ext.MessageBox.alert('Sukses', 'Berhasil Dihapus');
                                            cariDataSuratKontrol();
                                        } else {
                                            Ext.MessageBox.alert('Gagal', cst.metaData.message);
                                        }
                                    },
                                    error: function (jqXHR, exception) {
                                        loadMask.hide();
                                        Ext.MessageBox.alert('Gagal', 'Delete Surat Kontrol gagal.');
                                    }
                                });
                            }
                        });
                    }
                }
            ]
        }
	};
	return items;
}

function mCombJenisFilterKontrol() {
    var cboJenisFilterKontrol = new Ext.form.ComboBox({
        id: 'combo_filter_rencanan_kontrol_igd',
        x: 610,
        y: 10,
        typeAhead: true,
        triggerAction: 'all',
        width: 120,
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        tabIndex: 6,
        forceSelection: true,
        emptyText: '',
        anchor: '87%',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[1, 'Tgl Entri'], [2, 'Tgl Rencana Kontrol']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        enableKeyEvents: true,
        listeners: {
            'select': function (a, b, c) {
                console.log(b.data);
                if (b.data.Id == 1) {
                    jenis_filter_rencana_kontrol = 1;
                } else {
                    jenis_filter_rencana_kontrol = 2;
                }
            },

        }
    });
    return cboJenisFilterKontrol;
}

function cariDataSuratKontrol(){
    if(Ext.getCmp('txt_no_kartu_surat_kontrol').getValue() ==''){
        ShowPesanWarning_viDaftar('Nomor Kartu Harus Di isi !','Data Surat Kontrol');
    }else if(Ext.getCmp('dt_bulan_surat_kontrol').getValue() ==''){
        ShowPesanWarning_viDaftar('Bulan Harus Di isi !','Data Surat Kontrol');
    }else if(Ext.getCmp('dt_tahun_surat_kontrol').getValue() ==''){
        ShowPesanWarning_viDaftar('Tahun Harus Di isi !','Data Surat Kontrol');
    }else if(Ext.getCmp('combo_filter_rencanan_kontrol_igd').getValue() == ''){
        ShowPesanWarning_viDaftar('Jenis Harus Di isi !','Data Surat Kontrol');
    }
    else{
        var bulan = Ext.getCmp('dt_bulan_surat_kontrol').getValue();
        var tahun = Ext.getCmp('dt_tahun_surat_kontrol').getValue();
        var noka  = Ext.getCmp('txt_no_kartu_surat_kontrol').getValue();
        Ext.Ajax.request({
            method:'POST',
            url: baseURL + "index.php/rawat_jalan/functionCariBpjs/cari_data_rencana_kontrol_noka",
            params: {	
                no_kartu: noka,
                bulan   : bulan.format('m'),
                tahun   : tahun.format('Y'),
                jenis   : jenis_filter_rencana_kontrol,
            },
            success: function(imprt) 
            {
                var cst = Ext.decode(imprt.responseText);
                // console.log(cst.data.metaData.code);
                if(cst.data != null){
                    loadMask.hide();
                    // var cst1 = Ext.decode(cst.data);
                // console.log(cst1);
                    if (cst.data.metaData.code == 200) {					
                        var data = cst.data.response.list;

                        dataSourceSuratKontrol.loadData([],false);
                        for (var i = 0; i < data.length; i++) {					
                            dataSourceSuratKontrol.add(new dataSourceSuratKontrol.recordType(data[i]));
                        }
                    }else{
                        ShowPesanWarning_viDaftar(cst.data.metaData.message,'Get Surat Kontrol');
                        dataSourceSuratKontrol.loadData([],false);
                    }
                }else{
                    loadMask.hide();
                    ShowPesanWarning_viDaftar(cst.message,'Get Surat Kontrol');
                    dataSourceSuratKontrol.loadData([],false);      
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                ShowPesanWarning_viDaftar('Terjadi kesalahan dari server','Get Surat Kontrol');
                dataSourceSuratKontrol.loadData([],false);
            }
        });
    }
}
// ------------------------------------------------------------------------------------------------------------------

// -----------------------------Data Jadwal Dokter Bpjs--------------------------------------
// Egi 01 Maret 2022
var rowselectDataJadwalDokter;
var jenis_filter_jadwal_dokter;
var poli_jadwal_dokter;
function DataPanel7DataBPJS(){
	loaddatastoreUnitBPJS();
	var Field = ['kodeDokter','namaDokter','jadwalPraktek','kapasitas'];
	dataSourceJadwalDokter = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListDataJadwalDokter = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceJadwalDokter,
		columnLines: false,
		autoScroll: false,
		width : 820,
		height: 280, 
		x:10,
		y:60,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
				rowselectDataJadwalDokter = dataSourceJadwalDokter.getAt(row);
				console.log(rowselectDataJadwalDokter);
				}
			}
		}),
		listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowselectDataJadwalDokter = dataSourceJadwalDokter.getAt(ridx);
				console.log(rowselectDataJadwalDokter);
				
			},
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header:'No.'
			}),{
				id: 'colKodeDokter',
				header: 'Kode Dokter',
				dataIndex: 'kodeDokter',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colNamaDokter',
				header: 'Nama Dokter',
				dataIndex: 'namaDokter',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colJadwalPraktek',
				header: 'Jadwal Praktek',
				dataIndex: 'jadwalPraktek',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colKapasitas',
				header: 'Kapasitas',
				dataIndex: 'kapasitas',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			]),
		viewConfig: {
			forceFit: true
		}
	}); 
	var items ={
		title: 'Data Jadwal Praktek Dokter',
		layout: 'column',
		border: false,
		autoScroll: false,
		items:[
		{
			layout: 'absolute',
			bodyStyle: 'padding: 0px 0px 0px 0px',
			border: false,
			autoScroll: false,
			width: '100%',
			height: 350,
			anchor: '100% 100%',
			items:[
			{
				x: 25,
				y: 10,
				xtype: 'label',
				text: 'Poli'
			},{
				x: 60,
				y: 10,
				xtype: 'label',
				text: ' : '
			},
			McomboUnitJadwalDokter(),
			{
				x: 280,
				y: 10,
				xtype: 'label',
				text: 'Tanggal'
			},{
				x: 320,
				y: 10,
				xtype: 'label',
				text: ' : '
			},{
				xtype: 'datefield',
				fieldLabel: '',
				x: 330,
				y: 10,
				name: 'dt_tgl_jadwal_dokter',
				id: 'dt_tgl_jadwal_dokter',
				width: 120,
				value:now_viDaftar,
				format:'Y-M-d',
				hidden:false
			},
            {
				x: 460,
				y: 10,
				xtype: 'label',
				text: 'Jenis'
			},{
				x: 500,
				y: 10,
				xtype: 'label',
				text: ' : '
			},
            mCombJenisFilterJadwal(),

			{
				xtype: 'button',
				text: 'Cari',
				iconCls: 'find',
				x: 660,
				y: 10,
				id: 'btnCariJadwalDokter',
				handler: function (){
                    cariDataJadwalDokter();
				}
			},
			gridListDataJadwalDokter
			]
		}
		],
        // bbar: {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: 'Hapus Surat Kontrol',
        //             id: 'btnDeleteSEPHistorySEPs',
        //             handler: function () {
        //                 var data = rowselectDataJadwalDokter.data;
        //                 console.log(data);
        //                 Ext.MessageBox.confirm('Hapus Surat Kontrol', "Yakin Akan Hapus No Surat '" + data.noSuratKontrol + "' Sekarang ?", function (btn) {
        //                     if (btn === 'yes') {
        //                         var param = {
        //                             no_kontrol: data.noSuratKontrol,
        //                         }
        //                         $.ajax({
        //                             type: 'POST',
        //                             dataType: 'JSON',
        //                             url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rencana_kontrol/",
        //                             data: param,
        //                             success: function (cst) {
        //                                 if (cst.metaData.code == "200") {
        //                                     // winDelete.close();
        //                                     Ext.MessageBox.alert('Sukses', 'Berhasil Dihapus');
        //                                     cariDataSuratKontrol();
        //                                 } else {
        //                                     Ext.MessageBox.alert('Gagal', cst.metaData.message);
        //                                 }
        //                             },
        //                             error: function (jqXHR, exception) {
        //                                 loadMask.hide();
        //                                 Ext.MessageBox.alert('Gagal', 'Delete Surat Kontrol gagal.');
        //                             }
        //                         });
        //                     }
        //                 });
        //             }
        //         }
        //     ]
        // }
	};
	return items;
}
function mCombJenisFilterJadwal() {
    var cboJenisFilterJadwal = new Ext.form.ComboBox({
        id: 'combo_filter_jadwal_dokter_igd',
        x: 510,
        y: 10,
        typeAhead: true,
        triggerAction: 'all',
        width: 120,
        lazyRender: true,
        mode: 'local',
        selectOnFocus: true,
        tabIndex: 6,
        forceSelection: true,
        emptyText: '',
        anchor: '77%',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[1, 'SPRI'], [2, 'Rencana Kontrol']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        enableKeyEvents: true,
        listeners: {
            'select': function (a, b, c) {
                console.log(b.data);
                if (b.data.Id == 1) {
                    jenis_filter_jadwal_dokter = 1;
                } else {
                    jenis_filter_jadwal_dokter = 2;
                }
            },

        }
    });
    return cboJenisFilterJadwal;

}
function McomboUnitJadwalDokter() {
	// DISINI
	var Field = ['KODE', 'NAMA'];
	dsUnitBPJS = new WebApp.DataStore({ fields: Field });
	var cboUnitJadwal = new Ext.form.ComboBox({
		id: 'txt_poli_jadwal_dokter',
		x: 70,
		y: 10,
		width: 200,
		readOnly: false,
		hidden: false,
		//disable:true,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Poli...',
		fieldLabel: 'Poli',
		align: 'Right',
		tabIndex: 6,
		store: dsUnitBPJS,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				console.log(b.data);
				poli_jadwal_dokter=b.data.kode;
			}
		}
	});
	return cboUnitJadwal;
}
function cariDataJadwalDokter(){
    if(Ext.getCmp('txt_poli_jadwal_dokter').getValue() ==''){
        ShowPesanWarning_viDaftar('Poli Harus Di isi !','Data Jadwal Dokter');
    }else if(Ext.getCmp('combo_filter_jadwal_dokter_igd').getValue() ==''){
        ShowPesanWarning_viDaftar('Jenis Harus Di isi !','Data Jadwal Dokter');
    }else if(Ext.getCmp('dt_tgl_jadwal_dokter').getValue() ==''){
        ShowPesanWarning_viDaftar('Tanggal Harus Di isi !','Data Jadwal Dokter');
    }
    else{
        var tgl   = Ext.getCmp('dt_tgl_jadwal_dokter').getValue();
        Ext.Ajax.request({
            method:'POST',
            url: baseURL + "index.php/rawat_jalan/functionCariBpjs/cari_data_jadwal_dokter",
            params: {	
                tgl		: tgl.format('Y-m-d'),
                jenis   : jenis_filter_jadwal_dokter,
				poli	: poli_jadwal_dokter,
            },
            success: function(imprt) 
            {
                var cst = Ext.decode(imprt.responseText);
                // console.log(cst.data.metaData.code);
                if(cst.data != null){
                    loadMask.hide();
                    // var cst1 = Ext.decode(cst.data);
                // console.log(cst1);
                    if (cst.data.metaData.code == 200) {					
                        var data = cst.data.response.list;

                        dataSourceJadwalDokter.loadData([],false);
                        for (var i = 0; i < data.length; i++) {					
                            dataSourceJadwalDokter.add(new dataSourceJadwalDokter.recordType(data[i]));
                        }
                    }else{
                        ShowPesanWarning_viDaftar(cst.data.metaData.message,'Get Jadwal Dokter');
                        dataSourceJadwalDokter.loadData([],false);
                    }
                }else{
                    loadMask.hide();
                    ShowPesanWarning_viDaftar(cst.message,'Get Jadwal Dokter');
                    dataSourceJadwalDokter.loadData([],false);      
                }
            },
            error: function (jqXHR, exception) {
                loadMask.hide();
                ShowPesanWarning_viDaftar('Terjadi kesalahan dari server','Get Jadwal Dokter');
                dataSourceJadwalDokter.loadData([],false);
            }
        });
    }
}
// ------------------------------------------------------------------------------------------------------------------

// ------------------------------------------egi 22-12-2021----------------------------------------------------------
function getHistoryRujukanInternal(sep) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_history_rujukan_internal",
		params: {
			no_sep: sep
		},
		failure: function (o) {
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if (cst.totalrecords > 0) {
				dsDataRujukanInternal.removeAll();
				gridrujukaninternal.getView().refresh();
				for (var i = 0, iLen = cst.totalrecords; i < iLen; i++) {
					var records = new Array();
					records.push(new dsDataRujukanInternal.recordType(cst.listData[i]));
					dsDataRujukanInternal.add(records);
				}
				console.log(dsDataRujukanInternal);
			} else {
				ShowPesanInfo_viDaftar("Sep Tidak Ada Histori Rujukan Internal", "Informasi");
				formLookups_histori_rujukan_internal.close();
			}
		}
	});
}

function formLookup_histori_rujukan_internal(rowdata) {
	console.log(rowdata);
	var lebar = 730;
	formLookups_histori_rujukan_internal = new Ext.Window({
		id: 'formLookup_histori_rujukan_internal',
		name: 'formLookup_histori_rujukan_internal',
		title: 'Histori Rujukan Internal',
		closeAction: 'destroy',
		width: lebar,
		height: 400, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: [
			GetGridHistoriRujukanInternal(rowdata)], //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		},
		fbar: []
	});
	formLookups_histori_rujukan_internal.show();
}

function GetGridHistoriRujukanInternal(data) {
	var items = {
		layout: 'fit',
		flex: 2,
		labelAlign: 'Left',
		style: 'padding:4px 0px;',
		border: true,
		tbar: [
			{
				text: 'Cetak',
				id: 'btnCetakRujukanInternal',
				tooltip: nmLookup,
				disabled: false,
				iconCls: 'print',
				handler: function () {
					var line = gridrujukaninternal.getSelectionModel().selection.cell[0];
					var o = dsDataRujukanInternal.getRange()[line].data;
					console.log(line);
					console.log(o);
					console.log(o.nosep);
					var url = baseURL + "index.php/laporan/lap_sep_internal/cetak/" + o.nosep + "/" + o.tglrujukinternal + "/" + o.kdpolituj + "/" + line + "/true";
					new Ext.Window({
						title: 'SEP Internal',
						width: 900,
						height: 500,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_sep_internal/cetak/" + o.nosep + "/" + o.tglrujukinternal + "/" + o.kdpolituj + "/" + line + "/false", "_blank");
								}
							}
						]
					}).show();
				}
			}, '-', {
				xtype: 'button',
				text: 'Hapus',
				disabled: false,
				iconCls: 'remove',
				id: 'btnDeleteRujukanInternal',
				handler: function () {
					var line = gridrujukaninternal.getSelectionModel().selection.cell[0];
					var o = dsDataRujukanInternal.getRange()[line].data;
					console.log(line);
					console.log(o);
					console.log(o.nosep);
					Ext.MessageBox.confirm('Hapus SEP Internal', "Yakin Akan Hapus Rujukan Internal '" + o.nosurat + "' Sekarang ?", function (btn) {
						if (btn === 'yes') {
							var param = {
								nosep: o.nosep,
								nosurat: o.nosurat,
								tgl: o.tglrujukinternal,
								poli: o.tujuanrujuk
							}
							$.ajax({
								type: 'POST',
								dataType: 'JSON',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/hapusSEPInternal/",
								data: param,
								success: function (cst) {
									if (cst.metaData.code == "200") {
										Ext.MessageBox.alert('Sukses', 'Rujukan Internal berhasil dihapus');
										dsDataRujukanInternal.removeAt(line);
										gridrujukaninternal.getView().refresh();
									} else {
										Ext.MessageBox.alert('Gagal', cst.metaData.message);
									}
								},
								error: function (jqXHR, exception) {
									loadMask.hide();
									Ext.MessageBox.alert('Gagal', 'Delete Rujukan Internal gagal.');
									Ext.Ajax.request({
										url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
										params: {
											respon: jqXHR.responseText,
											keterangan: 'Error create SEP'
										}
									});
								}
							});
						}
					});
				}
			}
		],
		autoScroll: true,
		items: GetRujukanInternalSecond()
	}
	if (data != undefined) {
		getHistoryRujukanInternal(data.response.noSep);
	}
	return items;
}

function GetRujukanInternalSecond(data) {

	gridrujukaninternal = new Ext.grid.EditorGridPanel({
		store: dsDataRujukanInternal,
		columnLines: true,
		border: false,
		height: 350,
		autoScroll: true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function (sm, row, rec) {
					console.log(row);
					cell_select_index_rwj = rec;
					CellSelected_RujukanInternal = dsDataRujukanInternal.getAt(row);
					curentIndexsRujukanInternal = gridrujukaninternal.getSelectionModel().selection.cell[0];
				}
			}
		}),
		stripeRows: true,
		listeners: {
			beforeedit: function (plugin, edit) {
				if (plugin.field == 'nama_obat' && plugin.record.data.racikan == true) {
					return false;
				}
				if (plugin.field == 'satuan' && plugin.record.data.racikan !== true) {
					return false;
				}
			},
			rowdblclick: function (sm, ridx, cidx) {
				console.log(cell_select_index_rwj);
				if (cell_select_index_rwj == 14) {
					var line = gridrujukaninternal.getSelectionModel().selection.cell[0];
				}
				if (cell_select_index_rwj == 6) {
					var line = gridrujukaninternal.getSelectionModel().selection.cell[0];
				}
			}
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{		//5	
				dataIndex: 'nosep',
				header: 'No.SEP',
				sortable: true,
				width: 150
			}, {		//6	
				dataIndex: 'nosurat',
				header: 'No.Rujuk Internal',
				sortable: true,
				width: 150
			}, { //11
				dataIndex: 'tglrujukinternal',
				header: 'Tanggal',
				sortable: true,
				width: 80
			}, {
				dataIndex: 'tujuanrujuk',
				header: 'Tujuan',
				width: 80,
			}, {
				dataIndex: 'nmdokter',
				header: 'Nama DPJP',
				width: 150,
			}, {
				dataIndex: 'diagppk',
				header: 'Diagnosa',
				width: 80,
			}
		]),
	});
	return gridrujukaninternal;
}

// ------------------------------------------------------------------------------------------------------------------

// ------------------------------------------egi 20-12-2021-----------------------------------------------------------
function formLookup_pulang_sep(rowdata) {
  var lebar = 670;
  formLookups_pulang_sep = new Ext.Window({
      id: 'formLookup_pulang_sep_bpjs',
      name: 'formLookup_pulang_sep_bpjs',
      title: 'Pulang Sep',
      closeAction: 'destroy',
      width: lebar,
      height: 170, //575,
      resizable: false,
      constrain: true,
      autoScroll: false,
      iconCls: 'Studi_Lanjut',
      modal: true,
      items: panel_form_lookup_pulang_sep_bpjs(rowdata), //1
      listeners: {
          afterShow: function () {
              this.activate();

          }
      }
  });
  formLookups_pulang_sep.show();

  if (rowdata != undefined) {
      Ext.getCmp('btn_pulang_sep').enable();
      Ext.getCmp('txt_no_sep_pulang').setValue(rowdata.response.noSep);
  }
}

function mCombStatusPulangSEP() {
  var cboStatusPulangSEP = new Ext.form.ComboBox({
      id: 'combo_status_pulang_sep',
      x: 420,
      y: 5,
      typeAhead: true,
      triggerAction: 'all',
      lazyRender: true,
      mode: 'local',
      selectOnFocus: true,
      tabIndex: 6,
      forceSelection: true,
      emptyText: '',
      // anchor: '95%',
      width: 200,
      store: new Ext.data.ArrayStore({
          id: 1,
          fields: ['Id', 'displayText'],
          data: [[1, 'Atas Persetujuan Dokter'], [3, 'Atas Permintaan Sendiri'], [4, 'Meninggal'], [5, 'Lain-lain']]
      }),
      valueField: 'Id',
      displayField: 'displayText',
      enableKeyEvents: true,
      listeners: {
          'select': function (a, b, c) {
              console.log(b.data);
              status_pulang_sep = b.data.Id;
              if (b.data.Id != 4) {
                  Ext.getCmp('tgl_meninggal_sep').setValue('');
                  Ext.getCmp('txt_no_surat_meninggal_sep').setValue('');
                  Ext.getCmp('tgl_meninggal_sep').disable();
                  Ext.getCmp('txt_no_surat_meninggal_sep').disable();
              } else {
                  Ext.getCmp('tgl_meninggal_sep').enable();
                  Ext.getCmp('txt_no_surat_meninggal_sep').enable();
              }
          },

      }
  });
  return cboStatusPulangSEP;

}

function panel_form_lookup_pulang_sep_bpjs(rowdata) {
  console.log(rowdata);
  var formPanelPulangSep = new Ext.form.FormPanel({
      id: "myformpanel_pulang_sep_bpjs",
      bodyStyle: "padding:5px",
      labelAlign: "top",
      defaults: 
      { 
        //anchor: "100%"
      },
      columnWidth: .10,
      layout: 'absolute',
      border: false,
      // width: 500,
      height: 140,
      //anchor: '100% 100%',
      items: [
        {
          x: 10,
          y: 5,
          xtype: 'label',
          text: 'No SEP'
        },
        {
          x: 100,
          y: 5,
          xtype: 'label',
          text: ':'
        },
        {
          x: 110,
          y: 5,
          xtype: 'textfield',
          id: 'txt_no_sep_pulang',
          name: 'txt_no_sep_pulang',
          width: 200,
          disabled: true,
          allowBlank: false,
          readOnly: true,
          maxLength: 3,
          tabIndex: 1
        },
        {
          x: 320,
          y: 5,
          xtype: 'label',
          text: 'Status Pulang'
        },
        {
          x: 410,
          y: 5,
          xtype: 'label',
          text: ':'
        },
        mCombStatusPulangSEP(),
        {
          x: 10,
          y: 30,
          xtype: 'label',
          text: 'Tgl Pulang'
        },
        {
          x: 100,
          y: 30,
          xtype: 'label',
          text: ':'
        },
        {
          x: 110,
          y: 30,
          xtype: 'datefield',
          id: 'tgl_sep_pulang',
          name: 'tgl_sep_pulang',
          format: 'd/M/Y',
          readOnly: false,
          value: now,
          width: 200,
        },
        {
          x: 320,
          y: 60,
          xtype: 'label',
          text: 'SEP KLL'
        },
        {
          x: 410,
          y: 60,
          xtype: 'label',
          text: ':'
        },
        {
          xtype: 'checkbox',
          x: 420,
          y: 60,
          id: 'cb_kll',
          name: 'cb_kll',
          hidden: false,
          boxLabel: 'KLL',
          width: 200,
          listeners : {
            check: function (checkbox, isChecked) {
              if (isChecked === true) {
                Ext.getCmp('txt_no_lp_manual').enable();
              }else{										
                Ext.getCmp('txt_no_lp_manual').setValue('');
                Ext.getCmp('txt_no_lp_manual').disable();
              }
            }
          }
        },
        {
          x: 320,
          y: 30,
          xtype: 'label',
          text: 'Tgl Meninggal'
        },
        {
          x: 410,
          y: 30,
          xtype: 'label',
          text: ':'
        },
        {
          xtype: 'datefield',
          x: 420,
          y: 30,
          id: 'tgl_meninggal_sep',
          name: 'tgl_meninggal_sep',
          format: 'd/M/Y',
          hidden: false,
          disabled: true,
          value: '',
          width: 200,
        },
        {
          x: 10,
          y: 55,
          xtype: 'label',
          text: 'No Surat Meninggal'
        },
        {
          x: 100,
          y: 55,
          xtype: 'label',
          text: ':'
        },
        {
          x: 110,
          y: 55,
          xtype: 'textfield',
          id: 'txt_no_surat_meninggal_sep',
          name: 'txt_no_surat_meninggal_sep',
          width: 200,
          disabled: true,
          allowBlank: false,
          readOnly: false,
          maxLength: 3,
          tabIndex: 1
        },
        {
          x: 10,
          y: 85,
          xtype: 'label',
          text: 'No LP Manual'
        },
        {
          x: 100,
          y: 85,
          xtype: 'label',
          text: ':'
        },
        {
          x: 110,
          y: 80,
          xtype: 'textfield',
          id: 'txt_no_lp_manual',
          name: 'txt_no_lp_manual',
          width: 200,
          disabled: true,
          allowBlank: false,
          readOnly: false,
          maxLength: 3,
          tabIndex: 1
        },
      ],
      buttons:
      [
        {
            text: "Pulang",
            id: 'btn_pulang_sep',
            hidden: false,
            handler: function () {
                var tmptglmeninggal = '';
                var pulang ='';
                var gagal ='';
                if (Ext.getCmp('tgl_meninggal_sep').getValue() != '') {
                    tmptglmeninggal = Ext.getCmp('tgl_meninggal_sep').getValue().format("Y-m-d");
                }
                console.log(Ext.getCmp('cb_kll').getValue());
                console.log(Ext.getCmp('txt_no_lp_manual').getValue());
                if(Ext.getCmp('cb_kll').getValue() == true){
                    console.log("1");
                    if(Ext.getCmp('txt_no_lp_manual').getValue() == ''){
                        Ext.MessageBox.alert('Gagal', 'No LP Manual Tidak boleh kosong!');
                        gagal ='ok';
                    }else{
                        pulang ='ok';
                    }
                }

                if (Ext.getCmp('combo_status_pulang_sep').getValue() == '') {
                  console.log("2");
                  Ext.MessageBox.alert('Gagal', 'Status Pulang Belum dipilih!');
                  gagal ='ok';
                } else { 
                  pulang ='ok';
                }
                if (pulang == 'ok' && gagal == '' ) {
                    console.log("ini");
                    var param = {
                      sep: Ext.getCmp('txt_no_sep_pulang').getValue(),
                      tgl_pulang: Ext.getCmp('tgl_sep_pulang').getValue().format("Y-m-d"),
                      status_pulang: status_pulang_sep,
                      tgl_meninggal: tmptglmeninggal,
                      noLPManual: Ext.getCmp('txt_no_lp_manual').getValue(),
                      surat_meninggal: Ext.getCmp('txt_no_surat_meninggal_sep').getValue()
                    }
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        url: baseURL + "index.php/rawat_jalan/functionRWJ/updatePulangSEP/",
                        data: param,
                        success: function (cst) {
                            console.log(cst);
                            if (cst.metaData.code == "200") {
                                formLookups_pulang_sep.close();
                                Ext.MessageBox.alert('Sukses', 'Berhasil Pulang');
                            } else {
                                Ext.MessageBox.alert('Gagal', cst.metaData.message);
                            }
                        },
                        error: function (jqXHR, exception) {
                            loadMask.hide();
                            Ext.MessageBox.alert('Gagal', 'Pulang Pasien diPulangkan.');
                            dataSourceHistorySEP.removeAt(barisNoSEPHistorySEPRWI);
                            Ext.Ajax.request({
                                url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                                params: {
                                    respon: jqXHR.responseText,
                                    keterangan: 'Error pulang SEP'
                                }
                            });
                        }
                    });    
                }                      
            }
        },
        {
            text: "Batal",
            id: 'btn_pulang_batal_sep',
            hidden: false,
            handler: function () {
                // ajax_update_rujukan();
                formLookups_pulang_sep.close();
                //  formLookups_rujukan_bpjs.close();   
            }
        },
      ]
  });
  return formPanelPulangSep;
}

// ------------------Update Proses Rencana Kkontrol dan SPRI vclaim 2.0-------------------------------
// Egi 11 Maret 2022
function formLookup_insert_rencana_kontrol(rowdata) {
	console.log(rowdata);
	var lebar = 690;
	formLookups_insert_rencana_kontrol = new Ext.Window({
		id: 'formLookup_insert_rencana_kontrol_bpjs_igd',
		name: 'formLookup_insert_rencana_kontrol_bpjs_igd',
		title: 'Pembuatan Rencana Kontrol',
		closeAction: 'destroy',
		width: lebar,
		height: 250, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: [
			panel_form_lookup_insert_rencana_kontrol_bpjs(rowdata)], //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		},
		fbar: [
			{
				xtype: 'button',
				text: "Simpan Rencana Kontrol",
				id: 'btn_simpan_rencana_kontrol_igd_igd',
				hidden: false,
				handler: function () {
					ajax_insert_rencana_kontrol();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Update Rencana Kontrol",
				id: 'btn_update_rencana_kontrol_igd',
				hidden: false,
				handler: function () {
					ajax_update_rencana_kontrol();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Hapus Rencana Kontrol",
				id: 'btn_hapus_rencana_kontrol_igd',
				hidden: false,
				handler: function () {
					Ext.MessageBox.confirm('Hapus Rencana Kontrol', "Yakin Akan Hapus Rencana Kontrol '" + Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue() + "'  ?", function (btn) {
						if (btn === 'yes') {
							var param = {
								no_kontrol: Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue(),
								no_sep: Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').getValue()
							}
							$.ajax({
								type: 'POST',
								dataType: 'JSON',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rencana_kontrol/",
								data: param,
								success: function (cst) {
									if (cst.metaData.code == "200") {
										Ext.MessageBox.alert('Sukses', 'Rencana Kontrol berhasil dihapus');
										formLookups_insert_rencana_kontrol.close();

									} else {
										Ext.MessageBox.alert('Gagal', cst.metaData.message);
									}
								},
								error: function (jqXHR, exception) {
									loadMask.hide();
									Ext.MessageBox.alert('Gagal', 'Delete Rencana Kontrol gagal.');
									//dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
									Ext.Ajax.request({
										url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
										params: {
											respon: jqXHR.responseText,
											keterangan: 'Error hapus Rencana Kontrol'
										}
									});
								}
							});
						}
					});
				}
			},
			{
				xtype: 'button',
				text: "Cetak Rencana Kontrol",
				id: 'btn_cetak_rencana_kontrol',
				hidden: true,
				handler: function () {
					var jenis = "Rencana Kontrol";
					var no_surat = Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue();
          var no_sep = Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').getValue();
					console.log(no_surat);
					console.log(jenis);
					var url = baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + no_sep + "/" + no_surat + "/" + jenis + "/true";
					new Ext.Window({
						title: jenis,
						width: 900,
						height: 500,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + no_sep + "/" + no_surat + "/" + jenis + "/false", "_blank");
								}
							}
						]
					}).show();
					// formLookups_insert_rencana_kontrol.close();
				}
			}
		]
	});
	formLookups_insert_rencana_kontrol.show();
}

function ajax_update_rencana_kontrol() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/update_rencana_kontrol/",
		data: getParamRencanaKontrolUpdate(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				//console.log(resp1.response.sep.peserta.noKartu);
				if (jenis_kontrol_insert_rencana_kontrol == 1) {
					ShowPesanInfo_viDaftar('SPRI Berhasil Di Update', 'Update SPRI');

				} else {
					ShowPesanInfo_viDaftar('Rencana Kontrol Berhasil Di Update', 'Update Rencana Kontrol');
				}
				/*Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
				Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
				Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
				Ext.getCmp('txtNoSJP').focus();
					Ext.Msg.show({
						title: 'Cetak Rujukan',
						msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
						buttons: Ext.MessageBox.YESNO,
						fn: function (btn){
							if (btn == 'yes'){
								cetak_rujukan_direct();
							}
						},
						icon: Ext.MessageBox.QUESTION
					});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error update rujukan balik'
				}
			});
		}
	});
}


//vclaim 2.0 // hani 16-02-2022 Start SPRI di kepesertaan
function getParamRencanaKontrol() {

	if (jenis_kontrol_insert_rencana_kontrol == 1) {
		var tgl_kontrol_clean = Ext.getCmp('tgl_insert_spri').getValue();
		var tgl_surat_clean = Ext.getCmp('tgl_surat_insert_spri').getValue();
		var params =
		{
			no_sep: '',
			tgl_kontrol: tgl_kontrol_clean.format("Y-m-d"),
			tgl_surat: tgl_surat_clean.format("Y-m-d"),
			noka: Ext.getCmp('txt_no_kartu_insert_spri').getValue(),
			jenisKontrol: 1,
			kodePoli: select_poliKontrol,
			namaPoli: select_namapoliKontrol,
			kodeDokter: select_dokterKontrol,
			namaDokter: select_nama_dokterKontrol,
			diagnosa:'',
			user: 'Test WS'
		}	
	}else{
		var tgl_kontrol_clean = Ext.getCmp('tgl_insert_rencana_kontrol_igd').getValue();
		var tgl_surat_clean = Ext.getCmp('tgl_surat_insert_rencana_kontrol_igd').getValue();
		var params =
		{
			no_surat_kontrol: Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue(),
			no_sep: Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').getValue(),
			tgl_kontrol: tgl_kontrol_clean.format("Y-m-d"),
			tgl_surat: tgl_surat_clean.format("Y-m-d"),
			noka: Ext.getCmp('txt_no_kartu_insert_rencana_kontrol_igd').getValue(),
			jenisKontrol: 2,
			kodePoli: select_poliKontrol,
			namaPoli: select_namapoliKontrol,
			kodeDokter: select_dokterKontrol,
			namaDokter: select_nama_dokterKontrol,
			diagnosa: Ext.getCmp('txt_diagnosa_insert_rencana_kontrol_igd').getValue(),
			user: 'Test WS'
		}
	}
	return params;
}

function getParamRencanaKontrolUpdate() {
	if (jenis_kontrol_insert_rencana_kontrol == 1) {
		var tgl_kontrol_clean = Ext.getCmp('tgl_insert_spri').getValue();
		var tgl_surat_clean = Ext.getCmp('tgl_surat_insert_spri').getValue();
		var params =
		{
			no_surat_kontrol: Ext.getCmp('txt_no_kontrol_insert_spri').getValue(),
			no_sep_update: "",
			tgl_kontrol_update: tgl_kontrol_clean.format("Y-m-d"),
			tgl_surat_update: tgl_surat_clean.format("Y-m-d"),
			noka_update: Ext.getCmp('txt_no_kartu_insert_spri').getValue(),
			jenisKontrol_update: 1,
			kodePoli_update: select_poliKontrol,
			namaPoli_update: select_namapoliKontrol,
			kodeDokter_update: select_dokterKontrol,
			namaDokter_update: select_nama_dokterKontrol,
			// diagnosa_update: Ext.getCmp('txt_diagnosa_insert_spri').getValue(),
			user_update: 'Test WS'
		}

	}else{
		var tgl_kontrol_clean = Ext.getCmp('tgl_insert_rencana_kontrol_igd').getValue();
		var tgl_surat_clean = Ext.getCmp('tgl_surat_insert_rencana_kontrol_igd').getValue();
		var params =
		{
			no_surat_kontrol: Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue(),
			no_sep_update: Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').getValue(),
			tgl_kontrol_update: tgl_kontrol_clean.format("Y-m-d"),
			tgl_surat_update: tgl_surat_clean.format("Y-m-d"),
			noka_update: Ext.getCmp('txt_no_kartu_insert_rencana_kontrol_igd').getValue(),
			jenisKontrol_update: 2,
			kodePoli_update: select_poliKontrol,
			namaPoli_update: select_namapoliKontrol,
			kodeDokter_update: select_dokterKontrol,
			namaDokter_update: select_nama_dokterKontrol,
			diagnosa_update: Ext.getCmp('txt_diagnosa_insert_rencana_kontrol_igd').getValue(),
			user_update: 'Test WS'
		}

	}
		return params;
}
//vclaim 2.0 // hani 16-02-2022 end 

function ajax_insert_rencana_kontrol() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/insert_rencana_kontrol/",
		data: getParamRencanaKontrol(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				// rujukan_tipe='';
				// console.log(resp1);
				// if (jenis_kontrol_insert_rencana_kontrol == 1) {
				// 	Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').setValue(resp1.response.noSPRI);
				// } else {
				// 	Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').setValue(resp1.response.noSuratKontrol);
				// }
				// ShowPesanInfo_viDaftar('Rencana Kontrol/SPRI Berhasil Dibuat', 'Pembuatan Rencana Kontrol');
				// formLookups_insert_rencana_kontrol.close();
				
				//vclaim 2.0 // hani 16-02-2022 // SPRI di kepesertaan
				if (jenis_kontrol_insert_rencana_kontrol == 1) {
					Ext.getCmp('txt_no_kontrol_insert_spri').setValue(resp1.response.noSPRI);
					formLookups_insert_spri.close();
					ShowPesanInfo_viDaftar('SPRI Berhasil Dibuat', 'Pembuatan SPRI');

				} else {
					Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').setValue(resp1.response.noSuratKontrol);
					formLookups_insert_rencana_kontrol.close();
					ShowPesanInfo_viDaftar('Rencana Kontrol Berhasil Dibuat', 'Pembuatan Rencana Kontrol');

				}

				/*Ext.Msg.show({
					title: 'Cetak Rujukan',
					msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function (btn){
						if (btn == 'yes'){
							cetak_rujukan_direct();
						}
					},
					icon: Ext.MessageBox.QUESTION
				});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error create SEP'
				}
			});
		}
	});
}


function panel_form_lookup_insert_rencana_kontrol_bpjs(rowdata) {
	// loaddatastoreDokterKontrol();
	console.log(rowdata);
		Date.prototype.addDays= function(d){
		this.setDate(this.getDate() + d);
		return this;
		};

		var tomorrow = new Date().addDays(1);

	console.log(tomorrow); 
	console.log(rowdata);
	jenis_kontrol_insert_rencana_kontrol = 2;
	noka_buat_surat = rowdata.response.peserta.noKartu;
	sep_buat_surat = rowdata.response.noSep;
	tgl_kontrol = tomorrow;

	var formPanelInsertRencanaKontrol = new Ext.form.FormPanel({
		id: "myformpanel_insert_rencana_kontrol_bpjsigd",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .5,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 200,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'No SEP'
			},
			{
				x: 100,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_sep_insert_rencana_kontrol_igd',
				name: 'txt_no_sep_insert_rencana_kontrol_igd',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			/*{
				x: 242,
				y: 5,
				xtype: 'textfield',
				id: 'txt_unit_insert_rujukan',
				name: 'txt_unit_insert_rujukan',
				width: 70,
				disabled:true,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},*/

			{
				x: 320,
				y: 5,
				xtype: 'label',
				text: 'No Surat Kontrol'
			},
			{
				x: 410,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_kontrol_insert_rencana_kontrol_igd',
				name: 'txt_no_kontrol_insert_rencana_kontrol_igd',
				width: 220,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'Tgl Kontrol'
			},
			{
				x: 100,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 30,
				xtype: 'datefield',
				id: 'tgl_insert_rencana_kontrol_igd',
				name: 'tgl_insert_rencana_kontrol_igd',
				format: 'd/M/Y',
				readOnly: false,
				value: tomorrow,
				width: 200,
			},
			{
				x: 320,
				y: 30,
				xtype: 'label',
				text: 'Tgl Surat'
			},
			{
				x: 410,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 420,
				y: 30,
				id: 'tgl_surat_insert_rencana_kontrol_igd',
				name: 'tgl_surat_insert_rencana_kontrol_igd',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 220,

			},

			{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'No Kartu'
			},
			{
				x: 100,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 55,
				xtype: 'textfield',
				id: 'txt_no_kartu_insert_rencana_kontrol_igd',
				name: 'txt_no_kartu_insert_rencana_kontrol_igd',
				width: 170,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 280,
				y: 55,
				xtype: 'textfield',
				id: 'txt_sex_insert_rencana_kontrol_igd',
				name: 'txt_sex_insert_rencana_kontrol_igd',
				width: 30,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			// {
			// 	x: 320,
			// 	y: 55,
			// 	xtype: 'label',
			// 	text: 'Jenis Kontrol'
			// },
			// {
			// 	x: 410,
			// 	y: 55,
			// 	xtype: 'label',
			// 	text: ':'
			// },
			// mCombJenisInsertKontrol(),
			{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Nama'
			},
			{
				x: 100,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 80,
				xtype: 'textfield',
				id: 'txt_nama_peserta_insert_rencana_kontrol_igd',
				name: 'txt_nama_peserta_insert_rencana_kontrol_igd',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 80,
				xtype: 'label',
				text: 'Diagnosa'
			},
			{
				x: 410,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 80,
				xtype: 'textfield',
				id: 'txt_diagnosa_insert_rencana_kontrol_igd',
				name: 'txt_diagnosa_insert_rencana_kontrol_igd',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
			},

			{
				x: 320,
				y: 105,
				xtype: 'label',
				text: 'Unit/Poli'
			},
			{
				x: 410,
				y: 105,
				xtype: 'label',
				text: ':'
			},
			mCombPoliKontrol(),


			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Dokter'
			},
			{
				x: 100,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			mCombDokterKontrol(),
		],

	});

	if (rowdata != undefined) {
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_history_rencana_kontrol",
			params: { 
        noka: rowdata.response.peserta.noKartu, 
        jenis_kontrol: 2,
        no_sep: rowdata.response.noSep },
			failure: function (o) {
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				console.log(cst);
				if (cst.sudah_ada == true) {
					loaddatastorePoliKontrol();
					Ext.getCmp('btn_simpan_rencana_kontrol_igd_igd').hide();
					Ext.getCmp('btn_update_rencana_kontrol_igd').show();
					Ext.getCmp('btn_hapus_rencana_kontrol_igd').show();
					Ext.getCmp('btn_cetak_rencana_kontrol').show();
					Ext.getCmp('txt_no_kartu_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.nama);
					Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').setValue(rowdata.response.noSep);
					Ext.getCmp('txt_diagnosa_insert_rencana_kontrol_igd').setValue(rowdata.response.diagnosa).disable();
					Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').setValue(cst.listData.noSuratKontrol);
					Ext.getCmp('txt_sex_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.kelamin);
					Ext.getCmp('tgl_insert_rencana_kontrol_igd').setValue(cst.listData.tglRencanaKontrol);
					Ext.getCmp('tgl_surat_insert_rencana_kontrol_igd').setValue(cst.listData.tglTerbitKontrol).disable();
					jenis_kontrol_insert_rencana_kontrol = cst.listData.jnsKontrol;
					jenis_pelayanan = cst.listData.jnsPelayanan;
                    no_surat_kontrol = cst.listData.noSuratKontrol;
					select_poliKontrol = cst.listData.poliTujuan;
					select_namapoliKontrol = cst.listData.namaPoliTujuan;
					select_dokterKontrol = cst.listData.kodeDokter;
					select_nama_dokterKontrol = cst.listData.namaDokter;
					loaddatastoreDokterKontrol(select_poliKontrol);
					Ext.getCmp('cboPoliKontrol_ugd').setValue(select_namapoliKontrol);
					Ext.getCmp('cboDokterKontrol_ugd').setValue(select_nama_dokterKontrol);
					
						// Ext.getCmp('combo_kontrol_insert_rencanan_kontrol_igd').setValue('Rencana Kontrol').disable();
						Ext.getCmp('btn_update_rencana_kontrol_igd').setText('Update Rencana Kontrol');
						Ext.getCmp('btn_hapus_rencana_kontrol_igd').setText('Hapus Rencana Kontrol');
						Ext.getCmp('btn_cetak_rencana_kontrol').setText('Cetak Rencana Kontrol');
					
				} else {
					Ext.getCmp('btn_update_rencana_kontrol_igd').hide();
					Ext.getCmp('btn_hapus_rencana_kontrol_igd').hide();
					Ext.getCmp('btn_cetak_rencana_kontrol').hide();
					Ext.getCmp('btn_simpan_rencana_kontrol_igd_igd').show();
					Ext.getCmp('txt_no_kartu_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.nama);
					Ext.getCmp('txt_no_sep_insert_rencana_kontrol_igd').setValue(rowdata.response.noSep);
					Ext.getCmp('txt_diagnosa_insert_rencana_kontrol_igd').setValue(rowdata.response.diagnosa).disable();
					Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').setValue("");
					Ext.getCmp('txt_sex_insert_rencana_kontrol_igd').setValue(rowdata.response.peserta.kelamin);
					Ext.getCmp('tgl_surat_insert_rencana_kontrol_igd').disable();
					jenis_pelayanan = rowdata.response.jnsPelayanan;
                    no_surat_kontrol = rowdata.response.kontrol.noSurat;
					loaddatastorePoliKontrol();

				}
			}
		});


	}
	return formPanelInsertRencanaKontrol;
}

function mCombDokterKontrol() {
	var Field = ['kodeDokter', 'namaDokter'];
	dsDokterKontrol = new WebApp.DataStore({ fields: Field });
	var cboDokterKontrol = new Ext.form.ComboBox({
		id: 'cboDokterKontrol_ugd',
		x: 110,
		y: 105,
		width: 200,
		readOnly: false,
		hidden: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Dokter...',
		fieldLabel: 'Dokter Kontrol',
		align: 'Right',
		tabIndex: 6,
		store: dsDokterKontrol,
		valueField: 'kodeDokter',
		displayField: 'namaDokter',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_dokterKontrol = b.data.kodeDokter;
				select_nama_dokterKontrol = b.data.namaDokter;
			}
		}
	});
	return cboDokterKontrol;
}

function mCombPoliKontrol() {
	var Field = ['kodePoli', 'namaPoli'];
	dsPoliKontrol = new WebApp.DataStore({ fields: Field });
	var cboPoliKontrol = new Ext.form.ComboBox({
		id: 'cboPoliKontrol_ugd',
		x: 420,
		y: 105,
		width: 200,
		readOnly: false,
		hidden: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Poli Kontrol...',
		fieldLabel: 'Poli Kontrol',
		align: 'Right',
		tabIndex: 6,
		store: dsPoliKontrol,
		valueField: 'kodePoli',
		displayField: 'namaPoli',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_poliKontrol = b.data.kodePoli;
				select_namapoliKontrol = b.data.namaPoli;
				loaddatastoreDokterKontrol(b.data.kodePoli);
			}
		}
	});
	return cboPoliKontrol;
}

//vclaim 2.0 // hani 16-02-2022 // SPRI di Kepesertaan
function loaddatastoreDokterKontrol(tmp_poli_tujuan) {
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_jalan/functionRWJ/get_dokterKontrol",
        params: {
            jenis_kontrol: jenis_kontrol_insert_rencana_kontrol,
            poli: tmp_poli_tujuan,
            tgl_kontrol: tgl_kontrol
        },
        failure: function (o) {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            dsDokterKontrol.removeAll();
            for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
                var recs = [], recType = dsDokterKontrol.recordType;
                var o = cst['listData'][i];

                recs.push(new recType(o));
                dsDokterKontrol.add(recs);
            }
            console.log(dsDokterKontrol);
        }
    });
}

function mCombJenisInsertKontrol() {
	var cboJenisKontrolInsertKontrol = new Ext.form.ComboBox({
		id: 'combo_kontrol_insert_rencanan_kontrol_igd',
		x: 420,
		y: 55,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		tabIndex: 6,
		forceSelection: true,
		emptyText: '',
		anchor: '95%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, 'SPRI'], [1, 'Rencanan Kontrol']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				console.log(b.data);
				if (b.data.Id == 0) {
					jenis_kontrol_insert_rencana_kontrol = 1;
				} else {
					jenis_kontrol_insert_rencana_kontrol = 2;
				}
				loaddatastorePoliKontrol();
			},

		}
	});
	return cboJenisKontrolInsertKontrol;

}

function loaddatastorePoliKontrol(tmp_poli_tujuan) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_poliKontrol",
		params: {
			jenis_kontrol: jenis_kontrol_insert_rencana_kontrol,
			noka: noka_buat_surat,
			sep: sep_buat_surat,
			tgl_kontrol: tgl_kontrol,
			jenis_pelayanan: jenis_pelayanan,
            no_surat_kontrol: no_surat_kontrol
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsPoliKontrol.removeAll();
			if (cst.success == true) {
				for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
					var recs = [], recType = dsPoliKontrol.recordType;
					var o = cst['listData'][i];

					recs.push(new recType(o));
					dsPoliKontrol.add(recs);
				}
				//vclaim 2.0 // hani 21-02-22 // Buat Rencana KOntrol Khusus Rawat Inap
				console.log(jenis_kontrol_insert_rencana_kontrol);
                if (jenis_pelayanan == 'Rawat Inap' && jenis_kontrol_insert_rencana_kontrol == 2) {
                    console.log("iniaaaaaaa " + jenis_kontrol_insert_rencana_kontrol);
                    if (cst.poli_rwi != '') {
                    console.log("iniaaaaaaa 2222222222 " + jenis_kontrol_insert_rencana_kontrol);
                        Ext.getCmp('cboPoliKontrol_ugd').setValue(cst.poli_rwi);
                        Ext.getCmp('cboPoliKontrol_ugd').disable();
                        loaddatastoreDokterKontrol(cst.poli_rwi);
                        select_poliKontrol = cst.poli_rwi;
                        select_namapoliKontrol = cst.nama_poli;
                    }
                } else {
                    Ext.getCmp('cboPoliKontrol_ugd').setValue(select_poliKontrol);
                    Ext.getCmp('cboPoliKontrol_ugd').enable();
                }
				console.log(dsPoliKontrol);
			} else {
				ShowPesanWarning_viDaftar(cst.pesan, 'Warning');
			}
		}
	});
}
// ---------------------------------------------------------------------------------------------------

function formLookup_insert_rujukan_balik(rowdata) {
	console.log(rowdata);
	var lebar = 690;
	formLookups_insert_rujukan_balik = new Ext.Window({
		id: 'formLookup_insert_rujukan_balik_bpjs_igd',
		name: 'formLookup_insert_rujukan_balik_bpjs_igd',
		title: 'Pembuatan Rujukan Balik',
		closeAction: 'destroy',
		width: lebar,
		height: 400, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: [
			panel_form_lookup_insert_rujukan_balik_bpjs(rowdata),
			GetDTLTRGrid_PRB(rowdata)], //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		},
		fbar: [
			{
				xtype: 'button',
				text: "Simpan Rujukan",
				id: 'btn_simpan_rujukan_balik_igd_igd',
				hidden: false,
				handler: function () {
					ajax_insert_rujukan_balik();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Update Rujukan",
				id: 'btn_update_rujukan_balik_igd',
				hidden: false,
				handler: function () {
					ajax_update_rujukan_balik();
					//	formLookups_rujukan_bpjs.close();	
				}
			},
			{
				xtype: 'button',
				text: "Hapus Rujukan",
				id: 'btn_hapus_rujukan_balik_igd',
				hidden: false,
				handler: function () {
					Ext.MessageBox.confirm('Hapus Rujukan Balik', "Yakin Akan Hapus No SRB '" + Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').getValue() + "'  ?", function (btn) {
						if (btn === 'yes') {
							var param = {
								no_rujukan: Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').getValue(),
								no_sep: Ext.getCmp('txt_no_sep_insert_rujukan_balik_igd').getValue()
							}
							$.ajax({
								type: 'POST',
								dataType: 'JSON',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rujukan_balik/",
								data: param,
								success: function (cst) {
									if (cst.metaData.code == "200") {
										Ext.MessageBox.alert('Sukses', 'Rujukan Balik berhasil dihapus');
										formLookups_insert_rujukan_balik.close();

									} else {
										Ext.MessageBox.alert('Gagal', cst.metaData.message);
									}
								},
								error: function (jqXHR, exception) {
									loadMask.hide();
									Ext.MessageBox.alert('Gagal', 'Delete Rujukan Balik gagal.');
									//dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
									Ext.Ajax.request({
										url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
										params: {
											respon: jqXHR.responseText,
											keterangan: 'Error hapus Rujukan Balik'
										}
									});
								}
							});
						}
					});
				}
			},
			{
				xtype: 'button',
				text: "Cetak Rujukan",
				id: 'btn_cetak_rujukan__igd',
				hidden: true,
				handler: function () {
					var jenis = Ext.getCmp('combo_kontrol_insert_rencanan_kontrol_igd').getValue();
					var no_surat = Ext.getCmp('txt_no_kontrol_insert_rencana_kontrol_igd').getValue();
					console.log(no_surat);
					console.log(jenis);
					var url = baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + noSEPHistorySEPRWJ + "/" + no_surat + "/" + jenis + "/true";
					new Ext.Window({
						title: jenis,
						width: 900,
						height: 500,
						constrain: true,
						modal: false,
						html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
						tbar: [
							{
								xtype: 'button',
								text: '<i class="fa fa-print"></i> Cetak',
								handler: function () {
									window.open(baseURL + "index.php/laporan/lap_rencana_kontrol/cetak/" + noSEPHistorySEPRWJ + "/" + no_surat + "/" + jenis + "/false", "_blank");
								}
							}
						]
					}).show();
					// formLookups_insert_rencana_kontrol.close();
				}
			}
		]
	});
	formLookups_insert_rujukan_balik.show();
}

function ajax_insert_rujukan_balik() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/insert_rujukan_balik/",
		data: getParamRujukanBalik(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				// rujukan_tipe='';
				// console.log(resp1);
				Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').setValue(resp1.response.noSRB);
				ShowPesanInfo_viDaftar('Rujukan Balik Berhasil Dibuat', 'Pembuatan Rujukan Balik');
				formLookups_insert_rujukan_balik.close();
				/*Ext.Msg.show({
					title: 'Cetak Rujukan',
					msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function (btn){
						if (btn == 'yes'){
							cetak_rujukan_direct();
						}
					},
					icon: Ext.MessageBox.QUESTION
				});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error create SEP'
				}
			});
		}
	});
}

function getParamRujukanBalik() {
	var tgl_dirujuk_clean = Ext.getCmp('tgl_rujukan_insert_rujukan_balik_igd').getValue()
	var params =
	{
		no_sep: Ext.getCmp('txt_no_sep_insert_rujukan_balik_igd').getValue(),
		tgl_rujukan: tgl_dirujuk_clean.format("Y-m-d"),
		noka: Ext.getCmp('txt_no_kartu_insert_rujukan_balik_igd').getValue(),
		alamat: Ext.getCmp('txt_alamat_insert_rujukan_balik_igd').getValue(),
		email: Ext.getCmp('txt_email_insert_rujukan_balik_igd').getValue(),
		programPRB: select_programPRB,
		kodeDPJP: select_dokterPRB,
		keterangan: Ext.getCmp('txt_keterangan_insert_rujukan_balik_igd').getValue(),
		saran: Ext.getCmp('txt_saran_rujukan_balik_insert_rujukan_igd').getValue(),
		user: 'Test WS'
	}
	params['jumlah'] = dsDataGrdObat_PRB.getCount();
	var line = 0;
	for (var i = 0; i < dsDataGrdObat_PRB.getCount(); i++) {
		var o = dsDataGrdObat_PRB.data.items[i].data;
		var signa = o.signa.split("x");
		params['kode-' + line] = o.kode;
		params['nama-' + line] = o.nama;
		params['jml-' + line] = o.jml;
		params['signa1-' + line] = signa[0];
		params['signa2-' + line] = signa[1];
		line++;
	}
	return params;
}

function getParamRujukanBalikUpdate() {
	var tgl_dirujuk_clean = Ext.getCmp('tgl_rujukan_insert_rujukan_balik_igd').getValue()
	var params =
	{
		no_srb: Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').getValue(),
		no_sep_update: Ext.getCmp('txt_no_sep_insert_rujukan_balik_igd').getValue(),
		tgl_rujukan_update: tgl_dirujuk_clean.format("Y-m-d"),
		noka_update: Ext.getCmp('txt_no_kartu_insert_rujukan_balik_igd').getValue(),
		alamat_update: Ext.getCmp('txt_alamat_insert_rujukan_balik_igd').getValue(),
		email_update: Ext.getCmp('txt_email_insert_rujukan_balik_igd').getValue(),
		programPRB_update: select_programPRB,
		kodeDPJP_update: select_dokterPRB,
		namaDPJP_update: select_nama_dokterPRB,
		keterangan_update: Ext.getCmp('txt_keterangan_insert_rujukan_balik_igd').getValue(),
		saran_update: Ext.getCmp('txt_saran_rujukan_balik_insert_rujukan_igd').getValue(),
		user_update: 'Test WS'
	}
	params['jumlah_update'] = dsDataGrdObat_PRB.getCount();
	var line = 0;
	for (var i = 0; i < dsDataGrdObat_PRB.getCount(); i++) {
		var o = dsDataGrdObat_PRB.data.items[i].data;
		var signa = o.signa.split("x");
		params['kode_update-' + line] = o.kode;
		params['nama_update-' + line] = o.nama;
		params['jml_update-' + line] = o.jml;
		params['signa1_update-' + line] = signa[0];
		params['signa2_update-' + line] = signa[1];
		line++;
	}
	return params;
}

function ajax_update_rujukan_balik() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/update_rujukan_balik/",
		data: getParamRujukanBalikUpdate(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				//console.log(resp1.response.sep.peserta.noKartu);
				ShowPesanInfo_viDaftar('Rujukan Balik Berhasil Di Update', 'Update Rujukan');
				/*Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
				Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
				Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
				Ext.getCmp('txtNoSJP').focus();
					Ext.Msg.show({
						title: 'Cetak Rujukan',
						msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
						buttons: Ext.MessageBox.YESNO,
						fn: function (btn){
							if (btn == 'yes'){
								cetak_rujukan_direct();
							}
						},
						icon: Ext.MessageBox.QUESTION
					});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error update rujukan balik'
				}
			});
		}
	});
}

function GetDTLTRGrid_PRB(data) {
	var items = {
		layout: 'fit',
		flex: 2,
		labelAlign: 'Left',
		style: 'padding:4px 0px;',
		border: true,
		tbar: [
			{
				text: 'Tambah Obat',
				id: 'btnAddObat',
				tooltip: nmLookup,
				disabled: false,
				iconCls: 'find',
				handler: function () {
					// if (Ext.getCmp('txtTmpKdCustomer').getValue() == '') {
					// ShowPesanErrorResepRWJ('Jenis pasien tidak boleh kosong!', 'Error');
					// } else {
					// if (Ext.getCmp('txtTmpStatusPost').getValue() != 1) {
					var records = new Array();
					// Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
					// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
					// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
					// Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
					// var kd_customer = Ext.getCmp('txtTmpKdCustomer').getValue();
					// if (kd_customer === '' || kd_customer === 'Kelompok Pasien') {
					// } else {
					// 	getAdm(kd_customer);
					// }
					// setTimeout(function () {
					records.push(new dsDataGrdObat_PRB.recordType());
					dsDataGrdObat_PRB.add(records);
					var row = dsDataGrdObat_PRB.getCount() - 1;
					gridPRB.startEditing(row, 2);
					// }, 200);
					// }

					// }
				}
			}, '-', {
				xtype: 'button',
				text: 'Hapus',
				disabled: false,
				iconCls: 'remove',
				id: 'btnDelete_viApotekResepRWJ',
				handler: function () {
					var line = gridPRB.getSelectionModel().selection.cell[0];
					var o = dsDataGrdObat_PRB.getRange()[line].data;
					if (dsDataGrdObat_PRB.getCount() > 1) {
						// 	Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function (button) {
						// 		if (button == 'yes') {
						// 			console.log(o);
						// 			if (dsDataGrdObat_PRB.getRange()[line].data.no_out != undefined) {
						// 				Ext.Ajax.request({
						// 					url: baseURL + "index.php/apotek/functionAPOTEK/hapusBarisGridResepRWJ",
						// 					params: {
						// 						no_out: o.no_out,
						// 						tgl_out: o.tgl_out,
						// 						kd_prd: o.kd_prd,
						// 						kd_milik: o.kd_milik,
						// 						no_urut: o.no_urut,
						// 						racikan: o.racikan,
						// 					},
						// 					failure: function (o) {
						// 						ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
						// 					},
						// 					success: function (o) {
						// 						var cst = Ext.decode(o.responseText);
						// 						if (cst.success === true) {
						// 							dsDataGrdObat_PRB.removeAt(line);
						// 							gridPRB.getView().refresh();
						// 							hasilJumlah();
						// 							Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
						// 							Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();

						// 						} else {
						// 							ShowPesanErrorResepRWJ('Gagal melakukan penghapusan', 'Error');
						// 						}
						// 					}
						// 				});

						// 			} else {
						dsDataGrdObat_PRB.removeAt(line);
						gridPRB.getView().refresh();
						// 				hasilJumlah();
						// 				// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
						// 				// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
						// 			}
						// 			if (o.racikan === true || o.racikan == 'true') {
						// 				// alert();
						// 				var tmp_total_racik = parseInt(Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWJ').getValue()) - 1;
						// 				Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWJ').setValue(tmp_total_racik);
						// 				Ext.Ajax.request({
						// 					url: baseURL + "index.php/apotek/functionAPOTEK/hitung_nilai_adm_racik",
						// 					params: { jml_grup_racik: tmp_total_racik, jml_item_racik: tmp_total_racik },
						// 					failure: function (o) {
						// 						ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
						// 					},
						// 					success: function (o) {
						// 						var cst = Ext.decode(o.responseText);
						// 						if (cst.success === true) {
						// 							Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').setValue(cst.nilai_adm_racik);
						// 							hasilJumlah();
						// 						}
						// 						else {
						// 							ShowPesanWarningResepRWJ('Gagal mendapatkan konfigurasi perhitungan racik!', 'Warning');

						// 						};
						// 					}
						// 				});
						// 			}
						// 		}
						// 	});
					} else {
						ShowPesanErrorResepRWJ('Data tidak bisa dihapus karena minimal resep 1 obat', 'Error');
					}
				}
			}
		],
		autoScroll: true,
		items: GetDTLTRGrid_PRBSecond()
	}
	return items;
}

function GetDTLTRGrid_PRBSecond(data) {

	gridPRB = new Ext.grid.EditorGridPanel({
		store: dsDataGrdObat_PRB,
		columnLines: true,
		border: false,
		height: 100,
		autoScroll: true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function (sm, row, rec) {
					console.log(row);
					cell_select_index_rwj = rec;
					CellSelected_viApotekResepRWJ = dsDataGrdObat_PRB.getAt(row);
					currentKdPrdRacik_ResepRWJ = CellSelected_viApotekResepRWJ.data.kd_prd;
					currentNamaObatRacik_ResepRWJ = CellSelected_viApotekResepRWJ.data.nama_obat;
					// currentHargaRacik_ResepRWJ = CellSelected_viApotekResepRWJ.data.harga_jual;
					currentJumlah_ResepRWJ = CellSelected_viApotekResepRWJ.data.jml;
					// currentHargaJualObat = CellSelected_viApotekResepRWJ.data.harga_jual;
					// currentCitoNamaObat = CellSelected_viApotekResepRWJ.data.nama_obat;
					// currentCitoKdPrd = CellSelected_viApotekResepRWJ.data.kd_prd;
					curentIndexsSelection_ResepRWJ = gridPRB.getSelectionModel().selection.cell[0];
				}
			}
		}),
		stripeRows: true,
		listeners: {
			beforeedit: function (plugin, edit) {
				if (plugin.field == 'nama_obat' && plugin.record.data.racikan == true) {
					return false;
				}
				if (plugin.field == 'satuan' && plugin.record.data.racikan !== true) {
					return false;
				}
			},
			rowdblclick: function (sm, ridx, cidx) {
				console.log(cell_select_index_rwj);
				if (cell_select_index_rwj == 14) {
					var line = gridPRB.getSelectionModel().selection.cell[0];
					// showSignaturePRB(line);
				}
				if (cell_select_index_rwj == 6) {
					var line = gridPRB.getSelectionModel().selection.cell[0];
					// if (dsDataGrdObat_PRB.data.items[line].data.racikan == true) {
					// showRacikRWJ(line, true);
					// }
				}
			}
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{		//5	
				dataIndex: 'kode',
				header: 'Kode Obat',
				sortable: true,
				width: 100
			}, {		//6	
				dataIndex: 'nama',
				header: 'Nama Obat',
				sortable: true,
				width: 280,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents: true,
					listeners: {
						keyDown: function (a, b, c) {
							if (b.getKey() == 13) {
								var line = gridPRB.getSelectionModel().selection.cell[0];
								if (a.getValue().length < 1) {
									if (a.getValue().length != 0) {
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Kriteria Pencarian Obat Tidak Boleh Kosong!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok') {
												}
											}
										});
									}
								} else {
									// PencarianLookupResep = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
									// FocusExitResepRWJ = false;
									LookUpSearchListGetObat_resepRWJ(a.getValue());
								}
							}
						}
					}
				})
			}, { //11
				dataIndex: 'jml',
				header: 'Jumlah',
				sortable: true,
				width: 80,
				// align: 'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					enableKeyEvents: true,
					listeners: {

						keyDown: function (a, b, c) {
							if (b.getKey() == 13) {
								var line = gridPRB.getSelectionModel().selection.cell[0];
								// 	if (dsDataGrdObat_PRB.data.items[line].data.racikan == false) {
								// 		var line = this.index;
								// 		if (a.getValue() == '') {
								// 			Ext.Msg.show({
								// 				title: 'Perhatian',
								// 				msg: 'Qty obat belum di isi!',
								// 				buttons: Ext.MessageBox.OK,
								// 				fn: function (btn) {
								// 					if (btn == 'ok') {
								// 						var thisrow = dsDataGrdObat_PRB.getCount() - 1;
								// 						gridPRB.startEditing(thisrow, 9);
								// 					}
								// 				}
								// 			});
								// 		} else {
								// 			statusRacikan_ResepRWJ = 0;
								// 			var o = dsDataGrdObat_PRB.getRange()[line].data;
								// 			var tmp_jml_stok = 0;
								// 			var tmp_min_stok = 0;
								// 			Ext.Ajax.request({
								// 				url: baseURL + "index.php/apotek/functionAPOTEK/getStokObat",
								// 				params: { kd_prd: o.kd_prd, kd_milik: o.kd_milik },
								// 				failure: function (o) {
								// 					var cst = Ext.decode(o.responseText);
								// 				},
								// 				success: function (o) {
								// 					var cst = Ext.decode(o.responseText);
								// 					if (cst.success == true) {
								// 						tmp_jml_stok = cst.jml_stok;
								// 						tmp_min_stok = cst.min_stok;
								// 						console.log(parseFloat(tmp_jml_stok));
								// 						console.log(parseFloat(tmp_min_stok));
								// 						if (parseFloat(tmp_min_stok) == parseFloat(tmp_jml_stok)) {
								// 							Ext.Msg.show({
								// 								title: 'Perhatian',
								// 								msg: 'Sisa stok sudah mencapai atau melebihi minimum stok!',
								// 								buttons: Ext.MessageBox.OK,
								// 								fn: function (btn) {
								// 									if (btn == 'ok') {
								// 										o.jml = a.getValue();
								// 										hasilJumlah();
								// 										var thisrow = dsDataGrdObat_PRB.getCount() - 1;
								// 										gridPRB.startEditing(thisrow, 9);
								// 									}
								// 								}
								// 							});
								// 						} else if (parseFloat(a.getValue()) > parseFloat(tmp_jml_stok)) {
								// 							Ext.Msg.show({
								// 								title: 'Perhatian',
								// 								msg: 'Qty melebihi sisa stok tersedia,  Stok hanya tersedia ' + cst.jml_stok + '!',
								// 								buttons: Ext.MessageBox.OK,
								// 								fn: function (btn) {
								// 									if (btn == 'ok') {
								// 										o.jml = tmp_jml_stok;
								// 										hasilJumlah();
								// 										var thisrow = dsDataGrdObat_PRB.getCount() - 1;
								// 										gridPRB.startEditing(thisrow, 9);
								// 									}
								// 								}
								// 							});
								// 						} else {
								// 							o.jml = a.getValue();
								// 							hasilJumlah();
								// showSignaturePRB(line);
								// 						}
								// 						Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
								// 					}
								// 				}
								// 			})
								// 		}
								// 	} else {
								showSignaturePRB(line);
								// 	}
							}
						},

						focus: function (a) {
							this.index = gridPRB.getSelectionModel().selection.cell[0]
						}
					},

				})
			}, {
				dataIndex: 'signa',
				header: 'Signa',
				width: 100,
			}
		]),
	});
	return gridPRB;
}

function showSignaturePRB(line) {
	var signa = dsDataGrdObat_PRB.data.items[line].data.signa;
	var spSigna = [];
	if (signa != undefined) {
		spSigna = signa.split('x');
	}
	var signa1 = '';
	var signa2 = '';
	if (spSigna.length > 1) {
		signa1 = spSigna[0];
		signa2 = spSigna[1];
	}
	var showSignatureIGD = new Ext.Window({
		title: 'Signature',
		closeAction: 'destroy',
		modal: true,
		layout: 'fit',
		constraint: true,
		items: [
			{
				xtype: 'panel',
				layout: 'column',
				height: 30,
				style: 'background:#ffffff;',
				bodyStyle: 'padding: 4px;',
				width: 200,
				border: false,
				items: [
					{
						xtype: 'displayfield',
						value: 'Signature :&nbsp;'
					}, {
						xtype: 'numberfield',
						id: 'txtSignatureIGD1',
						width: 50,
						value: signa1,
						listeners: {
							'specialkey': function (a) {
								if (Ext.EventObject.getKey() === 13) {
									if (a.getValue() != '' && a.getValue() != 0) {
										Ext.getCmp('txtSignatureIGD2').focus();
									}
								}
							}
						}
					}, {
						xtype: 'displayfield',
						value: '&nbsp;X&nbsp;'
					}, {
						xtype: 'numberfield',
						id: 'txtSignatureIGD2',
						width: 50,
						value: signa2,
						listeners: {
							'specialkey': function (a) {
								if (Ext.EventObject.getKey() === 13) {
									if (a.getValue() != '' && a.getValue() != 0) {
										dsDataGrdObat_PRB.getRange()[line].set('signa', Ext.getCmp('txtSignatureIGD1').getValue() + 'x' + a.getValue());
										showSignatureIGD.close();
										gridPRB.startEditing(line, 15);
									}
								}
							}
						}
					}
				]
			}
		],
		fbar: [
			{
				text: 'Ok',
				handler: function () {
					var err = false;
					if (Ext.getCmp('txtSignatureIGD1').getValue() == '' || Ext.getCmp('txtSignatureIGD1').getValue() == 0) {
						err = true;
						Ext.getCmp('txtSignatureIGD1').focus();
					}
					if (err == false && (Ext.getCmp('txtSignatureIGD2').getValue() == '' || Ext.getCmp('txtSignatureIGD2').getValue() == 0)) {
						err = true;
						Ext.getCmp('txtSignatureIGD2').focus();
					}
					if (err == false) {
						dsDataGrdObat_PRB.getRange()[line].set('signa', Ext.getCmp('txtSignatureIGD1').getValue() + 'x' + Ext.getCmp('txtSignatureIGD2').getValue());
						showSignatureIGD.close();
						gridPRB.startEditing(line, 15);
					}
				}
			}
		]
	}).show();
	Ext.getCmp('txtSignatureIGD1').focus(true, 100);
}

function LookUpSearchListGetObat_resepRWJ(nama_obat, resep) {
	WindowLookUpSearchListGetObat_ResepRWJ = new Ext.Window({
		id: 'pnlLookUpSearchListGetObat_ResepRWJ',
		title: 'List Pencarian Obat',
		width: 500,
		height: 200,
		border: false,
		resizable: false,
		plain: true,
		iconCls: 'icon_lapor',
		modal: true,
		items: [
			gridListObatPencarianResepRWJ(resep)
		],
		listeners: {
			afterShow: function () {
				this.activate();
			},
			close: function () {
				// if (FocusExitResepRWJ == false) {
				// if (resep != undefined && resep == 'Y') {
				// 	var line = gridPRB.getSelectionModel().selection.cell[0];
				// 	gridPRB.startEditing(line, 2);
				// } else {

				// }
				// }
			}
		}
	}
	);
	WindowLookUpSearchListGetObat_ResepRWJ.show();
	getListObatSearch_ResepRWJ(nama_obat, '');
}

function gridListObatPencarianResepRWJ(racik) {
	var fldDetail = ['kode', 'nama'];
	dsGridListPencarianObat_ResepRWJ = new WebApp.DataStore({ fields: fldDetail });
	GridListPencarianObatColumnModel = new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			dataIndex: 'kode',
			header: 'Kode',
			width: 50,
			menuDisabled: true,
		}, {
			dataIndex: 'nama',
			header: 'Nama Obat',
			width: 200,
			menuDisabled: true,
		}
	]);


	GridListPencarianObat_ResepRWJ = new Ext.grid.EditorGridPanel({
		id: 'GrdListPencarianObat_ResepRWJ',
		stripeRows: true,
		width: 610,
		height: 170,
		store: dsGridListPencarianObat_ResepRWJ,
		border: true,
		frame: false,
		autoScroll: true,
		cm: GridListPencarianObatColumnModel,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					currentRowSelectionPencarianObatPRB = undefined;
					currentRowSelectionPencarianObatPRB = dsGridListPencarianObat_ResepRWJ.getAt(row);
				}
			}
		}),
		listeners: {
			rowclick: function ($this, rowIndex, e) {
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
			},
			celldblclick: function (gridView, htmlElement, columnIndex, dataRecord) {
			},
			'keydown': function (e) {
				if (e.getKey() == 13) {
					console.log(gridPRB);
					var line = gridPRB.getSelectionModel().selection.cell[0];
					dsDataGrdObat_PRB.getRange()[line].data.nama = currentRowSelectionPencarianObatPRB.data.nama;
					dsDataGrdObat_PRB.getRange()[line].data.kode = currentRowSelectionPencarianObatPRB.data.kode;

					gridPRB.getView().refresh();
					FocusExitResepRWJ = true;
					WindowLookUpSearchListGetObat_ResepRWJ.close();
					console.log(line);
					gridPRB.startEditing(line, 3);
				}
			},
		},
		viewConfig: { forceFit: true }
	});
	return GridListPencarianObat_ResepRWJ;
}

function getListObatSearch_ResepRWJ(nama_obat, kd_milik) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionCariBpjs/getListObat",
		params: {
			nama_obat: nama_obat
		},
		failure: function (o) {
			ShowPesanErrorResepRWJ('Error menampilkan pencarian obat. Hubungi Admin!', 'Error');
		},
		success: function (o) {
			dsGridListPencarianObat_ResepRWJ.removeAll();
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if (cst.success === true) {
				if (cst.totalrecords == 0) {
					if (PencarianLookupResep == true) {
						Ext.Msg.show({
							title: 'Information',
							msg: 'Tidak ada obat yang sesuai atau kriteria obat kurang!',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok') {
									console.log('ok');
									var line_1 = gridPRB.getSelectionModel().selection.cell[0];
									console.log(line_1);
									gridPRB.startEditing(line_1, 1);
									WindowLookUpSearchListGetObat_ResepRWJ.close();
								}
							}
						});
					}
				} else {
					var recs = [],
						recType = dsGridListPencarianObat_ResepRWJ.recordType;
					for (var i = 0; i < cst.ListDataObj.length; i++) {
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridListPencarianObat_ResepRWJ.add(recs);
					GridListPencarianObat_ResepRWJ.getView().refresh();
					GridListPencarianObat_ResepRWJ.getSelectionModel().selectRow(0);
					GridListPencarianObat_ResepRWJ.getView().focusRow(0);
				}

			} else {
				ShowPesanErrorResepRWJ('Gagal membaca data list pencarian obat', 'Error');
			};
		}
	});
}
function panel_form_lookup_insert_rujukan_balik_bpjs(rowdata) {
	loaddatastoreProgramPRB();
	loaddatastoreDokterPRB();
	console.log(rowdata);
	var formPanelInsertRujukanBalik = new Ext.form.FormPanel({
		id: "myformpanel_insert_rujukan_balik_bpjsigd",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .5,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 200,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'No SEP'
			},
			{
				x: 100,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_sep_insert_rujukan_balik_igd',
				name: 'txt_no_sep_insert_rujukan_balik_igd',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			/*{
				x: 242,
				y: 5,
				xtype: 'textfield',
				id: 'txt_unit_insert_rujukan',
				name: 'txt_unit_insert_rujukan',
				width: 70,
				disabled:true,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},*/

			{
				x: 320,
				y: 5,
				xtype: 'label',
				text: 'No SRB'
			},
			{
				x: 410,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_rujukan_insert_rujukan_balik_igd',
				name: 'txt_no_rujukan_insert_rujukan_igd',
				width: 220,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'Tgl SEP'
			},
			{
				x: 100,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 30,
				xtype: 'datefield',
				id: 'tgl_sep_insert_rujukan_balik_igd',
				name: 'tgl_sep_insert_rujukan_balik_igd',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,
			},
			{
				x: 320,
				y: 30,
				xtype: 'label',
				text: 'Tgl SRB'
			},
			{
				x: 410,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 420,
				y: 30,
				id: 'tgl_rujukan_insert_rujukan_balik_igd',
				name: 'tgl_rujukan_insert_rujukan_balik_igd',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 220,

			},

			{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'No Kartu'
			},
			{
				x: 100,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 55,
				xtype: 'textfield',
				id: 'txt_no_kartu_insert_rujukan_balik_igd',
				name: 'txt_no_kartu_insert_rujukan_balik_igd',
				width: 170,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 280,
				y: 55,
				xtype: 'textfield',
				id: 'txt_sex_insert_rujukan_balik_igd',
				name: 'txt_sex_insert_rujukan_balik_igd',
				width: 30,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			// {
			// 	x: 320,
			// 	y: 55,
			// 	xtype: 'label',
			// 	text: 'Pelayanan'
			// },
			// {
			// 	x: 410,
			// 	y: 55,
			// 	xtype: 'label',
			// 	text: ':'
			// },
			/*{
				x: 420,
				y: 55,
				xtype: 'textfield',
				id: 'combo',
				name: 'combo',
				width: 220,
				disabled:true,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},*/
			// mCombPelayananInsertRujukanBalik(),
			/*{
				xtype: '',
				fieldLabel: '',
				x: 420,
				y: 55,
				id: 'combo_pelayanan_insert_rujukan',
				editable: true,
				store: new Ext.data.ArrayStore({
					id: 0,
					fields:[
						'Id',
						'displayText'
					],
					data: [[0, 'Rawat Inap'], [1, 'Rawat Jalan']]
				}),
				valueField: 'Id',
				displayField: 'displayText',
				mode: 'local',
				width: 100,
				triggerAction: 'all',
				emptyText: 'Pilih Salah Satu...',
				selectOnFocus: true,
				tabIndex: 30,
				anchor: '99%',
				disabled : true,
				enableKeyEvents:true,
				listeners:{
					'select': function (a, b, c){
								
					},
							
				}
			},*/

			{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Nama'
			},
			{
				x: 100,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 80,
				xtype: 'textfield',
				id: 'txt_nama_peserta_insert_rujukan_balik_igd',
				name: 'txt_nama_peserta_insert_rujukan_balik_igd',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 55,
				xtype: 'label',
				text: 'Email'
			},
			{
				x: 410,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 55,
				xtype: 'textfield',
				id: 'txt_email_insert_rujukan_balik_igd',
				name: 'txt_email_insert_rujukan_balik_igd',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
			},

			{
				x: 320,
				y: 80,
				xtype: 'label',
				text: 'Program PRB'
			},
			{
				x: 410,
				y: 80,
				xtype: 'label',
				text: ':'
			},
			mCombProgramPRB(),


			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Dokter DPJP'
			},
			{
				x: 100,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			mCombDokterPRB(),


			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Alamat'
			},
			{
				x: 100,
				y: 130,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 130,
				xtype: 'textfield',
				id: 'txt_alamat_insert_rujukan_balik_igd',
				name: 'txt_alamat_insert_rujukan_balik_igd',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 105,
				xtype: 'label',
				text: 'Keterangan'
			},
			{
				x: 410,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 105,
				xtype: 'textfield',
				id: 'txt_keterangan_insert_rujukan_balik_igd',
				name: 'txt_keterangan_insert_rujukan_balik_igd',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
			},
			{
				x: 10,
				y: 155,
				xtype: 'label',
				text: 'Saran'
			},
			{
				x: 100,
				y: 155,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 155,
				xtype: 'textfield',
				id: 'txt_saran_rujukan_balik_insert_rujukan_igd',
				name: 'txt_saran_rujukan_balik_insert_rujukan_igd',
				width: 300,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},


		],

	});

	if (rowdata != undefined) {
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_history_rujukan_balik",
			params: { no_sep: rowdata.response.noSep },
			failure: function (o) {
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				console.log(cst);
				if (cst.sudah_ada == true) {
					Ext.getCmp('btn_update_rujukan_balik_igd').show();
					Ext.getCmp('btn_hapus_rujukan_balik_igd').show();
					Ext.getCmp('btn_simpan_rujukan_balik_igd_igd').hide();
					getHistoryObatPRB(rowdata.response.noSep, cst.listData[0].no_srb, cst.listData[0].tgl_srb);
					Ext.getCmp('btn_update_rujukan_balik_igd').show();
					Ext.getCmp('btn_hapus_rujukan_balik_igd').show();
					Ext.getCmp('txt_no_kartu_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.nama);
					Ext.getCmp('txt_no_sep_insert_rujukan_balik_igd').setValue(rowdata.response.noSep);
					Ext.getCmp('tgl_sep_insert_rujukan_balik_igd').setValue(rowdata.response.tglSep).disable();
					Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').setValue(cst.listData[0].no_srb);
					Ext.getCmp('txt_sex_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.kelamin);
					Ext.getCmp('tgl_rujukan_insert_rujukan_balik_igd').setValue(cst.listData[0].tgl_srb);
					Ext.getCmp('txt_email_insert_rujukan_balik_igd').setValue(cst.listData[0].email);
					Ext.getCmp('txt_alamat_insert_rujukan_balik_igd').setValue(cst.listData[0].alamat);
					Ext.getCmp('txt_keterangan_insert_rujukan_balik_igd').setValue(cst.listData[0].keterangan);
					Ext.getCmp('txt_saran_rujukan_balik_insert_rujukan_igd').setValue(cst.listData[0].saran);
					select_programPRB = cst.listData[0].kode_program_prb;
					select_dokterPRB = cst.listData[0].kode_dpjp;
					select_nama_dokterPRB = cst.listData[0].nama_dpjp;
					Ext.getCmp('cboProgramPRB_ugd').setValue(cst.listData[0].nama_program_prb);
					Ext.getCmp('cboDokterPRB_ugd').setValue(cst.listData[0].nama_dpjp);
				} else {
					Ext.getCmp('btn_update_rujukan_balik_igd').hide();
					Ext.getCmp('btn_hapus_rujukan_balik_igd').hide();
					Ext.getCmp('btn_simpan_rujukan_balik_igd_igd').show();
					Ext.getCmp('txt_no_kartu_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.noKartu);
					Ext.getCmp('txt_nama_peserta_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.nama);
					Ext.getCmp('txt_no_sep_insert_rujukan_balik_igd').setValue(rowdata.response.noSep);
					Ext.getCmp('tgl_sep_insert_rujukan_balik_igd').setValue(rowdata.response.tglSep).disable();
					Ext.getCmp('txt_no_rujukan_insert_rujukan_balik_igd').setValue("");
					Ext.getCmp('txt_sex_insert_rujukan_balik_igd').setValue(rowdata.response.peserta.kelamin);
					dsDataGrdObat_PRB.removeAll();
					gridPRB.getView().refresh();
				}
			}
		});


	}
	return formPanelInsertRujukanBalik;
}

function getHistoryObatPRB(sep, srb, tgl) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_history_obat_prb",
		params: {
			no_sep: sep,
			no_srb: srb,
			tgl_srb: tgl
		},
		failure: function (o) {
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if (cst.sudah_ada == true) {
				dsDataGrdObat_PRB.removeAll();
				gridPRB.getView().refresh();
				for (var i = 0, iLen = cst.totalrecords; i < iLen; i++) {
					var records = new Array();
					records.push(new dsDataGrdObat_PRB.recordType(cst.listData[i]));
					dsDataGrdObat_PRB.add(records);
				}
			} else {

			}
		}
	});
}

function loaddatastoreDokterPRB(tmp_poli_tujuan) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_dokterPRB",
		params: { unit: 'IGD' },
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsDokterPRB.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsDokterPRB.recordType;
				var o = cst['listData'][i];

				recs.push(new recType(o));
				dsDokterPRB.add(recs);
			}
			console.log(dsDokterPRB);
		}
	});
}

function mCombDokterPRB() {
	var Field = ['kode', 'nama'];
	dsDokterPRB = new WebApp.DataStore({ fields: Field });
	var cboDokterPRB = new Ext.form.ComboBox({
		id: 'cboDokterPRB_ugd',
		x: 110,
		y: 105,
		width: 200,
		readOnly: false,
		hidden: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Dokter DPJP...',
		fieldLabel: 'Dokter PRB',
		align: 'Right',
		tabIndex: 6,
		store: dsDokterPRB,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_dokterPRB = b.data.kode;
				select_nama_dokterPRB = b.data.nama;
			}
		}
	});
	return cboDokterPRB;
}

function loaddatastoreProgramPRB(tmp_poli_tujuan) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_programPRB",
		params: { unit: 'IGD' },
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsProgramPRB.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsProgramPRB.recordType;
				var o = cst['listData'][i];

				recs.push(new recType(o));
				dsProgramPRB.add(recs);
			}
			console.log(dsProgramPRB);
		}
	});
}

function mCombProgramPRB() {
	var Field = ['kode', 'nama'];
	dsProgramPRB = new WebApp.DataStore({ fields: Field });
	var cboProgramPRB = new Ext.form.ComboBox({
		id: 'cboProgramPRB_ugd',
		x: 420,
		y: 80,
		width: 200,
		readOnly: false,
		hidden: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Program PRB...',
		fieldLabel: 'Program PRB',
		align: 'Right',
		tabIndex: 6,
		store: dsProgramPRB,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_programPRB = b.data.kode;
			}
		}
	});
	return cboProgramPRB;
}

function mCombPelayananInsertRujukanBalik() {
	var cboJenisPelayananInsertRujukanBalik = new Ext.form.ComboBox({
		id: 'combo_pelayanan_insert_rujukan_balik_igd',
		x: 420,
		y: 55,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		tabIndex: 6,
		forceSelection: true,
		emptyText: '',
		anchor: '95%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, 'Rawat Inap'], [1, 'Rawat Jalan']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				console.log(b.data);
				if (b.data.Id == 0) {
					jenis_pelayanan_insert_rujukan_balik = 1;
				} else {
					jenis_pelayanan_insert_rujukan_balik = 2;
				}
			},

		}
	});
	return cboJenisPelayananInsertRujukanBalik;

}
// -------------------------------------------------------------------------------------------------------------
function DataPanel5() {
	var items = {
		title: 'Data Keluarga',
		layout: 'form',
		items: [
			{
				xtype: 'fieldset',
				title: 'Data Ayah',
				items: [
					{
						columnWidth: .50,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [
							{
								xtype: 'textfield',
								fieldLabel: 'Nama Ayah ',
								name: 'txtNamaAyah',
								id: 'txtNamaAyah',
								emptyText: ' ',
								tabIndex: 13,
								anchor: '99%',
								listeners: {
									'render': function (c) {
										c.getEl().on('keypress', function (e) {
											if (e.getKey() == 13)
												Ext.getCmp('cboPendidikanAyahRequestEntry').focus();
										}, c);
									}
								}
							}
						]
					}, {
						columnWidth: .15,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPendidikanAyahRequestEntry()]
					}, {
						columnWidth: .17,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPekerjaanAyahRequestEntry()]
					}
				]
			}, {
				xtype: 'fieldset',
				title: 'Data Ibu',
				items: [
					{
						columnWidth: .15,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPendidikanIbuRequestEntry()]
					}, {
						columnWidth: .17,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPekerjaanIbuRequestEntry()]
					}
				]
			}, {
				xtype: 'fieldset',
				title: 'Data Suami/Istri',
				items: [
					{
						columnWidth: .10,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [
							{
								xtype: 'textfield',
								fieldLabel: 'Nama Suami/Istri ',
								name: 'txtNamaSuamiIstri',
								id: 'txtNamaSuamiIstri',
								tabIndex: 14,
								emptyText: ' ',
								anchor: '99%',
								listeners: {
									'render': function (c) {
										c.getEl().on('keypress', function (e) {
											if (e.getKey() == 13) Ext.getCmp('cboPendidikanSuamiIstriRequestEntry').focus();
										}, c);
									}
								}
							}
						]
					}, {
						columnWidth: .15,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPendidikanSuamiIstriRequestEntry()]
					}, {
						columnWidth: .17,
						layout: 'form',
						border: false,
						labelWidth: 90,
						items: [mComboPekerjaanSuamiIstriRequestEntry()]
					}
				]
			}
		]
	};
	return items;
}
function datasave_viDaftar(mBol, cetakBpjs, bpjs) {
	loadMask.hide();
	if (ValidasiEntry_viDaftar('Simpan Data', false) == 1) {
		if (Ext.get('txtNoRequest').getValue() === 'Automatic from the system...' || Ext.get('txtNoRequest').getValue() === '') {
			/* var dlg = Ext.MessageBox.prompt('Scan E-KTP', 'Scan E-KTP anda :', function (btn, combo) {
				if (btn == 'ok'){
					if (combo != ''){ 
						nik_pasien=combo;
					}else{ */
			nik_pasien = 0;
			//}
			// alert(addNew_viDaftar);
			if (addNew_viDaftar == true) {
				loadMask.show();
				Ext.Ajax.request({
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparam_viDaftar(),
					failure: function (o) {
						loadMask.hide();
						Ext.MessageBox.hide();
						addNew_viDaftar = false;
						ShowPesanError_viDaftar('Error Database ! Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
						janganDuluCetakSEP = 'ya';
					},
					success: function (o) {
						loadMask.hide();
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) {
							tmp_sub_unit_rehab = "";
							koderujuk = '';
							janganDuluCetakSEP = 'tidak';
							//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
							Ext.MessageBox.hide();
							Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
							Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
							tmpnotransaksi = cst.NoTrans;
							addNew_viDaftar = false;
							printtracer();
							Ext.getCmp('btnSimpanExit_viDaftar').disable();
							Ext.getCmp('btnSimpan_viDaftar').disable();
							if (bpjs === true) {
								ajax_cetak_sep();
							}
							ShowPesanInfo_viDaftar('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
							if (cetakBpjs == true) {
								_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
							}
							getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
							Ext.getCmp('txtNama').focus(true);
						} else if (cst.success === false && cst.pesan === 0) {
							ShowPesanWarning_viDaftar('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');

						} else if (cst.success === false && cst.cari === true) {
							ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

						} else {
							ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');

						}
					}
				});
			} else {

				loadMask.show();
				Ext.Ajax.request({
					url: WebAppUrl.UrlUpdateData,
					params: dataparam_viDaftar(),
					failure: function (o) {
						loadMask.hide();
						Ext.MessageBox.hide();
						addNew_viDaftar = false;
						ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
						janganDuluCetakSEP = 'ya';
					},
					success: function (o) {
						loadMask.hide();
						var cst = Ext.decode(o.responseText);
						Ext.MessageBox.hide();
						if (cst.success === true) {
							tmp_sub_unit_rehab = "";
							janganDuluCetakSEP = 'tidak';
							//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
							Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
							Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
							ShowPesanInfo_viDaftar('Kunjungan berhasil disimpan', 'Edit Kunjungan');
							if (cetakBpjs == true) {
								_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
							}
							if (bpjs === true) {
								ajax_cetak_sep();
							}
							printtracer();
							koderujuk = 0;
							Ext.getCmp('btnSimpanExit_viDaftar').disable();
							Ext.getCmp('btnSimpan_viDaftar').disable();
							getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
							Ext.getCmp('txtNama').focus(true);
						} else if (cst.success === false && cst.cari === true) {
							janganDuluCetakSEP = 'ya';
							ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

						} else if (cst.success === false && cst.pesan === 0) {
							janganDuluCetakSEP = 'ya';
							ShowPesanWarning_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

						} else {
							janganDuluCetakSEP = 'ya';
							ShowPesanError_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

						}
					}
				});

			}
			/* }
		}); */
		} else {
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/cekKunjungan",
				params: {
					kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
				},
				failure: function (o) {
					ShowPesanError_viDaftar('Error cek kunjungan. Hubungi Admin!', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success == false) {
						ShowPesanError_viDaftar('Pasien Rawat Inap, Belum dipulangkan!!.', 'Error');
					} else {
						loadMask.show();
						Ext.Ajax.request({
							url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien",
							params: { kdPasien: Ext.get('txtNoRequest').getValue() },
							failure: function (o) {
								loadMask.hide();
								ShowPesanError_viDaftar('Pencarian error ! ', 'RawatJalan');
							},
							success: function (o) {
								loadMask.hide();
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) {
									nik_pasien = cst.niknya;
									if (addNew_viDaftar == true) {
										loadMask.show();
										Ext.Ajax.request({
											url: baseURL + "index.php/main/CreateDataObj",
											params: dataparam_viDaftar(),
											failure: function (o) {
												loadMask.hide();
												Ext.MessageBox.hide();
												addNew_viDaftar = false;
												ShowPesanError_viDaftar('Error Database ! Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
												janganDuluCetakSEP = 'ya';
											},
											success: function (o) {
												loadMask.hide();
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													tmp_sub_unit_rehab = "";
													//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
													Ext.MessageBox.hide();
													Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
													Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
													tmpnotransaksi = cst.NoTrans;
													addNew_viDaftar = false;
													printtracer();
													Ext.getCmp('btnSimpanExit_viDaftar').disable();
													Ext.getCmp('btnSimpan_viDaftar').disable();
													janganDuluCetakSEP = 'tidak';
													ShowPesanInfo_viDaftar('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
													if (cetakBpjs == true) {
														_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
													}
													if (bpjs === true) {
														ajax_cetak_sep();
													}
													getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
													Ext.getCmp('txtNama').focus(true);
												} else if (cst.success === false && cst.pesan === 0) {
													janganDuluCetakSEP = 'ya';
													ShowPesanWarning_viDaftar('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');

												} else if (cst.success === false && cst.cari === true) {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

												} else {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');

												}
											}
										});
									} else {
										loadMask.show();
										Ext.Ajax.request({
											url: WebAppUrl.UrlUpdateData,
											params: dataparam_viDaftar(),
											failure: function (o) {
												loadMask.hide();
												Ext.MessageBox.hide();
												addNew_viDaftar = false;
												ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
												janganDuluCetakSEP = 'ya';
											},
											success: function (o) {
												loadMask.hide();
												var cst = Ext.decode(o.responseText);
												Ext.MessageBox.hide();
												if (cst.success === true) {
													tmp_sub_unit_rehab = "";
													janganDuluCetakSEP = 'tidak';
													//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
													Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
													Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
													ShowPesanInfo_viDaftar('Kunjungan berhasil disimpan', 'Edit Kunjungan');
													if (cetakBpjs == true) {
														_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
													}
													if (bpjs === true) {
														ajax_cetak_sep();
													}
													printtracer();
													koderujuk = 0;
													Ext.getCmp('btnSimpanExit_viDaftar').disable();
													Ext.getCmp('btnSimpan_viDaftar').disable();
													getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
													Ext.getCmp('txtNama').focus(true);
												} else if (cst.success === false && cst.cari === true) {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

												} else if (cst.success === false && cst.pesan === 0) {
													janganDuluCetakSEP = 'ya';
													ShowPesanWarning_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

												} else {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

												}
											}
										});
									}
								} else {
									/* var dlg = Ext.MessageBox.prompt('Scan E-KTP', 'Scan E-KTP anda :', function (btn, combo) {
										if (btn == 'ok'){
											if (combo != ''){ 
												nik_pasien=combo;
											}else{ */
									nik_pasien = 0;
									//}
									if (addNew_viDaftar == true) {
										loadMask.show();
										Ext.Ajax.request({
											url: baseURL + "index.php/main/CreateDataObj",
											params: dataparam_viDaftar(),
											failure: function (o) {
												loadMask.hide();
												Ext.MessageBox.hide();
												addNew_viDaftar = false;
												ShowPesanError_viDaftar('Error Database ! Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
												janganDuluCetakSEP = 'ya';
											},
											success: function (o) {
												loadMask.hide();
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													janganDuluCetakSEP = 'tidak';
													tmp_sub_unit_rehab = "";
													//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
													Ext.MessageBox.hide();
													Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
													Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
													tmpnotransaksi = cst.NoTrans;
													addNew_viDaftar = false;
													printtracer();
													Ext.getCmp('btnSimpanExit_viDaftar').disable();
													Ext.getCmp('btnSimpan_viDaftar').disable();
													ShowPesanInfo_viDaftar('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
													if (cetakBpjs == true) {
														_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
													}
													if (bpjs === true) {
														ajax_cetak_sep();
													}
													getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
													Ext.getCmp('txtNama').focus(true);
												} else if (cst.success === false && cst.pesan === 0) {
													janganDuluCetakSEP = 'ya';
													ShowPesanWarning_viDaftar('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');

												} else if (cst.success === false && cst.cari === true) {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

												} else {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');

												}
											}
										});
									} else {
										loadMask.show();
										Ext.Ajax.request({
											url: WebAppUrl.UrlUpdateData,
											params: dataparam_viDaftar(),
											failure: function (o) {
												loadMask.hide();
												Ext.MessageBox.hide();
												addNew_viDaftar = false;
												ShowPesanError_viDaftar('Kunjungan tidak berhasil di simpan ', 'Simpan Kunjungan');
												janganDuluCetakSEP = 'ya';
											},
											success: function (o) {
												loadMask.hide();
												var cst = Ext.decode(o.responseText);
												Ext.MessageBox.hide();
												if (cst.success === true) {
													tmp_sub_unit_rehab = "";
													janganDuluCetakSEP = 'tidak';
													//savePendaftaranRWJ_SQL(cst.KD_PASIEN,cst.NoTrans);
													Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
													Ext.getCmp('txtNoRegRWJ').setValue(cst.NoTrans);
													ShowPesanInfo_viDaftar('Kunjungan berhasil disimpan', 'Edit Kunjungan');
													if (cetakBpjs == true) {
														_bpjs.cetakSep(Ext.getCmp('txtNoSJP').getValue(), undefined, Ext.getCmp('txtcatatan').getValue());
													}
													if (bpjs === true) {
														ajax_cetak_sep();
													}
													printtracer();
													koderujuk = 0;
													Ext.getCmp('btnSimpanExit_viDaftar').disable();
													Ext.getCmp('btnSimpan_viDaftar').disable();
													getLookupDataHistoryKunjungan_PendaftaranRWJ(cst.KD_PASIEN);
													Ext.getCmp('txtNama').focus(true);
												} else if (cst.success === false && cst.cari === true) {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Anda telah berkunjung pada poli yang sama ', 'Simpan Kunjungan');

												} else if (cst.success === false && cst.pesan === 0) {
													janganDuluCetakSEP = 'ya';
													ShowPesanWarning_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

												} else {
													janganDuluCetakSEP = 'ya';
													ShowPesanError_viDaftar('Kunjungan tidak berhasil disimpan ' + cst.pesan, 'Edit Kunjungan');

												}
											}
										});
									}
									/* }
								}); */
								}
							}
						});
					}
				}
			});
		}
	} else {
		janganDuluCetakSEP = 'ya';
		if (mBol === true) {
			return false;
		}
	}
}
function datadelete_viDaftar() {
	if (ValidasiEntry_viDaftar('Hapus Data', true) == 1) {
		Ext.Msg.show({
			title: 'Hapus Data',
			msg: "Akan menghapus data?",
			buttons: Ext.MessageBox.YESNO,
			width: 300,
			fn: function (btn) {
				if (btn == 'yes') {
					Ext.Ajax.request({
						url: WebAppUrl.UrlDeleteData,
						params: dataparamDelete_viDaftar(),
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) {
								ShowPesanInfo_viDaftar('Data berhasil dihapus', 'Hapus Data');
								datarefresh_viDaftar(tmpcriteriaRWJ);
								dataaddnew_viDaftar();
								Ext.getCmp('btnSimpan_viDaftar').disable();
								Ext.getCmp('btnSimpanExit_viDaftar').disable();
							} else if (cst.success === false && cst.pesan === 0) {
								ShowPesanWarning_viDaftar('Data tidak berhasil dihapus ', 'Hapus Data');
							} else if (cst.success === false && cst.pesan === 1) {
								ShowPesanError_viDaftar('Data tidak berhasil dihapus ' + cst.msg, 'Hapus Data');
							}
						}
					})
				}
			}
		});
	}
}
function ValidasiEntry_viDaftar(modul, mBolHapus) {
	var x = 1;
	if (Ext.get('txtNama').getValue() === "") {
		ShowPesanWarning_viDaftar("Nama belum terisi...", modul);
		Ext.getCmp('txtNama').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboJK_Daftar').getValue() === "") {
		ShowPesanWarning_viDaftar("Jenis Kelamin belum dipilih...", modul);
		Ext.getCmp('cboJK_Daftar').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}

	if (Ext.get('cboAgamaRequestEntry').dom.value === "" || Ext.get('cboAgamaRequestEntry').dom.value === "Pilih Agama...") {
		ShowPesanWarning_viDaftar("Agama belum dipilih...", modul);
		Ext.getCmp('cboAgamaRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboPoliklinikRequestEntry').dom.value === "" || Ext.get('cboPoliklinikRequestEntry').dom.value === "Pilih Poliklinik..." && Ext.get('cbnonkunjungan').dom.checked === false) {
		ShowPesanWarning_viDaftar("poli belum dipilih...", modul);
		Ext.getCmp('cboPoliklinikRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboDokterRequestEntry').dom.value === "" || Ext.get('cboDokterRequestEntry').dom.value === "Pilih Dokter..." && Ext.get('cbnonkunjungan').dom.checked === false) {
		ShowPesanWarning_viDaftar("Dokter belum dipilih...", modul);
		Ext.getCmp('cboDokterRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboPropinsiRequestEntry').dom.value === "" || Ext.get('cboPropinsiRequestEntry').dom.value === "'Pilih Propinsi...'") {
		ShowPesanWarning_viDaftar("Propinsi Atau Kota belum dipilih...", modul);
		Ext.getCmp('cboPropinsiRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('txtAlamat').dom.value === "") {
		ShowPesanWarning_viDaftar("Alamat belum dipilih...", modul);
		Ext.getCmp('txtAlamat').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboStatusMarital_Daftar').dom.value === "" || Ext.get('cboStatusMarital_Daftar').dom.value === "Pilih Status...") {
		Ext.getCmp('cboStatusMarital_Daftar').focus(false);
		janganDuluCetakSEP = 'ya';
		PasienSetSatusMarital = 0;
	}
	if (Ext.get('cboKecamatanRequestEntry').dom.value === "" || Ext.get('cboKecamatanRequestEntry').dom.value === "Pilih kecamatan...") {
		ShowPesanWarning_viDaftar("Kecamatan Atau Kota belum dipilih...", modul);
		Ext.getCmp('cboKecamatanRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboPendidikanRequestEntry').dom.value === "" || Ext.get('cboPendidikanRequestEntry').dom.value === 'Pilih Pendidikan...') {
		ShowPesanWarning_viDaftar("Pendidikan belum dipilih...", modul);
		Ext.getCmp('cboPendidikanRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboPekerjaanRequestEntry').dom.value === "" ||
		Ext.get('cboPekerjaanRequestEntry').dom.value === 'Pilih Pekerjaan...') {
		ShowPesanWarning_viDaftar("Pekerjaan belum dipilih...", modul);
		Ext.getCmp('cboPekerjaanRequestEntry').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboJK_Daftar').dom.value === "" ||
		Ext.get('cboJK_Daftar').dom.value === 'Pilih Jenis Kelamin...') {
		ShowPesanWarning_viDaftar("Jenis Kelamin belum dipilih...", modul);
		Ext.getCmp('cboJK_Daftar').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('cboStatusMarital_Daftar').dom.value === "" || Ext.get('cboStatusMarital_Daftar').dom.value === 'Pilih Status...') {
		ShowPesanWarning_viDaftar("Status belum dipilih...", modul);
		Ext.getCmp('cboStatusMarital_Daftar').focus(false);
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	if (Ext.get('txtAlamat').dom.value === "") {
		ShowPesanWarning_viDaftar("Alamat belum diisi...", modul);
		Ext.getCmp('txtAlamat').focus();
		janganDuluCetakSEP = 'ya';
		x = 0;
	}
	// if (Ext.get('cbokelurahanPenanggungJawab').dom.value === "") {
	// 	ShowPesanWarning_viDaftar("Kelurahan Penanggungjawab belum diisi...", modul);
	// 	Ext.getCmp('cbokelurahanPenanggungJawab').focus();
	// 	// janganDuluCetakSEP = 'ya';
	// 	x = 0;
	// }

	var tmpNoIIMedrec = Ext.get('txtpos').dom.value;
	if (Ext.get('cboKecamatanKtp').dom.value === "" || Ext.get('cboKecamatanKtp').dom.value === "Pilih Kecamatan...") {
		Ext.getCmp('cboPropinsiKtp').setValue(Ext.get('cboPropinsiRequestEntry').dom.value);
		Ext.getCmp('cboKtpKabupaten').setValue(Ext.get('cboKabupatenRequestEntry').dom.value);
		Ext.getCmp('txtAlamatktp').setValue(Ext.getCmp('txtAlamat').getValue());
		Ext.getCmp('cboKecamatanKtp').setValue(Ext.get('cboKecamatanRequestEntry').dom.value);
		Ext.getCmp('txtposktp').setValue(Ext.getCmp('txtpos').getValue());
		Ext.getCmp('cbokelurahanKtp').setValue(Ext.get('cbokelurahan').dom.value);
		selectkelurahanktp = kelurahanpasien;
		selectPropinsiKtp = Ext.getCmp('txtTmpPropinsi').getValue();
		selectKecamatanktp = Ext.getCmp('txtTmpKecamatan').getValue();
	}
	if (Ext.get('cbokelurahanKtp').dom.value === "" || Ext.get('cbokelurahanKtp').dom.value === "Pilih Kelurahan...") {
		Ext.getCmp('cboPropinsiKtp').setValue(Ext.get('cboPropinsiRequestEntry').dom.value);
		Ext.getCmp('cboKtpKabupaten').setValue(Ext.get('cboKabupatenRequestEntry').dom.value);
		Ext.getCmp('txtAlamatktp').setValue(Ext.getCmp('txtAlamat').getValue());
		Ext.getCmp('cboKecamatanKtp').setValue(Ext.get('cboKecamatanRequestEntry').dom.value);
		Ext.getCmp('txtposktp').setValue(Ext.getCmp('txtpos').getValue());
		Ext.getCmp('cbokelurahanKtp').setValue(Ext.get('cbokelurahan').dom.value);
		selectkelurahanktp = kelurahanpasien;
		selectPropinsiKtp = Ext.getCmp('txtTmpPropinsi').getValue();
		selectKecamatanktp = Ext.getCmp('txtTmpKecamatan').getValue();
	}
	if (Ext.getCmp('txtposktp').getValue().length > 5) {
		ShowPesanWarning_viDaftar("Kode pos ktp tidak boleh lebih dari 5!", 'WARNING');
		Ext.getCmp('txtposktp').focus();
		x = 0;
	}
	if (Ext.getCmp('txtpos').getValue().length > 5) {
		ShowPesanWarning_viDaftar("Kode pos tidak boleh lebih dari 5!", 'WARNING');
		Ext.getCmp('txtpos').focus();
		x = 0;
	}
	var letters = /[a-zA-Z]/;
	if (Ext.getCmp('txttlpnPasien').getValue().match(letters)) {
		ShowPesanWarning_viDaftar("Telepon hanya bisa diisi dengan angka!", 'WARNING');
		Ext.getCmp('txttlpnPasien').focus();
		x = 0;
	}
	if (Ext.getCmp('txttlpnPasien').getValue().length > 30) {
		ShowPesanWarning_viDaftar("Telepon tidak boleh lebih dari 30 karakter!", 'WARNING');
		Ext.getCmp('txttlpnPasien').focus();
		x = 0;
	}
	if (Ext.getCmp('cboCustomer').getValue() === '' || Ext.getCmp('cboCustomer').getValue() === undefined) {
		ShowPesanWarning_viDaftar("Kelompok pasien tidak boleh kosong!", 'WARNING');
		Ext.getCmp('cboCustomer').focus(true, 10);
		x = 0;
	}
	//alert(Ext.getCmp('rb_datang').getValue());
	if (Ext.getCmp('rb_datang').getValue() == false) {
		if (Ext.getCmp('cboRujukanDariRequestEntry').getValue() == '' || Ext.getCmp('cboRujukanDariRequestEntry').getValue() == 'Pilih Rujukan...') {

			ShowPesanWarning_viDaftar("Rujukan dari tidak boleh kosong!", 'WARNING');
			Ext.getCmp('cboRujukanDariRequestEntry').focus(true, 10);
			x = 0;
		}
		if (Ext.getCmp('cboRujukanRequestEntry').getValue() == '' || Ext.getCmp('cboRujukanRequestEntry').getValue() == 'Pilih Rujukan...') {
			ShowPesanWarning_viDaftar("Rujukan tidak boleh kosong!", 'WARNING');
			//Ext.getCmp('cboRujukanRequestEntry').focus(true,10);
			x = 0;
		}
	}

	console.log(Ext.getCmp('cboGolDarah_Daftar').getValue());
	if (Ext.getCmp('cboGolDarah_Daftar').getValue() === "" || Ext.getCmp('cboGolDarah_Daftar').getValue() == "Pilih Gol. Darah...") {
		console.log('masuk gol darah');
		Ext.getCmp('cboGolDarah_Daftar').focus();
		ShowPesanWarning_viDaftar("Golongan Darah tidak boleh kosong!", 'WARNING');
		x = 0;
	}

	return x;
}
function ShowPesanWarning_viDaftar(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.WARNING,
		width: 250
	});
}
function ShowPesanError_viDaftar(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 250
	});
}
function ShowPesanInfo_viDaftar(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width: 250
	})
}
function datarefresh_viDaftar(criteria, show) {
	loadMask.show();
	dataSource_viDaftar.load({
		params: {
			Skip: Ext.getCmp('perpage_bbar_paging').getValue(),
			Take: selectCount_viDaftar,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewKunjunganList',
			param: criteria
		},
		callback: function (a, b, c) {
			loadMask.hide();
			if (show != undefined && show == 'Y') {
				if (a.length == 1) {
					Ext.Ajax.request({
						url: baseURL + "index.php/main/getDataKunjungan",
						params: {
							command: a[0].data.KD_PASIEN
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							setLookUp_viDaftar(cst);
						}
					});
				}
			}
		}
	});
	return dataSource_viDaftar;
}
function datarefresh_medrec_viDaftar(criteria) {
	dataSource_medrec_viDaftar.load({
		params: {
			Skip: 0,
			Take: selectCount_viDaftar,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewDataKunjunganPasien',
			param: criteria
		}
	});
	return dataSource_medrec_viDaftar;
}
function getCriteriaFilter_viDaftar() {
	var strKriteria = "";
	if (Ext.get('txtNoMedrec').dom.value != "") {
		strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec').dom.value + "'" + " or pasien.part_number_nik= " + "'" + Ext.get('txtNoMedrec').dom.value + "'" + " " + tmpcriteriaRWJ;
	}
	if (Ext.get('txtNamaKeluargacari').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(pasien.nama_keluarga) like lower (" + "'%" + Ext.get('txtNamaKeluargacari').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(pasien.nama_keluarga) like lower (" + "'%" + Ext.get('txtNamaKeluargacari').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtAlamatPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(pasien.alamat) " + "LIKE lower('%" + Ext.get('txtAlamatPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(pasien.alamat) " + "LIKE lower('%" + Ext.get('txtAlamatPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtNamaPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(pasien.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(pasien.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtHPcari').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(pasien.handphone) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%')" + " or lower(pasien.telepon) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%') " + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(pasien.handphone) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%')" + "  or lower(pasien.telepon) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%') " + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtAsuransiPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " pasien.no_asuransi = '" + Ext.get('txtAsuransiPasien').dom.value + "'" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and pasien.no_asuransi = '" + Ext.get('txtAsuransiPasien').dom.value + "'" + tmpcriteriaRWJ;
		}
	}
	return strKriteria;
}
function getCriteriaFilter_viDaftar2() {
	var strKriteria = "";
	if (Ext.get('txtNoMedrec').dom.value != "") {
		strKriteria = " p.kd_pasien = " + "'" + Ext.get('txtNoMedrec').dom.value + "'" + tmpcriteriaRWJ;
	}
	if (Ext.get('txtNamaKeluargacari').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(p.nama_keluarga) like lower (" + "'%" + Ext.get('txtNamaKeluargacari').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(p.nama_keluarga) like lower (" + "'%" + Ext.get('txtNamaKeluargacari').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtAlamatPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(p.alamat) " + "LIKE lower('%" + Ext.get('txtAlamatPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(p.alamat) " + "LIKE lower('%" + Ext.get('txtAlamatPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtNamaPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(p.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(p.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien').dom.value + "%')" + "" + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtHPcari').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " lower(p.handphone) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%')" + " or lower(p.telepon) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%') " + tmpcriteriaRWJ;
		} else {
			strKriteria += " and lower(p.handphone) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%')" + "  or lower(p.telepon) " + "LIKE lower('%" + Ext.get('txtHPcari').dom.value + "%') " + tmpcriteriaRWJ;
		}
	}
	if (Ext.get('txtAsuransiPasien').dom.value != "") {
		if (strKriteria == "") {
			strKriteria = " p.no_asuransi = '" + Ext.get('txtAsuransiPasien').dom.value + "'" + tmpcriteriaRWJ;
		} else {
			strKriteria += " and p.no_asuransi = '" + Ext.get('txtAsuransiPasien').dom.value + "'" + tmpcriteriaRWJ;
		}
	}
	return strKriteria;
}

function getLookupDataHistoryKunjungan_PendaftaranRWJ(kd_pasien) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/viewdatakunjunganpasien/getHistoryKunjungan",
		params: {
			kd_pasien: kd_pasien
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			LookupDataSourceHistoryKunjungan_PendaftaranRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs = [],
					recType = LookupDataSourceHistoryKunjungan_PendaftaranRWJ.recordType;
				for (var i = 0; i < cst.ListDataObj.length; i++) {
					recs.push(new recType(cst.ListDataObj[i]));
				}
				LookupDataSourceHistoryKunjungan_PendaftaranRWJ.add(recs);
				// LookupGrdDataHistoryKunjungan_PendaftaranRWJ.getView().refresh();
			}
			else {
				ShowPesanWarning_viDaftar('Gagal membaca history kunjungan pasien!', 'WARNING');
			};
		}
	});
}
function datainit_viDaftar(rowdata) {
	console.log(rowdata);
	Ext.getCmp('cboPoliklinikRequestEntry').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cboPoliklinikRequestEntry').focus(false, 1000);
	addNew_viDaftar = false;
	addNew_viDaftar = true;
	dsPropinsiRequestEntry = undefined;
	console.log(rowdata);
	carapenerimaanpasien = rowdata.KD_PENERIMAAN;
	str_kdpasien = rowdata.KD_PASIEN;
	koderujuk = rowdata.KD_RUJUKAN;
	Ext.getCmp('txtNoRequest').setValue(rowdata.KD_PASIEN);
	Ext.getCmp('txtNama').setValue(rowdata.NAMA);
	Ext.getCmp('txtNamaKeluarga').setValue(rowdata.NAMA_KELUARGA);
	Ext.getCmp('txtTempatLahir_Daftar').setValue(rowdata.TEMPAT_LAHIR);
	PILIHKABUPATEN = rowdata.KABUPATEN;
	Pendidikanpasien = rowdata.KD_PENDIDIKAN;
	PendidikanAyah = rowdata.KD_PENDIDIKAN_AYAH;
	PendidikanIbu = rowdata.KD_PENDIDIKAN_IBU;
	PendidikanSuamiIstri = rowdata.KD_PENDIDIKAN_SUAMIISTRI;
	PekerjaanPasien = rowdata.KD_PEKERJAAN;
	PekerjaanAyah = rowdata.KD_PEKERJAAN_AYAH;
	PekerjaanIbu = rowdata.KD_PEKERJAAN_IBU;
	PekerjaanSuamiIstri = rowdata.KD_PEKERJAAN_SUAMIISTRI;
	Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
	Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
	Ext.getCmp('txtAlamat').setValue(rowdata.ALAMAT);
	Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
	Ext.getCmp('cboGolDarah_Daftar').setValue(rowdata.GOL_DARAH);
	PasienSetSatusMarital = rowdata.STATUS_MARITA;
	var tgl_lahirnya = getParamHitungUmurSQL(rowdata.TGL_LAHIR);
	Ext.get('dtpTanggalLahir').dom.value = tgl_lahirnya;
	Ext.getCmp('cboStatusMarital_Daftar').setValue(rowdata.STATUS_MARITA);
	Ext.getCmp('txtNamaAyah').setValue(rowdata.NAMA_AYAH);
	Ext.getCmp('cboPendidikanAyahRequestEntry').setValue(rowdata.PENDIDIKAN_AYAH);
	Ext.getCmp('cboPekerjaanAyahRequestEntry').setValue(rowdata.PEKERJAAN_AYAH);
	Ext.getCmp('txtNamaIbu').setValue(rowdata.NAMA_IBU);
	Ext.getCmp('cboPendidikanIbuRequestEntry').setValue(rowdata.PENDIDIKAN_IBU);
	Ext.getCmp('cboPekerjaanIbuRequestEntry').setValue(rowdata.PEKERJAAN_IBU);
	Ext.getCmp('txtNamaSuamiIstri').setValue(rowdata.NAMA_SUAMIISTRI);
	Ext.getCmp('cboPendidikanSuamiIstriRequestEntry').setValue(rowdata.PENDIDIKAN_SUAMIISTRI);
	Ext.getCmp('cboPekerjaanSuamiIstriRequestEntry').setValue(rowdata.PEKERJAAN_SUAMIISTRI);
	Ext.getCmp('cbokelurahan').setValue(rowdata.KELURAHAN);
	Ext.getCmp('txtAlamatktp').setValue(rowdata.ALAMAT_KTP);
	Ext.getCmp('txtposktp').setValue(rowdata.KD_POS_KTP);
	Ext.getCmp('txtpos').setValue(rowdata.KD_POS);
	Ext.getCmp('cboPropinsiKtp').setValue(rowdata.PRO_KTP);
	Ext.getCmp('cboKtpKabupaten').setValue(rowdata.KAB_KTP);
	Ext.getCmp('cbokelurahanKtp').setValue(rowdata.KEL_KTP);
	Ext.getCmp('cboKecamatanKtp').setValue(rowdata.KEC_KTP);
	Ext.getCmp('txttlpnPasien').setValue(rowdata.TLPN_PASIEN);
	Ext.getCmp('txtnikPasien').setValue(rowdata.NIK);
	Ext.getCmp('txtHandphonePasien').setValue(rowdata.HP_PASIEN);
	Ext.getCmp('txtEmailPasien').setValue(rowdata.EMAIL_PASIEN);
	loaddatastoreKtpkabupaten(rowdata.PROPINSIKTP);
	loaddatastoreKtpkecamatan(rowdata.KABUPATENKTP);
	loaddatastoreKtpkelurahan(rowdata.KECAMATANKTP);
	kelurahanpasien = rowdata.KD_KELURAHAN;
	selectAgamaRequestEntry = rowdata.KD_AGAMA;
	selectPropinsiKtp = rowdata.PROPINSIKTP;
	selectkelurahanktp = rowdata.KD_KELURAHAN_KTP;
	selectKecamatanktp = rowdata.KECAMATANKTP;
	NoAsuransiPasien = rowdata.NO_ASURANSI;
	Ext.getCmp('cboRujukanDariRequestEntry').show();
	Ext.getCmp('cboRujukanRequestEntry').show();
	jeniscus = rowdata.JENIS_CUST;
	RefreshDatacombo(rowdata.JENIS_CUST);
	kodePenerimaanPasien = rowdata.KD_PENERIMAAN;

	if (rowdata.JENIS_CUST == '0') {
		Ext.getCmp('kelPasien').setValue('Perseorangan');
		Combo_Select_prwj('Perseorangan');

	} else if (rowdata.JENIS_CUST == '1') {
		Ext.getCmp('kelPasien').setValue('Perusahaan');
		Combo_Select_prwj('Perusahaan');

		//loaddatastorerujukan(rowdata.KD_PENERIMAAN);
	} else if (rowdata.JENIS_CUST == '2') {
		Ext.getCmp('kelPasien').setValue('Asuransi');
		Combo_Select_prwj('Asuransi');
		//loaddatastorerujukan(rowdata.KD_PENERIMAAN);
	} else if (rowdata.JENIS_CUST == '3') {
		Ext.getCmp('kelPasien').setValue('Dinas Sosial');
		Combo_Select_prwj('Dinas Sosial');
		//loaddatastorerujukan(rowdata.KD_PENERIMAAN);
	}
	Ext.getCmp('cboAsuransi').setValue(rowdata.NAMA_CUSTOMER);
	selectSetAsuransi = rowdata.KD_CUSTOMER;
	if (rowdata.KD_CUSTOMER === '0000000009' || rowdata.KD_CUSTOMER === '0000000005') {
		Ext.getCmp('rb_datang').enable();
	} else if (Ext.getCmp('kelPasien').getValue() === 'Perseorangan' || Ext.getCmp('kelPasien').getValue() === 'Perusahaan') {
		Ext.getCmp('rb_datang').enable();
	} else {
		Ext.getCmp('rb_datang').disable();
	}

	if (rowdata.KD_CUSTOMER === '0000000043' || rowdata.KD_CUSTOMER === '0000000044') {
		Ext.getCmp('txtcatatan').show();
	} else {
		Ext.getCmp('txtcatatan').hide();
	}


	polipilihanpasien = rowdata.KD_UNIT;
	loaddatastoredokter(polipilihanpasien);
	// kodeDokterDefault='000';
	// Ext.getCmp('cboDokterRequestEntry').setValue('-----');
	// autocomdiagnosa.setValue(rowdata.NAMA_PENYAKIT);
	Ext.getCmp('txtNamaPeserta').setValue(rowdata.NAMA_PESERTA);
	Ext.getCmp('txtDiagnosa_RWJ').setValue(rowdata.NAMA_PENYAKIT);
	autocomdiagnosa_master = rowdata.KD_PENYAKIT;
	tmp_diagnosa = rowdata.KD_PENYAKIT;
	console.log("KD_RUJUKAN");

	/* if(rowdata.KD_RUJUKAN===0){
		carapenerimaanpasien=99;
		koderujuk=0;
		pasienrujukan=false;
	}else{
		Ext.getCmp('rb_Rujukan').setValue(true);
		Ext.getCmp('cboRujukanDariRequestEntry').enable();
		pasienrujukan=true;
		Ext.getCmp('cboRujukanDariRequestEntry').setValue(rowdata.NAMA_PENERIMAAN);
		loaddatastorerujukan(rowdata.KD_PENERIMAAN);	
		Ext.getCmp('cboRujukanRequestEntry').enable();
		Ext.getCmp('cboRujukanRequestEntry').setValue(rowdata.NAMA_RUJUAKAN);
		loaddatastoredatarujukan(rowdata.KD_RUJUKAN);
	} */
	var tmpjk = "";
	var tmpwni = "";
	if (rowdata.JENIS_KELAMIN === "t" || rowdata.JENIS_KELAMIN === 1) {
		tmpjk = "Laki - Laki";
	} else {
		tmpjk = "Perempuan";
	}
	//alert(rowdata.JENIS_KELAMIN);
	selectSetJK = rowdata.JENIS_KELAMIN;
	Ext.getCmp('cboJK_Daftar').setValue(tmpjk);
	if (rowdata.WNI === "t" || rowdata.WNI === 1) {
		tmpwni = "WNI";
	} else {
		tmpwni = "WNA";
	}

	Ext.getCmp('cboPropinsiRequestEntry').setValue(rowdata.PROPINSI);

	Ext.getCmp('cboKabupatenRequestEntry').setValue(rowdata.KABUPATEN);
	Ext.getCmp('cboKecamatanRequestEntry').setValue(rowdata.KECAMATAN);
	loaddatastorekabupaten(rowdata.KD_PROPINSI);
	loaddatastorekecamatan(rowdata.KD_KABUPATEN);
	loaddatastorekelurahan(rowdata.KD_KECAMATAN);
	//recs.push(new recType(data.insert(o)));
	//alert(rowdata.RIWAYAT_PENYAKIT);
	var recType = dataSourceHistoryPenyakit.recordType;
	for (var i = 0, iLen = rowdata.RIWAYAT_PENYAKIT.length; i < iLen; i++) {
		var recs = [],
			recType = dataSourceHistoryPenyakit.recordType;
		recs.push(new recType(rowdata.RIWAYAT_PENYAKIT[i]));
		dataSourceHistoryPenyakit.add(recs);
	}
	//dataSourceHistoryPenyakit.add(new recType(rowdata.RIWAYAT_PENYAKIT));
	//dataSourceHistoryPenyakit.loadData(rowdata.RIWAYAT_PENYAKIT,false);

	//Ext.each(rowdata.RIWAYAT_PENYAKIT,function(rec){
	//	dataSourceHistoryPenyakit.add({});
	//});
	//Ext.getCmp('rwj.riwayat.penyakit').getView().refresh();
	//dataSourceHistoryPenyakit.loadData(rowdata.RIWAYAT_PENYAKIT);
	Ext.getCmp('txtTmpPropinsi').setValue(rowdata.KD_PROPINSI);
	Ext.getCmp('txtTmpKabupaten').setValue(rowdata.KD_KABUPATEN);
	Ext.getCmp('txtTmpKecamatan').setValue(rowdata.KD_KECAMATAN);
	Ext.getCmp('txtTmpPendidikan').setValue(rowdata.KD_PENDIDIKAN);
	Ext.getCmp('txtTmpPekerjaan').setValue(rowdata.KD_PEKERJAAN);
	Ext.getCmp('txtTmpAgama').setValue(rowdata.KD_AGAMA);
	Ext.getCmp('cboSukuRequestEntry').setValue(rowdata.SUKU);
	selectSukuRequestEntry = rowdata.KD_SUKU;
	setUsia(tgl_lahirnya);
	mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	getLookupDataHistoryKunjungan_PendaftaranRWJ(rowdata.KD_PASIEN);

	if (booking_online != null) {
		kodeDokterDefault = booking_online.data.KD_DOKTER;
		Ext.getCmp('cboDokterRequestEntry').setValue(booking_online.data.NAMA_DOKTER);

		Ext.getCmp('cboCustomer').setValue(booking_online.data.CUSTOMER);
		Kd_Customer = booking_online.data.KD_CUSTOMER;
		if (booking_online.data.JENIS_CUST == 0) {
			Combo_Select_prwj('Perseorangan');
			Ext.getCmp('kelPasien').setValue('Perseorangan');
			jeniscus = '0';
		} else if (booking_online.data.JENIS_CUST == 1) {
			Combo_Select_prwj('Perusahaan');
			Ext.getCmp('kelPasien').setValue('Perusahaan');
			Ext.getCmp('cboAsuransi').setValue(booking_online.data.CUSTOMER);
			jeniscus = '1';
		} else if (booking_online.data.JENIS_CUST == 2) {
			Combo_Select_prwj('Asuransi');
			Ext.getCmp('kelPasien').setValue('Asuransi');
			Ext.getCmp('cboAsuransi').setValue(booking_online.data.CUSTOMER);
			jeniscus = '2';
		} else if (booking_online.data.JENIS_CUST == 3) {
			Combo_Select_prwj('Asuransi');
			Ext.getCmp('kelPasien').setValue('Dinas Sosial');
			Ext.getCmp('cboAsuransi').setValue(booking_online.data.CUSTOMER);
			jeniscus = '2';
		}
		RefreshDatacombo(booking_online.data.JENIS_CUST);
		jeniscus = booking_online.data.JENIS_CUST;
		selectSetAsuransi = booking_online.data.KD_CUSTOMER;
		tmp_nomor_skdp = booking_online.data.NOMOR_SKDP;
		booking_online = null;
		booking_online_status = true;
	} else {
		Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/data_kunjungan/kunjungan_terakhir",
			params: {
				kd_pasien: rowdata.KD_PASIEN,
				//kd_unit     : 3,
			},
			failure: function (o) {
				var cst = Ext.decode();
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				kodeDokterDefault = cst.kd_dokter;
				Ext.getCmp('cboDokterRequestEntry').setValue(cst.kd_dokter);
				if (cst.kd_customer != 0 || cst.kd_customer != '0') {
					Ext.getCmp('cboCustomer').setValue(cst.kd_customer);
					Kd_Customer = cst.kd_customer;
				} else {
					Ext.getCmp('cboCustomer').setValue('');
					Kd_Customer = '';
				}
				if (cst.count > 0 && cst.kd_rujukan > 0) {
					if (cst.kd_rujukan > 0 || cst.kd_rujukan != 'undefined') {
						Ext.getCmp('rb_Rujukan').setValue(true);
						// Ext.getCmp('cboRujukanDariRequestEntry').disable();
						pasienrujukan = true;
						// loaddatastorerujukan(cst.cara_penerimaan);	
						Ext.getCmp('cboRujukanDariRequestEntry').setValue(cst.cara_penerimaan);
						Ext.getCmp('cboRujukanRequestEntry').enable();

						loaddatastoredatarujukan(cst.kd_rujukan);
						Ext.getCmp('cboRujukanRequestEntry').setValue(cst.kd_rujukan);

						caranerima = cst.cara_penerimaan;
						kdrujukannya = cst.kd_rujukan;
						carapenerimaanpasien = cst.cara_penerimaan;
						koderujuk = cst.kd_rujukan;


					} else {
						carapenerimaanpasien = 99;
						koderujuk = 0;
						pasienrujukan = false;
					}

				} else {
					carapenerimaanpasien = 99;
					koderujuk = 0;
					pasienrujukan = false;
				}

				if (cst.jenis_cust == 0) {
					Combo_Select_prwj('Perseorangan');
					Ext.getCmp('kelPasien').setValue('Perseorangan');
					//Ext.getCmp('cboAsuransi').setValue(cst.kd_customer);
					jeniscus = '0';
				} else if (cst.jenis_cust == 1) {
					Combo_Select_prwj('Perusahaan');
					Ext.getCmp('kelPasien').setValue('Perusahaan');
					Ext.getCmp('cboAsuransi').setValue(cst.customer);
					jeniscus = '1';
				} else if (cst.jenis_cust == 2) {
					Combo_Select_prwj('Asuransi');
					Ext.getCmp('kelPasien').setValue('Asuransi');
					Ext.getCmp('cboAsuransi').setValue(cst.customer);
					//Ext.getCmp('txtNoAskes').setValue(cst.no_asuransi);
					jeniscus = '2';
				} else {
					Combo_Select_prwj('Dinas Sosial');
					Ext.getCmp('kelPasien').setValue('Dinas Sosial');
					Ext.getCmp('cboAsuransi').setValue(cst.customer);
					//Ext.getCmp('txtNoAskes').setValue(cst.no_asuransi);
					jeniscus = '3';
				}
				// tmp_sub_unit_rehab
				//Ext.getCmp('txtNoAskes').setValue(cst.no_sjp);
				Ext.getCmp('txtNoSJP').setValue(cst.no_sjp);
				jeniscus = cst.jenis_cust;
				RefreshDatacombo(cst.jenis_cust);
				selectSetAsuransi = cst.kd_customer;
			}
		});
		booking_online_status = false;
	}
	is_rujukan_rs = 'false';
}
function dataaddnew_viDaftar() {
	var nama = Ext.get('txtNamaPasien').getValue();
	var alamat = Ext.get('txtAlamatPasien').getValue();
	var tgl_lahir = Ext.get('txtNamaKeluargacari').getValue();
	Ext.getCmp('btnSimpanExit_viDaftar').enable();
	Ext.getCmp('btnSimpan_viDaftar').enable();
	Ext.getCmp('txtNama').focus('false', 10);
	loaddatastoredokter();
	//Ext.getCmp('cboDokterRequestEntry').setValue('-----');
	addNew_viDaftar = true;
	dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest').setValue('');
	Ext.getCmp('txtNama').setValue(nama);
	Ext.getCmp('txtNamaKeluarga').setValue('');
	Ext.getCmp('txtTempatLahir_Daftar').setValue('');
	Ext.getCmp('txtThnLahir').setValue('');
	Ext.getCmp('txtBlnLahir').setValue('');
	Ext.getCmp('txtHariLahir').setValue('');
	Ext.getCmp('txtAlamat').setValue(alamat);
	Ext.getCmp('txtJamKunjung').setValue(h + ':' + m + ':' + s);
	Ext.getCmp('txtNamaPeserta').setValue('');
	Ext.getCmp('txtNoAskes').setValue('');
	Ext.getCmp('txtNoSJP').setValue('');
	Ext.getCmp('txtnik').setValue();
	// dataCboRujukanDariRWJ('');
	Ext.getCmp('txtcatatan').setValue()
	Ext.getCmp('txtAnamnese_prwj').setValue('');
	Ext.getCmp('txtAlergi_prwj').setValue('');
	Ext.getCmp('txtNamaPerujuk').setValue('');
	Ext.getCmp('txtAlamatPerujuk').setValue('');
	Ext.getCmp('txtAlamatPenanggungjawab').setValue('');
	Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').setValue('');
	Ext.getCmp('txtTlpPenanggungjawab').setValue('');
	Ext.getCmp('cboJK_Daftar').setValue('');
	Ext.get('rb_datang').dom.checked = true;
	Ext.getCmp('cboRujukanDariRequestEntry').disable();
	Ext.getCmp('cboRujukanDariRequestEntry').setValue('');
	Ext.getCmp('cboRujukanRequestEntry').setValue('');
	Ext.getCmp('cboRujukanRequestEntry').disable();
	Ext.getCmp('cboAgamaRequestEntry').setValue('');
	Ext.getCmp('cboGolDarah_Daftar').setValue('');
	Ext.getCmp('cboPropinsiRequestEntry').setValue('');
	Ext.getCmp('cboKabupatenRequestEntry').setValue('');
	Ext.getCmp('cboKecamatanRequestEntry').setValue('');
	Ext.getCmp('cboPendidikanRequestEntry').setValue('');
	Ext.getCmp('cboStatusMarital_Daftar').setValue();
	Ext.getCmp('cboDokterRequestEntry').setValue('');
	Ext.getCmp('cboPerseorangan').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
	Ext.Ajax.request({
		url: baseURL + "index.php/main/defaultset_cus_pendaftaran",
		params: {
			command: '0'
		},
		success: function (o) {
			Ext.getCmp('cboAsuransi').setValue(o.responseText);
		}
	});
	Ext.Ajax.request({
		url: baseURL + "index.php/main/defaultset_kd_cus_pendaftaran",
		params: {
			command: '0'
		},
		success: function (o) {
			selectSetAsuransi = o.responseText;
		}
	});
	Ext.getCmp('cboPoliklinikRequestEntry').setValue('');
	Ext.getCmp('dtpTanggalLahir').setValue(now_viDaftar);
	Ext.getCmp('cboPekerjaanRequestEntry').setValue()
	Ext.getCmp('txtTmpPropinsi').setValue('');
	Ext.getCmp('txtTmpKabupaten').setValue('');
	Ext.getCmp('txtTmpKecamatan').setValue('');
	Ext.getCmp('txtTmpPendidikan').setValue('');
	Ext.getCmp('txtTmpPekerjaan').setValue('');
	Ext.getCmp('txtTmpAgama').setValue('');
	Ext.getCmp('txtAlamatktp').setValue('');
	Ext.getCmp('cboPropinsiKtp').setValue('');
	Ext.getCmp('cboKtpKabupaten').setValue('');
	Ext.getCmp('cboKecamatanKtp').setValue('');
	Ext.getCmp('cbokelurahanKtp').setValue('');
	Ext.getCmp('txtposktp').setValue('');
	Ext.getCmp('txtpos').setValue('');
	Ext.getCmp('cbokelurahan').setValue('');
	Ext.getCmp('txtNamaIbu').setValue('');
	kelompokpasien = "";
	rowSelected_viDaftar = undefined;
	Ext.Ajax.request({
		url: baseURL + "index.php/main/defaultset_pendaftaran",
		params: {
			command: '0'
		},
		success: function (o) {
			Ext.getCmp('cboRujukanDariRequestEntry').show();
			Ext.getCmp('cboRujukanRequestEntry').show();
			jeniscus = '0';
			RefreshDatacombo(jeniscus);
			Ext.getCmp('kelPasien').setValue(o.responseText);
			Combo_Select_prwj(o.responseText);
		}
	});
	Ext.get(cbWni).dom.checked = false;
	Ext.getCmp('txtNamaAyah').setValue('');
	Ext.getCmp('txttlpnPasien').setValue('');
	Ext.getCmp('txtnikPasien').setValue('');
	Ext.getCmp('txtHandphonePasien').setValue('');
	Ext.getCmp('txtEmailPasien').setValue('');
	Ext.getCmp('txtNamaPenanggungjawab').setValue('');
	Ext.getCmp('txtNoKtpPenanggungjawab').setValue('');
	Ext.getCmp('txtKotaPerujuk').setValue('');
	Ext.getCmp('txtTempatPenanggungjawab').setValue('');
	Ext.getCmp('btnSimpan_viDaftar').enable();
	Ext.getCmp('btnSimpanExit_viDaftar').enable();
	selectSetHubunganKeluargaPj = 1;
	jeniscus = '0';
	Pendidikanpasien = 1;
	PekerjaanPasien = 1;
	PendidikanAyah = 0;
	Ext.getCmp('cboPropinsiKtpPenanggungJawab').setValue();
	Ext.getCmp('cboKabupatenKtppenanggungjawab').setValue();
	Ext.getCmp('cboKecamatanKtpPenanggungJawab').setValue();
	Ext.getCmp('cbokelurahanKtpPenanggungJawab').setValue();
	Ext.getCmp('txtalamatktpPenanggungjawab').setValue();
	Ext.getCmp('cboHubunganKeluarga').setValue();
	Ext.getCmp('cboPendidikanPenanggungJawab').setValue();
	Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').setValue();
	Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').setValue();
	Ext.getCmp('txtEmailPenanggungjawab').setValue();
	Ext.getCmp('cboJK_DaftarPenanggungjawab').setValue();
	Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').setValue();
	Ext.getCmp('cboPropinsiPenanggungJawab').setValue();
	Ext.getCmp('cboKabupatenpenanggungjawab').setValue();
	Ext.getCmp('cboKecamatanPenanggungJawab').setValue();
	Ext.getCmp('cbokelurahanPenanggungJawab').setValue();
	Ext.getCmp('txtAlamatPenanggungjawab').setValue();
	Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').setValue();
	Ext.getCmp('txtTlpPenanggungjawab').setValue();
	Ext.getCmp('txtHpPenanggungjawab').setValue();
	Ext.getCmp('cboPropinsiKtpPenanggungJawab').setValue();
	Ext.getCmp('cboKabupatenKtppenanggungjawab').setValue();
	Ext.getCmp('cboKecamatanKtpPenanggungJawab').setValue();
	Ext.getCmp('cbokelurahanKtpPenanggungJawab').setValue();
	Ext.getCmp('txtalamatktpPenanggungjawab').setValue();
	Ext.getCmp('txtKdPosKtpPerusahaanPenanggungjawab').setValue();
	Ext.getCmp('cboHubunganKeluarga').setValue();
	Ext.getCmp('cboPendidikanPenanggungJawab').setValue();
	Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').setValue();
	Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').setValue();
	Ext.getCmp('txtEmailPenanggungjawab').setValue();
	Ext.getCmp('cboJK_DaftarPenanggungjawab').setValue();
	NoAsuransiPasien = '';
}
function dataparam_viDaftar() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/getcurrentshift",
		params: {
			command: '0'
		},
		success: function (o) {
			tampungshiftsekarang = o.responseText;
		}
	});
	var pasienBaru;
	var jenis_kelamin;
	var Kd_customer
	if (selectSetJK === 1 || selectSetJK === 't') {
		jenis_kelamin = true;
	} else {
		jenis_kelamin = false;
	}
	var tmpwarga;
	if (Ext.get(txtNoRequest).getValue() === '' || Ext.get(txtNoRequest).getValue() === 'Automatic from the system...') {
		pasienBaru = true;
	} else {
		pasienBaru = false;
	}
	/*if (selectSetAsuransi == ''){
		Kd_Customer = '0000000001';
	}else{
		Kd_Customer = selectSetAsuransi;
	}*/
	if (Ext.getCmp('cbokelurahanPenanggungJawab').getValue() == "" || Ext.getCmp('cbokelurahanPenanggungJawab').getValue() == "Pilih Kelurahan...") {

		Ext.getCmp('cbokelurahanPenanggungJawab').setValue("");
	}
	if (Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() == "" || Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() == "Pilih Kelurahan...") {
		Ext.getCmp('cbokelurahanKtpPenanggungJawab').setValue("");
	}
	if (Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() == "" || Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() == "Pilih Pekerjaan...") {
		Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').setValue(1);
	}
	if (Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() == "" || Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() == "Pilih Perusahaan...") {
		Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').setValue(1);
	}
	if (Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == "" || Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == "Pilih Jenis Kelamin...") {
		Ext.getCmp('cboJK_DaftarPenanggungjawab').setValue(1);
	}
	var jkpenanggungjawab;
	if (Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == 1) {
		jkpenanggungjawab = true;
	} else {
		jkpenanggungjawab = false;
	}
	if (Ext.getCmp('cboPendidikanPenanggungJawab').getValue() == "" || Ext.getCmp('cboPendidikanPenanggungJawab').getValue() == "Pilih Pendidikan...") {
		Ext.getCmp('cboPendidikanPenanggungJawab').setValue(1);
	}
	if (Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue() == "" || Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue() == "Pilih Status...") {
		Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').setValue(1);
	}
	var kunjunganpasien;
	var nonkunjungan;
	//var caranerima;
	var kdrujukannya;
	var dokter_dpjp;
	var tmp_kd_diagnosa;
	if (pasienrujukan === false) {
		caranerima = 99;
		kdrujukannya = 0;
	} else {
		caranerima = carapenerimaanpasien;
		kdrujukannya = koderujuk;
	}
	if (kode_dpjp_kunj2 == null || kode_dpjp_kunj2 == '') {
		console.log('ga ganti poli');
		dokter_dpjp = tmp_kd_dokter_dpjp;
	} else {
		console.log('ganti poli');
		dokter_dpjp = kode_dpjp_kunj2;
	}
	console.log(tmp_diagnosa);
	if (tmp_diagnosa == undefined || tmp_diagnosa == 'undefined') {
		tmp_diagnosa = 'null';
	}

	var tmp_no_sjp;
	if (kelompokpasien == 'Perseorangan') {
		tmp_no_sjp = "";
	} else {
		tmp_no_sjp = Ext.get('txtNoSJP').getValue();
	}
	var params_ViPendaftaran = {
		Table: 'ViewKunjungan',
		NoMedrec: Ext.get('txtNoRequest').getValue(),

		ppk_rujukan: tmp_ppk_rujukan,
		no_rujukan: Ext.getCmp('txtNoRujukan').getValue(),
		kd_dokter_dpjp: kd_dokter_dpjp_insert,

		NamaPasien: Ext.get('txtNama').getValue(),
		NamaKeluarga: Ext.get('txtNamaKeluarga').getValue(),
		JenisKelamin: jenis_kelamin,
		Tempatlahir: Ext.get('txtTempatLahir_Daftar').getValue(),
		TglLahir: Ext.get('dtpTanggalLahir').getValue(),
		Agama: selectAgamaRequestEntry,
		GolDarah: Ext.getCmp('cboGolDarah_Daftar').getValue(),
		StatusMarita: PasienSetSatusMarital, // Ext.get('cbWni').dom.checked,
		StatusWarga: Ext.getCmp('cbWni').getValue(),
		Alamat: Ext.get('txtAlamat').getValue(),
		No_Tlp: '',
		Pendidikan: Pendidikanpasien,
		Pekerjaan: PekerjaanPasien,
		suami_istrinya: Ext.getCmp('txtNamaSuamiIstri').getValue(),
		Pendidikan_Ayah: PendidikanAyah,
		Pekerjaan_Ayah: PekerjaanAyah,
		Pendidikan_Ibu: PendidikanIbu,
		Pekerjaan_Ibu: PekerjaanIbu,
		Pendidikan_SuamiIstri: PendidikanSuamiIstri,
		Pekerjaan_SuamiIstri: PekerjaanSuamiIstri,
		NamaPeserta: Ext.get('txtNamaPeserta').getValue(),
		KD_Asuransi: 1,
		NoAskes: Ext.get('txtNoAskes').getValue(),
		NoSjp: tmp_no_sjp,
		Kd_Suku: selectSukuRequestEntry,
		Jabatan: '',
		Perusahaan: Ext.getCmp('cboPerusahaanRequestEntry').getValue(),
		Perusahaan1: 0,
		Cek: kelompokpasien,
		// Poli: polipilihanpasien,
		Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
		TanggalMasuk: Ext.get('dtpTanggalKunjungan').getValue(),
		UrutMasuk: 0,
		JamKunjungan: now_viDaftar,
		//CaraPenerimaan: caranerima,
		CaraPenerimaan: carapenerimaanpasien,
		//KdRujukan: kdrujukannya,
		KdRujukan: koderujuk,
		KdRujukanClone: kdrujukannya,
		KdCustomer: Kd_Customer,
		KdDokter: kodeDokterDefault,//Ext.getCmp('cboDokterRequestEntry').getValue(),
		Baru: pasienBaru,
		Shift: tampungshiftsekarang,
		Karyawan: 0,
		Kontrol: false,
		Antrian: 0,
		NoSurat: '',
		Alergi: Ext.get('txtAlergi_prwj').getValue(),
		Anamnese: Ext.get('txtAnamnese_prwj').getValue(),
		TahunLahir: Ext.get('txtThnLahir').getValue(),
		BulanLahir: Ext.get('txtBlnLahir').getValue(),
		HariLahir: Ext.get('txtHariLahir').getValue(),
		Kota: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
		Kd_Kecamatan: selectKecamatanpasien,
		Kelurahan: kelurahanpasien,
		AsalPasien: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
		KDPROPINSI: Ext.getCmp('txtTmpPropinsi').getValue(),
		KDKABUPATEN: Ext.getCmp('txtTmpKabupaten').getValue(),
		KDKECAMATAN: Ext.getCmp('txtTmpKecamatan').getValue(),
		KDKECAMATANKTP: selectKecamatanktp,
		KDPENDIDIKAN: Pendidikanpasien,
		KDPEKERJAAN: PekerjaanPasien,
		KDAGAMA: selectAgamaRequestEntry,
		NAMA_PJ: Ext.getCmp('txtNamaPenanggungjawab').getValue(),
		AlamatPJ: Ext.getCmp('txtAlamatPenanggungjawab').getValue(),
		TgllahirPJ: Ext.getCmp('dtpPenanggungJawabTanggalLahir').getValue(),
		WNIPJ: Ext.getCmp('cbWniPenanggungJawab').getValue(),
		KelurahanPJ: Ext.getCmp('cbokelurahanPenanggungJawab').getValue(),
		PosPJ: Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').getValue(),
		TeleponPj: Ext.getCmp('txtTlpPenanggungjawab').getValue(),
		HpPj: Ext.getCmp('txtHpPenanggungjawab').getValue(),
		KTP: Ext.getCmp('txtNoKtpPenanggungjawab').getValue(),
		KelurahanKtpPJ: Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue(),
		AlamatKtpPJ: Ext.getCmp('txtalamatktpPenanggungjawab').getValue(),
		KdPosKtp: Ext.getCmp('txtKdPosKtpPerusahaanPenanggungjawab').getValue(),
		KdPendidikanPj: Ext.getCmp('cboPendidikanPenanggungJawab').getValue(),
		kdpekerjaanPj: Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue(),
		PerusahaanPj: Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue(),
		HubunganPj: selectSetHubunganKeluargaPj,
		AlamatKtpPasien: Ext.getCmp('txtAlamatktp').getValue(),
		KdKelurahanKtpPasien: selectkelurahanktp,
		KdPostKtpPasien: Ext.getCmp('txtposktp').getValue(),
		NamaAyahPasien: Ext.getCmp('txtNamaAyah').getValue(),
		NamaIbuPasien: Ext.getCmp('txtNamaIbu').getValue(),
		KdposPasien: Ext.getCmp('txtpos').getValue(),
		EmailPenanggungjawab: Ext.getCmp('txtEmailPenanggungjawab').getValue(),
		tempatlahirPenanggungjawab: Ext.getCmp('txtTempatPenanggungjawab').getValue(),
		StatusMaritalPenanggungjawab: Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue(),
		TLPNPasien: Ext.getCmp('txttlpnPasien').getValue(),
		nik: Ext.getCmp('txtnikPasien').getValue(),
		HPPasien: Ext.getCmp('txtHandphonePasien').getValue(),
		EmailPasien: Ext.getCmp('txtEmailPasien').getValue(),
		JKpenanggungJwab: jkpenanggungjawab,
		RadioRujukanPasien: radios.getValue().inputValue,
		PasienBaruRujukan: pasienBaru,
		NoNIK: Ext.getCmp('txtnik').getValue(),
		NonKunjungan: Ext.getCmp('cbnonkunjungan').getValue(),
		KdDiagnosa: tmp_diagnosa,
		part_number_nik: nik_pasien,
		unitMana: 'rwj',
		JenisRujukan: Ext.getCmp('cboRujukanDariRequestEntry').getValue(),
		catatanSep: Ext.getCmp('txtcatatan').getValue(),
		booking_online: booking_online_status,
		sub_unit_rehab: tmp_sub_unit_rehab,
	};
	return params_ViPendaftaran
}
function dataparamDelete_viDaftar() {
	var paramsDelete_ViPendaftaran = {
		Table: 'viKunjungan',
		NO_KUNJUNGAN: Ext.get('txtNoKunjungan_viDaftar').getValue(),
		KD_CUSTOMER: strKD_CUST
	}
	return paramsDelete_ViPendaftaran
}
function HapusBarisNgajar(nBaris) {
	if (CurrentData_viDaftar.row >= 0) {
		Ext.Msg.show({
			title: 'Hapus Baris',
			msg: 'Apakah baris ini akan dihapus ?',
			buttons: Ext.MessageBox.YESNO,
			fn: function (btn) {
				if (btn == 'yes') {
					DataDeletebaris_viDaftar()
					dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
					SELECTDATASTUDILANJUT = undefined;
				}
			},
			icon: Ext.MessageBox.QUESTION
		});
	} else {
		dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
		SELECTDATASTUDILANJUT = undefined;
	}
}
function DataDeletebaris_viDaftar() {
	Ext.Ajax.request({
		url: "./Datapool.mvc/DeleteDataObj",
		params: dataparam_viDaftar(),
		success: function (o) {
			var cst = o.responseText;
			if (cst == '{"success":true}') {
				ShowPesanInfo_viDaftar('Data berhasil dihapus', 'Hapus Data');
			} else {
				ShowPesanInfo_viDaftar('Data gagal dihapus', 'Hapus Data');
			}
		}
	});
}
var mRecord = Ext.data.Record.create([
	'NIP',
	'ID_STUDI_LANJUT',
	'NO_SURAT_STLNJ',
	'TGL_MULAI_SURAT',
	'TGL_AKHIR_SURAT',
	'ID_DANA',
	'SURAT_DARI',
	'TGL_SURAT',
	'THN_PERKIRAAN_LULUS'
]);
function getArrDetail_viDaftar() {
	var x = '';
	for (var i = 0; i < dsDetailSL_viDaftar.getCount(); i++) {
		var y = '';
		var z = '@@##$$@@';
		y += 'NIP=' + Ext.get('txtNPP_viDaftar').getValue()
		y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftar').getValue()
		y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftar.data.items[i].data.NO_SURAT_STLNJ
		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
		y += z + 'ID_DANA=' + dsDetailSL_viDaftar.data.items[i].data.ID_DANA
		y += z + 'SURAT_DARI=' + dsDetailSL_viDaftar.data.items[i].data.SURAT_DARI
		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftar.data.items[i].data.THN_PERKIRAAN_LULUS
		if (i === (dsDetailSL_viDaftar.getCount() - 1)) {
			x += y
		} else {
			x += y + '##[[]]##'
		}
	}
	return x;
}
function DatarefreshDetailSL_viDaftar(rowdataaparam) {
	dsDetailSL_viDaftar.load({
		params: {
			Skip: 0,
			Take: 50,
			Sort: '',
			Sortdir: 'ASC',
			target: 'viviewDetailStudiLanjut',
			param: rowdataaparam
		}
	});
	return dsDetailSL_viDaftar;
}
function getKriteriaCariLookup() {
	var strKriteria = "";
	if (Ext.get('txtKDPasien_viDaftar').getValue() != '') {
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%'"
	}
	if (Ext.get('txtNamaPs_viDaftar').getValue() != '') {
		if (strKriteria != "") {
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		} else {
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		}
	}
	if (Ext.get('txtAlamat_viDaftar').getValue() != '') {
		if (strKriteria != "") {
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		} else {
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		}
	}
	if (strKriteria != "") {
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	} else {
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	return strKriteria;
}
function dataprint_viDaftar() {
	var params_ViPendaftaran = {
		Table: 'ViewPrintBill',
		No_TRans: tmpnotransaksi,
		KdKasir: tmpkdkasir,
		printer: Ext.getCmp('cbopasienorder_printer_pendaftaranrwj').getValue()
	};
	return params_ViPendaftaran
}
function GetPrintKartu() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/LaporanDataObj",
		params: dataprint_viDaftar()
	});
}
function mComboJK_Daftar() {
	var cboJK_Daftar = new Ext.form.ComboBox({
		id: 'cboJK_Daftar',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		tabIndex: 5,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Jenis Kelamin...',
		fieldLabel: 'Jen. Kelamin',
		anchor: '99%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, 'Perempuan'], [1, 'Laki - Laki']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetJK,
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectSetJK = b.data.Id;
				Ext.getCmp('cboStatusMarital_Daftar').focus();
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						selectSetJK = c.value;
						Ext.getCmp('cboStatusMarital_Daftar').focus();
					} else if (e.getKey() == 9) {
						selectSetJK = c.value;
					}
				}, c);
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					selectSetJK = c.value;
					Ext.getCmp('cboStatusMarital_Daftar').focus();
				} else if (e.keyCode == 9) {
					selectSetJK = c.value;
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboJK_Daftar;
}
function mComboPenanggungjawabJK() {
	var cboJK_DaftarPenanggungjawab = new Ext.form.ComboBox({
		id: 'cboJK_DaftarPenanggungjawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		tabIndex: 69,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Jenis Kelamin...',
		fieldLabel: 'Jen. Kelamin',
		anchor: '95%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetJK,
		listeners: {
			'select': function (a, b, c) {
				selectSetJK = b.data.displayText;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboStatusMarital_Daftar').focus();
				}, c);
			}
		}
	});
	return cboJK_DaftarPenanggungjawab;
}
function mComboWargaNegara() {
	var cboWarga = new Ext.form.ComboBox({
		id: 'cboWarga',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hidden: true,
		emptyText: 'Pilih...',
		selectOnFocus: true,
		forceSelection: true,
		fieldLabel: 'Warga Negara ',
		width: 100,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[1, 'WNI'], [2, 'WNA']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetWarga,
		listeners: {
			'select': function (a, b, c) {
				selectSetWarga = b.data.displayText;
				GetDataWargaNegara(b.data.displayText)
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboStatusMarital_Daftar').focus();
				}, c);
			}
		}
	});
	return cboWarga;
}
function mComboHubunganKeluarga() {
	var cboHubunganKeluarga = new Ext.form.ComboBox({
		id: 'cboHubunganKeluarga',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: 'Pilih...',
		selectOnFocus: true,
		forceSelection: true,
		fieldLabel: 'Hub.Keluarga',
		width: 120,
		tabIndex: 55,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [
				[1, 'Anak Angkat'],
				[2, 'Anak Kandung'],
				[3, 'Anak Tiri'],
				[4, 'Ayah'],
				[5, 'Cucu'],
				[6, 'Famili'],
				[7, 'Ibu'],
				[8, 'Istri'],
				[9, 'Kep. Keluarga'],
				[10, 'Mertua'],
				[11, 'Pembantu'],
				[12, 'Suami'],
				[13, 'Sdr Kandung'],
				[14, 'Lain-Lain']
			]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetHubunganKeluarga,
		listeners: {
			'select': function (a, b, c) {
				selectSetHubunganKeluargaPj = b.data.Id;
				GetDataHubunganKeluarga(b.data.displayText)
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) //atau Ext.EventObject.ENTER
						Ext.getCmp('cboStatusMarital_Daftar').focus();
				}, c);
			}
		}
	});
	return cboHubunganKeluarga;
}
var StatusWargaNegara
function GetDataWargaNegara(warga) {
	var tampung = warga
	if (tampung === 'WNI') {
		StatusWargaNegara = 'true'
	} else {
		StatusWargaNegara = 'false'
	}
}
function mComboGolDarah_Daftar() {
	var cboGolDarah_Daftar = new Ext.form.ComboBox({
		id: 'cboGolDarah_Daftar',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		tabIndex: 4,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Gol. Darah...',
		fieldLabel: 'G.Darah',
		width: 50,
		anchor: '99%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-']/*, [7, 'AB-'], [8, 'O-']*/]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		enableKeyEvents: true,
		value: selectSetGolDarah,
		listeners: {
			'select': function (a, b, c) {
				selectSetGolDarah = b.data.Id;
				Ext.getCmp('cboJK_Daftar').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					selectSetGolDarah = c.value;
					Ext.getCmp('cboJK_Daftar').focus();
				} else if (e.keyCode == 9) {
					selectSetGolDarah = c.value;
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboGolDarah_Daftar;
}
function loadComboCustomer(params) {
	var criteria = "";
	console.log(params);
	if (params == undefined) {
		criteria = '';
	} else {
		criteria = 'jenis_cust=~' + params + '~';
	}

	var Field = ['KD_CUSTOMER', 'CUSTOMER'];
	dsComboCustomer = new WebApp.DataStore({ fields: Field });
	dsComboCustomer.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'customer',
			Sortdir: 'ASC',
			target: 'ViewComboCustomer',
			param: criteria
		}
	});
}
function mComboCustomer() {
	loadComboCustomer();
	// console.log(ds_customer_viDaftar);
	
	var cboCustomer = new Ext.form.ComboBox({
		id: 'cboCustomer',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Silahkan Pilih...',
		fieldLabel: 'Customer',
		tabIndex: 23,
		width: 50,
		anchor: '95%',
		enableKeyEvents: true,
		store: dsComboCustomer,
		valueField: 'KD_CUSTOMER',
		displayField: 'CUSTOMER',

		listeners: {
			select: function (a, b, c) {
				Kd_Customer = b.data.KD_CUSTOMER;
				Ext.Ajax.request({
					url: baseURL + "index.php/rawat_jalan/Rawat_jalan/getKontraktor",
					params: {
						kd_customer: b.data.KD_CUSTOMER
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						console.log("aaa"+cst.data[0].JENIS_CUST );
						if (cst.status === true) {
							if (cst.data[0].JENIS_CUST == 0) {
								Ext.getCmp('txtDiagnosa_RWJ').focus(true, 10);
								Combo_Select_prwj('Perseorangan');
								Ext.getCmp('kelPasien').setValue('Perseorangan');
								kelompokpasien = 'Perseorangan';
								jeniscus = '0';
							} else if (cst.data[0].JENIS_CUST == 1) {
								Combo_Select_prwj('Perusahaan');
								Ext.getCmp('cboRujukanRequestEntry').focus(true, 10);
								Ext.getCmp('kelPasien').setValue('Perusahaan');
								jeniscus = '1';
								kelompokpasien = 'Perusahaan';
							} else if (cst.data[0].JENIS_CUST == 2) {
								Ext.getCmp('cboRujukanRequestEntry').focus(true, 10);
								Ext.getCmp('kelPasien').setValue('Asuransi');
								Combo_Select_prwj('Asuransi');
								jeniscus = '2';
								kelompokpasien = 'Asuransi';
							} else if (cst.data[0].JENIS_CUST == 3) {
								Ext.getCmp('cboRujukanRequestEntry').focus(true, 10);
								Ext.getCmp('kelPasien').setValue('Dinas Sosial');
								Combo_Select_prwj('Dinas Sosial');
								jeniscus = '2';
								kelompokpasien = 'Dinas Sosial';
							}
						}
					},
				});
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboRujukanRequestEntry').focus(true, 10);
				}
			},
			/*keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}*/
		}
	});
	return cboCustomer;
}
function mComboPerseorangan() {
	var cboPerseorangan = new Ext.form.ComboBox({
		id: 'cboPerseorangan',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		hidden: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Silahkan Pilih...',
		fieldLabel: 'Jenis',
		tabIndex: 23,
		width: 50,
		anchor: '95%',
		value: 1,
		enableKeyEvents: true,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[1, 'Umum']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetPerseorangan,
		listeners: {
			'select': function (a, b, c) {
				selectSetPerseorangan = b.data.displayText;
				Ext.getCmp('nci-gen1').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13 || e.keyCode == 9) {
					Ext.getCmp('nci-gen1').focus();
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboPerseorangan;
}
function mComboSatusMarital_Daftar() {
	var cboStatusMarital_Daftar = new Ext.form.ComboBox({
		id: 'cboStatusMarital_Daftar',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		tabIndex: 6,
		forceSelection: true,
		emptyText: 'Pilih Status...',
		fieldLabel: 'Status Marital ',
		anchor: '95%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetSatusMarital,
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				PasienSetSatusMarital = b.data.Id;
				Ext.getCmp('txtTempatLahir_Daftar').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					PasienSetSatusMarital = c.value;
					Ext.getCmp('txtTempatLahir_Daftar').focus();
				} else if (e.keyCode == 9) {
					PasienSetSatusMarital = c.value;
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboStatusMarital_Daftar;
}
function mComboStatusMaritalPenanggungjawab() {
	var cboStatusMarital_DaftarPenanggungjawab = new Ext.form.ComboBox({
		id: 'cboStatusMarital_DaftarPenanggungjawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		tabIndex: 43,
		forceSelection: true,
		emptyText: 'Pilih Status...',
		fieldLabel: 'Status Marital ',
		anchor: '95%',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		listeners: {
			'select': function (a, b, c) {
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('txtAlamat').focus();
				}, c);
			}
		}
	});
	return cboStatusMarital_DaftarPenanggungjawab;
}
function mComboNamaRujukan() {
	var cboNamaRujukan = new Ext.form.ComboBox({
		id: 'cboNamaRujukan',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Rujukan...',
		fieldLabel: 'Nama ',
		width: 110,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields: ['Id', 'displayText'],
			data: [[1, ''], [2, ''], [3, ''], [4, '']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value: selectSetNamaRujukan,
		listeners: {
			'select': function (a, b, c) {
				selectSetNamaRujukan = b.data.displayText;
			}
		}
	});
	return cboNamaRujukan;
}
function mComboPoli_viDaftar(lebar, Nama_ID) {
	var Field_poli_viDaftar = ['KD_UNIT', 'NAMA_UNIT'];
	ds_Poli_viDaftar = new WebApp.DataStore({ fields: Field_poli_viDaftar });
	ds_Poli_viDaftar.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA_UNIT',
			Sortdir: 'ASC',
			target: 'ViewSetupUnitB',
			param: "kd_bagian=2 and type='0'"
		}
	});
	var cbo_Poli_viDaftar = new Ext.form.ComboBox({
		flex: 1,
		fieldLabel: 'Poli',
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		store: ds_Poli_viDaftar,
		width: lebar,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Poli...',
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		name: Nama_ID,
		lazyRender: true,
		id: 'cboPoliviDaftar'
	});
	return cbo_Poli_viDaftar;
}
function mComboAgamaRequestEntry() {
	var Field = ['KD_AGAMA', 'AGAMA'];
	dsAgamaRequestEntry = new WebApp.DataStore({ fields: Field });
	dsAgamaRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'agama',
			Sortdir: 'ASC',
			target: 'ViewComboAgama',
			param: ''
		}
	});
	var cboAgamaRequestEntry = new Ext.form.ComboBox({
		id: 'cboAgamaRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Agama...',
		fieldLabel: 'Agama',
		align: 'Right',
		enableKeyEvents: true,
		tabIndex: 3,
		store: dsAgamaRequestEntry,
		valueField: 'KD_AGAMA',
		displayField: 'AGAMA',
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				selectAgamaRequestEntry = b.data.KD_AGAMA;
				Ext.getCmp('cboGolDarah_Daftar').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboGolDarah_Daftar').focus();
				} else if (e.keyCode == 9) {
					selectAgamaRequestEntry = c.value;
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboAgamaRequestEntry;
}
function mComboPropinsiRequestEntry() {
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPropinsiRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsiRequestEntry = new Ext.form.ComboBox({
		id: 'cboPropinsiRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '99%',
		tabIndex: 18,
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupaten(b.data.KD_PROPINSI);
				Ext.getCmp('cboPropinsiKtp').setValue(a.getValue());
				Ext.getCmp('cboKabupatenRequestEntry').focus();
			}, keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboKabupatenRequestEntry').focus();
					selectPropinsiRequestEntry = c.value;
					Ext.getCmp('cboPropinsiKtp').setValue(c.getValue());
				} else if (e.keyCode == 9) {
					selectPropinsiRequestEntry = c.value;
					Ext.getCmp('cboKabupatenRequestEntry').focus();
				}
			}, keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}, blur: function (a) {
				Ext.getCmp('cboPropinsiKtp').setValue(a.getValue());
			}
		}
	});
	return cboPropinsiRequestEntry;
}
function mComboPropinsiKtp() {
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPropinsiRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsiKtp = new Ext.form.ComboBox({
		id: 'cboPropinsiKtp',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih  Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi Ktp',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '99%',
		tabIndex: 24,
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectPropinsiKtp = b.data.KD_PROPINSI;
				loaddatastoreKtpkabupaten(b.data.KD_PROPINSI);
				Ext.getCmp('cboKtpKabupaten').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboKtpKabupaten').focus();
					selectPropinsiKtp = c.value;
				} else if (e.keyCode == 9) {
					selectPropinsiKtp = c.value;
					Ext.getCmp('cboKtpKabupaten').focus();
				}
			}
		}
	});
	return cboPropinsiKtp;
}
function mComboPropinsiPenanggungJawab() {
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPropinsiRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsiPenanggungJawab = new Ext.form.ComboBox({
		id: 'cboPropinsiPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'pilih  Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '95%',
		tabIndex: 44,
		listeners: {
			'select': function (a, b, c) {
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupatenpenanggungjawab(b.data.KD_PROPINSI);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('cboKabupatenRequestEntry').focus();
					} else if (e.getKey() == 9) {
						dataKabupaten = c.value;
						loaddatastorekabupatenpenanggungjawab(dataKabupaten);
					}
				}, c);
			}
		}
	});
	return cboPropinsiPenanggungJawab;
}
function mComboPropinsiKTPPenanggungJawab() {
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPropinsiRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsiKtpPenanggungJawab = new Ext.form.ComboBox({
		id: 'cboPropinsiKtpPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '95%',
		tabIndex: 51,
		listeners: {
			'select': function (a, b, c) {
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupatenKtppenanggungjawab(b.data.KD_PROPINSI);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('cboKabupatenRequestEntry').focus();
					} else if (e.getKey() == 9) {
						dataKabupaten = c.value;
						loaddatastorekabupatenKtppenanggungjawab(dataKabupaten);
					}
				}, c);
			}
		}
	});
	return cboPropinsiKtpPenanggungJawab;
}
function loaddatastorekabupaten(kd_propinsi) {
	dsKabupatenRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kabupaten',
			Sortdir: 'ASC',
			target: 'ViewComboKabupaten',
			param: 'kd_propinsi=' + kd_propinsi
		}
	});
}
function loaddatastoreKtpkabupaten(kd_propinsi) {
	dsKabupatenKtp.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kabupaten',
			Sortdir: 'ASC',
			target: 'ViewComboKabupaten',
			param: 'kd_propinsi=' + kd_propinsi
		}
	});
}
function loaddatastorekabupatenpenanggungjawab(kd_propinsi) {
	dsKabupatenpenanggungjawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kabupaten',
			Sortdir: 'ASC',
			target: 'ViewComboKabupaten',
			param: 'kd_propinsi=' + kd_propinsi
		}
	});
}
function loaddatastorekabupatenKtppenanggungjawab(kd_propinsi) {
	dsKabupatenKtppenanggungjawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kabupaten',
			Sortdir: 'ASC',
			target: 'ViewComboKabupaten',
			param: 'kd_propinsi=' + kd_propinsi
		}
	});
}
function mComboKabupatenRequestEntry() {
	var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
	dsKabupatenRequestEntry = new WebApp.DataStore({ fields: Field });
	var cboKabupatenRequestEntry = new Ext.form.ComboBox({
		id: 'cboKabupatenRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		selectOnFocus: true,
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dsKabupatenRequestEntry,
		valueField: 'KD_KABUPATEN',
		displayField: 'KABUPATEN',
		tabIndex: 19,
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
				loaddatastorekecamatan(b.data.KD_KABUPATEN);
				Ext.getCmp('cboKtpKabupaten').setValue(a.getValue());
				Ext.getCmp('cboKecamatanRequestEntry').focus(false);
			}, keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}, keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboKtpKabupaten').setValue(c.getValue());
					Ext.getCmp('cboKecamatanRequestEntry').focus(false);
					selectKabupatenRequestEntry = c.value;
				} else if (e.keyCode == 9) {
					selectKabupatenRequestEntry = c.value;
					Ext.getCmp('cboKecamatanRequestEntry').focus(false);
				}
			}, blur: function (a) {
				Ext.getCmp('cboKtpKabupaten').setValue(a.getValue());
			}
		}
	});
	return cboKabupatenRequestEntry;
}
function mComboKtpKabupaten() {
	var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
	dsKabupatenKtp = new WebApp.DataStore({ fields: Field });
	var cboKtpKabupaten = new Ext.form.ComboBox({
		id: 'cboKtpKabupaten',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod Ktp',
		align: 'Right',
		store: dsKabupatenKtp,
		valueField: 'KD_KABUPATEN',
		displayField: 'KABUPATEN',
		tabIndex: 25,
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKabupatenKTP = b.data.KD_KABUPATEN;
				loaddatastoreKtpkecamatan(b.data.KD_KABUPATEN);
				/* Ext.getCmp('cboKecamatanKtp').setValue();
				Ext.getCmp('cbokelurahanKtp').setValue(); */
				Ext.getCmp('cboKecamatanKtp').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboKecamatanKtp').focus();
					selectKabupatenKTP = c.value;
					//loaddatastoreKtpkecamatan(selectKabupatenKTP);
				} else if (e.keyCode == 9) {
					selectKabupatenKTP = c.value;
					Ext.getCmp('cboKecamatanKtp').focus();
					//loaddatastoreKtpkecamatan(selectKabupatenKTP);
				}
			}
		}
	});
	return cboKtpKabupaten;
}
function mComboKabupatenpenanggungjawab() {
	var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
	dsKabupatenpenanggungjawab = new WebApp.DataStore({ fields: Field });
	var cboKabupatenpenanggungjawab = new Ext.form.ComboBox({
		id: 'cboKabupatenpenanggungjawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dsKabupatenpenanggungjawab,
		valueField: 'KD_KABUPATEN',
		displayField: 'KABUPATEN',
		tabIndex: 45,
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
				loaddatastorekecamatanpenanggungjawab(b.data.KD_KABUPATEN);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('cboKecamatanRequestEntry').focus();
					} else if (e.getKey() == 9) {
						var dataKecamatan = c.value;
						loaddatastorekecamatanpenanggungjawab(dataKecamatan);
					}
				}, c);
			}
		}
	});
	return cboKabupatenpenanggungjawab;
}
function mComboKabupatenKtppenanggungjawab() {
	var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
	dsKabupatenKtppenanggungjawab = new WebApp.DataStore({ fields: Field });
	var cboKabupatenKtppenanggungjawab = new Ext.form.ComboBox({
		id: 'cboKabupatenKtppenanggungjawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dsKabupatenKtppenanggungjawab,
		valueField: 'KD_KABUPATEN',
		displayField: 'KABUPATEN',
		tabIndex: 51,
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
				loaddatastoreKtpkecamatanpenanggungjawab(b.data.KD_KABUPATEN);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('cboKecamatanRequestEntry').focus();
					} else if (e.getKey() == 9) {
						var dataKecamatan = c.value;
						loaddatastoreKtpkecamatanpenanggungjawab(dataKecamatan);
					}
				}, c);
			}
		}
	});
	return cboKabupatenKtppenanggungjawab;
}
function loaddatastorekecamatan(kd_kabupaten) {
	dsKecamatanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kecamatan',
			Sortdir: 'ASC',
			target: 'ViewComboKecamatan',
			param: 'kd_kabupaten=' + kd_kabupaten
		}
	});
}
function loaddatastoreKtpkecamatan(kd_kabupaten) {
	dsKecamatanKtp.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kecamatan',
			Sortdir: 'ASC',
			target: 'ViewComboKecamatan',
			param: 'kd_kabupaten=' + kd_kabupaten
		}
	});
}
function loaddatastorekecamatanpenanggungjawab(kd_kabupaten) {
	dsKecamatanPenanggungJawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kecamatan',
			Sortdir: 'ASC',
			target: 'ViewComboKecamatan',
			param: 'kd_kabupaten=' + kd_kabupaten
		}
	});
}
function loaddatastoreKtpkecamatanpenanggungjawab(kd_kabupaten) {
	dsKecamatanKtpPenanggungJawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kecamatan',
			Sortdir: 'ASC',
			target: 'ViewComboKecamatan',
			param: 'kd_kabupaten=' + kd_kabupaten
		}
	});
}
function mComboKecamatanRequestEntry() {
	var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
	dsKecamatanRequestEntry = new WebApp.DataStore({ fields: Field });
	var cboKecamatanRequestEntry = new Ext.form.ComboBox({
		id: 'cboKecamatanRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 20,
		store: dsKecamatanRequestEntry,
		valueField: 'KD_KECAMATAN',
		displayField: 'KECAMATAN',
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKecamatanpasien = b.data.KD_KECAMATAN;
				loaddatastorekelurahan(b.data.KD_KECAMATAN);
				//Ext.getCmp('cbokelurahan').setValue();
				Ext.getCmp('cboKecamatanKtp').setValue(a.getValue());
				Ext.getCmp('cbokelurahan').focus(false)
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cbokelurahan').focus(false);
					selectKecamatanpasien = c.value;
					Ext.getCmp('cboKecamatanKtp').setValue(c.getValue());
				} else if (e.keyCode == 9) {
					Ext.getCmp('cbokelurahan').focus(false);
					//loaddatastorekelurahan(c.value);
					selectKecamatanpasien = c.value;
				}
			}, blur: function (a) {
				Ext.getCmp('cboKecamatanKtp').setValue(a.getValue());
			}
		}
	});
	return cboKecamatanRequestEntry;
}
function mComboKecamatanktp() {
	var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
	dsKecamatanKtp = new WebApp.DataStore({ fields: Field });
	var cboKecamatanKtp = new Ext.form.ComboBox({
		id: 'cboKecamatanKtp',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 26,
		store: dsKecamatanKtp,
		valueField: 'KD_KECAMATAN',
		displayField: 'KECAMATAN',
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKecamatanktp = b.data.KD_KECAMATAN;
				loaddatastoreKtpkelurahan(b.data.KD_KECAMATAN);
				Ext.getCmp('cbokelurahanKtp').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					selectKecamatanktp = c.value;
					Ext.getCmp('cbokelurahanKtp').focus();
				} else if (e.keyCode == 9) {
					selectKecamatanktp = c.value;
					Ext.getCmp('cbokelurahanKtp').focus(false);
				}
			}
		}
	});
	return cboKecamatanKtp;
}
function loaddatastorekelurahanpenanggungjawab(kd_kecamatan) {
	dsKelurahanPenanggungJawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kelurahan',
			Sortdir: 'ASC',
			target: 'ViewComboKelurahan',
			param: 'kd_kecamatan=' + kd_kecamatan
		}
	});
}
function loaddatastorekelurahan(kd_kecamatan) {
	dsKelurahan.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kelurahan',
			Sortdir: 'ASC',
			target: 'ViewComboKelurahan',
			param: 'kd_kecamatan=' + kd_kecamatan
		}
	});
}
function loaddatastoreKtpkelurahan(kd_kecamatan) {
	dsKtpKelurahan.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kelurahan',
			Sortdir: 'ASC',
			target: 'ViewComboKelurahan',
			param: 'kd_kecamatan=' + kd_kecamatan
		}
	});
}
function loaddatastorekelurahanKtppenanggungjawab(kd_kecamatan) {
	dsKelurahanKtpPenanggungJawab.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kelurahan',
			Sortdir: 'ASC',
			target: 'ViewComboKelurahan',
			param: 'kd_kecamatan=' + kd_kecamatan
		}
	});
}
function mComboKecamatanPenanggungJawab() {
	var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
	dsKecamatanPenanggungJawab = new WebApp.DataStore({ fields: Field });
	var cboKecamatanPenanggungJawab = new Ext.form.ComboBox({
		id: 'cboKecamatanPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 46,
		store: dsKecamatanPenanggungJawab,
		valueField: 'KD_KECAMATAN',
		displayField: 'KECAMATAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				loaddatastorekelurahanpenanggungjawab(b.data.KD_KECAMATAN);
				selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboPendidikanRequestEntry').focus();
				}, c);
			}
		}
	});
	return cboKecamatanPenanggungJawab;
}
function mCombokelurahanPenanggungJawab() {
	var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
	dsKelurahanPenanggungJawab = new WebApp.DataStore({ fields: Field });
	var cbokelurahanPenanggungJawab = new Ext.form.ComboBox({
		id: 'cbokelurahanPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kelurahan...',
		fieldLabel: 'Kelurahan',
		align: 'Right',
		tabIndex: 47,
		store: dsKelurahanPenanggungJawab,
		valueField: 'KD_KELURAHAN',
		displayField: 'KELURAHAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboPendidikanRequestEntry').focus();
				}, c);
			}
		}
	});
	return cbokelurahanPenanggungJawab;
}
function mCombokelurahan() {
	var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
	dsKelurahan = new WebApp.DataStore({ fields: Field });
	var cbokelurahan = new Ext.form.ComboBox({
		id: 'cbokelurahan',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kelurahan...',
		fieldLabel: 'Kelurahan',
		align: 'Right',
		tabIndex: 21,
		store: dsKelurahan,
		valueField: 'KD_KELURAHAN',
		displayField: 'KELURAHAN',
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				kelurahanpasien = b.data.KD_KELURAHAN;
				Ext.getCmp('txtpos').focus();
				Ext.getCmp('cbokelurahanKtp').setValue(a.getValue());
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cbokelurahanKtp').setValue(c.getValue());
					Ext.getCmp('txtpos').focus();
				} else if (e.keyCode == 9) {
					kelurahanpasien = c.value;
				}
			}, blur: function (a) {
				Ext.getCmp('cbokelurahanKtp').setValue(a.getValue());
			}
		}
	});
	return cbokelurahan;
}
function mComboKtpkelurahan() {
	var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
	dsKtpKelurahan = new WebApp.DataStore({ fields: Field });
	var cbokelurahanKtp = new Ext.form.ComboBox({
		id: 'cbokelurahanKtp',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kelurahan...',
		fieldLabel: 'Kelurahan',
		align: 'Right',
		tabIndex: 27,
		store: dsKtpKelurahan,
		valueField: 'KD_KELURAHAN',
		displayField: 'KELURAHAN',
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectkelurahanktp = b.data.KD_KELURAHAN;
				Ext.getCmp('txtposktp').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('txtposktp').focus();
				} else if (e.keyCode == 9) {
					selectkelurahanktp = c.value;
				}
			}
		}
	});
	return cbokelurahanKtp;
}
function mCombokelurahanKtpPenanggungJawab() {
	var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
	dsKelurahanKtpPenanggungJawab = new WebApp.DataStore({ fields: Field });
	var cbokelurahanKtpPenanggungJawab = new Ext.form.ComboBox({
		id: 'cbokelurahanKtpPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kelurahan...',
		fieldLabel: 'Kelurahan',
		align: 'Right',
		tabIndex: 51,
		store: dsKelurahanKtpPenanggungJawab,
		valueField: 'KD_KELURAHAN',
		displayField: 'KELURAHAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboPendidikanRequestEntry').focus();
				}, c);
			}
		}
	});
	return cbokelurahanKtpPenanggungJawab;
}
function mComboKecamatanKtpPenanggungJawab() {
	var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
	dsKecamatanKtpPenanggungJawab = new WebApp.DataStore({ fields: Field });
	var cboKecamatanKtpPenanggungJawab = new Ext.form.ComboBox({
		id: 'cboKecamatanKtpPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih  Kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 51,
		store: dsKecamatanKtpPenanggungJawab,
		valueField: 'KD_KECAMATAN',
		displayField: 'KECAMATAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
				loaddatastorekelurahanKtppenanggungjawab(b.data.KD_KECAMATAN);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) Ext.getCmp('cboPendidikanRequestEntry').focus();
				}, c);
			}
		}
	});
	return cboKecamatanKtpPenanggungJawab;
}
function mComboPendidikanPenanggungJawab() {
	var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
	dsPendidikanRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPendidikanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pendidikan',
			Sortdir: 'ASC',
			target: 'ViewComboPendidikan',
			param: ''
		}
	});
	var cboPendidikanPenanggungJawab = new Ext.form.ComboBox({
		id: 'cboPendidikanPenanggungJawab',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pendidikan...',
		fieldLabel: 'Pendidikan',
		align: 'Right',
		store: dsPendidikanRequestEntry,
		valueField: 'KD_PENDIDIKAN',
		displayField: 'PENDIDIKAN',
		tabIndex: 56,
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				var selectPendidikanRequestEntry = b.data.KD_PROPINSI;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) Ext.getCmp('cboPekerjaanRequestEntry').focus();
				}, c);
			}
		}
	});
	return cboPendidikanPenanggungJawab;
}
function mComboPendidikanRequestEntry() {
	var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
	dsPendidikanRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPendidikanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pendidikan',
			Sortdir: 'ASC',
			target: 'ViewComboPendidikan',
			param: ''
		}
	});
	var cboPendidikanRequestEntry = new Ext.form.ComboBox({
		id: 'cboPendidikanRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pendidikan...',
		fieldLabel: 'Pendidikan',
		align: 'Right',
		store: dsPendidikanRequestEntry,
		valueField: 'KD_PENDIDIKAN',
		displayField: 'PENDIDIKAN',
		tabIndex: 15,
		anchor: '95%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				Pendidikanpasien = b.data.KD_PENDIDIKAN;
				Ext.getCmp('cboPekerjaanRequestEntry').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboPekerjaanRequestEntry').focus();
				}
			}
		}
	});
	return cboPendidikanRequestEntry;
}
function mComboPendidikanAyahRequestEntry() {
	var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
	dsPendidikanAyahRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPendidikanAyahRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pendidikan',
			Sortdir: 'ASC',
			target: 'ViewComboPendidikan',
			param: ''
		}
	});
	var cboPendidikanAyahRequestEntry = new Ext.form.ComboBox({
		id: 'cboPendidikanAyahRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pendidikan...',
		fieldLabel: 'Pendidikan',
		align: 'Right',
		store: dsPendidikanAyahRequestEntry,
		valueField: 'KD_PENDIDIKAN',
		displayField: 'PENDIDIKAN',
		tabIndex: 15,
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PendidikanAyah = b.data.KD_PENDIDIKAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						PendidikanAyah = c.value;
						Ext.getCmp('cboPekerjaanAyahRequestEntry').focus();
					} else if (e.getKey() == 9) {
						PendidikanAyah = c.value;
					}
				}, c);
			}
		}
	});
	return cboPendidikanAyahRequestEntry;
}
function mComboPekerjaanAyahRequestEntry() {
	var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
	dsPekerjaanAyahRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPekerjaanAyahRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pekerjaan',
			Sortdir: 'ASC',
			target: 'ViewComboPekerjaan',
			param: ''
		}
	})
	var cboPekerjaanAyahRequestEntry = new Ext.form.ComboBox({
		id: 'cboPekerjaanAyahRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pekerjaan...',
		fieldLabel: 'Pekerjaan',
		align: 'Right',
		tabIndex: 16,
		store: dsPekerjaanAyahRequestEntry,
		valueField: 'KD_PEKERJAAN',
		displayField: 'PEKERJAAN',
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PekerjaanAyah = b.data.KD_PEKERJAAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('txtNamaIbu').focus();
						PekerjaanAyah = c.value;
					} else if (e.getKey() == 9) {
						PekerjaanAyah = c.value;
					}
				}, c);
			}
		}
	});
	return cboPekerjaanAyahRequestEntry;
}
function mComboPendidikanIbuRequestEntry() {
	var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
	dsPendidikanIbuRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPendidikanIbuRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pendidikan',
			Sortdir: 'ASC',
			target: 'ViewComboPendidikan',
			param: ''
		}
	});
	var cboPendidikanIbuRequestEntry = new Ext.form.ComboBox({
		id: 'cboPendidikanIbuRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pendidikan...',
		fieldLabel: 'Pendidikan',
		align: 'Right',
		store: dsPendidikanIbuRequestEntry,
		valueField: 'KD_PENDIDIKAN',
		displayField: 'PENDIDIKAN',
		tabIndex: 15,
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PendidikanIbu = b.data.KD_PENDIDIKAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						PendidikanIbu = c.value;
						Ext.getCmp('cboPekerjaanIbuRequestEntry').focus();
					} else if (e.getKey() == 9) {
						PendidikanIbu = c.value;
					}
				}, c);
			}
		}
	});
	return cboPendidikanIbuRequestEntry;
}
function mComboPekerjaanIbuRequestEntry() {
	var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
	dsPekerjaanIbuRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPekerjaanIbuRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pekerjaan',
			Sortdir: 'ASC',
			target: 'ViewComboPekerjaan',
			param: ''
		}
	});
	var cboPekerjaanIbuRequestEntry = new Ext.form.ComboBox({
		id: 'cboPekerjaanIbuRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pekerjaan...',
		fieldLabel: 'Pekerjaan',
		align: 'Right',
		tabIndex: 16,
		store: dsPekerjaanIbuRequestEntry,
		valueField: 'KD_PEKERJAAN',
		displayField: 'PEKERJAAN',
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PekerjaanIbu = b.data.KD_PEKERJAAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						Ext.getCmp('txtNamaSuamiIstri').focus();
						PekerjaanIbu = c.value;
					} else if (e.getKey() == 9) {
						PekerjaanIbu = c.value;
					}
				}, c);
			}
		}
	});
	return cboPekerjaanIbuRequestEntry;
}
function mComboPendidikanSuamiIstriRequestEntry() {
	var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
	dsPendidikanSuamiIstriRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPendidikanSuamiIstriRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pendidikan',
			Sortdir: 'ASC',
			target: 'ViewComboPendidikan',
			param: ''
		}
	});
	var cboPendidikanSuamiIstriRequestEntry = new Ext.form.ComboBox({
		id: 'cboPendidikanSuamiIstriRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pendidikan...',
		fieldLabel: 'Pendidikan',
		align: 'Right',
		store: dsPendidikanSuamiIstriRequestEntry,
		valueField: 'KD_PENDIDIKAN',
		displayField: 'PENDIDIKAN',
		tabIndex: 15,
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PendidikanSuamiIstri = b.data.KD_PENDIDIKAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						PendidikanSuamiIstri = c.value;
						Ext.getCmp('cboPekerjaanSuamiIstriRequestEntry').focus();
					} else if (e.getKey() == 9) {
						PendidikanSuamiIstri = c.value;
					}
				}, c);
			}
		}
	});
	return cboPendidikanSuamiIstriRequestEntry;
}
function mComboPekerjaanSuamiIstriRequestEntry() {
	var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
	dsPekerjaanSuamiIstriRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPekerjaanSuamiIstriRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pekerjaan',
			Sortdir: 'ASC',
			target: 'ViewComboPekerjaan',
			param: ''
		}
	});
	var cboPekerjaanSuamiIstriRequestEntry = new Ext.form.ComboBox({
		id: 'cboPekerjaanSuamiIstriRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pekerjaan...',
		fieldLabel: 'Pekerjaan',
		align: 'Right',
		tabIndex: 16,
		store: dsPekerjaanSuamiIstriRequestEntry,
		valueField: 'KD_PEKERJAAN',
		displayField: 'PEKERJAAN',
		anchor: '99%',
		listeners: {
			'select': function (a, b, c) {
				PekerjaanSuamiIstri = b.data.KD_PEKERJAAN;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) {
						PekerjaanSuamiIstri = c.value;
					} else if (e.getKey() == 9) {
						PekerjaanSuamiIstri = c.value;
					}
				}, c);
			}
		}
	});
	return cboPekerjaanSuamiIstriRequestEntry;
}
function mComboAsuransi() {
	var Field_poli_viDaftar = ['KD_CUSTOMER', 'CUSTOMER'];
	ds_customer_viDaftar = new WebApp.DataStore({ fields: Field_poli_viDaftar });
	cboAsuransi = new Ext.form.ComboBox({
		id: 'cboAsuransi',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: false,
		emptyText: 'Pilih Kelompok pasien...',
		fieldLabel: namacbokelpasien,
		align: 'Right',
		anchor: '95%',
		tabIndex: 31,
		enableKeyEvents: true,
		store: ds_customer_viDaftar,
		valueField: 'KD_CUSTOMER',
		displayField: 'CUSTOMER',
		listeners: {
			'select': function (a, b, c) {
				selectSetAsuransi = b.data.KD_CUSTOMER;
				if (b.data.KD_CUSTOMER === '0000000009' || b.data.KD_CUSTOMER === '0000000005') {
					Ext.getCmp('rb_datang').enable();
				} else if (Ext.getCmp('kelPasien').getValue() === 'Perseorangan' || Ext.getCmp('kelPasien').getValue() === 'Perusahaan') {
					Ext.getCmp('rb_datang').enable();
				} else {
					Ext.getCmp('rb_datang').disable();
				}

				if (b.data.KD_CUSTOMER === '0000000043' || b.data.KD_CUSTOMER === '0000000044') {
					Ext.getCmp('txtcatatan').show();
				} else {
					Ext.getCmp('txtcatatan').hide();
				}
				//alert(b.data.KD_CUSTOMER);
				Ext.getCmp('txtDiagnosa_RWJ').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13 || e.keyCode == 9) {
					Ext.getCmp('txtDiagnosa_RWJ').focus();
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboAsuransi;
}
function mComboPekerjaanRequestEntry() {
	var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
	dsPekerjaanRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPekerjaanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pekerjaan',
			Sortdir: 'ASC',
			target: 'ViewComboPekerjaan',
			param: ''
		}
	});
	var cboPekerjaanRequestEntry = new Ext.form.ComboBox({
		id: 'cboPekerjaanRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pekerjaan...',
		fieldLabel: 'Pekerjaan',
		align: 'Right',
		tabIndex: 16,
		store: dsPekerjaanRequestEntry,
		valueField: 'KD_PEKERJAAN',
		displayField: 'PEKERJAAN',
		anchor: '99%',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				PekerjaanPasien = b.data.KD_PEKERJAAN;
				Ext.getCmp('txtAlamat').focus();
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('txtAlamat').focus();
				} else if (e.keyCode == 9) {
				}
			}
		}
	});
	return cboPekerjaanRequestEntry;
}

function mComboPekerjaanPenanggungJawabRequestEntry() {
	var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
	dsPekerjaanPenanggungJawabRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPekerjaanPenanggungJawabRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'pekerjaan',
			Sortdir: 'ASC',
			target: 'ViewComboPekerjaan',
			param: ''
		}
	});
	var cboPekerjaanPenanggungJawabRequestEntry = new Ext.form.ComboBox({
		id: 'cboPekerjaanPenanggungJawabRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Pekerjaan...',
		fieldLabel: 'Pekerjaan',
		align: 'Right',
		tabIndex: 57,
		store: dsPekerjaanPenanggungJawabRequestEntry,
		valueField: 'KD_PEKERJAAN',
		displayField: 'PEKERJAAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				var selectPekerjaanPenanggungJawabRequestEntry = b.data.KD_PROPINSI;
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13)
						Ext.getCmp('cboSukuRequestEntry').focus();
				}, c);
			}
		}
	});
	return cboPekerjaanPenanggungJawabRequestEntry;
}
function mComboSukuRequestEntry() {
	var Field = ['KD_SUKU', 'SUKU'];
	dsSukuRequestEntry = new WebApp.DataStore({ fields: Field });
	dsSukuRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kd_suku',
			Sortdir: 'ASC',
			target: 'ViewComboSuku',
			param: ''
		}
	});
	var cboSukuRequestEntry = new Ext.form.ComboBox({
		id: 'cboSukuRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: ' ',
		editable: false,
		fieldLabel: 'Suku ',
		align: 'Right',
		store: dsSukuRequestEntry,
		valueField: 'KD_SUKU',
		displayField: 'SUKU',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				selectSukuRequestEntry = b.data.KD_SUKU;
				console.log(selectSukuRequestEntry);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13 || e.getKey() == 9)
						Ext.getCmp('setunit').collapsed = true;
				}, c);
			}
		}
	});
	return cboSukuRequestEntry;
}

var jam_klinik;
function mComboJamKlinik()
{
  var cboJamKlinik = new Ext.form.ComboBox
    (
        {
                    id:'cboJamKlinik',
                    // x: 155,
                    // y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'Jam Klinik',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Klinik Pagi'],[2, 'Klinik Sore']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:1,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    jam_klinik=b.data.displayText ;
                            }
                    }
        }
    );
    return cboJamKlinik;
};

function mComboPoliklinik() {
	var Field = ['KD_UNIT', 'NAMA_UNIT'];
	ds_Poli_viDaftar = new WebApp.DataStore({ fields: Field });
	ds_Poli_viDaftar.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'NAMA_UNIT',
			Sortdir: 'ASC',
			target: 'ViewSetupUnitB',
			param: "kd_bagian=2 and type='0'"
		}
	});
	var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
		id: 'cboPoliklinikRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Poliklinik...',
		fieldLabel: 'Poliklinik ',
		align: 'Right',
		enableKeyEvents: true,
		store: ds_Poli_viDaftar,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		anchor: '95%',
		tabIndex: 29,
		listeners: {
			select: function (a, b, c) {
				loaddatastoredokter(b.data.KD_UNIT)
				polipilihanpasien = b.data.KD_UNIT;
				Ext.getCmp('cboDokterRequestEntry').setValue();
				tmp_sub_unit_rehab = "";
				// Ext.getCmp('cboDokterRequestEntry').focus(true);
				if (b.data.NAMA_UNIT == "Rehabilitasi Medik") {
					Ext.getCmp('rb_induk_unit_rehabmedik').show();
				} else {
					Ext.getCmp('rb_induk_unit_rehabmedik').hide();
				}
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboDokterRequestEntry').focus(true);
				}
			},
			/*keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}*/
		}
	});
	return cboPoliklinikRequestEntry;
}
function loaddatastoredokter(kd_unit) {
	dsDokterRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokter',
			param: 'where dk.kd_unit=~' + kd_unit + '~' //+ 'and d.jenis_dokter=1 '
		},
		// callback:function(records, options, success){
		// 	if (success) {
		// 		console.log(kodeDokterDefault);
		// 		Ext.getCmp('cboDokterRequestEntry').setValue(kodeDokterDefault);
		// 	}
		// }
	})
}
function mComboDokterRequestEntry() {
	var Field = ['KD_DOKTER', 'NAMA'];
	dsDokterRequestEntry = new WebApp.DataStore({ fields: Field });
	var cboDokterRequestEntry = new Ext.form.ComboBox({
		id: 'cboDokterRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Dokter...',
		fieldLabel: 'Dokter ',
		align: 'Right',
		tabIndex: 33,
		enableKeyEvents: true,
		store: dsDokterRequestEntry,
		valueField: 'KD_DOKTER',
		displayField: 'NAMA',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				kodeDokterDefault = b.data.KD_DOKTER;
				console.log(kodeDokterDefault);
				jml_antrian_dokter(kodeDokterDefault);
				// Ext.getCmp('kelPasien').focus(true,10);
				// if(Ext.getCmp('txtNoAskes').getValue() == ' '){
				// 	Ext.getCmp('txtNoAskes').setValue('');
				// 	Ext.getCmp('cboCustomer').focus(true,10);
				// }
				Ext.getCmp('cboCustomer').focus(true, 10);
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('cboCustomer').focus(true, 10);
				}
			},
			/*keydown:function(text,e){
				var nav=navigator.platform.match("Mac");
				if (e.keyCode == 83 && ( nav? e.metaKey : e.ctrlKey)){
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				}else if(e.keyCode == 117){
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}*/
		}
	});
	return cboDokterRequestEntry;
}
function dataCboRujukanDariRWJ(params) {
	dsRujukanDariRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'penerimaan',
			Sortdir: 'ASC',
			target: 'ViewComboRujukanDari',
			param: params
		}
	});
	return dsRujukanDariRequestEntry;
}
function mcomborujukandari() {
	var Field = ['CARA_PENERIMAAN', 'PENERIMAAN'];
	dsRujukanDariRequestEntry = new WebApp.DataStore({ fields: Field });
	dsRujukanDariRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'penerimaan',
			Sortdir: 'ASC',
			target: 'ViewComboRujukanDari',
			param: ''
		}
	});
	var cboRujukanDariRequestEntry = new Ext.form.ComboBox({
		id: 'cboRujukanDariRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		disabled: true,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Rujukan...',
		fieldLabel: 'Rujukan Dari ',
		align: 'Right',
		store: dsRujukanDariRequestEntry,
		tabIndex: 35,
		valueField: 'CARA_PENERIMAAN',
		displayField: 'PENERIMAAN',
		anchor: '90%',
		enableKeyEvents: true,
		disabled: true,
		listeners: {
			'select': function (a, b, c) {
				carapenerimaanpasien = b.data.CARA_PENERIMAAN;
				// loaddatastorerujukan(b.data.CARA_PENERIMAAN);
				Ext.getCmp('txtDiagnosa_RWJ').focus();
			},
			/*'render': function (c)
			{
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) //atau Ext.EventObject.ENTER
					   Ext.getCmp('txtDiagnosa_RWJ').focus();
				}, c);
			} */
		}
	});
	return cboRujukanDariRequestEntry;
}
function loaddatastorerujukan(cara_penerimaan) {
	dsRujukanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'rujukan',
			Sortdir: 'ASC',
			target: 'ViewComboRujukan',
			param: 'cara_penerimaan=~' + cara_penerimaan + '~'
		},
		callback: function () {
			//Ext.getCmp('cboRujukanRequestEntry').focus();
		}
	});
}
function jml_antrian_dokter(kd_dokter){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/jml_antrian_dokter",
		params: {
			kd_dokter: kd_dokter
		},
		success: function (o) {
			var tmphasil = Ext.decode(o.responseText);
			Ext.getCmp('txtJmlAntrianPanggilan').setValue(tmphasil.jml);
			console.log(tmphasil);
		},
		callback: function () {
			Ext.getCmp('txtJmlAntrianPanggilan').focus();
		}
	});
}
function loaddatastoredatarujukan(koderujukan) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataRujukan",
		params: {
			koderujuk: koderujukan
		},
		success: function (o) {
			var tmphasil = Ext.decode(o.responseText);
			Ext.getCmp('txtNamaPerujuk').setValue(tmphasil.nama);
			Ext.getCmp('txtAlamatPerujuk').setValue(tmphasil.alamat);
			Ext.getCmp('txtKotaPerujuk').setValue(tmphasil.kota);
			Ext.getCmp('txtNamaPerujuk').disable();
			Ext.getCmp('txtAlamatPerujuk').disable();
			Ext.getCmp('txtKotaPerujuk').disable();
			console.log(tmphasil);
		},
		callback: function () {
			Ext.getCmp('cboPoliklinikRequestEntry').focus();
		}
	});
}
function mComboRujukan() {
	var Field = ['KD_RUJUKAN', 'RUJUKAN', 'CARA_PENERIMAAN'];
	dsRujukanRequestEntry = new WebApp.DataStore({ fields: Field });
	dsRujukanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'rujukan',
			Sortdir: 'ASC',
			target: 'ViewComboRujukan',
			//param: 'cara_penerimaan=~' + kodePenerimaanPasien + '~'
		},
		callback: function () {
			//Ext.getCmp('cboRujukanRequestEntry').focus();
		}
	});
	var cboRujukanRequestEntry = new Ext.form.ComboBox({
		id: 'cboRujukanRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Rujukan...',
		fieldLabel: 'Rujukan ',
		align: 'Right',
		tabIndex: 36,
		disabled: true,
		enableKeyEvents: true,
		store: dsRujukanRequestEntry,
		valueField: 'KD_RUJUKAN',
		displayField: 'RUJUKAN',
		anchor: '99%',
		listeners: {
			select: function (a, b, c) {
				// console.log(b.data);
				carapenerimaanpasien = b.data.CARA_PENERIMAAN;
				koderujuk = b.data.KD_RUJUKAN;
				Ext.getCmp('cboRujukanDariRequestEntry').setValue(b.data.CARA_PENERIMAAN);
				Ext.getCmp('txtDiagnosa_RWJ').focus(true, 10);
				//loaddatastoredatarujukan(b.data.KD_RUJUKAN);
			},
			keypress: function (c, e) {
				if (e.keyCode == 13) {
					Ext.getCmp('txtDiagnosa_RWJ').focus(true, 10);
				}
			},
		}
	});
	return cboRujukanRequestEntry;
}
function mComboPerusahaan() {
	var Field = ['KD_PERUSAHAAN', 'PERUSAHAAN'];
	dsPerusahaanRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPerusahaanRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'perusahaan',
			Sortdir: 'ASC',
			target: 'ViewComboPerusahaan',
			param: ''
		}
	});
	var cboPerusahaanRequestEntry = new Ext.form.ComboBox({
		id: 'cboPerusahaanRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Perusahaan...',
		fieldLabel: 'Perusahaan',
		align: 'Right',
		hidden: true,
		enableKeyEvents: true,
		store: dsPerusahaanRequestEntry,
		valueField: 'KD_PERUSAHAAN',
		displayField: 'PERUSAHAAN',
		anchor: '95%',
		listeners: {
			'select': function (a, b, c) {
				Ext.getCmp('nci-gen1').focus();
			},
			keypress: function (c, e) {
				if (e.keyCode == 13 || e.keyCode == 9) {
					Ext.getCmp('nci-gen1').focus();
				}
			},
			keydown: function (text, e) {
				var nav = navigator.platform.match("Mac");
				if (e.keyCode == 83 && (nav ? e.metaKey : e.ctrlKey)) {
					e.preventDefault();
					Ext.getCmp('btnSimpan_viDaftar').el.dom.click();
				} else if (e.keyCode == 117) {
					e.preventDefault();
					cekKunjunganPasienLaluCetakBpjs();
				}
			}
		}
	});
	return cboPerusahaanRequestEntry;
}
function mComboPerusahaanPenanggungJawab() {
	var Field = ['KD_PERUSAHAAN', 'PERUSAHAAN', 'ALAMAT'];
	dsPerusahaanPenanggungJawabRequestEntry = new WebApp.DataStore({ fields: Field });
	dsPerusahaanPenanggungJawabRequestEntry.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'perusahaan',
			Sortdir: 'ASC',
			target: 'ViewComboPerusahaan',
			param: ''
		}
	})
	var cboPerusahaanPenanggungJawabRequestEntry = new Ext.form.ComboBox({
		id: 'cboPerusahaanPenanggungJawabRequestEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Perusahaan...',
		fieldLabel: 'Perusahaan',
		align: 'Right',
		width: 120,
		tabIndex: 58,
		store: dsPerusahaanPenanggungJawabRequestEntry,
		valueField: 'KD_PERUSAHAAN',
		displayField: 'PERUSAHAAN',
		anchor: '95%'
	});
	return cboPerusahaanPenanggungJawabRequestEntry;
}
function refreshComboLoketRWJ() {
	dsPilihLoket.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewComboPilihLoket',
			param: ''
		}
	});
}
function mComboPilihLoketRWJ() {
	var Field = ['KODE_LOKET', 'NAMA_LOKET', 'ID_GROUP', 'LOKET'];
	dsPilihLoket = new WebApp.DataStore({ fields: Field });
	refreshComboLoketRWJ();
	var cboPilihLoketRWJ = new Ext.form.ComboBox({
		id: 'cboPilihLoket',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Loket...',
		fieldLabel: 'Loket',
		align: 'Right',
		width: 120,
		tabIndex: 58,
		store: dsPilihLoket,
		valueField: 'KODE_LOKET',
		displayField: 'NAMA_LOKET',
		anchor: '95%',
		listeners: {
			select: function (a, b) {
				console.log(a);
				console.log(b);
				loket_terpilih = b.data.KODE_LOKET;
			}
		}
	});
	return cboPilihLoketRWJ;
}
var rowSelectedLookPasien;
var rowSelectedLookSL;
var mWindowLookup;
var nFormPendaftaran = 1;
var dsLookupPasienList;
var criteria = '';
function FormLookupPasien(criteria, nFormAsal, nName_ID, strRM, strNama, strAlamat) {
	var vWinFormEntry = new Ext.Window({
		id: 'FormPasienLookup',
		title: 'Lookup Pasien',
		closable: true,
		width: 600,
		height: 400,
		border: true,
		plain: true,
		resizable: false,
		layout: 'form',
		iconCls: 'find',
		modal: true,
		items: [
			fnGetDTLGridLookUpPas(criteria, nFormAsal, nName_ID),
			{
				xtype: 'button',
				text: 'Ok',
				width: 70,
				style: { 'margin-left': '510px', 'margin-top': '7px' },
				hideLabel: true,
				id: 'btnOkpgw',
				handler: function () {
					GetPasien(nFormAsal, nName_ID);
				}
			}
		],
		tbar: {
			xtype: 'toolbar',
			items: [
				{
					xtype: 'tbtext',
					text: 'No. RM : '
				}, {
					xtype: 'textfield',
					id: 'txtNoRMLookupPasien',
					listeners: {
						'specialkey': function () {
							if (Ext.EventObject.getKey() === 13) {
								criteria = getQueryCariPasien();
								RefreshDataLookupPasien(criteria);
							}
						}
					}
				}, {
					xtype: 'tbtext',
					text: 'Nama : '
				}, {
					xtype: 'textfield',
					id: 'txtNamaMLookupPasien',
					listeners: {
						'specialkey': function () {
							if (Ext.EventObject.getKey() === 13) {
								criteria = getQueryCariPasien();
								RefreshDataLookupPasien(criteria);
							}
						}
					}
				}, {
					xtype: 'tbtext',
					text: 'Alamat : '
				}, {
					xtype: 'textfield',
					id: 'txtAlamatMLookupPasien',
					listeners: {
						'specialkey': function () {
							if (Ext.EventObject.getKey() === 13) {
								criteria = getQueryCariPasien();
								RefreshDataLookupPasien(criteria);
							}
						}
					}
				}, {
					xtype: 'tbfill'
				}, {
					xtype: 'button',
					id: 'btnRefreshGLLookupCalonMHS',
					iconCls: 'refresh',
					handler: function () {
						criteria = getQueryCariPasien();
						RefreshDataLookupPasien(criteria);
					}
				}
			]
		},
		listeners: {
			activate: function () {
				Ext.get('txtNoRMLookupPasien').dom.value = strRM;
				Ext.get('txtNamaMLookupPasien').dom.value = strNama;
				Ext.get('txtAlamatMLookupPasien').dom.value = strAlamat;
			}
		}
	});
	vWinFormEntry.show();
	mWindowLookup = vWinFormEntry;
}
function fnGetDTLGridLookUpPas(criteria, nFormAsal, nName_ID) {
	var fldDetail = ['KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
		'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
		'NAMA_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KELURAHAN', 'KABUPATEN', 'KECAMATAN', 'PROPINSI',
	];
	dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	RefreshDataLookupPasien(criteria);
	var vGridLookupPasienFormEntry = new Ext.grid.EditorGridPanel({
		id: 'vGridLookupPasienFormEntry',
		title: '',
		stripeRows: true,
		store: dsLookupPasienList,
		height: 310,
		columnLines: true,
		bodyStyle: 'padding:0px',
		enableKeyEvents: true,
		border: false,
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowSelectedLookPasien = dsLookupPasienList.getAt(row);
				}
			}
		}),
		listeners: {
			rowdblclick: function (sm, ridx, cidx) {
				var nFormAsal = 1;
				GetPasien(nFormAsal, nName_ID);
			},
			'specialkey': function () {
				if (Ext.EventObject.getKey() == 13) {
					GetPasien(nFormAsal, nName_ID);
				}
			}
		},
		cm: fnGridLookPasienColumnModel(),
		viewConfig: { forceFit: true }
	});
	return vGridLookupPasienFormEntry;
}
function fnGridLookPasienColumnModel() {
	return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			id: 'colLookupPasien',
			header: "No. RM",
			dataIndex: 'KD_PASIEN',
			width: 200
		}, {
			id: 'colLookupNamaPasien',
			header: "Nama",
			dataIndex: 'NAMA',
			width: 300
		}, {
			id: 'colLookupAlamatPasien',
			header: "Alamat",
			dataIndex: 'ALAMAT',
			width: 300
		}
	]);
}
function GetPasien(nFormAsal, nName_ID) {
	if (rowSelectedLookPasien != undefined || nName_ID != undefined) {
		if (nFormAsal === nFormPendaftaran) {
			Ext.getCmp('txtNoRequest').setValue(rowSelectedLookPasien.data.KD_PASIEN);
			Ext.getCmp('txtNama').setValue(rowSelectedLookPasien.data.NAMA);
			Ext.getCmp('txtNamaKeluarga').setValue(rowSelectedLookPasien.data.NAMA_KELUARGA);
			Ext.getCmp('txtTempatLahir_Daftar').setValue(rowSelectedLookPasien.data.TEMPAT_LAHIR);
			Ext.getCmp('cboPendidikanRequestEntry').setValue(rowSelectedLookPasien.data.PENDIDIKAN);
			Pendidikanpasien = rowSelectedLookPasien.data.KD_PENDIDIKAN;
			Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowSelectedLookPasien.data.PEKERJAAN);
			PekerjaanPasien = rowSelectedLookPasien.data.KD_PEKERJAAN;
			Ext.getCmp('txtAlamat').setValue(rowSelectedLookPasien.data.ALAMAT);
			Ext.getCmp('cboAgamaRequestEntry').setValue(rowSelectedLookPasien.data.AGAMA);
			selectAgamaRequestEntry = rowSelectedLookPasien.data.KD_AGAMA;
			Ext.getCmp('cboGolDarah_Daftar').setValue(rowSelectedLookPasien.data.GOL_DARAH);
			Ext.get('dtpTanggalLahir').dom.value = ShowDate(rowSelectedLookPasien.data.TGL_LAHIR);
			var tglLahir = Ext.get('dtpTanggalLahir').dom.value;
			Ext.getCmp('cboStatusMarital_Daftar').setValue(rowSelectedLookPasien.data.STATUS_MARITA);
			PasienSetSatusMarital = rowSelectedLookPasien.data.STATUS_MARITA;
			var tmpjk = "";
			var tmpwni = "";
			if (rowSelectedLookPasien.data.JENIS_KELAMIN === "t" || rowSelectedLookPasien.data.JENIS_KELAMIN === 1) {
				tmpjk = "Laki - Laki";
			} else {
				tmpjk = "Perempuan";
			}
			Ext.getCmp('cboJK_Daftar').setValue(tmpjk);
			if (rowSelectedLookPasien.data.WNI === "t" || rowSelectedLookPasien.data.WNI === 1) {
				tmpwni = "WNI";
			} else {
				tmpwni = "WNA";
			}
			Ext.getCmp('cboPropinsiRequestEntry').setValue(rowSelectedLookPasien.data.PROPINSI);
			Ext.getCmp('cboKabupatenRequestEntry').setValue(rowSelectedLookPasien.data.KABUPATEN);
			Ext.getCmp('cboKecamatanRequestEntry').setValue(rowSelectedLookPasien.data.KECAMATAN);
			Ext.getCmp('btnSimpan_viDaftar').disable();
			Ext.getCmp('btnSimpanExit_viDaftar').disable();
		}
	}
	rowSelectedLookPasien = undefined;
	mWindowLookup.close();
}
function setUsia(Tanggal) {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/GetUmur",
		params: {
			TanggalLahir: getParamHitungUmur(Tanggal)
		},
		success: function (o) {
			var tmphasil = o.responseText;
			var tmp = tmphasil.split(' ');
			if (tmp.length == 6) {
				Ext.getCmp('txtThnLahir').setValue(tmp[0]);
				Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
				Ext.getCmp('txtHariLahir').setValue(tmp[4]);
				getParamBalikanHitungUmur(Tanggal);
			} else if (tmp.length == 4) {
				if (tmp[1] == 'years' && tmp[3] == 'day') {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue(tmp[2]);
				} else if (tmp[1] == 'years' && tmp[3] == 'days') {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue(tmp[2]);
				} else if (tmp[1] == 'year' && tmp[3] == 'days') {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue(tmp[2]);
				} else {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
					Ext.getCmp('txtHariLahir').setValue('0');
				}
				getParamBalikanHitungUmur(Tanggal);
			} else if (tmp.length == 2) {
				if (tmp[1] == 'year') {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue('0');
				} else if (tmp[1] == 'years') {
					Ext.getCmp('txtThnLahir').setValue(tmp[0]);
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue('0');
				} else if (tmp[1] == 'mon') {
					Ext.getCmp('txtThnLahir').setValue('0');
					Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
					Ext.getCmp('txtHariLahir').setValue('0');
				} else if (tmp[1] == 'mons') {
					Ext.getCmp('txtThnLahir').setValue('0');
					Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
					Ext.getCmp('txtHariLahir').setValue('0');
				} else {
					Ext.getCmp('txtThnLahir').setValue('0');
					Ext.getCmp('txtBlnLahir').setValue('0');
					Ext.getCmp('txtHariLahir').setValue(tmp[0]);
				}
				getParamBalikanHitungUmur(Tanggal);
			} else if (tmp.length == 1) {
				Ext.getCmp('txtThnLahir').setValue('0');
				Ext.getCmp('txtBlnLahir').setValue('0');
				Ext.getCmp('txtHariLahir').setValue('1');
				getParamBalikanHitungUmur(Tanggal);
			} else {
				alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
			}
		}
	});
}
function getQueryCariPasien() {
	var strKriteria = "";
	if (Ext.get('txtNoRMLookupPasien').getValue() != '') {
		strKriteria += " RIGHT(pasien.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	}
	if (Ext.get('txtNamaMLookupPasien').getValue() != '') {
		if (strKriteria != "") {
			strKriteria += " AND pasien.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		} else {
			strKriteria += " pasien.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		}
	}
	if (Ext.get('txtAlamatMLookupPasien').getValue() != '') {
		if (strKriteria != "") {
			strKriteria += " AND pasien.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		} else {
			strKriteria += " pasien.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		}
	}
	return strKriteria;
}
function RefreshDataLookupPasien(criteria) {
	dsLookupPasienList.load({
		params: {
			Skip: 0,
			Take: 100,
			Sortdir: 'ASC',
			target: 'ViewKunjungan',
			param: criteria
		}
	});
	return dsLookupPasienList;
}
function Combo_Select_prwj(combo) {
	var value = combo;
	console.log(value); 
	if (value == "Perseorangan" || value == "Dinas Sosial") {
		// autocomdiagnosa.show();
		Ext.getCmp('txtNamaPeserta').hide();
		Ext.getCmp('txtNoAskes').hide();
		Ext.getCmp('txtNoRujukan').hide();
		Ext.getCmp('chk_rujukan_rs').hide();
		Ext.getCmp('txtNoSJP').hide();
		cboAsuransi.label.update('Perseorangan');
		Ext.getCmp('txtnik').hide();
		Ext.getCmp('txtcatatan').hide();
		Ext.getCmp('cboAsuransi').hide()
		Ext.getCmp('rb_Rujukan').setValue(false);
		Ext.getCmp('rb_datang').setValue(true);
		Ext.getCmp('rb_datang').enable();
		/* Ext.getCmp('rb_datang').enable();
		Ext.getCmp('rb_Rujukan').disable(); */
		Ext.getCmp('cboRujukanDariRequestEntry').setValue('');
		Ext.getCmp('cboRujukanDariRequestEntry').disable();
		Ext.getCmp('cboRujukanRequestEntry').disable();
		Ext.getCmp('cboRujukanRequestEntry').setValue('');
		/*if (Ext.getCmp("rb_datang").getValue() == true) {
			carapenerimaanpasien = 99;
			koderujuk            = 0;
			caranerima           = carapenerimaanpasien
		}*/
		kdrujukannya = koderujuk;
		Ext.getCmp('txtNamaPerujuk').disable();
		Ext.getCmp('txtAlamatPerujuk').disable();
		Ext.getCmp('txtKotaPerujuk').disable();
		var kriteria = "cara_penerimaan = ~99~";
		// dataCboRujukanDariRWJ(caranerima);

	} else if (value == "Perusahaan") {
		// autocomdiagnosa.show();
		Ext.getCmp('txtNamaPeserta').hide();
		Ext.getCmp('txtNoAskes').hide();
		Ext.getCmp('txtNoSJP').hide();
		cboAsuransi.label.update('Perusahaan');
		Ext.getCmp('txtNoRujukan').hide();
		Ext.getCmp('chk_rujukan_rs').hide();
		Ext.getCmp('cboAsuransi').hide();
		Ext.getCmp('txtnik').show();
		Ext.getCmp('txtcatatan').hide();
		Ext.getCmp('cboRujukanDariRequestEntry').show();
		Ext.getCmp('cboRujukanRequestEntry').show();

		Ext.getCmp('rb_datang').setValue(false);
		Ext.getCmp('rb_datang').enable();
		Ext.getCmp('rb_Rujukan').setValue(true);
		/* Ext.getCmp('rb_Rujukan').enable();
		Ext.getCmp('rb_datang').disable(); */
		/*caranerima=99;
		kdrujukannya=0;*/

		Ext.getCmp('cboRujukanDariRequestEntry').disable();
		Ext.getCmp('cboRujukanRequestEntry').enable();
		Ext.getCmp('txtNamaPerujuk').enable();
		Ext.getCmp('txtAlamatPerujuk').enable();
		Ext.getCmp('txtKotaPerujuk').enable();
		var kriteria = "cara_penerimaan <> ~99~";
		/*carapenerimaanpasien=99;
		koderujuk=0;*/
		// dataCboRujukanDariRWJ(caranerima);
		// loaddatastorerujukan(kodePenerimaanPasien);
	} else {
		// autocomdiagnosa.show();
		Ext.getCmp('txtNamaPeserta').show();
		Ext.getCmp('txtNoAskes').show();
		Ext.getCmp('txtNoRujukan').show();
		Ext.getCmp('chk_rujukan_rs').show();
		Ext.getCmp('txtNoAskes').setValue(NoAsuransiPasien);
		Ext.getCmp('txtNoSJP').show();
		Ext.getCmp('cboAsuransi').hide();
		Ext.getCmp('txtnik').hide();
		Ext.getCmp('txtcatatan').show();
		cboAsuransi.label.update('Asuransi');

		Ext.getCmp('rb_datang').setValue(false);
		Ext.getCmp('rb_Rujukan').setValue(true);
		Ext.getCmp('rb_datang').disable();
		/* Ext.getCmp('rb_Rujukan').enable();
		Ext.getCmp('rb_datang').disable(); */
		/*caranerima=99;
		kdrujukannya=0;*/

		Ext.getCmp('cboRujukanDariRequestEntry').disable();
		Ext.getCmp('cboRujukanRequestEntry').enable();
		Ext.getCmp('txtNamaPerujuk').enable();
		Ext.getCmp('txtAlamatPerujuk').enable();
		Ext.getCmp('txtKotaPerujuk').enable();
		var kriteria = "cara_penerimaan <> ~99~";
		/*carapenerimaanpasien=99;
		koderujuk=0;*/
		// dataCboRujukanDariRWJ(caranerima);
		// loaddatastorerujukan(kodePenerimaanPasien);
	}
}
function printbill() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: dataparamreport_viDaftar(),
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				ShowPesanInfo_viDaftar('Bill segera dicetak', 'Cetak Bill');
				datarefresh_viDaftar(tmpcriteriaRWJ);
				addNew_viDaftar = false;
				Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN
				Ext.getCmp('btnSimpanExit_viDaftar').disable();
			} else if (cst.success === false && cst.pesan === 0) {
				ShowPesanWarning_viDaftar('Bill tidak bisa dicetak ' + cst.pesan, 'Cetak Bill');
			} else {
				ShowPesanError_viDaftar('Bill tidak bisa dicetak' + cst.pesan, 'Cetak Bill');
			}
		}
	});
}
function dataparamreport_viDaftar() {
	var paramsreport_ViPendaftaran = {
		Table: 'DirectPrinting',
		Medrec: Ext.getCmp('txtNoRequest').getValue(),
		No_TRans: tmpnotransaksi,
		kdUnit: polipilihanpasien,
		KdKasir: tmpkdkasir,
		JmlBayar: '0',
		JmlDibayar: '0',
		printer: Ext.getCmp('cbopasienorder_printer_pendaftaranrwj').getValue()
	};
	return paramsreport_ViPendaftaran
}
function CetakKartuPasien() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: dataparamcetakkartupasien_viDaftar(),
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
			} else if (cst.success === false && cst.pesan === 0) {
				ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
			} else {
				ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
			}
		}
	})
}
function dataparamcetakkartupasien_viDaftar() {
	var paramcetakkartupasien_ViPendaftaran = {
		Table: 'CetakKartuPasien',
		Medrec: Ext.get('txtNoRequest').getValue(),
		Nama: Ext.get('txtNama').getValue()
	};
	return paramcetakkartupasien_ViPendaftaran;
}
function printtracer() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: datacetaktracer(),
		failure: function (o) {
			ShowPesanError_viDaftar('Tracer Gagal di cetak ', 'Cetak Data');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.result == 'SUCCESS') {
				if (cst.message != undefined && cst.message != '') { }
				if (cst.printBaru == true) {
					//printRWJStatusPasienBaru();
				}
			} else if (cst.result == 'ERROR') {
				ShowPesanWarning_viDaftar('Data tidak berhasil di cetak ');
			} else {
				ShowPesanError_viDaftar('Data tidak berhasil di cetak ');
			}
		}
	});
}
function datacetaktracer() {
	var paramstracependaftaran = {
		Table: 'tracerprinting',
		NoMedrec: Ext.get('txtNoRequest').getValue(),
		NamaPasien: Ext.get('txtNama').getValue(),
		Alamat: Ext.get('txtAlamat').getValue(),
		Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
		TanggalMasuk: Ext.get('dptTanggal').getValue(),
		KdDokter: kodeDokterDefault//Ext.getCmp('cboDokterRequestEntry').getValue(),
	}
	return paramstracependaftaran
}
function savePendaftaranRWJ_SQL(nomedrec, notrans) {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: dataparam_viDaftar_sql(nomedrec, notrans),
		failure: function (o) {
			Ext.MessageBox.hide();
			ShowPesanError_viDaftar('Error Database! Kunjungan tidak berhasil di simpan ke Database sql, Hubungi Admin!', 'Simpan Kunjungan');
		}
	})
}
function dataparam_viDaftar_sql(nomedrec, notrans) {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/getcurrentshift",
		params: {
			command: '0'
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			tampungshiftsekarang = o.responseText;
		}
	});
	var pasienBaru;
	var jenis_kelamin;
	var Kd_customer
	if (selectSetJK === 1) {
		jenis_kelamin = true;
	} else {
		jenis_kelamin = false;
	}
	var tmpwarga;
	if (Ext.get(txtNoRequest).getValue() === '' || Ext.get(txtNoRequest).getValue() === 'Automatic from the system...') {
		pasienBaru = true;
	} else {
		pasienBaru = false;
	}
	/*if (selectSetAsuransi == ''){
		Kd_Customer = '0000000001';
	} else{
		Kd_Customer = selectSetAsuransi;
	}*/
	if (Ext.getCmp('cbokelurahanPenanggungJawab').getValue() == "" || Ext.getCmp('cbokelurahanPenanggungJawab').getValue() == "Pilih Kelurahan...") {
		Ext.getCmp('cbokelurahanPenanggungJawab').setValue(323);
	}
	if (Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() == "" || Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() == "Pilih Kelurahan...") {
		Ext.getCmp('cbokelurahanKtpPenanggungJawab').setValue(323);
	}
	if (Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() == "" || Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() == "Pilih Pekerjaan...") {

		Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').setValue(1);
	}
	if (Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() == "" || Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() == "Pilih Perusahaan...") {
		Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').setValue(1);
	}
	if (Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == "" || Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == "Pilih Jenis Kelamin...") {

		Ext.getCmp('cboJK_DaftarPenanggungjawab').setValue(1);
	}
	var jkpenanggungjawab;
	if (Ext.getCmp('cboJK_DaftarPenanggungjawab').getValue() == 1) {
		jkpenanggungjawab = true;
	} else {
		jkpenanggungjawab = false;
	}
	if (Ext.getCmp('cboPendidikanPenanggungJawab').getValue() == "" || Ext.getCmp('cboPendidikanPenanggungJawab').getValue() == "Pilih Pendidikan...") {
		Ext.getCmp('cboPendidikanPenanggungJawab').setValue(1);
	}
	if (Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue() == "" || Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue() == "Pilih Status...") {
		Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').setValue(1);
	}
	var kunjunganpasien;
	var nonkunjungan;
	var caranerima;
	var kdrujukannya;
	if (pasienrujukan === false) {
		caranerima = 99;
		kdrujukannya = 0;
	} else {
		caranerima = carapenerimaanpasien;
		kdrujukannya = koderujuk;
	}
	var params_ViPendaftaran = {
		Table: 'SQLViewKunjungan',
		No_Transaksi: notrans,
		NoMedrec: nomedrec,
		NamaPasien: Ext.get('txtNama').getValue(),
		NamaKeluarga: Ext.get('txtNamaKeluarga').getValue(),
		JenisKelamin: jenis_kelamin,
		Tempatlahir: Ext.get('txtTempatLahir_Daftar').getValue(),
		TglLahir: Ext.get('dtpTanggalLahir').getValue(),
		Agama: selectAgamaRequestEntry,
		GolDarah: Ext.getCmp('cboGolDarah_Daftar').getValue(),
		StatusMarita: PasienSetSatusMarital,
		StatusWarga: Ext.getCmp('cbWni').getValue(),
		Alamat: Ext.get('txtAlamat').getValue(),
		No_Tlp: '',
		Pendidikan: Pendidikanpasien,
		Pekerjaan: PekerjaanPasien,
		suami_istrinya: Ext.getCmp('txtNamaSuamiIstri').getValue(),
		Pendidikan_Ayah: PendidikanAyah,
		Pekerjaan_Ayah: PekerjaanAyah,
		Pendidikan_Ibu: PendidikanIbu,
		Pekerjaan_Ibu: PekerjaanIbu,
		Pendidikan_SuamiIstri: PendidikanSuamiIstri,
		Pekerjaan_SuamiIstri: PekerjaanSuamiIstri,
		NamaPeserta: Ext.get('txtNamaPeserta').getValue(),
		KD_Asuransi: 1,
		NoAskes: Ext.get('txtNoAskes').getValue(),
		NoSjp: Ext.get('txtNoSJP').getValue(),
		Kd_Suku: selectSukuRequestEntry,
		Jabatan: '',
		Perusahaan: Ext.getCmp('cboPerusahaanRequestEntry').getValue(),
		Perusahaan1: 0,
		Cek: kelompokpasien,
		Poli: polipilihanpasien,
		TanggalMasuk: Ext.get('dptTanggal').getValue(),
		UrutMasuk: 0,
		JamKunjungan: now_viDaftar,
		//CaraPenerimaan: caranerima,
		CaraPenerimaan: carapenerimaanpasien,
		//KdRujukan: kdrujukannya,
		KdRujukan: koderujuk,
		KdCustomer: Kd_Customer,
		KdDokter: kodeDokterDefault,//Ext.getCmp('cboDokterRequestEntry').getValue(),
		Baru: pasienBaru,
		Shift: tampungshiftsekarang,
		Karyawan: 0,
		Kontrol: false,
		Antrian: 0,
		NoSurat: '',
		Alergi: Ext.get('txtAlergi_prwj').getValue(),
		Anamnese: Ext.get('txtAnamnese_prwj').getValue(),
		TahunLahir: Ext.get('txtThnLahir').getValue(),
		BulanLahir: Ext.get('txtBlnLahir').getValue(),
		HariLahir: Ext.get('txtHariLahir').getValue(),
		Kota: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
		Kd_Kecamatan: selectKecamatanpasien,
		Kelurahan: kelurahanpasien,
		AsalPasien: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
		KDPROPINSI: Ext.getCmp('txtTmpPropinsi').getValue(),
		KDKABUPATEN: Ext.getCmp('txtTmpKabupaten').getValue(),
		KDKECAMATAN: Ext.getCmp('txtTmpKecamatan').getValue(),
		KDKECAMATANKTP: selectKecamatanktp,
		KDPENDIDIKAN: Pendidikanpasien,
		KDPEKERJAAN: PekerjaanPasien,
		KDAGAMA: selectAgamaRequestEntry,
		NAMA_PJ: Ext.getCmp('txtNamaPenanggungjawab').getValue(),
		AlamatPJ: Ext.getCmp('txtAlamatPenanggungjawab').getValue(),
		TgllahirPJ: Ext.getCmp('dtpPenanggungJawabTanggalLahir').getValue(),
		WNIPJ: Ext.getCmp('cbWniPenanggungJawab').getValue(),
		KelurahanPJ: Ext.getCmp('cbokelurahanPenanggungJawab').getValue(),
		PosPJ: Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').getValue(),
		TeleponPj: Ext.getCmp('txtTlpPenanggungjawab').getValue(),
		HpPj: Ext.getCmp('txtHpPenanggungjawab').getValue(),
		KTP: Ext.getCmp('txtNoKtpPenanggungjawab').getValue(),
		KelurahanKtpPJ: Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue(),
		AlamatKtpPJ: Ext.getCmp('txtalamatktpPenanggungjawab').getValue(),
		KdPosKtp: Ext.getCmp('txtKdPosKtpPerusahaanPenanggungjawab').getValue(),
		KdPendidikanPj: Ext.getCmp('cboPendidikanPenanggungJawab').getValue(),
		kdpekerjaanPj: Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue(),
		PerusahaanPj: Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue(),
		HubunganPj: selectSetHubunganKeluargaPj,
		AlamatKtpPasien: Ext.getCmp('txtAlamatktp').getValue(),
		KdKelurahanKtpPasien: selectkelurahanktp,
		KdPostKtpPasien: Ext.getCmp('txtposktp').getValue(),
		NamaAyahPasien: Ext.getCmp('txtNamaAyah').getValue(),
		NamaIbuPasien: Ext.getCmp('txtNamaIbu').getValue(),
		KdposPasien: Ext.getCmp('txtpos').getValue(),
		EmailPenanggungjawab: Ext.getCmp('txtEmailPenanggungjawab').getValue(),
		tempatlahirPenanggungjawab: Ext.getCmp('txtTempatPenanggungjawab').getValue(),
		StatusMaritalPenanggungjawab: Ext.getCmp('cboStatusMarital_DaftarPenanggungjawab').getValue(),
		TLPNPasien: Ext.getCmp('txttlpnPasien').getValue(),
		nik: Ext.getCmp('txtniknPasien').getValue(),
		HPPasien: Ext.getCmp('txtHandphonePasien').getValue(),
		EmailPasien: Ext.getCmp('txtEmailPasien').getValue(),
		JKpenanggungJwab: jkpenanggungjawab,
		RadioRujukanPasien: pasienrujukan,
		PasienBaruRujukan: pasienBaru,
		NoNIK: Ext.getCmp('txtnik').getValue(),
		NonKunjungan: Ext.getCmp('cbnonkunjungan').getValue(),
		KdDiagnosa: autocomdiagnosa_master,
		part_number_nik: nik_pasien,
		unitMana: 'rwj'
	};
	return params_ViPendaftaran
}
function getIdGetDataSettingPendaftaran() {
	Ext.Ajax.request({
		url: baseURL + "index.php/setup/manageutillity/getDataSetting",
		params: {
			UserId: 0
		},
		success: function (response, opts) {
			var cst = Ext.decode(response.responseText);
			Ext.getCmp('cboPropinsiRequestEntry').setValue(cst.kd_propinsi);
			Ext.getCmp('cboPropinsiKtp').setValue(cst.kd_propinsi);
			loaddatastorekabupaten(cst.kd_propinsi);
			loaddatastoreKtpkabupaten(cst.kd_propinsi);
			selectPropinsiRequestEntry = cst.kd_propinsi;
			selectPropinsiKtp = cst.kd_propinsi;
			Ext.getCmp('cboKabupatenRequestEntry').setValue(cst.kabupaten);
			Ext.getCmp('cboKtpKabupaten').setValue(cst.kabupaten);
			loaddatastorekecamatan(cst.kd_kabupaten);
			loaddatastoreKtpkecamatan(cst.kd_kabupaten);
			selectKabupatenRequestEntry = cst.kd_kabupaten;
			selectKabupatenKTP = cst.kd_kabupaten;
			Ext.getCmp('cboKecamatanRequestEntry').setValue(cst.kecamatan);
			Ext.getCmp('cboKecamatanKtp').setValue(cst.kecamatan);
			loaddatastorekelurahan(cst.kd_kecamatan);
			loaddatastoreKtpkelurahan(cst.kd_kecamatan);
			selectKecamatanpasien = cst.kd_kecamatan;
			selectKecamatanktp = cst.kd_kecamatan;
			Ext.getCmp('cbokelurahan').setValue(cst.kelurahan);
			Ext.getCmp('cbokelurahanKtp').setValue(cst.kelurahan);
			kelurahanpasien = cst.kd_kelurahan;
			selectkelurahanktp = cst.kd_kelurahan;
			/* Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.kd_child_unit.substr(1,3));
			polipilihanpasien = cst.kd_child_unit.substr(1,3);
			loaddatastoredokter(cst.kd_child_unit.substr(1,3)); */
			jeniscus = cst.jenis_cust;
			Ext.getCmp('cboAsuransi').setValue("");
			RefreshDatacombo(jeniscus);
			Ext.getCmp('cboCustomer').setValue(cst.customer);
            Kd_Customer = cst.kd_customer;

			pasienrujukan = false;
			var lisanCombo = "";
			Ext.getCmp('cboCustomer').setValue(cst.customer);
            Kd_Customer = cst.kd_customer;
			if (jeniscus == 0) {
				Ext.getCmp('kelPasien').setValue("Perseorangan");
				Ext.getCmp('cboPerseorangan').setValue(cst.kd_customer);
				Ext.getCmp('cboRujukanRequestEntry').disable();
				Ext.getCmp('cboRujukanDariRequestEntry').disable();
				selectSetAsuransi = cst.kd_customer;
				Ext.get('rb_datang').dom.checked = true;
				Ext.get('rb_Rujukan').dom.checked = false;
				lisanCombo = "Perseorangan";
			} else if (jeniscus == 1) {
				Ext.getCmp('kelPasien').setValue("Perusahaan");
				Ext.getCmp('cboRujukanDariRequestEntry').show();
				Ext.getCmp('cboRujukanRequestEntry').show();
				Ext.getCmp('cboPerusahaanRequestEntry').setValue(cst.kd_customer);
				selectSetAsuransi = cst.kd_customer;
				Ext.get('rb_Rujukan').dom.checked = true;
				Ext.get('rb_datang').dom.checked = false;
				Ext.getCmp('cboRujukanDariRequestEntry').disable();
				Ext.getCmp('cboRujukanRequestEntry').disable();
				lisanCombo = "Perusahaan";
			} else if (jeniscus == 2) {
				Ext.getCmp('kelPasien').setValue("Asuransi");
				Ext.getCmp('cboRujukanDariRequestEntry').show();
				Ext.getCmp('cboRujukanRequestEntry').show();
				selectSetAsuransi = cst.kd_customer;
				Ext.getCmp('cboAsuransi').setValue(cst.kd_customer);
				Ext.get('rb_Rujukan').dom.checked = true;
				Ext.get('rb_datang').dom.checked = false;
				Ext.getCmp('cboRujukanDariRequestEntry').disable();
				Ext.getCmp('cboRujukanRequestEntry').enable();
				lisanCombo = "Asuransi";
			} else if (eniscus == 3) {
				Ext.getCmp('kelPasien').setValue("Dinas Sosial");
				Ext.getCmp('cboPerseorangan').setValue(cst.kd_customer);
				selectSetAsuransi = cst.kd_customer;
				Ext.get('rb_datang').dom.checked = true;
				Ext.get('rb_Rujukan').dom.checked = false;
			}
			Combo_Select_prwj(lisanCombo);
			Ext.getCmp('cboAsuransi').setValue(cst.customer);
			Ext.getCmp('cboPendidikanAyahRequestEntry').setValue("");
		}
	});
}
function bbar_pagingPendaftaran(mod_name, count, source) {
	// Deklarasi Variabel pada bbar_paging # --------------
	var combo_mod_name = "combo_" + mod_name;
	var count_mod_name = "count_" + mod_name;
	var source_mod_name = "source_" + mod_name;
	var bbar_mod_name = "bbar_" + mod_name;
	var count_mod_name = count;
	var source_mod_name = source;
	// End Deklarasi Variabel pada bbar_paging # --------------

	// Pengaturan Bar Paging Bawah # --------------

	// Combo Per Halaman # --------------
	var combo_mod_name = new Ext.form.ComboBox
		(
			{
				id: 'perpage_bbar_paging',
				name: 'perpage_bbar_paging',
				width: 40,
				store: new Ext.data.ArrayStore
					(
						{
							fields: ['id'],
							data: [
								['5'],
								['10'],
								['25'],
								['50'],
								['100'],
							]
						}
					),
				mode: 'local',
				value: count_mod_name,

				listWidth: 40,
				triggerAction: 'all',
				displayField: 'id',
				valueField: 'id',
				editable: false,
				forceSelection: true,
				listeners: {
					select: function (a, b, c) {
						datarefresh_viDaftar();
						// console.log(b.displayText);
					}
				}
			}
		);
	// End Combo Per Halaman # --------------

	// ExtJS Paging # --------------
	var bbar_mod_name = new WebApp.PaggingBar
		(
			{
				//store: source,
				displayInfo: false,
				//pageSize: count_mod_name,
				//displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
				emptyMsg: "Tidak ada record&nbsp;&nbsp;",
				items: [
					'-',
					'Per Halaman: ',
					combo_mod_name
				]
			}
		);
	// End ExtJS Paging # --------------

	// Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    
	/*combo_mod_name.on('select', function(combo_mod_name, record_mod_name) 
		{
			bbar_mod_name.pageSize = parseInt(record_mod_name.get('id'), 10);
			bbar_mod_name.doLoad(bbar_mod_name.cursor);
		}, 
	this
	);*/
	// End Event refresh tampilan grid sesuai jumlah saat dipilih pada combobox  # --------------    

	// End Pengaturan Bar Paging Bawah # --------------
	return bbar_mod_name;
}

shortcut.set({
	code: 'main',
	list: [
		{
			key: 'f7',
			fn: function () {
				//txtNoMedrec
				var tmpNoMedrec = Ext.get('txtNoMedrec').getValue();
				var tmpgetNoMedrec = "";
				if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
					tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
					Ext.getCmp('txtNoMedrec').setValue(tmpgetNoMedrec);
					// updateAntrianRWJ();
				} else {
					tmpgetNoMedrec = tmpNoMedrec;
				}
				var url = baseURL + "index.php/rawat_jalan/cetaklabelpasien/cetak/" + tmpgetNoMedrec;
				new Ext.Window({
					title: 'Label Barcode',
					width: 900,
					height: 500,
					constrain: true,
					modal: true,
					html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
				}).show();
			}
		},
		{
			key: 'f8',
			fn: function () {
				//txtNoMedrec
				var tmpNoMedrec = Ext.get('txtNoMedrec').getValue();
				var tmpgetNoMedrec = "";
				if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
					tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
					Ext.getCmp('txtNoMedrec').setValue(tmpgetNoMedrec);
					// updateAntrianRWJ();
				} else {
					tmpNoMedrec = Ext.get('txtNoRequest').getValue();
					tmpgetNoMedrec = tmpNoMedrec;
				}

				var url = baseURL + "index.php/rawat_jalan/cetakkartupasien/cetak/" + tmpgetNoMedrec;
				new Ext.Window({
					title: 'Kartu Pasien',
					width: 900,
					height: 500,
					constrain: true,
					modal: true,
					html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>"
				}).show();
			}
		},
		{
			key: 'esc',
			fn: function () {
				Ext.getCmp('txtNoMedrec').setValue('');
				Ext.getCmp('txtNoMedrec').focus();
			}
		},
	]
});


/*
	FORM EDIT TARIF
	OLEH 	: HADAD AL GOJALI
	TANGGAL : 2017 - 04 - 20 
	
 */

function PilihDiagnosaLookUp_PendaftaranRWJ(data) {
	//console.log(dataRowIndexDetail);
	//
	dsDataStoreGridDiagnosa.removeAll();
	loaddatastoreListDiagnosa(data);
	varPilihDiagnosaLookUp_PendaftaranRWJ = new Ext.Window
		(
			{
				id: 'idFormDiagnosa_PendaftaranRWI',
				title: 'Daftar diagnosa',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle: 'padding: 3px;',
				items: [
					GridDiagnosa_PendaftaranRWJ(),
					// paneltotal
				],
				listeners:
				{
					activate: function () {

					},
					afterShow: function () {
						this.activate();

					},
					deactivate: function () {

					},
					close: function () {
						/* if(FocusExitResepRWJ == false){
							var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
							AptResepRWJ.form.Grid.a.startEditing(line, 4);	
						} */
					}
				}
			}
		);
	varPilihDiagnosaLookUp_PendaftaranRWJ.show();
};


function GridDiagnosa_PendaftaranRWJ() {
	var rowLine = 0;
	var rowData = "";
	var currentRowSelectionListKunjunganResepRWJ;
	var Field = ['kd_penyakit', 'penyakit'];
	DataStoreListDiagnosa = new WebApp.DataStore({ fields: Field });
	//loaddatastoreProdukComponent();

	var cm = new Ext.grid.ColumnModel({
		// specify any defaults for each column
		defaults: {
			sortable: false // columns are not sortable by default           
		},
		columns: [
			{
				header: 'Kode Penyakit',
				dataIndex: 'kd_penyakit',
				width: 25,
			},
			{
				header: 'Penyakit',
				dataIndex: 'penyakit',
				width: 75,
			},
		]
	});

	gridDiagnosa_Pendaftaranrwi = new Ext.grid.EditorGridPanel
		(
			{
				title: '',
				id: 'idGridDiagnosa_Pendaftaranrwj',
				store: dsDataStoreGridDiagnosa,
				clicksToEdit: 1,
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows: true,
				trackMouseOver: true,
				height: 250,
				width: 560,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
					(
						{
							singleSelect: true,
							listeners:
							{
							}
						}
					),
				sm: new Ext.grid.RowSelectionModel
					(
						{
							singleSelect: true,
							listeners:
							{
								rowselect: function (sm, row, rec) {
									rowLine = row;
									currentRowSelectionListKunjunganResepRWJ = undefined;
									currentRowSelectionListKunjunganResepRWJ = dsDataStoreGridDiagnosa.getAt(row);
								}
							}
						}
					),
				cm: cm,
				viewConfig: {
					forceFit: true
				},
				listeners: {
					'keydown': function (e, d) {
						if (e.getKey() == 13) {
							autocomdiagnosa_master = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
							tmp_diagnosa = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
							Ext.getCmp('txtDiagnosa_RWJ').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit + " - " + currentRowSelectionListKunjunganResepRWJ.data.penyakit);
							varPilihDiagnosaLookUp_PendaftaranRWJ.close();
							Ext.getCmp('txtNoAskes').focus(true, 10);
							console.log(tmp_diagnosa);
						}
					}
				}
			}
		);
	return gridDiagnosa_Pendaftaranrwi;
};


function loaddatastoreListDiagnosa(params) {
	// console.log(params);
	// console.log(params);
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getPenyakit",
		params: params,
		success: function (response) {
			var cst = Ext.decode(response.responseText);
			// PilihDiagnosaLookUp_PendaftaranRWI();
			if (cst['listData'].length > 0) {
				for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
					var recs = [], recType = DataStoreListDiagnosa.recordType;
					var o = cst['listData'][i];
					recs.push(new recType(o));
					dsDataStoreGridDiagnosa.add(recs);
				}

				Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getView().refresh();
				Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getSelectionModel().selectRow(0);
				Ext.getCmp('idGridDiagnosa_Pendaftaranrwj').getView().focusRow(0);
			} else {
				varPilihDiagnosaLookUp_PendaftaranRWJ.close();
				Ext.MessageBox.alert('Informasi', 'Tindakan ' + params.text + ' tidak ada', function () {
					Ext.getCmp('txtDiagnosa_RWJ').focus(true, 10);
				});
			}
		},
	});
}


/* 
	===============================================================================================================
	======================================================== Form Lakalantas
	===============================================================================================================
*/
function formLookup_Lakalantas_RWJ(rowdata) {
	var lebar = 500;
	formLookups_Lakalantas = new Ext.Window({
		id: 'formLookup_Lakalantas',
		name: 'formLookup_Lakalantas',
		title: 'Formulir Lakalantas',
		closeAction: 'destroy',
		width: lebar,
		height: 240, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		// iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_lakalantas_RWJ(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookups_Lakalantas.show();
}

function panel_form_lookup_lakalantas_RWJ() {
	var formPanel = new Ext.form.FormPanel({
		id: "myformpanel",
		anchor: '100% 100%',
		height: 210,
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			anchor: "100%"
		},
		items:
			[
				{
					// 1=Jasa raharja PT, 2=BPJS Ketenagakerjaan, 3=TASPEN PT, 4=ASABRI PT} jika lebih dari 1 isi -> 1,2 (pakai delimiter koma)
					xtype: 'checkboxgroup',
					fieldLabel: 'Penjamin',
					itemId: 'chck_lakalantas_RWJ',
					columns: 2,
					vertical: true,
					items: [
						{
							boxLabel: 'Jasa Raharja',
							name: 'rb1',
							id: 'chck_lakalantas_RWJ_1',
							inputValue: '1',
							checked: lakalantas_checkbox_1_RWJ,
							listeners: {
								check: function (checkbox, isChecked) {
									lakalantas_checkbox_1_RWJ = isChecked;
								}
							}

						},
						{
							boxLabel: 'BPJS Ketenagakerjaan',
							name: 'rb1',
							id: 'chck_lakalantas_RWJ_2',
							inputValue: '2',
							checked: lakalantas_checkbox_2_RWJ,
							listeners: {
								check: function (checkbox, isChecked) {
									lakalantas_checkbox_2_RWJ = isChecked;
								}
							}
						},
						{
							boxLabel: 'TASPEN PT',
							name: 'rb1',
							id: 'chck_lakalantas_RWJ_3',
							inputValue: '3',
							checked: lakalantas_checkbox_3_RWJ,
							listeners: {
								check: function (checkbox, isChecked) {
									lakalantas_checkbox_3_RWJ = isChecked;
								}
							}
						},
						{
							boxLabel: 'ASABRI PT',
							name: 'rb1',
							id: 'chck_lakalantas_RWJ_4',
							inputValue: '4',
							checked: lakalantas_checkbox_4_RWJ,
							listeners: {
								check: function (checkbox, isChecked) {
									lakalantas_checkbox_4_RWJ = isChecked;
								}
							}
						},
					]
				}, {
					xtype: 'textfield',
					fieldLabel: "Lokasi",
					name: 'txtLokasi_Lakalantas',
					id: 'txtLokasi_Lakalantas',
				}
			],
		buttons:
			[
				{
					text: "Simpan",
					handler: function () {
						txtLokasi_Lakalantas = Ext.getCmp('txtLokasi_Lakalantas').getValue();
						formLookups_Lakalantas.close();
					}
				}
			]
	});
	return formPanel;
}

// UPDATE LOOKUP FORM BPJS BY NO KARTU MULTI RECORD GAN
function form_rujukan_multi_bpjs_RWJ(stat_rujukan) {
	var lebar = 750;
	form_rujukan_multi_bpjs_RWJ_window = new Ext.Window({
		id: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs_multi',
		name: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs_multi',
		title: 'Form Multi Rujukan',
		closeAction: 'destroy',
		width: lebar,
		height: 300, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_rujukan_bpjs_RWJ_multi(stat_rujukan), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	form_rujukan_multi_bpjs_RWJ_window.show();
}


// UPDATE LOOKUP PANLE BPJS BY NO KARTU MULTI RECORD Egi 08 Febuari 2022
function panel_form_lookup_rujukan_bpjs_RWJ_multi(stat_rujukan) {
	radios4 = new Ext.form.RadioGroup({
		id: 'radios4',
		column: 3,
		items: [
		],
		listeners: {
			change: function () {

			}
		}
	});
	/*
	getDataBpjs_by_nokartu_multi_rs

														no_kartu: Ext.getCmp('txtNoAskes').getValue() 
	 */
	var gridListFKTP = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceRujukanByNoka,
		columnLines: false,
		autoScroll: false,
		width: 720,
		height: 250,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					//update gan(rujukan)
					rowselectRujukan_byNoka = dataSourceRujukanByNoka.getAt(row);
					loadMask.show();
					if (stat_rujukan === true) {
						// DISINI
						if (rowselectRujukan_byNoka.data.NO_KUNJUNGAN != '' && Ext.getCmp('chk_rujukan_rs').getValue() === false) {
							form_rujukan_multi_bpjs_RWJ_window.close();
							formLookup_rujukan_bpjs_RWJ(stat_rujukan);
							Ext.Ajax.request({
								method: 'POST',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_norujukan",
								params: {
									no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN
								},
								success: function (o) {
									loadMask.hide();
									var cst = Ext.decode(o.responseText);
									console.log(cst.data.response.rujukan.poliRujukan.kode);
									poli_rujukan_asal = cst.data.response.rujukan.poliRujukan.kode;
									if (cst.data.metaData.code == '200') {
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
											params: {
												no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN,
												kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
                        						noka: Ext.getCmp('txtNoAskes').getValue(),
												kd_unit: polipilihanpasien
											},
											failure: function (o) {
												ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												kunjungan_pertama = cst.kunjungan_pertama;
												if (cst.kunjungan_pertama == false) {
                          						console.log('cek true');
													generate_surat_kontrol(false);
													loaddatastoreUnitBPJS();
													console.log('false');
													get_dokter_pertama(rowselectRujukan_byNoka.data.NO_KUNJUNGAN, Ext.getCmp('txtNoRequest').getValue());
													Ext.getCmp('txt_poli').enable();
													// Ext.getCmp('txt_no_surat_kontrol').enable();
													// Ext.getCmp('cboDokterPJBPJSKunj2').hide();
													Ext.Ajax.request({
														method: 'POST',
														url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
														params: {
															select: " * ",
															where: " kd_unit = '" + polipilihanpasien + "' ",
															table: " map_unit_bpjs ",
														},
														failure: function (o) {
															ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
														},
														success: function (o) {
															var cst = Ext.decode(o.responseText);
															tmp_poli_tujuan = cst[0].unit_bpjs;
															tmp_poli_tujuan_kunj2 = cst[0].unit_bpjs;
															// loaddatastoreDokterPJBPJS2('RWJ', cst[0].unit_bpjs);
                              								loaddatastoreDokterPJBPJS("RWJ", cst[0].unit_bpjs);
															// loaddatastoreDokterPJBPJSKunj2(cst[0].unit_bpjs);

															Ext.Ajax.request({
																method: 'POST',
																url: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
																params: {
																	select: " * ",
																	where: " kd_unit = '" + polipilihanpasien + "' ",
																	table: " unit ",
																},
																failure: function (o) {
																	ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
																},
																success: function (o) {
																	var cst = Ext.decode(o.responseText);
																	Ext.getCmp('txt_poli').setValue(cst[0].nama_unit);
																}
															});
														}
													});
												}
												else {
                         							console.log('cek false');
													generate_surat_kontrol(true);
													Ext.Ajax.request({
														method: 'POST',
														url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
														params: {
															kd_poli_bpjs: res.data.response.rujukan.poliRujukan.kode
														},
														failure: function (o) {
															ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
														},
														success: function (o) {
															var cst = Ext.decode(o.responseText);
															if (cst.success === true) {
																console.log(cst.listData[0].nama_unit);
																Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
																polipilihanpasien = cst.listData[0].kd_unit;
																tmp_poli_tujuan_kunj2 = cst.listData[0].unit_bpjs;
																tmp_poli_tujuan = cst.listData[0].unit_bpjs;
															}
															else {
																ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
															}
														}
													});
													// console.log(Ext.getCmp('cboDokterRequestEntry'));
													Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
													//Ext.getCmp('txt_poli').disable();
													selectTujuanKunjSEP = 0;
													Ext.getCmp('cboTujuanKunj').setValue('Normal');
													// Ext.getCmp('cboTujuanKunj').disable(); // HUDI // 10-02-2022 // dienable karena ada kasus yang mengharuskan ini dienable
												}
											}

										});

										var res = cst;
										if (res.data.response.rujukan.peserta.mr.noTelepon == '' || res.data.response.rujukan.peserta.mr.noTelepon == null) {
											console.log('1');
											Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
										} else {
											console.log('2');
											Ext.getCmp('txt_no_tlp').setValue(res.data.response.rujukan.peserta.mr.noTelepon);
										}
										Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
										Ext.getCmp('txtNoRujukan').setValue(res.data.response.rujukan.noKunjungan);
										Ext.getCmp('txtNamaPeserta').setValue(res.data.response.rujukan.peserta.nama)
										tmp_ppk_rujukan = res.data.response.rujukan.provPerujuk.kode;
										// tmp_kelas_rawat=res.data.response.rujukan.peserta.hakKelas.kode;
										tmp_kelas_rawat = 3;
										tmp_diagnosa = res.data.response.rujukan.diagnosa.kode;
										tmp_poli_tujuan = res.data.response.rujukan.poliRujukan.kode;
										tmp_tgl_rujukan = res.data.response.rujukan.tglKunjungan;
										Ext.getCmp('tgl_rujukan').setValue(res.data.response.rujukan.tglKunjungan);
										Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.rujukan.peserta.nama);
										Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.rujukan.peserta.noKartu);
										// Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.rujukan.peserta.hakKelas.keterangan)
										Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue('KELAS 3');
										Ext.getCmp('txt_perujuk').setValue(res.data.response.rujukan.provPerujuk.nama);
										Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
										// 17-02-2022
										console.log(sep_rwi);
										if(sep_rwi != ''){
                      					console.log('seprwi true');
											Ext.getCmp('txt_no_rujukan').setValue(sep_rwi);
										}else{
                      					console.log('seprwi false');
											Ext.getCmp('txt_no_rujukan').setValue(res.data.response.rujukan.noKunjungan);
										}
										Ext.getCmp('txt_kd_diagnosa').setValue(res.data.response.rujukan.diagnosa.kode);
										Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.rujukan.diagnosa.nama);

										Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
										response_bpjs = cst.data.response;
										// loaddatastoreDokterPJBPJS2(tmp_poli_tujuan, res.data.response.rujukan.poliRujukan.kode);
                    					loaddatastoreDokterPJBPJS(tmp_poli_tujuan, res.data.response.rujukan.poliRujukan.kode);
										loaddatastoreUnitBPJS();
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
											params: {
												kd_prov_rujukan: cst.data.response.rujukan.provPerujuk.kode
											},
											failure: function (o) {
												ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													console.log(cst);
													Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
													Ext.getCmp('cboRujukanDariRequestEntry').setValue(cst.listData[0].cara_penerimaan);
													koderujuk = cst.listData[0].kd_rujukan;
												}
												else {
													ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
												}
											}

										});

									} else {
										loadMask.hide();
									}
								}, error: function (jqXHR, exception) {
									Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
								}
							});
						} else if (rowselectRujukan_byNoka.data.NO_KUNJUNGAN != '' && Ext.getCmp('chk_rujukan_rs').getValue() === true) {
							form_rujukan_multi_bpjs_RWJ_window.close();
							formLookup_rujukan_bpjs_RWJ(stat_rujukan);
							Ext.Ajax.request({
								method: 'POST',
								url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_norujukan_RS",
								params: {
									no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN
								},
								success: function (o) {
									loadMask.hide();
									var cst = Ext.decode(o.responseText);
									// console.log(cst);
									console.log(cst.data.response.rujukan.poliRujukan.kode);
									poli_rujukan_asal = cst.data.response.rujukan.poliRujukan.kode;
									if (cst.data.metaData.code == '200') {
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
											params: {
												kd_prov_rujukan: cst.data.response.rujukan.provPerujuk.kode
											},
											failure: function (o) {
												ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													console.log(cst);
													Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
													koderujuk = cst.listData[0].kd_rujukan;
												}
												else {
													ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
												}
											}

										});
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
											params: {
												no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN,
												kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
                        						noka: Ext.getCmp('txtNoAskes').getValue(),
												kd_unit: polipilihanpasien
											},
											failure: function (o) {
												ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												kunjungan_pertama = cst.kunjungan_pertama;
												if (cst.kunjungan_pertama == false) {
													generate_surat_kontrol(false);
													loaddatastoreUnitBPJS();
													console.log('false');
													get_dokter_pertama(rowselectRujukan_byNoka.data.NO_KUNJUNGAN, Ext.getCmp('txtNoRequest').getValue());
													Ext.getCmp('txt_poli').enable();
													// Ext.getCmp('txt_no_surat_kontrol').enable();
												}
												else {
													generate_surat_kontrol(true);
													//Ext.getCmp('txt_poli').disable();
													selectTujuanKunjSEP = 0;
													Ext.getCmp('cboTujuanKunj').setValue('Normal');
													// Ext.getCmp('cboTujuanKunj').disable(); // HUDI // 10-02-2022 // dienable karena ada kasus yang mengharuskan ini dienable
												}
											}

										});

										var res = cst;
										console.log(res);
										if (res.data.response.rujukan.peserta.mr.noTelepon == '' || res.data.response.rujukan.peserta.mr.noTelepon == null) {
											//console.log('1');
											Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
										} else {
											//console.log('2');
											Ext.getCmp('txt_no_tlp').setValue(res.data.response.rujukan.peserta.mr.noTelepon);
										}
										console.log(res.data.response.rujukan.peserta.nama);

										Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
										Ext.getCmp('txtNoRujukan').setValue(res.data.response.rujukan.noKunjungan);
										Ext.getCmp('txtNamaPeserta').setValue(res.data.response.rujukan.peserta.nama)
										tmp_ppk_rujukan = res.data.response.rujukan.provPerujuk.kode;
										// tmp_kelas_rawat=res.data.response.rujukan.peserta.hakKelas.kode;
										tmp_kelas_rawat = 3;
										tmp_diagnosa = res.data.response.rujukan.diagnosa.kode;
										tmp_poli_tujuan = res.data.response.rujukan.poliRujukan.kode;
										tmp_tgl_rujukan = res.data.response.rujukan.tglKunjungan;
										Ext.getCmp('tgl_rujukan').setValue(res.data.response.rujukan.tglKunjungan);
										Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.rujukan.peserta.nama);
										Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.rujukan.peserta.noKartu);
										// Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.rujukan.peserta.hakKelas.keterangan)
										Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue('KELAS 3');
										Ext.getCmp('txt_perujuk').setValue(res.data.response.rujukan.provPerujuk.nama);
										Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
										Ext.getCmp('txt_no_rujukan').setValue(res.data.response.rujukan.noKunjungan);
										Ext.getCmp('txt_kd_diagnosa').setValue(res.data.response.rujukan.diagnosa.kode);
										Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.rujukan.diagnosa.nama);
										Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
										response_bpjs = cst.data.response;
										// loaddatastoreDokterPJBPJS2(tmp_poli_tujuan, res.data.response.rujukan.poliRujukan.kode);
                    					loaddatastoreDokterPJBPJS(tmp_poli_tujuan, res.data.response.rujukan.poliRujukan.kode);
										loaddatastoreUnitBPJS();
										Ext.Ajax.request({
											method: 'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
											params: {
												kd_poli_bpjs: res.data.response.rujukan.poliRujukan.kode
											},
											failure: function (o) {
												ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													console.log(cst.listData[0].nama_unit);
													Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
													polipilihanpasien = cst.listData[0].kd_unit;
												}
												else {
													ShowPesanError_viDaftar('Gagal membaca data unit', 'Error');
												}
											}

										});

									} else {
										loadMask.hide();
									}
								}, error: function (jqXHR, exception) {
									Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
								}
							});

						} else {
							ShowPesanError_viDaftar('Rujukan Tidak Ada ! ', 'Rawat Jalan');
						}
					} else {
						form_rujukan_multi_bpjs_RWJ_window.close();
						formLookup_rujukan_bpjs_RWJ(stat_rujukan);
						Ext.Ajax.request({
							method: 'POST',
							url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs_by_norujukan_RS_local",
							params: {
								no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN
							},
							success: function (o) {
								loadMask.hide();
								var cst = Ext.decode(o.responseText);
								if (cst.data.metaData.code == '200') {
									// tmp_kelas_rawat = cst.data.response.rujukan.peserta.hakKelas.kode;
									tmp_kelas_rawat = 3;
									tmp_tgl_rujukan = cst.data.response.rujukan.tglSep;
									tmp_ppk_rujukan = cst.data.response.rujukan.provPerujuk.kode;
									tmp_diagnosa = "";
									tmp_katarak = "0";
									tmp_cob = "0";
									tmp_eksekutif = "0";
									asal_rujukan = "2";
									tgl_sep_clean = cst.data.response.rujukan.tglSep;
									Ext.Ajax.request({
										method: 'POST',
										url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_kunjungan_rujukan",
										params: {
											no_rujukan: rowselectRujukan_byNoka.data.NO_KUNJUNGAN,
											kd_pasien: Ext.getCmp('txtNoRequest').getValue(),
                      						noka: Ext.getCmp('txtNoAskes').getValue(),
											kd_unit: polipilihanpasien
										},
										failure: function (o) {
											ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											kunjungan_pertama = cst.kunjungan_pertama;
											if (cst.kunjungan_pertama == false) {
												generate_surat_kontrol(false);
												loaddatastoreUnitBPJS();
												console.log('false');
												get_dokter_pertama(rowselectRujukan_byNoka.data.NO_KUNJUNGAN, Ext.getCmp('txtNoRequest').getValue());
												Ext.getCmp('txt_poli').enable();
												// Ext.getCmp('txt_no_surat_kontrol').enable();
											}
											else {
												generate_surat_kontrol(true);
												//Ext.getCmp('txt_poli').disable();
												selectTujuanKunjSEP = 0;
												Ext.getCmp('cboTujuanKunj').setValue('Normal');
												Ext.getCmp('cboTujuanKunj').disable();
											}
										}

									});

									var res = cst;
									if (res.data.response.rujukan.peserta.mr.noTelepon == '' || res.data.response.rujukan.peserta.mr.noTelepon == null) {
										console.log('1');
										Ext.getCmp('txt_no_tlp').setValue(Ext.getCmp('txttlpnPasien').getValue());
									} else {
										console.log('2');
										Ext.getCmp('txt_no_tlp').setValue(res.data.response.rujukan.peserta.mr.noTelepon);
									}
									Ext.getCmp('txtDiagnosa_RWJ').setValue(res.data.response.rujukan.diagnosa.kode + ' - ' + res.data.response.rujukan.diagnosa.nama);
									Ext.getCmp('txtNoRujukan').setValue(res.data.response.rujukan.noKunjungan);
									Ext.getCmp('txtNamaPeserta').setValue(res.data.response.rujukan.peserta.nama)
									tmp_ppk_rujukan = res.data.response.rujukan.provPerujuk.kode;
									// tmp_kelas_rawat =res.data.response.rujukan.peserta.hakKelas.kode;
									tmp_kelas_rawat = 3;
									tmp_diagnosa = res.data.response.rujukan.diagnosa.kode;
									// tmp_poli_tujuan =res.data.response.rujukan.poliRujukan.kode;
									tmp_tgl_rujukan = res.data.response.rujukan.tglKunjungan;
									Ext.getCmp('tgl_rujukan').setValue(res.data.response.rujukan.tglKunjungan);
									Ext.getCmp('txt_nama_peserta_rwj_insert').setValue(res.data.response.rujukan.peserta.nama);
									Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(res.data.response.rujukan.peserta.noKartu);
									// Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(res.data.response.rujukan.peserta.hakKelas.keterangan)
									Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue('KELAS 3');
									Ext.getCmp('txt_perujuk').setValue(res.data.response.rujukan.provPerujuk.nama);
									Ext.getCmp('txt_poli').setValue(res.data.response.rujukan.poliRujukan.nama);
									Ext.getCmp('txt_no_rujukan').setValue(res.data.response.rujukan.noKunjungan);
									Ext.getCmp('txt_kd_diagnosa').setValue(res.data.response.rujukan.diagnosa.kode);
									Ext.getCmp('txt_nama_diagnosa').setValue(res.data.response.rujukan.diagnosa.nama);

									Ext.getCmp('txt_no_medrec').setValue(Ext.getCmp('txtNoRequest').getValue());
									response_bpjs = cst.data.response;
									// loaddatastoreDokterPJBPJS(tmp_poli_tujuan,res.data.response.rujukan.poliRujukan.kode);
									loaddatastoreUnitBPJS();
									Ext.Ajax.request({
										method: 'POST',
										url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_spesialis_bpjs",
										params: {
											kd_poli_bpjs: res.data.response.rujukan.poliRujukan.kode
										},
										failure: function (o) {
											ShowPesanWarning_viDaftar('Unit Tidak Ada', 'Warning');
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success === true && cst.totalrecords > 0) {
												console.log(cst.listData[0].nama_unit);
												Ext.getCmp('cboPoliklinikRequestEntry').setValue(cst.listData[0].nama_unit);
												polipilihanpasien = cst.listData[0].kd_unit;
											}
										}

									});
									Ext.Ajax.request({
										method: 'POST',
										url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_map_rujukan_bpjs",
										params: {
											kd_prov_rujukan: cst.data.response.rujukan.provPerujuk.kode
										},
										failure: function (o) {
											ShowPesanWarning_viDaftar('Mapping Belum Lengkap', 'Warning');
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												console.log(cst);
												Ext.getCmp('cboRujukanRequestEntry').setValue(cst.listData[0].rujukan);
												koderujuk = cst.listData[0].kd_rujukan;
											}
											else {
												ShowPesanError_viDaftar('Gagal membaca rujukan', 'Error');
											}
										}

									});

								} else {
									loadMask.hide();
								}
							}, error: function (jqXHR, exception) {
								Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
							}
						});
						// Ext.getCmp('cboDokterPJBPJS').hide();
						// Ext.getCmp('cboDokterPJBPJSKunj2').show();
						// loaddatastoreUnitBPJS();
					}
				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colNoKunjunganMultiRujukan',
				header: 'NO RUJUKAN',
				dataIndex: 'NO_KUNJUNGAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colNoKartuMultiRujukan',
				header: 'NO KARTU',
				dataIndex: 'NO_KARTU',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colNamaPesertaMultiRujukan',
				header: 'NAMA PESERTA',
				dataIndex: 'NAMA_PESERTA',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'coltglKunjunganMultiRujukan',
				header: 'TANGGAL KUNJUNGAN',
				dataIndex: 'TGL_KUNJUNGAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'coPerujukMultiRujukan',
				header: 'PPK PERUJUK',
				dataIndex: 'PPK_PERUJUK',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			},
			{
				id: 'colPoliMultiRujukan',
				header: 'SPESIALIS',
				dataIndex: 'SPESIALIS',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 1
			}
		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListFKTP
				]
			}
		]
	};
	return items;
}
// ---------------------------------------------------------------------

// UPDATE FORM LOOPUP RUJUKAN BPJS SINGLE RECORD GAN
function formLookup_rujukan_bpjs_RWJ(stat_rujukan) {
	var lebar = 650;
	formLookups_rujukan_bpjs = new Ext.Window({
		id: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs',
		name: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs',
		title: 'Form Generate SEP',
		closeAction: 'destroy',
		width: lebar,
		height: 380, //575,
		resizable: true,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_rujukan_bpjs_RWJ(stat_rujukan), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookups_rujukan_bpjs.show();
}

// --------------------------Update Get Surat Kontrol-------------------------
// Egi 08 Febuari 2022
function generate_surat_kontrol(kunjungan,spesialis) {
	Ext.Ajax.request({
		method: 'POST',
		url: baseURL + "index.php/rawat_jalan/functionRWJ/generate_no_surat_kontrol",
		params: {
			noka: Ext.getCmp('txtNoAskes').getValue(),
			kd_unit: polipilihanpasien,
			kunjungan_pertama: kunjungan,
			sep_rwi: sep_rwi
		},
		failure: function (o) {
			ShowPesanError_viDaftar('Gagal Mengambil No Surat Kontrol', 'Error');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if (cst.succes == true) {
				if(cst.surat_kontrol != null && cst.surat_kontrol != ''){
					Ext.getCmp('lbl_no_surat_kontrol').show();
					Ext.getCmp('lbl2_no_surat_kontrol').show();
					Ext.getCmp('txt_no_surat_kontrol').show();
					Ext.getCmp('txt_no_surat_kontrol').setValue(cst.surat_kontrol);
					Ext.getCmp('cboDokterPJBPJS').setValue(cst.nama_dokter);
					kd_dpjp_kontrol = cst.kd_dpjp;
					kd_dokter_dpjp_insert = cst.kode_dokter;
					if (cst.kode_poli != null | cst.kode_poli != '') {
						// Ext.getCmp('txt_poli').setValue(cst.kode_poli);
					}
					// rwi_kode_poli = cst.kode_poli;
					// console.log("rwi1"+rwi_kode_poli);
					// rwi_nama_poli = cst.nama_poli;
					// rwi_kode_dokter = cst.kode_dokter;
					// rwi_nama_dokter = cst.nama_dokter;
					// rwi_diag_awal = cst.diag_awal;
					// rwi_kd_prov = cst.kd_prov;
					// rwi_kd_kab = cst.kd_kab;
					// rwi_kd_kec = cst.kd_kec;
					// rwi_no_tlp = cst.no_tlp;
					// rwi_kd_dpjp = cst.kd_dpjp;
					// Ext.getCmp('txt_no_surat_kontrol').disable();
				}else{
					console.log(kunjungan_pertama);
						kd_dpjp_kontrol = '';
						kd_dokter_dpjp_insert = '';
						Ext.getCmp('cboDokterPJBPJS').setValue('');
						Ext.getCmp('txt_no_surat_kontrol').setValue('');
					if(kunjungan_pertama == true || kunjungan_pertama == 'true'){
						Ext.getCmp('lbl_no_surat_kontrol').hide();
						Ext.getCmp('lbl2_no_surat_kontrol').hide();
						Ext.getCmp('txt_no_surat_kontrol').hide();
					}else if(poli_rujukan_asal != spesialis){
						Ext.getCmp('lbl_no_surat_kontrol').hide();
						Ext.getCmp('lbl2_no_surat_kontrol').hide();
						Ext.getCmp('txt_no_surat_kontrol').hide();
					}else{
						Ext.getCmp('lbl_no_surat_kontrol').show();
						Ext.getCmp('lbl2_no_surat_kontrol').show();
						Ext.getCmp('txt_no_surat_kontrol').show();
					}
					// ShowPesanWarning_viDaftar('No Surat Kontrol Tidak Ada','Warning')
				}
			} else {
				kd_dpjp_kontrol = '';
				kd_dokter_dpjp_insert = '';
				Ext.getCmp('cboDokterPJBPJS').setValue('');
				Ext.getCmp('txt_no_surat_kontrol').setValue('');
				if(kunjungan_pertama == true || kunjungan_pertama == 'true'){
					Ext.getCmp('lbl_no_surat_kontrol').hide();
					Ext.getCmp('lbl2_no_surat_kontrol').hide();
					Ext.getCmp('txt_no_surat_kontrol').hide();
				// ShowPesanWarning_viDaftar('Gagal Mengambil No Surat Kontrol', 'Warning');
				}else if(poli_rujukan_asal != spesialis){
					Ext.getCmp('lbl_no_surat_kontrol').hide();
					Ext.getCmp('lbl2_no_surat_kontrol').hide();
					Ext.getCmp('txt_no_surat_kontrol').hide();
				}else{
					Ext.getCmp('lbl_no_surat_kontrol').show();
					Ext.getCmp('lbl2_no_surat_kontrol').show();
					Ext.getCmp('txt_no_surat_kontrol').show();
				}
			}
		}
	});
}
// ---------------------------------------------------------------------------

// UPDATE PANEL LOOPUP RUJUKAN BPJS SINGLE RECORD Egi 09 Febuari 2022
function panel_form_lookup_rujukan_bpjs_RWJ(stat_rujukan) {
  var formPanel = new Ext.form.FormPanel({
    id: "myformpanel_rujukan_bpjs",
    bodyStyle: "padding:5px",
    labelAlign: "top",
    defaults: {},
    columnWidth: 0.1,
    layout: "absolute",
    border: true,
    // width: 500,
    height: 350,
    //anchor: '100% 100%',
    items: [
      {
        x: 10,
        y: 5,
        xtype: "label",
        text: "Nama Peserta",
      },
      {
        x: 120,
        y: 5,
        xtype: "label",
        text: ":",
      },
      {
        x: 130,
        y: 5,
        xtype: "textfield",
        id: "txt_nama_peserta_rwj_insert",
        name: "txt_nama_peserta_rwj_insert",
        width: 200,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 340,
        y: 5,
        xtype: "label",
        text: "No Kartu",
      },
      {
        x: 410,
        y: 5,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 5,
        xtype: "textfield",
        id: "txt_no_kartu_peserta_rwj",
        name: "txt_no_kartu_peserta_rwj",
        width: 200,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 10,
        y: 30,
        xtype: "label",
        text: "Hak Kelas",
      },
      {
        x: 120,
        y: 30,
        xtype: "label",
        text: ":",
      },
      {
        x: 130,
        y: 30,
        xtype: "textfield",
        id: "txt_hak_kelas_peserta_rwj",
        name: "txt_hak_kelas_peserta_rwj",
        width: 200,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        maxLength: 3,
        tabIndex: 1,
      },
      {
        x: 340,
        y: 30,
        xtype: "label",
        text: "Perujuk",
      },
      {
        x: 410,
        y: 30,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 30,
        xtype: "textfield",
        id: "txt_perujuk",
        name: "txt_perujuk",
        width: 200,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        maxLength: 3,
        tabIndex: 1,
      },
      {
        x: 10,
        y: 55,
        xtype: "label",
        text: "Spesialis/SubSpesialis",
      },
      {
        x: 120,
        y: 55,
        xtype: "label",
        text: ":",
      },

      McomboUnitBPJS(stat_rujukan),

      {
        xtype: "textfield",
        id: "txt_kd_spesialis",
        name: "txt_kd_spesialis",
        width: 150,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        maxLength: 3,
        tabIndex: 1,
        hidden: true,
      },

      {
        x: 340,
        y: 55,
        xtype: "label",
        text: "Eksekutif",
      },
      {
        x: 410,
        y: 55,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 55,
        xtype: "checkbox",
        boxLabel: "Eksekutif ",
        name: "cbo_eksekutif",
        id: "cbo_eksekutif",
        listeners: {
          check: function (checkbox, isChecked) {
            if (isChecked === true) {
              tmp_eksekutif = 1;
            } else {
              tmp_eksekutif = 0;
            }
          },
        },
      },

      {
        x: 10,
        y: 80,
        xtype: "label",
        text: "Tgl.Rujukan",
      },
      {
        x: 120,
        y: 80,
        xtype: "label",
        text: ":",
      },
      {
        xtype: "datefield",
        x: 130,
        y: 80,
        id: "tgl_rujukan",
        name: "tgl_rujukan",
        format: "d/M/Y",
        readOnly: true,
        disabled: true,
        allowBlank: false,
        readOnly: true,
        width: 200,
        tabIndex: 1,
      },

      {
        x: 340,
        y: 75,
        xtype: "label",
        text: "No. Rujukan",
      },
      {
        x: 410,
        y: 75,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 75,
        xtype: "textfield",
        id: "txt_no_rujukan",
        name: "txt_no_rujukan",
        width: 200,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 10,
        y: 105,
        xtype: "label",
		id: "lbl_no_surat_kontrol",
        text: "No.Surat Kontrol/SKDP",
      },
      {
        x: 120,
        y: 105,
        xtype: "label",
		id: "lbl2_no_surat_kontrol",
        text: ":",
      },
      {
        x: 130,
        y: 105,
        xtype: "textfield",
        id: "txt_no_surat_kontrol",
        name: "txt_no_surat_kontrol",
        width: 200,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
        value: tmp_nomor_skdp,
        // autoCreate: { tag: "input", type: "text", size: "6", autocomplete: "off", maxlength: "6" },
      },

      {
        x: 340,
        y: 100,
        xtype: "label",
        text: "Dokter",
      },
      {
        x: 410,
        y: 100,
        xtype: "label",
        text: ":",
      },
      {
        x: 380,
        y: 125,
        xtype: "textfield",
        id: "txt_kd_dokter_dpjp",
        name: "txt_kd_dokter_dpjp",
        width: 150,
        disabled: false,
        hidden: true,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },
      mCombDokterBPJS(),

      {
        x: 10,
        y: 130,
        xtype: "label",
        text: "Tgl.SEP",
      },
      {
        x: 120,
        y: 130,
        xtype: "label",
        text: ":",
      },
      {
        xtype: "datefield",
        x: 130,
        y: 130,
        id: "tgl_SEP",
        name: "tgl_SEP",
        format: "d/M/Y",
        readOnly: false,
        value: now,
        width: 200,
      },
			{
				x: 340,
				y: 130,
				xtype: 'label',
				text: 'TujuanKunj'
			},
			{
				x: 410,
				y: 130,
				xtype: 'label',
				text: ':'
			},
      MComboTujuanKunjSEP(),
      {
				x: 340,
				y: 150,
				xtype: 'label',
				text: 'Katarak'
			},
      {
        x: 410,
        y: 150,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 150,
        xtype: "checkbox",
        boxLabel: "",
        name: "cbo_katarak",
        id: "cbo_katarak",
        listeners: {
          check: function (checkbox, isChecked) {
            if (isChecked === true) {
              tmp_katarak = 1;
              tmp_katarak_update = 1;
            } else {
              tmp_katarak = 0;
              tmp_katarak_update = 0;
            }
          },
        },
      },

      {
        x: 10,
        y: 155,
        xtype: "label",
        text: "No Medrec",
      },
      {
        x: 120,
        y: 155,
        xtype: "label",
        text: ":",
      },
      {
        x: 130,
        y: 155,
        xtype: "textfield",
        id: "txt_no_medrec",
        name: "txt_no_medrec",
        width: 200,
        disabled: true,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },
      {
        x: 340,
        y: 165,
        xtype: "label",
        text: "Peserta COB",
      },
      {
        x: 410,
        y: 165,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 165,
        xtype: "checkbox",
        boxLabel: "",
        name: "cbo_peserta_cob",
        id: "cbo_peserta_cob",
        disabled: false,
        listeners: {
          check: function (checkbox, isChecked) {
            if (isChecked === true) {
              tmp_cob = 1;
            } else {
              tmp_cob = 0;
            }
          },
        },
      },

      {
        x: 340,
        y: 125,
        xtype: "label",
        text: "Dokter",
        id: "lbl_dokter_kunj_2",
        hidden: true,
      },
      {
        x: 410,
        y: 125,
        xtype: "label",
        text: ":",
        id: "separator_dokter_kunj_2",
        hidden: true,
      },

      mCombDokterBPJS_kunj2(),

      {
        x: 10,
        y: 180,
        xtype: "label",
        text: "Diagnosa",
      },
      {
        x: 120,
        y: 180,
        xtype: "label",
        text: ":",
      },
      {
        x: 130,
        y: 180,
        xtype: "textfield",
        id: "txt_kd_diagnosa",
        name: "txt_kd_diagnosa",
        width: 50,
        disabled: true,
        hidden: true,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 130,
        y: 180,
        xtype: "textfield",
        id: "txt_nama_diagnosa",
        name: "txt_nama_diagnosa",
        width: 300,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
        listeners: {
          specialkey: function (text, e) {
				if (e.getKey() == 13) {
					//vclaim 2.0 // hani 24-03-2022 // Diagnosa di Generate SEP bisa di edit
					if (Ext.getCmp('txt_nama_diagnosa').getValue().trim() != '') {
						form_daftar_diagnosa();
						loadMask.show();
						Ext.Ajax.request({
							method: 'POST',
							url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_data_diagnosa",
							params: {
								nama_diagnosa: Ext.getCmp('txt_nama_diagnosa').getValue()
							},
							success: function (o) {
								loadMask.hide();
								var cst = Ext.decode(o.responseText);
								//dataSourceListDiagosa.removeAll();
								for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
									var recs = [], recType = dataSourceListDiagosa.recordType;
									console.log(recs);
									var o = cst['listData'][i];
									recs.push(new recType(o));
									dataSourceListDiagosa.add(recs);
								}

							}, error: function (jqXHR, exception) {
								Ext.getCmp('txt_nama_diagnosa').setValue('Terjadi kesalahan dari server');
							}
						});
					} else {
						Ext.MessageBox.show({
							title: 'Informasi',
							msg: 'Harap Nama Diagnosa.',
							buttons: Ext.MessageBox.OK,
							icon: Ext.MessageBox.INFO,
							fn: function (btn) {
								if (btn == 'ok') {
									Ext.getCmp('txt_nama_diagnosa').focus();
								}
							}
						});
					}
				}
			}
        },
      },

      {
        x: 340,
        y: 205,
        xtype: "label",
        text: "No Tlp",
      },
      {
        x: 410,
        y: 205,
        xtype: "label",
        text: ":",
      },
      {
        x: 420,
        y: 205,
        xtype: "textfield",
        id: "txt_no_tlp",
        name: "txt_no_tlp",
        width: 200,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 10,
        y: 205,
        xtype: "label",
        text: "Catatan",
      },
      {
        x: 120,
        y: 205,
        xtype: "label",
        text: ":",
      },
      {
        x: 130,
        y: 205,
        xtype: "textfield",
        id: "txt_no_catatan",
        name: "txt_no_catatan",
        width: 200,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
      },

      {
        x: 10,
        y: 235,
        xtype: "label",
        text: "No SEP",
        id: "lbl_no_sep",
        hidden: true,
      },
      {
        x: 120,
        y: 235,
        xtype: "label",
        text: ":",
        id: "separator_lbl_no_sep",
        hidden: true,
      },
      {
        x: 130,
        y: 235,
        xtype: "textfield",
        id: "txt_no_sep_rwj",
        name: "txt_no_sep_rwj",
        width: 200,
        disabled: false,
        allowBlank: false,
        readOnly: false,
        maxLength: 3,
        tabIndex: 1,
        hidden: true,
      },
			{
				x: 10,
				y: 235,
				xtype: 'label',
				text: 'flagprocedure'
			},
			{
				x: 120,
				y: 235,
				xtype: 'label',
				text: ':'
			},
			MComboFlagProcedure(),
			{
				x: 340,
				y: 235,
				xtype: 'label',
				text: 'Penunjang'
			},
			{
				x: 410,
				y: 235,
				xtype: 'label',
				text: ':'
			},
			MComboPenunjang(),
			{
				x: 10,
				y: 265,
				xtype: 'label',
				text: 'AssesmentPel'
			},
			{
				x: 120,
				y: 265,
				xtype: 'label',
				text: ':'
			},
			MComboAssementpel(),
    ],
    buttons: [
      {
        text: "Generate SEP",
        id: "generate_sep",
        handler: function () 
        {
					if(Ext.getCmp('txt_no_tlp').getValue()=='' || Ext.getCmp('txt_no_tlp').getValue()==''){
						ShowPesanWarning_viDaftar('Nomor Telepon Harus Di isi!', 'Generate SEP RWJ');
					}else{
						Ext.Ajax.request({
							url: baseURL + "index.php/rawat_jalan/functionRWJ/getValidasiFinger",
								params: {noka : Ext.getCmp('txt_no_kartu_peserta_rwj').getValue(), poli_tujuan: tmp_poli_tujuan_kunj2, tgl_sep:Ext.getCmp('tgl_SEP').getValue() },
								failure: function(o){
									var cst = Ext.decode(o.responseText);
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
									console.log(cst.validasi);
									if(cst.pesan != '' && cst.validasi == false) {
										// pesan_finger = cst.pesan;
										ShowPesanWarning_viDaftar(cst.pesan, 'Generate Sep');
										// validasi_finger = "gagal";
									}else{
										ajax_cetak_sep();
									}
									// return cst;
								}
						});
					}
					// datasave_viDaftar(addNew_viDaftar, false, true);
    			//formLookups_rujukan_bpjs.close();	
        },
      },
      {
        text: "Update SEP",
        id: "update_sep",
        hidden: true,
        handler: function () {
          ajax_update_sep();
          //formLookups_rujukan_bpjs.close();
        },
      },
    ],
  });
  return formPanel;
}

function MComboPenunjang() {
	var Field = ['KD_PENUNJANG', 'PENUNJANG'];
	dspenunjang = new WebApp.DataStore({ fields: Field });
	var cboPenunjang = new Ext.form.ComboBox
		(
			{
				id: 'cboPenunjang',
				x: 420,
				y: 235,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				tabIndex: 3,
				selectOnFocus: true,
				disabled: true,
				forceSelection: true,
				emptyText: 'KdPenunjang',
				fieldLabel: 'KdPenunjang',
				editable: false,
				width: 200,
				store: dspenunjang,
				valueField: 'KD_PENUNJANG',
				displayField: 'PENUNJANG',
				enableKeyEvents: true,
				listeners: {
					'select': function (a, b, c) {
						console.log(b);
						selectKdPenunjang = b.data.Id;
						console.log(selectKdPenunjang);
					},
				}
			}
		);
	return cboPenunjang;
};

function MComboAssementpel() {
	var cboAssement = new Ext.form.ComboBox
		(
			{
				id: 'cboAssement',
				x: 130,
				y: 265,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				tabIndex: 3,
				selectOnFocus: true,
				disabled: true,
				forceSelection: true,
				emptyText: 'AssesmentPel',
				fieldLabel: 'AssementPel',
				editable: false,
				width: 300,
				store: new Ext.data.ArrayStore
					(
						{
							id: 0,
							fields:
								[
									'Id',
									'displayText'
								],
							data: [[1, 'Poli Spesialis tidak tersedia pada hari sebelumnya'], [2, 'Jam Poli telah berakhir pada hari sebelumnya'],
							[3, 'Dokter Spesialis yang dimaksud tidak praktek pada hari sebelumnya'], [4, 'Atas Instruksi RS'], [5, 'Tujuan Kontrol']]
						}
					),
				valueField: 'Id',
				displayField: 'displayText',
				enableKeyEvents: true,
				listeners: {
					'select': function (a, b, c) {
						console.log(b);
						selectAssementPel = b.data.Id;
						console.log(selectAssementPel);
					},
				}
			}
		);
	return cboAssement;
};

function MComboFlagProcedure() {
	var cboFlag = new Ext.form.ComboBox
		(
			{
				id: 'cboFlag',
				x: 130,
				y: 235,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				tabIndex: 3,
				selectOnFocus: true,
				disabled: true,
				forceSelection: true,
				emptyText: 'Flag Procedure',
				fieldLabel: 'Flag Procedure',
				editable: false,
				width: 200,
				store: new Ext.data.ArrayStore
					(
						{
							id: 0,
							fields:
								[
									'Id',
									'displayText'
								],
							data: [[0, 'Prosedur Tidak Berkelanjutan'], [1, 'Prosedur dan Terapi Berkelanjutan']]
						}
					),
				valueField: 'Id',
				displayField: 'displayText',
				enableKeyEvents: true,
				listeners: {
					'select': function (a, b, c) {
						console.log(b);
						selectFlagProcedure = b.data.Id;
						if (b.data.Id == 1) {
							loaddatastorepenunjang(true);
						} else {
							loaddatastorepenunjang(false);
						}
						console.log(selectFlagProcedure);
					},
				}
			}
		);
	return cboFlag;
};

function loaddatastorepenunjang(selectFlagProcedure) {
	dspenunjang.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'penunjang',
					Sortdir: 'ASC',
					target: 'ViewComboPenunjang',
					param: 'flag=' + selectFlagProcedure
				}
			}
		)
}

function MComboTujuanKunjSEP() {
	var cboTujuanKunj = new Ext.form.ComboBox
		(
			{
				id: 'cboTujuanKunj',
				x: 420,
				y: 125,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				tabIndex: 3,
				selectOnFocus: true,
				forceSelection: true,
				emptyText: 'Tujuan Kunjungan',
				fieldLabel: 'Tujuan Kunjungan',
				editable: false,
				width: 200,
				store: new Ext.data.ArrayStore
					(
						{
							id: 1,
							fields:
								[
									'Id',
									'displayText'
								],
							data: [[0, 'Normal'], [1, 'Prosedur'], [2, 'Konsul Dokter']]
						}
					),
				valueField: 'Id',
				displayField: 'displayText',
				enableKeyEvents: true,
				listeners: {
					'select': function (a, b, c) {
						console.log(b);
						selectTujuanKunjSEP = b.data.Id;
						if (b.data.Id == 0) {
							Ext.getCmp('cboFlag').disable();
							Ext.getCmp('cboFlag').setValue(null);
							selectFlagProcedure = null;
							Ext.getCmp('cboPenunjang').disable();
							Ext.getCmp('cboPenunjang').setValue(null);
							selectKdPenunjang = null;
							Ext.getCmp('cboAssement').enable();
						} else if (b.data.Id == 1) {
							Ext.getCmp('cboPenunjang').enable();
							Ext.getCmp('cboFlag').enable();
							Ext.getCmp('cboAssement').disable();
							Ext.getCmp('cboAssement').setValue(null);
							selectAssementPel = null;
						} else if (b.data.Id == 2) {
							Ext.getCmp('cboFlag').disable();
							Ext.getCmp('cboFlag').setValue(null);
							selectFlagProcedure = null;
							Ext.getCmp('cboPenunjang').disable();
							Ext.getCmp('cboPenunjang').setValue(null);
							selectKdPenunjang = null;
							Ext.getCmp('cboAssement').enable();
						}

						console.log(selectTujuanKunjSEP);
					},
				}
			}
		);
	return cboTujuanKunj;
};
// ---------------------------------------------------------------------
//UPDATE BPJS V2.0 (INSERT SEP) Egi 09 Febuari 2022 //
function ajax_cetak_sep() {
  console.log(kunjungan_pertama);
  // loadMask.show();
  Ext.Ajax.request({
    url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
    params 		: {
      select 	: " count(*) as count ",
      where 	: " kd_pasien = '"+Ext.getCmp('txtNoRequest').getValue()+"' AND kd_unit = '"+polipilihanpasien+"' AND tgl_masuk = '"+now.format("Y-m-d")+"'",
      table 	: "	kunjungan ",
    },
    success: function(o){
      var noka = Ext.getCmp('txt_no_kartu_peserta_rwj').getValue();
      var cst 	= Ext.decode(o.responseText);
      if (cst[0].count > 0 || cst[0].count != '0') {
        Ext.Ajax.request({
          url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
          params 		: {
            select 	: " count(*) as count ",
            where 	: " kd_pasien = '"+Ext.getCmp('txtNoRequest').getValue()+"' AND kd_unit = '"+polipilihanpasien+"' AND tgl_masuk = '"+now.format("Y-m-d")+"'",
            table 	: "	rujukan_kunjungan ",
          },
          success: function(o) {
            console.log(cst);
            var cst_rujukan 	= Ext.decode(o.responseText);
              $.ajax({
                  type: 'POST',
                  dataType: 'JSON',
                  crossDomain: true,
                  url: baseURL + "index.php/rawat_jalan/functionRWJ/GenerateSEP/",
                  data: getParamSepBpjs(), 
                  success: function (resp1) {
                    loadMask.hide();
                    tmp_katarak=0;
                    tmp_eksekutif=0;
                    tmp_cob=0;
                    if (resp1.metaData.code == '200') {
                      formLookups_rujukan_bpjs.close();
                      console.log(resp1.response);
                      Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
                      Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
                      Ext.getCmp('txtDiagnosa_RWJ').setValue(resp1.response.sep.diagnosa);
                      Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
                      Ext.getCmp('txtNoSJP').focus();
                      if(kunjungan_pertama == true || sep_rwi != ''){
                        var url = baseURL + "index.php/laporan/lap_sep/cetak/" + resp1.response.sep.noSep+"/true";
                        new Ext.Window({
                          title: 'SEP',
                          width: 900,
                          height: 600,
                          constrain: true,
                          modal: false,
                          html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
                          tbar 	: [
                            {
                              xtype 	: 'button',
                              text 	: '<i class="fa fa-print"></i> Cetak',
                              handler : function(){
                                window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue()+"/false", "_blank");
                              }
                            }
                          ]
                        }).show();
                      }else{
                          var url = baseURL + "index.php/laporan/lap_sep/cetak2/" + resp1.response.sep.noSep+"/" +resp1.response.sep.poli+"/true";
                          // var url = baseURL + "index.php/laporan/lap_sep_internal/cetak/" + o.nosep + "/" + o.nosepref + "/" + o.tglrujukinternal + "/" + o.kdpolituj + "/" + line + "/true";
                          new Ext.Window({
                            title: 'SEP Internal',
                            width: 900,
                            height: 500,
                            constrain: true,
                            modal: false,
                            html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
                            tbar: [
                              {
                                xtype: 'button',
                                text: '<i class="fa fa-print"></i> Cetak',
                                handler: function () {
                                  window.open(baseURL + "index.php/laporan/lap_sep_internal/cetak/" + o.nosep + "/" + o.nosepref + "/" + o.tglrujukinternal + "/" + o.kdpolituj + "/" + line + "/false", "_blank");
                                }
                              }
                            ]
                          }).show();
                      }
                      
                      // datasave_viDaftar(addNew_viDaftar,false);
                    }else{
                      Ext.MessageBox.alert('Gagal', resp1.metaData.message );
                    }
                  },
                  error: function (jqXHR, exception) {
                    loadMask.hide();
                    Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
                    Ext.Ajax.request({
                      url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                      params: {
                        respon: jqXHR.responseText,
                        keterangan: 'Error create SEP'
                      }
                    });
                  }
                });
            // }

          }
        });
      }else{
        var noka = Ext.getCmp('txt_no_kartu_peserta_rwj').getValue();
        Ext.Ajax.request({
                  url: baseURL + "index.php/rawat_jalan/Controller_kunjungan/save",
                  params: dataparam_viDaftar(),
                  failure: function (o){
                    loadMask.hide();
                    tmp_sub_unit_rehab = "";
                    //Ext.getCmp('btnSimpan_viDaftar').enable();
                  },
                  success: function (o){
                    loadMask.hide();
                    //Ext.getCmp('btnSimpan_viDaftar').enable();
                    Ext.getCmp('btnSimpan_viDaftar').disable();
                    var cst = Ext.decode(o.responseText);
                    // ShowPesanInfo_viDaftar(cst.message, 'Informasi');
                    tmp_sub_unit_rehab = "";
                    Ext.getCmp('txtNoRequest').setValue(cst.kd_pasien);
                    resp_bpjs.kd_pasien  = cst.kd_pasien;
                    resp_bpjs.kd_unit    = cst.kd_unit;
                    resp_bpjs.tgl_masuk  = cst.tgl_masuk;
                    resp_bpjs.urut_masuk = cst.urut_masuk;
                    printtracer();
                    $.ajax({
                      type: 'POST',
                      dataType: 'JSON',
                      crossDomain: true,
                      url: baseURL + "index.php/rawat_jalan/functionRWJ/GenerateSEP/",
                      data: getParamSepBpjs(), 
                      success: function (resp1) {
                        loadMask.hide();
                        tmp_katarak=0;
                        tmp_eksekutif=0;
                        tmp_cob=0;
                        if (resp1.metaData.code == '200') {
                          formLookups_rujukan_bpjs.close();
                          console.log(resp1.response.sep.peserta.noKartu);
                          Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
                          Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
                          Ext.getCmp('txtDiagnosa_RWJ').setValue(resp1.response.sep.diagnosa);
                          Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
                          Ext.getCmp('txtNoSJP').focus();
                          if(kunjungan_pertama == true || sep_rwi != ''){
                          var url = baseURL + "index.php/laporan/lap_sep/cetak/" + resp1.response.sep.noSep+"/true";
                          new Ext.Window({
                            title: 'SEP',
                            width: 900,
                            height: 600,
                            constrain: true,
                            modal: false,
                            html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
                            tbar 	: [
                              {
                                xtype 	: 'button',
                                text 	: '<i class="fa fa-print"></i> Cetak',
                                handler : function(){
                                  window.open(baseURL + "index.php/laporan/lap_sep/cetak/" + Ext.get('txtNoSJP').getValue()+"/false", "_blank");
                                }
                              }
                            ]
                          }).show();
                        }else{
                            
                          var url = baseURL + "index.php/laporan/lap_sep/cetak2/" + resp1.response.sep.noSep+"/" +resp1.response.sep.poli+"/true";
                          new Ext.Window({
                              title: 'SEP Internal',
                              width: 900,
                              height: 500,
                              constrain: true,
                              modal: false,
                              html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
                              tbar: [
                                {
                                  xtype: 'button',
                                  text: '<i class="fa fa-print"></i> Cetak',
                                  handler: function () {
                                    window.open(baseURL + "index.php/laporan/lap_sep_internal/cetak/" + o.nosep + "/" + o.nosepref + "/" + o.tglrujukinternal + "/" + o.kdpolituj + "/" + line + "/false", "_blank");
                                  }
                                }
                              ]
                            }).show();
                        }

                            
                          // datasave_viDaftar(addNew_viDaftar,false);
                        }else{
                          Ext.MessageBox.alert('Gagal', resp1.metaData.message );
                        }
                      },
                      error: function (jqXHR, exception) {
                        loadMask.hide();
                        Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
                        Ext.Ajax.request({
                          url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                          params: {
                            respon: jqXHR.responseText,
                            keterangan: 'Error create SEP'
                          }
                        });
                      }
                    })
                  }
                });
        

      }
    }
  });
}	

function loadDataComboProvinsiLaka() {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_provisi_from_bpjs",
		params: '0',
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsPropinsiLokasiLaka.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsPropinsiLokasiLaka.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsPropinsiLokasiLaka.add(recs);
			}
			console.log(dsPropinsiLokasiLaka);
		}
	});
}

function getParamGetKabupaten() {
	var params =
	{
		kode_propinsi: selectProvinsiLokasiLaka,

	}
	return params
};

function loaddatastoreKabupatenLakalantas(kd_provinsi) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kabupaten_from_bpjs",
		params: getParamGetKabupaten(),
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsKabupatenLokasiLaka.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsKabupatenLokasiLaka.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsKabupatenLokasiLaka.add(recs);
			}
			console.log(dsKabupatenLokasiLaka);
		}
	});
}

function getParamGetKecamatan() {
	var params =
	{
		kode_kab: selectKabupatenLokasiLaka,

	}
	return params
};

function loaddatastorekecamatanLakalantas(kd_kab) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kecamatan_from_bpjs",
		params: getParamGetKecamatan(),
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsKecamatanLokasiLaka.remove();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsKecamatanLokasiLaka.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsKecamatanLokasiLaka.add(recs);
			}
			console.log(dsKecamatanLokasiLaka);
		}
	});
}

// ------------------------Proses Get Dokter Bpjs--------------------------------
// Egi 09 Febuari 2022
function loaddatastoreDokterPJBPJS(tmp_poli_tujuan, spesialis) {
	console.log(polipilihanpasien);
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_dokter_bpjs",
		params: {
			unit : 'RWJ',
			kd_unit : polipilihanpasien,
			// unit: polipilihanpasien,
			spesialis: spesialis
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsDokterPJBPJS.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsDokterPJBPJS.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsDokterPJBPJS.add(recs);
			}
			console.log(dsDokterPJBPJS);
		}
	});
}

function loaddatastoreDokterPJBPJS2(unit, spesialis) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_dokter_bpjs2",
		params: {
			unit: unit,
			spesialis: spesialis
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsDokterPJBPJSKunj2.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsDokterPJBPJSKunj2.recordType;
				var o = cst['listData'][i];
				recs.push(new recType(o));
				dsDokterPJBPJSKunj2.add(recs);
			}
			console.log(dsDokterPJBPJSKunj2);
		}
	});
}

function loaddatastoreDokterPJBPJSKunj2(tmp_poli_tujuan) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kode_dokter_bpjs",
		params: {
			unit: tmp_poli_tujuan,
			tgl_rujukan: Ext.getCmp('tgl_rujukan').getValue()
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsDokterPJBPJSKunj2.removeAll();
			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [], recType = dsDokterPJBPJSKunj2.recordType;
				var o = cst['listData'][i];

				recs.push(new recType(o));
				dsDokterPJBPJSKunj2.add(recs);
			}
			console.log(dsDokterPJBPJSKunj2);
		}
	});
}

function mCombDokterBPJS() {
	var Field = ['kode', 'nama'];
	dsDokterPJBPJS = new WebApp.DataStore({ fields: Field });
	var cboDokterPJBPJS = new Ext.form.ComboBox({
		id: 'cboDokterPJBPJS',
		x: 420,
		y: 100,
		width: 200,
		readOnly: false,
		hidden: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Dokter...',
		fieldLabel: 'Dokter',
		align: 'Right',
		tabIndex: 6,
		store: dsDokterPJBPJS,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_kd_dokter_bpjs = b.data.kode;
				select_kd_dpjp = b.data.kode;
				kd_dokter_dpjp_insert = b.data.kode;
				console.log(kd_dokter_dpjp_insert);
				//loaddatastorekelurahan(b.data.KD_KECAMATAN);
				console.log(b.data.kode);
				Ext.getCmp('txt_kd_dokter_dpjp').setValue(b.data.kode);
				Ext.Ajax.request({
					url: baseURL + "index.php/rawat_jalan/functionRWJ/get_map_dokter_bpjs",
					params: { kd_dokter: b.data.kode },
					failure: function (o) {
						var cst = Ext.decode(o.responseText);
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						console.log(cst);
						Ext.getCmp('cboDokterRequestEntry').setValue(cst.listData[0].nama);
						kodeDokterDefault = cst.listData[0].kd_dokter;

					}
				});
			}
		}
	});
	return cboDokterPJBPJS;
}

function McomboUnitBPJS(stat_rujukan) {
  // DISINI
  var Field = ["KODE", "NAMA"];
  dsUnitBPJS = new WebApp.DataStore({ fields: Field });
  var cboUnitBPJS = new Ext.form.ComboBox({
    id: "txt_poli",
    x: 130,
    y: 55,
    width: 200,
    readOnly: false,
    hidden: false,
    //disable:true,
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih Unit...",
    fieldLabel: "Unit",
    align: "Right",
    tabIndex: 6,
    store: dsUnitBPJS,
    valueField: "kode",
    displayField: "nama",
    enableKeyEvents: true,
    listeners: {
      select: function (a, b, c) {
        console.log(sep_rwi);
        if(sep_rwi == '' || sep_rwi==null){
			console.log('yes seprwi');
          cek_kunjungan_kontrol(b.data.kode,b.data.kode_unit_nci);
        }
        console.log(b.data);
        tmp_poli_tujuan_kunj2 = b.data.kode;
        poli_tujuan_finger = b.data.kode;
        polipilihanpasien = b.data.kode_unit_nci;
        Ext.getCmp("cboPoliklinikRequestEntry").setValue(b.data.nama);

        if (stat_rujukan === true) {
          // loaddatastoreDokterPJBPJSKunj2(b.data.kode);
          loaddatastoreDokterPJBPJS(b.data.spesialis);
          flagging_pasien_lama_beda_poli = true;
          console.log(b.data.kode);
          console.log(tmp_poli_tujuan);
          if (b.data.kode == tmp_poli_tujuan) {
            console.log("if");
            Ext.getCmp("lbl_dokter_kunj_2").hide();
            Ext.getCmp("separator_dokter_kunj_2").hide();
            Ext.getCmp("cboDokterPJBPJSKunj2").hide();
          } else {
            //tmp_poli_tujuan = b.data.kode;
            console.log("else");
            tmp_poli_tujuan_update = b.data.kode;
            // Ext.getCmp("lbl_dokter_kunj_2").show();
            // Ext.getCmp("separator_dokter_kunj_2").show();
            // Ext.getCmp("cboDokterPJBPJSKunj2").show();
            loaddatastoreDokterPJBPJS("RWJ", b.data.spesialis);
          }
          loaddatastoreDokterPJBPJS(b.data.kode_unit_nci, b.data.spesialis);
        } else {
          tmp_poli_tujuan = b.data.kode;
          loaddatastoreDokterPJBPJS(b.data.kode_unit_nci, b.data.spesialis);
          flagging_pasien_lama_beda_poli = true;
        }
      },
    },
  });
  return cboUnitBPJS;
}

function cek_kunjungan_kontrol(spesialis,unit) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_kunjungan_kontrol",
			params: {unit   :unit,
					 spesialis :spesialis,
					kd_pasien: Ext.getCmp('txt_no_medrec').getValue(),
					no_rujukan: Ext.getCmp('txt_no_rujukan').getValue()
	  },
			failure: function(o){
				var cst = Ext.decode(o.responseText);
			},	    
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				console.log(cst);
				console.log(kunjungan_pertama);
				if (cst.surat_kontrol == true || cst.surat_kontrol == 'true') {
					// Ext.getCmp('txt_no_surat_kontrol').disable();
					generate_surat_kontrol(false,spesialis);
				}else{
					Ext.getCmp('txt_no_surat_kontrol').enable();
					Ext.getCmp('txt_no_surat_kontrol').setValue('');
					kd_dpjp_kontrol='';
					kd_dokter_dpjp_insert = '';
					Ext.getCmp('cboDokterPJBPJS').setValue('');
					console.log(spesialis);
					console.log(poli_rujukan_asal);
					if(kunjungan_pertama == true || poli_rujukan_asal != spesialis){
						Ext.getCmp('lbl_no_surat_kontrol').hide();
						Ext.getCmp('lbl2_no_surat_kontrol').hide();
						Ext.getCmp('txt_no_surat_kontrol').hide();
					}else if(poli_rujukan_asal == spesialis){
						Ext.getCmp('lbl_no_surat_kontrol').show();
						Ext.getCmp('lbl2_no_surat_kontrol').show();
						Ext.getCmp('txt_no_surat_kontrol').show();
					}
				}
			}
		});
}

function mComboPropinsiLokasiLaka() {
	var Field = ['kode', 'nama'];
	dsPropinsiLokasiLaka = new WebApp.DataStore({ fields: Field });

	cboPropinsiLokasiLaka = new Ext.form.ComboBox
		(
			{
				x: 130,
				y: 310,
				readOnly: false,
				hidden: true,
				id: 'cboPropinsiLokasiLaka',
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				selectOnFocus: true,
				forceSelection: true,
				align: 'Right',
				emptyText: 'Pilih Propinsi...',
				store: dsPropinsiLokasiLaka,
				valueField: 'kode',
				displayField: 'nama',
				width: 200,
				tabIndex: 3,
				listeners:
				{
					'select': function (a, b, c) {
						selectProvinsiLokasiLaka = b.data.kode;
						loaddatastoreKabupatenLakalantas(b.data.kode);
						Ext.getCmp('cboKabupatenLokasiLaka').focus(false);
					},
				}
			}
		);
	return cboPropinsiLokasiLaka;
}

function mComboKabupatenLokasiLaka() {
	var Field = ['kode', 'nama'];
	dsKabupatenLokasiLaka = new WebApp.DataStore({ fields: Field });
	var cboKabupatenLokasiLaka = new Ext.form.ComboBox({
		id: 'cboKabupatenLokasiLaka',
		typeAhead: true,
		x: 130,
		y: 335,
		readOnly: false,
		hidden: true,
		triggerAction: 'all',
		selectOnFocus: true,
		lazyRender: true,
		mode: 'local',
		width: 200,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dsKabupatenLokasiLaka,
		valueField: 'kode',
		displayField: 'nama',
		tabIndex: 6,
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKabupatenLokasiLaka = b.data.kode;
				loaddatastorekecamatanLakalantas(b.data.kode);
			}
		}
	});
	return cboKabupatenLokasiLaka;
}

function mComboKecamatanLokasiLaka() {
	var Field = ['kode', 'nama'];
	dsKecamatanLokasiLaka = new WebApp.DataStore({ fields: Field });
	var cboKecamatanLokasiLaka = new Ext.form.ComboBox({
		id: 'cboKecamatanSLokasiLaka',
		x: 130,
		y: 360,
		width: 200,
		readOnly: false,
		hidden: true,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 6,
		store: dsKecamatanLokasiLaka,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				selectKecamatannLokasiLaka = b.data.kode;
				//loaddatastorekelurahan(b.data.KD_KECAMATAN);
				//Ext.getCmp('cbokelurahan').setValue();
				//Ext.getCmp('cbokelurahan').focus(false)
			}
		}
	});
	return cboKecamatanLokasiLaka;
}
function getParamSepBpjs() {
	// DISINI
	if (tmp_poli_tujuan_kunj2 == '') {
		tmp_poli_tujuan_kunj2=tmp_poli_tujuan;
	}
	if (Ext.getCmp('cboDokterPJBPJSKunj2').getValue() != '') {
		tmp_poli_tujuan = tmp_poli_tujuan_kunj2;
	}
	if (tmp_cari_rujukan === true) {
		if (Ext.getCmp('chk_rujukan_rs').getValue() === true) {
			asal_rujukan = 2;
		} else {
			asal_rujukan = 1;
		}
	} else {
		asal_rujukan = 2;
	}
	if(kd_dpjp_kontrol=='' || kd_dpjp_kontrol==undefined){
		kd_dpjp_kontrol=kd_dokter_dpjp_insert;
	}

	var tgl_sep_clean = Ext.getCmp('tgl_SEP').getValue();
	var params =
	{
		flaging			: 'RWJ',
		noKartu			: Ext.getCmp('txt_no_kartu_peserta_rwj').getValue(), //1
		klsRawat		: tmp_kelas_rawat, //2
		noMR			: Ext.getCmp('txt_no_medrec').getValue(), //3
		tglRujukan		: tmp_tgl_rujukan,//4
		noRujukan		: Ext.getCmp('txt_no_rujukan').getValue(), //5
		ppkRujukan		: tmp_ppk_rujukan, //6
		catatan			: Ext.getCmp('txt_no_catatan').getValue(),//7
		diagAwal		: tmp_diagnosa,
		tujuan			: tmp_poli_tujuan_kunj2,
		katarak			: tmp_katarak,
		cob				: tmp_cob,
		eksekutif		: tmp_eksekutif,
		asal_rujukan	: asal_rujukan,
		lakaLantas		: '0',
		penjamin_1		: '',
		penjamin_2		: '',
		penjamin_3		: '',
		penjamin_4		: '',
		tglKejadian		: '',
		keterangan		: '',
		suplesi			: '0',
		noSepSuplesi	: '',
		no_lp			: '',
		kdPropinsi		: selectPropinsiRequestEntry,
		kdKabupaten		: selectKabupatenRequestEntry,
		kdKecamatan		: selectKecamatanpasien,
		noSurat			: Ext.getCmp('txt_no_surat_kontrol').getValue(),
		kodeDPJP		: kd_dokter_dpjp_insert,
		noTelp			: Ext.getCmp('txt_no_tlp').getValue(),
		user			: 'Test WS',
		no_suplesi		: '0',
		tgl_sep			: tgl_sep_clean.format("Y-m-d"),
		tujuankunj		: selectTujuanKunjSEP,
		flagprocedure	: selectFlagProcedure,
		assesmentpel	: selectAssementPel,
		kdpenunjang		: selectKdPenunjang,
		kd_unit_tujuan  : polipilihanpasien,
		tgl_kunjungan   : Ext.getCmp('dtpTanggalKunjungan').getValue().format("Y-m-d"),
		kd_dpjp_kontrol	: kd_dpjp_kontrol
	}
	return params
};

function getParamSepBpjsUpdate() {
	if (Ext.getCmp('cboDokterPJBPJSKunj2').getValue() != '') {
		tmp_poli_tujuan = tmp_poli_tujuan_kunj2;
	}
	var params =
	{
		flaging: 'RWJ',
		noKartu: Ext.getCmp('txt_no_kartu_peserta_rwj').getValue(), //1
		klsRawat: '3', //tmp_kelas_rawat, //2
		noMR: Ext.getCmp('txt_no_medrec').getValue(), //3
		tglRujukan: tmp_tgl_rujukan,//4
		noRujukan: Ext.getCmp('txt_no_rujukan').getValue(), //5
		ppkRujukan: tmp_ppk_rujukan, //6
		catatan: Ext.getCmp('txt_no_catatan').getValue(),//7
		diagAwal: tmp_diagnosa_update,
		tujuan: tmp_poli_tujuan_update,
		katarak: tmp_katarak_update,
		cob: tmp_cob,
		eksekutif: tmp_eksekutif,
		no_sep: Ext.getCmp('txt_no_sep_rwj').getValue(),

		lakaLantas: '0',
		penjamin_1: '',
		penjamin_2: '',
		penjamin_3: '',
		penjamin_4: '',
		tglKejadian: '',
		keterangan: '',
		suplesi: '0',
		noSepSuplesi: '',
		kdPropinsi: '',
		kdKabupaten: '',
		kdKecamatan: '',
		noSurat: Ext.getCmp('txt_no_surat_kontrol').getValue(),
		kodeDPJP: tmp_dokter_dpjp,
		noTelp: Ext.getCmp('txt_no_tlp').getValue(),
		user: 'Test WS',
		no_suplesi: '0',
	}
	return params
};

function formLookup_poli_faskesTLSEP(rowdata) {
	var lebar = 530;
	formLookup_poli_faskes_tlsep = new Ext.Window({
		id: 'formLookup_poli_faskes_tlsepcreate',
		name: 'formLookup_poli_faskes_tlsepcreate',
		title: 'Daftar Poli',
		closeAction: 'destroy',
		width: lebar,
		height: 210, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_poli_faskes_tl_sep(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookup_poli_faskes_tlsep.show();
}

function panel_form_lookup_poli_faskes_tl_sep() {
	radios4 = new Ext.form.RadioGroup({
		id: 'radios4',
		column: 3,
		items: [
		],
		listeners: {
			change: function () {

			}
		}
	});
	var Field = ['KODE', 'NAMA'];
	dataSourcePoliFaskesTL = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListPoliFaskesTLSEPRWJ = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourcePoliFaskesTL,
		columnLines: false,
		autoScroll: false,
		width: 520,
		height: 200,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectPoliFaskesTL = dataSourcePoliFaskesTL.getAt(row);
					console.log(rowselectPoliFaskesTL);
					if (rowselectPoliFaskesTL.data.kode != '') {

						kode_faskesTL = rowselectPoliFaskesTL.data.kode;
						Ext.getCmp('txt_poli').setValue(rowselectPoliFaskesTL.data.nama);
						if (rowselectPoliFaskesTL != tmp_poli_tujuan) {
							loaddatastoreDokterPJBPJSKunj2(Ext.getCmp('txt_poli').getValue());
							console.log('if');
							Ext.getCmp('lbl_dokter_kunj_2').show();
							Ext.getCmp('separator_dokter_kunj_2').show();
							Ext.getCmp('cboDokterPJBPJSKunj2').show();
						} else {
							console.log('else');
						}
						//	loaddatastoreDokterPJBPJS(kode_faskesTL);
						formLookup_poli_faskes_tlsep.close();
					} else {
						ShowPesanWarning_viDaftar('Pilih Poli Fasilitas Kesehatan', 'Daftar Fasilitas Kesehatan');
					}

				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colNoKunjunganMultiRujukan',
				header: 'KODE',
				dataIndex: 'kode',
				hideable: false,
				menuDisabled: true,
				width: 10
			},
			{
				id: 'colNoKartuMultiRujukan',
				header: 'NAMA FASKES',
				dataIndex: 'nama',
				hideable: false,
				menuDisabled: true,
				width: 50
			}

		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListPoliFaskesTLSEPRWJ
				]
			}
		]
	};
	return items;
}
function get_dokter_pertama(No_Rujukan, Kode_Pasien) {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_dokter_pertama",
		params: {
			no_rujukan: No_Rujukan,
			kd_pasien: Kode_Pasien
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst);
			if (cst.success === true && cst.totalrecords > 0) {
				tmp_kd_dokter_dpjp = cst.listData[0].kd_dokter_dpjp;
				Ext.getCmp('cboDokterPJBPJS').setValue(cst.listData[0].nama).disable();
				Ext.getCmp('txt_kd_dokter_dpjp').setValue(cst.listData[0].kd_dokter_dpjp).disable();
			}
		}
	});

}

function mCombDokterBPJS_kunj2() {
	var Field = ['kode', 'nama'];
	dsDokterPJBPJSKunj2 = new WebApp.DataStore({ fields: Field });
	var cboDokterPJBPJS_kunj2 = new Ext.form.ComboBox({
		id: 'cboDokterPJBPJSKunj2',
		x: 420,
		y: 100,
		width: 200,
		hidden: true,
		readOnly: false,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Dokter...',
		fieldLabel: 'Dokter',
		align: 'Right',
		tabIndex: 6,
		store: dsDokterPJBPJSKunj2,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents: true,
		listeners: {
			'select': function (a, b, c) {
				select_kd_dokter_bpjs = b.data.kode;
				kd_dokter_dpjp_insert = b.data.kode;
				console.log(kd_dokter_dpjp_insert);
				Ext.Ajax.request({
					url: baseURL + "index.php/rawat_jalan/functionRWJ/get_dokter_map_bpjs",
					params: { kd_dokter_dpjp: b.data.kode },
					failure: function (o) {
						var cst = Ext.decode(o.responseText);
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) {
							console.log(cst);
							Ext.getCmp('cboDokterRequestEntry').setValue(cst.listData[0].nama);
							kodeDokterDefault = cst.listData[0].kd_dokter;
							kode_dpjp_kunj2 = cst.listData[0].kd_dokter_dpjp;
						}
					}
				});
			}
		}
	});
	return cboDokterPJBPJS_kunj2;
}

function loaddatastoreUnitBPJS() {
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_poli_rujukan",
		params: {
			unit: polipilihanpasien,
			//				 tgl_rujukan :Ext.getCmp('tgl_rujukan').getValue()
		},
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			dsUnitBPJS.removeAll();
			if (cst.success === true) {
				var recs = [],
					recType = dsUnitBPJS.recordType;
				for (var i = 0; i < cst.listData.length; i++) {
					recs.push(new recType(cst.listData[i]));
				}
				dsUnitBPJS.add(recs);
			}
		}
	});
	/*Ext.Ajax.request({
		method:'POST',
		url: baseURL + "index.php/rawat_jalan/functionRWJ/get_poli_rujukan",
		params: {
			nama_poli_rujuk: Ext.getCmp('txt_poli').getValue() 
			},
		success:function(o){
		loadMask.hide();
		var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				var recs=[],
				recType=dsUnitBPJS.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));
				}
			  dsUnitBPJS.add(recs);
			}
	});*/
}


function panel_form_lookup_update_sep_rwj() {
	var formPanelUpdateSEP = new Ext.form.FormPanel({
		id: "myformpanel_rujukan_bpjs",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .10,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 300,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'Nama Peserta'
			},
			{
				x: 120,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 5,
				xtype: 'textfield',
				id: 'txt_nama_peserta_rwj',
				name: 'txt_nama_peserta_rwj',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 340,
				y: 5,
				xtype: 'label',
				text: 'No Kartu'
			},
			{
				x: 410,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_kartu_peserta_rwj',
				name: 'txt_no_kartu_peserta_rwj',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'Hak Kelas'
			},
			{
				x: 120,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 30,
				xtype: 'textfield',
				id: 'txt_hak_kelas_peserta_rwj',
				name: 'txt_hak_kelas_peserta_rwj',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 340,
				y: 30,
				xtype: 'label',
				text: 'Perujuk'
			},
			{
				x: 410,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 30,
				xtype: 'textfield',
				id: 'txt_perujuk',
				name: 'txt_perujuk',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'Spesialis/SubSpesialis'
			},
			{
				x: 120,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			McomboUnitBPJS(),

			{

				xtype: 'textfield',
				id: 'txt_kd_spesialis',
				name: 'txt_kd_spesialis',
				width: 150,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1,
				hidden: true
			},


			{
				x: 340,
				y: 55,
				xtype: 'label',
				text: 'Eksekutif'
			},
			{
				x: 410,
				y: 55,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 55,
				xtype: 'checkbox',
				boxLabel: 'Eksekutif ',
				name: 'cbo_eksekutif',
				id: 'cbo_eksekutif',
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							tmp_eksekutif = 1;
						} else {
							tmp_eksekutif = 0;
						}
					}
				}
			},

			{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Tgl.Rujukan'
			},
			{
				x: 120,
				y: 80,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 130,
				y: 80,
				id: 'tgl_rujukan',
				name: 'tgl_rujukan',
				format: 'd/M/Y',
				readOnly: true,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				width: 200,
				tabIndex: 1

			},

			{
				x: 340,
				y: 75,
				xtype: 'label',
				text: 'No. Rujukan'
			},
			{
				x: 410,
				y: 75,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 75,
				xtype: 'textfield',
				id: 'txt_no_rujukan',
				name: 'txt_no_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'No.Surat Kontrol/SKDP'
			},
			{
				x: 120,
				y: 105,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 105,
				xtype: 'textfield',
				id: 'txt_no_surat_kontrol',
				name: 'txt_no_surat_kontrol',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
				value: tmp_nomor_skdp,
				autoCreate: { tag: 'input', type: 'text', size: '6', autocomplete: 'off', maxlength: '6' }
			},

			{
				x: 340,
				y: 100,
				xtype: 'label',
				text: 'Dokter'
			},
			{
				x: 410,
				y: 100,
				xtype: 'label',
				text: ':'
			},
			{
				x: 380,
				y: 125,
				xtype: 'textfield',
				id: 'txt_kd_dokter_dpjp',
				name: 'txt_kd_dokter_dpjp',
				width: 150,
				disabled: false,
				hidden: true,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},
			mCombDokterBPJS(),

			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Tgl.SEP'
			},
			{
				x: 120,
				y: 130,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 130,
				y: 130,
				id: 'tgl_SEP',
				name: 'tgl_SEP',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,

			},

			{
				x: 340,
				y: 150,
				xtype: 'label',
				text: 'Katarak'
			},
			{
				x: 410,
				y: 150,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 150,
				xtype: 'checkbox',
				boxLabel: '',
				name: 'cbo_katarak',
				id: 'cbo_katarak',
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							tmp_katarak = 1;
						}
					}
				}
			},

			{
				x: 10,
				y: 155,
				xtype: 'label',
				text: 'No Medrec'
			},
			{
				x: 120,
				y: 155,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 155,
				xtype: 'textfield',
				id: 'txt_no_medrec',
				name: 'txt_no_medrec',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 340,
				y: 165,
				xtype: 'label',
				text: 'Peserta COB'
			},
			{
				x: 410,
				y: 165,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 165,
				xtype: 'checkbox',
				boxLabel: '',
				name: 'cbo_peserta_cob',
				id: 'cbo_peserta_cob',
				disabled: false,
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							tmp_cob = 1;
						} else {
							tmp_cob = 0;
						}
					}
				}
			},

			{
				x: 340,
				y: 125,
				xtype: 'label',
				text: 'Dokter',
				id: 'lbl_dokter_kunj_2',
				hidden: true
			},
			{
				x: 410,
				y: 125,
				xtype: 'label',
				text: ':',
				id: 'separator_dokter_kunj_2',
				hidden: true
			},

			mCombDokterBPJS_kunj2(),

			{
				x: 10,
				y: 180,
				xtype: 'label',
				text: 'Diagnosa'
			},
			{
				x: 120,
				y: 180,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 180,
				xtype: 'textfield',
				id: 'txt_kd_diagnosa',
				name: 'txt_kd_diagnosa',
				width: 50,
				disabled: true,
				hidden: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 180,
				y: 180,
				xtype: 'textfield',
				id: 'txt_nama_diagnosa',
				name: 'txt_nama_diagnosa',
				width: 300,
				disabled: true,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 340,
				y: 205,
				xtype: 'label',
				text: 'No Tlp'
			},
			{
				x: 410,
				y: 205,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 205,
				xtype: 'textfield',
				id: 'txt_no_tlp',
				name: 'txt_no_tlp',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 205,
				xtype: 'label',
				text: 'Catatan'
			},
			{
				x: 120,
				y: 205,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 205,
				xtype: 'textfield',
				id: 'txt_no_catatan',
				name: 'txt_no_catatan',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},




		],
		buttons:
			[
				{
					text: "Generate SEP",
					handler: function () {
						ajax_cetak_sep();
						//formLookups_rujukan_bpjs.close();	
					}
				}
			]
	});
	return formPanel;
}

function datainit_viUpdateSep(rowdata) {
	console.log(rowdata);
	loaddatastoreDokterPJBPJS(tmp_poli_tujuan);
	loaddatastoreUnitBPJS();
	Ext.getCmp('txt_nama_peserta_rwj').setValue(rowdata.response.peserta.nama);
	Ext.getCmp('txt_no_kartu_peserta_rwj').setValue(rowdata.response.peserta.noKartu);
	Ext.getCmp('txt_hak_kelas_peserta_rwj').setValue(rowdata.response.peserta.hakKelas);
	Ext.getCmp('txt_perujuk').setValue(rowdata.response.peserta.nama);
	Ext.getCmp('txt_poli').setValue(rowdata.response.poli);
	Ext.getCmp('tgl_rujukan').setValue(rowdata.response.tglSep);
	//Ext.getCmp('cboDokterPJBPJS').setValue(rowdata.response.peserta.nama);
	Ext.getCmp('txt_no_medrec').setValue(rowdata.response.peserta.noMr);
	//Ext.getCmp('txt_kd_diagnosa').setValue(rowdata.response.peserta.nama);
	Ext.getCmp('txt_nama_diagnosa').setValue(rowdata.response.diagnosa).enable();
	Ext.getCmp('txt_no_catatan').setValue(rowdata.response.catatan);
	Ext.getCmp('txt_no_rujukan').setValue(rowdata.response.noRujukan);
	Ext.getCmp('txt_no_sep_rwj').setValue(rowdata.response.noSep).show();
	Ext.getCmp('lbl_no_sep').show();
	Ext.getCmp('separator_lbl_no_sep').show();
	Ext.getCmp('update_sep').show();
	Ext.getCmp('generate_sep').hide();
}

function ajax_update_sep() {
	if (Ext.getCmp('txt_no_surat_kontrol').getValue() == '' || Ext.getCmp('txt_no_surat_kontrol').getValue() == '') {
		ShowPesanWarning_viDaftar('Nomor Surat Harus Diisi min 6 digit!', 'Generate SEP RWJ');
	} else {
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			crossDomain: true,
			url: baseURL + "index.php/rawat_jalan/functionRWJ/UpdateSEP/",
			data: getParamSepBpjsUpdate(),
			success: function (resp1) {
				loadMask.hide();
				formLookups_rujukan_bpjs.close();
				if (resp1.metaData.code == '200') {
					ShowPesanInfo_viDaftar('SEP Berhasil Di Update', 'Update SEP');
					//	console.log(resp1.response.sep.peserta.noKartu);
					/*	Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
						Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
						Ext.getCmp('txtDiagnosa_RWJ').setValue(resp1.response.sep.diagnosa);
						Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
						Ext.getCmp('txtNoSJP').focus();
						datasave_viDaftar(addNew_viDaftar,false);*/
				} else {
					Ext.MessageBox.alert('Gagal', resp1.metaData.message);
				}
			},
			error: function (jqXHR, exception) {
				loadMask.hide();
				Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
				Ext.Ajax.request({
					url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
					params: {
						respon: jqXHR.responseText,
						keterangan: 'Error create SEP'
					}
				});
			}
		});
	}

}

function form_daftar_diagnosa(rowdata) {
	var lebar = 550;
	form_list_rujukan_bpjs = new Ext.Window({
		id: 'formLookup_diagnosa_bpjs_rwi',
		name: 'formLookup_diagnosa_bpjs_rwi',
		title: 'Form List Diagnosa',
		closeAction: 'destroy',
		width: lebar,
		height: 200, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_daftar_diagnosa(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	form_list_rujukan_bpjs.show();
}
function panel_form_lookup_daftar_diagnosa() {
	var Field = ['kode', 'nama'];
	dataSourceListDiagosa = new WebApp.DataStore({ fields: Field });
	var gridListDiagnosa = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceListDiagosa,
		columnLines: false,
		autoScroll: false,
		width: 720,
		height: 250,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					//update gan(rujukan)
					rowselectDiagnosa = dataSourceListDiagosa.getAt(row);
					if (rowselectDiagnosa.data.nama != '') {
						form_list_rujukan_bpjs.close();
						tmp_diagnosa_update = rowselectDiagnosa.data.kode;
						tmp_diagnosa = rowselectDiagnosa.data.kode;
						Ext.getCmp('txt_nama_diagnosa').setValue(rowselectDiagnosa.data.nama);
						Ext.getCmp('txt_kd_diagnosa').setValue(rowselectDiagnosa.data.kode);
						console.log(rowselectDiagnosa.data.kode);
					}

				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colKodeDiagnosaBPJS',
				header: 'KODE',
				dataIndex: 'kode',
				sortable: true,
				hideable: false,
				menuDisabled: true,
				width: 10
			},
			{
				id: 'colNamaDiagnosaBPJS',
				header: 'NAMA',
				dataIndex: 'nama',
				menuDisabled: false,
				sortable: true,
				hideable: false,
				menuDisabled: true,
				width: 40
			}
		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items_diagnosa = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListDiagnosa
				]
			}
		]
	};
	return items_diagnosa;
}

// ----------------------------Update Pengajuan dan Approval SEP--------------------------------------
// Egi 08 Febuari 2022
function LookUpAprovalSEP(rowdata) {
	var lebar = 370;
	formLookups_approval_sep_bpjs = new Ext.Window({
		id: 'formLookup_Approval_SEP_RWJ',
		name: 'formLookup_Approval_SEP_RWJ',
		title: 'Form Pengajuan Approval SEP',
		closeAction: 'destroy',
		width: lebar,
		resizable: true,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_approval_sep_bpjs_RWJ(), //1
		listeners: {
			afterShow: function () {
				this.activate();
			}
		}
	});
	formLookups_approval_sep_bpjs.show();
	datainit_viApprovalSEPRWJ();
}

function LookUpAprovalFinger(rowdata) {
	var lebar = 370;
	formLookups_approval_finger_bpjs = new Ext.Window({
		id: 'formLookup_Approval_Finger_RWJ',
		name: 'formLookup_Approval_Finger_RWJ',
		title: 'Form Aprroval Pengajuan FingerPrint',
		closeAction: 'destroy',
		width: lebar,
		resizable: true,
		constrain: true,
		autoScroll: false,
		height: 250,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_approval_finger_bpjs_RWJ(), //1
		listeners:{
			afterShow: function () {
				this.activate();
			}
		}
  	});
	formLookups_approval_finger_bpjs.show();
	datainit_viApprovalFingerRWJ();
}
  
function panel_form_lookup_approval_sep_bpjs_RWJ() {
	var formPanel = new Ext.form.FormPanel({
		id: "myformpanel_approval_sep_bpjs",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults: {},
		columnWidth: .10,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 240,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'Tanggal'
			},{
				x: 120,
				y: 5,
				xtype: 'label',
				text: ':'
			},{
				xtype: 'datefield',
				x: 130,
				y: 5,
				id: 'tgl_approval_SEP',
				name: 'tgl_approval_SEP',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,

			},{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'No Kartu'
			},{
				x: 120,
				y: 30,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 30,
				xtype: 'textfield',
				id: 'txt_noka_rwj_approval_sep',
				name: 'txt_noka_rwj_approval_sep',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'Nama Peserta'
			},{
				x: 120,
				y: 55,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 55,
				xtype: 'textfield',
				id: 'txt_nama_peserta_rwj_approval_sep',
				name: 'txt_nama_peserta_rwj_approval_sep',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Jenis Pelayanan'
			},{
				x: 120,
				y: 80,
				xtype: 'label',
				text: ':'
			},
			MComboLayananApprovalSEP(),
			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Jenis Pengajuan'
			},{
				x: 120,
				y: 105,
				xtype: "label",
				text: ":",
			},{  	
        		xtype:'radio',
				x: 130,
				y: 110,
				name: 'rb_pengajuan',
				id:'rb_pengajuan_backdate',
				boxLabel: 'Backdate',
				inputValue: "backdate",
				listeners:{
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							backdate=1;
							selectPengajuanApprovalSEP = 1;
						} else {
							backdate=0;
						}
					}
				}
			},{
				xtype: 'radio',
				x: 200,
				y: 110,
				boxLabel: 'Finger Print',
				id: 'rb_pengajuan_finger',
				name: 'rb_pengajuan',
				inputValue: "finger",
				listeners: {
				check: function (checkbox, isChecked) {
						if (isChecked === true) {
							finger=1;
							selectPengajuanApprovalSEP = 2;
						} else {
							finger=0;
						}
					}
				}
	    	},{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'keterangan'
			},{
				x: 120,
				y: 130,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 130,
				xtype: 'textarea',
				id: 'txt_catatan_approval_sep',
				name: 'txt_catatan_approval_sep',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},
		],
		buttons: [
			{
				text: "Pengajuan SEP",
				id: 'approval_sep_act',
				handler: function () {
					ajax_approval_sep();
					//formLookups_rujukan_bpjs.close();	
				}
			},
		]
	});
	return formPanel;
}

function panel_form_lookup_approval_finger_bpjs_RWJ() {
	var formPanel = new Ext.form.FormPanel({
		id: "myformpanel_approval_finger_bpjs",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{

		},
		columnWidth: .10,
		layout: 'absolute',
		border: true,
		// width: 500,
		height: 200,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'Tanggal'
			},{
				x: 120,
				y: 5,
				xtype: 'label',
				text: ':'
			},{
				xtype: 'datefield',
				x: 130,
				y: 5,
				id: 'tgl_approval_finger',
				name: 'tgl_approval_finger',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,

			},{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'No Kartu'
			},{
				x: 120,
				y: 30,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 30,
				xtype: 'textfield',
				id: 'txt_noka_rwj_approval_finger',
				name: 'txt_noka_rwj_approval_finger',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'Nama Peserta'
			},{
				x: 120,
				y: 55,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 55,
				xtype: 'textfield',
				id: 'txt_nama_peserta_rwj_approval_finger',
				name: 'txt_nama_peserta_rwj_approval_finger',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Jenis Pelayanan',
				hidden: true
			},{
				x: 120,
				y: 80,
				xtype: 'label',
				text: ':',
				hidden: true
			},
			// MComboLayananApprovalSEP(),
			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Jenis Pengajuan',
				hidden: true

			},{
				x: 120,
				y: 105,
				xtype: 'label',
				hidden: true,
				text: ':',
			},{
				xtype: 'radio',
				x: 130,
				y: 110,
				name: 'rb_pengajuan',
				id: 'rb_pengajuan_backdate',
				boxLabel: 'Backdate',
				hidden: true,
				inputValue: "backdate",
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							backdate = 1;
							selectPengajuanApprovalSEP = 1;
						} else {
							backdate = 0;
						}
					}
				}
			},{
				xtype: 'radio',
				x: 200,
				y: 110,
				boxLabel: 'Finger Print',
				id: 'rb_pengajuan_finger',
				name: 'rb_pengajuan',
				hidden: true,
				inputValue: "finger",
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							finger = 1;
							selectPengajuanApprovalSEP = 2;
						} else {
							finger = 0;
						}
					}
				}
			},{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'keterangan'
			},{
				x: 120,
				y: 80,
				xtype: 'label',
				text: ':'
			},{
				x: 130,
				y: 80,
				xtype: 'textarea',
				id: 'txt_catatan_approval_finger',
				name: 'txt_catatan_approval_finger',
				width: 200,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},
		],
		buttons:
			[
				{
					text: "Approve",
					id: 'approval_finger_act',
					handler: function () {
						ajax_approval_finger();
					}
				},

			]
	});
	return formPanel;
}

function MComboLayananApprovalSEP() {
	var cboPelayananApproval = new Ext.form.ComboBox
		(
			{
				id: 'cboPelayananApprovalSep',
				x: 130,
				y: 80,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender: true,
				mode: 'local',
				tabIndex: 3,
				selectOnFocus: true,
				forceSelection: true,
				emptyText: 'Jenis Pelayanan',
				fieldLabel: 'Jenis Pelayanan ',
				editable: false,
				width: 200,
				store: new Ext.data.ArrayStore
					(
						{
							id: 1,
							fields:
								[
									'Id',
									'displayText'
								],
							data: [[1, 'Rawat Inap'], [2, 'Rawat Jalan']]
						}
					),
				valueField: 'Id',
				displayField: 'displayText',
				enableKeyEvents: true,
				listeners: {
					'select': function (a, b, c) {
						console.log(b);
						selectPelayananApprovalSEP = b.data.Id;
						console.log(selectPelayananApprovalSEP);
					},
				}
			}
		);
	return cboPelayananApproval;
};

function datainit_viApprovalSEPRWJ() {
	console.log(tmp_data_approval_sep);
	Ext.getCmp('txt_noka_rwj_approval_sep').setValue(tmp_data_approval_sep.peserta.noKartu);
	Ext.getCmp('txt_nama_peserta_rwj_approval_sep').setValue(tmp_data_approval_sep.peserta.nama);
}

function datainit_viApprovalFingerRWJ() {
	console.log(tmp_data_approval_sep);
	Ext.getCmp('txt_noka_rwj_approval_finger').setValue(tmp_data_approval_sep.peserta.noKartu);
	Ext.getCmp('txt_nama_peserta_rwj_approval_finger').setValue(tmp_data_approval_sep.peserta.nama);
}

function ajax_approval_sep() {
	console.log(selectPengajuanApprovalSEP);
	if (Ext.getCmp('cboPelayananApprovalSep').getValue() == '' || Ext.getCmp('cboPelayananApprovalSep').getValue() == '') {
		ShowPesanWarning_viDaftar('Jenis Pelayanan Belum Dipilih', 'Pengajuan SEP RWJ');
	} else if (selectPengajuanApprovalSEP == '') {
		ShowPesanWarning_viDaftar('Jenis Pengajuan Belum Dipilih', 'Pengajuan SEP RWJ');
	} else {
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType: 'JSON',
			crossDomain: true,
			url: baseURL + "index.php/rawat_jalan/functionRWJ/PengajuanApprovalSEP/",
			data: getParamSepApprovalBpjs(),
			success: function (resp1) {
				loadMask.hide();
				formLookups_approval_sep_bpjs.close();
				if (resp1.metaData.code == '200') {
					console.log(resp1);
					ShowPesanInfo_viDaftar(resp1.response, 'Approval SEP');
				} else {
					Ext.MessageBox.alert('Gagal', resp1.metaData.message);
				}
			},
			error: function (jqXHR, exception) {
				loadMask.hide();
				Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
				Ext.Ajax.request({
					url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
					params: {
						respon: jqXHR.responseText,
						keterangan: 'Error Pengajuan Approval SEP'
					}
				});
			}
		});
	}
}

function ajax_approval_finger() {
	console.log(selectPengajuanApprovalSEP);
	if(Ext.getCmp('txt_catatan_approval_finger').getValue()=='') {
	  	ShowPesanWarning_viDaftar('Keterangan belum diisi', 'Approval Pengajuan FingerPrint RWJ');
	} else {		
	  	loadMask.show();
	  	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/PengajuanApprovalFinger/",
		data: getParamFingerApprovalBpjs(), 
		success: function (resp1) {
		  	loadMask.hide();
		  	formLookups_approval_finger_bpjs.close();
		  	if (resp1.metaData.code == '200') {
				console.log(resp1);
				ShowPesanInfo_viDaftar("Approval No. Kartu "+resp1.response+" berhasil !",'Approval FingerPrint');
		  	} else {
				Ext.MessageBox.alert('Gagal', resp1.response );
		  	}
		}, error: function (jqXHR, exception) {
		  	loadMask.hide();
		  	Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
		  	Ext.Ajax.request({
			url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error Approval FingerPrint'
				}
		  	});
		}
	  	}); 
	}
}	
  
function getParamSepApprovalBpjs() {
	console.log(selectPengajuanApprovalSEP); 
	var tgl_sep_pengajuan_approve_clean = Ext.getCmp('tgl_approval_SEP').getValue();
	var params =
	{
		noKartu			: Ext.getCmp('txt_noka_rwj_approval_sep').getValue(),
		tglSep			: tgl_sep_pengajuan_approve_clean.format("Y-m-d"),
		jnsPelayanan	: selectPelayananApprovalSEP,
		jenisPengajuan  : selectPengajuanApprovalSEP,
		keterangan		: Ext.getCmp('txt_catatan_approval_sep').getValue(),
	};
	return params;
};

function getParamFingerApprovalBpjs() {
	console.log(selectPengajuanApprovalSEP);
	var tgl_finger_pengajuan_approve_clean=Ext.getCmp('tgl_approval_finger').getValue();
	var	params =
	{
		noKartu 		    : Ext.getCmp('txt_noka_rwj_approval_finger').getValue(),
		tglSep 		 	    : tgl_finger_pengajuan_approve_clean.format("Y-m-d"),
		jnsPelayanan 	  	: 2,
		jenisPengajuan 		: 2,
		keterangan 		  	: Ext.getCmp('txt_catatan_approval_finger').getValue(),
	};
  return params;
}

function formLookup_insert_rujukan(rowdata) {
	console.log(rowdata);
	var lebar = 690;
	formLookups_insert_rujukan = new Ext.Window({
		id: 'formLookup_insert_rujukan_bpjs',
		name: 'formLookup_insert_rujukan_bpjs',
		title: 'Pembuatan Rujukan',
		closeAction: 'destroy',
		width: lebar,
		height: 350, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_insert_rujukan_bpjs(rowdata), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookups_insert_rujukan.show();
}
function panel_form_lookup_insert_rujukan_bpjs(rowdata) {
	console.log(rowdata);
	var formPanelInsertRujukan = new Ext.form.FormPanel({
		id: "myformpanel_insert_rujukan_bpjs",
		bodyStyle: "padding:5px",
		labelAlign: "top",
		defaults:
		{
			//anchor: "100%"
		},
		columnWidth: .10,
		layout: 'absolute',
		border: false,
		// width: 500,
		height: 300,
		//anchor: '100% 100%',
		items: [
			{
				x: 10,
				y: 5,
				xtype: 'label',
				text: 'No SEP'
			},
			{
				x: 100,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_sep_insert_rujukan',
				name: 'txt_no_sep_insert_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			/*{
				x: 242,
				y: 5,
				xtype: 'textfield',
				id: 'txt_unit_insert_rujukan',
				name: 'txt_unit_insert_rujukan',
				width: 70,
				disabled:true,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},*/

			{
				x: 320,
				y: 5,
				xtype: 'label',
				text: 'No Rujukan'
			},
			{
				x: 410,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{
				x: 420,
				y: 5,
				xtype: 'textfield',
				id: 'txt_no_rujukan_insert_rujukan',
				name: 'txt_no_rujukan_insert_rujukan',
				width: 220,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 30,
				xtype: 'label',
				text: 'Tgl SEP'
			},
			{
				x: 100,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				x: 110,
				y: 30,
				xtype: 'datefield',
				id: 'tgl_sep_insert_rujukan',
				name: 'tgl_sep_insert_rujukan',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,
			},
			{
				x: 320,
				y: 30,
				xtype: 'label',
				text: 'TGL Rujukan'
			},
			{
				x: 410,
				y: 30,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 420,
				y: 30,
				id: 'tgl_rujukan_insert_rujukan',
				name: 'tgl_rujukan_insert_rujukan',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 220,

			},

			{
				x: 10,
				y: 55,
				xtype: 'label',
				text: 'No Kartu'
			},
			{
				x: 100,
				y: 55,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 55,
				xtype: 'textfield',
				id: 'txt_no_kartu_insert_rujukan',
				name: 'txt_no_kartu_insert_rujukan',
				width: 170,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 280,
				y: 55,
				xtype: 'textfield',
				id: 'txt_sex_insert_rujukan',
				name: 'txt_sex_insert_rujukan',
				width: 30,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 55,
				xtype: 'label',
				text: 'Pelayanan'
			},
			{
				x: 410,
				y: 55,
				xtype: 'label',
				text: ':'
			},
			/*{
				x: 420,
				y: 55,
				xtype: 'textfield',
				id: 'combo',
				name: 'combo',
				width: 220,
				disabled:true,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},*/
			mCombPelayananInsertRujukan(),
			/*{
				xtype: '',
				fieldLabel: '',
				x: 420,
				y: 55,
				id: 'combo_pelayanan_insert_rujukan',
				editable: true,
				store: new Ext.data.ArrayStore({
					id: 0,
					fields:[
						'Id',
						'displayText'
					],
					data: [[0, 'Rawat Inap'], [1, 'Rawat Jalan']]
				}),
				valueField: 'Id',
				displayField: 'displayText',
				mode: 'local',
				width: 100,
				triggerAction: 'all',
				emptyText: 'Pilih Salah Satu...',
				selectOnFocus: true,
				tabIndex: 30,
				anchor: '99%',
				disabled : true,
				enableKeyEvents:true,
				listeners:{
					'select': function (a, b, c){
								
					},
							
				}
			},*/

			{
				x: 10,
				y: 80,
				xtype: 'label',
				text: 'Nama'
			},
			{
				x: 100,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 80,
				xtype: 'textfield',
				id: 'txt_nama_peserta_insert_rujukan',
				name: 'txt_nama_peserta_insert_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 80,
				xtype: 'label',
				text: 'Tipe'
			},
			{
				x: 410,
				y: 80,
				xtype: 'label',
				text: ':'
			},

			{
				xtype: 'radio',
				x: 420,
				y: 80,
				name: 'rb_tipe',
				id: 'rb_tipe_penuh',
				boxLabel: 'Penuh',
				inputValue: "penuh",
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							tipe_penuh = 1;
							rujukan_tipe = '';
							rujukan_tipe = 0;
							console.log(rujukan_tipe);
						} else {
							tipe_penuh = 0;
							//rujukan_tipe=0;
						}
					}
				}
			}, {
				xtype: 'radio',
				x: 470,
				y: 80,
				boxLabel: 'Partial',
				id: 'rb_tipe_partial',
				name: 'rb_tipe',
				inputValue: "partial",
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							partial = 1;
							rujukan_tipe = '';
							rujukan_tipe = 1;
							console.log(rujukan_tipe);
						} else {
							partial = 0;
							//rujukan_tipe=1;
						}
					}
				}
			}, {
				xtype: 'radio',
				x: 520,
				y: 80,
				boxLabel: 'Rujukan Balik (Non PBR)',
				id: 'rb_rujukan_balik',
				name: 'rb_tipe',
				inputValue: "noktp",
				listeners: {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							rujuk_balik = 1;
							rujukan_tipe = '';
							rujukan_tipe = 2;
							console.log(rujukan_tipe);
							Ext.getCmp('txt_dirujuk_ke_insert_rujukan').setValue(nama_rujukan_pbr);
							Ext.getCmp('txt_dirujuk_ke_insert_rujukan').disable();
							kode_faskes_insert_rujukanTL= kd_rujukan_pbr;
							//	console.log(rujuk_balik);	
						} else {
							rujuk_balik = 0;
							Ext.getCmp('txt_dirujuk_ke_insert_rujukan').setValue("");
							Ext.getCmp('txt_dirujuk_ke_insert_rujukan').enable();
							kode_faskes_insert_rujukanTL= ''
							//	rujukan_tipe=2;
							//	console.log(rujuk_balik);
						}
					}
				}
			},


			{
				x: 10,
				y: 105,
				xtype: 'label',
				text: 'Tanggal Lahir'
			},
			{
				x: 100,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 105,
				xtype: 'textfield',
				id: 'txt_tgl_lahir_peserta_insert_rujukan',
				name: 'txt_tgl_lahir_peserta_insert_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 105,
				xtype: 'label',
				text: 'Di Rujuk Ke'
			},
			{
				x: 410,
				y: 105,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 105,
				xtype: 'textfield',
				id: 'txt_dirujuk_ke_insert_rujukan',
				name: 'txt_dirujuk_ke_insert_rujukan',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
				listeners: {
					specialkey: function (text, e) {
						if (e.getKey() == 13) {

							loadMask.show();
							if (Ext.getCmp('txt_dirujuk_ke_insert_rujukan').getValue() != '') {
								Ext.Ajax.request({
									method: 'POST',
									url: baseURL + "index.php/rawat_jalan/functionRWJ/get_daftar_rujukan",
									params: {
										tipe_fasker: tipe_faskes,
										nama_faskes: Ext.getCmp('txt_dirujuk_ke_insert_rujukan').getValue()
									},
									success: function (o) {
										loadMask.hide();
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											formLookup_faskes_tl();
											var recs = [],
												recType = dataSourceFaskesTLInsertRujukan.recordType;

											for (var i = 0; i < cst.listData.length; i++) {
												recs.push(new recType(cst.listData[i]));
											}
											dataSourceFaskesTLInsertRujukan.add(recs);
											gridListFaskesTL.getView().refresh();
										}
										else {
											ShowPesanError_viDaftar('Gagal membaca data di rujuk', 'Error');
										}

									}, error: function (jqXHR, exception) {
										Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
									}
								});

							}
							else {
								ShowPesanWarning_viDaftar('Nama Rujukan Belum Diisi', 'No Rujukan');
							}

						}

					}
				}
			},


			{
				x: 10,
				y: 130,
				xtype: 'label',
				text: 'Hak Kelas'
			},
			{
				x: 100,
				y: 130,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 130,
				xtype: 'textfield',
				id: 'txt_hak_kelas_insert_rujukan',
				name: 'txt_hak_kelas_insert_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 320,
				y: 130,
				xtype: 'label',
				text: 'Spesialis'
			},
			{
				x: 410,
				y: 130,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 130,
				xtype: 'textfield',
				id: 'txt_spesialis_insert_rujukan',
				name: 'txt_spesialis_insert_rujukan',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1,
				listeners: {
					specialkey: function (text, e) {
						if (e.getKey() == 13) {
							formLookup_poli_faskesTL();
							loadMask.show();
							if (Ext.getCmp('txt_spesialis_insert_rujukan').getValue() != '') {
								Ext.Ajax.request({
									method: 'POST',
									url: baseURL + "index.php/rawat_jalan/functionRWJ/get_poli_rujukan_insert_rujukan",
									params: {
										nama_poli_rujuk: Ext.getCmp('txt_spesialis_insert_rujukan').getValue()
									},
									success: function (o) {
										loadMask.hide();
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											var recs = [],
												recType = dataSourcePoliFaskesTL.recordType;

											for (var i = 0; i < cst.listData.length; i++) {
												recs.push(new recType(cst.listData[i]));
											}
											dataSourcePoliFaskesTL.add(recs);
											gridListPoliFaskesTL.getView().refresh();
										}
										else {
											ShowPesanError_viDaftar('Gagal membaca data di poli rujuk', 'Error');
										}

									}, error: function (jqXHR, exception) {
										Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
									}
								});

							}
							else {
								ShowPesanWarning_viDaftar('Nama Rujukan Belum Diisi', 'No Rujukan');
							}

						}

					}
				}
			},

			{
				x: 10,
				y: 155,
				xtype: 'label',
				text: 'Diagnosa'
			},
			{
				x: 100,
				y: 155,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 155,
				xtype: 'textfield',
				id: 'txt_diagnosa_insert_rujukan',
				name: 'txt_diagnosa_insert_rujukan',
				width: 200,
				disabled: true,
				allowBlank: false,
				readOnly: true,
				maxLength: 3,
				tabIndex: 1
			},
			{
				x: 320,
				y: 155,
				xtype: 'label',
				text: 'Diagnosa Rujukan'
			},
			{
				x: 410,
				y: 155,
				xtype: 'label',
				text: ':'
			},

			{
				x: 420,
				y: 155,
				xtype: 'textfield',
				id: 'txt_diag_rujukan_insert_rujukan',
				name: 'txt_diag_rujukan_insert_rujukan',
				width: 220,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				listeners: {
					specialkey: function (text, e) {
						if (e.getKey() == 13) {
							formLookup_diagnosa_poli_faskesTL();
							loadMask.show();
							if (Ext.getCmp('txt_diag_rujukan_insert_rujukan').getValue() != '') {
								Ext.Ajax.request({
									method: 'POST',
									url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_data_diagnosa",
									params: {
										nama_diagnosa: Ext.getCmp('txt_diag_rujukan_insert_rujukan').getValue()
									},
									success: function (o) {
										loadMask.hide();
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											var recs = [],
												recType = dataSourceDiagnosaFaskesTL.recordType;

											for (var i = 0; i < cst.listData.length; i++) {
												recs.push(new recType(cst.listData[i]));
											}
											dataSourceDiagnosaFaskesTL.add(recs);
											gridListDiagnosaPoliFaskesTL.getView().refresh();
										}
										else {
											ShowPesanError_viDaftar('Gagal membaca data di diagnosa poli rujuk', 'Error');
										}

									}, error: function (jqXHR, exception) {
										//   Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
									}
								});

							}
							else {
								ShowPesanWarning_viDaftar('Nama Diagnosa Belum Diisi', 'No Rujukan');
							}

						}

					}
				}
			},
			{
				x: 10,
				y: 180,
				xtype: 'label',
				text: 'Catatan Rujukan'
			},
			{
				x: 100,
				y: 180,
				xtype: 'label',
				text: ':'
			},

			{
				x: 110,
				y: 180,
				xtype: 'textfield',
				id: 'txt_catatan_rujukan_insert_rujukan',
				name: 'txt_catatan_rujukan_insert_rujukan',
				width: 300,
				disabled: false,
				allowBlank: false,
				readOnly: false,
				maxLength: 3,
				tabIndex: 1
			},

			{
				x: 10,
				y: 205,
				xtype: 'label',
				text: 'Tipe Faskes'
			},
			{
				x: 100,
				y: 205,
				xtype: 'label',
				text: ':'
			},
			mCombTipeFaskesRujukan(),
				{
				x: 10,
				y: 235,
				xtype: 'label',
				text: 'Tgl Ren Kunjungan'
			},
			{
				x: 100,
				y: 235,
				xtype: 'label',
				text: ':'
			},
			{
				xtype: 'datefield',
				x: 110,
				y: 235,
				id: 'tgl_rencana_rujukan_insert_rujukan',
				name: 'tgl_rencana_rujukan_insert_rujukan',
				format: 'd/M/Y',
				readOnly: false,
				value: now,
				width: 200,

			}

		],
		buttons:
			[
				{
					text: "Simpan Rujukan",
					id: 'btn_simpan_rujukan',
					hidden: false,
					handler: function () {
						ajax_insert_rujukan();
						//	formLookups_rujukan_bpjs.close();	
					}
				},
				{
					text: "Update Rujukan",
					id: 'btn_update_rujukan',
					hidden: false,
					handler: function () {
						ajax_update_rujukan();
						//	formLookups_rujukan_bpjs.close();	
					}
				},
				{
					text: "Hapus Rujukan",
					id: 'btn_hapus_rujukan',
					hidden: false,
					handler: function () {
						Ext.MessageBox.confirm('Hapus Rujukan', "Yakin Akan Hapus Rujukan '" + Ext.getCmp('txt_no_rujukan_insert_rujukan').getValue() + "'  ?", function (btn) {
							if (btn === 'yes') {
								var param = {
									no_rujukan: Ext.getCmp('txt_no_rujukan_insert_rujukan').getValue()
								}
								$.ajax({
									type: 'POST',
									dataType: 'JSON',
									url: baseURL + "index.php/rawat_jalan/functionRWJ/hapus_rujukan/",
									data: param,
									success: function (cst) {
										if (cst.metaData.code == "200") {
											Ext.MessageBox.alert('Sukses', 'Rujukan berhasil dihapus');
											formLookups_insert_rujukan.close();

										} else {
											Ext.MessageBox.alert('Gagal', cst.metaData.message);
										}
									},
									error: function (jqXHR, exception) {
										loadMask.hide();
										Ext.MessageBox.alert('Gagal', 'Delete Rujukan gagal.');
										//dataSourceHistorySEP.removeAt(barisNoSEPHistorySEP);
										Ext.Ajax.request({
											url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
											params: {
												respon: jqXHR.responseText,
												keterangan: 'Error hapus Rujukan'
											}
										});
									}
								});
							}
						});
					}
				},
				/* {
					text: "Cetak Rujukan",
					id: 'btn_cetak_rujukan',
					hidden: true,
					handler: function () {
						cetak_rujukan_direct();
						//	formLookups_rujukan_bpjs.close();	
					}
				} */
				{
					text: "Cetak Rujukan",
					id: 'btn_cetak_rujukan',
					hidden: false,
					handler: function () {
						var no_surat = Ext.getCmp('txt_no_rujukan_insert_rujukan').getValue();
						console.log(no_surat);
						var url = baseURL + "index.php/laporan/lap_rujukan/cetak/" + no_surat + "/true";
						new Ext.Window({
							title: "CETAK RUJUKAN",
							width: 900,
							height: 500,
							constrain: true,
							modal: false,
							html: "<iframe style='width: 100%; height: 100%;' src='" + url + "'></iframe>",
							tbar: [
								{
									xtype: 'button',
									text: '<i class="fa fa-print"></i> Cetak',
									handler: function () {
										window.open(baseURL + "index.php/laporan/lap_rujukan/cetak/" + no_surat + "/true", "_blank");
									}
								}
							]
						}).show();
						// formPanelInsertRujukan.close();
					}
				}
			]
	});

	if(rowdata!=undefined){
		console.log(rowdata.response.noSep);
		getRujukanPBR(rowdata.response.peserta.noKartu);
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_history_rujukan",
				params: {no_sep: rowdata.response.noSep},
				failure: function (o){
				},
				success: function (o){
					//vclaim 2.0 // hani 23-02-22
					var cst = Ext.decode(o.responseText);
					console.log(cst);
					if(cst.sudah_ada ==true){
						Ext.getCmp('btn_simpan_rujukan').hide();
						Ext.getCmp('btn_update_rujukan').show();
						Ext.getCmp('btn_hapus_rujukan').show();
						Ext.getCmp('btn_cetak_rujukan').show();
						Ext.getCmp('txt_no_kartu_insert_rujukan').setValue(rowdata.response.peserta.noKartu);
						Ext.getCmp('txt_nama_peserta_insert_rujukan').setValue(rowdata.response.peserta.nama);
						Ext.getCmp('txt_tgl_lahir_peserta_insert_rujukan').setValue(rowdata.response.peserta.tglLahir);
						Ext.getCmp('txt_hak_kelas_insert_rujukan').setValue(rowdata.response.peserta.hakKelas.keterangan);
						Ext.getCmp('txt_diagnosa_insert_rujukan').setValue(rowdata.response.diagnosa);
						Ext.getCmp('txt_no_sep_insert_rujukan').setValue('');
						Ext.getCmp('tgl_sep_insert_rujukan').setValue(rowdata.response.tglSep).disable();
						Ext.getCmp('txt_hak_kelas_insert_rujukan').setValue(rowdata.response.kelasRawat);	
						Ext.getCmp('txt_no_rujukan_insert_rujukan').setValue(cst.listData.noRujukan);	
						Ext.getCmp('txt_sex_insert_rujukan').setValue(rowdata.response.peserta.kelamin);
						Ext.getCmp('tgl_rencana_rujukan_insert_rujukan').setValue(cst.listData.tglRencanaKunjungan);
						Ext.getCmp('tgl_rujukan_insert_rujukan').setValue(cst.listData.tglRujukan);
						Ext.getCmp('combo_pelayanan_insert_rujukan').setValue(cst.listData.jnsPelayanan);
						Ext.getCmp('txt_diag_rujukan_insert_rujukan').setValue(cst.listData.diagRujukan);
						Ext.getCmp('txt_no_sep_insert_rujukan').setValue(cst.listData.noSep);
						Ext.getCmp('txt_catatan_rujukan_insert_rujukan').setValue(cst.listData.catatan);
						Ext.getCmp('txt_spesialis_insert_rujukan').setValue(cst.listData.namaPoliRujukan);
						kode_poli_insert_rujukan_faskesTL=cst.listData.poliRujukan;
						tipe_rujukan_last=cst.listData.tipeRujukan;
						var tmp_pelayanan=cst.listData.jnsPelayanan;
						if(tmp_pelayanan==1){
							Ext.getCmp('combo_pelayanan_insert_rujukan').setValue('Rawat Inap');
							jenis_pelayanan_insert_rujukan=1
						}else{
							Ext.getCmp('combo_pelayanan_insert_rujukan').setValue('Rawat Jalan');
							jenis_pelayanan_insert_rujukan=2
						}
						var tmp_tipe_rujukan=cst.listData.tipeRujukan;
						if(tmp_tipe_rujukan==0){
							Ext.getCmp('rb_tipe_penuh').setValue(true);
						}else if(tmp_tipe_rujukan==1){
							Ext.getCmp('rb_tipe_partial').setValue(true);
						}else{
							Ext.getCmp('rb_rujukan_balik').setValue(true);
						}
						//CARI FAKSES//
						console.log(cst.listData.namaPoliRujukan);
						Ext.Ajax.request({
							url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_faskes",
							params: {kd_faskes  : cst.listData.ppkDirujuk,
									tipe_faskes : tipe_faskes},
							failure: function (o){
								ShowPesanError_viDaftar('Pencarian error ! ', 'Rawat Jalan');
							},
							success: function (o){
								var cst = Ext.decode(o.responseText);
								console.log(cst);
								Ext.getCmp('txt_dirujuk_ke_insert_rujukan').setValue(cst.listData[0].nama);
								kode_faskes_insert_rujukanTL=cst.listData[0].kode;								
							}
						});	
						//CARI POLI FASKES//
						//	Ext.getCmp('txt_spesialis_insert_rujukan').setValue();
						/*Ext.Ajax.request({
							url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_poli",
							params: {kd_poli  : cst.listData[0].poli_rujukan},
							failure: function (o){
								ShowPesanError_viDaftar('Pencarian error ! ', 'Rawat Jalan');
							},
							success: function (o){
								var cst = Ext.decode(o.responseText);
								console.log(cst);
								Ext.getCmp('txt_spesialis_insert_rujukan').setValue(cst.listData[0].nama);
								kode_poli_insert_rujukan_faskesTL=cst.listData[0].kode;
							}
						});*/

						//CARI DIAGNOSA//
						Ext.Ajax.request({
							url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_diagnosa",
							params: {kd_diagnosa  : cst.listData.diagRujukan},
							failure: function (o){
								ShowPesanError_viDaftar('Pencarian error ! ', 'Rawat Jalan');
							},
							success: function (o){
								var cst = Ext.decode(o.responseText);
								console.log(cst);
								Ext.getCmp('txt_diag_rujukan_insert_rujukan').setValue(cst.listData[0].nama);
								kode_diagnosa_insert_rujukan_faskesTL=cst.listData[0].kode;
							}
						});
					}else{
						Ext.getCmp('btn_update_rujukan').hide();
						Ext.getCmp('btn_hapus_rujukan').hide();
						Ext.getCmp('btn_cetak_rujukan').hide();
						Ext.getCmp('btn_simpan_rujukan').show();
						Ext.getCmp('txt_no_kartu_insert_rujukan').setValue(rowdata.response.peserta.noKartu);
						Ext.getCmp('txt_nama_peserta_insert_rujukan').setValue(rowdata.response.peserta.nama);
						Ext.getCmp('txt_tgl_lahir_peserta_insert_rujukan').setValue(rowdata.response.peserta.tglLahir);
						Ext.getCmp('txt_hak_kelas_insert_rujukan').setValue(rowdata.response.peserta.hakKelas.keterangan);
						Ext.getCmp('txt_diagnosa_insert_rujukan').setValue(rowdata.response.diagnosa);
						Ext.getCmp('txt_no_sep_insert_rujukan').setValue(rowdata.response.noSep);
						Ext.getCmp('tgl_sep_insert_rujukan').setValue(rowdata.response.tglSep).disable();
						Ext.getCmp('txt_hak_kelas_insert_rujukan').setValue(rowdata.response.kelasRawat);
						Ext.getCmp('txt_no_rujukan_insert_rujukan').setValue("");	
						Ext.getCmp('txt_sex_insert_rujukan').setValue(rowdata.response.peserta.kelamin);

					}
				}
			});
			
					
	}
	return formPanelInsertRujukan;
}

//CEK RUJUKAN NON PBR
function getRujukanPBR(noka) {
  Ext.Ajax.request({
    method:'POST',
    url: baseURL + "index.php/rawat_jalan/functionRWJ/getDataBpjs",
    params: {
      klinik : '',
      no_kartu: noka 
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if(cst.data != null){
          kd_rujukan_pbr = cst.data.response.peserta.provUmum.kdProvider;
          nama_rujukan_pbr =cst.data.response.peserta.provUmum.nmProvider;
          // Ext.getCmp('txt_dirujuk_ke_insert_rujukan').setValue(nama_rujukan_pbr);
      }else{
        ShowPesanError_viDaftar(cst.metaData.message,'Cek PPK Dirujuk');
      }
    },error: function (jqXHR, exception) {
       Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
    }
  });
}

function mCombPelayananInsertRujukan() {
  var cboJenisPelayananInsertRujukan = new Ext.form.ComboBox({
    id: "combo_pelayanan_insert_rujukan",
    x: 420,
    y: 55,
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    tabIndex: 6,
    forceSelection: true,
    emptyText: "",
    anchor: "95%",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [0, "Rawat Inap"],
        [1, "Rawat Jalan"],
      ],
    }),
    valueField: "Id",
    displayField: "displayText",
    enableKeyEvents: true,
    listeners: {
      select: function (a, b, c) {
        console.log(b.data);
        if (b.data.Id == 0) {
          jenis_pelayanan_insert_rujukan = 1;
        } else {
          jenis_pelayanan_insert_rujukan = 2;
        }
      },
    },
  });
  return cboJenisPelayananInsertRujukan;
}

function formLookup_faskes_tl(rowdata) {
	var lebar = 530;
	formLookup_faskes_tl_insert_rujukan = new Ext.Window({
		id: 'formLookup_faskes_tlInsertRujukan',
		name: 'formLookup_faskes_tlInsertRujukan',
		title: 'Daftar Fasilitas Kesehatan',
		closeAction: 'destroy',
		width: lebar,
		height: 210, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_faskes_tl(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookup_faskes_tl_insert_rujukan.show();
}

function panel_form_lookup_faskes_tl() {
	var Field = ['KODE', 'NAMA'];
	dataSourceFaskesTLInsertRujukan = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListFaskesTL = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceFaskesTLInsertRujukan,
		columnLines: false,
		autoScroll: false,
		width: 520,
		height: 200,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectFaskesTL = dataSourceFaskesTLInsertRujukan.getAt(row);
					console.log(rowselectFaskesTL.data.nama);
					if (rowselectFaskesTL.data.kode != '') {

						kode_faskes_insert_rujukanTL = rowselectFaskesTL.data.kode;
						Ext.getCmp('txt_dirujuk_ke_insert_rujukan').setValue(rowselectFaskesTL.data.nama);
						formLookup_faskes_tl_insert_rujukan.close();
					} else {
						ShowPesanWarning_viDaftar('Pilih Fasilitas Kesehatan', 'Daftar  Fasilitas Kesehatan');
					}

				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				id: 'colNoKunjunganMultiRujukan',
				header: 'KODE',
				dataIndex: 'kode',
				hideable: false,
				menuDisabled: true,
				width: 10
			},
			{
				id: 'colNoKartuMultiRujukan',
				header: 'NAMA FASKES',
				dataIndex: 'nama',
				hideable: false,
				menuDisabled: true,
				width: 50
			}

		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListFaskesTL
				]
			}
		]
	};
	return items;
}

function formLookup_poli_faskesTL(rowdata) {
	var lebar = 530;
	formLookup_poli_faskes_tl = new Ext.Window({
		id: 'formLookup_poli_faskes_tl',
		name: 'formLookup_poli_faskes_tl',
		title: 'Daftar Poli Fasilitas Kesehatan',
		closeAction: 'destroy',
		width: lebar,
		height: 210, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_poli_faskes_tl(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookup_poli_faskes_tl.show();
}

function panel_form_lookup_poli_faskes_tl() {
	var Field = ['KODE', 'NAMA'];
	dataSourcePoliFaskesTL = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListPoliFaskesTL = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourcePoliFaskesTL,
		columnLines: false,
		autoScroll: false,
		width: 520,
		height: 200,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectPoliFaskesTL = dataSourcePoliFaskesTL.getAt(row);
					console.log(rowselectPoliFaskesTL.data.nama);
					if (rowselectPoliFaskesTL.data.kode != '') {

						kode_poli_insert_rujukan_faskesTL = rowselectPoliFaskesTL.data.kode;
						Ext.getCmp('txt_spesialis_insert_rujukan').setValue(rowselectPoliFaskesTL.data.nama);
						formLookup_poli_faskes_tl.close();
					} else {
						ShowPesanWarning_viDaftar('Pilih Poli Fasilitas Kesehatan', 'Daftar Fasilitas Kesehatan');
					}

				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				//id: 'colNoKunjunganMultiRujukan',
				header: 'KODE',
				dataIndex: 'kode',
				hideable: false,
				menuDisabled: true,
				width: 10
			},
			{
				//	id: 'colNoKartuMultiRujukan',
				header: 'NAMA FASKES',
				dataIndex: 'nama',
				hideable: false,
				menuDisabled: true,
				width: 50
			}

		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListPoliFaskesTL
				]
			}
		]
	};
	return items;
}


function formLookup_diagnosa_poli_faskesTL(rowdata) {
	var lebar = 530;
	formLookup_diagnosa_poli_faskes_tl = new Ext.Window({
		id: 'formLookup_poli_faskes_tl',
		name: 'formLookup_poli_faskes_tl',
		title: 'Daftar Diagnosa Fasilitas Kesehatan',
		closeAction: 'destroy',
		width: lebar,
		height: 210, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_diagnosa_poli_faskes_tl(), //1
		listeners: {
			afterShow: function () {
				this.activate();

			}
		}
	});
	formLookup_diagnosa_poli_faskes_tl.show();
}

function panel_form_lookup_diagnosa_poli_faskes_tl() {
	var Field = ['KODE', 'NAMA'];
	dataSourceDiagnosaFaskesTL = new Ext.data.ArrayStore({
		fields: Field
	});
	var gridListDiagnosaPoliFaskesTL = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dataSourceDiagnosaFaskesTL,
		columnLines: false,
		autoScroll: false,
		width: 520,
		height: 200,
		x: 5,
		y: 10,
		border: true,
		sort: false,
		autoHeight: false,
		layout: 'absolute',
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowselectDiagnosaPoliFaskesTL = dataSourceDiagnosaFaskesTL.getAt(row);
					console.log(rowselectDiagnosaPoliFaskesTL.data.nama);
					if (rowselectDiagnosaPoliFaskesTL.data.kode != '') {

						kode_diagnosa_insert_rujukan_faskesTL = rowselectDiagnosaPoliFaskesTL.data.kode;
						console.log(kode_diagnosa_insert_rujukan_faskesTL);
						Ext.getCmp('txt_diag_rujukan_insert_rujukan').setValue(rowselectDiagnosaPoliFaskesTL.data.nama);
						formLookup_diagnosa_poli_faskes_tl.close();
					} else {
						ShowPesanWarning_viDaftar('Pilih Poli Fasilitas Kesehatan', 'Daftar Fasilitas Kesehatan');
					}

				}
			}
		}),
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header: 'No.'
			}), {
				//id: 'colNoKunjunganMultiRujukan',
				header: 'KODE',
				dataIndex: 'kode',
				hideable: false,
				menuDisabled: true,
				width: 10
			},
			{
				//id: 'colNoKartuMultiRujukan',
				header: 'NAMA FASKES',
				dataIndex: 'nama',
				hideable: false,
				menuDisabled: true,
				width: 50
			}

		]),
		viewConfig: {
			forceFit: true
		}
	});
	var items = {
		//title: 'PPK Rujukan FKTP',
		layout: 'column',
		border: false,
		autoScroll: false,
		items: [
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items: [
					/*{
						xtype: 'textarea',
						width : 594,
						height: 250, 
						x: 245,
						y:80,
						name: 'textArearujukfktp',
						id: 'textArearujukfktp'
					}, */
					gridListDiagnosaPoliFaskesTL
				]
			}
		]
	};
	return items;
}

function ajax_insert_rujukan() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/insert_rujukan/",
		data: getParamRujukan(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				rujukan_tipe = '';
				Ext.getCmp('txt_no_rujukan_insert_rujukan').setValue(resp1.response.rujukan.noRujukan);
				ShowPesanInfo_viDaftar('Rujukan Berhasil Dibuat', 'Pembuatan Rujukan');
				Ext.getCmp('btn_simpan_rujukan').disable();
				Ext.getCmp('btn_update_rujukan').enable();
				Ext.getCmp('btn_hapus_rujukan').enable();
        Ext.getCmp('btn_cetak_rujukan').enable();
				/*Ext.Msg.show({
					title: 'Cetak Rujukan',
					msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function (btn){
						if (btn == 'yes'){
							cetak_rujukan_direct();
						}
					},
					icon: Ext.MessageBox.QUESTION
				});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error create SEP'
				}
			});
		}
	});
}

function getParamRujukan() {
	var tgl_dirujuk_clean = Ext.getCmp('tgl_rujukan_insert_rujukan').getValue()
	var tgl_rencanaKunjungan_clean=Ext.getCmp('tgl_rencana_rujukan_insert_rujukan').getValue()
	var params =
	{
		no_sep: Ext.getCmp('txt_no_sep_insert_rujukan').getValue(),
		tgl_rujukan: tgl_dirujuk_clean.format("Y-m-d"),
		tgl_renKunj	  	 :tgl_rencanaKunjungan_clean.format("Y-m-d"),
		pkk_dirujuk: kode_faskes_insert_rujukanTL,
		jenis_pelayanan: jenis_pelayanan_insert_rujukan,
		catatan: Ext.getCmp('txt_catatan_rujukan_insert_rujukan').getValue(),
		diag_rujukan: kode_diagnosa_insert_rujukan_faskesTL,
		tipe_rujukan: rujukan_tipe,
		poli_rujukan: kode_poli_insert_rujukan_faskesTL,
		user: 'Test WS',
		nama_poli_rujuk: Ext.getCmp('txt_spesialis_insert_rujukan').getValue()
	}
	return params;
}

function getParamRujukanUpdate() {
	var tgl_dirujuk_clean = Ext.getCmp('tgl_rujukan_insert_rujukan').getValue();
	var tgl_rencanaKunjungan_clean=Ext.getCmp('tgl_rencana_rujukan_insert_rujukan').getValue()

	var params =
	{
		no_sep: Ext.getCmp('txt_no_sep_insert_rujukan').getValue(),
		no_rujukan: Ext.getCmp('txt_no_rujukan_insert_rujukan').getValue(),
		tgl_rujukan: tgl_dirujuk_clean.format("Y-m-d"),
		tgl_renKunj	  	 :tgl_rencanaKunjungan_clean.format("Y-m-d"),
		pkk_dirujuk: kode_faskes_insert_rujukanTL,
		jenis_pelayanan: jenis_pelayanan_insert_rujukan,
		catatan: Ext.getCmp('txt_catatan_rujukan_insert_rujukan').getValue(),
		diag_rujukan: kode_diagnosa_insert_rujukan_faskesTL,
		tipe_rujukan: rujukan_tipe,
		tipe_rujukan_last: tipe_rujukan_last,
		poli_rujukan: kode_poli_insert_rujukan_faskesTL,
		user: 'Test WS',
		nama_poli_rujuk: Ext.getCmp('txt_spesialis_insert_rujukan').getValue()
	}
	return params;
}

function ajax_update_rujukan() {
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/update_rujukan/",
		data: getParamRujukanUpdate(),
		success: function (resp1) {
			loadMask.hide();
			if (resp1.metaData.code == '200') {
				//console.log(resp1.response.sep.peserta.noKartu);
				ShowPesanInfo_viDaftar('Rujukan Berhasil Di Update', 'Update Rujukan');
				/*Ext.getCmp('txtNoSJP').setValue(resp1.response.sep.noSep);
				Ext.getCmp('txtNoAskes').setValue(resp1.response.sep.peserta.noKartu);
				Ext.getCmp('txtNamaPeserta').setValue(resp1.response.sep.peserta.nama);
				Ext.getCmp('txtNoSJP').focus();
					Ext.Msg.show({
						title: 'Cetak Rujukan',
						msg: 'Apakah Rujukan Akan Dicetak Sekarang ?',
						buttons: Ext.MessageBox.YESNO,
						fn: function (btn){
							if (btn == 'yes'){
								cetak_rujukan_direct();
							}
						},
						icon: Ext.MessageBox.QUESTION
					});*/
			} else {
				Ext.MessageBox.alert('Gagal', resp1.metaData.message);
			}
		},
		error: function (jqXHR, exception) {
			loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error create SEP'
				}
			});
		}
	});
}

function mCombTipeFaskesRujukan() {
	var cboTipeFaskesInsertRujukan = new Ext.form.ComboBox({
		id: 'combo_tipe_faskes_insert_rujukan',
		x: 110,
		y: 205,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		tabIndex: 6,
		forceSelection: true,
		emptyText: '',
		//anchor: '95%',
		width: 200,
		store: new Ext.data.ArrayStore({
			id: 2,
			fields: ['Id', 'displayText'],
			data: [[1, 'Fakses Tingkat Pertama'], [2, 'Faskes Tingkat Lanjut']]
		}),
		valueField: 'Id',
		value: 2,
		displayField: 'displayText',
		//	enableKeyEvents:true,
		listeners: {
			'select': function (a, b, c) {
				console.log(b.data);
				if (b.data.Id == 1) {
					tipe_faskes = 1;
					console.log(tipe_faskes);
				} else {
					tipe_faskes = 2;
					console.log(tipe_faskes);
				}
			},

		}
	});
	return cboTipeFaskesInsertRujukan;

}