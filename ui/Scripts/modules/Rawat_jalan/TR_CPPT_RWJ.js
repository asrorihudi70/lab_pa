var CPPTPenataJasaRWJ = {};
var rowSelectedPJRWI_CPPT;
var now = new Date();
CPPTPenataJasaRWJ.selectCount        = 50;
CPPTPenataJasaRWJ.dsComboObat;
CPPTPenataJasaRWJ.gridRiwayatKunjungan;
CPPTPenataJasaRWJ.gridRiwayatDiagnosa;
CPPTPenataJasaRWJ.gridRiwayatTindakan;
CPPTPenataJasaRWJ.selectCountStatus_bayar = 'Belum Pulang';
CPPTPenataJasaRWJ.gridRiwayatObat;
CPPTPenataJasaRWJ.gridRiwayatLab;
CPPTPenataJasaRWJ.gridRiwayatRad;
CPPTPenataJasaRWJ.iComboObat;
CPPTPenataJasaRWJ.iComboVerifiedObat;
CPPTPenataJasaRWJ.gridObat;
CPPTPenataJasaRWJ.gridLastHistoryDiagnosa;
CPPTPenataJasaRWJ.DataStoreDokter=new Ext.data.ArrayStore({id: 0,fields:['kd_dokter','nama','status','jenis_dokter'],data: []});
CPPTPenataJasaRWJ.grid1;
CPPTPenataJasaRWJ.grid2;
CPPTPenataJasaRWJ.grid3;
CPPTPenataJasaRWJ.mRecordRwiCTTP = Ext.data.Record.create([
    {name: 'tgl', mapping: 'tgl'},
    {name: 'jam', mapping: 'jam'},
    {name: 'bagian', mapping: 'bagian'},
    {name: 'hasil', mapping: 'hasil'},
    {name: 'verifikasi', mapping: 'verifikasi'}
]);
CPPTPenataJasaRWJ.ds1;
CPPTPenataJasaRWJ.ds2;
CPPTPenataJasaRWJ.ds3;
CPPTPenataJasaRWJ.ds4 = new WebApp.DataStore({fields: ['kd_produk', 'kd_klas', 'deskripsi', 'username', 'kd_lab']});
CPPTPenataJasaRWJ.ds5 = new WebApp.DataStore({fields: ['kd_cara_keluar', 'cara_keluar']});
CPPTPenataJasaRWJ.dssebabkematian = new WebApp.DataStore({fields: ['kd_sebab_mati', 'sebab_mati']});
CPPTPenataJasaRWJ.dsstatupulang = new WebApp.DataStore({fields: ['kd_status_pulang', 'status_pulang']});
CPPTPenataJasaRWJ.dsGridObat;
CPPTPenataJasaRWJ.dsGridTindakan;
CPPTPenataJasaRWJ.s1;
CPPTPenataJasaRWJ.btn1;
CPPTPenataJasaRWJ.ds_trbokter_rwi;
CPPTPenataJasaRWJ.form = {};
CPPTPenataJasaRWJ.form.Grid = {};
CPPTPenataJasaRWJ.func = {};
CPPTPenataJasaRWJ.TGL_TRANSAKSI;
CPPTPenataJasaRWJ.TGL_TRANSAKSI_detailtransaksi;
CPPTPenataJasaRWJ.pj_req_rad;
CPPTPenataJasaRWJ.form.Checkbox = {};
CPPTPenataJasaRWJ.form.Class = {};
CPPTPenataJasaRWJ.form.ComboBox = {};
CPPTPenataJasaRWJ.form.DataStore = {};
CPPTPenataJasaRWJ.form.Grid = {};
CPPTPenataJasaRWJ.form.Group = {};
CPPTPenataJasaRWJ.form.Group.print = {};
CPPTPenataJasaRWJ.form.Window = {};
CPPTPenataJasaRWJ.URUT;
CPPTPenataJasaRWJ.KD_PRODUK;
CPPTPenataJasaRWJ.QTY;
CPPTPenataJasaRWJ.TGL_BERLAKU;
CPPTPenataJasaRWJ.HARGA;
CPPTPenataJasaRWJ.varkd_tarif;
CPPTPenataJasaRWJ.KD_TARIF;
CPPTPenataJasaRWJ.DESKRIPSI;
CPPTPenataJasaRWJ.dshasil_cppt_rwi;
CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT;
CPPTPenataJasaRWJ.gridrad;
CPPTPenataJasaRWJ.gridIcd9;
CPPTPenataJasaRWJ.var_kd_dokter_leb;
CPPTPenataJasaRWJ.var_kd_dokter_rad;
CPPTPenataJasaRWJ.form.DataStore.penyakit = new Ext.data.ArrayStore({id: 0, fields: ['text', 'kd_penyakit', 'penyakit'], data: []});
CPPTPenataJasaRWJ.form.DataStore.kdpenyakit= new Ext.data.ArrayStore({id: 0, fields: ['text', 'kd_penyakit', 'penyakit'], data: []});
CPPTPenataJasaRWJ.form.DataStore.produk = new Ext.data.ArrayStore({id: 0, fields: ['kd_produk', 'deskripsi', 'harga', 'tglberlaku','kd_tarif'], data: []});
CPPTPenataJasaRWJ.form.DataStore.trdokter = new Ext.data.ArrayStore({id: 0, fields: ['kd_dokter', 'nama'], data: []});
CPPTPenataJasaRWJ.form.DataStore.dokter_inap_int = new Ext.data.ArrayStore({id: 0, fields: ['kd_job', 'label'], data: []});
CPPTPenataJasaRWJ.form.DataStore.kdIcd9 = new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});
CPPTPenataJasaRWJ.form.DataStore.deskripsi = new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});
CPPTPenataJasaRWJ.form.DataStore.obat=new Ext.data.ArrayStore({
    id: 0,
    fields: [
                'kd_prd','nama_obat','jml_stok_apt','kd_unit_far','kd_milik'
            ],
    data: []
});
var fieldsDokterPenindak = [
    {name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
    {name: 'NAMA', mapping : 'NAMA'}
];
dsDataDokterPenindak_PJ_RWI = new WebApp.DataStore({ fields: fieldsDokterPenindak });

var Field = ['KD_DOKTER','NAMA'];
dsDokterRequestEntry = new WebApp.DataStore({fields: Field});

CurrentPage.page = getPanelRWI_CPPT(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
CPPTPenataJasaRWJ.ComboVerifiedObat = function () {
    CPPTPenataJasaRWJ.iComboVerifiedObat = new Ext.form.ComboBox({
        id: Nci.getId(),
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        anchor: '96.8%',
        emptyText: '',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[0, 'Verified'], [1, 'Not Verified']]
        }),
        valueField: 'displayText',
        displayField: 'displayText'
    });
    return CPPTPenataJasaRWJ.iComboVerifiedObat;
};
CPPTPenataJasaRWJ.classGridObat = Ext.data.Record.create([
    {name: 'kd_prd', mapping: 'kd_prd'},
    {name: 'nama_obat', mapping: 'nama_obat'},
    {name: 'jumlah', mapping: 'jumlah'},
    {name: 'satuan', mapping: 'satuan'},
    {name: 'cara_pakai', mapping: 'cara_pakai'},
    {name: 'kd_dokter', mapping: 'kd_dokter'},
    {name: 'verified', mapping: 'verified'},
    {name: 'racikan', mapping: 'racikan'},
    {name: 'order_mng',     mapping: 'order_mng'},
    {name: 'jml_stok_apt', mapping: 'jml_stok_apt'}

]);
CPPTPenataJasaRWJ.classGrid3 = Ext.data.Record.create([
    {name: 'kd_produk', mapping: 'kd_produk'},
    {name: 'kd_klas', mapping: 'kd_klas'},
    {name: 'deskripsi', mapping: 'deskripsi'},
    {name: 'username', mapping: 'username'},
    {name: 'kd_lab', mapping: 'kd_lab'}
]);
CPPTPenataJasaRWJ.form.Class.diagnosa = Ext.data.Record.create([
    {name: 'KD_PENYAKIT', mapping: 'KD_PENYAKIT'},
    {name: 'PENYAKIT', mapping: 'PENYAKIT'},
    {name: 'KD_PASIEN', mapping: 'KD_PASIEN'},
    {name: 'URUT', mapping: 'UURUTRUT'},
    {name: 'URUT_MASUK', mapping: 'URUT_MASUK'},
    {name: 'TGL_MASUK', mapping: 'TGL_MASUK'},
    {name: 'KASUS', mapping: 'KASUS'},
    {name: 'STAT_DIAG', mapping: 'STAT_DIAG'},
    {name: 'NOTE', mapping: 'NOTE'},
    {name: 'DETAIL', mapping: 'DETAIL'}
]);
CPPTPenataJasaRWJ.form.Class.produk = Ext.data.Record.create([
    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
    {name: 'DESKRIPSI', mapping: 'PDESKRIPSIENYAKIT'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'DOKTER', mapping: 'DOKTER'},
    {name: 'TGL_TINDAKAN', mapping: 'TGL_TINDAKAN'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
    {name: 'TGL_BERLAKU', mapping: 'TGL_BERLAKU'},
    {name: 'NO_TRANSAKSI', mapping: 'NO_TRANSAKSI'},
    {name: 'URUT', mapping: 'URUT'},
    {name: 'DESC_STATUS', mapping: 'DESC_STATUS'},
    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
    {name: 'HARGA', mapping: 'HARGA'}
]);
CPPTPenataJasaRWJ.func.getNullProduk = function () {
    console.log(grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0]);
    var o = grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    // var tmptgl_transaksi = new Date(Ext.getCmp('txtTanggalTransaksi_PJRWI').getValue());
    var tmptgl_transaksi = new Date(now);
    return new CPPTPenataJasaRWJ.form.Class.diagnosa({
        KD_UNIT: tmpkd_unitKamar_PJRWI,
        KD_UNIT_TR: tmpkd_unitKamar_PJRWI,
        KD_PRODUK: '',
        DESKRIPSI: '',
        QTY: 1,
        DOKTER: '',
        TGL_TINDAKAN: '',
        DESC_REQ: '',
        TGL_BERLAKU: '',
        NO_TRANSAKSI: '',
        URUT: '',
        // KD_UNIT_TR: o.KD_UNIT,
        DESC_STATUS: '',
        // TGL_TRANSAKSI: o.TANGGAL_TRANSAKSI,
        TGL_TRANSAKSI: tmptgl_transaksi.format("d-M-Y"),
        KD_TARIF: '',
        HARGA: '',
    });
}
function gettrdokterdarivisitedok(rowdata) {
    //var o=grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    return new CPPTPenataJasaRWJ.form.Class.trdokter({
        kd_nama: '',
        kd_lab: '',
        jp: 0,
        jpp: 0,
    });
}
CPPTPenataJasaRWJ.func.getNulltrdokter = function () {
    //var o=grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    return new CPPTPenataJasaRWJ.form.Class.trdokter({
        kd_nama: '',
        kd_lab: '',
        jp: 0,
        jpp: 0,
    });
};
CPPTPenataJasaRWJ.func.getNullDiagnosa = function () {
    return new CPPTPenataJasaRWJ.form.Class.diagnosa({
        KD_PENYAKIT: '',
        PENYAKIT: '',
        KD_PASIEN: '',
        URUT: 0,
        URUT_MASUK: '',
        TGL_MASUK: '',
        KASUS: '',
        STAT_DIAG: '',
        NOTE: '',
        DETAIL: ''
    });
}
CPPTPenataJasaRWJ.nullGridObat = function () {
    return new CPPTPenataJasaRWJ.classGridObat({
        kd_produk   : '',
        nama_obat   : '',
        jumlah      : 0,
        signa       : '',
        satuan      : '',
        cara_pakai  : '',
        kd_dokter   : '',
        verified    : 'Tidak Disetujui',
        racikan     : false,
        racikan_text        : 'Tidak',
        order_mng   : 'Belum Dilayani',
        jml_stok_apt :0
    });
}
function hasilJumlah(qty){
    for(var i=0; i<dsPjTrans2_panatajasarwi.getCount() ; i++){
        var o=dsPjTrans2_panatajasarwi.getRange()[i].data;
        if(qty != undefined){
            if(o.jumlah <= o.jml_stok_apt){
            
            } else{
                CPPTPenataJasaRWJ.gridObat.getView().refresh();
            }
        }
    }
}
function hasilJumlah_rwj(qty) {
    for (var i = 0; i < dsPjTrans2_panatajasarwi.getCount(); i++) {
        var o = dsPjTrans2_panatajasarwi.getRange()[i].data;
        if (qty != undefined) {
            if (o.jumlah <= o.jml_stok_apt) {
            } else {
                o.jumlah = o.jml_stok_apt;
                CPPTPenataJasaRWJ.gridObat.getView().refresh();
                ShowPesanWarning_CPPTPenataJasaRWJ('Jumlah obat melebihi stok yang tersedia', 'Warning');
            }
        }
    }
}
CPPTPenataJasaRWJ.nullGrid3 = function () {
    var tgltranstoday = new Date();
    return new CPPTPenataJasaRWJ.classGrid3({
        kd_produk: '',
        deskripsi: '',
        kd_tarif: '',
        harga: '',
        qty: '',
        tgl_berlaku: '',
        urut: '',
        tgl_transaksi: tgltranstoday.format('Y/m/d'),
        no_transaksi: '',
    });
};
CPPTPenataJasaRWJ.comboObat = function(){
    var $this   = this;
    $this.dsComboObat   = new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
/*  $this.dsComboObat.load({ 
        params  : { 
            Skip    : 0, 
            Take    : 50, 
            target  :'ViewComboObatRJPJ',
            kdcustomer: vkode_customer
        } 
    }); */
    $this.iComboObat= new Ext.form.ComboBox({
        id              : Nci.getId(),
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        emptyText       : '',
        store           : $this.dsComboObat,
        valueField      : 'nama_obat',
        hideTrigger     : true,
        displayField    : 'nama_obat',
        value           : '',
        listeners       : {
            select  : function(a, b, c){
                var line    = $this.gridObat.getSelectionModel().selection.cell[0];
                $this.dsGridObat.getRange()[line].data.kd_prd       =b.json.kd_prd;
                $this.dsGridObat.getRange()[line].data.satuan       =b.json.satuan;
                $this.dsGridObat.getRange()[line].data.kd_dokter    =b.json.nama;
                $this.dsGridObat.getRange()[line].data.nama_obat    =b.json.nama_obat;
                $this.dsGridObat.getRange()[line].data.jml_stok_apt =b.json.jml_stok_apt;
                $this.gridObat.getView().refresh();
            }
        }
    });
    return $this.iComboObat;
}
function getPanelRWI_CPPT(mod_id) {
    var Field = ['PEKERJAAN','JENIS_KELAMIN','KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT','KD_PAY','KD_SPESIAL', 
                'ALAMAT', 'TANGGAL_TRANSAKSI', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 
                'POSTING_TRANSAKSI', 'ANAMNESE', 'CAT_FISIK','namaunit','KD_UNIT_KAMAR','data_pulang',
                'NAMA_KAMAR','KELAS','KD_KELAS','NO_KAMAR','KD_KASIR','KETERANGAN','JAM_MASUK','TANGGAL_MASUK',
                'CARA_KELUAR','SEBAB_MATI','KEADAAN_PASIEN','BBL','RAWAT_GABUNG','HAK_KELAS'];
    dsTRKasirRWIList_panatajasarwi_cppt = new WebApp.DataStore({fields: Field});
    CPPTPenataJasaRWJ.ds1 = dsTRKasirRWIList_panatajasarwi_cppt;
    RefreshCPPTPenataJasaRWJ_cppt();
    /* getTotKunjunganRWI();
    var i = setInterval(function(){
        getTotKunjunganRWI();
    }, 100000);  */
    grListTRRWI_CPPT_RWI = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsTRKasirRWIList_panatajasarwi_cppt,
        columnLines: true,
        autoScroll: false,
        border: true,
        flex:1,
        sort: false,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelectedPJRWI_CPPT = dsTRKasirRWIList_panatajasarwi_cppt.getAt(ridx);
                CPPTPenataJasaRWJ.s1        = CPPTPenataJasaRWJ.ds1.getAt(ridx);
                if (rowSelectedPJRWI_CPPT != undefined) {
                    console.log(rowSelectedPJRWI_CPPT)
                    RWILookUpCPPT(rowSelectedPJRWI_CPPT.data);
                } else {
                    RWILookUpCPPT();
                }

                loaddatastoredokter_CPPT_REVISI(rowSelectedPJRWI_CPPT);
            }
        },
        cm: new Ext.grid.ColumnModel([
            new Ext.grid.RowNumberer(),
            {
                header: 'St. Posting',
                width: 50,
                sortable: false,
                hideable: true,
                hidden: false,
                menuDisabled: true,
                dataIndex: 'POSTING_TRANSAKSI',
                id: 'txtposting',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 1:
                            metaData.css = 'StatusHijau';
                            break;
                        case 0:
                            metaData.css = 'StatusMerah';
                            break;
                    }
                    return '';
                }
            }, {
                header: 'St. pulang',
                width: 50,
                sortable: false,
                hideable: true,
                hidden: true,
                menuDisabled: true,
                dataIndex: 'data_pulang',
                id: 'txtposting',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 'true':
                            metaData.css = 'StatusHijau';
                            break;
                        case 'false':
                            metaData.css = 'StatusMerah';
                            break;
                    }
                    return '';
                }
            }, {
                id: 'colReqIdViewRWI',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 80
            }, {
                id: 'colTglRWIViewRWI',
                header: 'Tgl Transaksi',
                dataIndex: 'TANGGAL_TRANSAKSI',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 75,
                renderer: function (v, params, record) {
                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                }
            }, {
                header: 'No. Medrec',
                width: 65,
                sortable: false,
                hideable: false,
                menuDisabled: true,
                dataIndex: 'KD_PASIEN',
                id: 'colRWIerViewRWI'
            }, {
                header: 'Pasien',
                width: 190,
                sortable: false,
                hideable: false,
                menuDisabled: true,
                dataIndex: 'NAMA',
                id: 'colRWIerViewRWI'
            }, {
                id: 'colLocationViewRWI',
                header: 'Alamat',
                dataIndex: 'ALAMAT',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 170
            }, {
                id: 'colDeptViewRWI',
                header: 'Pelaksana',
                dataIndex: 'NAMA_DOKTER',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 150
            },{
                id: 'colDeptViewRWI',
                header: 'Kamar',
                dataIndex: 'KETERANGAN',
                sortable: false,
                hidden:true,
                hideable: false,
                menuDisabled: true,
                width: 150
            },{
                id: 'colKdUnitKamar_PJRWI',
                header: 'Unit Kamar',
                hidden:true,
                dataIndex: 'KD_UNIT_KAMAR',
                sortable: false,
                hideable: true,
                menuDisabled: true,
                width: 150
            },
            
        ]),
        viewConfig: {forceFit: true},
    });
    CPPTPenataJasaRWJ.grid1 = grListTRRWI_CPPT_RWI;
    var FormDepanCttpRWI    = new Ext.Panel({
        id: mod_id,
        closable: true,
        layout: {
            type:'vbox',
            align:'stretch'
        },
        style:'padding: 4px;',
        title: 'CPPT RWJ ',
        border: false,
        autoScroll: false,
        items: [
            {
                xtype: 'panel',
                // plain: true,
                // border:false,
                height: 90,
                layout:'column',
                style:'padding-bottom: 4px;',
                defaults: {
                    bodyStyle: 'padding:4px',
                    autoScroll: true
                },
                items: [
                    {
                        layout: 'form',
                        border: false,
                        columnWidth:.3,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel:'No. Medrec ',
                                id: 'txtFilterNomedrec_panatajasarwi_cppt',
                                anchor: '100%',
                                onInit: function () { },
                                listeners: {
                                    'specialkey': function () {
                                        var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10) {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
                                                var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue());
                                                Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').setValue(tmpgetNoMedrec);
                                                // var tmpkriteria = getCriteriaFilter_viDaftar();
                                                RefreshDataFilter_panatajasarwi_CPPT();
                                            } else {
                                                if (tmpNoMedrec.length === 10) {
                                                    RefreshDataFilter_panatajasarwi_CPPT();
                                                } else {
                                                    Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').setValue('');
                                                }
                                            }
                                        }
                                    }
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: ' Pasien' + ' ',
                                id: 'TxtFilterGridDataView_NAMA_viKasirRwi_cppt',
                                enableKeyEvents: true,
                                listeners: {
                                    'specialkey': function () {
                                        if (Ext.EventObject.getKey() === 13) {
                                            RefreshDataFilter_panatajasarwi_CPPT();
                                        }
                                    }
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: ' Dokter' + ' ',
                                id: 'TxtFilterGridDataView_DOKTER_viKasirRwi_cppt',
                                enableKeyEvents: true,
                                listeners: {
                                    'specialkey': function () {
                                        if (Ext.EventObject.getKey() === 13) {
                                            RefreshDataFilter_panatajasarwi_CPPT();
                                        }
                                    }
                                }
                            },
                            
                        ]
                    },{
                        layout: 'form',
                        // margins: '0 5 5 0',
                        columnWidth:.7,
                        border: false,
                        items: [
                            // getItemcombo_filter_CPPTPenataJasaRWJ(), 
                            getItemPaneltgl_filter_CPPTPenataJasaRWJ(),
                            mComboViewDataPJRWI_cppt(),
                        ]
                    }
                ]
            },
            grListTRRWI_CPPT_RWI,
            ]
    });
    // loaddatastoredokter_CPPT_REVISI(rowSelectedPJRWI_CPPT);
    // RefreshDataFilter_panatajasarwi_CPPT();
    return FormDepanCttpRWI;
}

function mComboViewDataPJRWI_cppt(){
    var mComboViewDataPJRWI_cppt = new Ext.form.ComboBox({
        width: 50,
        fieldLabel      : 'Jumlah data',
        id              : 'mComboViewDataPJRWI_cppt',
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        selectOnFocus   : true,
        forceSelection  : true,
        emptyText       : '',
        store: new Ext.data.ArrayStore({
            fields:['id',],
            data: [[5],[10],[25],[50],[100],]
        }),
        valueField: 'id',
        displayField: 'id',
        value: 10,
        listeners:{
            'select': function (a, b, c) {
                RefreshDataFilter_panatajasarwi_CPPT();
            }
        }
    });
    return mComboViewDataPJRWI_cppt;
}

function getItemPaneltgl_filter_CPPTPenataJasaRWJ(){
    var items ={
        layout: 'column',
        border: false,
        items:[
            {
                columnWidth: .35,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:[
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTglAwalFilter_CPPTPenataJasaRWJ_cppt',
                        name: 'dtpTglAwalFilter_CPPTPenataJasaRWJ_cppt',
                        value: now,
                        // value: new Date().add(Date.DAY, -30),
                        width: 100,
                        format: 'd/M/Y',
                        altFormats: 'dmy',
                        listeners:{
                            'specialkey': function (){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
                                    RefreshDataFilter_panatajasarwi_CPPT();
                                }
                            }
                        }
                    }
                ]
            },
            {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
            {
                columnWidth: .35,
                layout: 'form',
                border: false,
                labelWidth: 1,
                items:[
                    {
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilter_CPPTPenataJasaRWJ_cppt',
                        name: 'dtpTglAkhirFilter_CPPTPenataJasaRWJ_cppt',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:{
                            'specialkey': function (){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
                                    RefreshDataFilter_panatajasarwi_CPPT();
                                }
                            }
                        }
                    }
                ]
            }
        ]
    };
    return items;
}

function getItemcombo_filter_CPPTPenataJasaRWJ(){
    var items ={
        layout: 'column',
        border: false,
        items:[
            {
                columnWidth: .35,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:[ mComboStatusBayar_panatajasarwi_CPPT()]
            },
            // {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
            /*{
                columnWidth: .35,
                layout: 'form',
                border: false,
                labelWidth: 80,
                items:[
                    mCombokelas_kamar(),
                    mComboUnit_panatajasarwi()
                ]
            }*/
        ]
    };
    return items;
}
function RefreshCPPTPenataJasaRWJ_cppt(){
    var tmp_date = new Date().add(Date.DAY, -30);
    dsTRKasirRWIList_panatajasarwi_cppt.load({
        params:{
            Skip: 10,
            Take: CPPTPenataJasaRWJ.selectCount,
            // Sort: 'no_transaksi',
            Sort: '',
            // Sort: 'no_transaksi',
            Sortdir: 'ASC',
            target: 'ViewTrKasirRwj',
            param: " AND (tgl_transaksi between ~" + now.format('Y-m-d') + "~ and ~" + now.format('Y-m-d') + "~)"
        }
    });
    return dsTRKasirRWIList_panatajasarwi_cppt;
}
function RWILookUpCPPT(rowdata) {
    var lebar = 850;
    FormLookUpsdetailTRrwi_cttp = new Ext.Window({
        id: 'gridRWI',
        title: 'Catatan Perkembangan Pasien Terintrgrasi',
        closeAction: 'destroy',
        maximized:true,
        border: false,
        resizable: false,
        plain: true,
        constrain: true,
        layout: 'fit',
        modal: true,
        items: getFormEntryTR_panatajasa_cttp(lebar, rowdata),
        listeners: {
            close: function(){  
                RefreshDataFilter_panatajasarwi_CPPT();
            },
            'beforeclose': function(){clearInterval(i);}
        }
    });
    FormLookUpsdetailTRrwi_cttp.show();
    if (rowdata == undefined) {
        CPPTPenataJasaRWJ_AddNew();
    } else {
        TRPenata_jasaInit(rowdata);
    }
}

function loaddatastoredokter_CPPT_REVISI(params, dokter = null){
    // dsDataDokterPenindak_PJ_RWI.removeAll();
    DataStorefirstGridStore_PJRWI.removeAll();
    var tmp_urut;
    if (typeof params.data.URUT == 'undefined') {
        tmp_urut = params.data.URUT_MASUK;
    }else{
        tmp_urut = params.data.URUT;
    }
    Ext.Ajax.request({
        url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
        params: {
            kd_unit         : tmpkd_unitKamar_PJRWI,
            jenis_dokter    : tmpKdJob,
            kd_kasir        : '02',
            no_transaksi    : params.data.NO_TRANSAKSI,
            urut            : tmp_urut,
            tgl_transaksi   : params.data.TGL_TRANSAKSI,
            kd_job          : tmpKdJob,
            txtDokter       : dokter,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                var recs    = [],recType = dsDataDokterPenindak_PJ_RWI.recordType;
                var o       = cst['data'][i];
                recs.push(new recType(o));
                DataStorefirstGridStore_PJRWI.add(recs);
            }
        }
    });
}


function mComboStatusBayar_panatajasarwi_CPPT() {
    var cboStatus_panatajasarwi_cppt = new Ext.form.ComboBox({
        id: 'cboStatus_panatajasarwi_cppt',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        anchor: '96.8%',
        emptyText: '',
        fieldLabel: 'Status Pulang',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[1, 'Semua'], [2, 'Pulang'], [3, 'Belum Pulang']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value: CPPTPenataJasaRWJ.selectCountStatus_bayar,
        listeners: {
            select: function (a, b, c) {
                CPPTPenataJasaRWJ.selectCountStatus_bayar = b.data.displayText;
                RefreshDataFilter_panatajasarwi_CPPT();
            }
        }
    });
    return cboStatus_panatajasarwi_cppt;
}

function RefreshDataFilter_panatajasarwi_CPPT(){
    var KataKunci = '';
    loadMask.show();
    if (Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue() + '%~)';
        } else{
            KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue() + '%~)';
        }
    }
    if (Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() + '%~)';
        } else{
            KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() + '%~)';
        }
    }
    /*if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 2 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Posting'){
        KataKunci += ' and t.posting_transaksi =  ~true~';
    }else if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 3 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Posting') {
        KataKunci += ' and t.posting_transaksi =  ~false~';
    }*/
    // if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 2 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Pulang'){
    //     KataKunci += ' and ( k.tgl_keluar is not null  AND k.jam_keluar is not null) ';
    // }else if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 3 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Pulang') {
    //     KataKunci += ' and ( k.tgl_keluar is null  AND k.jam_keluar is null) ';
    // }
    /*if (Ext.get('cboStatuslunas_viKasirrwiKasir').getValue() == 'Semua'){
        if (KataKunci == ''){
            KataKunci = ' and (t.lunas = ~false~ or  t.lunas = ~true~)';
        }else{
            KataKunci += ' and (t.lunas = ~false~ or  t.lunas = ~true~)';
        };
    };*/
    /*if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Lunas'){
        if (KataKunci == ''){
            KataKunci = ' and  t.lunas = ~false~';
        }else{
            KataKunci += ' and t.lunas =  ~false~';
        };
    };*/
    if (Ext.getCmp('dtpTglAwalFilter_CPPTPenataJasaRWJ_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilter_CPPTPenataJasaRWJ_cppt').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilter_CPPTPenataJasaRWJ_cppt').getValue() + "~)";
        } else{

            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilter_CPPTPenataJasaRWJ_cppt').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilter_CPPTPenataJasaRWJ_cppt').getValue() + "~)";
        }
    }
    if (KataKunci != undefined || KataKunci != ''){
        dsTRKasirRWIList_panatajasarwi_cppt.load({
            params:{
                Skip: Ext.getCmp('mComboViewDataPJRWI_cppt').getValue(),
                Take:  CPPTPenataJasaRWJ.selectCount,
                //Sort: 'no_transaksi',
                Sort: 'tgl_transaksi',
                //Sort: 'no_transaksi',
                Sortdir: 'DESC',
                // target: 'ViewKasirRWI',
                target: 'ViewTrKasirRwj',
                param: KataKunci
            },
            callback: function(){
                loadMask.hide();
            }
        });
    } else{
        dsTRKasirRWIList_panatajasarwi_cppt.load({
            params:{
                Skip: Ext.getCmp('mComboViewDataPJRWI_cppt').getValue(),
                Take:  CPPTPenataJasaRWJ.selectCount,
                //Sort: 'no_transaksi',
                Sort: '',
                //Sort: 'no_transaksi',
                Sortdir: 'DESC',
                // target: 'ViewKasirRWI',
                target: 'ViewTrKasirRwj',
                param: KataKunci
            },
            callback: function(){
                loadMask.hide();
            }
        });
    }
    return dsTRKasirRWIList_panatajasarwi_cppt;
}


function AddNewCPPT_RWI(){
  var x=true;
    if (x === true){
        var p = RecordBaru_CTTP_RWI();
        dataSource_viCPPT_RWI.insert(dataSource_viCPPT_RWI.getCount(), p);
        var row =dataSource_viCPPT_RWI.getCount()-1;
        CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT.startEditing(row, 3);
        lineCttpRWI=row;
    };
    
};

function RecordBaru_CTTP_RWI(){
    var p = new CPPTPenataJasaRWJ.mRecordRwiCTTP({
                        'waktu': now.format("Y-m-d H:i:s"),
                        'bagian': '',
                        'hasil': '',
                        'verifikasi': '',
                        'instruksi': '',
                    });
    return p;
};
function getFormEntryTR_panatajasa_cttp(lebar, data) {
    var pnlTR_panatajasarwi_CPPT = new Ext.FormPanel({
        id: 'PanelTR_CPPT_RWI',
        layout: 'fit',
        bodyStyle: 'padding:4px;',
        border: false,
        items: [GetDTGrid_PJRWI_CPPT(lebar)], // langsung gridnya
        tbar: [

            {
                xtype:'button',
                text: 'Tambah Baris',
                iconCls: 'add',
                arrowAlign:'right',
                handler: function(){
                    AddNewCPPT_RWI()
                }
            },
            {
                xtype:'button',
                text: 'Simpan',
                iconCls: 'save',
                arrowAlign:'right',
                handler: function(){
                    // DISINI
                    var tmp_data_grid = [];
                    var data_grid = CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT;
                    // console.log(CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT);
                    if (data_grid.store.data.length > 0) {
                        for (var i = 0; i < data_grid.store.data.length; i++) {
                            var data = {};
                            if (data_grid.store.data.items[i].data.verifikasi != "" ) {
                                data.waktu      = data_grid.store.data.items[i].data.waktu;
                                data.bagian     = data_grid.store.data.items[i].data.bagian;
                                data.hasil      = data_grid.store.data.items[i].data.hasil;
                                data.instruksi  = data_grid.store.data.items[i].data.instruksi;
                                data.verifikasi = data_grid.store.data.items[i].data.verifikasi;
                                data.urut       = data_grid.store.data.items[i].data.urut;
                                tmp_data_grid.push(data);
                            }
                        }
                    }
                    console.log(tmp_data_grid);
                    loadMask.show();
                    console.log(rowSelectedPJRWI_CPPT);
                    Ext.Ajax.request({
                        url: baseURL + "index.php/rawat_jalan/function_CPPT_RWJ/insert",
                        params: {
                              kd_pasien     : rowSelectedPJRWI_CPPT.data.KD_PASIEN,
                              kd_unit       : rowSelectedPJRWI_CPPT.data.KD_UNIT,
                              tgl_masuk     : rowSelectedPJRWI_CPPT.data.TANGGAL_TRANSAKSI,
                              urut_masuk    : rowSelectedPJRWI_CPPT.data.URUT_MASUK,
                              data_grid     : JSON.stringify(tmp_data_grid),
                        },
                        failure: function(o)
                        {
                            loadMask.hide();
                            ShowPesanError_CPPTPenataJasaRWJ('Hubungi Admin', 'Error');
                        },  
                        success: function(o) {
                            loadMask.hide();
                            var cst = Ext.decode(o.responseText);
                            ShowPesanInfoCPPT_RWI(cst.message,'Information');
                            dataSource_viCPPT_RWI.removeAll();
                            dataGridItem_CPPT_RWI();
                        }
                    });
                    // dataSave_viCPPT_RWI()
                }
            },
            {
                xtype:'button',
                text: 'Hapus Baris',
                iconCls: 'RemoveRow',
                arrowAlign:'right',
                handler: function(){
                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
                                if (button == 'yes'){
                                    // loadMask.show();
                                    if(rowSelected_viCPPT_RWITest != undefined) {
                                        console.log(rowSelected_viCPPT_RWITest);
                                            Ext.Ajax.request({
                                                    url: baseURL + "index.php/rawat_jalan/function_CPPT_RWJ/delete",
                                                    params: {
                                                        kd_pasien     : rowSelectedPJRWI_CPPT.data.KD_PASIEN,
                                                        kd_unit       : rowSelectedPJRWI_CPPT.data.KD_UNIT,
                                                        tgl_masuk     : rowSelectedPJRWI_CPPT.data.TANGGAL_TRANSAKSI,
                                                        urut_masuk    : rowSelectedPJRWI_CPPT.data.URUT_MASUK,
                                                        waktu         : rowSelected_viCPPT_RWITest.data.waktu,
                                                        urut          : rowSelected_viCPPT_RWITest.data.urut,
                                                    },
                                                    failure: function(o)
                                                    {
                                                        ShowPesanError_CPPTPenataJasaRWJ('Error simpan. Hubungi Admin!', 'Gagal');
                                                    },  
                                                    success: function(o) {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true) {
                                                            loadMask.hide();
                                                            ShowPesanInfoCPPT_RWI('Data Berhasil Di hapus', 'Sukses');
                                                            dataSource_viCPPT_RWI.removeAll();
                                                            dataGridItem_CPPT_RWI();
                                                            //Delete_Detail_Rad_SQL();
                                                        }
                                                        else {
                                                           ShowPesanError_CPPTPenataJasaRWJ('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                                        };
                                                    }
                                                }
                                            )
                                        }
                                }
                            });
                }
            },{
                text: 'Refresh',
                iconCls: 'refresh',
                hidden : false,
                handler: function () {
                    dataSource_viCPPT_RWI.removeAll();
                    dataGridItem_CPPT_RWI();
                }
            },{
                text: 'Cetak',
                iconCls: 'print',
                handler: function () {
                    // DISINI
                    // var url_laporan = baseURL + "index.php/fisiotherapy/lap_billing/";
                    var url_laporan    = baseURL + "index.php/laporan/lap_assesment/";
                    var params         = {
                          kd_pasien     : rowSelectedPJRWI_CPPT.data.KD_PASIEN,
                          kd_unit       : rowSelectedPJRWI_CPPT.data.KD_UNIT,
                          tgl_masuk     : rowSelectedPJRWI_CPPT.data.TANGGAL_TRANSAKSI,
                          urut_masuk    : rowSelectedPJRWI_CPPT.data.URUT_MASUK,
                    } ;
                    console.log(params);
                    var form        = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("target", "_blank");
                    form.setAttribute("action", url_laporan+"cppt_rawat_jalan");
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", "data");
                    hiddenField.setAttribute("value", Ext.encode(params));
                    form.appendChild(hiddenField);
                    document.body.appendChild(form);
                    form.submit();   
                }
            },
        ]
    });
    var x;
    var GDtabDetail_panatajasarwi_cppt = new Ext.TabPanel({
        id: 'GDtabDetail_panatajasarwi_cppt',
        activeTab: 0,
        flex:1,
        border: true,
        style:'padding: 0px 4px 4px 4px;',
        defaults: {
            autoScroll: false
        },
        items: [

        ],
        listeners: {
            tabchange: function (panel, tab) {
                
            }
        }
    });
    var FormDepanCttpRWI = new Ext.Panel({
        id: 'FormDepanCttpRWI',
        layout: {
            type:'vbox',
            align:'stretch'
        },
        border: true,
        bodyStyle: 'background:#FFFFFF;',
        items: [pnlTR_panatajasarwi_CPPT, GDtabDetail_panatajasarwi_cppt]
    });
    return FormDepanCttpRWI;
}

function GetDTGrid_PJRWI_CPPT(){
    dataGridItem_CPPT_RWI();
    var fm = Ext.form;
    var fldDetailCPPT_RWI = ['urut', 'kd_pasien', 'tgl','bagian' ,'jam', 'bagian', 'hasil', 'verifikasi', 'waktu'];
     dataSource_viCPPT_RWI = new WebApp.DataStore
    ({
        fields: fldDetailCPPT_RWI
    });
    CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT = new Ext.grid.EditorGridPanel({
        title: '',
        flex:1,
        id:'CPPT_RWI_GRID',
        style:'padding-top: 4px;',
        stripeRows: true,
        store: dataSource_viCPPT_RWI,
        border: true,
        height:1000,
        columnLines: true,
        autoScroll: true,
        selModel: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec){
                            lineCttpRWI =row;
                            console.log(lineCttpRWI);
                            rowSelected_viCPPT_RWITest = undefined;
                            rowSelected_viCPPT_RWITest = dataSource_viCPPT_RWI.getAt(row);
                            CurrentData_viCPPT_RWITest;
                            CurrentData_viCPPT_RWITest.row = row;
                            CurrentData_viCPPT_RWITest.data = rowSelected_viCPPT_RWITest.data;
                            console.log(rowSelected_viCPPT_RWITest);
                            
                        }
                    }
                }
            ),
        cm: new Ext.grid.ColumnModel([
            {
                header: 'URUT',
                dataIndex: 'urut',
                width: 10,
                menuDisabled: true,
                hidden: true
            },{
                header: 'KD PASIEN',
                dataIndex: 'Kd_pasien',
                sortable: false,
                hidden: true,
                menuDisabled: true,
                width: 10

            },{
                header: 'WAKTU',
                dataIndex: 'waktu',
                sortable: false,
                align: 'left',
                hidden: false,
                menuDisabled: true,
                width:1,
                editor: new Ext.form.TextField({
                    allowBlank: true,
                    enableKeyEvents:true,
                })
            },{
                header: 'BAGIAN/PROFESI',
                dataIndex: 'bagian',
                sortable: false,
                hidden: false,
                align: 'left',
                menuDisabled: true,
                width: 20,
                editor: CPPTComboBoxKdDokter= Nci.form.Combobox.autoComplete({
                    store   : CPPTPenataJasaRWJ.DataStoreDokter,
                    select  : function(a,b,c){
                        console.log( dataSource_viCPPT_RWI.data.items);
                        dataSource_viCPPT_RWI.data.items[lineCttpRWI].data.bagian = b.data.data;
                        Ext.getCmp('CPPT_RWI_GRID').getView().refresh();
                        CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT.startEditing(lineCttpRWI, 4);
                       
                    },
                    param   : function(){
                        var params={
                            jenis_dokter : null,
                        };
                        return params;
                    },
                    insert  : function(o){
                        return {
                            text            :  '<table style="font-size: 13px;"><tr><td width="200">'+o.nama+'</td></tr></table>',
                            data            : o.nama,
                        }
                    },
                    url     : baseURL + "index.php/rawat_jalan/function_CPPT_RWJ/getProfesi",
                    valueField: 'nama',
                    displayField: 'text',
                })
            },{
                header: 'HASIL PEMERIKSAAN',
                dataIndex: 'hasil',
                sortable: false,
                align: 'left',
                hidden: false,
                menuDisabled: true,
                width: 40,
                editor: new Ext.form.TextArea({
                    allowBlank: true,
                    enableKeyEvents:true,
                })
            },{
                header: 'INSTRUKSI PPA',
                dataIndex: 'instruksi',
                sortable: false,
                align: 'left',
                hidden: false,
                menuDisabled: true,
                width: 40,
                editor: new Ext.form.TextArea({
                    allowBlank: true,
                    enableKeyEvents:true,
                })
            },{
                header: 'VERIFIKASI',
                dataIndex: 'verifikasi',
                align: 'left',
                width: 40,
                editor: CPPTComboBoxKdDokter= Nci.form.Combobox.autoComplete({
                            store   : CPPTPenataJasaRWJ.DataStoreDokter,
                            select  : function(a,b,c){
                                
                                dataSource_viCPPT_RWI.data.items[lineCttpRWI].data.verifikasi = b.data.data;
                                Ext.getCmp('CPPT_RWI_GRID').getView().refresh();
                                AddNewCPPT_RWI();
                                // dataSave_viCPPT_RWI_enter();
                               
                            },
                            param   : function(){
                                var params={
                                    jenis_dokter : 1,
                                };
                                return params;
                            },
                            insert  : function(o){
                                return {
                                    text            :  '<table style="font-size: 13px;"><tr><td width="200">'+o.nama+'</td></tr></table>',
                                    data            :o.nama,
                                }
                            },
                            url     : baseURL + "index.php/rawat_jalan/function_CPPT_RWJ/getProfesi",
                            valueField: 'nama',
                            displayField: 'text',
                        })
            }
        ]),
        viewConfig: {forceFit: true}
    });
    return CPPTPenataJasaRWJ.gridhasil_lab_PJRWI_CPPT;
}

function dataGridItem_CPPT_RWI(){
    Ext.Ajax.request({
            url: baseURL + "index.php/rawat_jalan/function_CPPT_RWJ/get_data",
            params: {
                kd_pasien   : rowSelectedPJRWI_CPPT.data.KD_PASIEN,
                kd_unit     : rowSelectedPJRWI_CPPT.data.KD_UNIT,
                tgl_masuk   : rowSelectedPJRWI_CPPT.data.TANGGAL_TRANSAKSI,
                urut_masuk  : rowSelectedPJRWI_CPPT.data.URUT_MASUK,
            },
            failure: function(o){
                ShowPesanError_CPPTPenataJasaRWJ('Hubungi Admin', 'Error');
            },  
            success: function(o) {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    var recs=[],
                        recType=dataSource_viCPPT_RWI.recordType;
                        
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dataSource_viCPPT_RWI.add(recs);
                //    GridDataView_viCPPT_RWITest.getView().refresh();
                }
                else {
                    ShowPesanError_CPPTPenataJasaRWJ('Gagal membaca data CPPT', 'Error');
                }
            }
        }
        
    )   
}

function ShowPesanInfoCPPT_RWI(str, modul) {
  Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width:250
    });
};
