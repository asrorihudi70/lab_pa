var TrKasirRWJ_AG = {};
TrKasirRWJ_AG.setting = {};
TrKasirRWJ_AG.mod_id = "010104";
getSetting(
  TrKasirRWJ_AG.mod_id,
  ["KD_KASIR", "KD_KASIR_LAB", "KD_KASIR_RAD"],
  function (data) {
    TrKasirRWJ_AG.setting = data;
  }
);

console.log(TrKasirRWJ_AG.setting);

var tmp_group_dokter = 0;
var DataStoreSecondGridStore_PJRWJ = new Ext.data.JsonStore();
var fieldsDokterPenindak = [
  { name: "KD_DOKTER", mapping: "KD_DOKTER" },
  { name: "NAMA", mapping: "NAMA" },
];
var DataStorefirstGridStore_PJRWJ = new Ext.data.JsonStore();
var rowSelectedPJRWJ;
var tmpKdJob = 1;
dsDataDokterPenindak_PJ_RWJ = new WebApp.DataStore({
  fields: fieldsDokterPenindak,
});
var dataRowIndexDetail = 0;
var selectedPenjasRWJLab;
var selectedPenjasRWJRad;
var ViewGridDetailHasilLab_rwj_kd_produk;
var ViewGridDetailHasilLab_rwj_urut;
var ViewGridDetailHasilRad_rwj_kd_produk;
var ViewGridDetailHasilRad_rwj_urut;
var rowSelectedPJKasir_rad;
var rowSelectedPJKasir;
var dsrad;
var ds_cbo_aturan_racik;
var ds_cbo_aturan_pakai;
var cellCurrentTindakan;
var cellselectedrad;
var CurrentRad = {
  data: Object,
  details: Array,
  row: 0,
};
var grListTRRWJ;
var setting_tracer;
var dsLookProdukList_dokter_rad;
var dsLookProdukList_dokter_leb;

// HUDI
// 21-05-2024
// -----------------------------------
// var dsNoTransaksiLaboratoriumEntry;
// -----------------------------------

var objRWJAG = null;
var PenataJasaRJ = {};
var CurrentUrutMasuk;
var CurrentKasirRWJ = {
  data: Object,
  details: Array,
  row: 0,
};

var kode_unit_scanberkas;
var blRefreshPenataJasa = true;
/*setInterval(function(){
	if(blRefreshPenataJasa==true){
		RefreshDataFilterKasirRWJ();
	}
},20000);*/
var tgl_transaksidatanyarwj;
var kd_customerdatanyarwj;
var nowTglTransaksiGrid_poli = new Date();
var tglGridBawah_poli = nowTglTransaksiGrid_poli.format("d/M/Y");
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create([
  { name: "DESKRIPSI2", mapping: "DESKRIPSI2" },
  { name: "KD_PRODUK", mapping: "KD_PRODUK" },
  { name: "DESKRIPSI", mapping: "DESKRIPSI" },
  { name: "KD_TARIF", mapping: "KD_TARIF" },
  { name: "HARGA", mapping: "HARGA" },
  { name: "QTY", mapping: "QTY" },
  { name: "TGL_TRANSAKSI", mapping: "TGL_TRANSAKSI" },
  { name: "DESC_REQ", mapping: "DESC_REQ" },
  { name: "URUT", mapping: "URUT" },
]);

var CurrentDiagnosa = {
  data: Object,
  details: Array,
  row: 0,
};
var CurrentDiagnosaIcd9 = {
  data: Object,
  details: Array,
  row: 0,
};
var CurrentSelectedRiwayatKunjunanPasien = {
  data: Object,
  details: Array,
  row: 0,
};
var dsLookProdukList_rad;
var combo;
var tmpKdUnit_TrKasir = "";
var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create([
  { name: "KASUS", mapping: "KASUS" },
  { name: "KD_PENYAKIT", mapping: "KD_PENYAKIT" },
  { name: "PENYAKIT", mapping: "PENYAKIT" },
  { name: "STAT_DIAG", mapping: "STAT_DIAG" },
  { name: "TGL_TRANSAKSI", mapping: "TGL_TRANSAKSI" },
  { name: "URUT_MASUK", mapping: "URUT_MASUK" },
]);

var currentRiwayatKunjunganPasien_TglMasuk_RWJ;
var currentRiwayatKunjunganPasien_KdUnit_RWJ;
var currentRiwayatKunjunganPasien_UrutMasuk_RWJ;
var currentRiwayatKunjunganPasien_KdKasir_RWJ;
var currentRiwayatKunjunganPasien_NoTrans_RWJ;
var currentRiwayatKunjunganPasien_KdDokter_RWJ;

var setLookUpPilihDokterPenindak_RWJ;
var dsDokterPenindak_RWJ;
var setLookUpLastHistoryDiagnosa_RWJ;
var dsLastHistoryDiagnosa_RWJ;
var currentJasaDokterKdTarif_RWJ;
var currentJasaDokterKdProduk_RWJ;
var currentJasaDokterUrutDetailTransaksi_RWJ;
var currentJasaDokterHargaJP_RWJ;
var dsGridJasaDokterPenindak_RWJ;
var dsgridpilihdokterpenindak_RWJ;
var dsDataStoreGridPoduk = new Ext.data.JsonStore();
dsgridpilihdokterpenindak_RWJ = new Ext.data.ArrayStore({
  id: 0,
  fields: ["kd_dokter", "nama"],
  data: [],
});
var GridDokterTr_RWJ;

var cbounitscanberkasrwj;
var kdUnitLab_PenjasRWJ = "41";
var dsTRRiwayatKunjuganPasien;
var dsTRRiwayatDiagnosa_RWJ;
var dsTRRiwayatAnamnese_RWJ;
var dsTRRiwayatTindakan_RWJ;
var dsTRRiwayatObat_RWJ;
var dsTRRiwayatLab_RWJ;
var dsTRRiwayatRad_RWJ;
var cellSelectedriwayatkunjunganpasien;
var dsTRDetailDiagnosaList;
var dsTRDetailDiagnosaListIcd9;
var dsTRDetailAnamneseList;
var dsTRDetailEyecareList;
var dsTRDetailDentalcareList;
var dsTRDetailDentalcareList2;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var cellSelecteddeskripsiIcd9;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView = "All";
var nowTglTransaksi = new Date();
var selectCountStatusPeriksa_viKasirRwj;
var selectCountDiMohon;
var selectKlinikPoli;
var polipilihanpasien;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj = "Belum Posting";
var dsTRKasirRWJList;
var dsTRDetailKasirRWJList;
var dsPjTrans2;
var dsRwJPJRAD;
var dsCmbRwJPJDiag;
var dsCmbRwJPJIcd9;
var AddNewKasirRWJ = true;
var selectCountKasirRWJ = 50;
var currentKdKasirRWJ;

var rowSelectedKasirRWJ;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView = "All";
var nowTglTransaksi = new Date();
var KelompokPasienAddNew = true;
var anamnese;
var CurrentAnamnesese;
var vkode_customer;
var CurrentKasirRWJSelection;
CurrentPage.page = getPanelRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
// Asep
PenataJasaRJ.dsComboObat;
PenataJasaRJ.iComboObat;
PenataJasaRJ.iComboVerifiedObat;
PenataJasaRJ.gridObat;
var gridPenataJasaTabItemTransaksiRWJ;
PenataJasaRJ.pj_req_rad;
PenataJasaRJ.pj_req__obt;
PenataJasaRJ.varkd_tarif;
PenataJasaRJ.grid2;
PenataJasaRJ.gridIcd9;
PenataJasaRJ.gridRiwayatAnamnese;
PenataJasaRJ.gridRiwayatKunjungan;
PenataJasaRJ.gridRiwayatDiagnosa;
PenataJasaRJ.gridRiwayatTindakan;
PenataJasaRJ.gridRiwayatObat;
PenataJasaRJ.gridRiwayatLab;
PenataJasaRJ.gridRiwayatRad;
PenataJasaRJ.gridhasil_lab_PJRWJ;
PenataJasaRJ.gridLastHistoryDiagnosa;
PenataJasaRJ.grid3; //untuk data grid di tab labolatorium
PenataJasaRJ.ds1;
PenataJasaRJ.ds2;
PenataJasaRJ.var_kd_dokter_leb;
PenataJasaRJ.ds3; //untuk data store grid di tab labolatorium
PenataJasaRJ.ds4 = new WebApp.DataStore({
  fields: ["kd_produk", "kd_klas", "deskripsi", "username", "kd_lab"],
});
PenataJasaRJ.ds5 = new WebApp.DataStore({ fields: ["ID_STATUS", "STATUS"] });
PenataJasaRJ.dsstatupulang = new WebApp.DataStore({
  fields: ["KD_STATUS_PULANG", "STATUS_PULANG", "STAT_MENINGGAL"],
});
PenataJasaRJ.dssebabkematian = new WebApp.DataStore({
  fields: ["kd_sebab_mati", "sebab_mati"],
});
PenataJasaRJ.ds_dokter_spesial = new WebApp.DataStore({
  fields: ["KD_DOKTER", "NAMA"],
});
PenataJasaRJ.dsGridObat;
PenataJasaRJ.dsGridTindakan;
PenataJasaRJ.s1;
PenataJasaRJ.btn1;
PenataJasaRJ.dshasilLabRWJ;
PenataJasaRJ.form = {};
PenataJasaRJ.func = {};
PenataJasaRJ.panelradiodetail_hasil;
PenataJasaRJ.form.Checkbox = {};
PenataJasaRJ.form.Class = {};
PenataJasaRJ.form.ComboBox = {};
PenataJasaRJ.form.DataStore = {};
PenataJasaRJ.form.Grid = {};
PenataJasaRJ.form.Group = {};
PenataJasaRJ.form.Group.print = {};
PenataJasaRJ.form.Window = {};
PenataJasaRJ.var_kd_dokter_rad;
PenataJasaRJ.var_id_mrresep;
PenataJasaRJ.form.DataStore.kdpenyakit = new Ext.data.ArrayStore({
  id: 0,
  fields: ["text", "kd_penyakit", "penyakit"],
  data: [],
});
PenataJasaRJ.form.DataStore.penyakit = new Ext.data.ArrayStore({
  id: 0,
  fields: ["text", "kd_penyakit", "penyakit"],
  data: [],
});
PenataJasaRJ.form.DataStore.produk = new Ext.data.ArrayStore({
  id: 0,
  fields: ["kd_produk", "deskripsi", "harga", "tglberlaku", "klasifikasi"],
  data: [],
});
PenataJasaRJ.form.DataStore.kdIcd9 = new Ext.data.ArrayStore({
  id: 0,
  fields: ["text", "kd_icd9", "deskripsi"],
  data: [],
});
PenataJasaRJ.form.DataStore.deskripsi = new Ext.data.ArrayStore({
  id: 0,
  fields: ["text", "kd_icd9", "deskripsi"],
  data: [],
});
PenataJasaRJ.form.DataStore.obat = new Ext.data.ArrayStore({
  id: 0,
  fields: ["kd_prd", "nama_obat", "jml_stok_apt", "kd_unit_far", "kd_milik"],
  data: [],
});
PenataJasaRJ.func.getNullLab = function () {
  // var o=PenataJasaIGD.grid3.getSelectionModel().getSelections()[0].data;
  return new PenataJasaRJ.form.Class.diagnosa({
    kd_produk: "",
    deskripsi: "",
    qty: 1,
    dokter: "",
    desc_req: "",
    tgl_berlaku: "",
    no_transaksi: "",
    urut: 0,
    desc_status: "",
    tgl_transaksi: tanggaltransaksitampung,
    kd_tarif: "",
    harga: "",
    jumlah: "",
    namadok: "",
  });
};
LoadDataUser();
PenataJasaRJ.ComboVerifiedObat = function () {
  PenataJasaRJ.iComboVerifiedObat = new Ext.form.ComboBox({
    id: Nci.getId(),
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "96.8%",
    emptyText: "",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [0, "Disetujui"],
        [1, "Tdk Disetujui"],
      ],
    }),
    valueField: "displayText",
    displayField: "displayText",
  });
  return PenataJasaRJ.iComboVerifiedObat;
};

function load_ds_aturan_racik() {}

function load_ds_aturan_pakai() {}
PenataJasaRJ.classGridObat = Ext.data.Record.create([
  { name: "kd_prd", mapping: "kd_prd" },
  { name: "nama_obat", mapping: "nama_obat" },
  { name: "jumlah", mapping: "jumlah" },
  { name: "satuan", mapping: "satuan" },
  { name: "cara_pakai", mapping: "cara_pakai" },
  { name: "kd_dokter", mapping: "kd_dokter" },
  { name: "verified", mapping: "verified" },
  { name: "racikan", mapping: "racikan" },
  { name: "order_mng", mapping: "order_mng" },
  { name: "jml_stok_apt", mapping: "jml_stok_apt" },
]);

PenataJasaRJ.classGrid3 = Ext.data.Record.create([
  { name: "kd_produk", mapping: "kd_produk" },
  { name: "kd_klas", mapping: "kd_klas" },
  { name: "deskripsi", mapping: "deskripsi" },
  { name: "username", mapping: "username" },
  { name: "kd_lab", mapping: "kd_lab" },
]);

PenataJasaRJ.form.Class.diagnosa = Ext.data.Record.create([
  { name: "KD_PENYAKIT", mapping: "KD_PENYAKIT" },
  { name: "PENYAKIT", mapping: "PENYAKIT" },
  { name: "KD_PASIEN", mapping: "KD_PASIEN" },
  { name: "URUT", mapping: "UURUTRUT" },
  { name: "URUT_MASUK", mapping: "URUT_MASUK" },
  { name: "TGL_MASUK", mapping: "TGL_MASUK" },
  { name: "KASUS", mapping: "KASUS" },
  { name: "STAT_DIAG", mapping: "STAT_DIAG" },
  { name: "NOTE", mapping: "NOTE" },
  { name: "DETAIL", mapping: "DETAIL" },
]);
PenataJasaRJ.form.Class.produk = Ext.data.Record.create([
  { name: "KD_PRODUK", mapping: "KD_PRODUK" },
  { name: "DESKRIPSI", mapping: "PDESKRIPSIENYAKIT" },
  { name: "QTY", mapping: "QTY" },
  { name: "DOKTER", mapping: "DOKTER" },
  { name: "TGL_TINDAKAN", mapping: "TGL_TINDAKAN" },
  { name: "QTY", mapping: "QTY" },
  { name: "DESC_REQ", mapping: "DESC_REQ" },
  { name: "TGL_BERLAKU", mapping: "TGL_BERLAKU" },
  { name: "NO_TRANSAKSI", mapping: "NO_TRANSAKSI" },
  { name: "URUT", mapping: "URUT" },
  { name: "DESC_STATUS", mapping: "DESC_STATUS" },
  { name: "TGL_TRANSAKSI", mapping: "TGL_TRANSAKSI" },
  { name: "KD_TARIF", mapping: "KD_TARIF" },
  { name: "HARGA", mapping: "HARGA" },
]);

PenataJasaRJ.func.getNullProduk = function () {
  return new PenataJasaRJ.form.Class.produk({
    KD_PRODUK: "",
    DESKRIPSI: "",
    QTY: 1,
    DOKTER: Ext.getCmp("txtNamaDokter").getValue(),
    TGL_TINDAKAN: "",
    QTY: 1,
    DESC_REQ: "",
    TGL_BERLAKU: "",
    NO_TRANSAKSI: "",
    URUT: "",
    DESC_STATUS: "",
    // TGL_TRANSAKSI: tgl_transaksidatanyarwj,//o.TANGGAL_TRANSAKSI,
    TGL_TRANSAKSI: nowTglTransaksiGrid_poli.format("Y-m-d"),
    KD_TARIF: "",
    HARGA: "",
    JUMLAH: "",
  });
};

PenataJasaRJ.func.getNullDiagnosa = function () {
  return new PenataJasaRJ.form.Class.diagnosa({
    KD_PENYAKIT: "",
    PENYAKIT: "",
    KD_PASIEN: "",
    URUT: 0,
    URUT_MASUK: "",
    TGL_MASUK: "",
    KASUS: "",
    STAT_DIAG: "",
    NOTE: "",
    DETAIL: 0,
  });
};

PenataJasaRJ.nullGridObat = function () {
  return new PenataJasaRJ.classGridObat({
    kd_produk: "",
    nama_obat: "",
    jumlah: 0,
    signa: "",
    satuan: "",
    cara_pakai: "",
    kd_dokter: "",
    verified: "Tidak Disetujui",
    racikan: false,
    racikan_text: "Tidak",
    order_mng: "Belum Dilayani",
    jml_stok_apt: 0,
  });
};

function hasilJumlah_rwj(qty) {
  // var line = PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
  // var kd_prd = dsPjTrans2.data.items[line].data.kd_prd;
  // var kd_milik = dsPjTrans2.data.items[line].data.kd_milik;
  // var kd_unit_far = dsPjTrans2.data.items[line].data.kd_unit_far;
  // Ext.Ajax.request({
  // url     : baseURL + "index.php/main/functionRWJ/get_stok_obat",
  // params  :  {
  // kd_prd      : kd_prd,
  // kd_milik    : kd_milik,
  // kd_unit_far : kd_unit_far,
  // },
  // success:function(o){
  // var cst = Ext.decode(o.responseText);
  // if (cst.success==true)
  // {
  // console.log(qty,cst.stok);
  // if(qty < cst.stok ){
  // // dsPjTrans2.data.items[line].data.jml_stok_apt = cst.stok - qty;
  // PenataJasaRJ.pj_req__obt.getView().refresh();
  // PenataJasaRJ.pj_req__obt.startEditing(line, 8);
  // }else{
  // PenataJasaRJ.pj_req__obt.getView().refresh();
  // ShowPesanWarningRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
  // Ext.Msg.show({
  // title: 'Information',
  // msg: 'Jumlah obat melebihi stok yang tersedia, jumlah stok tersedia adalah '+cst.stok,
  // buttons: Ext.MessageBox.OK,
  // fn: function (btn) {
  // if (btn == 'ok')
  // {
  // PenataJasaRJ.pj_req__obt.startEditing(line, 6);
  // }
  // }
  // });
  // }
  // }else{
  // ShowPesanWarningRWJ('Error get stok obat!','Warning');
  // }
  // }
  // });

  for (var i = 0; i < dsPjTrans2.getCount(); i++) {
    var o = dsPjTrans2.getRange()[i].data;
    console.log(o);
    if (qty != undefined) {
      if (o.jumlah <= o.jml_stok_apt) {
      } else {
        // o.jumlah=o.jml_stok_apt;
        PenataJasaRJ.pj_req__obt.getView().refresh();
        //ShowPesanWarningRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
      }
    }
  }
}
PenataJasaRJ.nullGrid3 = function () {
  var tgltranstoday = new Date();
  return new PenataJasaRJ.classGrid3({
    lunas: "f",
    kd_produk: "",
    deskripsi: "",
    kd_tarif: "",
    harga: "",
    qty: "",
    tgl_berlaku: "",
    urut: "",
    tgl_transaksi: tgltranstoday.format("Y/m/d"),
    no_transaksi: "",
    kd_dokter: "",
  });
};
PenataJasaRJ.comboObat = function () {
  var $this = this;
  $this.dsComboObat = new WebApp.DataStore({
    fields: ["kd_prd", "nama_obat", "jml_stok_apt"],
  });
  $this.dsComboObat.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboObatRJPJ",
      param: vkode_customer,
    },
  });

  $this.iComboObat = new Ext.form.ComboBox({
    id: Nci.getId(),
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    store: $this.dsComboObat,
    valueField: "nama_obat",
    hideTrigger: true,
    displayField: "nama_obat",
    value: "",
    listeners: {
      select: function (a, b, c) {
        var line = $this.gridObat.getSelectionModel().selection.cell[0];
        $this.dsGridObat.getRange()[line].data.kd_prd = b.json.kd_prd;
        $this.dsGridObat.getRange()[line].data.satuan = b.json.satuan;
        $this.dsGridObat.getRange()[line].data.kd_dokter = b.json.nama;
        $this.dsGridObat.getRange()[line].data.nama_obat = b.json.nama_obat;
        $this.dsGridObat.getRange()[line].data.jml_stok_apt =
          b.json.jml_stok_apt;
        $this.gridObat.getView().refresh();
      },
    },
  });
  return $this.iComboObat;
};

function getPanelRWJ(mod_id) {
  var Field = [
    "KD_DOKTER",
    "NO_TRANSAKSI",
    "KD_UNIT",
    "KD_PASIEN",
    "NAMA",
    "NAMA_UNIT",
    "ALAMAT",
    "TANGGAL_TRANSAKSI",
    "NAMA_DOKTER",
    "KD_CUSTOMER",
    "CUSTOMER",
    "URUT_MASUK",
    "POSTING_TRANSAKSI",
    "ANAMNESE",
    "CAT_FISIK",
    "KD_KASIR",
    "ASAL_PASIEN",
    "CARA_PENERIMAAN",
    "STATUS_ANTRIAN",
    "NO_ANTRIAN",
    "UMUR",
    "PEKERJAAN",
    "JAM_JADWAL_DOKTER",
    "JAM_DITINDAK",
    "JAM_DATANG",
    "JAM_TINDAK",
    "TGL_TINDAK",
    "TEST_BUTA_WARNA",
    "TGL_LAHIR"
  ];
  dsTRKasirRWJList = new WebApp.DataStore({ fields: Field });
  PenataJasaRJ.ds1 = dsTRKasirRWJList;
  // refeshkasirrwj();
  getTotKunjunganRWJ();
  //hani 22-06-2022 //JKN
  timeInterval = setInterval(function () {
    // console.log("aaa");
    refreshAntrianPoli();
    // refreshComboUnitRWJ();
  }, 1000);
  //=======================

  /*var i = setInterval(function(){
		loadMask.hide();
		getTotKunjunganRWJ();
		loadMask.hide();
	}, 100000); */
  grListTRRWJ = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    store: dsTRKasirRWJList,
    columnLines: true,
    style: "padding:0px 4px;",
    flex: 1,
    autoScroll: false,
    border: true,
    sort: false,
    sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: function (sm, row, rec) {
          rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(row);
          CurrentUrutMasuk = rowSelectedKasirRWJ.data.URUT_MASUK;
          PenataJasaRJ.s1 = dsTRKasirRWJList.getAt(row);
          tmpKdUnit_TrKasir = rec.data.KD_UNIT;
          objRWJAG = rowSelectedKasirRWJ.data;
          console.log(rowSelectedKasirRWJ);
        },
      },
    }),
    listeners: {
      rowdblclick: function (sm, ridx, rec) {
        rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(ridx);
        PenataJasaRJ.s1 = PenataJasaRJ.ds1.getAt(ridx);
        CurrentUrutMasuk = rowSelectedKasirRWJ.data.URUT_MASUK;
        tmpKdUnit_TrKasir = rowSelectedKasirRWJ.data.KD_UNIT;
        if (rowSelectedKasirRWJ != undefined) {
          /* if(rowSelectedKasirRWJ.data.KD_DOKTER == '' || rowSelectedKasirRWJ.data.KD_DOKTER == undefined){
						LookupPilihDokterPenindak_RWJ(rowSelectedKasirRWJ.data);
					} else{
						RWJLookUp(rowSelectedKasirRWJ.data);
					} */
          /*
						PERBARUAN LOOK UP GRID
						OLEH 	: HADAD 
						TANGGAL : 2017 - 01 - 16

					 */
          if (rowSelectedKasirRWJ.data.STATUS_ANTRIAN === "Tunggu Panggilan") {
            Ext.Msg.show({
              title: "Konfirmasi",
              msg: " Apakah pasien sudah ada ?",
              buttons: Ext.MessageBox.YESNO,
              fn: function (btn) {
                if (btn == "yes") {
                  //cekTransferTindakanPenjasRWJ(true,rowSelectedKasirRWJ.data);
                  RWJLookUp(rowSelectedKasirRWJ.data);

                  updateAntrianSedangDilayani(2);
                  refreshDataDepanPenjasRWJ();
                }
              },
              icon: Ext.MessageBox.QUESTION,
            });
          } else if (
            rowSelectedKasirRWJ.data.STATUS_ANTRIAN === "Tunggu Berkas"
          ) {
            kode_unit_scanberkas = rowSelectedKasirRWJ.data.KD_UNIT;
            panelScanBerkas_RWJ(rowSelectedKasirRWJ.data.KD_PASIEN);
            //ShowPesanWarningDiagnosa("Anda belum scan berkas","Perhatian");
          } else {
            //cekTransferTindakanPenjasRWJ(true,rowSelectedKasirRWJ.data);
            RWJLookUp(rowSelectedKasirRWJ.data);
          }
        } else {
          RWJLookUp();
        }
        // get_data_pacs(rowSelectedKasirRWJ);
      },
    },
    cm: new Ext.grid.ColumnModel([
      new Ext.grid.RowNumberer({ width: 30 }),
      {
        header: "Status Posting",
        width: 50,
        sortable: false,
        //hideable	: true,
        //hidden		: false,
        //menuDisabled: true,
        dataIndex: "POSTING_TRANSAKSI",
        id: "txtposting",
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          console.log(value);
          switch (value) {
            case 1:
              metaData.css = "StatusHijau";
              break;
            case 0:
              metaData.css = "StatusMerah";
              break;
          }
          return "";
        },
      },
      {
        header: "No. Antrian",
        width: 40,
        sortable: false,
        hideable: false,
        //menuDisabled: true,
        dataIndex: "NO_ANTRIAN",
        id: "colRWNomorAntrianViewRWJ",
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        header: "Status antrian",
        width: 90,
        sortable: false,
        hideable: false,
        //menuDisabled: true,
        dataIndex: "STATUS_ANTRIAN",
        id: "colRWStatusAntrianViewRWJ",
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colReqIdViewRWJ",
        header: "No. Transaksi",
        dataIndex: "NO_TRANSAKSI",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 80,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colTglRWJViewRWJ",
        header: "Tgl Transaksi",
        dataIndex: "TANGGAL_TRANSAKSI",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 75,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return ShowDate(record.data.TANGGAL_TRANSAKSI);
        },
      },
      {
        header: "No. Medrec",
        width: 65,
        sortable: false,
        hideable: false,
        menuDisabled: true,
        dataIndex: "KD_PASIEN",
        id: "colRWJerViewRWJ",
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        header: "Pasien",
        width: 190,
        sortable: false,
        hideable: false,
        menuDisabled: true,
        dataIndex: "NAMA",
        id: "colRWJerViewRWJ",
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colLocationViewRWJ",
        header: "Alamat",
        dataIndex: "ALAMAT",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 170,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colDeptViewRWJ",
        header: "Dokter",
        dataIndex: "NAMA_DOKTER",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 150,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colImpactViewRWJ",
        header: "Unit",
        dataIndex: "NAMA_UNIT",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 90,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
      {
        id: "colCustomerViewRWJ",
        header: "Customer",
        dataIndex: "CUSTOMER",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 90,
        renderer: function (
          value,
          metaData,
          record,
          rowIndex,
          colIndex,
          store
        ) {
          if (record.data.CARA_PENERIMAAN == "0") {
            metaData.style = "background:#ffb3b3;";
          } else {
            metaData.style = "background:#ffffff;";
          }
          return value;
        },
      },
    ]),
    viewConfig: { forceFit: true },
    tbar: [
      //RefreshDataFilterKasirRWJ();
      {
        id: "btnScanBerkasRWJ",
        text: "Scan Berkas [F2]",
        disabled: false,
        tooltip: nmEditData,
        iconCls: "Edit_Tr",
        handler: function (sm, row, rec) {
          kode_unit_scanberkas = Ext.getCmp("cboUNIT_viKasirRwj").getValue();
          panelScanBerkas_RWJ();
        },
      },
      // ,{
      // id		: 'btnRefreshPoliRWJ',
      // text	: 'Refresh',
      // //disabled:true,
      // //tooltip	: nmEditData,
      // //iconCls	: 'Edit_Tr',
      // handler	: function(sm, row, rec){
      // RefreshDataFilterKasirRWJ();
      // }
      // }
    ],
  });
  gridPenataJasaTabItemTransaksiRWJ = grListTRRWJ;
  var LegendViewpenatajasa = new Ext.Panel({
    id: "LegendViewpenatajasa",
    border: true,
    style: "padding: 0px 4px 4px 4px; margin-top: -1px;",
    bodyStyle: "padding:4px",
    layout: "column",
    autoScroll: false,
    items: [
      {
        width: 25,
        layout: "form",
        border: false,
        html:
          '<img src="' +
          baseURL +
          'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>',
      },
      {
        layout: "form",
        border: false,
        html: " Posting",
      },
      {
        width: 25,
        layout: "form",
        style: { "padding-left": "4px" },
        border: false,
        html:
          '<img src="' +
          baseURL +
          'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>',
      },
      {
        layout: "form",
        style: { "padding-left": "4px" },
        border: false,
        html: " Belum posting",
      },
      {
        xtype: "displayfield",
        value: "Kunjungan hari ini : ",
        style: { "font-weight": "bold", "padding-left": "4px" },
      },
      {
        xtype: "textfield",
        id: "txtTotKunjunganHariIni_TrKasirRWJ",
        style: { "text-align": "right", "font-weight": "bold", color: "red" },
        width: 50,
        readOnly: true,
      },
    ],
  });
  var FormDepanRWJ = new Ext.Panel({
    id: mod_id,
    closable: true,
    layout: {
      type: "vbox",
      align: "stretch",
    },
    title: "Asesmen Medis",
    border: false,
    autoScroll: false,
    items: [
      {
        layout: "form",
        border: true,
        height: 230,
        bodyStyle: "padding: 4px;",
        style: "padding: 4px;",
        labelWidth: 80,
        items: [
          {
            layout: "column",
            border: false,
            items: [
              {
                layout: "form",
                border: false,
                items: [
                  {
                    xtype: "textfield",
                    fieldLabel: "No. Medrec ",
                    id: "txtFilterNomedrec",
                    width: 150,
                    listeners: {
                      specialkey: function () {
                        var tmpNoMedrec =
                          Ext.get("txtFilterNomedrec").getValue();
                        if (
                          Ext.EventObject.getKey() === 13 ||
                          Ext.EventObject.getKey() === 9 ||
                          tmpNoMedrec.length === 10
                        ) {
                          if (
                            tmpNoMedrec.length !== 0 &&
                            tmpNoMedrec.length < 10
                          ) {
                            var tmpgetNoMedrec = formatnomedrec(
                              Ext.get("txtFilterNomedrec").getValue()
                            );
                            Ext.getCmp("txtFilterNomedrec").setValue(
                              tmpgetNoMedrec
                            );
                            //var tmpkriteria = getCriteriaFilter_viDaftar();
                            RefreshDataFilterKasirRWJ();
                          } else {
                            if (tmpNoMedrec.length === 10) {
                              RefreshDataFilterKasirRWJ();
                            } else {
                              Ext.getCmp("txtFilterNomedrec").setValue("");
                              RefreshDataFilterKasirRWJ();
                            }
                          }
                        }
                      },
                    },
                  },
                ],
              },
              {
                layout: "form",
                border: false,
                bodyStyle: "padding-left: 4px;",
                items: [
                  {
                    xtype: "textfield",
                    fieldLabel: " Pasien ",
                    id: "TxtFilterGridDataView_NAMA_viKasirRwj",
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                      specialkey: function () {
                        if (Ext.EventObject.getKey() === 13) {
                          RefreshDataFilterKasirRWJ();
                        }
                      },
                    },
                  },
                ],
              },
              {
                layout: "form",
                border: false,
                bodyStyle: "padding-left: 4px;",
                items: [
                  {
                    xtype: "textfield",
                    fieldLabel: " Dokter",
                    id: "TxtFilterGridDataView_DOKTER_viKasirRwj",
                    width: 200,
                    enableKeyEvents: true,
                    listeners: {
                      specialkey: function () {
                        if (Ext.EventObject.getKey() === 13) {
                          RefreshDataFilterKasirRWJ();
                        }
                      },
                    },
                  },
                ],
              },
            ],
          },
          getItemcombo_filter(),
          getItemPaneltgl_filter(),
          getItemPanelfilterData(),
          getItemAntrian(),
        ],
      },
      grListTRRWJ,
      LegendViewpenatajasa,
    ],
  });
  return FormDepanRWJ;
}
// hani 22-06-2022 //JKN
function getItemAntrian() {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        // columnWidth: .40,
        title: "Antrian",
        style: "padding-left:4px;",
        bodyStyle: "padding:4px;",
        width: 300,
        layout: "column",
        items: [
          {
            columnWidth: 0.25,
            xtype: "textfield",
            name: "txtTotalAntrian",
            id: "txtTotalAntrian",
            readOnly: true,
            style: "font-weight:bold; font-size: 42px;text-align: center",
            // columnWidth:1,
            height: 70,
            enableKeyEvents: true,
            anchor: "100%",
            listeners: {
              specialkey: function () {
                if (Ext.EventObject.getKey() === 13) {
                  var tmpkriteria = getCriteriaFilter_viDaftar();
                  datarefresh_viDaftar(tmpkriteria);
                }
              },
            },
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "textfield",
                readOnly: true,
                name: "txtSudahPanggil",
                id: "txtSudahPanggil",
                style: "font-weight:bold; font-size: 42px;text-align: center",
                columnWidth: 1,
                height: 70,
                enableKeyEvents: true,
                anchor: "100%",
                listeners: {
                  specialkey: function () {
                    if (Ext.EventObject.getKey() === 13) {
                      var tmpkriteria = getCriteriaFilter_viDaftar();
                      datarefresh_viDaftar(tmpkriteria);
                    }
                  },
                },
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "textfield",
                name: "txtAntrianPanggil",
                id: "txtAntrianPanggil",
                style: "font-weight:bold; font-size: 42px;text-align: center",
                columnWidth: 1,
                height: 70,
                enableKeyEvents: true,
                anchor: "100%",
                listeners: {
                  specialkey: function () {
                    if (Ext.EventObject.getKey() === 13) {
                      var tmpkriteria = getCriteriaFilter_viDaftar();
                      datarefresh_viDaftar(tmpkriteria);
                    }
                  },
                },
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "textfield",
                columnWidth: 1,
                name: "txtAntrianPrioritas",
                id: "txtAntrianPrioritas",
                style: "font-weight:bold; font-size: 42px;text-align: center",
                height: 70,
                enableKeyEvents: true,
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "displayfield",
                value: "Total",
                style:
                  "font-weight:bold; font-size: 10px;text-align: center; padding-left:4px;",
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "displayfield",
                value: "Sudah",
                style:
                  "font-weight:bold; font-size: 10px;text-align: center; padding-left:4px;",
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "displayfield",
                value: "Sedang",
                style:
                  "font-weight:bold; font-size: 10px;text-align: center; padding-left:4px;",
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            border: false,
            items: [
              {
                xtype: "displayfield",
                value: "Prioritas",
                style:
                  "font-weight:bold; font-size: 10px;text-align: center; padding-left:4px;",
              },
            ],
          },
          {
            columnWidth: 0.25,
            layout: "column",
            style: "padding-top: 4px;",
            border: false,
            items: [
              {
                xtype: "button",
                text: "Ulangi",
                id: "btnAntrianUlangi",
                hidden: true,
                handler: function () {
                  var param = {
                    nomor_antri: Ext.getCmp("txtSudahPanggil").getValue(),
                    loket: Ext.getCmp("cboPilihLoket").getValue(),
                  };
                  var loket = Ext.getCmp("cboPilihLoket").getValue();
                  if (
                    loket === undefined ||
                    loket === "" ||
                    loket === "Pilih Loket..."
                  ) {
                    ShowPesanWarning_viDaftar(
                      "Pilih loket terlebih dahulu",
                      "Antrian"
                    );
                  } else {
                    $.ajax({
                      type: "POST",
                      dataType: "JSON",
                      crossDomain: true,
                      headers: {
                        "Access-Control-Allow-Origin": "*",
                        "Content-type": "application/xml",
                        "X-cons-id": cst.id,
                        "X-timestamp": cst.timestamp,
                        "X-signature": cst.signature,
                      },
                      url:
                        baseURL +
                        "index.php/rawat_jalan/functionRWJ/ulangiPanggilDisplayAntrianPoli",
                      data: param,
                      success: function (resp1) {
                        loadMask.hide();
                      },
                      error: function (jqXHR, exception) {
                        loadMask.hide();
                      },
                    });
                  }
                },
              },
              {
                xtype: "tbseparator",
              },
            ],
          },
        ],
      },
      {
        xtype: "fieldset",
        width: 190,
        labelWidth: 40,
        title: "Panggil Antrian",
        layout: "form",
        // style: 'padding:4px;',
        style: { "margin-left": "10px", "margin-top": "0px" },
        border: true,
        items: [
          mComboPilihUnitRWJ(),
          {
            xtype: "button",
            text: "Panggil",
            anchor: "100%",
            id: "btnAntrianDepan",
            handler: function () {
              var loket = Ext.getCmp("cboPilihUnit").getValue();
              var txtPrioritas = false;
              if (
                Ext.getCmp("txtAntrianPrioritas").getValue() === "0" ||
                Ext.getCmp("txtAntrianPrioritas").getValue() !== ""
              ) {
                txtPrioritas = true;
              } else {
                txtPrioritas = false;
              }
              // if (grup === undefined || grup === '' || grup === 'Pilih Grup...') {
              //     ShowPesanWarning_viDaftar('Pilih grup terlebih dahulu', 'Antrian');
              // } else
              // updatePanggilAntrian(1);
              if (
                loket === undefined ||
                loket === "" ||
                loket === "Pilih Unit..."
              ) {
                ShowPesanWarning_viDaftar(
                  "Pilih Unit terlebih dahulu",
                  "Antrian"
                );
              } else {
                var param = {
                  nomor_antri: Ext.getCmp("txtAntrianPanggil").getValue(),
                  kd_unit: Ext.getCmp("cboPilihUnit").getValue(),
                  id_display: "poli",
                  txt_prioritas: txtPrioritas,
                  sisa_antrian: sisa_antrian,
                  kodePoli: kodePoli_antrian,
                };
                // sendAntrian(param);

                $.ajax({
                  type: "POST",
                  dataType: "JSON",
                  crossDomain: true,
                  url:
                    baseURL +
                    "index.php/rawat_jalan/functionRWJ/simpanDisplayAntrianPoli",
                  data: param,
                  success: function (resp1) {
                    loadMask.hide();
                  },
                  error: function (jqXHR, exception) {
                    loadMask.hide();
                  },
                });
              }
              // }
            },
          },
          {
            xtype: "button",
            text: "Ulang",
            anchor: "100%",
            id: "btnUlangAntrianDepanJKN",
            handler: function () {
              // updateAntrol(5, kodebooking);
              var loket = Ext.getCmp("cboPilihUnit").getValue();
              // var grup = Ext.getCmp('cboPilihGrupLoket').getValue();
              console.log(Ext.getCmp("txtAntrianPrioritas").getValue());
              var txtPrioritas = false;
              if (
                Ext.getCmp("txtAntrianPrioritas").getValue() == "0" ||
                Ext.getCmp("txtAntrianPrioritas").getValue() == ""
              ) {
                txtPrioritas = false;
                var nomor_antri = Ext.getCmp("txtSudahPanggil").getValue();
              } else {
                txtPrioritas = true;
                var nomor_antri = Ext.getCmp("txtAntrianPrioritas").getValue();
              }
              console.log(txtPrioritas);
              if (
                loket === undefined ||
                loket === "" ||
                loket === "Pilih Loket..."
              ) {
                ShowPesanWarning_viDaftar(
                  "Pilih Unit terlebih dahulu",
                  "Antrian"
                );
              } else {
                var param = {
                  nomor_antri: nomor_antri,
                  kd_unit: Ext.getCmp("cboPilihUnit").getValue(),
                  id_display: "poli",
                  txt_prioritas: txtPrioritas,
                  sisa_antrian: sisa_antrian,
                  kodePoli: kodePoli_antrian,
                };
                // sendAntrian(param);

                $.ajax({
                  type: "POST",
                  dataType: "JSON",
                  crossDomain: true,
                  url:
                    baseURL +
                    "index.php/rawat_jalan/functionRWJ/ulangiPanggilDisplayAntrianPoli",
                  data: param,
                  success: function (resp1) {
                    loadMask.hide();
                  },
                  error: function (jqXHR, exception) {
                    loadMask.hide();
                  },
                });
              }
              // }
            },
          },
        ],
      },
    ],
  };
  return items;
}

function mComboPilihUnitRWJ() {
  var Field = ["KD_UNIT", "NAMA_UNIT"];
  dsPilihUnit = new WebApp.DataStore({ fields: Field });
  dsPilihUnit.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "NAMA_UNIT",
      Sortdir: "ASC",
      target: "ViewSetupUnitB",
      param: "kd_bagian=2 and type=0",
    },
  });

  console.log(dsPilihUnit.data.items[0]);
  // refreshComboUnitRWJ();
  var cboPilihUnitRWJ = new Ext.form.ComboBox({
    id: "cboPilihUnit",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih Unit...",
    fieldLabel: "Unit",
    align: "Right",
    width: 120,
    tabIndex: 58,
    store: dsPilihUnit,
    valueField: "KD_UNIT",
    displayField: "NAMA_UNIT",
    anchor: "95%",
    // value: NAMA_UNIT,
    listeners: {
      select: function (a, b) {
        // console.log(a);
        // console.log(b);
        Unit_terpilih = b.data.Unit;
        refreshAntrianPoli();
      },
    },
  });
  // Ext.getCmp('cboPilihUnit').setValue(dsPilihUnit.data.items[0].data.NAMA_UNIT);

  return cboPilihUnitRWJ;
}

function refreshAntrianPoli() {
  // console.log(Ext.getCmp('cboPilihUnit'));
  if (Ext.getCmp("cboPilihUnit") === undefined) {
    clearInterval(timeInterval);
    clearInterval();
  } else {
    var grupPoli = Ext.getCmp("cboPilihUnit").getValue();
    if (
      grupPoli === undefined ||
      grupPoli === "" ||
      grupPoli === "Pilih Loket..."
    ) {
      grupPoli = "kosong";
    } else {
      grupPoli = Ext.getCmp("cboPilihUnit").getValue();
    }
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan/functionRWJ/getAntrianPoli",
      params: { grupPoli: grupPoli },
      failure: function (o) {},
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        sisa_antrian = cst.sisa_antrian;
        kodePoli_antrian = cst.kodePoli;

        if (cst.semuanomor === "") {
          Ext.getCmp("txtTotalAntrian").setValue("0");
        } else {
          Ext.getCmp("txtTotalAntrian").setValue(cst.semuanomor);
        }
        if (cst.semuanomorterpanggil === "") {
          Ext.getCmp("txtSudahPanggil").setValue("0");
        } else {
          Ext.getCmp("txtSudahPanggil").setValue(cst.semuanomorterpanggil);
        }
        if (cst.semuanomorbelumterpanggil === "") {
          Ext.getCmp("txtAntrianPanggil").setValue("0");
        } else {
          Ext.getCmp("txtAntrianPanggil").setValue(
            cst.semuanomorbelumterpanggil
          );
        }
      },
    });
  }
}

// ==========================

function get_data_pacs(parameter) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRAD/get_data_pacs",
    params: {
      no_transaksi: parameter.data.NO_TRANSAKSI,
      kd_kasir: parameter.data.KD_KASIR,
    },
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status === true) {
        console.log(cst);
      } else {
        ShowPesanWarningRWJ(cst.message, "Gagal");
      }
    },
  });
}

function mComboStatusBayar_viKasirRwj() {
  var cboStatus_viKasirRwj = new Ext.form.ComboBox({
    id: "cboStatus_viKasirRwj",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "90%",
    emptyText: "",
    fieldLabel: "Status Posting",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [1, "Semua"],
        [2, "Posting"],
        [3, "Belum Posting"],
      ],
    }),
    valueField: "Id",
    displayField: "displayText",
    value: selectCountStatusByr_viKasirRwj,
    listeners: {
      select: function (a, b, c) {
        selectCountStatusByr_viKasirRwj = b.data.displayText;
        RefreshDataFilterKasirRWJ();
      },
    },
  });
  return cboStatus_viKasirRwj;
}
function mComboStatusPeriksa_viKasirRwj() {
  var cboStatusPeriksa_viKasirRwj = new Ext.form.ComboBox({
    id: "cboStatusPeriksa_viKasirRwj",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "99.9%",
    emptyText: "",
    fieldLabel: "Status Periksa",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [1, "Semua"],
        [2, "Sudah Periksa"],
        [3, "Belum Periksa"],
      ],
    }),
    valueField: "Id",
    displayField: "displayText",
    value: "Semua",
    listeners: {
      select: function (a, b, c) {
        selectCountStatusPeriksa_viKasirRwj = b.data.displayText;
        RefreshDataFilterKasirRWJ();
      },
    },
  });
  return cboStatusPeriksa_viKasirRwj;
}

function mComboUnit_viKasirRwj() {
  var Field = ["KD_UNIT", "NAMA_UNIT"];
  dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
  dsunit_viKasirRwj.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "NAMA_UNIT",
      Sortdir: "ASC",
      target: "ViewSetupUnitB",
      param: "kd_bagian=2 and type='0'",
    },
  });
  var cboUNIT_viKasirRwj = new Ext.form.ComboBox({
    id: "cboUNIT_viKasirRwj",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    fieldLabel: " Poli Tujuan",
    align: "Right",
    width: 100,
    anchor: "90%",
    store: dsunit_viKasirRwj,
    valueField: "NAMA_UNIT",
    displayField: "NAMA_UNIT",
    value: "All",
    listeners: {
      select: function (a, b, c) {
        RefreshDataFilterKasirRWJ();
      },
    },
  });
  return cboUNIT_viKasirRwj;
}

function RWJLookUp(rowdata_AG) {
  /*var i = setInterval(function(){
		loadMask.hide();
		RefreshDataKasirRWJDetai2(rowdata_AG);
		loadMask.hide();
	}, 400000);*/
  var lebar = 900;
  FormLookUpsdetailTRrwj = new Ext.Window({
    id: "gridRWJ",
    title: "Asesmen Medis",
    closeAction: "destroy",
    // width		: lebar,
    // height		: 600,
    maximized: true,
    border: false,
    resizable: false,
    // plain		: true,
    constrain: true,
    layout: "fit",
    // iconCls		: 'Request',
    modal: true,
    items: getFormEntryTRRWJ(lebar, rowdata_AG),
    listeners: {
      close: function () {
        blRefreshPenataJasa = true;
        RefreshDataFilterKasirRWJ();
      },
      hide: function () {
        blRefreshPenataJasa = true;
      },
      beforeclose: function () {
        clearInterval(i);
      },
    },
  });
  FormLookUpsdetailTRrwj.show();
  blRefreshPenataJasa = false;
  dsCmbRwJPJDiag.loadData([], false);
  for (var i = 0, iLen = PenataJasaRJ.ds2.getCount(); i < iLen; i++) {
    var recs = [],
      recType = dsCmbRwJPJDiag.recordType;
    var o = PenataJasaRJ.ds2.getRange()[i].data;
    recs.push(
      new recType({
        Id: o.KD_PENYAKIT,
        displayText: o.KD_PENYAKIT,
      })
    );
    dsCmbRwJPJDiag.add(recs);
  }
  PenataJasaRJ.ds4.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboLabRJPJ",
    },
  });
  // console.log(rowdata_AG);
  var o = rowdata_AG;
  var par =
    " A.kd_pasien=~" +
    o.KD_PASIEN +
    "~ AND A.kd_unit=~" +
    o.KD_UNIT +
    "~ AND tgl_masuk=~" +
    o.TANGGAL_TRANSAKSI +
    "~ AND urut_masuk=" +
    o.URUT_MASUK;
  /* PenataJasaRJ.ds3.load({
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewGridLabRJPJ',
			param	:par
		}
	}); */
  PenataJasaRJ.ds5.load({
    params: {
      Skip: 0,
      Take: 50,
      // target: 'ViewComboJenisPelayanan'
      target: "ViewMrStatusPulang",
      param: "",
      // param: "kd_bagian = ~2~",
    },
  });
  PenataJasaRJ.dsstatupulang.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboStatusPulang",
      param: "kd_bagian = ~2~",
    },
  });
  PenataJasaRJ.dssebabkematian.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboSebabKematian",
    },
  });
  PenataJasaRJ.ds_dokter_spesial.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboDokterSpesial",
    },
  });
  var o = rowdata_AG;
  var params = {
    kd_pasien: o.KD_PASIEN,
    kd_unit: o.KD_UNIT,
    tgl_masuk: o.TANGGAL_TRANSAKSI,
    urut_masuk: o.URUT_MASUK,
  };
  /*Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/getTindakan",
		params		: params,
		failure		: function(o){
			ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(cst.echo.id_status >=0){
					//alert(cst.echo.id_status);
					PenataJasaRJ.iCombo1.selectedIndex=cst.echo.id_status;
					PenataJasaRJ.iCombo1.setValue(PenataJasaRJ.ds5.getRange()[(cst.echo.id_status-1)].data.status);
					PenataJasaRJ.iTArea1.setValue(cst.echo.catatan);
				}
			}else{
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
			}
		}
	});*/
  if (rowdata_AG == undefined) {
    RWJAddNew();
  } else {
    TRRWJInit(rowdata_AG);
    get_anamnese(rowdata_AG);
    if (rowdata_AG.KD_UNIT === "221") {
      get_pemeriksaanmata(rowdata_AG);
    }
  }
  RefreshDataKasirRWJDetail(rowdata_AG.NO_TRANSAKSI);
}

function mComboPoliklinik(lebar) {
  var Field = ["KD_UNIT", "NAMA_UNIT"];
  ds_Poli_viDaftar = new WebApp.DataStore({ fields: Field });
  ds_Poli_viDaftar.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "NAMA_UNIT",
      Sortdir: "ASC",
      target: "ViewSetupKonsultasi",
      //param	: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitRWJ').dom.value +'~)'
      param: "LEFT (kd_unit,1)='2'",
    },
  });
  var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
    id: "cboPoliklinikRequestEntry",
    typeAhead: true,
    triggerAction: "all",
    width: 170,
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih Poliklinik...",
    fieldLabel: "Poliklinik ",
    align: "Right",
    store: ds_Poli_viDaftar,
    valueField: "KD_UNIT",
    displayField: "NAMA_UNIT",
    anchor: "95%",
    listeners: {
      select: function (a, b, c) {
        loaddatastoredokter(b.data.KD_UNIT);
        selectKlinikPoli = b.data.KD_UNIT;
      },
    },
  });
  return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit) {
  dsDokterRequestEntry.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "nama",
      Sortdir: "ASC",
      target: "ViewComboDokter",
      param: "where dk.kd_unit=~" + kd_unit + "~",
    },
  });
}

function mComboDokterRequestEntry() {
  var Field = ["KD_DOKTER", "NAMA"];
  dsDokterRequestEntry = new WebApp.DataStore({ fields: Field });
  var cboDokterRequestEntry = new Ext.form.ComboBox({
    id: "cboDokterRequestEntry",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih Dokter...",
    labelWidth: 80,
    fieldLabel: "Dokter      ",
    align: "Right",
    store: dsDokterRequestEntry,
    valueField: "KD_DOKTER",
    displayField: "NAMA",
    anchor: "95%",
    listeners: {
      select: function (a, b, c) {
        selectDokter = b.data.KD_DOKTER;
      },
      render: function (c) {
        c.getEl().on(
          "keypress",
          function (e) {
            if (e.getKey() == 13)
              // atau
              Ext.getCmp("kelPasien").focus();
          },
          c
        );
      },
    },
  });
  return cboDokterRequestEntry;
}
PenataJasaRJ.gridrad = function (data) {
  PenataJasaRJ.panelradiodetail_hasil = new Ext.Panel({
    id: Nci.getId(),
    title: "Hasil Pemeriksaaan",
    closable: true,
    layout: "fit",
    flex: 1,
    itemCls: "blacklabel",
    bodyStyle: "padding: 4px;",
    border: true,
    items: [
      {
        xtype: "textarea",
        name: "textarea",
        id: "textareapopuphasil_pjrwj",
        readOnly: true,
      },
    ],
  });
  return PenataJasaRJ.panelradiodetail_hasil;
};
function KonsultasiLookUp(rowdata_AG, callback) {
  var lebar = 350;
  FormLookUpsdetailTRKonsultasi = new Ext.Window({
    id: "gridKonsultasi",
    title: "Konsultasi",
    closeAction: "destroy",
    width: lebar,
    height: 130,
    border: false,
    resizable: false,
    plain: true,
    constrain: true,
    layout: "fit",
    iconCls: "Request",
    modal: true,
    items: {
      layout: "hBox",
      // width:222,
      border: false,
      bodyStyle: "padding:5px 0px 5px 5px",
      defaults: { margins: "3 3 3 3" },
      anchor: "90%",

      items: [
        {
          columnWidth: 250,
          layout: "form",
          labelWidth: 80,
          border: false,
          items: [
            {
              xtype: "textfield",
              // fieldLabel:'Dokter ',
              name: "txtKdunitKonsultasi",
              id: "txtKdunitKonsultasi",
              // emptyText:nmNomorOtomatis,
              hidden: true,
              readOnly: true,
              anchor: "80%",
            },
            {
              xtype: "textfield",
              fieldLabel: "Poli Asal ",
              // hideLabel:true,
              name: "txtnamaunitKonsultasi",
              id: "txtnamaunitKonsultasi",
              readOnly: true,
              anchor: "95%",
              listeners: {},
            },
            mComboPoliklinik(lebar),

            mComboDokterRequestEntry(),
          ],
        },
        {
          columnWidth: 250,
          layout: "form",
          labelWidth: 70,
          border: false,
          items: [
            {
              xtype: "button",
              text: "Konsultasi",
              width: 70,
              style: { "margin-left": "0px", "margin-top": "0px" },
              hideLabel: true,
              id: "btnOkkonsultasi",
              handler: function () {
                Datasave_Konsultasi(false, callback);

                FormLookUpsdetailTRKonsultasi.close();
              },
            },
            {
              xtype: "tbspacer",

              height: 3,
            },
            {
              xtype: "button",
              text: "Batal",
              width: 70,
              hideLabel: true,
              id: "btnCancelkonsultasi",
              handler: function () {
                FormLookUpsdetailTRKonsultasi.close();
              },
            },
          ],
        },
      ],
    },
    listeners: {},
  });
  FormLookUpsdetailTRKonsultasi.show();
  KonsultasiAddNew();
}

function KonsultasiAddNew() {
  AddNewKasirKonsultasi = true;
  Ext.get("txtKdunitKonsultasi").dom.value = Ext.get("txtKdUnitRWJ").dom.value;
  Ext.get("txtnamaunitKonsultasi").dom.value = Ext.get("txtNamaUnit").dom.value;
}

function showSuratKeteranganKontrol() {
  /*
	
			kd_pasien 		: rowSelectedKasirRWJ.data.KD_PASIEN,
			kd_unit		 	: rowSelectedKasirRWJ.data.KD_UNIT,
			tgl_masuk		: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
			urut_masuk		: rowSelectedKasirRWJ.data.URUT_MASUK,
	 */
  showSuratKeteranganKontrol = new Ext.Window({
    title: "Surat Keterangan Kontrol",
    closeAction: "destroy",
    width: 400,
    resizable: false,
    layout: "form",
    constrain: true,
    bodyStyle: "padding: 4px; background: white;",
    modal: true,
    items: [
      {
        xtype: "datefield",
        id: "showSuratKeteranganKontrol1",
        fieldLabel: "Tgl. Rujuk",
        format: "d/m/Y",
        submitFormat: "ymd",
        value: new Date(),
      },
      {
        xtype: "datefield",
        id: "showSuratKeteranganKontrol2",
        fieldLabel: "Tgl. Kontrol",
        format: "d/m/Y",
        submitFormat: "ymd",
        value: new Date(),
      },
      {
        xtype: "fieldset",
        title: "",
        border: false,
        autoHeight: true,
        labelStyle: "display:none;",
        labelWidth: 1,
        items: [
          {
            xtype: "label",
            text: "Belum dapat dikembalikan ke fasilitas perujuk dengan alasan : ",
          },
          {
            xtype: "textarea",
            id: "showSuratKeteranganKontrol3",
            fieldLabel: "",
            anchor: "100%",
          },
          {
            xtype: "label",
            text: "Rencana tindak lanjut yang akan dilakukan pada kunjungan selanjutnya : ",
          },
          {
            xtype: "textarea",
            id: "showSuratKeteranganKontrol4",
            fieldLabel: "",
            anchor: "100%",
          },
        ],
      },
    ],
    fbar: [
      {
        text: "Cetak",
        handler: function () {
          var kembali = Ext.getCmp("showSuratKeteranganKontrol4")
            .getValue()
            .replace(/(?:\r\n|\r|\n)/g, "<br>");
          var alasan = Ext.getCmp("showSuratKeteranganKontrol3")
            .getValue()
            .replace(/(?:\r\n|\r|\n)/g, "<br>");
          window.open(
            baseURL +
              "index.php/rawat_jalan/cetak/kontrol?kd_pasien=" +
              Ext.get("txtNoMedrecDetransaksi").getValue() +
              "&kd_unit=" +
              Ext.get("txtKdUnitRWJ").getValue() +
              "&tgl_masuk=" +
              Ext.getCmp("dtpTanggalDetransaksi")
                .getValue()
                .format("Y-m-d")
                .trim() +
              "&urut_masuk=" +
              rowSelectedKasirRWJ.data.URUT_MASUK +
              "&tgl_rujuk=" +
              Ext.getCmp("showSuratKeteranganKontrol1")
                .getValue()
                .format("Y-m-d")
                .trim() +
              "&alasan=" +
              alasan +
              "&selanjutnya=" +
              kembali +
              "&tgl_kembali=" +
              Ext.getCmp("showSuratKeteranganKontrol2")
                .getValue()
                .format("Y-m-d")
                .trim()
          );
        },
      },
      {
        text: "Batal",
        handler: function () {
          showSuratKeteranganKontrol.close();
        },
      },
    ],
  });
  showSuratKeteranganKontrol.show();
  Ext.getCmp("showSuratKeteranganKontrol1").focus(true, 10);
}

function getFormEntryTRRWJ(lebar, data) {
  var pnlTRRWJ = new Ext.FormPanel({
    id: "PanelTRRWJ",
    // region		: 'north',
    layout: "fit",
    bodyStyle: "padding:4px",
    border: false,
    items: [getItemPanelInputRWJ(lebar)],
    tbar: [
      {
        text: " Konsultasi",
        id: "btnLookUpKonsultasi_viKasirIgd",
        iconCls: "Konsultasi",
        handler: function () {
          KonsultasiLookUp();
        },
      },
      {
        text: " Ganti Dokter",
        id: "btnLookUpGantiDokter_viKasirIgd",
        iconCls: "gantidok",
        handler: function () {
          GantiDokterLookUp();
        },
      },
      {
        text: "Ganti Kelompok Pasien",
        id: "btngantipasien_rwj",
        iconCls: "gantipasien",
        handler: function () {
          KelompokPasienLookUp();
        },
      },
      {
        text: "Posting Ke Kasir",
        id: "btnposting_pj_rwj",
        iconCls: "gantidok",
        handler: function () {
          setpostingtransaksi(data.NO_TRANSAKSI);
        },
      },
      {
        xtype: "splitbutton",
        text: "Cetak",
        iconCls: "print",
        id: "btnPrint_Poliklinik",
        menu: [
          {
            text: "Cetak Billing",
            iconCls: "print",
            id: "btnPrint_Billing",
            handler: function () {
              //window.open(baseURL + "index.php/main/resep/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()), "_blank");
              var url_laporan = baseURL + "index.php/laporan/lap_billing/";
              // var params={
              // 	no_transaksi 	: data.NO_TRANSAKSI,
              // 	kd_kasir 		: data.KD_KASIR,
              // };
              //console.log(params);
              //var form        = document.createElement("form");
              //form.setAttribute("method", "post");
              //form.setAttribute("target", "_blank");
              //form.setAttribute("action", url_laporan+"/preview_pdf");
              //var hiddenField = document.createElement("input");
              //hiddenField.setAttribute("type", "hidden");
              //hiddenField.setAttribute("name", "data");
              //hiddenField.setAttribute("value", Ext.encode(params));
              //form.appendChild(hiddenField);
              //document.body.appendChild(form);
              //form.submit();
              //
              GetDTLPreviewBilling(
                url_laporan + "preview_pdf",
                data.NO_TRANSAKSI,
                data.KD_KASIR
              );
            },
          },
          {
            text: "Cetak Resep",
            iconCls: "print",
            id: "CetakResep",
            handler: function () {
              //window.open(baseURL + "index.php/main/resep/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()), "_blank");
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
              };
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/resep/cetak"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
            },
          },
          {
            text: "Cetak Tindakan",
            iconCls: "print",
            id: "CetakTindakan",
            handler: function () {
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
              };
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/tindakan/cetak"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
              //window.open(baseURL + "index.php/main/tindakan/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()), "_blank");
            },
          },
          {
            text: "Cetak Radiologi",
            iconCls: "print",
            id: "CetakRadiologi",
            handler: function () {
              //window.open(baseURL + "index.php/main/functionRWJ/cetakRad/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
              console.log(dsRwJPJRAD.data.length);
              var order_rad = "";
              for (var i = 0; i < dsRwJPJRAD.data.length; i++) {
                console.log(dsRwJPJRAD.data.items[i].data.lunas);
                if (
                  dsRwJPJRAD.data.items[i].data.lunas === 0 ||
                  dsRwJPJRAD.data.items[i].data.lunas == "0"
                ) {
                  order_rad += dsRwJPJRAD.data.items[i].data.no_transaksi + ",";
                }
              }

              order_rad = order_rad.substring(0, order_rad.length - 1);
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
                notr: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
                order_rad: order_rad,
              };
              console.log(params);
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/functionRWJ/cetakRad"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
            },
          },
          {
            text: "Cetak Laboratorium",
            iconCls: "print",
            id: "CetakLab",
            handler: function () {
              //window.open(baseURL + "index.php/main/functionRWJ/cetakLab/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
              var order_lab = "";
              for (var i = 0; i < PenataJasaRJ.ds3.data.length; i++) {
                if (
                  PenataJasaRJ.ds3.data.items[i].data.lunas === 0 ||
                  PenataJasaRJ.ds3.data.items[i].data.lunas == "0"
                ) {
                  order_lab +=
                    PenataJasaRJ.ds3.data.items[i].data.no_transaksi + ",";
                }
              }

              order_lab = order_lab.substring(0, order_lab.length - 1);
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
                notr: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
                order_lab: order_lab,
              };
              console.log(params);
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/functionRWJ/cetakLab"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
            },
          },
          {
            text: "Cetak Ringkasan Medis",
            iconCls: "print",
            id: "CetakRM",
            handler: function () {
              //window.open(baseURL + "index.php/main/ringkasan/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
                notr: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
              };
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/ringkasan/cetak"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
            },
          },
          {
            text: "Cetak Kartu Status",
            iconCls: "print",
            id: "CetakRM",
            hidden: true,
            handler: function () {
              var paramsstatuspasienpendaftaran = {
                Table: "statuspasienprinting",
                NoMedrec: Ext.get("txtNoMedrecDetransaksi").getValue(),
                NamaPasien: Ext.get("txtNamaPasienDetransaksi").getValue(),
                Alamat: "X",
                Poli: Ext.getCmp("txtKdUnitRWJ").getValue(),
                TanggalMasuk: Ext.get("dtpTanggalDetransaksi").getValue(),
                KdDokter: Ext.getCmp("txtKdDokter").getValue(),
              };
              Ext.Ajax.request({
                url: baseURL + "index.php/main/CreateDataObj",
                params: paramsstatuspasienpendaftaran,
                failure: function (o) {
                  //ShowPesanError_viDaftar('Data tidak berhasil di Cetak ' ,'Cetak Data');
                },
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  if (cst.success === true) {
                    //ShowPesanInfo_viDaftar('Status Pasien Segera di Cetak','Cetak Data');
                  } else if (cst.success === false && cst.pesan === 0) {
                    //ShowPesanWarning_viDaftar('Data tidak berhasil di Cetak '  + cst.pesan,'Cetak Data');
                  } else {
                    //ShowPesanError_viDaftar('Data tidak berhasil di Cetak '  + cst.pesan,'Cetak Data');
                  }
                },
              });
            },
          },
          {
            text: "Cetak Kartu Pasien",
            iconCls: "print",
            hidden: true,
            id: "pJasaCetakKartuPasien",
            handler: function () {
              printPJasaRWJKartuPasien();
            },
          },
          {
            text: "Cetak Surat Keterangan Sakit",
            iconCls: "print",
            id: "pJasaCetakSuratKeteranganSakit",
            handler: function () {
              FormSuratKeteranganSakitLookUp("SKS");
            },
          },
          {
            text: "Cetak Surat Keterangan Istirahat",
            iconCls: "print",
            id: "pJasaCetakSuratKeteranganIstirahat",
            handler: function () {
              FormSuratKeteranganSakitLookUp("SKI");
            },
          },
          {
            text: "Cetak Surat Keterangan Berbadan Sehat",
            iconCls: "print",
            id: "pJasaCetakSuratKeteranganBerbadanSehat",
            handler: function () {
              FormSuratKeteranganSakitLookUp("SKB");
            },
          },
          {
            text: "Cetak Surat Keterangan Kontrol",
            iconCls: "print",
            id: "pJasaCetakSuratKeteranganKontrol",
            handler: function () {
              showSuratKeteranganKontrol();
            },
          },
          {
            text: "Cetak Surat Pengantar Rawat Inap",
            iconCls: "print",
            id: "pJasaCetakSuratPengantarRawatInap",
            handler: function () {
              FormSuratPengantarRawatInap();
            },
          },
          {
            text: "Cetak Surat Rujukan",
            iconCls: "print",
            id: "pJasaCetakSuratRujukan",
            handler: function (){
              FormSuratRujukan();
              /* var params = {
                kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
                dokter: Ext.getCmp("txtNamaDokter").getValue(),
                kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
                tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
                urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
              };
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/general/surat_rujukan/cetak"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit(); */
            }
          },
          {
            text: "Resume Pelayanan Rawat Jalan",
            iconCls: "print",
            id: "pJasaCetakResumePelayananRWJ",
            handler: function () {
              var params = {
                kd_pasien: data.KD_PASIEN,
                kd_unit: data.KD_UNIT,
                tgl: getFormatTanggal(
                  Ext.getCmp("dtpTanggalDetransaksi").getValue()
                ),
              };
              var form = document.createElement("form");
              form.setAttribute("method", "post");
              form.setAttribute("target", "_blank");
              form.setAttribute(
                "action",
                baseURL + "index.php/main/pelayananrwj/cetak"
              );
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", "data");
              hiddenField.setAttribute("value", Ext.encode(params));
              form.appendChild(hiddenField);
              document.body.appendChild(form);
              form.submit();
            },
          },
        ],
      },
    ],
  });
  var x;
  console.log(data);

  /* Roni 20-10-2023 */
  var tabItems = [
    PenataJasaRJ.assesment_rwj_tab1(data),
    PenataJasaRJ.assesment_rwj_tab2(data),
    GetDTLTRAnamnesisGrid(),
    GetDTLTRDiagnosaGrid(),
    GetDTLTRRWJGrid(data),
    GetDTLTRRWJGrid_order_resep(data),
    PenataJasaRJ.getLabolatorium(data),
    GetDTLTRRadiologiGrid(data),
    PenataJasaRJ.ok_ok(data),
    PenataJasaRJ.getTindakan(data),
    PenataJasaRJ.riwayatPasien(data)
  ];
  // Tambahkan kondisi untuk Pemeriksaan Mata
  if (data.KD_UNIT === "221") {
    tabItems.splice(1, 0, GetDTLTRPemeriksaanMataGrid());
  }
  // Tambahkan kondisi untuk Pemeriksaan Gigi
  if (data.KD_UNIT === "203") {
    tabItems.splice(1, 0, GetDTLTRPemeriksaanGigiGrid());
  }
  /* =================================================================== */

  var GDtabDetailRWJ_AG = new Ext.TabPanel({
    id: "GDtabDetailRWJ_AG",
    activeTab: 0,
    style: "padding: 0px 0px 0px 4px;",
    bodyStyle: "padding: 0px;",
    // layout:'fit',
    defaults: {
      autoScroll: true,
    },
    items: tabItems,
    // items: [
    //     PenataJasaRJ.assesment_rwj_tab1(data),
    //     GetDTLTRPemeriksaanGigiGrid(),
    //     GetDTLTRPemeriksaanMataGrid(),
    //     GetDTLTRAnamnesisGrid(),
    //     GetDTLTRDiagnosaGrid(),
    //     GetDTLTRRWJGrid(data),
    //     GetDTLTRRWJGrid_order_resep(data),
    //     PenataJasaRJ.getLabolatorium(data),
    //     GetDTLTRRadiologiGrid(data),
    //     PenataJasaRJ.ok_ok(data),
    //     PenataJasaRJ.getTindakan(data),
    //     PenataJasaRJ.riwayatPasien(data)
    // ],
    tbar: [
      {
        text: "Jadwalkan Operasi",
        id: "btnsimpanop_PJ_RWJ",
        iconCls: "Konsultasi",
        handler: function () {
          var dateNow = Ext.util.Format.date(new Date(now), "Y-m-d");
          var dateOperasi = Ext.util.Format.date(
            new Date(Ext.getCmp("TglOperasi_viJdwlOperasi").getValue()),
            "Y-m-d"
          );
          if (dateOperasi == "" || dateOperasi < dateNow) {
            ShowPesanWarningRWJ(
              "Tanggal tidak bisa tanggal sebelumnya",
              "Validasi"
            );
          } else if (
            Ext.getCmp("txtJam_viJdwlOperasi").getValue() > 24 ||
            Ext.getCmp("txtMenit_viJdwlOperasi").getValue() > 59 ||
            Ext.getCmp("txtJam_viJdwlOperasi").getValue() < 0 ||
            Ext.getCmp("txtMenit_viJdwlOperasi").getValue() < 0
          ) {
            ShowPesanWarningRWJ("Ketentuan Jam salah ", "Validasi");
          } else if (
            Ext.getCmp("txtJam_viJdwlOperasi").getValue() == "" ||
            Ext.getCmp("txtJam_viJdwlOperasi").getValue() == "Jam" ||
            Ext.getCmp("txtMenit_viJdwlOperasi").getValue() == "" ||
            Ext.getCmp("txtMenit_viJdwlOperasi").getValue() == "Menit"
          ) {
            ShowPesanWarningRWJ("Harap Isi Jam dan Menit ", "Validasi");
          } else if (
            Ext.getCmp("cbo_viComboJenisTindakan_viJdwlOperasi").getValue() ==
              "" ||
            Ext.getCmp("cbo_viComboJenisTindakan_viJdwlOperasi").getValue() ==
              "Pilih Jenis Tindakan.."
          ) {
            ShowPesanWarningRWJ("Harap Isi Tindakan ", "Validasi");
          } else if (
            Ext.getCmp("cbo_viComboKamar_viJdwlOperasi").getValue() == "" ||
            Ext.getCmp("cbo_viComboKamar_viJdwlOperasi").getValue() ==
              "Pilih Jenis Tindakan.."
          ) {
            ShowPesanWarningRWJ("Harap Isi Kamar Operasi ", "Validasi");
          } else {
            Datasave_ok();
          }
        },
      },
      {
        text: "Simpan Anamnese",
        id: "btnsimpanAnamnese_PJ_RWJ",
        iconCls: "Konsultasi",
        handler: function () {
          Datasave_Anamnese(false);
          //refeshkasirrwj();
        },
      },
      {
        text: "Simpan Pemeriksaan Mata",
        id: "btnsimpanPemeriksaanMata",
        iconCls: "Konsultasi",
        handler: function () {
          Datasave_PemeriksaanMata(false);
          //refeshkasirrwj();
        },
      },
      {
        text: "Simpan Pemeriksaan Gigi",
        id: "btnsimpanPemeriksaanGigi",
        iconCls: "Konsultasi",
        handler: function () {
          Datasave_PemeriksaanGigi(false);
          //refeshkasirrwj();
        },
      },
      {
        text: "Simpan",
        id: "btnsimpanAssesment_1",
        iconCls: "save",
        hidden: true,
        handler: function () {
          CrudData("ControllerAskepRWJ");
          //refeshkasirrwj();
        },
      },
      {
        text: "Cetak",
        id: "btncetakAssesment_1",
        iconCls: "print",
        hidden: true,
        handler: function (){
            var params = {
              kd_pasien: data.KD_PASIEN,
              kd_unit: data.KD_UNIT,
              tgl: getFormatTanggal(
                Ext.getCmp("dtpTanggalDetransaksi").getValue()
              ),
            };
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("target", "_blank");
            form.setAttribute(
              "action",
              baseURL + "index.php/main/asmenawal/cetak"
            );
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "data");
            hiddenField.setAttribute("value", Ext.encode(params));
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            form.submit();
        }
      },
      {
        text: "Cetak",
        id: "btncetakAssesment_2",
        iconCls: "print",
        hidden: true,
        handler: function (){
            var params = {
              kd_pasien: data.KD_PASIEN,
              kd_unit: data.KD_UNIT,
              tgl: getFormatTanggal(
                Ext.getCmp("dtpTanggalDetransaksi").getValue()
              ),
            };
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("target", "_blank");
            form.setAttribute(
              "action",
              baseURL + "index.php/main/asmenawalKep/cetak"
            );
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "data");
            hiddenField.setAttribute("value", Ext.encode(params));
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            form.submit();
        }
      },
      {
        text: "tambah Item Pemeriksaan",
        id: "btnbarisRad_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "add",
        handler: function () {
          //TambahBarisRad();

          var records = new Array();
          records.push(new dsRwJPJRAD.recordType());
          dsRwJPJRAD.add(records);
        },
      },
      {
        text: "Simpan",
        id: "btnSimpanRad_PJ_RWJ",
        tooltip: nmSimpan,
        iconCls: "save",
        handler: function () {
          var ada = false;
          var label = "";
          for (
            var i = 0, iLen = PenataJasaRJ.pj_req_rad.store.data.length;
            i < iLen;
            i++
          ) {
            var o = PenataJasaRJ.pj_req_rad.store.data.items[i].data;
            for (
              var j = 0, jLen = PenataJasaRJ.pj_req_rad.store.data.length;
              j < jLen;
              j++
            ) {
              var p = PenataJasaRJ.pj_req_rad.store.data.items[j].data;
              if (i !== j) {
                if (
                  (p.lunas == "f" || p.lunas == undefined) &&
                  (o.lunas == "f" || o.lunas == undefined)
                ) {
                  if (p.kd_produk == o.kd_produk) {
                    if (label != "") {
                      label = label + ",";
                    }
                    label = label + p.deskripsi;
                    ada = true;
                  }
                }
              }
            }
            if (ada == true) {
              break;
            }
          }
          if (dsRwJPJRAD.getCount() == 0) {
            PenataJasaRJ.alertError(
              "Radiologi: Harap isi data Radiologi.",
              "Peringatan"
            );
          } else if (ada === true) {
            ShowPesanWarningRWJ(
              "Item " + label + " Sudah Diinput.",
              "Peringatan"
            );
          } else {
            var e = false;
            for (var i = 0, iLen = dsRwJPJRAD.getCount(); i < iLen; i++) {
              if (
                dsRwJPJRAD.getRange()[i].data.kd_produk == "" ||
                dsRwJPJRAD.getRange()[i].data.kd_produk == null
              ) {
                // PenataJasaRJ.alertError('Radiologi: Nilai normal item '+PenataJasaRJ.ds3.getRange()[i].data.deskripsi+' belum tersedia','Peringatan');
                // e=true;
                // break;
                dsRwJPJRAD.removeAt(i);
              }
            }
            if (e == false) {
              // if (PenataJasaRJ.var_kd_dokter_rad==="" || PenataJasaRJ.var_kd_dokter_rad===undefined){
              // ShowPesanWarningRWJ('harap Isi salah satu dokter pada kolom dokter', 'Gagal');
              // }else{
              Ext.Ajax.request({
                url:
                  baseURL +
                  "index.php/main/functionRADPoliklinik/savedetailrad",
                params: getParamDetailTransaksiRAD(),
                failure: function (o) {
                  ShowPesanWarningRWJ(
                    "Data Tidak berhasil disimpan hubungi admin",
                    "Gagal"
                  );
                  PenataJasaRJ.var_kd_dokter_rad = "";
                },
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  if (cst.success === true) {
                    ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");
                    //saveRujukanRad_SQL(cst.NO_TRANS);
                    var o = gridPenataJasaTabItemTransaksiRWJ
                      .getSelectionModel()
                      .getSelections()[0].data;
                    var par =
                      " A.kd_pasien=~" +
                      o.KD_PASIEN +
                      "~ AND A.kd_unit=~" +
                      o.KD_UNIT +
                      "~ AND tgl_masuk=~" +
                      o.TANGGAL_TRANSAKSI +
                      "~ AND urut_masuk=" +
                      o.URUT_MASUK;
                    //	ViewGridBawahpoliLab(o.KD_PASIEN);
                    ViewGridBawahpoliRad(
                      o.NO_TRANSAKSI,
                      Ext.getCmp("txtKdUnitRWJ").getValue(),
                      o.TANGGAL_TRANSAKSI,
                      o.URUT_MASUK
                    );
                  } else if (cst.success === false && cst.cari === false) {
                    //PenataJasaRJ.var_kd_dokter_rad="";
                    ShowPesanWarningRWJ(
                      "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya",
                      "Gagal"
                    );
                  } else {
                    //PenataJasaRJ.var_kd_dokter_rad="";
                    ShowPesanWarningRWJ(
                      "Data Tidak berhasil disimpan hubungi admin",
                      "Gagal"
                    );
                  }
                },
              });
              // }
            }
          }
        },
      },
      {
        id: "btnHpsBrsRad_PJ_RWJ",
        text: "Hapus item",
        tooltip: "Hapus Baris",
        iconCls: "RemoveRow",
        handler: function () {
          var line =
            PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];
          if (PenataJasaRJ.pj_req_rad.getSelectionModel().selection == null) {
            ShowPesanWarningRWJ(
              "Harap Pilih terlebih dahulu data labolatorium.",
              "Gagal"
            );
          } else {
            console.log(rowSelectedPJKasir_rad);
            if (rowSelectedPJKasir_rad.data.lunas == "0") {
              Ext.Msg.show({
                title: nmHapusBaris,
                msg:
                  "Anda yakin akan menghapus data kode produk" +
                  " : " +
                  dsRwJPJRAD.getRange()[line].data.kd_produk,
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                  if (btn == "yes") {
                    console.log(rowSelectedPJKasir_rad);
                    var params = {
                      no_transaksi: rowSelectedPJKasir_rad.data.no_transaksi,
                      kd_kasir: rowSelectedPJKasir_rad.data.kd_kasir,
                      kd_produk: rowSelectedPJKasir_rad.data.kd_produk,
                      urut: rowSelectedPJKasir_rad.data.urut,
                    };
                    Ext.Ajax.request({
                      url:
                        baseURL + "index.php/main/functionRWJ/deleteradiologi",
                      params: params,
                      success: function (o) {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) {
                          ShowPesanInfoRWJ(
                            "Baris berhasil di hapus",
                            "Information"
                          );
                        }
                      },
                    });

                    if (
                      dsRwJPJRAD.getRange()[line].data.no_transaksi === "" ||
                      dsRwJPJRAD.getRange()[line].data.no_transaksi ===
                        undefined ||
                      dsRwJPJRAD.getRange()[line].data.lunas == "0"
                    ) {
                      dsRwJPJRAD.removeAt(line);
                      PenataJasaRJ.pj_req_rad.getView().refresh();
                    } else {
                      ShowPesanWarningRWJ(
                        "data Tidak dapat dihapus karena sudah tersimpan didatabase",
                        "Gagal"
                      );
                    }
                  }
                },
                icon: Ext.MessageBox.QUESTION,
              });
            } else {
              ShowPesanWarningRWJ(
                "Data tidak berhasil dihapus! Transaksi telah lunas.",
                "Gagal"
              );
            }
          }
        },
      },
      (PenataJasaRJ.btn1 = new Ext.Button({
        text: "Tambah Item Pemeriksaan",
        id: "RJPJBtnAddLab_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "add",
        handler: function () {
          PenataJasaRJ.ds3.insert(
            PenataJasaRJ.ds3.getCount(),
            PenataJasaRJ.nullGrid3()
          );
        },
      })),
      (PenataJasaRJ.btn2 = new Ext.Button({
        text: "Simpan",
        id: "RJPJBtnSaveLab_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "save",
        handler: function () {
          var ada = false;
          var label = "";
          for (
            var i = 0, iLen = PenataJasaRJ.grid3.store.data.length;
            i < iLen;
            i++
          ) {
            var o = PenataJasaRJ.grid3.store.data.items[i].data;
            for (
              var j = 0, jLen = PenataJasaRJ.grid3.store.data.length;
              j < jLen;
              j++
            ) {
              var p = PenataJasaRJ.grid3.store.data.items[j].data;
              if (i !== j) {
                if (
                  (p.lunas == "f" || p.lunas == undefined) &&
                  (o.lunas == "f" || o.lunas == undefined)
                ) {
                  if (p.kd_produk == o.kd_produk) {
                    if (label != "") {
                      label = label + ",";
                    }
                    label = label + p.deskripsi;
                    ada = true;
                    break;
                  }
                }
              }
            }
            if (ada == true) {
              break;
            }
          }
          if (PenataJasaRJ.ds3.getCount() == 0) {
            PenataJasaRJ.alertError(
              "Laboratorium: Harap isi data Labolatorium.",
              "Peringatan"
            );
          } else if (ada === true) {
            ShowPesanWarningRWJ(
              "Item " + label + " Sudah Diinput",
              "Peringatan"
            );
          } else {
            var e = false;
            for (var i = 0, iLen = PenataJasaRJ.ds3.getCount(); i < iLen; i++) {
              if (
                PenataJasaRJ.ds3.getRange()[i].data.kd_produk == "" ||
                PenataJasaRJ.ds3.getRange()[i].data.kd_produk == null
              ) {
                // PenataJasaRJ.alertError('Laboratorium: Nilai normal item '+PenataJasaRJ.ds3.getRange()[i].data.deskripsi+' belum tersedia','Peringatan');
                // e=true;
                // break;
                PenataJasaRJ.ds3.removeAt(i);
              }
            }
            if (e == false && ada === false) {
              // if (PenataJasaRJ.var_kd_dokter_leb==="" || PenataJasaRJ.var_kd_dokter_leb===undefined)
              // {
              // ShowPesanWarningRWJ('harap Isi salah satu dokter pada kolom dokter', 'Gagal');
              // }else{
              Ext.Ajax.request({
                url:
                  baseURL +
                  "index.php/main/functionLABPoliklinik/savedetaillab",
                params: getParamDetailTransaksiLAB(),
                failure: function (o) {
                  ShowPesanWarningRWJ(
                    "Data Tidak berhasil disimpan hubungi admin",
                    "Gagal"
                  );
                  //PenataJasaRJ.var_kd_dokter_leb="";
                },
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  if (cst.success === true) {
                    ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");
                    //saveRujukanLab_SQL(cst.notrans);
                    var o = gridPenataJasaTabItemTransaksiRWJ
                      .getSelectionModel()
                      .getSelections()[0].data;
                    var par =
                      " A.kd_pasien=~" +
                      o.KD_PASIEN +
                      "~ AND A.kd_unit=~" +
                      o.KD_UNIT +
                      "~ AND tgl_masuk=~" +
                      o.TANGGAL_TRANSAKSI +
                      "~ AND urut_masuk=" +
                      o.URUT_MASUK;
                    ViewGridBawahpoliLab(
                      o.NO_TRANSAKSI,
                      Ext.getCmp("txtKdUnitRWJ").getValue(),
                      o.TANGGAL_TRANSAKSI,
                      o.URUT_MASUK
                    );
                  } else if (cst.success === false && cst.cari === false) {
                    //PenataJasaRJ.var_kd_dokter_leb="";
                    ShowPesanWarningRWJ(
                      "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya",
                      "Gagal"
                    );
                  } else {
                    //PenataJasaRJ.var_kd_dokter_leb="";
                    ShowPesanWarningRWJ(
                      "Data Tidak berhasil disimpan hubungi admin",
                      "Gagal"
                    );
                  }
                },
              });
              // }
            }
          }
        },
      })),
      (PenataJasaRJ.btn3 = new Ext.Button({
        text: "Hapus",
        id: "RJPJBtnDelLab_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "RemoveRow",
        handler: function () {
          var line = PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
          if (PenataJasaRJ.grid3.getSelectionModel().selection == null) {
            ShowPesanWarningRWJ(
              "Harap Pilih terlebih dahulu data labolatorium.",
              "Gagal"
            );
          } else {
            if (rowSelectedPJKasir.data.lunas == "f") {
              Ext.Msg.show({
                title: nmHapusBaris,
                msg:
                  "Anda yakin akan menghapus data kode produk" +
                  " : " +
                  PenataJasaRJ.ds3.getRange()[line].data.kd_produk,
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                  if (btn == "yes") {
                    var o = gridPenataJasaTabItemTransaksiRWJ
                      .getSelectionModel()
                      .getSelections()[0].data;
                    console.log(rowSelectedPJKasir);
                    var params = {
                      no_transaksi: rowSelectedPJKasir.data.no_transaksi,
                      kd_kasir: rowSelectedPJKasir.data.kd_kasir,
                      kd_produk: rowSelectedPJKasir.data.kd_produk,
                      urut: rowSelectedPJKasir.data.urut,
                    };
                    Ext.Ajax.request({
                      url:
                        baseURL +
                        "index.php/main/functionRWJ/deletelaboratorium",
                      params: params,
                      success: function (o) {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) {
                          ShowPesanInfoRWJ(
                            "Baris berhasil di hapus",
                            "Information"
                          );
                        }
                      },
                    });
                    if (
                      PenataJasaRJ.ds3.getRange()[line].data.no_transaksi ===
                        "" ||
                      PenataJasaRJ.ds3.getRange()[line].data.no_transaksi ===
                        undefined ||
                      PenataJasaRJ.ds3.getRange()[line].data.lunas == "f"
                    ) {
                      PenataJasaRJ.ds3.removeAt(line);
                      PenataJasaRJ.grid3.getView().refresh();
                    } else {
                      ShowPesanWarningRWJ(
                        "data Tidak dapat dihapus karena sudah tersimpan didatabase",
                        "Gagal"
                      );
                    }
                  }
                },
                icon: Ext.MessageBox.QUESTION,
              });
            } else {
              ShowPesanWarningRWJ(
                "Data tidak berhasil dihapus! Transaksi telah lunas.",
                "Gagal"
              );
            }
          }
        },
      })),
      (PenataJasaRJ.btn4 = new Ext.Button({
        text: "Simpan",
        id: "RJPJBtnSaveTin",
        tooltip: nmLookup,
        iconCls: "save",
        handler: function () {
          var o = objRWJAG;
          if (
            PenataJasaRJ.iCombo1.selectedIndex > -1 ||
            PenataJasaRJ.iTArea1.getValue() == ""
          ) {
            var params = {
              kd_pasien: o.KD_PASIEN,
              kd_unit: o.KD_UNIT,
              tgl_masuk: o.TANGGAL_TRANSAKSI,
              urut_masuk: o.URUT_MASUK,
              // id_status	: PenataJasaRJ.ds5.getRange()[PenataJasaRJ.iCombo1.selectedIndex].data.id_status,
              id_status: Ext.getCmp("iComboStatusTindakanRJPJ").getValue(),
              cara_keluar: Ext.getCmp("iComboStatusTindakanRJPJ").getValue(),
              // keadaan_akhir	: Ext.getCmp('iComboStatusPulangTindakanRJPJ').getValue(),
              // sebab_mati		: Ext.getCmp('iComboSebabMatiTindakanRJPJ').getValue(),
              keadaan_akhir: "",
              sebab_mati: "",
              // catatan			: Ext.getCmp('iTextAreaCatLabRJPJ').getValue(),
              catatan: PenataJasaRJ.iTArea1.getValue(),
            };
            Ext.Ajax.request({
              url: baseURL + "index.php/main/functionRWJ/saveTindakan",
              params: params,
              failure: function (o) {
                ShowPesanWarningRWJ(
                  "Data Tidak berhasil disimpan hubungi admin",
                  "Gagal"
                );
              },
              success: function (o) {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                  refreshDataDepanPenjasRWJ();
                  SuratRawatLookUp_rwj(
                    Ext.getCmp("iComboStatusTindakanRJPJ").getValue()
                  );
                  ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");

                  //refeshkasirrwj();
                } else {
                  ShowPesanWarningRWJ(
                    "Data Tidak berhasil disimpan hubungi admin",
                    "Gagal"
                  );
                }
              },
            });
          } else {
            ShowPesanWarningRWJ(
              "Status/Catatan tidak boleh kosong.",
              "Peringatan"
            );
          }
        },
      })),
      {
        id: "btnLookUpGetProdukRad",
        text: "Lookup Produk",
        iconCls: "Edit_Tr",
        handler: function () {
          GetPodukLookUp("RAD");
        },
      },
      {
        id: "btnLookUpGetProdukLab",
        text: "Lookup Produk",
        iconCls: "Edit_Tr",
        handler: function () {
          GetPodukLookUp("LAB");
        },
      },
      // HUDI
      // 21-05-2024
      // ----------------------------------------------------
      {
        id: "btnPreviewHasilLab",
        text: "Preview Hasil Lab",
        iconCls: "Print",
        handler: function () {
          PreviewHasilLabLookUp(objRWJAG);
          // var url_laporan = baseURL + "index.php/laporan/lap_billing/";
          /* var url_laporan = baseURL+"index.php/main/cetaklaporanRadLab/Laporan_Laboratorium_preview";
          PreviewHasilLab(
            // url_laporan + "preview_pdf",
            url_laporan,
            data.NO_TRANSAKSI,
            data.KD_KASIR
          ); */
        },
      },
      // ----------------------------------------------------
    ],
    listeners: {
      tabchange: function (panel, tab) {
        // console.log(tab);
        Ext.getCmp("btnsimpanAssesment_1").hide();
        Ext.getCmp("btncetakAssesment_1").hide();
        Ext.getCmp("btncetakAssesment_2").hide();
        if (tab.id == "tabDiagnosa") {
          dsCmbRwJPJDiag.loadData([], false);
          for (var i = 0, iLen = PenataJasaRJ.ds2.getCount(); i < iLen; i++) {
            var recs = [],
              recType = dsCmbRwJPJDiag.recordType;
            var o = PenataJasaRJ.ds2.getRange()[i].data;
            recs.push(
              new recType({
                Id: o.KD_PENYAKIT,
                displayText: o.KD_PENYAKIT,
              })
            );
            dsCmbRwJPJDiag.add(recs);
          }
          Ext.getCmp("catLainGroup").hide();
          Ext.getCmp("txtneoplasma").hide();
          Ext.getCmp("txtkecelakaan").hide();
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").show();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").show();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").show();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          PenataJasaRJ.btn4.hide();
          RefreshDataSetDiagnosa(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabTransaksi") {
          // console.log(rowSelectedKasirRWJ.data.NO_TRANSAKSI);
          RefreshDataKasirRWJDetail(rowSelectedKasirRWJ.data.NO_TRANSAKSI);
          HitungTotalTIndakanPenataJasa(dsTRDetailKasirRWJList);
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").show();
          Ext.getCmp("btnSimpanRWJ_AG").show();
          Ext.getCmp("btnLookupRWJ").show();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          Ext.get("txtKdUrutMasuk").dom.value =
            rowSelectedKasirRWJ.data.URUT_MASUK;
            Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabradiologi") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();

          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").show();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").show();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").show();
          Ext.getCmp("btnLookUpGetProdukRad").show();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabEyeCare") {
          Ext.getCmp("btnsimpanPemeriksaanMata").show();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          PenataJasaRJ.btn4.hide();
          get_pemeriksaanmata(rowSelectedKasirRWJ.data);
          console.log(rowSelectedKasirRWJ.data);
          // Ext.get('txtareaAnamnesis').dom.value         = rowSelectedKasirRWJ.data.ANAMNESE;
          Ext.get("txtareaEyeCarecatatan").dom.value =
            rowSelectedKasirRWJ.data.TEST_BUTA_WARNA;
          Ext.get("txtKdUrutMasuk").dom.value =
            rowSelectedKasirRWJ.data.URUT_MASUK;
            
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabDentalCare") {
          Ext.getCmp("btnsimpanPemeriksaanGigi").show();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          PenataJasaRJ.btn4.hide();
          console.log(rowSelectedKasirRWJ.data);
          // Ext.get('txtareaAnamnesis').dom.value         = rowSelectedKasirRWJ.data.ANAMNESE;
          Ext.get("txtareaDentalCarecatatan").dom.value =
            rowSelectedKasirRWJ.data.TEST_BUTA_WARNA;
          Ext.get("txtKdUrutMasuk").dom.value =
            rowSelectedKasirRWJ.data.URUT_MASUK;
            
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabAnamnses") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").show();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          PenataJasaRJ.btn4.hide();
          get_anamnese(rowSelectedKasirRWJ.data);
          // Ext.get('txtareaAnamnesis').dom.value         = rowSelectedKasirRWJ.data.ANAMNESE;
          Ext.get("txtareaAnamnesiscatatan").dom.value =
            rowSelectedKasirRWJ.data.CAT_FISIK;
            Ext.get("txtareaEyeCarecatatan").dom.value =
            rowSelectedKasirRWJ.data.TEST_BUTA_WARNA;
          Ext.get("txtKdUrutMasuk").dom.value =
            rowSelectedKasirRWJ.data.URUT_MASUK;
            
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabLaboratorium") {
          PenataJasaRJ.btn1.show();
          PenataJasaRJ.btn2.show();
          PenataJasaRJ.btn3.show();
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();

          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").show();

          PenataJasaRJ.btn4.hide();
          
          Ext.getCmp("btnPreviewHasilLab").show();
        } else if (tab.id == "tabjadwalop") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").show();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabTindakan") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.show();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();

          // var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
          var o = rowSelectedKasirRWJ.data;
          // console.log(o);
          var params = {
            kd_pasien: o.KD_PASIEN,
            kd_unit: o.KD_UNIT,
            tgl_masuk: o.TANGGAL_TRANSAKSI,
            urut_masuk: o.URUT_MASUK,
          };
          Ext.Ajax.request({
            url: baseURL + "index.php/main/functionRWJ/getTindakan",
            params: params,
            failure: function (o) {
              ShowPesanWarningIGD(
                "Produk/Komponen Belum terdaftar segera Hubungi Admin",
                "Gagal"
              );
            },
            success: function (o) {
              var cst = Ext.decode(o.responseText);
              if (cst.success === true) {
                // if(cst.data.id_status >=0){
                // PenataJasaIGD.iCombo1.setValue(cst.data.cara_keluar);
                Ext.getCmp("iComboStatusTindakanRJPJ").setValue(
                  cst.data.id_cara_keluar
                );
                Ext.getCmp("iComboStatusPulangTindakanRJPJ").setValue(
                  cst.data.kd_status_pulang
                );
                Ext.getCmp("iComboSebabMatiTindakanRJPJ").setValue(
                  cst.data.kd_sebab_mati
                );
                Ext.getCmp("iTextAreaCatLabRJPJ").setValue(cst.data.catatan);
                //PenataJasaIGD.iCombo1.setValue(PenataJasaIGD.ds5.getRange()[(cst.echo.id_status-1)].data.status);
                // PenataJasaIGD.iCombo2.setValue(cst.data.catatan);

                // console.log(PenataJasaIGD.iCombo2);
                // }
              } else {
                ShowPesanWarningIGD(
                  "Produk/Komponen Belum terdaftar segera Hubungi Admin",
                  "Gagal"
                );
              }
            },
          });
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "panelRiwayatAllKunjunganPasienRWJ") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabAssesment") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          Ext.getCmp("btnsimpanAssesment_1").show();
          Ext.getCmp("btncetakAssesment_1").show();
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (
          tab.id == "tabAssesment_Pengkajian_Awal_Medis_Rawat_Jalan_2"
        ) {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          Ext.getCmp("btnsimpanAssesment_1").show();
          Ext.getCmp("btncetakAssesment_2").show();
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        } else if (tab.id == "tabOrderObat") {
          Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanPemeriksaanMata").hide();
          Ext.getCmp("btnsimpanPemeriksaanGigi").hide();
          Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").hide();
          Ext.getCmp("btnsimpanop_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRWJ_AG").hide();
          Ext.getCmp("btnSimpanRWJ_AG").hide();
          Ext.getCmp("btnLookupRWJ").hide();

          PenataJasaRJ.btn1.hide();
          PenataJasaRJ.btn2.hide();
          PenataJasaRJ.btn3.hide();

          PenataJasaRJ.btn4.hide();

          Ext.getCmp("btnbarisRad_PJ_RWJ").hide();
          Ext.getCmp("btnSimpanRad_PJ_RWJ").hide();
          Ext.getCmp("btnHpsBrsRad_PJ_RWJ").hide();
          Ext.getCmp("btnLookUpGetProdukRad").hide();
          Ext.getCmp("btnLookUpGetProdukLab").hide();
          Ext.getCmp("btnsimpanAssesment_1").hide();
          Ext.getCmp("btncetakAssesment_1").hide();
          
          Ext.getCmp("btnPreviewHasilLab").hide();
        }
      },
    },
  });

  var pnlTRRWJ2 = new Ext.FormPanel({
    id: "PanelTRRWJ2",
    layout: "fit",
    flex: 1,
    border: false,
    items: [GDtabDetailRWJ_AG],
  });
  var FormDepanRWJ = new Ext.Panel({
    id: "FormDepanRWJ",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    items: [pnlTRRWJ, pnlTRRWJ2],
  });

  if (data.POSTING_TRANSAKSI == "t") {
    setdisablebutton_PJ_RWJ();
  } else {
    setenablebutton_PJ_RWJ();
  }
  return FormDepanRWJ;
}

function RecordBaruDiagnosa() {
  var p = new mRecordDiagnosa({
    KASUS: "",
    KD_PENYAKIT: "",
    PENYAKIT: "",
    STAT_DIAG: "",
    TGL_TRANSAKSI: Ext.get("dtpTanggalDetransaksi").dom.value,
    URUT_MASUK: "",
  });
  return p;
}

function HapusBarisDiagnosa() {
  if (cellSelecteddeskripsi != undefined) {
    if (
      cellSelecteddeskripsi.data.PENYAKIT != "" &&
      cellSelecteddeskripsi.data.KD_PENYAKIT != ""
    ) {
      Ext.Msg.show({
        title: nmHapusBaris,
        msg:
          "Anda yakin akan menghapus produk" +
          " : " +
          cellSelecteddeskripsi.data.PENYAKIT,
        buttons: Ext.MessageBox.YESNO,
        fn: function (btn) {
          if (btn == "yes") {
            dsCmbRwJPJDiag.removeAt(
              PenataJasaRJ.grid2.getSelectionModel().selection.cell[0]
            );
            if (
              dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data
                .URUT_MASUK === ""
            ) {
              dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
            } else {
              if (btn == "yes") {
                DataDeleteDiagnosaDetail();
              }
            }
          }
        },
        icon: Ext.MessageBox.QUESTION,
      });
    } else {
      dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
    }
  }
}

function DataDeleteDiagnosaDetail() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/DeleteDataObj",
    params: getParamDataDeleteDiagnosaDetail(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoDiagnosa("Data berhasil dihapus", "Information");
        dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        cellSelecteddeskripsi = undefined;
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        AddNewDiagnosa = false;
        //DataDeleteDiagnosaDetail_SQL();
      } else {
        ShowPesanWarningDiagnosa(nmPesanHapusError, nmHeaderHapusData);
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
      }
    },
  });
}

function getParamDataDeleteDiagnosaDetail() {
  var params = {
    Table: "ViewDiagnosa",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    TglMasuk: CurrentDiagnosa.data.data.TGL_MASUK,
    KdPenyakit: CurrentDiagnosa.data.data.KD_PENYAKIT,
    UrutMasuk: CurrentDiagnosa.data.data.URUT_MASUK,
    Urut: CurrentDiagnosa.data.data.URUT,
  };
  return params;
}

function getParamDetailTransaksiDiagnosa2() {
  var o = rowSelectedKasirRWJ.data;
  var params = {
    Table: "ViewTrDiagnosa",
    KdPasien: o.KD_PASIEN,
    KdUnit: o.KD_UNIT,
    UrutMasuk: o.URUT_MASUK,
    Tgl: o.TANGGAL_TRANSAKSI,
    JmlList: GetListCountDetailDiagnosa(),
  };
  var x = "";
  for (var i = 0; i < dsTRDetailDiagnosaList.getCount(); i++) {
    params["URUT_MASUK" + i] =
      dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
    params["KD_PENYAKIT" + i] =
      dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
    params["STAT_DIAG" + i] =
      dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
    params["KASUS" + i] = dsTRDetailDiagnosaList.data.items[i].data.KASUS;
    params["DETAIL" + i] = dsTRDetailDiagnosaList.data.items[i].data.DETAIL;
    params["NOTE" + i] = dsTRDetailDiagnosaList.data.items[i].data.NOTE;
  }
  return params;
}

function GetListCountDetailDiagnosa() {
  var x = 0;
  for (var i = 0; i < dsTRDetailDiagnosaList.getCount(); i++) {
    if (
      dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != "" ||
      dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != ""
    ) {
      x += 1;
    }
  }
  return x;
}

function getArrDetailTrDiagnosa() {
  var x = "";
  for (var i = 0; i < dsTRDetailDiagnosaList.getCount(); i++) {
    if (
      dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != "" &&
      dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != ""
    ) {
      var y = "";
      var z = "@@##$$@@";
      y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
      y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
      y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
      y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS;
      if (i === dsTRDetailDiagnosaList.getCount() - 1) {
        x += y;
      } else {
        x += y + "##[[]]##";
      }
    }
  }
  return x;
}

function getArrdetailAnamnese() {
  var x = "";
  var y = "";
  var z = "::";
  var hasil;
  for (var k = 0; k < dsTRDetailAnamneseList.getCount(); k++) {
    if (dsTRDetailAnamneseList.data.items[k].data.HASIL == null) {
      x += "";
    } else {
      hasil = dsTRDetailAnamneseList.data.items[k].data.HASIL;
      y = dsTRDetailAnamneseList.data.items[k].data.ID_KONDISI;
      y += z + hasil;
    }
    x += y + "<>";
  }
  return x;
}

function getArrdetailPemeriksaanMata() {
  var w = "";
  var x = "";
  var y = "";
  var z = "::";
  var hasil;
  for (var k = 0; k < dsTRDetailEyecareList.getCount(); k++) {
    console.log(dsTRDetailEyecareList);
    if (dsTRDetailEyecareList.data.items[k].data.HASIL_ODRE == null) {
      w += "";
    } else if (dsTRDetailEyecareList.data.items[k].data.HASIL_OSLE == null) {
      x += "";
    } else {
      hasil_odre = dsTRDetailEyecareList.data.items[k].data.HASIL_ODRE;
      hasil_osle = dsTRDetailEyecareList.data.items[k].data.HASIL_OSLE;
      y = dsTRDetailEyecareList.data.items[k].data.ID_PARAMETER;
      y += z + hasil_odre + z + hasil_osle;
    }
    w += x += y + "<>";
  }
  return w;
}

function getArrdetailPemeriksaanGigiAtas() {
  var results = [];
  var w = "";
  var x = "";
  var y = "";
  var z = "::";
  var hasil;
  for (var k = 0; k < dsTRDetailDentalcareList.getCount(); k++) {
    if (dsTRDetailDentalcareList.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKIRI == null) {
      w += "";
    } else if (dsTRDetailDentalcareList.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKANAN == null) {
      x += "";
    } else {
      hasil_gigikiri = dsTRDetailDentalcareList.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKIRI;
      hasil_gigikanan = dsTRDetailDentalcareList.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKANAN;
      y = dsTRDetailDentalcareList.data.items[k].data.ID_PARAMETER;
      y += z + hasil_gigikiri + z + hasil_gigikanan;
    }
    w += x += y + "<>";
  }
  return w;
}

function getArrdetailPemeriksaanGigiBawah() {
  var w = "";
  var x = "";
  var y = "";
  var z = "::";
  var hasil;
  for (var k = 0; k < dsTRDetailDentalcareList2.getCount(); k++) {
    if (dsTRDetailDentalcareList2.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKIRI == null) {
      w += "";
    } else if (dsTRDetailDentalcareList2.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKANAN == null) {
      x += "";
    } else {
      hasilgigikiri2 = dsTRDetailDentalcareList2.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKIRI;
      hasilgigikanan2 = dsTRDetailDentalcareList2.data.items[k].data.HASIL_GIGI_ATASBAWAHPOSISIKANAN;
      y = dsTRDetailDentalcareList2.data.items[k].data.ID_PARAMETER;
      y += z + hasilgigikiri2 + z + hasilgigikanan2;
    }
    w += x += y + "<>";
  }
  return w;
}

function Datasave_Diagnosa(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/saveDiagnosaPoliklinik",
    params: getParamDetailTransaksiDiagnosa2(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoDiagnosa("Data berhasil disimpan", "Information");
        updateStatusPeriksa();
        //Datasave_Diagnosa_SQL();
        //refreshDataDepanPenjasRWJ();
        //RefreshDataFilterKasirRWJ();
        //refeshkasirrwj();
        if (mBol === false) {
          RefreshDataSetDiagnosa(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
        }
      } else if (cst.success === false && cst.pesan === 0) {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanWarningDiagnosa(nmPesanSimpanGagal, nmHeaderSimpanData);
      } else {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanErrorDiagnosa(nmPesanSimpanError, nmHeaderSimpanData);
      }
    },
  });
}

function Datasave_Anamnese(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: getParamDetailAnamnese(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoAnamnese("Data berhasil disimpan", "Information");
        //RefreshDataFilterKasirRWJ();
        if (mBol === false) {
          RefreshDataSetAnamnese(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
          get_anamnese(rowSelectedKasirRWJ.data);
        }
      } else if (cst.success === false && cst.pesan === 0) {
        RefreshDataSetAnamnese(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanWarningRWJ(nmPesanSimpanGagal, nmHeaderSimpanData);
      } else {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanErrorRWJ(nmPesanSimpanError, nmHeaderSimpanData);
      }
    },
  });
}

// Roni 19-10-2023
function Datasave_PemeriksaanMata(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: getParamDetailPemeriksaanMata(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoAnamnese("Data berhasil disimpan", "Information");
        rowSelectedKasirRWJ.data.TEST_BUTA_WARNA = Ext.get('txtareaEyeCarecatatan').getValue();
        //RefreshDataFilterKasirRWJ();
        if (mBol === false) {
          RefreshDataPemeriksaanMata(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
          get_pemeriksaanmata(rowSelectedKasirRWJ.data);
        }
      } else if (cst.success === false && cst.pesan === 0) {
        RefreshDataPemeriksaanMata(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanWarningRWJ(nmPesanSimpanGagal, nmHeaderSimpanData);
      } else {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanErrorRWJ(nmPesanSimpanError, nmHeaderSimpanData);
      }
    },
  });
}

function get_pemeriksaanmata(rowdata) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/get_pemeriksaanmata",
    params: {
      kd_pasien: rowdata.KD_PASIEN,
      kd_unit: rowdata.KD_UNIT,
      urut_masuk: rowdata.URUT_MASUK,
      tgl_masuk: rowdata.TANGGAL_TRANSAKSI,
    },
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      console.log(cst);
      if (cst.success === true) {
        eyecare_new = cst.listData;
        Ext.getCmp("txtareaEyeCarecatatan").setValue(cst.listData);
      } else {
        ShowPesanWarningRWJ(cst.message, "Gagal");
      }
    },
  });
}

function Datasave_PemeriksaanGigi(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: getParamDetailPemeriksaanGigi(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoAnamnese("Data berhasil disimpan", "Information");
        rowSelectedKasirRWJ.data.TEST_BUTA_WARNA = Ext.get('txtareaEyeCarecatatan').getValue();
        //RefreshDataFilterKasirRWJ();
        if (mBol === false) {
          RefreshDataPemeriksaanGigi(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
          RefreshDataPemeriksaanGigi2(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
        }
      } else if (cst.success === false && cst.pesan === 0) {
        RefreshDataPemeriksaanGigi(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        RefreshDataPemeriksaanGigi2(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanWarningRWJ(nmPesanSimpanGagal, nmHeaderSimpanData);
      } else {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanErrorRWJ(nmPesanSimpanError, nmHeaderSimpanData);
      }
    },
  });
}

/* function get_pemeriksaangigi(rowdata) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/get_pemeriksaangigi",
    params: {
      kd_pasien: rowdata.KD_PASIEN,
      kd_unit: rowdata.KD_UNIT,
      urut_masuk: rowdata.URUT_MASUK,
      tgl_masuk: rowdata.TANGGAL_TRANSAKSI,
    },
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      console.log(cst);
      if (cst.success === true) {
        Dentalcare_new = cst.listData;
        Ext.getCmp("txtareaDentalCarecatatan").setValue(cst.listData);
      } else {
        ShowPesanWarningRWJ(cst.message, "Gagal");
      }
    },
  });
} */
//================================================

/* ================================================================= */

//hani 2023-07-18
function get_anamnese(rowdata) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/get_anamnese",
    params: {
      kd_pasien: rowdata.KD_PASIEN,
      kd_unit: rowdata.KD_UNIT,
      urut_masuk: rowdata.URUT_MASUK,
      tgl_masuk: rowdata.TANGGAL_TRANSAKSI,
    },
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      console.log(cst);
      if (cst.success === true) {
        anamnese_new = cst.listData;
        Ext.getCmp("txtareaAnamnesis").setValue(cst.listData);
      } else {
        ShowPesanWarningRWJ(cst.message, "Gagal");
      }
    },
  });
}
//================================================

function ShowPesanWarningDiagnosa(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.WARNING,
    width: 250,
  });
}

function ShowPesanErrorDiagnosa(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.ERROR,
    width: 250,
  });
}

function ShowPesanInfoDiagnosa(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.INFO,
    width: 250,
  });
}

function ShowPesanInfoAnamnese(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: "Data Anamnese Ada Masalah",
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.WARNING,
    width: 250,
  });
}

function ShowPesanInfoAnamnese(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: "Data Gagal Disimpan",
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.ERROR,
    width: 250,
  });
}

function ShowPesanInfoAnamnese(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: "Data Sukses diSimpan",
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.INFO,
    width: 250,
  });
}

function GetDTLTRRadiologiGrid(data) {
  var tabTransaksi = new Ext.Panel({
    title: "Order Radiologi",
    id: "tabradiologi",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: false,
    items: [GetGridRwJPJRad(data), PenataJasaRJ.gridrad(data)],
  });
  return tabTransaksi;
}
PenataJasaRJ.getLabolatorium = function (data) {
  var $this = this;
  var tabTransaksi = new Ext.Panel({
    title: "Order Laboratorium",
    id: "tabLaboratorium",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: false,
    items: [$this.getGrid3(data), GetDTGridHasilLab_PJRWJ()],
  });
  return tabTransaksi;
};
PenataJasaRJ.assesment_rwj_tab2 = function (data) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan/control_askep_rwj/getData",
    params: {
      kd_pasien: data.KD_PASIEN,
      kd_unit: data.KD_UNIT,
      tgl_masuk: data.TANGGAL_TRANSAKSI,
      urut_masuk: data.URUT_MASUK,
      group: "RWJ_ASKEP4_UMUM",
    },
    method: "GET",
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      dataSource_AsesmenRWJ4.loadData([], false);
      for (var i = 0; i < cst.length; i++) {
        dataSource_AsesmenRWJ4.add(
          new dataSource_AsesmenRWJ4.recordType(cst[i])
        );
      }
    },
  });

  var Field = [
    "kd_askep",
    "nama",
    "keterangan",
    "jenis_data",
    "satuan",
    "kd_group",
    "nilai",
    "nilai_text",
    "ada",
    "view",
    "enable_yes",
    "enable_no",
    "disable_yes",
    "disable_no",
    "enab",
    "saved",
  ];
  dataSource_AsesmenRWJ4 = new Ext.data.ArrayStore({
    fields: Field,
  });

  gridListAsesmenRWJ4 = new Ext.grid.EditorGridPanel({
    stripeRows: false,
    hideHeaders: true,
    flex: 1,
    store: dataSource_AsesmenRWJ4,
    columnLines: false,
    autoScroll: true,
    border: false,
    sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
    }),
    cm: new Ext.grid.ColumnModel([
      {
        header: "Status Posting",
        width: 300,
        sortable: false,
        hideable: true,
        // hidden:true,
        menuDisabled: true,
        dataIndex: "nama",
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
            if (b.data.kd_askep == "RWJ_ASKEP4_SKRINING_NYERI") {
              a.colspan = 3;
              a.style = "width: 1200;";
            }
          }
          if (
            (b.data.kd_askep === "RWJ_ASKEP4_TERPASANG_NGT" ||
             b.data.kd_askep === "RWJ_ASKEP4_TERPASANG_NGT_TUJUAN" ||
             b.data.kd_askep === "RWJ_ASKEP4_SKRINING_NUTRISI" ||
             b.data.kd_askep === "RWJ_ASKEP4_PENURUNANBB" ||
             b.data.kd_askep === "RWJ_ASKEP4_PENURUNANBB_KG" ||
             b.data.kd_askep === "RWJ_ASKEP4_NAFSU_MAKAN_KURANG" ||
             b.data.kd_askep === "RWJ_ASKEP4_PASIEN_DENGAN_DIAGNOSA_KHUSUS")
          ) {
            if (rowSelectedKasirRWJ.json.UMUR.YEAR < 18) {
              a.style = "display: none;";
            }
          }
          return (
            '<div style="white-space: normal;">' +
            value.replace(
              new RegExp("&nbsp;", "g"),
              "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            ) +
            "</div>"
          );
        },
      },
      {
        id: "colReqIdViewIGD",
        dataIndex: "nilai_text",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 1000,
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "RADIO") {
            if (b.data.satuan == null) {
              b.data.satuan = "";
            }
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value = "";
            ch = "";
            if (b.data.nilai == "X") {
              ch = "checked";
            }
            value =
              '<input type="radio" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="javascript:action_asessmen_radio(\'' +
              b.data.kd_askep +
              "','" +
              b.data.kd_grup +
              '\',dataSource_AsesmenRWJ4);" name="' +
              b.data.kd_grup +
              '" style="" value="' +
              i +
              '" ' +
              ch +
              " " +
              enab +
              '/><div style="float:left;margin-top: 3px;margin-left: 4px;white-space: normal;">' +
              b.data.satuan +
              "</div>";
          } else if (b.data.jenis_data == "RADIOLIST") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            var sat = b.data.satuan.split(",");
            value = "";
            ch = "";
            a.colspan = 3;
            for (var i = 0, iLen = sat.length; i < iLen; i++) {
              ch = "";
              if (i === parseInt(b.data.nilai)) {
                ch = "checked";
              }
              if (
                (b.data.kd_askep === "RWJ_ASKEP4_TERPASANG_NGT" ||
                 b.data.kd_askep === "RWJ_ASKEP4_TERPASANG_NGT_TUJUAN" ||
                 b.data.kd_askep === "RWJ_ASKEP4_PENURUNANBB" ||
                 b.data.kd_askep === "RWJ_ASKEP4_PENURUNANBB_KG" ||
                 b.data.kd_askep === "RWJ_ASKEP4_NAFSU_MAKAN_KURANG" ||
                 b.data.kd_askep === "RWJ_ASKEP4_PASIEN_DENGAN_DIAGNOSA_KHUSUS")
              ) {
                if (rowSelectedKasirRWJ.json.UMUR.YEAR >= 18) {
                  value +=
                    '<input type="radio" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_RADIOLIST(dataSource_AsesmenRWJ4.getRange()[' +
                    c +
                    "].data,dataSource_AsesmenRWJ4," +
                    i +
                    ");dataSource_AsesmenRWJ4.getRange()[" +
                    c +
                    "].set('nilai','" +
                    i +
                    "'); dataSource_AsesmenRWJ4.getRange()[" +
                    c +
                    "].set('nilai_text','" +
                    i +
                    '\');" name="' +
                    b.data.kd_askep +
                    '" style="" value="' +
                    i +
                    '" ' +
                    ch +
                    " " +
                    enab +
                    '/><div style="float:left;margin-top: 3px;margin-left: 4px;">' +
                    sat[i] +
                    "</div>";
                } else {
                }
              } else {
                value +=
                '<input type="radio" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_RADIOLIST(dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].data,dataSource_AsesmenRWJ4," +
                i +
                ");dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai','" +
                i +
                "'); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text','" +
                i +
                '\');" name="' +
                b.data.kd_askep +
                '" style="" value="' +
                i +
                '" ' +
                ch +
                " " +
                enab +
                '/><div style="float:left;margin-top: 3px;margin-left: 4px;">' +
                sat[i] +
                "</div>"; 
              }
            }
          } else if (b.data.jenis_data == "CHECK") {
            if (b.data.satuan == null) {
              b.data.satuan = "";
            }
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value = "";
            ch = "";
            if (b.data.nilai == "X") {
              ch = "checked";
            }
            value =
              '<input type="checkbox" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_check(\'' +
              b.data.kd_askep +
              '\',dataSource_AsesmenRWJ4);" name="' +
              b.data.kd_askep +
              '" style="" value="' +
              i +
              '" ' +
              ch +
              " " +
              enab +
              '/><div style="float:left;margin-top: 3px;margin-left: 4px;white-space: normal;">' +
              b.data.satuan +
              "</div>";
          } else if (b.data.jenis_data == "CHECKLIST") {
            if (b.data.satuan == null) {
              b.data.satuan = "";
            }
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            var sat = b.data.satuan.split(",");
            value = "";
            ch = "";
            if (b.data.nilai == "X") {
              ch = "checked";
            }
            for (var i = 0, iLen = sat.length; i < iLen; i++) {
              ch = "";
              if (i === parseInt(b.data.nilai)) {
                ch = "checked";
              }
              value =
                '<input type="checkbox" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_checklist(\'' +
                b.data.kd_askep +
                '\',dataSource_AsesmenRWJ4);" name="' +
                b.data.kd_askep +
                '" style="" value="' +
                i +
                '" ' +
                ch +
                " " +
                enab +
                '/><div style="float:left;margin-top: 3px;margin-left: 4px;white-space: normal;font-size:14px;">' +
                sat[i] +
                "</div>";
            }
          } else if (b.data.jenis_data == "TEXT") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            console.log(b.data.kd_askep);
            if (b.data.kd_askep == "RWJ_ASKEP4_RIWAYAT_DATANG") {
              value =
                '<input type="datetime-local" style="margin: -3px 0px;width: 200px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text',this.value);\" " +
                enab +
                "/>";
              a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_DIRAWAT_TERAKHIR_RUANG") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 130px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder="................................."/>';
            a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_ALASAN_FAST_TRACK") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 200px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder="......................................................"/>';
            a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_GCS") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 175px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder="E........V............M.........."/>';
            a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_TEKANAN_DARAH") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 175px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder=".................................mmHg"/>';
            a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_RESPIRATION_RATE" || b.data.kd_askep == "RWJ_ASKEP4_NADI") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 175px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder=".................................x/menit"/>';
            a.style = "padding: 0px;";
            } else if (b.data.kd_askep == "RWJ_ASKEP4_SUHU") {
              value =
                '<input type="text" style="margin: -3px 0px; width: 175px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai', this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text', this.value);\" " +
                enab +
                ' placeholder="................................. ◦C"/>';
            a.style = "padding: 0px;";
            } else {
              value =
                '<input type="text" style="margin: -3px 0px;width: 200px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
                c +
                "].set('nilai_text',this.value);\" " +
                enab +
                "/>";
              a.style = "padding: 0px;";
            }
          } else if (b.data.jenis_data == "TEXTLIST") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            // value='<div style="margin: -3px 0px;width: 290px;"><input type="text" style="position: absolute;width: 400px;" value="'+value+'" onkeypress="javascript: action_asessmen_TEXTLIST(dataSource_AsesmenRWJ4.getRange()['+c+'].data,dataSource_AsesmenRWJ4,this.value,event);"  '+enab+'/></div>';
            value =
              '<div style="margin: -3px 0px;width: 290px;"><input type="text" style="position: absolute;width: 400px;" value="' +
              value +
              '" onkeydown="javascript: action_asessmen_TEXTLIST(dataSource_AsesmenRWJ4.getRange()[' +
              c +
              '].data,dataSource_AsesmenRWJ4,this.value,event);"  ' +
              enab +
              "/></div>";
            // value='<div style="margin: -3px 0px;width: 290px;"><input type="text" style="position: absolute;width: 400px;" value="'+value+'" onkeydown="javascript:alert(event.key);"  '+enab+'/></div>';
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "NUMBER") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="number" style="margin: -3px 0px;width: 80px;" value="' +
              value +
              '" ' +
              'onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
              c +
              "].set('nilai_text',this.value);\"" +
              ' onchange="javascript: dataSource_AsesmenRWJ4.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
              c +
              "].set('nilai_text',this.value);\"" +
              "" +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "DATE") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="date" style="margin: -3px 0px;width: 130px;" value="' +
              value +
              '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
              c +
              "].set('nilai_text',this.value);\" " +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "TIME") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="time" style="margin: -3px 0px;width: 130px;" value="' +
              value +
              '" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
              c +
              "].set('nilai_text',this.value);\" " +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "TEXTAREA") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<div style="margin: -3px 0px;width: 290px;height: 100px;"><textarea type="text" style="width: 500px;height: 100px;position: absolute;" onblur="javascript: dataSource_AsesmenRWJ4.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ4.getRange()[" +
              c +
              "].set('nilai_text',this.value);\" " +
              enab +
              ">" +
              value +
              "</textarea></div>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
          } else if (b.data.jenis_data == "LABEL") {
            a.style = "background: #e6ef7b;";
          }
          if (
            b.data.satuan != null &&
            b.data.satuan != "" &&
            b.data.jenis_data !== "RADIOLIST" &&
            b.data.jenis_data !== "CHECK" &&
            b.data.jenis_data !== "RADIO"
          ) {
            value += "&nbsp;" + b.data.satuan + "";
            return '<div style="white-space: normal;">' + value + "</div>";
          } else {
            return '<div style="white-space: normal;">' + value + "</div>";
          }
        },
      },
      {
        width: 400,
        sortable: false,
        hideable: true,
        hidden: false,
        menuDisabled: true,
        dataIndex: "keterangan",
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
          }
          return (
            '<div style="white-space: normal;">' +
            value.replace(
              new RegExp("&nbsp;", "g"),
              "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            ) +
            "</div>"
          );
        },
      },
    ]),
  });

  var tabAssesment = new Ext.Panel({
    title: "Pengkajian Awal Medis 2",
    id: "tabAssesment_Pengkajian_Awal_Medis_Rawat_Jalan_2",
    layout: "column",
    autoScroll: true,
    layout: "fit",
    border: false,
    items: [gridListAsesmenRWJ4],
  });
  gridListAsesmenRWJ4.getSelectionModel().lock();
  return tabAssesment;
};

PenataJasaRJ.assesment_rwj_tab1 = function (data) {
  console.log(data.KD_UNIT);
  if (data.KD_UNIT == "221") {
    var headerAsmenAwal =
      "<b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Mata</i>)</b>";
  } else if (data.KD_UNIT == "203"){
    var headerAsmenAwal =
    "<b>ASESMEN AWAL MEDIS PASIEN RAWAT JALAN (<i>Khusus Pasien Gigi</i>)</b>";
} else {
    var headerAsmenAwal =
      "<b>ASESMEN AWAL MEDIS RAWAT JALAN (<i>Kecuali kasus Mata dan Gigi </i>)</b>";
  }
  if (data.KD_UNIT == "221") {
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan/control_askep_rwj/getData",
      params: {
        kd_pasien: data.KD_PASIEN,
        kd_unit: data.KD_UNIT,
        tgl_masuk: data.TANGGAL_TRANSAKSI,
        urut_masuk: data.URUT_MASUK,
        group: "RWJ_ASKEP3_MATA",
      },
      method: "GET",
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        dataSource_AsesmenRWJ3.loadData([], false);
        for (var i = 0; i < cst.length; i++) {
          dataSource_AsesmenRWJ3.add(
            new dataSource_AsesmenRWJ3.recordType(cst[i])
          );
        }
      },
    });
  } else if (data.KD_UNIT == "203") {
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan/control_askep_rwj/getData",
      params: {
        kd_pasien: data.KD_PASIEN,
        kd_unit: data.KD_UNIT,
        tgl_masuk: data.TANGGAL_TRANSAKSI,
        urut_masuk: data.URUT_MASUK,
        group: "RWJ_ASKEP3_GIGI",
      },
      method: "GET",
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        dataSource_AsesmenRWJ3.loadData([], false);
        for (var i = 0; i < cst.length; i++) {
          dataSource_AsesmenRWJ3.add(
            new dataSource_AsesmenRWJ3.recordType(cst[i])
          );
        }
      },
    });
  }else {
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan/control_askep_rwj/getData",
      params: {
        kd_pasien: data.KD_PASIEN,
        kd_unit: data.KD_UNIT,
        tgl_masuk: data.TANGGAL_TRANSAKSI,
        urut_masuk: data.URUT_MASUK,
        group: "RWJ_ASKEP3",
      },
      method: "GET",
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        dataSource_AsesmenRWJ3.loadData([], false);
        for (var i = 0; i < cst.length; i++) {
          dataSource_AsesmenRWJ3.add(
            new dataSource_AsesmenRWJ3.recordType(cst[i])
          );
        }
      },
    });
  }
  var Field = [
    "kd_askep",
    "nama",
    "keterangan",
    "jenis_data",
    "satuan",
    "kd_group",
    "nilai",
    "nilai_text",
    "ada",
    "view",
    "enable_yes",
    "enable_no",
    "disable_yes",
    "disable_no",
    "enab",
    "saved",
  ];
  dataSource_AsesmenRWJ3 = new Ext.data.ArrayStore({
    fields: Field,
  });

  gridListAsesmenRWJ3 = new Ext.grid.EditorGridPanel({
    id: "gridAssesment",
    flex: 1,
    stripeRows: true,
    store: dataSource_AsesmenRWJ3,
    border: true,
    autoScroll: true,
    sm: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {
        rowselect: function (sm, row, rec) {},
      },
    }),
    cm: new Ext.grid.ColumnModel([
      {
        header: headerAsmenAwal,
        width: 515,
        sortable: false,
        hideable: true,
        // hidden:true,
        menuDisabled: true,
        dataIndex: "nama",
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
          }
          return (
            '<div style="white-space: normal;">' +
            value.replace(
              new RegExp("&nbsp;", "g"),
              "&nbsp;&nbsp;&nbsp;&nbsp;"
            ) +
            "</div>"
          );
        },
      },
      {
        id: "colReqIdViewIGD",
        dataIndex: "nilai_text",
        sortable: false,
        hideable: false,
        menuDisabled: true,
        width: 300,
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "RADIO") {
            if (b.data.satuan == null) {
              b.data.satuan = "";
            }
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value = "";
            ch = "";
            if (b.data.nilai == "X") {
              ch = "checked";
            }
            value =
              '<input type="radio" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="javascript:action_asessmen_radio(\'' +
              b.data.kd_askep +
              "','" +
              b.data.kd_grup +
              '\',dataSource_AsesmenRWJ3);" name="' +
              b.data.kd_grup +
              '" style="" value="' +
              i +
              '" ' +
              ch +
              " " +
              enab +
              '/><div style="float:left;margin-top: 3px;margin-left: 4px;white-space: normal;">' +
              b.data.satuan +
              "</div>";
          } else if (b.data.jenis_data == "RADIOLIST") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            var sat = b.data.satuan.split(",");
            value = "";
            ch = "";
            for (var i = 0, iLen = sat.length; i < iLen; i++) {
              ch = "";
              if (i === parseInt(b.data.nilai)) {
                ch = "checked";
              }
              value +=
                '<input type="radio" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_RADIOLIST(dataSource_AsesmenRWJ3.getRange()[' +
                c +
                "].data,dataSource_AsesmenRWJ3," +
                i +
                ");dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai','" +
                i +
                "'); dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai_text','" +
                i +
                '\');" name="' +
                b.data.kd_askep +
                '" style="" value="' +
                i +
                '" ' +
                ch +
                " " +
                enab +
                '/><div style="float:left;margin-top: 3px;margin-left: 4px;">' +
                sat[i] +
                "</div>";
            }
          } else if (b.data.jenis_data == "CHECK") {
            if (b.data.satuan == null) {
              b.data.satuan = "";
            }
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value = "";
            ch = "";
            if (b.data.nilai == "X") {
              ch = "checked";
            }
            value =
              '<input type="checkbox" style="margin-top: 3px;float:left;margin-left: 4px;" onclick="action_asessmen_check(\'' +
              b.data.kd_askep +
              '\',dataSource_AsesmenRWJ3);" name="' +
              b.data.kd_askep +
              '" style="" value="' +
              i +
              '" ' +
              ch +
              " " +
              enab +
              '/><div style="float:left;margin-top: 3px;margin-left: 4px;white-space: normal;">' +
              b.data.satuan +
              "</div>";
          } else if (b.data.jenis_data == "TEXT") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            if (b.data.kd_askep == "RWJ_ASKEP3_PASIEN_DATANG_KE_RWJ" || b.data.kd_askep == "RWJ_ASKEP3_PASIEN_MULAI_DIKAJI_DPJP" || b.data.kd_askep == "RWJ_ASKEP3_WAKTU_SELESAI" || b.data.kd_askep == "RWJ_ASKEP3_MATA_WAKTU_PENGKAJIAN" || b.data.kd_askep == "RWJ_ASKEP3_GIGI_WAKTU_PENGKAJIAN") {
              value =
                '<input type="datetime-local" style="margin: -3px 0px;width: 200px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai_text',this.value);\" " +
                enab +
                "/>";
              a.style = "padding: 0px;";
            } else {
              value =
                '<input type="text" style="margin: -3px 0px;width: 200px;" value="' +
                value +
                '" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai_text',this.value);\" " +
                enab +
                "/>";
              // value='<input type="date" value="'+value+'" onblur="javascript: dataSource_AsesmenRWJ3.getRange()['+c+'].set('nilai', this.value); dataSource_AsesmenRWJ3.getRange()['+c+'].set(\'nilai_text\', this.value);" '+enab+'/>';
              a.style = "padding: 0px;";
            }
          } else if (b.data.jenis_data == "TEXTLIST") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<div style="margin: -3px 0px;width: 290px;height: 100px;"><input type="text" style="margin: -3px 0px;width: 400px;" value="' +
              value +
              '" onkeypress="javascript: action_asessmen_TEXTLIST(dataSource_AsesmenRWJ3.getRange()[' +
              c +
              '].data,dataSource_AsesmenRWJ3,this.value,event);"  ' +
              enab +
              "/></div>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "NUMBER") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="number" style="margin: -3px 0px;width: 80px;" value="' +
              value +
              '" ' +
              'onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
              c +
              "].set('nilai_text',this.value);\"" +
              ' onchange="javascript: dataSource_AsesmenRWJ3.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
              c +
              "].set('nilai_text',this.value);\"" +
              "" +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "DATE") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="date" style="margin: -3px 0px;width: 130px;" value="' +
              value +
              '" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
              c +
              "].set('nilai_text',this.value);\" " +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "TIME") {
            var enab = "";
            if (b.data.enab == 0) {
              enab = "disabled";
            }
            value =
              '<input type="time" style="margin: -3px 0px;width: 130px;" value="' +
              value +
              '" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
              c +
              "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
              c +
              "].set('nilai_text',this.value);\" " +
              enab +
              "/>";
            a.style = "padding: 0px;";
          } else if (b.data.jenis_data == "TEXTAREA") {
            if (b.data.kd_askep == "RWJ_ASKEP3_DPJP") {
              value =
                '<div style="margin: -3px 0px; width: 290px; height: 100px;"><textarea type="text" style="width: 500px; height: 100px; position: absolute; text-align: center; vertical-align: bottom; font-weight: bold;" placeholder="(Tanda tangan dan Nama Lengkap)" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai_text',this.value);\">" +
                value +
                "</textarea></div>";
              a.style = "padding: 0px;";
            } else {
              value =
                '<div style="margin: -3px 0px;width: 290px;height: 100px;"><textarea type="text" style="width: 500px;height: 100px;position: absolute;" onblur="javascript: dataSource_AsesmenRWJ3.getRange()[' +
                c +
                "].set('nilai',this.value); dataSource_AsesmenRWJ3.getRange()[" +
                c +
                "].set('nilai_text',this.value);\">" +
                value +
                "</textarea></div>";
              a.style = "padding: 0px;";
            }
          } else if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
          } else if (b.data.jenis_data == "LABEL") {
            a.style = "background: #e6ef7b;";
          }
          if (
            b.data.satuan != null &&
            b.data.satuan != "" &&
            b.data.jenis_data !== "RADIOLIST" &&
            b.data.jenis_data !== "CHECK" &&
            b.data.jenis_data !== "RADIO"
          ) {
            value += "&nbsp;" + b.data.satuan + "";
            return '<div style="white-space: normal;">' + value + "</div>";
          } else {
            return '<div style="white-space: normal;">' + value + "</div>";
          }
        },
      },
      {
        width: 400,
        sortable: false,
        hideable: true,
        hidden: false,
        menuDisabled: true,
        dataIndex: "keterangan",
        renderer: function (value, a, b, c) {
          if (value == null) {
            value = "";
          }
          if (b.data.jenis_data == "TITLE") {
            a.style = "background: #e8e8e8;";
          }
          return (
            '<div style="white-space: normal;">' +
            value.replace(
              new RegExp("&nbsp;", "g"),
              "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
            ) +
            "</div>"
          );
        },
      },
    ]),
    viewConfig: { forceFit: true },
  });

  var tabAssesment = new Ext.Panel({
    title: "Pengkajian Awal Medis",
    id: "tabAssesment",
    layout: "column",
    autoScroll: true,
    layout: "fit",
    border: false,
    items: [gridListAsesmenRWJ3],
  });
  gridListAsesmenRWJ3.getSelectionModel().lock();
  return tabAssesment;
};

PenataJasaRJ.getTindakan = function (data) {
  var $this = this;
  var tabTransaksi = new Ext.Panel({
    title: "Tindak Lanjut",
    id: "tabTindakan",
    layout: "column",
    border: false,
    items: [
      {
        layout: "column",
        border: false,
        items: [
          {
            layout: "form",
            labelWidth: 100,
            labelAlign: "right",
            border: false,
            items: [
              ($this.iCombo1 = new Ext.form.ComboBox({
                id: "iComboStatusTindakanRJPJ",
                typeAhead: true,
                triggerAction: "all",
                lazyRender: true,
                mode: "local",
                emptyText: "",
                width: 400,
                store: $this.ds5,
                valueField: "ID_STATUS",
                displayField: "STATUS",
                value: "",
                fieldLabel: "Cara Keluar &nbsp;",
                listeners: {
                  select: function (a, b, c) {
                    if (b.json.ID_STATUS == 11) {
                      KonsultasiLookUp(null, function (id) {
                        //Ext.getCmp('txtnamaunitKonsultasi').setValue(data.NAMA_UNIT);
                        var o = gridPenataJasaTabItemTransaksiRWJ
                          .getSelectionModel()
                          .getSelections()[0].data;
                        var params = {
                          kd_pasien: o.KD_PASIEN,
                          kd_unit: o.KD_UNIT,
                          tgl_masuk: o.TANGGAL_TRANSAKSI,
                          urut_masuk: o.URUT_MASUK,
                          id_status: b.json.ID_STATUS,
                          catatan: PenataJasaRJ.iTArea1.getValue(),
                          kd_unit_tujuan: id,
                        };
                        Ext.Ajax.request({
                          url:
                            baseURL + "index.php/main/functionRWJ/saveTindakan",
                          params: params,
                          failure: function (o) {
                            ShowPesanWarningRWJ(
                              "Data Tidak berhasil disimpan hubungi admin",
                              "Gagal"
                            );
                          },
                          success: function (o) {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true) {
                            } else {
                              ShowPesanWarningRWJ(
                                "Data Tidak berhasil disimpan hubungi admin",
                                "Gagal"
                              );
                            }
                          },
                        });
                      });
                    }
                  },
                },
              })),
              new Ext.form.ComboBox({
                id: "iComboStatusPulangTindakanRJPJ",
                typeAhead: true,
                triggerAction: "all",
                lazyRender: true,
                mode: "local",
                emptyText: "",
                hidden: true,
                width: 400,
                store: $this.dsstatupulang,
                valueField: "KD_STATUS_PULANG",
                displayField: "STATUS_PULANG",
                value: "",
                fieldLabel: "Keadaan Akhir &nbsp;",
                listeners: {
                  select: function (a, b, c) {},
                },
              }),
              new Ext.form.ComboBox({
                id: "iComboSebabMatiTindakanRJPJ",
                typeAhead: true,
                triggerAction: "all",
                lazyRender: true,
                mode: "local",
                emptyText: "",
                width: 400,
                hidden: true,
                store: $this.dssebabkematian,
                valueField: "kd_sebab_mati",
                displayField: "sebab_mati",
                value: "",
                fieldLabel: "Sebab Mati &nbsp;",
                listeners: {
                  select: function (a, b, c) {},
                },
              }),
              ($this.iTArea1 = new Ext.form.TextArea({
                id: "iTextAreaCatLabRJPJ",
                fieldLabel: "Catatan &nbsp; ",
                width: 400,
                autoScroll: true,
                height: 80,
              })),
            ],
          } /*{
						layout: 'form',
						labelWidth:100,
						labelAlign:'right',
					    border: false,
					    items	: [
							$this.iCombo1= new Ext.form.ComboBox({
								id				: 'iComboDokterRWI',
								typeAhead		: true,
								hidden 			: true,
							    triggerAction	: 'all',
							    lazyRender		: true,
							    mode			: 'local',
							    emptyText		: '',
							    width			: 400,
								store			: $this.ds_dokter_spesial,
								valueField		: 'NAMA',
								displayField	: 'NAMA',
								value			: '',
								fieldLabel		: 'Dokter &nbsp;',
								listeners		: {
									select	: function(a, b, c){
								    }
								}
							}),
							$this.iTArea1= new Ext.form.TextArea({
								id			: 'iTextAreaDengan',
								fieldLabel	: 'Dengan Rencana &nbsp; ',
								width       : 400,
						        autoScroll  : true,
						        hidden 		: true,
						        height      : 70
							}),
							$this.iTArea1= new Ext.form.TextArea({
								id			: 'iTextAreaTerapi',
								fieldLabel	: 'Rencana tindakan &nbsp; ',
								width       : 400,
						        autoScroll  : true,
						        hidden 		: true,
						        height      : 70
							}),
							$this.iTArea1= new Ext.form.TextArea({
								id			: 'iTextAreaTerapiDiberikan',
								fieldLabel	: 'Terapi yang di berikan &nbsp; ',
								width       : 400,
						        autoScroll  : true,
						        hidden 		: true,
						        height      : 70
							}),
							{	
								id 		: 'btn_cetak_surat_rawat',
								xtype 	: 'button',
								hidden 	: true,
								text 	: 'Cetak Surat Rawat',
								handler	: function(){
									var params={
										kd_dokter_rwj 	: Ext.getCmp('txtKdDokter').getValue(),
										kd_dokter_rwi 	: Ext.getCmp('iComboDokterRWI').getValue(),
										kd_pasien 		: Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
										dengan 			: Ext.getCmp('iTextAreaDengan').getValue(),
										terapi 			: Ext.getCmp('iTextAreaTerapi').getValue(),
										terapi_berikan 	: Ext.getCmp('iTextAreaTerapiDiberikan').getValue(),
									};

									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/general/surat_rawat/cetak");
									// form.setAttribute("action", baseURL + "index.php/main/functionRWJ/cetakLab");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();
								},
							}
						]
					}*/,
        ],
      },
    ],
  });
  return tabTransaksi;
};
PenataJasaRJ.ok_ok = function (data) {
  var $this = this;
  var tab_OK = new Ext.Panel({
    id: "tabjadwalop",
    layout: "Form",
    title: "Order Jadwal Operasi",
    labelAlign: "Left",
    items: [
      {
        xtype: "compositefield",
        fieldLabel: "Tgl. Operasi",
        anchor: "100%",
        width: 200,
        items: [
          {
            xtype: "datefield",
            id: "TglOperasi_viJdwlOperasi",
            format: "d/M/Y",
            width: 120,
            value: now,
            listeners: {
              specialkey: function () {
                if (Ext.EventObject.getKey() === 13) {
                  datarefresh_viJdwlOperasi();
                }
              },
            },
          },
        ],
      },
      {
        xtype: "compositefield",
        fieldLabel: "Jam Operasi",
        anchor: "100%",
        width: 200,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Jam",
            id: "txtJam_viJdwlOperasi",
            name: "txtJam_viJdwlOperasi",
            width: 50,
            emptyText: "Jam",
            maxLength: 2,
          },
          {
            xtype: "textfield",
            fieldLabel: "Menit",
            id: "txtMenit_viJdwlOperasi",
            name: "txtMenit_viJdwlOperasi",
            width: 50,
            emptyText: "Menit",
            maxLength: 2,
          },
        ],
      },
      viComboJenisTindakan_viJdwlOperasi(),
      viComboKamar_viJdwlOperasi(),
    ],
  });
  return tab_OK;
};
PenataJasaRJ.riwayatPasien = function (data) {
  var $this = this;
  var subPanel_accordionRiwayat_RWJ = new Ext.Panel({
    layout: {
      type: "accordion",
      titleCollapse: true,
      multi: true,
      fill: false,
      animate: false,
      flex: 1,
    },
    style: "padding:4px;",
    flex: 1,
    autoScroll: true,
    id: "accordionNavigationContainerPenataJasaRWJ",
    layoutConfig: {
      titleCollapse: false,
      animate: true,
      activeOnTop: false,
    },
    items: [
      {
        title: "Kondisi Anamnese",
        layout: "fit",
        collapsed: true,
        items: [GetGridAnamnese_RWJ()],
      },
      {
        title: "Diagnosa",
        collapsed: true,
        items: [GetGridRiwayatDiagnosa_RWJ()],
      },
      {
        title: "Tindakan / ICD 9",
        collapsed: true,
        items: [GetGridRiwayatTindakan_RWJ()],
      },
      {
        title: "Obat",
        collapsed: true,
        items: [GetGridRiwayatObat_RWJ()],
      },
      {
        title: "Laboratorium",
        collapsed: true,
        items: [GetGridRiwayatLab_RWJ()],
      },
      {
        title: "Radiologi",
        collapsed: true,
        items: [GetGridRiwayatRad_RWJ()],
      },
    ],
  });
  var subPanel_riwayatAmnase_RWJ = new Ext.Panel({
    id: "panelriwayatAmnaseRWJ",
    layout: "form",
    height: 58,
    border: false,
    style: "padding:4px",
    items: [
      {
        xtype: "textfield",
        fieldLabel: "Menit",
        id: "txtAmnase_RWJ",
        fieldLabel: "Anamnese",
        anchor: "100%",
        name: "txtAmnase_RWJ",
        readOnly: true,
      },
      {
        xtype: "textfield",
        id: "txtStatus_RWJ",
        anchor: "100%",
        fieldLabel: "Status",
        name: "txtStatus_RWJ",
        width: 460,
        hidden: true,
        readOnly: true,
      },
      {
        fieldLabel: "Catatan",
        xtype: "textfield",
        id: "txtCatatan_RWJ",
        name: "txtCatatan_RWJ",
        anchor: "100%",
        readOnly: true,
      },
    ],
  });

  var panel_riwayatKunjunganPasien_RWJ = new Ext.Panel({
    id: "panelriwayatKunjunganPasienRWJ",
    // fileUpload: true,
    layout: "fit",
    // anchor: '100%',
    style: "padding:4px;",
    title: "Kunjungan Pasien",
    width: 250,
    // height: 292,
    // bodyStyle: 'padding:0px 0px 0px 0px',
    items: [GetGridRiwayatKunjunganPasien_RWJ()],
  });
  var panel_riwayatPasien_RWJ = new Ext.Panel({
    id: "panelriwayatPasienRWJ",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    flex: 1,
    style: "padding: 4px 4px 4px 0px;",
    title: "Riwayat Pasien",
    autoScroll: true,
    items: [subPanel_riwayatAmnase_RWJ, subPanel_accordionRiwayat_RWJ],
  });

  var panel_riwayatAllKunjunganPasien_RWJ = new Ext.Panel({
    id: "panelRiwayatAllKunjunganPasienRWJ",
    // fileUpload: true,
    layout: {
      type: "hbox",
      align: "stretch",
    },
    // anchor: '100%',
    border: true,
    title: "Riwayat Pasien",
    // width: 915,
    // height: 700,
    // autoScroll : true,
    labelAlign: "Left",
    items: [panel_riwayatKunjunganPasien_RWJ, panel_riwayatPasien_RWJ],
    tbar: [
      {
        text: "Cetak Riwayat",
        id: "btnCetakRiwayatPasien_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "print",
        handler: function () {
          if (
            currentRiwayatKunjunganPasien_TglMasuk_RWJ == "" ||
            currentRiwayatKunjunganPasien_TglMasuk_RWJ == undefined
          ) {
            ShowPesanWarningRWJ(
              "Pilih riwayat kunjungan yang akan dicetak!",
              "Warning"
            );
          } else {
            var params = {
              kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
              kd_unit: currentRiwayatKunjunganPasien_KdUnit_RWJ,
              kd_kasir: currentRiwayatKunjunganPasien_KdKasir_RWJ,
              urut_masuk: currentRiwayatKunjunganPasien_UrutMasuk_RWJ,
              tgl_masuk: currentRiwayatKunjunganPasien_TglMasuk_RWJ,
              kd_dokter: currentRiwayatKunjunganPasien_KdDokter_RWJ,
              no_transaksi: currentRiwayatKunjunganPasien_NoTrans_RWJ,
            };
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("target", "_blank");
            form.setAttribute(
              "action",
              baseURL +
                "index.php/rawat_jalan/functionRWJ/cetakRiwayatKunjungan"
            );
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "data");
            hiddenField.setAttribute("value", Ext.encode(params));
            form.appendChild(hiddenField);
            document.body.appendChild(form);
            form.submit();
          }
        },
      },
    ],
  });
  return panel_riwayatAllKunjunganPasien_RWJ;
};

function viComboJenisTindakan_viJdwlOperasi() {
  var Field = ["kd_tindakan", "tindakan"];
  dsvComboTindakanOperasiJadwalOperasiOK = new WebApp.DataStore({
    fields: Field,
  });
  dsComboTindakanOperasiJadwalOperasiOK();
  var cbo_viComboJenisTindakan_viJdwlOperasi = new Ext.form.ComboBox({
    id: "cbo_viComboJenisTindakan_viJdwlOperasi",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "Pilih Jenis Tindakan..",
    fieldLabel: "Jenis Tindakan",
    width: 230,
    store: dsvComboTindakanOperasiJadwalOperasiOK,
    valueField: "kd_tindakan",
    displayField: "tindakan",
    listeners: {},
  });
  return cbo_viComboJenisTindakan_viJdwlOperasi;
}

function dsComboTindakanOperasiJadwalOperasiOK(kriteria) {
  dsvComboTindakanOperasiJadwalOperasiOK.load({
    params: {
      Skip: 0,
      Take: "",
      Sort: "",
      Sortdir: "ASC",
      target: "vComboTindakanOperasiJadwalOperasiOK",
      param: kriteria,
    },
  });
  return dsvComboTindakanOperasiJadwalOperasiOK;
}

PenataJasaRJ.getGrid3 = function (data) {
  var $this = this;
  PenataJasaRJ.ds3 = new WebApp.DataStore({
    fields: [
      "kd_produk",
      "deskripsi",
      "kd_tarif",
      "harga",
      "qty",
      "desc_req",
      "tgl_berlaku",
      "kd_dokter",
      "no_transaksi",
      "urut",
      "desc_status",
      "tgl_transaksi",
      "jumlah",
      "namadok",
      "lunas",
      "kd_pasien",
      "urutkun",
      "tglkun",
      "kdunitkun",
      "kd_unit",
      "catatan_dokter"
    ],
  });
  PenataJasaRJ.grid3 = new Ext.grid.EditorGridPanel({
    title: "Laboratorium",
    id: "grid3",
    flex: 1,
    stripeRows: true,
    store: PenataJasaRJ.ds3,
    border: true,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          rowSelectedPJKasir = PenataJasaRJ.ds3.getAt(row);
          if (
            rowSelectedPJKasir.data.kd_pasien === undefined ||
            rowSelectedPJKasir.data.kd_pasien === ""
          ) {
          } else {
            ViewGridDetailHasilLab(
              rowSelectedPJKasir.data.kd_pasien,
              rowSelectedPJKasir.data.tglkun,
              rowSelectedPJKasir.data.urutkun,
              rowSelectedPJKasir.data.kd_produk
            );
          }
        },
      },
    }),
    cm: $this.getModel1(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.grid3;
};

function ViewGridDetailHasilLab(kd_pasien, tgl_masuk, urut_masuk, kd_produk) {
  var strKriteriaHasilLab = "";
  strKriteriaHasilLab =
    "LAB_hasil.Kd_Pasien = '" +
    kd_pasien +
    "' And LAB_hasil.Tgl_Masuk = '" +
    tgl_masuk +
    "'  and LAB_hasil.Urut_Masuk =" +
    urut_masuk +
    "  and LAB_hasil.kd_unit= '" +
    kdUnitLab_PenjasRWJ +
    "' and LAB_hasil.kd_produk='" +
    kd_produk +
    "'  order by lab_test.kd_lab,lab_test.kd_test asc";
  PenataJasaRJ.dshasilLabRWJ.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "tgl_transaksi",
      //Sort: 'tgl_transaksi',
      Sortdir: "ASC",
      target: "ViewGridHasilLab",
      param: strKriteriaHasilLab,
    },
  });
  return PenataJasaRJ.dshasilLabRWJ;
}
function GetGridRwJPJRad(data) {
  var fldDetail = [
    "kd_produk",
    "deskripsi",
    "kd_tarif",
    "harga",
    "qty",
    "desc_req",
    "cito",
    "tgl_berlaku",
    "no_transaksi",
    "urut",
    "desc_status",
    "tgl_transaksi",
    "jumlah",
    "kd_dokter",
    "namadok",
    "lunas",
    "kd_pasien",
    "urutkun",
    "tglkun",
    "kdunitkun",
    "kd_unit",
    "catatan_dokter"
  ];
  dsRwJPJRAD = new WebApp.DataStore({ fields: fldDetail });
  PenataJasaRJ.pj_req_rad = new Ext.grid.EditorGridPanel({
    title: "Radiologi",
    id: "gridRwJPJRad",
    stripeRows: true,
    store: dsRwJPJRAD,
    border: true,
    flex: 1,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          rowSelectedPJKasir_rad = dsRwJPJRAD.getAt(row);
          if (
            rowSelectedPJKasir_rad.data.kd_pasien === undefined ||
            rowSelectedPJKasir_rad.data.kd_pasien === ""
          ) {
          } else {
            pj_req_radhasil(
              rowSelectedPJKasir_rad.data.kd_pasien,
              rowSelectedPJKasir_rad.data.kdunitkun,
              rowSelectedPJKasir_rad.data.tglkun,
              rowSelectedPJKasir_rad.data.urutkun,
              rowSelectedPJKasir_rad.data.kd_produk,
              rowSelectedPJKasir_rad.data.urut
            );
          }
        },
      },
    }),
    cm: getModelRwJPJRad(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.pj_req_rad;
}
PenataJasaRJ.getModel1 = function () {
  //    var Field = ['KD_DOKTER','NAMA'];
  var fldDetail = ["KD_DOKTER", "NAMA"];
  dsLookProdukList_dokter_leb = new WebApp.DataStore({ fields: fldDetail });
  PenataJasaRJ.form.ComboBox.dok_lab = new Ext.form.ComboBox({
    id: Nci.getId(),
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    store: dsLookProdukList_dokter_leb,
    valueField: "NAMA",
    hideTrigger: true,
    displayField: "NAMA",
    value: "",
    listeners: {
      select: function (a, b, c) {
        var line = PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
        if (
          PenataJasaRJ.ds3.data.items[line].data.no_transaksi === "" ||
          PenataJasaRJ.ds3.data.items[line].data.no_transaksi === undefined
        ) {
          PenataJasaRJ.ds3.data.items[line].data.namadok = b.data.NAMA;
          PenataJasaRJ.var_kd_dokter_leb = b.data.KD_DOKTER;
          PenataJasaRJ.grid3.getView().refresh();
        } else {
          ViewGridBawahpoliLab(
            PenataJasaRJ.ds3.data.items[line].data.no_transaksi,
            Ext.getCmp("txtKdUnitRWJ").getValue()
          );
          ShowPesanWarningRWJ(
            "dokter tidak bisa di ganti karena data sudah tersimpan ",
            "Warning"
          );
        }
      },
    },
  });
  var $this = this;
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Cito",
      dataIndex: "cito",
      width: 65,
      menuDisabled: true,
      renderer: function (v, metaData, record) {
        if (record.data.cito == "0") {
          record.data.cito = "Tidak";
        } else if (record.data.cito == "1") {
          metaData.style = 'background:#FF0000;  "font-weight":"bold";';
          record.data.cito = "Ya";
        } else if (record.data.cito == "Ya") {
          metaData.style = 'background:#FF0000;  "font-weight":"bold";';
        }
        return record.data.cito;
      },
      editor: new Ext.form.ComboBox({
        id: "cboKasus2",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Silahkan Pilih...",
        width: 50,
        anchor: "95%",
        value: 1,
        store: new Ext.data.ArrayStore({
          id: 0,
          fields: ["Id", "displayText"],
          data: [
            [1, "Ya"],
            [2, "Tidak"],
          ],
        }),
        valueField: "displayText",
        displayField: "displayText",
        value: "",
      }),
    },
    {
      id: Nci.getId(),
      header: "No Transaksi",
      width: 60,
      hidden: false,
      menuDisabled: true,
      dataIndex: "no_transaksi",
    },
    {
      id: Nci.getId(),
      header: "Pembayaran",
      dataIndex: "lunas",
      sortable: true,
      width: 60,
      align: "center",
      renderer: function (value, metaData, record, rowIndex, colIndex, store) {
        switch (value) {
          case "t":
            value = "lunas"; //
            break;
          case "f":
            value = "belum "; // rejected
            break;
        }
        return value;
      },
    },
    {
      id: Nci.getId(),
      header: "Kode",
      dataIndex: "kd_produk",
      width: 35,
      menuDisabled: true,
      hidden: false,
    },
    {
      id: Nci.getId(),
      header: "Item lab",
      dataIndex: "deskripsi",
      width: 150,
      sortable: false,
      hidden: false,
      menuDisabled: true,
      editor: (PenataJasaRJ.form.ComboBox.produk_labdesk =
        new Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.produk,
          select: function (a, b, c) {
            // console.log(PenataJasaRJ.ds3);
            console.log(PenataJasaRJ.grid3);
            Ext.Ajax.request({
              url: baseURL + "index.php/main/functionLAB/cekProduk",
              params: { kd_lab: b.data.kd_produk },
              failure: function (o) {
                ShowPesanErrorRWJ("Hubungi Admin", "Error");
              },
              success: function (o) {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                  var search_produk = true;
                  for (
                    var i = 0;
                    i < PenataJasaRJ.grid3.store.data.length;
                    i++
                  ) {
                    if (
                      (PenataJasaRJ.grid3.store.data.items[i].data
                        .no_transaksi != null ||
                        PenataJasaRJ.grid3.store.data.items[i].data
                          .no_transaksi != "") &&
                      PenataJasaRJ.grid3.store.data.items[i].data.kd_produk ==
                        b.data.kd_produk &&
                      PenataJasaRJ.grid3.store.data.items[i].data.lunas == "f"
                    ) {
                      search_produk = false;
                      break;
                    } else {
                    }
                  }
                  if (search_produk === true) {
                    var line =
                      PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
                    PenataJasaRJ.ds3.data.items[line].data.deskripsi =
                      b.data.deskripsi;
                    PenataJasaRJ.ds3.data.items[line].data.uraian =
                      b.data.uraian;
                    PenataJasaRJ.ds3.data.items[line].data.kd_tarif =
                      b.data.kd_tarif;
                    PenataJasaRJ.ds3.data.items[line].data.kd_produk =
                      b.data.kd_produk;
                    PenataJasaRJ.ds3.data.items[line].data.tgl_transaksi =
                      b.data.tgl_transaksi;
                    PenataJasaRJ.ds3.data.items[line].data.tgl_berlaku =
                      b.data.tgl_berlaku;
                    PenataJasaRJ.ds3.data.items[line].data.harga = b.data.harga;
                    PenataJasaRJ.ds3.data.items[line].data.qty = b.data.qty;
                    PenataJasaRJ.ds3.data.items[line].data.jumlah =
                      b.data.jumlah;
                    PenataJasaRJ.ds3.data.items[line].data.status_konsultasi =
                      b.data.status_konsultasi;
                    PenataJasaRJ.grid3.getView().refresh();
                  } else {
                    var line =
                      PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
                    PenataJasaRJ.ds3.data.items[line].data.deskripsi = "";
                    PenataJasaRJ.ds3.data.items[line].data.uraian = "";
                    Ext.MessageBox.alert(
                      "Information",
                      "Produk " +
                        b.data.deskripsi +
                        " sudah ada dalam status belum lunas"
                    );
                  }
                } else {
                  var line =
                    PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
                  ShowPesanInfoRWJ(
                    "Nilai normal item " + b.data.deskripsi + " belum tersedia",
                    "Information"
                  );
                  PenataJasaRJ.ds3.data.items[line].data.kd_produk = "";
                  PenataJasaRJ.grid3.getView().refresh();
                }
              },
            });
          },
          insert: function (o) {
            return {
              uraian: o.uraian,
              kd_tarif: o.kd_tarif,
              tarif: o.tarif,
              kd_produk: o.kd_produk,
              tgl_transaksi: o.tgl_transaksi,
              tgl_berlaku: o.tgl_berlaku,
              harga: o.harga,
              qty: o.qty,
              deskripsi: o.deskripsi,
              jumlah: o.jumlah,
              status_konsultasi: o.status_konsultasi,
              text:
                '<table style="font-size: 11px;"><tr><td width="60">' +
                o.kd_produk +
                '</td><td width="150">' +
                o.deskripsi +
                "</td></tr></table>",
            };
          },
          param: function () {
            var o = gridPenataJasaTabItemTransaksiRWJ
              .getSelectionModel()
              .getSelections()[0].data;
            var params = {};
            params["kd_unit"] = o.KD_UNIT;
            params["kd_customer"] = o.KD_CUSTOMER;
            params["penjas"] = "rwj";
            return params;
          },
          url: baseURL + "index.php/main/functionLAB/getProduk",
          valueField: "deskripsi",
          displayField: "text",
          listWidth: 210,
        })),
    },
    {
      id: Nci.getId(),
      header: "Catatan Klinis",
      dataIndex: "catatan_dokter",
      editor: new Ext.form.TextField({
        id: "fieldcolCatatanKlinis_Lab",
        allowBlank: true,
        enableKeyEvents: true,
        // width: 30,
        flex : 1
      }),
    },
    {
      id: Nci.getId(),
      header: "Tanggal Berkunjung",
      dataIndex: "tgl_transaksi",
      width: 130,
      menuDisabled: true,
      renderer: function (v, params, record) {
        if (record.data.tgl_transaksi == undefined) {
          record.data.tgl_transaksi = tglGridBawah_poli;
          return record.data.tgl_transaksi;
        } else {
          if (record.data.tgl_transaksi.substring(5, 4) == "-") {
            return ShowDate(record.data.tgl_transaksi);
          } else {
            var tgl = record.data.tgl_transaksi.split("/");
            if (tgl[2].length == 4 && isNaN(tgl[1])) {
              return record.data.tgl_transaksi;
            } else {
              return ShowDate(record.data.tgl_transaksi);
            }
          }
        }
      },
    },
    {
      //no_transaksi
      id: Nci.getId(),
      header: "QTY",
      dataIndex: "qty",
      width: 100,
      menuDisabled: true,
      hidden: true,
      editor: new Ext.form.NumberField({ allowBlank: false }),
    },
    {
      id: Nci.getId(),
      header: "Dokter",
      hidden: true,
      menuDisabled: true,
      dataIndex: "namadok",
      editor: PenataJasaRJ.form.ComboBox.dok_lab,
    } /* ,{
            id			: Nci.getId(),
            header		: 'Kode Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'kd_dokter',
		
        } */,
  ]);
};
function getproduk_PJRWJ() {
  var Field = ["kd_produk", "deskripsi", "tarifx", "kp_produk"];
  DataStoreProduk_KasirRWJ = new WebApp.DataStore({ fields: Field });
  dsLookProdukList_rad.removeAll();
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getProdukList",
    params: {
      modul: "RAD",
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      customer: Ext.getCmp("txtCustomer").getValue(),
      kd_customer: vkode_customer,
      kp_produk: "",
      deskripsi: "",
    },
    success: function (response) {
      var cst = Ext.decode(response.responseText);
      for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
        var recs = [],
          recType = DataStoreProduk_KasirRWJ.recordType;
        var o = cst["data"][i];
        recs.push(new recType(o));
        dsLookProdukList_rad.add(recs);
      }
    },
  });

  // var str='LOWER(tarif.kd_tarif)=LOWER(~'+PenataJasaRJ.varkd_tarif+'~) and tarif.kd_unit= ~5~ '
  // dsLookProdukList_rad.load({
  // params:{
  // Skip: 0,
  // Take: 1000,
  // Sort: 'tgl_transaksi',
  // Sortdir: 'ASC',
  // target: 'LookupProduk',
  // param: str
  // }
  // });
  return dsLookProdukList_rad;
}
function dokter_leb_rwj() {
  dsLookProdukList_dokter_leb.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "kd_dokter",
      Sortdir: "ASC",
      target: "ViewDokterPenunjang",
      param: "kd_unit = '41'",
    },
  });
  return dsLookProdukList_dokter_leb;
}

function dokter_rad_rwj() {
  dsLookProdukList_dokter_rad.load({
    params: {
      Skip: 0,
      Take: 1000,

      Sort: "kd_dokter",
      Sortdir: "ASC",
      target: "ViewDokterPenunjang",
      param: "kd_unit = '5'",
    },
  });
  return dsLookProdukList_dokter_rad;
}

function getModelRwJPJRad() {
  var flddokterradio = ["KD_DOKTER", "NAMA"];
  dsLookProdukList_dokter_rad = new WebApp.DataStore({
    fields: flddokterradio,
  });
  PenataJasaRJ.form.ComboBox.dok_rad = new Ext.form.ComboBox({
    id: Nci.getId(),
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    store: dsLookProdukList_dokter_rad,
    valueField: "NAMA",
    hideTrigger: true,
    displayField: "NAMA",
    value: "",
    listeners: {
      select: function (a, b, c) {
        var line =
          PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];

        if (
          dsRwJPJRAD.data.items[line].data.no_transaksi === "" ||
          dsRwJPJRAD.data.items[line].data.no_transaksi === undefined
        ) {
          dsRwJPJRAD.data.items[line].data.namadok = b.data.NAMA;
          PenataJasaRJ.var_kd_dokter_rad = b.data.KD_DOKTER;
          PenataJasaRJ.pj_req_rad.getView().refresh();
        } else {
          ViewGridBawahpoliRad(dsRwJPJRAD.data.items[line].data.no_transaksi);
          ShowPesanWarningRWJ(
            "dokter tidak bisa di ganti karena data sudah tersimpan ",
            "Warning"
          );
        }
      },
    },
  });

  var fldDetail = [
    "TARIF",
    "KLASIFIKASI",
    "PERENT",
    "TGL_BERAKHIR",
    "KD_KAT",
    "KD_TARIF",
    "KD_KLAS",
    "DESKRIPSI",
    "YEARS",
    "NAMA_UNIT",
    "KD_PRODUK",
    "TGL_BERLAKU",
    "CHEK",
    "JUMLAH",
  ];
  dsLookProdukList_rad = new WebApp.DataStore({ fields: fldDetail });
  PenataJasaRJ.form.ComboBox.produk_rab = new Ext.form.ComboBox({
    id: Nci.getId(),
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    store: dsLookProdukList_rad,
    valueField: "deskripsi",
    hideTrigger: true,
    displayField: "deskripsi",
    value: "",
    listeners: {
      select: function (a, b, c) {
        var line =
          PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];
        dsRwJPJRAD.data.items[line].data.deskripsi2 = b.data.deskripsi;
        dsRwJPJRAD.data.items[line].data.kd_tarif = b.data.kd_tarif;
        dsRwJPJRAD.data.items[line].data.deskripsi = b.data.deskripsi;
        dsRwJPJRAD.data.items[line].data.tgl_berlaku = b.data.tgl_berlaku;
        dsRwJPJRAD.data.items[line].data.qty = 1;
        dsRwJPJRAD.data.items[line].data.kd_unit = b.data.kd_unit;
        dsRwJPJRAD.data.items[line].data.kd_produk = b.data.kd_produk;
        dsRwJPJRAD.data.items[line].data.harga = b.data.tarifx;
        dsRwJPJRAD.data.items[line].data.jumlah = b.data.tarifx;
        dsRwJPJRAD.data.items[line].data.no_transaksi = "";
        PenataJasaRJ.pj_req_rad.getView().refresh();
        //Ext.getCmp('btnSimpanPenJasRad').enable();
      },
    },
  });
  //'kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah'
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Cito",
      dataIndex: "cito",
      width: 65,
      menuDisabled: true,
      renderer: function (v, metaData, record) {
        if (record.data.cito == "0") {
          record.data.cito = "Tidak";
        } else if (record.data.cito == "1") {
          metaData.style = 'background:#FF0000;  "font-weight":"bold";';
          record.data.cito = "Ya";
        } else if (record.data.cito == "Ya") {
          metaData.style = 'background:#FF0000;  "font-weight":"bold";';
        }

        return record.data.cito;
      },
      editor: new Ext.form.ComboBox({
        id: "cboKasus1",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Silahkan Pilih...",
        width: 50,
        anchor: "95%",
        value: 1,
        store: new Ext.data.ArrayStore({
          id: 0,
          fields: ["Id", "displayText"],
          data: [
            [1, "Ya"],
            [2, "Tidak"],
          ],
        }),
        valueField: "displayText",
        displayField: "displayText",
        value: "",
      }),
    },
    {
      id: Nci.getId(),
      header: "No Transaksi",
      width: 60,
      hidden: false,
      menuDisabled: true,
      dataIndex: "no_transaksi",
    },
    {
      id: Nci.getId(),
      header: "Pembayaran",
      dataIndex: "lunas",
      sortable: true,
      width: 60,
      align: "center",
      renderer: function (value, metaData, record, rowIndex, colIndex, store) {
        switch (value) {
          case "t":
            value = "lunas"; //
            break;
          case "f":
            value = "belum "; // rejected

            break;
        }
        return value;
      },
    },
    {
      id: Nci.getId(),
      header: "Kode",
      dataIndex: "kd_produk",
      width: 30,
      menuDisabled: true,
      hidden: false,
      editor: getRadtest(),
    },

    {
      id: Nci.getId(),
      header: "Item Rad",
      dataIndex: "deskripsi",
      hidden: false,
      menuDisabled: true,
      width: 150,
      editor: PenataJasaRJ.form.ComboBox.produk_rab,
    },
    {
      id: Nci.getId(),
      header: "Catatan Klinis",
      dataIndex: "catatan_dokter",
      editor: new Ext.form.TextField({
        id: "fieldcolCatatanKlinis",
        allowBlank: true,
        enableKeyEvents: true,
        // width: 30,
        flex : 1
      }),
    },
    {
      id: Nci.getId(),
      header: "Tanggal Berkunjung",
      dataIndex: "tgl_transaksi",
      width: 130,
      menuDisabled: true,
      renderer: function (v, params, record) {
        if (record.data.tgl_transaksi == undefined) {
          record.data.tgl_transaksi = tglGridBawah_poli;
          return record.data.tgl_transaksi;
        } else {
          if (record.data.tgl_transaksi.substring(5, 4) == "-") {
            return ShowDate(record.data.tgl_transaksi);
          } else {
            var tgl = record.data.tgl_transaksi.split("/");

            if (tgl[2].length == 4 && isNaN(tgl[1])) {
              return record.data.tgl_transaksi;
            } else {
              return ShowDate(record.data.tgl_transaksi);
            }
          }
        }
      },
    },
    {
      id: Nci.getId(),
      header: "DOKTER",
      dataIndex: "namadok",
      menuDisabled: true,
      hidden: true,
      width: 100,
      editor: PenataJasaRJ.form.ComboBox.dok_rad,
    },
    {
      id: Nci.getId(),
      header: "QTY",
      dataIndex: "qty",
      width: 100,
      menuDisabled: true,
      hidden: true,
      editor: new Ext.form.NumberField({ allowBlank: false }),
    },
  ]);
}

/* Get Pemeriksaan Gigi Grid oleh Roni pada tanggal 20 - 10 - 2023 */

function GetDTLTRPemeriksaanGigiGrid() {
  var pnlTRPemeriksaanGigi = new Ext.Panel({
    title: "Pemeriksaan Gigi",
    id: "tabDentalCare",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: false,
    autoScroll: true,
    items: [
      GetDTLTRPemeriksaanGigi(),
      {
        xtype: 'displayfield', // Use displayfield to show an image
        value: '<img src="' + baseURL + 'ui/images/PoliGigi.png" width="680" height="120" style="display: block; margin: 0 auto;">',
        
      },
      GetDTLTRPemeriksaanGigi2(),
    ],
  });
  return pnlTRPemeriksaanGigi;
}

function GetDTLTRPemeriksaanGigi() {
  var DentalcareField = ["ID_PARAMETER", "GIGI_ATASBAWAHPOSISIKIRI", "GIGI_ATASBAWAHPOSISIKANAN", "HASIL_GIGI_ATASBAWAHPOSISIKIRI", "HASIL_GIGI_ATASBAWAHPOSISIKANAN"];
  dsTRDetailDentalcareList = new WebApp.DataStore({ fields: DentalcareField });
  var gridDTLTRDentalCare = new Ext.grid.EditorGridPanel({
    id: "tabcatPemeriksaanGigi",
    stripeRows: true,
    store: dsTRDetailDentalcareList,
    border: true,
    style: "padding: 4px 0px;",
    flex: 2,
    columnLines: true,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: TRPemeriksaanGigiColumModel(),
    viewConfig: { forceFit: true },
  });
  // gridDTLTRAnamnese.disable();
  return gridDTLTRDentalCare;
}

function GetDTLTRPemeriksaanGigi2() {
  var DentalcareField = ["ID_PARAMETER", "GIGI_ATASBAWAHPOSISIKIRI", "GIGI_ATASBAWAHPOSISIKANAN", "HASIL_GIGI_ATASBAWAHPOSISIKIRI", "HASIL_GIGI_ATASBAWAHPOSISIKANAN"];
  dsTRDetailDentalcareList2 = new WebApp.DataStore({ fields: DentalcareField });
  var gridDTLTRDentalCare2 = new Ext.grid.EditorGridPanel({
    id: "tabcatPemeriksaanGigi2",
    stripeRows: true,
    store: dsTRDetailDentalcareList2,
    border: true,
    style: "padding: 4px 0px;",
    flex: 2,
    columnLines: true,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: TRPemeriksaanGigiColumModel2(),
    viewConfig: { forceFit: true },
  });
  // gridDTLTRAnamnese.disable();
  return gridDTLTRDentalCare2;
}

function TRPemeriksaanGigiColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: "colGigiAtasKiri",
      // header: "GIGI_ATASBAWAHPOSISIKIRI",
      dataIndex: "GIGI_ATASBAWAHPOSISIKIRI",
      hidden: false,
      width: 80,
    },
    {
      id: "colHasilGigiAtasKiri",
      // header: "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
      dataIndex: "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textHasilGigiAtasKiri",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colHasilGigiAtasKanan",
      // header: "HASIL_GIGI_ATASBAWAHPOSISIKANAN",
      dataIndex: "HASIL_GIGI_ATASBAWAHPOSISIKANAN",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textHasilGigiAtasKanan",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colGigiAtasKanan",
      // header: "GIGI_ATASBAWAHPOSISIKANAN",
      dataIndex: "GIGI_ATASBAWAHPOSISIKANAN",
      hidden: false,
      width: 80,
    },
  ]);
}

function TRPemeriksaanGigiColumModel2() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: "colGigiBawahKiri",
      // header: "GIGI_ATASBAWAHPOSISIKIRI",
      dataIndex: "GIGI_ATASBAWAHPOSISIKIRI",
      hidden: false,
      width: 80,
    },
    {
      id: "colHasilGigiBawahKiri",
      // header: "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
      dataIndex: "HASIL_GIGI_ATASBAWAHPOSISIKIRI",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textHasilGigiBawahKiri",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colHasilGigiBawahKanan",
      // header: "HASIL_GIGI_ATASBAWAHPOSISIKANAN",
      dataIndex: "HASIL_GIGI_ATASBAWAHPOSISIKANAN",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textHasilGigiBawahKanan",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colGigiBawahKanan",
      // header: "GIGI_ATASBAWAHPOSISIKANAN",
      dataIndex: "GIGI_ATASBAWAHPOSISIKANAN",
      hidden: false,
      width: 80,
    },
  ]);
}

/* =============================================================== */

/* Get Pemeriksaan Mata Grid oleh Roni pada tanggal 18 - 10 - 2023 */

function GetDTLTRPemeriksaanMataGrid() {
  var pnlTRPemeriksaanMata = new Ext.Panel({
    title: "Pemeriksaan Mata",
    id: "tabEyeCare",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: false,
    items: [
      {
        xtype: 'displayfield', // Use displayfield to show an image
        value: '<img src="' + baseURL + 'ui/images/PoliMata.png" width="680" height="120" style="display: block; margin: 0 auto;">',
      },
      GetDTLTRPemeriksaanMata(),
      textareacatatanPemeriksaanMata(),
    ],
  });
  return pnlTRPemeriksaanMata;
}

function textareacatatanPemeriksaanMata() {
  var TextAreaCatatanPemeriksaanMata = new Ext.Panel({
    title: "Test Buta Warna :",
    id: "tabtextEyeCare",
    layout: "fit",
    flex: 1,
    items: [
      {
        xtype: "textarea",
        fieldLabel: "Catatan",
        name: "txtareaEyeCarecatatan",
        id: "txtareaEyeCarecatatan",
        // readOnly: true,
      },
    ],
  });
  return TextAreaCatatanPemeriksaanMata;
}

function GetDTLTRPemeriksaanMata() {
  var EyecareField = ["ID_PARAMETER", "PARAMETER", "HASIL_ODRE", "HASIL_OSLE"];
  dsTRDetailEyecareList = new WebApp.DataStore({ fields: EyecareField });
  var gridDTLTREyeCare = new Ext.grid.EditorGridPanel({
    id: "tabcatPemeriksaanMata",
    stripeRows: true,
    store: dsTRDetailEyecareList,
    border: true,
    style: "padding: 4px 0px;",
    flex: 2,
    columnLines: true,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: TRPemeriksaanMataColumModel(),
    viewConfig: { forceFit: true },
  });
  // gridDTLTRAnamnese.disable();
  return gridDTLTREyeCare;
}

function TRPemeriksaanMataColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: "colParameter",
      header: "PARAMETER",
      dataIndex: "PARAMETER",
      hidden: false,
      width: 80,
    },
    {
      id: "colOdRe",
      header: "OD/RE",
      dataIndex: "HASIL_ODRE",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textOdRe",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colOsLe",
      header: "OS/LE",
      dataIndex: "HASIL_OSLE",
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textOsLe",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
  ]);
}

/* =============================================================== */

function GetDTLTRAnamnesisGrid() {
  var pnlTRAnamnese = new Ext.Panel({
    title: "Anamnese",
    id: "tabAnamnses",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: false,
    items: [
      {
        xtype: "textarea",
        fieldLabel: "Anamnesis  ",
        name: "txtareaAnamnesis",
        id: "txtareaAnamnesis",
        width: "100%",
        flex: 1,
        // readOnly 	: true,
      },
      GetDTLTRAnamnesis(),
      textareacatatanAnemneses(),
    ],
  });
  return pnlTRAnamnese;
}

function textareacatatanAnemneses() {
  var TextAreaCatatanAnamnese = new Ext.Panel({
    title: "Catatan Fisik",
    id: "tabtextAnamnses",
    layout: "fit",
    flex: 1,
    items: [
      {
        xtype: "textarea",
        fieldLabel: "Catatan",
        name: "txtareaAnamnesiscatatan",
        id: "txtareaAnamnesiscatatan",
        readOnly: true,
      },
    ],
  });
  return TextAreaCatatanAnamnese;
}

function GetDTLTRAnamnesis() {
  var Anamfied = [
    "ID_KONDISI",
    "KONDISI",
    "SATUAN",
    "ORDERLIST",
    "KD_UNIT",
    "HASIL",
  ];
  dsTRDetailAnamneseList = new WebApp.DataStore({ fields: Anamfied });
  var gridDTLTRAnamnese = new Ext.grid.EditorGridPanel({
    id: "tabcatAnamnses",
    stripeRows: true,
    store: dsTRDetailAnamneseList,
    border: true,
    style: "padding: 4px 0px;",
    flex: 2,
    columnLines: true,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: TRAnamneseColumModel(),
    viewConfig: { forceFit: true },
  });
  // gridDTLTRAnamnese.disable();
  return gridDTLTRAnamnese;
}

function TRAnamneseColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: "colKondisi",
      header: "KONDISI",
      dataindex: "KONDISI",
      // menuDisbaled: true,
      width: 100,
      hidden: false,
    },
    {
      id: "colNilai",
      header: "NILAI",
      dataIndex: "HASIL",
      // menuDisabled: true,
      hidden: false,
      width: 80,
      editor: new Ext.form.TextField({
        id: "textnilai",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Masukan Nilai...",
        width: 50,
        anchor: "95%",
        value: 1,
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colkdunit",
      header: "KD UNIT",
      dataIndex: "KD_UNIT",
      // menuDisabled: true,
      hidden: true,
      width: 80,
    },
    {
      id: "colSatuan",
      header: "SATUAN",
      dataIndex: "SATUAN",
      // menuDisabled: true,
      width: 80,
    },
    {
      id: "colorderlist",
      header: "ORDER LIST",
      dataIndex: "ORDERLIST",
      // menuDisabled: true,
      hidden: true,
      width: 80,
    },
  ]);
}

function GetDTLTRDiagnosaGrid() {
  var pnlTRDiagnosa = new Ext.Panel({
    title: "Diagnosa",
    id: "tabDiagnosa",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    items: [
      GetDTLTRDiagnosaGridFirst(),
      FieldKeteranganDiagnosa(),
      GetDTLTRDiagnosaGridSecondIcd9(),
    ],
  });
  return pnlTRDiagnosa;
}

function FieldKeteranganDiagnosa() {
  dsCmbRwJPJDiag = new Ext.data.ArrayStore({
    id: 0,
    fields: ["Id", "displayText"],
    data: [],
  });
  var items = {
    layout: "column",
    border: false,
    height: 64,
    bodyStyle: "padding: 4px;",
    items: [
      {
        layout: "form",
        border: true,
        labelWidth: 150,
        labelAlign: "right",
        border: false,
        items: [
          (combo = new Ext.form.ComboBox({
            id: "cmbRwJPJDiag",
            typeAhead: true,
            triggerAction: "all",
            lazyRender: true,
            editable: false,
            mode: "local",
            width: 150,
            emptyText: "",
            fieldLabel: "Kode Penyakit &nbsp;",
            store: dsCmbRwJPJDiag,
            valueField: "Id",
            displayField: "displayText",
            listeners: {
              select: function () {
                if (this.getValue() != "") {
                  Ext.getCmp("catLainGroup").show();
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      if (dsTRDetailDiagnosaList.getRange()[j].data.NOTE == 2) {
                        Ext.getCmp("txtkecelakaan").setValue(
                          dsTRDetailDiagnosaList.getRange()[j].data.DETAIL
                        );
                        Ext.getCmp("txtkecelakaan").show();
                        Ext.getCmp("txtneoplasma").hide();
                        Ext.get("cbxkecelakaan").dom.checked = true;
                      } else if (
                        dsTRDetailDiagnosaList.getRange()[j].data.NOTE == 1
                      ) {
                        Ext.getCmp("txtneoplasma").setValue(
                          dsTRDetailDiagnosaList.getRange()[j].data.DETAIL
                        );
                        Ext.getCmp("txtneoplasma").show();
                        Ext.getCmp("txtkecelakaan").hide();
                        Ext.get("cbxneoplasma").dom.checked = true;
                      } else {
                        Ext.get("cbxlain").dom.checked = true;
                        Ext.getCmp("txtkecelakaan").hide();
                        Ext.getCmp("txtneoplasma").hide();
                      }
                    }
                  }
                }
              },
            },
          })),
        ],
      },
      {
        layout: "form",
        border: true,
        labelWidth: 150,
        labelAlign: "right",
        border: false,
        items: [
          {
            xtype: "radiogroup",
            width: 300,
            fieldLabel: "Catatan Lain &nbsp;",
            id: "catLainGroup",
            name: "mycbxgrp",
            columns: 3,
            items: [
              {
                id: "cbxlain",
                boxLabel: "Lain-lain",
                name: "mycbxgrp",
                width: 70,
                inputValue: 1,
              },
              {
                id: "cbxneoplasma",
                boxLabel: "Neoplasma",
                name: "mycbxgrp",
                width: 100,
                inputValue: 2,
              },
              {
                id: "cbxkecelakaan",
                boxLabel: "Kecelakaan",
                name: "mycbxgrp",
                width: 100,
                inputValue: 3,
              },
            ],
            listeners: {
              change: function (radiogroup, radio) {
                if (Ext.getDom("cbxlain").checked == true) {
                  Ext.getCmp("txtneoplasma").hide();
                  Ext.getCmp("txtkecelakaan").hide();
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      dsTRDetailDiagnosaList.getRange()[j].data.DETAIL = "";
                      dsTRDetailDiagnosaList.getRange()[j].data.NOTE = 0;
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                      break;
                    }
                  }
                } else if (Ext.getDom("cbxneoplasma").checked == true) {
                  Ext.getCmp("txtneoplasma").show();
                  Ext.getCmp("txtneoplasma").setValue("");
                  Ext.getCmp("txtkecelakaan").hide();
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      Ext.getCmp("txtneoplasma").setValue(
                        dsTRDetailDiagnosaList.getRange()[j].data.DETAIL
                      );
                      dsTRDetailDiagnosaList.getRange()[j].data.NOTE = 1;
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                      break;
                    }
                  }
                } else if (Ext.getDom("cbxkecelakaan").checked == true) {
                  Ext.getCmp("txtneoplasma").hide();
                  Ext.getCmp("txtkecelakaan").show();
                  Ext.getCmp("txtkecelakaan").setValue("");
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      Ext.getCmp("txtkecelakaan").setValue(
                        dsTRDetailDiagnosaList.getRange()[j].data.DETAIL
                      );
                      dsTRDetailDiagnosaList.getRange()[j].data.NOTE = 2;
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                      break;
                    }
                  }
                }
              },
            },
          },
        ],
      },
      {
        layout: "form",
        border: true,
        labelWidth: 150,
        labelAlign: "right",
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Neoplasma &nbsp;",
            name: "txtneoplasma",
            id: "txtneoplasma",
            hidden: true,
            width: 600,
            listeners: {
              blur: function () {
                if (Ext.getCmp("cmbRwJPJDiag").getValue() != "") {
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      dsTRDetailDiagnosaList.getRange()[j].data.DETAIL =
                        this.getValue();
                      dsTRDetailDiagnosaList.getRange()[j].data.NOTE = 1;
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                    }
                  }
                }
              },
            },
          },
          {
            xtype: "textfield",
            fieldLabel: "Kecelakaan / Keracunan &nbsp;",
            name: "txtkecelakaan",
            id: "txtkecelakaan",
            hidden: true,
            width: 600,
            listeners: {
              blur: function () {
                if (Ext.getCmp("cmbRwJPJDiag").getValue() != "") {
                  for (
                    var j = 0, jLen = dsTRDetailDiagnosaList.getRange().length;
                    j < jLen;
                    j++
                  ) {
                    if (
                      dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT ==
                      Ext.getCmp("cmbRwJPJDiag").getValue()
                    ) {
                      dsTRDetailDiagnosaList.getRange()[j].data.DETAIL =
                        this.getValue();
                      dsTRDetailDiagnosaList.getRange()[j].data.NOTE = 2;
                      Ext.getCmp("tabDiagnosaGrid").getView().refresh();
                    }
                  }
                }
              },
            },
          },
        ],
      },
    ],
  };
  return items;
}

PenataJasaRJ.form.Class.diagnosa = Ext.data.Record.create([
  { name: "KD_PENYAKIT", mapping: "KD_PENYAKIT" },
  { name: "PENYAKIT", mapping: "PENYAKIT" },
  { name: "KD_PASIEN", mapping: "KD_PASIEN" },
  { name: "URUT", mapping: "URUT" },
  { name: "URUT_MASUK", mapping: "URUT_MASUK" },
  { name: "TGL_MASUK", mapping: "TGL_MASUK" },
  { name: "KASUS", mapping: "KASUS" },
  { name: "STAT_DIAG", mapping: "STAT_DIAG" },
  { name: "NOTE", mapping: "NOTE" },
]);

function GetDTLTRDiagnosaGridFirst() {
  var fldDetail = [
    "KD_PENYAKIT",
    "PENYAKIT",
    "KD_PASIEN",
    "URUT",
    "URUT_MASUK",
    "TGL_MASUK",
    "KASUS",
    "STAT_DIAG",
    "NOTE",
    "DETAIL",
  ];
  dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail });
  PenataJasaRJ.ds2 = dsTRDetailDiagnosaList;
  RefreshDataSetDiagnosa(
    PenataJasaRJ.s1.data.KD_PASIEN,
    PenataJasaRJ.s1.data.KD_UNIT,
    PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI
  );
  PenataJasaRJ.grid2 = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "tabDiagnosaGrid",
    store: PenataJasaRJ.ds2,
    border: true,
    columnLines: true,
    autoScroll: true,
    flex: 1,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
          CurrentDiagnosa.row = row;
          CurrentDiagnosa.data = cellSelecteddeskripsi;
        },
      },
    }),
    tbar: [
      {
        text: "Tambah ICD 10",
        id: "btnLookupDiagnosa_PJ_RWJ",
        tooltip: nmLookup,
        iconCls: "add",
        handler: function () {
          PenataJasaRJ.ds2.insert(
            PenataJasaRJ.ds2.getCount(),
            PenataJasaRJ.func.getNullDiagnosa()
          );
        },
      },
      {
        text: "Simpan",
        id: "btnSimpanDiagnosa_PJ_RWJ",
        tooltip: nmSimpan,
        iconCls: "save",
        handler: function () {
          if (dsTRDetailDiagnosaList.getCount() > 0) {
            var e = false;
            for (
              var i = 0, iLen = dsTRDetailDiagnosaList.getCount();
              i < iLen;
              i++
            ) {
              var o = dsTRDetailDiagnosaList.getRange()[i].data;
              if (o.STAT_DIAG == "" || o.STAT_DIAG == null) {
                PenataJasaRJ.alertError(
                  "Diagnosa : Diagnosa Pada Baris Ke-" +
                    (i + 1) +
                    " Harus Diisi.",
                  "Peringatan"
                );
                e = true;
                break;
              }
              if (o.KASUS == "" || o.KASUS == null) {
                PenataJasaRJ.alertError(
                  "Diagnosa : Kasus Pada Baris Ke-" + (i + 1) + " Harus Diisi.",
                  "Peringatan"
                );
                e = true;
                break;
              }
            }
            if (e == false) {
              Datasave_Diagnosa(false);
            }
          }
        },
      },
      {
        id: "btnHpsBrsDiagnosa_PJ_RWJ",
        text: "Hapus item",
        tooltip: "Hapus Baris",
        iconCls: "RemoveRow",
        handler: function () {
          if (dsTRDetailDiagnosaList.getCount() > 0) {
            if (cellSelecteddeskripsi != undefined) {
              if (CurrentDiagnosa != undefined) {
                HapusBarisDiagnosa();
              }
            } else {
              ShowPesanWarning("Pilih record ", "Hapus data");
            }
          }
        },
      },
      "-",
      {
        id: "btnHistoryDiagnosa_PJ_RWJ",
        text: "History diagnosa",
        tooltip: "History diagnosa",
        iconCls: "find",
        handler: function () {
          LookupLastHistoryDiagnosa_RWJ();
        },
      },
    ],
    cm: TRDiagnosaColumModel(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.grid2;
}

function TRDiagnosaColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: Nci.getId(),
      header: "No.ICD 10",
      dataIndex: "KD_PENYAKIT",
      width: 70,
      menuDisabled: true,
      hidden: false,
      editor: (PenataJasaRJ.form.ComboBox.penyakit =
        Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.kdpenyakit,
          onEnter: function (a) {
            //alert(a.getValue());
            Ext.Ajax.request({
              method: "POST",
              url: baseURL + "index.php/main/functionRWJ/getNoIcdPenyakit",
              params: {
                text: a.getValue(),
              },
              success: function (o) {
                var cst = Ext.decode(o.responseText);
                if (cst.listData.length > 0) {
                  var line =
                    PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
                  PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT =
                    cst.listData[0].kd_penyakit;
                  PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT =
                    cst.listData[0].penyakit;
                  PenataJasaRJ.grid2.getView().refresh();
                  dsCmbRwJPJDiag.loadData([], false);
                  for (
                    var i = 0, iLen = PenataJasaRJ.ds2.getCount();
                    i < iLen;
                    i++
                  ) {
                    var recs = [],
                      recType = dsCmbRwJPJDiag.recordType;
                    var o = PenataJasaRJ.ds2.getRange()[i].data;
                    recs.push(
                      new recType({
                        Id: o.KD_PENYAKIT,
                        displayText: o.KD_PENYAKIT,
                      })
                    );
                    dsCmbRwJPJDiag.add(recs);
                  }
                }
              },
              error: function (jqXHR, exception) {
                Ext.getCmp("textAreaKepesertaan").setValue(
                  "Terjadi kesalahan dari server"
                );
              },
            });
          },
          select: function (a, b, c) {
            var line = PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
            PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT =
              b.data.kd_penyakit;
            PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT = b.data.penyakit;
            PenataJasaRJ.grid2.getView().refresh();
            dsCmbRwJPJDiag.loadData([], false);
            for (var i = 0, iLen = PenataJasaRJ.ds2.getCount(); i < iLen; i++) {
              var recs = [],
                recType = dsCmbRwJPJDiag.recordType;
              var o = PenataJasaRJ.ds2.getRange()[i].data;
              recs.push(
                new recType({
                  Id: o.KD_PENYAKIT,
                  displayText: o.KD_PENYAKIT,
                })
              );
              dsCmbRwJPJDiag.add(recs);
            }
          },
          insert: function (o) {
            return {
              kd_penyakit: o.kd_penyakit,
              penyakit: o.penyakit,
              text:
                '<table style="font-size: 11px;"><tr><td width="50">' +
                o.kd_penyakit +
                '</td><td width="200">' +
                o.penyakit +
                "</td></tr></table>",
            };
          },
          url: baseURL + "index.php/main/functionRWJ/getNoIcdPenyakit",
          valueField: "penyakit",
          displayField: "text",
          listWidth: 250,
        })),
    },
    {
      id: Nci.getId(),
      header: "Penyakit",
      dataIndex: "PENYAKIT",
      menuDisabled: true,
      width: 200,
      editor: (PenataJasaRJ.form.ComboBox.tindakan =
        Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.penyakit,
          select: function (a, b, c) {
            var line = PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
            PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT =
              b.data.kd_penyakit;
            PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT = b.data.penyakit;
            PenataJasaRJ.grid2.getView().refresh();
            dsCmbRwJPJDiag.loadData([], false);
            for (var i = 0, iLen = PenataJasaRJ.ds2.getCount(); i < iLen; i++) {
              var recs = [],
                recType = dsCmbRwJPJDiag.recordType;
              var o = PenataJasaRJ.ds2.getRange()[i].data;
              recs.push(
                new recType({
                  Id: o.KD_PENYAKIT,
                  displayText: o.KD_PENYAKIT,
                })
              );
              dsCmbRwJPJDiag.add(recs);
            }
          },
          insert: function (o) {
            return {
              kd_penyakit: o.kd_penyakit,
              penyakit: o.penyakit,
              text:
                '<table style="font-size: 11px;"><tr><td width="50">' +
                o.kd_penyakit +
                '</td><td width="200">' +
                o.penyakit +
                "</td></tr></table>",
            };
          },
          url: baseURL + "index.php/main/functionRWJ/getPenyakit",
          valueField: "penyakit",
          displayField: "text",
          listWidth: 250,
        })),
    },
    {
      id: Nci.getId(),
      header: "kd_pasien",
      dataIndex: "KD_PASIEN",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "urut",
      dataIndex: "URUT",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "urut masuk",
      dataIndex: "URUT_MASUK",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "tgl masuk",
      dataIndex: "TGL_MASUK",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "Diagnosa",
      width: 130,
      menuDisabled: true,
      dataIndex: "STAT_DIAG",
      editor: new Ext.form.ComboBox({
        id: Nci.getId(),
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Silahkan Pilih...",
        width: 50,
        anchor: "95%",
        value: 1,
        store: new Ext.data.ArrayStore({
          id: 0,
          fields: ["Id", "displayText"],
          data: [
            [1, "Diagnosa Awal"],
            [2, "Diagnosa Utama"],
            [3, "Komplikasi"],
            [4, "Diagnosa Sekunder"],
          ],
        }),
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colKasusDiagnosa",
      header: "Kasus",
      width: 130,
      menuDisabled: true,
      dataIndex: "KASUS",
      editor: new Ext.form.ComboBox({
        id: "cboKasus",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        emptyText: "Silahkan Pilih...",
        width: 50,
        anchor: "95%",
        value: 1,
        store: new Ext.data.ArrayStore({
          id: 0,
          fields: ["Id", "displayText"],
          data: [
            [1, "Baru"],
            [2, "Lama"],
          ],
        }),
        valueField: "displayText",
        displayField: "displayText",
        value: "",
        listeners: {},
      }),
    },
    {
      id: "colNote",
      header: "Note",
      dataIndex: "NOTE",
      width: 70,
      menuDisabled: true,
      hidden: true,
    },
    {
      id: "colKdProduk",
      header: "Detail",
      dataIndex: "DETAIL",
      width: 70,
      menuDisabled: true,
      hidden: true,
    },
  ]);
}

function GetDTLTRDiagnosaGridSecondIcd9() {
  var fldDetail = ["kd_icd9", "deskripsi", "urut"];
  dsTRDetailDiagnosaListIcd9 = new WebApp.DataStore({ fields: fldDetail });
  //PenataJasaRJ.ds2=dsTRDetailDiagnosaListIcd9;
  // RefreshDataSetDiagnosa(PenataJasaRJ.s1.data.KD_PASIEN,PenataJasaRJ.s1.data.KD_UNIT,PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI);
  PenataJasaRJ.gridIcd9 = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "tabDiagnosaICD9Grid",
    store: dsTRDetailDiagnosaListIcd9,
    border: true,
    columnLines: true,
    flex: 1,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          cellSelecteddeskripsiIcd9 = dsTRDetailDiagnosaListIcd9.getAt(row);
          CurrentDiagnosaIcd9.row = row;
          CurrentDiagnosaIcd9.data = cellSelecteddeskripsiIcd9;
        },
      },
    }),
    tbar: [
      {
        text: "Tambah ICD 9",
        id: "BtnTambahTindakanTrPenJasRWJ",
        iconCls: "add",
        border: true,
        handler: function () {
          var records = new Array();
          records.push(new dsTRDetailDiagnosaListIcd9.recordType());
          dsTRDetailDiagnosaListIcd9.add(records);
        },
      },
      {
        text: "Simpan",
        id: "BtnSimpanTindakanTrPenJasRWJ",
        iconCls: "save",
        border: true,
        handler: function () {
          datasave_TrPenJasRWJ();
        },
      },
      {
        id: "btnHpsBrsIcd9_RWJ",
        text: "Hapus item",
        tooltip: "Hapus Baris",
        iconCls: "RemoveRow",
        handler: function () {
          if (dsTRDetailDiagnosaListIcd9.getCount() > 0) {
            if (cellSelecteddeskripsiIcd9 != undefined) {
              if (CurrentDiagnosaIcd9 != undefined) {
                var line =
                  PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
                var o = dsTRDetailDiagnosaListIcd9.getRange()[line].data;
                var vkd_icd9 = o.kd_icd9;
                var vurut = o.urut;
                if (dsTRDetailDiagnosaListIcd9.getCount() > 0) {
                  Ext.Msg.confirm(
                    "Warning",
                    "Apakah data ini akan dihapus?",
                    function (button) {
                      if (button == "yes") {
                        if (
                          dsTRDetailDiagnosaListIcd9.getRange()[line].data
                            .urut != undefined
                        ) {
                          Ext.Ajax.request({
                            url:
                              baseURL +
                              "index.php/main/functionRWJ/hapusBarisGridIcd",
                            params: {
                              kd_pasien: Ext.getCmp(
                                "txtNoMedrecDetransaksi"
                              ).getValue(),
                              kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
                              tgl_masuk: Ext.getCmp(
                                "dtpTanggalDetransaksi"
                              ).getValue(),
                              urut_masuk:
                                Ext.getCmp("txtKdUrutMasuk").getValue(),
                              no_transaksi: Ext.getCmp(
                                "txtNoTransaksiKasirrwj"
                              ).getValue(),
                              kd_kasir: currentKdKasirRWJ,
                              kd_icd9: o.kd_icd9,
                              urut: o.urut,
                            },
                            failure: function (o) {
                              ShowPesanErrorRWJ("Hubungi Admin", "Error");
                            },
                            success: function (o) {
                              var cst = Ext.decode(o.responseText);
                              if (cst.success === true) {
                                dsTRDetailDiagnosaListIcd9.removeAt(line);
                                PenataJasaRJ.gridIcd9.getView().refresh();
                                //hapusICD9_SQL(vkd_icd9,vurut);
                              } else {
                                ShowPesanErrorRWJ(
                                  "Gagal menghapus data ini",
                                  "Error"
                                );
                              }
                            },
                          });
                        } else {
                          dsTRDetailDiagnosaListIcd9.removeAt(line);
                          PenataJasaRJ.gridIcd9.getView().refresh();
                        }
                      }
                    }
                  );
                } else {
                  ShowPesanErrorRWJ(
                    "Tidak ada data yang dapat dihapus",
                    "Error"
                  );
                }
              }
            } else {
              ShowPesanWarningRWJ("Pilih record ", "Hapus data");
            }
          }
        },
      },
    ],
    cm: TRDiagnosaIcd9ColumModel(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridIcd9;
}

function TRDiagnosaIcd9ColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: Nci.getId(),
      header: "No.ICD 9",
      dataIndex: "kd_icd9",
      width: 70,
      menuDisabled: true,
      hidden: false,
      editor: (PenataJasaRJ.form.ComboBox.kdicd9 =
        Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.kdIcd9,
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
            dsTRDetailDiagnosaListIcd9.getRange()[line].data.kd_icd9 =
              b.data.kd_icd9;
            dsTRDetailDiagnosaListIcd9.getRange()[line].data.deskripsi =
              b.data.deskripsi;
            PenataJasaRJ.gridIcd9.getView().refresh();
          },
          insert: function (o) {
            return {
              kd_icd9: o.kd_icd9,
              deskripsi: o.deskripsi,
              text:
                '<table style="font-size: 11px;"><tr><td width="50">' +
                o.kd_icd9 +
                '</td><td width="200">' +
                o.deskripsi +
                "</td></tr></table>",
            };
          },
          url: baseURL + "index.php/main/functionRWJ/getIcd9",
          valueField: "kd_icd9",
          displayField: "text",
          listWidth: 250,
        })),
    },
    {
      id: Nci.getId(),
      header: "Deskripsi ICD 9",
      dataIndex: "deskripsi",
      menuDisabled: true,
      width: 200,
      editor: (PenataJasaRJ.form.ComboBox.deskripsi =
        Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.deskripsi,
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
            dsTRDetailDiagnosaListIcd9.getRange()[line].data.kd_icd9 =
              b.data.kd_icd9;
            dsTRDetailDiagnosaListIcd9.getRange()[line].data.deskripsi =
              b.data.deskripsi;
            PenataJasaRJ.gridIcd9.getView().refresh();
          },
          insert: function (o) {
            return {
              kd_icd9: o.kd_icd9,
              deskripsi: o.deskripsi,
              text:
                '<table style="font-size: 11px;"><tr><td width="50">' +
                o.kd_icd9 +
                '</td><td width="200">' +
                o.deskripsi +
                "</td></tr></table>",
            };
          },
          url: baseURL + "index.php/main/functionRWJ/getIcd9",
          valueField: "deskripsi",
          displayField: "text",
          listWidth: 250,
        })),
    },
    {
      header: "Urut",
      dataIndex: "urut",
      hidden: true,
    },
  ]);
}

function form_histori() {
  var form = new Ext.form.FormPanel({
    baseCls: "x-plain",
    labelWidth: 55,
    url: "save-form.php",
    defaultType: "textfield",
    items: [
      {
        x: 0,
        y: 60,
        xtype: "textarea",
        id: "TxtHistoriDeleteDataPasien",
        hideLabel: true,
        name: "msg",
        anchor: "100% 100%",
      },
    ],
  });
}

function TambahBarisRWJ() {
  var x = true;
  if (x === true) {
    var p = RecordBaruRWJ();
    dsTRDetailKasirRWJList.insert(dsTRDetailKasirRWJList.getCount(), p);
  }
}

// function HapusBarisRWJ(){
// if ( cellSelecteddeskripsi != undefined ){
// if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != ''){
// var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
// if (btn == 'ok'){
// variablehistori=combo;
// DataDeleteKasirRWJDetail();
// dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
// }
// });

// }else{
// dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
// }
// }
// }
function HapusBarisPJasaRWJ() {
  var dlg = Ext.MessageBox.prompt(
    "Keterangan",
    "Masukan alasan penghapusan:",
    function (btn, combo) {
      if (btn == "ok") {
        variablehistori = combo;
        senderProdukDeletePJasaRWJ(
          currentKdKasirRWJ,
          cellSelecteddeskripsi.data.KD_PRODUK,
          Ext.get("txtNoTransaksiKasirrwj").dom.value,
          cellSelecteddeskripsi.data.URUT,
          cellSelecteddeskripsi.data.TGL_TRANSAKSI
        );
      }
    }
  );
}
function DataDeleteKasirRWJDetail() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/DeleteDataObj",
    params: getParamDataDeleteKasirRWJDetail(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoRWJ("Data berhasil dihapus", "Information");
        //deleteKasirRWJDetail_SQL();
        dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        cellSelecteddeskripsi = undefined;
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        AddNewKasirRWJ = false;
      } else if (cst.success === false && cst.produktr === true) {
        ShowPesanWarningRWJ(
          "Produk Transfer Tidak dapat dihapus",
          nmHeaderHapusData
        );
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        AddNewKasirRWJ = false;
      } else {
        ShowPesanWarningRWJ(nmPesanHapusError, nmHeaderHapusData);
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        AddNewKasirRWJ = false;
      }
    },
  });
}

function getParamDataDeleteKasirRWJDetail() {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
    TrTglTransaksi: CurrentKasirRWJ.data.data.TGL_TRANSAKSI,
    TrKdPasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    TrKdNamaPasien: Ext.get("txtNamaPasienDetransaksi").getValue(),
    TrKdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    TrNamaUnit: Ext.get("txtNamaUnit").getValue(),
    Uraian: CurrentKasirRWJ.data.data.DESKRIPSI2,
    AlasanHapus: variablehistori,
    TrHarga: CurrentKasirRWJ.data.data.HARGA,
    TrKdProduk: CurrentKasirRWJ.data.data.KD_PRODUK,
    RowReq: CurrentKasirRWJ.data.data.URUT,
    UrutMasuk: CurrentUrutMasuk,
    Hapus: 2,
  };
  return params;
}

function getParamDataupdateKasirRWJDetail() {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
    RowReq: CurrentKasirRWJ.data.data.URUT,
    Qty: CurrentKasirRWJ.data.data.QTY,
    Ubah: 1,
  };
  return params;
}

function GetDTLTRRWJGrid_order_resep(data) {
  var tab_order_obat = new Ext.Panel({
    title: "Order Obat",
    id: "tabOrderObat",
    border: false,
    layout: {
      type: "vbox",
      align: "stretch",
    },
    items: [GetDTLTRRWJGridFirst(data)],
    tbar: [
      {
        text: "Tambah Obat",
        id: "BtnTambahObatTrKasirRwj",
        iconCls: "add",
        handler: function () {
          PenataJasaRJ.dsGridObat.insert(
            PenataJasaRJ.dsGridObat.getCount(),
            PenataJasaRJ.nullGridObat()
          );
          var row = PenataJasaRJ.dsGridObat.getCount() - 1;
					PenataJasaRJ.pj_req__obt.startEditing(row, 6);
          setTimeout(function () {
            var row = dsPjTrans2.getCount() - 1;
            PenataJasaRJ.pj_req__obt.startEditing(row, 4);
          }, 200);
        },
      },
      {
        text: "Simpan",
        id: "simpan_order_resep_PJRWJ",
        iconCls: "save",
        handler: function () {
          var listNoRacik = {};
          var listObat = {};
          for (
            var i = 0, iLen = PenataJasaRJ.dsGridObat.getRange().length;
            i < iLen;
            i++
          ) {
            var o = PenataJasaRJ.dsGridObat.getRange()[i].data;
            if (listObat[o.kd_prd] !== undefined) {
              PenataJasaRJ.alertError(
                'Terapi Obat : "' + o.nama_obat + '" Tidak Boleh Sama.',
                "Peringatan"
              );
              e = true;
              break;
            }
            if (o.nama_obat == "" || o.nama_obat == null) {
              PenataJasaRJ.alertError(
                'Terapi Obat : "Nama Obat" Pada Baris Ke-' +
                  (i + 1) +
                  ", Wajib Diisi.",
                "Peringatan"
              );
              e = true;
              break;
            }
            if (o.jumlah == "" || o.jumlah == 0 || o.jumlah == null) {
              PenataJasaRJ.alertError(
                'Terapi Obat : "Qty" Pada Baris Ke-' +
                  (i + 1) +
                  ", Wajib Diisi.",
                "Peringatan"
              );
              e = true;
              break;
            }
            if (o.verified == "" || o.verified == null) {
              PenataJasaRJ.alertError(
                'Terapi Obat : "Verified" Baris Ke-' +
                  (i + 1) +
                  ", Wajib Diisi.",
                "Peringatan"
              );
              e = true;
              break;
            }
            if (o.no_racik == "" || o.no_racik == undefined) {
              if (o.racikan == true || o.racikan == 1) {
                PenataJasaRJ.alertError(
                  'Racikan : "Ya", No. Racik pada baris Ke-' +
                    (i + 1) +
                    ", Wajib Diisi.",
                  "Peringatan"
                );
                e = true;
                break;
              }
            }
            listObat[o.kd_prd] = true;
          }
          Datasave_KasirRWJ(false, false);
        },
      },
      {
        text: "Hapus",
        id: "BtnHapusObatTrKasirRwj",
        iconCls: "RemoveRow",
        handler: function () {
          var line =
            PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
          var o = PenataJasaRJ.dsGridObat.getRange()[line].data;
          Ext.Msg.confirm(
            "Warning",
            "Apakah data obat ini akan dihapus?",
            function (button) {
              if (button == "yes") {
                if (
                  PenataJasaRJ.dsGridObat.getRange()[line].data.kd_prd !=
                    undefined &&
                  PenataJasaRJ.dsGridObat.getRange()[line].data.urut !=
                    undefined &&
                  PenataJasaRJ.dsGridObat.getRange()[line].data.urut != null &&
                  PenataJasaRJ.dsGridObat.getRange()[line].data.urut != ""
                ) {
                  Ext.Ajax.request({
                    url:
                      baseURL + "index.php/main/functionRWJ/hapusBarisGridObat",
                    params: {
                      kd_prd: o.kd_prd,
                      id_mrresep: PenataJasaRJ.var_id_mrresep,
                      urut: o.urut,
                      no_racik: o.no_racik,
                    },
                    failure: function (o) {
                      ShowPesanErrorRWJ("Hubungi Admin", "Error");
                    },
                    success: function (o) {
                      var cst = Ext.decode(o.responseText);
                      if (cst.success === true) {
                        //hapusBarisGridResepOnline_SQL(o.kd_prd,PenataJasaRJ.var_id_mrresep,o.urut);
                        PenataJasaRJ.dsGridObat.removeAt(line);
                        PenataJasaRJ.gridObat.getView().refresh();
                      } else {
                        ShowPesanErrorRWJ(
                          "Gagal melakukan penghapusan",
                          "Error"
                        );
                      }
                    },
                  });
                } else {
                  PenataJasaRJ.dsGridObat.removeAt(line);
                  PenataJasaRJ.gridObat.getView().refresh();
                }
              }
            }
          );
        },
      },
    ],
  });
  return tab_order_obat;
}

function GetDTLTRRWJGrid(data) {
  var tabTransaksi = new Ext.Panel({
    title: "Tindakan",
    id: "tabTransaksi",
    layout: {
      type: "vbox",
      align: "stretch",
    },
    border: true,
    items: [GetDTLTRRWJGridSecond(data)],
    tbar: [
      {
        text: "Tambah item",
        id: "btnLookupRWJ",
        tooltip: nmLookup,
        iconCls: "add",
        handler: function () {
          PenataJasaRJ.dsGridTindakan.insert(
            PenataJasaRJ.dsGridTindakan.getCount(),
            PenataJasaRJ.func.getNullProduk()
          );
          var row = PenataJasaRJ.dsGridTindakan.getCount() - 1;
					PenataJasaRJ.form.Grid.produk.startEditing(row, 5);
        },
      },
      {
        text: "Simpan",
        id: "btnSimpanRWJ_AG",
        tooltip: nmSimpan,
        iconCls: "save",
        handler: function () {
          var e = false;
          if (PenataJasaRJ.dsGridTindakan.getRange().length > 0) {
            for (
              var i = 0, iLen = PenataJasaRJ.dsGridTindakan.getRange().length;
              i < iLen;
              i++
            ) {
              var o = PenataJasaRJ.dsGridTindakan.getRange()[i].data;
              if (o.QTY == "" || o.QTY == 0 || o.QTY == null) {
                PenataJasaRJ.alertError(
                  'Tindakan Yang Diberikan : "Qty" Pada Baris Ke-' +
                    (i + 1) +
                    ", Wajib Diisi.",
                  "Peringatan"
                );
                e = true;
                break;
              }
            }
          } else {
            PenataJasaRJ.alertError(
              "Isi Tindakan Yang Diberikan",
              "Peringatan"
            );
            e = true;
          }

          if (e == false) {
            Datasave_KasirRWJ(false, false, "tidak");
          }
        },
      },
      {
        id: "btnHpsBrsRWJ_AG",
        text: "Hapus item",
        tooltip: "Hapus Baris",
        iconCls: "RemoveRow",
        handler: function () {
          if (dsTRDetailKasirRWJList.getCount() > 0) {
            if (cellSelecteddeskripsi != undefined) {
              if (CurrentKasirRWJ != undefined) {
                HapusBarisPJasaRWJ();
              }
            } else {
              ShowPesanWarningRWJ("Pilih record ", "Hapus data");
            }
          }
        },
      },
      "-",
      {
        id: "btnLookUpEditDokterPenindak_RWJ",
        text: "Edit Pelaksana",
        iconCls: "Edit_Tr",
        //hidden : true,
        handler: function () {
          if (
            currentJasaDokterKdProduk_RWJ == "" ||
            currentJasaDokterKdProduk_RWJ == undefined
          ) {
            ShowPesanWarningRWJ(
              "Pilih item dokter penindak yang akan diedit!",
              "Error"
            );
          } else {
            if (
              currentJasaDokterUrutDetailTransaksi_RWJ == 0 ||
              currentJasaDokterUrutDetailTransaksi_RWJ == undefined
            ) {
              ShowPesanErrorRWJ(
                "Item ini belum ada dokter penindaknya!",
                "Error"
              );
            } else {
              loaddatastoredokterVisite_REVISI();
              PilihDokterLookUpPJ_RWJ_REVISI();
            }
          }
        },
      },
      "-",
      {
        id: "btnLookUpGetProduk",
        text: "Lookup Produk",
        iconCls: "Edit_Tr",
        //hidden : true,
        handler: function () {
          GetPodukLookUp();
        },
      },
    ],
    bbar: [
      { xtype: "tbspacer", width: 1100 },
      {
        xtype: "label",
        text: "Total :",
      },
      { xtype: "tbspacer", width: 10 },
      {
        xtype: "numberfield",
        id: "txtFieldTotalHarga",
        name: "txtFieldTotalHarga",
        width: 100,
        readOnly: true,
      },
    ],
  });
  return tabTransaksi;
}
function loaddatastorePenangan() {
  /*if(tmp_group_dokter==1){
		tmp_group_dokter=0;
	}
	tmp_group_dokter=0;*/
  if (tmp_group_dokter == "undefined" || tmp_group_dokter == undefined) {
    tmp_group_dokter = 0;
  }
  console.log(tmp_group_dokter);
  Ext.Ajax.request({
    url: baseURL + "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
    params: {
      groups: (tmp_group_dokter = 0),
    },
    success: function (response) {
      var cst = Ext.decode(response.responseText);
      for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
        var recs = [],
          recType = dsDataDaftarPenangan_Penjas_RWJ.recordType;
        var o = cst["data"][i];
        recs.push(new recType(o));
        dsDataDaftarPenangan_Penjas_RWJ.add(recs);
      }
      Ext.getCmp("cboPenangan_KASIRRWJ").setValue("1");
      console.log(cst["data"][0]);
      tmpKdJob = cst["data"][0].KD_JOB;
      // if(mod==null || mod=='' || mod==undefined){
      loaddatastoredokter_REVISI(rowSelectedPJRWJ, null, "");
      // }else if(mod=='LAB'){
      // 	loaddatastoredokter_REVISI(selectedPenjasRWJLab,null,mod);
      // }else if(mod=='RAD'){
      // 	loaddatastoredokter_REVISI(selectedPenjasRWJRad,null,mod);
      // }
    },
    callback: function () {
      Ext.getCmp("txtSearchDokter_KASIRRWJ").focus();
    },
  });
}
function cboPenangan_PenjasRWJ(mod) {
  var tmpValue;
  /*if (tmpKdJob == 3) {
		tmpValue = "Perawat";
	}else{
		tmpValue = "Dokter";
	}*/
  var Field = ["KD_JOB", "KD_COMPONENT", "LABEL"];
  dsDataDaftarPenangan_Penjas_RWJ = new WebApp.DataStore({ fields: Field });
  loaddatastorePenangan(mod);
  var cboPenangan_KASIRRWJ = new Ext.form.ComboBox({
    id: "cboPenangan_KASIRRWJ",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih penangan...",
    fieldLabel: "Penangan ",
    align: "Right",
    store: dsDataDaftarPenangan_Penjas_RWJ,
    valueField: "KD_JOB",
    displayField: "LABEL",
    //anchor           : '100%',
    value: tmpValue,
    listeners: {
      select: function (a, b, c) {
        DataStorefirstGridStore_PJRWJ.removeAll();
        tmpKdJob = b.data.KD_JOB;
        console.log(b.data);
        if (mod == null || mod == "" || mod == undefined) {
          loaddatastoredokter_REVISI(rowSelectedPJRWJ, null, mod);
        } else if (mod == "LAB") {
          loaddatastoredokter_REVISI(selectedPenjasRWJLab, null, mod);
        } else if (mod == "RAD") {
          loaddatastoredokter_REVISI(selectedPenjasRWJRad, null, mod);
        }
      },
    },
  });

  return cboPenangan_KASIRRWJ;
}
function PilihDokterLookUpPJ_RWJ_REVISI(mod) {
  var FormLookUDokter_RWJ = new Ext.Window({
    xtype: "fieldset",
    layout: "fit",
    id: "winTRDokterPenindak_RWJ_REVISI",
    title: "Pilih Pelaksana",
    closeAction: "destroy",
    width: 600,
    height: 400,
    border: true,
    resizable: false,
    constrain: true,
    modal: true,
    items: [
      {
        layout: {
          type: "hbox",
          align: "stretch",
        },
        bodyStyle: "padding: 4px;",
        border: false,
        items: [
          {
            border: false,
            flex: 1,
            layout: {
              type: "vbox",
              align: "stretch",
            },
            items: [cboPenangan_PenjasRWJ(mod), seconGridPenerimaan(mod)],
          },
          {
            border: false,
            flex: 1,
            layout: {
              type: "vbox",
              align: "stretch",
            },
            style: "padding-left: 4px;",
            items: [
              {
                xtype: "textfield",
                anchor: "100%",
                id: "txtSearchDokter_KASIRRWJ",
                name: "txtSearchDokter_KASIRRWJ",
                listeners: {
                  specialkey: function () {
                    if (
                      Ext.EventObject.getKey() === 13 ||
                      Ext.EventObject.getKey() === 9
                    ) {
                      DataStorefirstGridStore_PJRWJ.removeAll();
                      tmpSearchDokter = Ext.getCmp(
                        "txtSearchDokter_KASIRRWJ"
                      ).getValue();
                      loaddatastoredokter_REVISI(
                        rowSelectedPJRWJ,
                        Ext.getCmp("txtSearchDokter_KASIRRWJ").getValue()
                      );
                    }
                  },
                },
              },
              firstGridPenerimaan(mod),
            ],
          },
        ],
      },
    ],
    tbar: [
      {
        xtype: "button",
        text: "Simpan",
        iconCls: "save",
        hideLabel: true,
        id: "BtnOktrDokter",
        handler: function () {
          //RefreshDataDetail_kasirrwi(tmpno_transaksi);

          //PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+tmpNamaDokter+")");
          FormLookUDokter_RWJ.close();
        },
      },
      "-",
    ],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {
        batal_isi_tindakan = "y";
      },
    },
  });
  FormLookUDokter_RWJ.show();
  FormLookUDokter_RWJ.center();
}

// HUDI
// 12-05-2024
function LookUp_UpdateTarifTindakan(){
  var FormLookUp_UpdateTarifTindakan = new Ext.Window({
    id: "winUpdateTarifTindakan_RWJ",
    title: "Data Tarif",
    closeAction: "destroy",
    width: 500,
    height: 220,
    border: false,
    resizable: false,
    // iconCls: "Request",
    constrain: true,
    modal: true,
    items: [
      // GridDokterTr_RWJ
    ],
    tbar: [
      {
        xtype: "button",
        text: "Simpan",
        iconCls: "save",
        hideLabel: true,
        id: "BtnUpdateTarifTindakan",
        handler: function () {
          // savetransaksi(kondisi);
        },
      },
      // "-",
    ],
    listeners: {},
  });
  FormLookUp_UpdateTarifTindakan.show();
  FormLookUp_UpdateTarifTindakan.center();
}

function seconGridPenerimaan(mod) {
  DataStoreSecondGridStore_PJRWJ.removeAll();
  var secondGrid;
  var fields = [
    { name: "KD_UNIT", mapping: "KD_UNIT" },
    { name: "NAMA_UNIT", mapping: "NAMA_UNIT" },
  ];
  secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
    fields: fieldsDokterPenindak,
    root: "data",
  });
  var cols = [
    {
      id: "KD_DOKTER",
      header: "Kode Pelaksana",
      width: 0.25,
      sortable: true,
      dataIndex: "KD_DOKTER",
      hidden: false,
    },
    { header: "Nama", width: 0.75, sortable: true, dataIndex: "NAMA" },
    { header: "JP", width: 0.25, dataIndex: "JP" },
    { header: "PRC", width: 0.25, dataIndex: "PRC" },
  ];
  secondGrid = new Ext.grid.GridPanel({
    ddGroup: "firstGridDDGroup",
    store: DataStoreSecondGridStore_PJRWJ,
    columns: cols,
    autoScroll: true,
    columnLines: true,
    border: true,
    enableDragDrop: true,
    flex: 1,
    enableDragDrop: true,
    stripeRows: true,
    autoExpandColumn: "KD_DOKTER",
    title: "Dokter/ Perawat yang menangani",
    plugins: [new Ext.ux.grid.FilterRow()],
    listeners: {
      afterrender: function (comp) {
        console.log(tmp_group_dokter);
        if (mod == null || mod == undefined || mod == "") {
          var group_dokter = 0;
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // console.log(cellSelecteddeskripsi_panatajasaigd.data.GROUP);
            group_dokter = tmp_group_dokter;
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
          }
          var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
          var secondGridDropTarget = new Ext.dd.DropTarget(
            secondGridDropTargetEl,
            {
              ddGroup: "secondGridDDGroup",
              notifyDrop: function (ddSource, e, data) {
                var records = ddSource.dragData.selections;
                var kd_dokter = "";
                for (var i = 0, iLen = records.length; i < iLen; i++) {
                  var o = records[i];
                  kd_dokter = o.data.KD_DOKTER;
                }
                console.log("1");
                console.log(cellSelecteddeskripsi);

                Ext.Ajax.request({
                  url:
                    baseURL +
                    "index.php/rawat_inap/control_visite_dokter/insertDokter",
                  params: {
                    label: Ext.getCmp("cboPenangan_KASIRRWJ").getValue(),
                    kd_job: tmpKdJob,
                    no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                    tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                    kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                    urut: rowSelectedPJRWJ.data.URUT,
                    // no_transaksi    : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
                    // tgl_transaksi   : getFormatTanggal(Ext.get('dtpTanggalDetransaksi').getValue()),
                    // kd_produk       : currentJasaDokterKdProduk_RWJ,
                    // urut            : cellSelecteddeskripsi.data.URUT,
                    kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR,
                    kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
                    kd_dokter: kd_dokter,
                    kd_tarif: cellSelecteddeskripsi.data.KD_TARIF,
                    tgl_berlaku: cellSelecteddeskripsi.data.TGL_BERLAKU,
                    group: group_dokter,
                  },
                  success: function (response) {
                    DataStoreSecondGridStore_PJRWJ.removeAll();
                    
                    loaddatastoredokter_REVISI(rowSelectedPJRWJ, null, mod);
                    loaddatastoredokterVisite_REVISI();
                    RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
                    RefreshDataKasirRWJDetai2(rowSelectedKasirRWJ.data);
                    // RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                    
                    var cst = Ext.decode(response.responseText);
                    var dataRowIndexDetail =
                      PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
                    console.log(cst);
                    if (rowSelectedPJRWJ.data.KD_PRODUK == "3") {
                      if (cst.data.JUMLAH_DOKTER > 0) {
                        PenataJasaRJ.dsGridTindakan
                          .getRange()
                          [dataRowIndexDetail].set(
                            "DESKRIPSI",
                            cst.data.DESKRIPSI +
                              " (" +
                              cst.data.DAFTAR_DOKTER +
                              ")"
                          );
                      } else {
                        PenataJasaRJ.dsGridTindakan
                          .getRange()
                          [dataRowIndexDetail].set(
                            "DESKRIPSI",
                            cst.data.DESKRIPSI
                          );
                      }
                    }
                    // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
                    PenataJasaRJ.dsGridTindakan
                      .getRange()
                      [dataRowIndexDetail].set(
                        "NAMA_DOKTER_VISITE", cst.data.DAFTAR_DOKTER);
                    PenataJasaRJ.dsGridTindakan
                      .getRange()
                      [dataRowIndexDetail].set(
                        "JUMLAH_DOKTER_VISITE",
                        cst.data.JUMLAH_DOKTER
                      );
                    Ext.each(
                      records,
                      ddSource.grid.store.remove,
                      ddSource.grid.store
                    );
                    //secondGrid.store.add(records);
                    secondGrid.getView().refresh();
                    return true;
                  },
                });
              },
            }
          );
        } else if (mod == "LAB") {
          var group_dokter = 0;
          console.log(tmp_group_dokter);
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // console.log(cellSelecteddeskripsi_panatajasaigd.data.GROUP);
            group_dokter = tmp_group_dokter;
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
          }
          var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
          var secondGridDropTarget = new Ext.dd.DropTarget(
            secondGridDropTargetEl,
            {
              ddGroup: "secondGridDDGroup",
              notifyDrop: function (ddSource, e, data) {
                var records = ddSource.dragData.selections;
                var kd_dokter = "";
                for (var i = 0, iLen = records.length; i < iLen; i++) {
                  var o = records[i];
                  kd_dokter = o.data.KD_DOKTER;
                }
                console.log("2");
                Ext.Ajax.request({
                  url:
                    baseURL +
                    "index.php/rawat_inap/control_visite_dokter/insertDokter",
                  params: {
                    label: Ext.getCmp("cboPenangan_KASIRIGD").getValue(),
                    kd_job: tmpKdJob,
                    no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                    tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                    kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                    urut: rowSelectedPJRWJ.data.URUT,
                    // no_transaksi    : selectedPenjasIGDLab.data.no_transaksi,
                    // tgl_transaksi   : selectedPenjasIGDLab.data.tgl_transaksi,
                    // kd_produk       : selectedPenjasIGDLab.data.kd_produk,
                    // urut            : selectedPenjasIGDLab.data.urut,
                    kd_kasir: "08",
                    kd_unit: "31",
                    kd_dokter: kd_dokter,
                    kd_tarif: selectedPenjasIGDLab.data.kd_tarif,
                    tgl_berlaku: selectedPenjasIGDLab.data.tgl_berlaku,
                    group: group_dokter,
                  },
                  success: function (response) {
                    DataStoreSecondGridStore_PJRWJ.removeAll();
                    loaddatastoredokterVisite_REVISI(mod);
                    // RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                    var cst = Ext.decode(response.responseText);
                    //console.log(cst);
                    var dataRowIndexDetail =
                      PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
                    // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
                    PenataJasaIGD.ds3
                      .getRange()
                      [dataRowIndexDetail].set(
                        "jumlah_dokter",
                        cst.data.JUMLAH_DOKTER
                      );
                    Ext.each(
                      records,
                      ddSource.grid.store.remove,
                      ddSource.grid.store
                    );
                    //secondGrid.store.add(records);
                    secondGrid.getView().refresh();
                    return true;
                  },
                });
              },
            }
          );
        } else if (mod == "RAD") {
          var group_dokter = 0;
          console.log(tmp_group_dokter);
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // console.log(cellSelecteddeskripsi_panatajasaigd.data.GROUP);
            group_dokter = tmp_group_dokter;
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
          }
          var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
          var secondGridDropTarget = new Ext.dd.DropTarget(
            secondGridDropTargetEl,
            {
              ddGroup: "secondGridDDGroup",
              notifyDrop: function (ddSource, e, data) {
                var records = ddSource.dragData.selections;
                var kd_dokter = "";
                for (var i = 0, iLen = records.length; i < iLen; i++) {
                  var o = records[i];
                  kd_dokter = o.data.KD_DOKTER;
                }
                console.log("3");
                Ext.Ajax.request({
                  url:
                    baseURL +
                    "index.php/rawat_inap/control_visite_dokter/insertDokter",
                  params: {
                    label: Ext.getCmp("cboPenangan_KASIRIGD").getValue(),
                    kd_job: tmpKdJob,
                    no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                    tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                    kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                    urut: rowSelectedPJRWJ.data.URUT,
                    // no_transaksi    : selectedPenjasIGDRad.data.no_transaksi,
                    // tgl_transaksi   : selectedPenjasIGDRad.data.tgl_transaksi,
                    // kd_produk       : selectedPenjasIGDRad.data.kd_produk,
                    // urut            : selectedPenjasIGDRad.data.urut,
                    kd_kasir: "10",
                    kd_unit: "31",
                    kd_dokter: kd_dokter,
                    kd_tarif: selectedPenjasIGDRad.data.kd_tarif,
                    tgl_berlaku: selectedPenjasIGDRad.data.tgl_berlaku,
                    group: group_dokter,
                  },
                  success: function (response) {
                    DataStoreSecondGridStore_PJRWJ.removeAll();
                    loaddatastoredokterVisite_REVISI(mod);
                    // RefreshRekapitulasiTindakanKasirRWI(notransaksi,kdkasirnya);
                    var cst = Ext.decode(response.responseText);
                    var dataRowIndexDetail =
                      PenataJasaIGD.pj_req_rad.getSelectionModel().selection
                        .cell[0];
                    //console.log(cst);
                    // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
                    dsIGDPJLab_IGD
                      .getRange()
                      [dataRowIndexDetail].set(
                        "jumlah_dokter",
                        cst.data.JUMLAH_DOKTER
                      );
                    Ext.each(
                      records,
                      ddSource.grid.store.remove,
                      ddSource.grid.store
                    );
                    //secondGrid.store.add(records);
                    // PenataJasaIGD.pj_req_rad.getView().refresh();
                    secondGrid.getView().refresh();
                    return true;
                  },
                });
              },
            }
          );
        }
      },
      rowdblclick: function (dataview, index, item, e) {
        console.log(cellSelecteddeskripsi.data);
        //DataStorefirstGridStore.data.items[index].data.KD_DOKTER
        if (mod == null || mod == undefined || mod == "") {
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/rawat_inap/control_visite_dokter/deleteDokter",
            params: {
              kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR,
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              // no_transaksi    : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
              // urut            : currentJasaDokterUrutDetailTransaksi_RWJ,
              // tgl_transaksi   : Ext.get('dtpTanggalDetransaksi').dom.value,
              // kd_produk       : currentJasaDokterKdProduk_RWJ,
              kd_dokter:
                DataStoreSecondGridStore_PJRWJ.data.items[index].data.KD_DOKTER,
              kd_unit: cellSelecteddeskripsi.data.KD_UNIT,
              line: DataStoreSecondGridStore_PJRWJ.data.items[index].data.LINE,
            },
            success: function (response) {
              DataStoreSecondGridStore_PJRWJ.removeAll();
                    
              loaddatastoredokter_REVISI(rowSelectedPJRWJ, null, mod);
              loaddatastoredokterVisite_REVISI();
              RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
              RefreshDataKasirRWJDetai2(rowSelectedKasirRWJ.data);

              var cst = Ext.decode(response.responseText);
              // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
              var dataRowIndexDetail = PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
              if (rowSelectedPJRWJ.data.KD_PRODUK == "3") {
                if (cst.JUMLAH_DOKTER > 0) {
                  PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set("DESKRIPSI",cst.DESKRIPSI + " (" + cst.DAFTAR_DOKTER + ")");
                } else {
                  PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set("DESKRIPSI", cst.DESKRIPSI);
                }
              }
              PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
							
              secondGrid.getView().refresh();
            },
          });
        } else if (mod == "LAB") {
          console.log(mod);
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/rawat_inap/control_visite_dokter/deleteDokter",
            params: {
              kd_kasir: "08",
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              // no_transaksi    : selectedPenjasIGDLab.data.no_transaksi,
              // urut            : selectedPenjasIGDLab.data.urut,
              // tgl_transaksi   : selectedPenjasIGDLab.data.tgl_transaksi,
              // kd_produk       : selectedPenjasIGDLab.data.kd_produk,
              kd_dokter:
                DataStoreSecondGridStore_PJRWJ.data.items[index].data.KD_DOKTER,
              kd_unit: "31",
              line: DataStoreSecondGridStore_PJRWJ.data.items[index].data.LINE,
            },
            success: function (response) {
              var cst = Ext.decode(response.responseText);
              var dataRowIndexDetail =
                PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
              // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
              PenataJasaIGD.ds3
                .getRange()
                [dataRowIndexDetail].set("jumlah_dokter", cst.JUMLAH_DOKTER);
              DataStoreSecondGridStore_PJRWJ.removeAll();
              loaddatastoredokterVisite_REVISI(mod);
            },
          });
        } else if (mod == "RAD") {
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/rawat_inap/control_visite_dokter/deleteDokter",
            params: {
              kd_kasir: "10",
              // no_transaksi    : selectedPenjasIGDRad.data.no_transaksi,
              // urut            : selectedPenjasIGDRad.data.urut,
              // tgl_transaksi   : selectedPenjasIGDRad.data.tgl_transaksi,
              // kd_produk       : selectedPenjasIGDRad.data.kd_produk,
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              kd_dokter:
                DataStoreSecondGridStore_PJRWJ.data.items[index].data.KD_DOKTER,
              kd_unit: "31",
              line: DataStoreSecondGridStore_PJRWJ.data.items[index].data.LINE,
            },
            success: function (response) {
              var cst = Ext.decode(response.responseText);
              // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
              var dataRowIndexDetail =
                PenataJasaIGD.pj_req_rad.getSelectionModel().selection.cell[0];
              dsIGDPJLab_IGD
                .getRange()
                [dataRowIndexDetail].set("jumlah_dokter", cst.JUMLAH_DOKTER);
              DataStoreSecondGridStore_PJRWJ.removeAll();
              loaddatastoredokterVisite_REVISI(mod);
            },
          });
        }
      },
      callback: function () {
        Ext.getCmp("txtSearchDokter_KASIRRWJ").focus();
      },
    },
    viewConfig: {
      forceFit: true,
    },
  });
  return secondGrid;
}
function firstGridPenerimaan(mod) {
  DataStorefirstGridStore_PJRWJ.removeAll();
  var cols = [
    {
      id: "KD_DOKTER",
      header: "Kode Pelaksana",
      width: 0.25,
      sortable: true,
      dataIndex: "KD_DOKTER",
      hidden: false,
    },
    {
      id: "NAMA",
      header: "Nama",
      width: 0.75,
      sortable: true,
      dataIndex: "NAMA",
    },
  ];
  var firstGrid;
  firstGrid = new Ext.grid.GridPanel({
    ddGroup: "secondGridDDGroup",
    store: DataStorefirstGridStore_PJRWJ,
    columns: cols,
    autoScroll: true,
    columnLines: true,
    border: true,
    enableDragDrop: true,
    stripeRows: true,
    flex: 1,
    trackMouseOver: true,
    title: "Dokter/ Perawat penangan",
    plugins: [new Ext.ux.grid.FilterRow()],
    colModel: new Ext.grid.ColumnModel([
      new Ext.grid.RowNumberer(),
      {
        id: "colNMKd_Dokter",
        header: "KD Dokter",
        dataIndex: "KD_DOKTER",
        sortable: true,
        hidden: false,
        width: 0.25,
      },
      {
        id: "colNMDokter",
        header: "Nama",
        dataIndex: "NAMA",
        sortable: true,
        width: 0.75,
      },
    ]),
    listeners: {
      afterrender: function (comp) {
        var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
        var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
          ddGroup: "firstGridDDGroup",
          notifyDrop: function (ddSource, e, data) {
            var records = ddSource.dragData.selections;
            var kd_dokter = "";
            var line = "";
            for (var i = 0, iLen = records.length; i < iLen; i++) {
              var o = records[i];
              kd_dokter = o.data.KD_DOKTER;
              line = o.data.LINE;
            }
            if (mod == null || mod == undefined || mod == "") {
              console.log(cellSelecteddeskripsi.data.KD_UNIT);
              Ext.Ajax.request({
                url:
                  baseURL +
                  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
                params: {
                  kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR,
                  no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                  tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                  kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                  urut: rowSelectedPJRWJ.data.URUT,
                  // no_transaksi    : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
                  // urut            : currentJasaDokterUrutDetailTransaksi_RWJ,
                  // tgl_transaksi   : Ext.get('dtpTanggalDetransaksi').dom.value,
                  // kd_produk       : currentJasaDokterKdProduk_RWJ,
                  kd_dokter: kd_dokter,
                  kd_unit: cellSelecteddeskripsi.data.KD_UNIT,
                  line: line,
                },
                success: function (response) {
                  DataStoreSecondGridStore_PJRWJ.removeAll();
                        
                  loaddatastoredokter_REVISI(rowSelectedPJRWJ, null, mod);
                  loaddatastoredokterVisite_REVISI();
                  RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
                  RefreshDataKasirRWJDetai2(rowSelectedKasirRWJ.data);

                  var cst = Ext.decode(response.responseText);
                  // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
                  console.log(PenataJasaRJ.form.Grid.produk.getSelectionModel());
                  var dataRowIndexDetail = PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
                  if (rowSelectedPJRWJ.data.KD_PRODUK == "3") {
                    if (cst.JUMLAH_DOKTER > 0) {
                      PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set("DESKRIPSI",cst.DESKRIPSI + " (" + cst.DAFTAR_DOKTER + ")");
                    } else {
                      PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set("DESKRIPSI", cst.DESKRIPSI);
                    }
                  }
                  PenataJasaRJ.dsGridTindakan.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
                  secondGrid.getView().refresh();
                },
              });
            } else if (mod == "LAB") {
              Ext.Ajax.request({
                url:
                  baseURL +
                  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
                params: {
                  kd_kasir: "08",
                  no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                  tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                  kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                  urut: rowSelectedPJRWJ.data.URUT,
                  // no_transaksi    : selectedPenjasIGDLab.data.no_transaksi,
                  // urut            : selectedPenjasIGDLab.data.urut,
                  // tgl_transaksi   : selectedPenjasIGDLab.data.tgl_transaksi,
                  // kd_produk       : selectedPenjasIGDLab.data.kd_produk,
                  kd_dokter: kd_dokter,
                  kd_unit: "31",
                  line: line,
                },
                success: function (response) {
                  var cst = Ext.decode(response.responseText);
                  var dataRowIndexDetail =
                    PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
                  // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
                  PenataJasaIGD.ds3
                    .getRange()
                    [dataRowIndexDetail].set(
                      "jumlah_dokter",
                      cst.JUMLAH_DOKTER
                    );
                  Ext.each(
                    records,
                    ddSource.grid.store.remove,
                    ddSource.grid.store
                  );
                  firstGrid.store.add(records);
                  firstGrid.getView().refresh();
                  return true;
                },
              });
            } else if (mod == "RAD") {
              Ext.Ajax.request({
                url:
                  baseURL +
                  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
                params: {
                  kd_kasir: "10",
                  no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
                  tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
                  kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
                  urut: rowSelectedPJRWJ.data.URUT,
                  // no_transaksi    : selectedPenjasIGDRad.data.no_transaksi,
                  // urut            : selectedPenjasIGDRad.data.urut,
                  // tgl_transaksi   : selectedPenjasIGDRad.data.tgl_transaksi,
                  // kd_produk       : selectedPenjasIGDRad.data.kd_produk,
                  kd_dokter: kd_dokter,
                  kd_unit: "31",
                  line: line,
                },
                success: function (response) {
                  var cst = Ext.decode(response.responseText);
                  // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
                  var dataRowIndexDetail =
                    PenataJasaIGD.pj_req_rad.getSelectionModel().selection
                      .cell[0];
                  dsIGDPJLab_IGD
                    .getRange()
                    [dataRowIndexDetail].set(
                      "jumlah_dokter",
                      dsIGDPJLab_IGD.getRange()[dataRowIndexDetail].data
                        .jumlah_dokter - 1
                    );
                  Ext.each(
                    records,
                    ddSource.grid.store.remove,
                    ddSource.grid.store
                  );
                  firstGrid.store.add(records);
                  firstGrid.getView().refresh();
                  return true;
                },
              });
            }
          },
        });
      },
      rowdblclick: function (dataview, index, item, e) {
        if (mod == null || mod == undefined || mod == "") {
          var group_dokter = 0;
          console.log(tmp_group_dokter);
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
            group_dokter = tmp_group_dokter;
          }
          console.log("4");
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/gawat_darurat/control_visite_dokter/insertDokter",
            params: {
              label: Ext.getCmp("cboPenangan_KASIRRWJ").getValue(),
              kd_job: tmpKdJob,
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              // no_transaksi    : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
              // tgl_transaksi   : Ext.get('dtpTanggalDetransaksi').dom.value,
              // kd_produk       : currentJasaDokterKdProduk_RWJ,
              // urut            : currentJasaDokterUrutDetailTransaksi_RWJ,
              kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR,
              kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(), //tmpkd_unit,
              kd_dokter:
                DataStorefirstGridStore_PJRWJ.data.items[index].data.KD_DOKTER,
              kd_tarif: cellSelecteddeskripsi.data.KD_TARIF,
              tgl_berlaku: cellSelecteddeskripsi.data.TGL_BERLAKU,
              group: group_dokter,
            },
            success: function (response) {
              DataStoreSecondGridStore_PJRWJ.removeAll();
              loaddatastoredokterVisite_REVISI(mod);
              //loaddatastoredokter_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI'),getValue());
              //
              //var selection = firstGrid.getView().getSelectionModel().getSelection()[0];
              //console.log(selection);
              // RefreshRekapitulasiTindakanKasirRWI(notransaksi, kdkasirnya);
              var cst = Ext.decode(response.responseText);
              console.log(dataRowIndexDetail);
              // PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
              var dataRowIndexDetail =
                PenataJasaRJ.form.Grid.produk.getSelectionModel().selection
                  .cell[0];
              if (rowSelectedPJRWJ.data.KD_PRODUK == "3") {
                if (cst.data.JUMLAH_DOKTER > 0) {
                  PenataJasaRJ.dsGridTindakan
                    .getRange()
                    [dataRowIndexDetail].set(
                      "DESKRIPSI",
                      cst.data.DESKRIPSI + " (" + cst.data.DAFTAR_DOKTER + ")"
                    );
                } else {
                  PenataJasaRJ.dsGridTindakan
                    .getRange()
                    [dataRowIndexDetail].set("DESKRIPSI", cst.data.DESKRIPSI);
                }
              }
              PenataJasaRJ.dsGridTindakan
                .getRange()
                [dataRowIndexDetail].set(
                  "JUMLAH_DOKTER",
                  cst.data.JUMLAH_DOKTER
                );
              /*Ext.each(index, firstGrid.store.remove, firstGrid.store);
							firstGrid.store.remove(index);
							firstGrid.getView().refresh();*/
              var selected = firstGrid.getSelectionModel().getSelections();

              if (selected.length > 0) {
                for (var i = 0; i < selected.length; i++) {
                  DataStorefirstGridStore_PJIGD.remove(selected[i]);
                }
              }
            },
          });
        } else if (mod == "LAB") {
          var group_dokter = 0;
          console.log(tmp_group_dokter);
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
            group_dokter = tmp_group_dokter;
          }
          console.log("5");
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/gawat_darurat/control_visite_dokter/insertDokter",
            params: {
              label: Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
              kd_job: tmpKdJob,
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              // no_transaksi    : selectedPenjasIGDLab.data.no_transaksi,
              // tgl_transaksi   : selectedPenjasIGDLab.data.tgl_transaksi,
              // kd_produk       : selectedPenjasIGDLab.data.kd_produk,
              // urut            : selectedPenjasIGDLab.data.urut,
              kd_kasir: "08",
              kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
              kd_dokter:
                DataStorefirstGridStore_PJRWI.data.items[index].data.KD_DOKTER,
              kd_tarif: selectedPenjasIGDLab.data.kd_tarif,
              tgl_berlaku: selectedPenjasIGDLab.data.tgl_berlaku,
              group: group_dokter,
            },
            success: function (response) {
              DataStoreSecondGridStore_PJRWJ.removeAll();
              loaddatastoredokterVisite_REVISI(mod);
              var cst = Ext.decode(response.responseText);
              var dataRowIndexDetail =
                PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
              PenataJasaIGD.ds3
                .getRange()
                [dataRowIndexDetail].set(
                  "jumlah_dokter",
                  cst.data.JUMLAH_DOKTER
                );
              var selected = firstGrid.getSelectionModel().getSelections();

              if (selected.length > 0) {
                for (var i = 0; i < selected.length; i++) {
                  DataStorefirstGridStore_PJIGD.remove(selected[i]);
                }
              }
            },
          });
        } else if (mod == "RAD") {
          var group_dokter = 0;
          console.log(tmp_group_dokter);
          if (
            tmp_group_dokter != 0 ||
            tmp_group_dokter != "" ||
            tmp_group_dokter != "undefined" ||
            tmp_group_dokter != null
          ) {
            // group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
            group_dokter = tmp_group_dokter;
          }
          console.log("6");
          Ext.Ajax.request({
            url:
              baseURL +
              "index.php/gawat_darurat/control_visite_dokter/insertDokter",
            params: {
              label: Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
              kd_job: tmpKdJob,
              no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
              tgl_transaksi: rowSelectedPJRWJ.data.TGL_TRANSAKSI,
              kd_produk: rowSelectedPJRWJ.data.KD_PRODUK,
              urut: rowSelectedPJRWJ.data.URUT,
              kd_kasir: "10",
              kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
              kd_dokter:
                DataStorefirstGridStore_PJRWI.data.items[index].data.KD_DOKTER,
              kd_tarif: selectedPenjasIGDRad.data.kd_tarif,
              tgl_berlaku: selectedPenjasIGDRad.data.tgl_berlaku,
              group: group_dokter,
            },
            success: function (response) {
              DataStoreSecondGridStore_PJRWJ.removeAll();
              loaddatastoredokterVisite_REVISI(mod);

              var cst = Ext.decode(response.responseText);
              var dataRowIndexDetail =
                PenataJasaIGD.pj_req_rad.getSelectionModel().selection.cell[0];
              dsIGDPJLab_IGD
                .getRange()
                [dataRowIndexDetail].set(
                  "jumlah_dokter",
                  cst.data.JUMLAH_DOKTER
                );
              var selected = firstGrid.getSelectionModel().getSelections();

              if (selected.length > 0) {
                for (var i = 0; i < selected.length; i++) {
                  DataStorefirstGridStore_PJIGD.remove(selected[i]);
                }
              }
            },
          });
        }
      },
    },
    viewConfig: {
      forceFit: true,
    },
  });
  return firstGrid;
}
function loaddatastoredokter_REVISI(params, dokter = null, mod) {
  if (mod == null || mod == "" || mod == undefined) {
    // dsDataDokterPenindak_PJ_IGD.removeAll();
    DataStorefirstGridStore_PJRWJ.removeAll();
    var tmp_urut;
    if (typeof params.data.URUT == "undefined") {
      tmp_urut = params.data.URUT_MASUK;
    } else {
      tmp_urut = params.data.URUT;
    }

    Ext.Ajax.request({
      url:
        baseURL +
        "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
      params: {
        kd_unit: rowSelectedKasirRWJ.data.KD_UNIT, //di patok dulu, karena belum ada settingan
        mod_id: TrKasirRWJ_AG.mod_id,
        jenis_dokter: tmpKdJob,
        kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR, //di patok dulu, karena belum ada settingan
        no_transaksi: params.data.NO_TRANSAKSI,
        urut: tmp_urut,
        tgl_transaksi: params.data.TGL_TRANSAKSI,
        kd_job: tmpKdJob,
        txtDokter: dokter,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        console.log(cst);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_RWJ.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStorefirstGridStore_PJRWJ.add(recs);
        }
      },
    });
  } else if (mod == "LAB") {
    // dsDataDokterPenindak_PJ_IGD.removeAll();
    DataStorefirstGridStore_PJIGD.removeAll();
    var tmp_urut;
    if (typeof params.data.urut == "undefined") {
      tmp_urut = params.data.urut_masuk;
    } else {
      tmp_urut = params.data.urut;
    }
    Ext.Ajax.request({
      url:
        baseURL +
        "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
      params: {
        kd_unit: "41",
        jenis_dokter: tmpKdJob,
        kd_kasir: "08",
        no_transaksi: params.data.no_transaksi,
        urut: tmp_urut,
        tgl_transaksi: params.data.tgl_transaksi,
        kd_job: tmpKdJob,
        txtDokter: dokter,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        console.log(cst);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_IGD.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStorefirstGridStore_PJIGD.add(recs);
        }
      },
    });
  } else if (mod == "RAD") {
    // dsDataDokterPenindak_PJ_IGD.removeAll();
    DataStorefirstGridStore_PJIGD.removeAll();
    var tmp_urut;
    if (typeof params.data.urut == "undefined") {
      tmp_urut = params.data.urut_masuk;
    } else {
      tmp_urut = params.data.urut;
    }
    Ext.Ajax.request({
      url:
        baseURL +
        "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
      params: {
        kd_unit: "5",
        jenis_dokter: tmpKdJob,
        kd_kasir: "10",
        no_transaksi: params.data.no_transaksi,
        urut: tmp_urut,
        tgl_transaksi: params.data.tgl_transaksi,
        kd_job: tmpKdJob,
        txtDokter: dokter,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        console.log(cst);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_IGD.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStorefirstGridStore_PJIGD.add(recs);
        }
      },
    });
  }
}
function loaddatastoredokterVisite_REVISI(mod) {
  if (mod == "" || mod == null || mod == undefined) {
    Ext.Ajax.request({
      url: baseURL + "index.php/general/control_cmb_dokter/getCmbDokterVisite",
      params: {
        kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR,
        no_transaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
        tgl_transaksi: cellSelecteddeskripsi.data.TGL_TRANSAKSI,
        kd_produk: cellSelecteddeskripsi.data.KD_PRODUK,
        urut: cellSelecteddeskripsi.data.URUT,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_RWJ.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStoreSecondGridStore_PJRWJ.add(recs);
        }
      },
    });
  } else if (mod == "LAB") {
    Ext.Ajax.request({
      url: baseURL + "index.php/general/control_cmb_dokter/getCmbDokterVisite",
      params: {
        kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR_LAB,
        no_transaksi: selectedPenjasRWJLab.data.no_transaksi,
        tgl_transaksi: selectedPenjasRWJLab.data.tgl_transaksi,
        kd_produk: ViewGridDetailHasilLab_rwj_kd_produk,
        urut: ViewGridDetailHasilLab_rwj_urut,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_RWJ.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStoreSecondGridStore_PJRWJ.add(recs);
        }
      },
    });
  } else if (mod == "RAD") {
    Ext.Ajax.request({
      url: baseURL + "index.php/general/control_cmb_dokter/getCmbDokterVisite",
      params: {
        kd_kasir: TrKasirRWJ_AG.setting.KD_KASIR_RAD,
        no_transaksi: selectedPenjasRWJRad.data.no_transaksi,
        tgl_transaksi: selectedPenjasRWJRad.data.tgl_transaksi,
        kd_produk: ViewGridDetailHasilRad_rwj_kd_produk,
        urut: ViewGridDetailHasilRad_rwj_urut,
      },
      success: function (response) {
        var cst = Ext.decode(response.responseText);
        for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
          var recs = [],
            recType = dsDataDokterPenindak_PJ_RWJ.recordType;
          var o = cst["data"][i];
          recs.push(new recType(o));
          DataStoreSecondGridStore_PJRWJ.add(recs);
        }
      },
    });
  }
}
function showSignatureRJ(line) {
  var signa = dsPjTrans2.getRange()[line].data.signa;
  var spSigna = signa.split("X");
  var signa1 = "";
  var signa2 = "";
  if (spSigna.length > 1) {
    signa1 = spSigna[0];
    signa2 = spSigna[1];
  }
  var showSignatureIGD = new Ext.Window({
    title: "Signature",
    closeAction: "destroy",
    modal: true,
    layout: "fit",
    constraint: true,
    items: [
      {
        xtype: "panel",
        layout: "column",
        height: 30,
        style: "background:#ffffff;",
        bodyStyle: "padding: 4px;",
        width: 200,
        border: false,
        items: [
          {
            xtype: "displayfield",
            value: "Signature :&nbsp;",
          },
          {
            xtype: "numberfield",
            id: "txtSignatureIGD1",
            width: 50,
            value: signa1,
            listeners: {
              specialkey: function (a) {
                if (Ext.EventObject.getKey() === 13) {
                  if (a.getValue() != "" && a.getValue() != 0) {
                    Ext.getCmp("txtSignatureIGD2").focus();
                  }
                }
              },
            },
          },
          {
            xtype: "displayfield",
            value: "&nbsp;X&nbsp;",
          },
          {
            xtype: "numberfield",
            id: "txtSignatureIGD2",
            width: 50,
            value: signa2,
            listeners: {
              specialkey: function (a) {
                if (Ext.EventObject.getKey() === 13) {
                  if (a.getValue() != "" && a.getValue() != 0) {
                    dsPjTrans2
                      .getRange()
                      [line].set(
                        "signa",
                        Ext.getCmp("txtSignatureIGD1").getValue() +
                          "X" +
                          a.getValue()
                      );
                    showSignatureIGD.close();
                    PenataJasaRJ.gridObat.startEditing(line, 12);
                  }
                }
              },
            },
          },
        ],
      },
    ],
    fbar: [
      {
        text: "Ok",
        handler: function () {
          var err = false;
          if (
            Ext.getCmp("txtSignatureIGD1").getValue() == "" ||
            Ext.getCmp("txtSignatureIGD1").getValue() == 0
          ) {
            err = true;
            Ext.getCmp("txtSignatureIGD1").focus();
          }
          if (
            err == false &&
            (Ext.getCmp("txtSignatureIGD2").getValue() == "" ||
              Ext.getCmp("txtSignatureIGD2").getValue() == 0)
          ) {
            err = true;
            Ext.getCmp("txtSignatureIGD2").focus();
          }
          if (err == false) {
            dsPjTrans2
              .getRange()
              [line].set(
                "signa",
                Ext.getCmp("txtSignatureIGD1").getValue() +
                  "X" +
                  Ext.getCmp("txtSignatureIGD2").getValue()
              );
            showSignatureIGD.close();
            PenataJasaRJ.gridObat.startEditing(line, 12);
          }
        },
      },
    ],
  }).show();
  Ext.getCmp("txtSignatureIGD1").focus(true, 100);
}
function showRacikRJ(line) {
  var fldDetail = [
    "catatan_racik",
    "kd_prd",
    "kd_milik",
    "kd_unit_far",
    "nama_obat",
    "urut",
    "satuan",
    "jml_stok_apt",
    "jumlah",
    "order_mng",
  ];
  dsRacikan = new WebApp.DataStore({ fields: fldDetail });
  gridRacikanRJ = null;
  console.log(dsPjTrans2.data.items[line]);
  if (
    dsPjTrans2.data.items[line].data.result != undefined &&
    dsPjTrans2.data.items[line].data.result != "" &&
    dsPjTrans2.data.items[line].data.result !== null
  ) {
    var parse = JSON.parse(dsPjTrans2.data.items[line].data.result);
    for (var i = 0, iLen = parse.length; i < iLen; i++) {
      dsRacikan.insert(
        dsRacikan.getCount(),
        new PenataJasaRJ.classGridObat(parse[i])
      );
    }
  }
  var status = false;
  if (dsPjTrans2.data.items[line].data.order_mng == "Dilayani") {
    status = true;
  }
  var showSignatureIGD = new Ext.Window({
    title: "Racikan",
    closeAction: "destroy",
    modal: true,
    width: 800,
    height: 400,
    layout: {
      type: "vbox",
      align: "stretch",
    },
    constrain: true,
    items: [
      {
        xtype: "panel",
        layout: "form",
        style: "padding: 4px;",
        border: false,
        items: [
          new Ext.form.ComboBox({
            id: "gridcbo_aturan_racik",
            typeAhead: true,
            triggerAction: "all",
            lazyRender: true,
            fieldLabel: "Aturan Racik",
            mode: "local",
            disabled: status,
            selectOnFocus: true,
            forceSelection: true,
            store: ds_cbo_aturan_racik,
            valueField: "kd_racik_atr",
            displayField: "racik_aturan",
            value: dsPjTrans2.data.items[line].data.aturan_racik,
            listeners: {
              select: function (a, b, c) {
                if (a.getValue() != undefined) {
                  Ext.getCmp("gridcbo_aturan_pakai").focus(true, 10);
                }
              },
            },
          }),
          new Ext.form.ComboBox({
            id: "gridcbo_aturan_pakai",
            typeAhead: true,
            triggerAction: "all",
            lazyRender: true,
            mode: "local",
            disabled: status,
            fieldLabel: "Aturan Pakai",
            selectOnFocus: true,
            forceSelection: true,
            store: ds_cbo_aturan_pakai,
            valueField: "kd_racik_atr_pk",
            displayField: "singkatan",
            value: dsPjTrans2.data.items[line].data.aturan_pakai,
            listeners: {
              select: function (a, b, c) {
                if (a.getValue() != undefined) {
                  Ext.getCmp("BtnTambahObatTrKasirIGD_racikan").focus(
                    true,
                    100
                  );
                }
              },
            },
          }),
        ],
      },
      (gridRacikanIgd = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        flex: 1,
        border: false,
        style: "margin-top:-1px;",
        store: dsRacikan,
        autoScroll: true,
        cm: new Ext.grid.ColumnModel([
          new Ext.grid.RowNumberer(),
          {
            header: "Kd. Obat",
            dataIndex: "kd_prd",
            width: 60,
            menuDisabled: true,
          },
          {
            header: "Nama Obat",
            dataIndex: "nama_obat",
            flex: 1,
            menuDisabled: true,
            editor: new Nci.form.Combobox.autoComplete({
              store: PenataJasaRJ.form.DataStore.obat,
              matchFieldWidth: false,
              select: function (a, b, c) {
                // console.log (b.data);
                var linex =
                  gridRacikanIgd.getSelectionModel().selection.cell[0];
                dsRacikan.data.items[linex].data.kd_prd = b.data.kd_prd;
                dsRacikan.data.items[linex].data.nama_obat = b.data.nama_obat;
                dsRacikan.data.items[linex].data.satuan = b.data.satuan;
                dsRacikan.data.items[linex].data.kd_unit_far =
                  b.data.kd_unit_far;
                dsRacikan.data.items[linex].data.kd_milik = b.data.kd_milik;
                dsRacikan.data.items[linex].data.jml_stok_apt =
                  b.data.stok_current - b.data.jml_order;
                gridRacikanIgd.getView().refresh();
                gridRacikanIgd.startEditing(linex, 5);
              },
              insert: function (o) {
                return {
                  kd_prd: o.kd_prd,
                  nama_obat: o.nama_obat,
                  satuan: o.satuan,
                  kd_unit_far: o.kd_unit_far,
                  kd_milik: o.kd_milik,
                  stok_current: o.jml_stok_apt,
                  jml_order: 0,
                  text:
                    '<table style="font-size: 11px;" ><tr><td width="100" >' +
                    o.kd_prd +
                    '</td><td width="800">' +
                    o.nama_obat +
                    '</td><td width="100">' +
                    o.satuan +
                    '</td><td width="100"> Stok:' +
                    o.jml_stok_apt +
                    "</td></tr></table>",
                };
              },
              param: function () {
                var params = {};
                params["kd_customer"] = vkode_customer;
                return params;
              },
              url: baseURL + "index.php/main/functionRWJ/getMasterObatAll",
              valueField: "nama_obat",
              displayField: "text",
              listWidth: 400,
            }),
          },
          {
            header: "Satuan",
            menuDisabled: true,
            dataIndex: "satuan",
            width: 50,
          },
          {
            header: "Stok",
            menuDisabled: true,
            dataIndex: "jml_stok_apt",
            width: 50,
          },
          {
            header: "Qty",
            dataIndex: "jumlah",
            sortable: false,
            menuDisabled: true,
            width: 40,
            editor: new Ext.form.NumberField({
              selectOnFocus: true,
              width: 40,
              listeners: {
                blur: function (a) {
                  var line = this.index;
                  if (a.getValue() == 0) {
                    ShowPesanWarningRWJ("Qty obat belum di isi", "Warning");
                    Ext.Msg.show({
                      title: "Information",
                      msg: "Qty obat belum di isi ",
                      buttons: Ext.MessageBox.OK,
                      fn: function (btn) {
                        if (btn == "ok") {
                          var line1 =
                            gridRacikanIgd.getSelectionModel().selection
                              .cell[0];
                          gridRacikanIgd.startEditing(line1, 5);
                        }
                      },
                    });
                  } else {
                    var line1 =
                      gridRacikanIgd.getSelectionModel().selection.cell[0];
                    gridRacikanIgd.startEditing(line1, 6);
                  }
                },
              },
            }),
          },
          {
            header: "Catatan",
            menuDisabled: true,
            dataIndex: "catatan_racik",
            width: 100,
            editor: new Ext.form.TextField({
              selectOnFocus: true,
              width: 40,
            }),
          },
          {
            header: "Order",
            menuDisabled: true,
            width: 70,
            dataIndex: "order_mng",
            renderer: function (v, params, record) {
              if (record.data.order_mng === "Dilayani") {
                Ext.getCmp("BtnTambahObatTrKasirIGD_racikan").disable();
                Ext.getCmp("BtnHapusObatTrKasirIGD_racikan").disable();
              } else {
                Ext.getCmp("BtnTambahObatTrKasirIGD_racikan").enable();
                Ext.getCmp("BtnHapusObatTrKasirIGD_racikan").enable();
              }
              return record.data.order_mng;
            },
          },
          {
            hidden: true,
            dataIndex: "urut",
          },
          {
            dataIndex: "kd_unit_far",
            hidden: true,
          },
          {
            dataIndex: "kd_milik",
            hidden: true,
          },
        ]),
        viewConfig: { forceFit: true },
        tbar: [
          {
            text: "Tambah Obat",
            id: "BtnTambahObatTrKasirIGD_racikan",
            iconCls: "add",
            handler: function () {
              dsRacikan.insert(
                dsRacikan.getCount(),
                PenataJasaRJ.nullGridObat()
              );
            },
          },
          {
            text: "Hapus",
            id: "BtnHapusObatTrKasirIGD_racikan",
            iconCls: "RemoveRow",
            handler: function () {
              var line = gridRacikanIgd.getSelectionModel().selection.cell[0];
              var o = dsRacikan.getRange()[line].data;
              Ext.Msg.show({
                title: nmHapusBaris,
                msg: "Anda yakin akan menghapus data Obat ini?",
                buttons: Ext.MessageBox.YESNO,
                fn: function (btn) {
                  if (btn == "yes") {
                    if (
                      dsRacikan.getRange()[line].data.kd_prd != undefined &&
                      dsRacikan.getRange()[line].data.urut != undefined &&
                      dsRacikan.getRange()[line].data.urut != null &&
                      dsRacikan.getRange()[line].data.urut != ""
                    ) {
                      Ext.Ajax.request({
                        url:
                          baseURL +
                          "index.php/main/functionRWJ/hapusBarisGridObat",
                        params: {
                          kd_prd: o.kd_prd,
                          id_mrresep: PenataJasaRJ.var_id_mrresep,
                          urut: o.urut,
                        },
                        failure: function (o) {
                          ShowPesanErrorRWJ("Hubungi Admin", "Error");
                        },
                        success: function (o) {
                          var cst = Ext.decode(o.responseText);
                          if (cst.success === true) {
                            dsRacikan.removeAt(line);
                            gridRacikanIgd.getView().refresh();
                          } else {
                            ShowPesanErrorRWJ(
                              "Gagal melakukan penghapusan",
                              "Error"
                            );
                          }
                        },
                      });
                    } else {
                      dsRacikan.removeAt(line);
                      gridRacikanIgd.getView().refresh();
                    }
                  }
                },
                icon: Ext.MessageBox.QUESTION,
              });
            },
          },
        ],
      })),
    ],
    fbar: [
      {
        text: "Ok",
        handler: function () {
          var tmpDats = dsRacikan.data.items;
          var arrDat = [];
          for (var i = 0, iLen = tmpDats.length; i < iLen; i++) {
            if (
              tmpDats[i].data.kd_prd != "" &&
              tmpDats[i].data.kd_prd != null
            ) {
              arrDat.push(tmpDats[i].data);
            }
          }
          var err = false;
          if (
            Ext.getCmp("gridcbo_aturan_racik").getValue() == "" ||
            Ext.getCmp("gridcbo_aturan_racik").getValue() == null
          ) {
            err = true;
            Ext.getCmp("gridcbo_aturan_racik").focus();
          }
          if (
            err == false &&
            (Ext.getCmp("gridcbo_aturan_pakai").getValue() == "" ||
              Ext.getCmp("gridcbo_aturan_pakai").getValue() == 0)
          ) {
            err = true;
            Ext.getCmp("gridcbo_aturan_pakai").focus();
          }
          if (err == false && arrDat.length == 0) {
            err = true;
            ShowPesanWarningRWJ("Obat tidak boleh kosong", "Obat");
          }
          if (err == false) {
            dsPjTrans2.getRange()[line].set("result", JSON.stringify(arrDat));
            dsPjTrans2
              .getRange()
              [line].set(
                "aturan_racik",
                Ext.getCmp("gridcbo_aturan_racik").getValue()
              );
            dsPjTrans2
              .getRange()
              [line].set(
                "aturan_pakai",
                Ext.getCmp("gridcbo_aturan_pakai").getValue()
              );
            showSignatureIGD.close();
            PenataJasaRJ.gridObat.startEditing(line, 7);
          }
        },
      },
    ],
  }).show();

  if (status == true) {
    gridRacikanIgd.disable();
  }
  Ext.getCmp("gridcbo_aturan_racik").focus(true, 10);
}
function GetDTLTRRWJGridFirst(data) {
  var fldDetail = [
    "takaran",
    "racikan_text",
    "result",
    "kd_satuan",
    "kd_prd",
    "nama_obat",
    "jumlah",
    "satuan",
    "cara_pakai",
    "kd_dokter",
    "nama",
    "verified",
    "urut",
    "racikan",
    "jml_stok_apt",
    "order_mng",
    "id_mrresep",
    "aturan_pakai",
    "aturan_racik",
    "kd_unit_far",
    "kd_milik",
    "no_racik",
    "signa",
  ];
  dsPjTrans2 = new WebApp.DataStore({ fields: fldDetail });
  var fldDetail_ds_aturan_racik = ["kd_racik_atr", "racik_aturan"];
  ds_cbo_aturan_racik = new WebApp.DataStore({
    fields: fldDetail_ds_aturan_racik,
  });
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getAturanRacik",
    params: {
      kriteria: "",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      // gridcbo_aturan_racik.store.removeAll();
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = ds_cbo_aturan_racik.recordType;
        var o = cst["listData"][i];
        recs.push(new recType(o));
        ds_cbo_aturan_racik.add(recs);
      }
    },
  });
  var fldDetail_ds_aturan_pakai = [
    "kd_racik_atr_pk",
    "singkatan",
    "kepanjangan",
    "arti",
    "keterangan",
  ];
  ds_cbo_aturan_pakai = new WebApp.DataStore({
    fields: fldDetail_ds_aturan_pakai,
  });
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getAturanPakai",
    params: {
      kriteria: "",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      // gridcbo_aturan_pakai.store.removeAll();
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = ds_cbo_aturan_pakai.recordType;
        var o = cst["listData"][i];
        recs.push(new recType(o));
        ds_cbo_aturan_pakai.add(recs);
      }
    },
  });

  var fldDetail_ds_satuan = ["kd_satuan", "satuan"];
  ds_cbo_satuan = new WebApp.DataStore({ fields: fldDetail_ds_satuan });
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getSatuanRacik",
    params: {
      kriteria: "",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = ds_cbo_satuan.recordType;
        var o = cst["listData"][i];
        recs.push(new recType(o));
        ds_cbo_satuan.add(recs);
      }
    },
  });

  var fldDetail_ds_cara_pakai = ["kd_aturan", "nama_aturan"];
  ds_cbo_cara_pakai = new WebApp.DataStore({ fields: fldDetail_ds_cara_pakai });
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getCaraPakai",
    params: {
      kriteria: "",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = ds_cbo_cara_pakai.recordType;
        var o = cst["listData"][i];
        recs.push(new recType(o));
        ds_cbo_cara_pakai.add(recs);
      }
    },
  });

  var fldDetail_ds_takaran = ["kd_takaran", "nama_takaran"];
  ds_cbo_takaran = new WebApp.DataStore({ fields: fldDetail_ds_takaran });
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getTakaran",
    params: {
      kriteria: "",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = ds_cbo_takaran.recordType;
        var o = cst["listData"][i];
        recs.push(new recType(o));
        ds_cbo_takaran.add(recs);
      }
    },
  });
  RefreshDataKasirRWJDetai2(data);
  PenataJasaRJ.pj_req__obt = new Ext.grid.EditorGridPanel({
    title: "Resep Online",
    id: "PjTransGrid1",
    stripeRows: true,
    store: dsPjTrans2,
    flex: 1,
    autoScroll: true,
    border: false,
    clicksToEdit: 1,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          cell_select_index_igd = rec;
          cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
          CurrentKasirRWJ.row = row;
          CurrentKasirRWJ.data = cellSelecteddeskripsi;
          CurrentKasirRWJSelection = sm.selection.record;
          PenataJasaRJ.var_id_mrresep =
            CurrentKasirRWJSelection.data.id_mrresep;
          // console.log(sm.selection.record.data.order_mng);
          if (sm.selection.record.data.order_mng == "Dilayani") {
            Ext.getCmp("BtnHapusObatTrKasirRwj").disable();
          } else {
            Ext.getCmp("BtnHapusObatTrKasirRwj").enable();
          }
          // console.log(row);
          // console.log(rec);
        },
      },
    }),
    listeners: {
      beforeedit: function (plugin, edit) {
        if (plugin.field == "nama_obat" && plugin.record.data.racikan == true) {
          return false;
        }
        if (plugin.field == "satuan" && plugin.record.data.racikan !== true) {
          return false;
        }
      },
      rowdblclick: function (sm, ridx, cidx) {
        if (cell_select_index_igd == 11) {
          var line =
            PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
          showSignatureRJ(line);
        }
        if (cell_select_index_igd == 6) {
          var line =
            PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
          if (dsPjTrans2.data.items[line].data.racikan == true) {
            showRacikRJ(line);
          }
        }
      },
    },
    cm: TRRawatJalanColumModel_obt2(),
    viewConfig: { forceFit: true },
  });
  PenataJasaRJ.gridObat = PenataJasaRJ.pj_req__obt;
  PenataJasaRJ.dsGridObat = dsPjTrans2;
  return PenataJasaRJ.pj_req__obt;
}
function TRRawatJalanColumModel_obt2() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Racik",
      menuDisabled: true,
      dataIndex: "racikan_text",
      width: 30,
      editor: new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        emptyText: "",
        store: new Ext.data.ArrayStore({
          id: 0,
          fields: ["id", "displayText"],
          data: [
            [true, "Ya"],
            [false, "Tidak"],
          ],
        }),
        valueField: "displayText",
        displayField: "displayText",
        listeners: {
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
            dsPjTrans2.getRange()[line].set("racikan", b.data.id);
            if (b.data.id == true) {
              dsPjTrans2.getRange()[line].set("nama_obat", "[RACIKAN]");
              dsPjTrans2.getRange()[line].set("kd_prd", "");
              dsPjTrans2.getRange()[line].set("satuan", "");
              dsPjTrans2.getRange()[line].set("jml_stok_apt", 0);
            } else {
              dsPjTrans2.getRange()[line].set("nama_obat", "");
              dsPjTrans2.getRange()[line].set("kd_prd", "");
              dsPjTrans2.getRange()[line].set("satuan", "");
              dsPjTrans2.getRange()[line].set("jml_stok_apt", 0);
            }
            var no_racik = 0;
            for (
              var i = 0, iLen = dsPjTrans2.getRange().length;
              i < iLen;
              i++
            ) {
              dsPjTrans2.getRange()[i].set("no_racik", "0");
              if (dsPjTrans2.getRange()[i].data.racikan == true) {
                no_racik++;
                dsPjTrans2.getRange()[i].set("kd_prd", no_racik);
                dsPjTrans2.getRange()[i].set("no_racik", no_racik);
              }
            }
            if (b.data.id == true) {
              showRacikRJ(line);
            } else {
              PenataJasaRJ.gridObat.startEditing(line, 6);
            }
          },
        },
      }),
    },
    {
      hidden: true,
      dataIndex: "racikan",
    },
    {
      id: Nci.getId(),
      header: "Kd. Obat/No. Racik",
      dataIndex: "kd_prd",
      width: 60,
      menuDisabled: true,
    },
    {
      id: Nci.getId(),
      header: "kd_unit_far",
      dataIndex: "kd_unit_far",
      width: 60,
      menuDisabled: true,
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "kd_milik",
      dataIndex: "kd_milik",
      width: 60,
      menuDisabled: true,
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "Nama Obat",
      dataIndex: "nama_obat",
      flex: 1,
      menuDisabled: true,
      hidden: false,
      // editor			: PenataJasaRJ.comboObat()
      editor: new Nci.form.Combobox.autoComplete({
        store: PenataJasaRJ.form.DataStore.obat,
        matchFieldWidth: false,
        select: function (a, b, c) {
          console.log(b.data);
          var line =
            PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
          dsPjTrans2.data.items[line].data.kd_prd = b.data.kd_prd;
          dsPjTrans2.data.items[line].data.nama_obat = b.data.nama_obat;
          dsPjTrans2.data.items[line].data.satuan = b.data.satuan;
          dsPjTrans2.data.items[line].data.kd_unit_far = b.data.kd_unit_far;
          dsPjTrans2.data.items[line].data.kd_milik = b.data.kd_milik;
          dsPjTrans2.data.items[line].data.jml_stok_apt =
            b.data.stok_current - b.data.jml_order;

          PenataJasaRJ.gridObat.getView().refresh();
          PenataJasaRJ.gridObat.startEditing(line, 10);
        },
        insert: function (o) {
          return {
            kd_prd: o.kd_prd,
            nama_obat: o.nama_obat,
            satuan: o.satuan,
            kd_unit_far: o.kd_unit_far,
            kd_milik: o.kd_milik,
            stok_current: o.jml_stok_apt,
            jml_order: 0,
            text:
              '<table style="font-size: 11px;" ><tr><td width="100" >' +
              o.kd_prd +
              '</td><td width="800">' +
              o.nama_obat +
              '</td><td width="100">' +
              o.satuan +
              '</td><td width="100"> Stok:' +
              o.jml_stok_apt +
              "</td></tr></table>",
          };
        },
        param: function () {
          var params = {};
          // params['kd_customer']=kd_cus_gettarif;
          // params['kd_unit_asal']=Ext.getCmp('txtKdUnit_PenJasHemodialisa').getValue();
          params["kd_customer"] = vkode_customer;
          return params;
        },
        url: baseURL + "index.php/main/functionRWJ/getMasterObatAll",
        valueField: "nama_obat",
        displayField: "text",
        listWidth: 400,
      }),
    },
    {
      id: Nci.getId(),
      header: "Satuan",
      hidden: false,
      menuDisabled: true,
      dataIndex: "satuan",
      width: 50,
      editor: new Ext.form.ComboBox({
        id: "gridcbo_satuan",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        store: ds_cbo_satuan,
        valueField: "satuan",
        displayField: "satuan",
        listeners: {
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
            dsPjTrans2.getRange()[line].set("kd_satuan", b.data.kd_satuan);
            PenataJasaRJ.gridObat.startEditing(line, 10);
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Stok",
      hidden: false,
      menuDisabled: true,
      dataIndex: "jml_stok_apt",
      width: 50,
    },
    {
      hidden: true,
      dataIndex: "kd_satuan",
      width: 50,
    },
    {
      id: Nci.getId(),
      header: "Qty",
      dataIndex: "jumlah",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 40,
      editor: new Ext.form.NumberField({
        id: "txtQty",
        selectOnFocus: true,
        width: 40,
        anchor: "100%",
        listeners: {
          blur: function (a) {
            var line = this.index;
            if (a.getValue() == 0) {
              ShowPesanWarningIGD("Qty obat belum di isi", "Warning");
              Ext.Msg.show({
                title: "Information",
                msg: "Qty obat belum di isi ",
                buttons: Ext.MessageBox.OK,
                fn: function (btn) {
                  if (btn == "ok") {
                    var line =
                      PenataJasaRJ.gridObat.getSelectionModel().selection
                        .cell[0];
                    PenataJasaRJ.gridObat.startEditing(line, 10);
                  }
                },
              });
            } else {
              var line =
                PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
              hasilJumlah_rwj(a.getValue());
              showSignatureRJ(line);
            }
          },
          focus: function (a) {
            // this.index=PenataJasaIGD.dsGridObat.getSelectionModel().selection.cell[0]
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Signa",
      hidden: false,
      menuDisabled: true,
      width: 50,
      dataIndex: "signa",
    },
    {
      id: Nci.getId(),
      header: "Aturan Pakai",
      hidden: false,
      menuDisabled: true,
      width: 80,
      dataIndex: "cara_pakai",
      editor: new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        store: ds_cbo_cara_pakai,
        valueField: "nama_aturan",
        displayField: "nama_aturan",
        listeners: {
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
            dsPjTrans2
              .getRange()
              [line].set("kd_aturan_pakai", b.data.kd_aturan);
            PenataJasaRJ.gridObat.startEditing(line, 13);
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "TAKARAN",
      hidden: false,
      menuDisabled: false,
      width: 80,
      dataIndex: "takaran",
      editor: new Ext.form.TextField({
        id: "fieldcolTakaran",
        allowBlank: true,
        enableKeyEvents: true,
        // width: 30,
        flex : 1
      }),
      // editor: new Ext.form.ComboBox({
      //   typeAhead: true,
      //   triggerAction: "all",
      //   lazyRender: true,
      //   mode: "local",
      //   selectOnFocus: true,
      //   forceSelection: true,
      //   store: ds_cbo_takaran,
      //   valueField: "nama_takaran",
      //   displayField: "nama_takaran",
      //   listeners: {
      //     select: function (a, b, c) {
      //       var line =
      //         PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
      //       dsPjTrans2.getRange()[line].set("kd_takaran", b.data.kd_takaran);
      //     },
      //   },
      // }),
    },
    {
      id: Nci.getId(),
      header: "No.Racik",
      hidden: true,
      menuDisabled: true,
      dataIndex: "no_racik",
      editor: new Ext.form.NumberField({
        id: "txtNoRacikan",
        selectOnFocus: true,
        width: 50,
        anchor: "100%",
        listeners: {
          specialkey: function () {
            if (Ext.EventObject.getKey() === 13) {
              var line =
                PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
              PenataJasaRJ.gridObat.startEditing(line, 11);
            }
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Aturan Racik Obat",
      hidden: true,
      menuDisabled: true,
      dataIndex: "aturan_racik",
      editor: new Ext.form.ComboBox({
        // id				: 'gridcbo_aturan_racik',
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        width: 70,
        anchor: "95%",
        value: 1,
        store: ds_cbo_aturan_racik,
        valueField: "racik_aturan",
        displayField: "racik_aturan",
        value: "",
        listeners: {
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
            PenataJasaRJ.gridObat.startEditing(line, 12);
          },
          specialkey: function () {
            /* if(Ext.EventObject.getKey() == 13){
							var line = PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
							PenataJasaRJ.pj_req__obt.startEditing(line, 12);	
						} */
          },
        },
      }),
    },
    {
      header: "Aturan Pakai Obat",
      hidden: true,
      menuDisabled: true,
      dataIndex: "aturan_pakai",
      editor: new Ext.form.ComboBox({
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        width: 40,
        anchor: "95%",
        value: 1,
        store: ds_cbo_aturan_pakai,
        valueField: "singkatan",
        displayField: "singkatan",
        value: "",
        listeners: {
          select: function (a, b, c) {
            PenataJasaRJ.gridObat.insert(
              PenataJasaRJ.dsGridObat.getCount(),
              PenataJasaRJ.nullGridObat()
            );
            var nextRow = dsPjTrans2.getCount() - 1;
            PenataJasaRJ.gridObat.startEditing(nextRow, 4);
          },
          specialkey: function () {},
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Pelakasana",
      dataIndex: "nama",
      hidden: true,
    },
    {
      id: Nci.getId(),
      dataIndex: "kd_takaran",
      hidden: true,
    },
    {
      dataIndex: "kd_aturan_pakai",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "Verified",
      hidden: true,
      menuDisabled: true,
      dataIndex: "verified",
      editor: PenataJasaRJ.ComboVerifiedObat(),
    },
    {
      id: Nci.getId(),
      header: "Order",
      hidden: false,
      menuDisabled: true,
      width: 70,
      dataIndex: "order_mng",
      renderer: function (v, params, record) {
        /*if  (record.data.order_mng==='Dilayani'){
					Ext.getCmp('BtnTambahObatTrKasirRwj').disable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').disable();
				}else{
					Ext.getCmp('BtnTambahObatTrKasirRwj').enable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').enable();
				}*/
        return record.data.order_mng;
      },
    },
    {
      id: Nci.getId(),
      header: "urut",
      hidden: true,
      menuDisabled: true,
      dataIndex: "urut",
    },
  ]);
}
function GetDTLTRRWJGridSecond() {
  var fldDetail = [
    "KP_PRODUK",
    "KD_PRODUK",
    "DESKRIPSI",
    "QTY",
    "DOKTER",
    "TGL_TINDAKAN",
    "QTY",
    "DESC_REQ",
    "TGL_BERLAKU",
    "NO_TRANSAKSI",
    "URUT",
    "DESC_STATUS",
    "TGL_TRANSAKSI",
    "KD_TARIF",
    "HARGA",
    "JUMLAH_DOKTER",
    "JUMLAH",
    "DOKTER",
    "STATUS_KONSULTASI",
    "JUMLAH_DOKTER_VISITE",
    "GROUP",
    "KD_UNIT",
    "NAMA_DOKTER_VISITE"
  ];
  dsTRDetailKasirRWJList = new WebApp.DataStore({ fields: fldDetail });
  RefreshDataKasirRWJDetail();
  PenataJasaRJ.dsGridTindakan = dsTRDetailKasirRWJList;
  PenataJasaRJ.form.Grid.produk = new Ext.grid.EditorGridPanel({
    title: "Item Transaksi",
    id: "PjTransGrid2",
    stripeRows: true,
    store: PenataJasaRJ.dsGridTindakan,
    border: false,
    flex: 1,
    clicksToEdit: 1,
    autoScroll: true,
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          rowSelectedPJRWJ = dsTRDetailKasirRWJList.getAt(row);
          cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
          CurrentKasirRWJ.row = row;
          CurrentKasirRWJ.data = cellSelecteddeskripsi;
          console.log(cellSelecteddeskripsi);

          tmp_group_dokter = cellSelecteddeskripsi.data.GROUP;
          currentJasaDokterKdTarif_RWJ = cellSelecteddeskripsi.data.KD_TARIF;
          currentJasaDokterKdProduk_RWJ = cellSelecteddeskripsi.data.KD_PRODUK;
          currentJasaDokterUrutDetailTransaksi_RWJ =
            cellSelecteddeskripsi.data.URUT;
          currentJasaDokterHargaJP_RWJ = cellSelecteddeskripsi.data.HARGA;
        },
      },
    }),
    cm: TRRawatJalanColumModel2(),
    viewConfig: { forceFit: true },
    listeners: {
      rowclick: function ($this, rowIndex, e) {
        cellCurrentTindakan = rowIndex;
      },
      /* rowdblclick: function(gridView, htmlElement, columnIndex, dataRecord){
        console.log(columnIndex);
        if(columnIndex == 10){
          FormLookUp_UpdateTarifTindakan.show();
        }
      }, */
      celldblclick: function (gridView, htmlElement, columnIndex, dataRecord) {
        if (
          columnIndex == 7 &&
          (PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data
            .JUMLAH != "" ||
            PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data
              .JUMLAH_DOKTER != "")
        ) {
        }
      },
      // HUDI
      // 12-05-2024
      beforeedit:function($this, rowIndex, columnIndex, e){
        // console.log(PenataJasaRJ.dsGridTindakan.data);
        /* Ext.Ajax.request({
          url: baseURL + "index.php/main/functionRWJ/editTarifTindakan",
          params: {
            kd_kasir : '01', // kd_kasir rwj
            no_transaksi : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
            list_data : getArrTindakanPemeriksaan_RWJ(PenataJasaRJ.dsGridTindakan.data)
          },
          success: function (o) {

          }
        }) */
      }
    },
  });

  return PenataJasaRJ.form.Grid.produk;
}

function getArrTindakanPemeriksaan_RWJ(record) {
  /* console.log(record);
  var arr = [];
  for(var n = 0; n <record.length; n++){
    // var o = {};
    arr.push(record.items[n].data);
    // console.log(record.items[n].data.harga);
  } */
  var arr = [];
  for (var i = 0; i < record.getCount(); i++) {
    console.log(record.items[i].data.HARGA);
    var o = {};

    o["URUT"] = record.items[i].data.URUT;
    o["KD_PRODUK"] = record.items[i].data.KD_PRODUK;
    o["QTY"] = record.items[i].data.QTY;
    o["TGL_TRANSAKSI"] = record.items[i].data.TGL_TRANSAKSI;
    o["TGL_BERLAKU"] = record.items[i].data.TGL_BERLAKU;
    o["HARGA"] = record.items[i].data.HARGA;
    o["KD_UNIT"] = record.items[i].data.KD_UNIT;
    o["KD_TARIF"] = record.items[i].data.KD_TARIF;
    o["NO_TRANSAKSI"] = record.items[i].data.NO_TRANSAKSI;

    arr.push(o);
  }

  return Ext.encode(arr);
}

function TRRawatJalanColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: Nci.getId(),
      header: "KD.Obat",
      dataIndex: "kd_prd",
      width: 60,
      menuDisabled: true,
      hidden: false,
    },
    {
      id: Nci.getId(),
      header: "kd_unit_far",
      dataIndex: "kd_unit_far",
      width: 60,
      menuDisabled: true,
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "kd_milik",
      dataIndex: "kd_milik",
      width: 60,
      menuDisabled: true,
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "Nama Obat",
      dataIndex: "nama_obat",
      width: 150,
      menuDisabled: true,
      hidden: false,
      // editor			: PenataJasaRJ.comboObat()
      editor: new Nci.form.Combobox.autoComplete({
        store: PenataJasaRJ.form.DataStore.obat,
        select: function (a, b, c) {
          console.log(b.data);
          var line =
            PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
          dsPjTrans2.data.items[line].data.kd_prd = b.data.kd_prd;
          dsPjTrans2.data.items[line].data.nama_obat = b.data.nama_obat;
          dsPjTrans2.data.items[line].data.satuan = b.data.satuan;
          dsPjTrans2.data.items[line].data.kd_unit_far = b.data.kd_unit_far;
          dsPjTrans2.data.items[line].data.kd_milik = b.data.kd_milik;
          dsPjTrans2.data.items[line].data.jml_stok_apt = b.data.stok_current;
          console.log(b.data.kd_unit_far, b.data.kd_milik);

          PenataJasaRJ.pj_req__obt.getView().refresh();
          PenataJasaRJ.pj_req__obt.startEditing(line, 7);
        },
        insert: function (o) {
          return {
            kd_prd: o.kd_prd,
            nama_obat: o.nama_obat,
            satuan: o.satuan,
            kd_unit_far: o.kd_unit_far,
            kd_milik: o.kd_milik,
            stok_current: o.jml_stok_apt,
            jml_order: o.jml_order,
            text:
              '<table style="font-size: 11px;" ><tr><td width="100" >' +
              o.kd_prd +
              '</td><td width="800">' +
              o.nama_obat +
              '</td><td width="100">' +
              o.satuan +
              '</td><td width="100"> Stok:' +
              o.jml_stok_apt +
              "</td></tr></table>",
          };
        },
        param: function () {
          var params = {};
          // params['kd_customer']=kd_cus_gettarif;
          // params['kd_unit_asal']=Ext.getCmp('txtKdUnit_PenJasHemodialisa').getValue();
          params["kd_customer"] = Ext.getCmp("txtKDCustomer").getValue();
          return params;
        },
        url: baseURL + "index.php/main/functionRWJ/getMasterObatAll",
        valueField: "nama_obat",
        displayField: "text",
        listWidth: 400,
      }),
    },
    {
      id: Nci.getId(),
      header: "Satuan",
      hidden: false,
      menuDisabled: true,
      dataIndex: "satuan",
      width: 50,
    },
    {
      id: Nci.getId(),
      header: "Stok",
      hidden: false,
      menuDisabled: true,
      dataIndex: "jml_stok_apt",
      width: 50,
    },
    {
      id: Nci.getId(),
      header: "Qty",
      dataIndex: "jumlah",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 40,
      editor: new Ext.form.NumberField({
        id: "txtQty",
        selectOnFocus: true,
        width: 40,
        anchor: "100%",
        listeners: {
          blur: function (a) {
            var line = this.index;
            if (a.getValue() == 0) {
              ShowPesanWarningRWJ("Qty obat belum di isi", "Warning");
              Ext.Msg.show({
                title: "Information",
                msg: "Qty obat belum di isi ",
                buttons: Ext.MessageBox.OK,
                fn: function (btn) {
                  if (btn == "ok") {
                    var line =
                      PenataJasaRJ.pj_req__obt.getSelectionModel().selection
                        .cell[0];
                    PenataJasaRJ.pj_req__obt.startEditing(line, 6);
                  }
                },
              });
            } else {
              hasilJumlah_rwj(a.getValue());
            }
          },
          focus: function (a) {
            this.index =
              PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Signa",
      hidden: false,
      menuDisabled: true,
      dataIndex: "signa",
      editor: new Ext.form.TextField({
        id: "txtSigna",
        selectOnFocus: true,
        width: 40,
        anchor: "100%",
        listeners: {
          specialkey: function () {
            if (Ext.EventObject.getKey() === 13) {
              var line =
                PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
              PenataJasaRJ.pj_req__obt.startEditing(line, 9);
            }
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Aturan Pakai",
      hidden: false,
      menuDisabled: true,
      dataIndex: "cara_pakai",
      editor: new Ext.form.TextField({
        id: "txtcarapakai",
        selectOnFocus: true,
        width: 40,
        anchor: "100%",
        listeners: {
          specialkey: function (a) {
            if (Ext.EventObject.getKey() === 13) {
              a.setValue(a.getValue().toUpperCase());
              var line =
                PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
              PenataJasaRJ.pj_req__obt.startEditing(line, 9);
            }
          },
          blur: function (a) {
            a.setValue(a.getValue().toUpperCase());
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Racikan",
      xtype: "checkcolumn",
      hidden: false,
      menuDisabled: true,
      width: 50,
      dataIndex: "racikan",
    },
    {
      id: Nci.getId(),
      header: "No.Racik",
      hidden: false,
      menuDisabled: true,
      dataIndex: "no_racik",
      editor: new Ext.form.NumberField({
        id: "txtNoRacikan",
        selectOnFocus: true,
        width: 50,
        anchor: "100%",
        listeners: {
          specialkey: function () {
            if (Ext.EventObject.getKey() === 13) {
              var line =
                PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
              PenataJasaRJ.pj_req__obt.startEditing(line, 11);
            }
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Aturan Racik Obat",
      hidden: true,
      menuDisabled: true,
      dataIndex: "aturan_racik",
      editor: new Ext.form.ComboBox({
        id: "gridcbo_aturan_racik",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        width: 70,
        anchor: "95%",
        value: 1,
        store: ds_cbo_aturan_racik,
        valueField: "racik_aturan",
        displayField: "racik_aturan",
        value: "",
        listeners: {
          select: function (a, b, c) {
            var line =
              PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
            PenataJasaRJ.pj_req__obt.startEditing(line, 12);
          },
          specialkey: function () {
            /* if(Ext.EventObject.getKey() == 13){
							var line = PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0];
							PenataJasaRJ.pj_req__obt.startEditing(line, 12);	
						} */
          },
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Aturan Pakai Obat",
      hidden: true,
      menuDisabled: true,
      dataIndex: "aturan_pakai",
      editor: new Ext.form.ComboBox({
        id: "gridcbo_aturan_pakai",
        typeAhead: true,
        triggerAction: "all",
        lazyRender: true,
        mode: "local",
        selectOnFocus: true,
        forceSelection: true,
        width: 40,
        anchor: "95%",
        value: 1,
        store: ds_cbo_aturan_pakai,
        valueField: "singkatan",
        displayField: "singkatan",
        value: "",
        listeners: {
          select: function (a, b, c) {
            PenataJasaRJ.dsGridObat.insert(
              PenataJasaRJ.dsGridObat.getCount(),
              PenataJasaRJ.nullGridObat()
            );
            var nextRow = dsPjTrans2.getCount() - 1;
            PenataJasaRJ.pj_req__obt.startEditing(nextRow, 4);
          },
          specialkey: function () {},
        },
      }),
    },
    {
      id: Nci.getId(),
      header: "Dokter",
      hidden: true,
      menuDisabled: true,
      dataIndex: "nama",
      hidden: true,
    },
    {
      id: Nci.getId(),
      header: "Verified",
      hidden: true,
      menuDisabled: true,
      dataIndex: "verified",
      editor: PenataJasaRJ.ComboVerifiedObat(),
    },

    {
      id: Nci.getId(),
      header: "Order",
      hidden: false,
      menuDisabled: true,
      dataIndex: "order_mng",
      renderer: function (v, params, record) {
        /*if  (record.data.order_mng==='Dilayani')
				{
					Ext.getCmp('BtnTambahObatTrKasirRwj').disable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').disable();
				}
				else
				{
					Ext.getCmp('BtnTambahObatTrKasirRwj').enable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').enable();
				}*/

        return record.data.order_mng;
      },
    },
    {
      id: Nci.getId(),
      header: "urut",
      hidden: true,
      menuDisabled: true,
      dataIndex: "urut",
    },
  ]);
}

// function TRRawatJalanColumModel2(){
// return new Ext.grid.ColumnModel([
// new Ext.grid.RowNumberer(),
// {
// id				: 'colkonsulrwj2',
// header			: 'KONSUL',
// dataIndex		: 'STATUS_KONSULTASI',
// menuDisabled	: true,
// hidden 		: true
// },{
// id				: 'coleskripsirwj2',
// header			: 'Uraian',
// dataIndex		: 'DESKRIPSI2',
// menuDisabled	: true,
// hidden 		: true
// },{
// id				: 'coleskripsirwj23',
// header			: 'Kode',
// dataIndex		: 'KD_PRODUK',
// menuDisabled	: true,
// hidden 		: true
// },{
// id				: 'colKdProduk2',
// header			: 'Kode Produk',
// dataIndex		: 'KP_PRODUK',
// width			: 100,
// menuDisabled	: true,
// editor: new Ext.form.TextField({
// allowBlank: false,
// id:'fieldcolPenataJasaRWI',
// enableKeyEvents : true,
// listeners:{
// 'specialkey' : function(a){
// if (Ext.EventObject.getKey() === 13){
// a.setValue(Ext.get('fieldcolPenataJasaRWI').dom.value.toUpperCase());
// Ext.Ajax.request({
// url:  baseURL + "index.php/main/functionRWJ/getProdukKey",
// params: {
// kd_unit: Ext.getCmp('txtKdUnitRWJ').getValue(),
// kd_customer:vkode_customer,
// text:Ext.get('fieldcolPenataJasaRWI').dom.value.toUpperCase()
// },
// success: function(o) {
// var cst = Ext.decode(o.responseText);
// if(cst.listData != null){
// var line = dsTRDetailKasirRWJList.getCount();
// //alert(dsTRDetailKasirRWJList.data.items[1].data.KP_PRODUK)
// var kp_prod2=cst.listData.kp_produk;
// var xox=0;
// for(var i = 0 ; i < line-1;i++)
// {
// //alert(dsTRDetailKasirRWJList.data.items[i].data.KP_PRODUK);
// var o=dsTRDetailKasirRWJList.data.items[i].data.KP_PRODUK;
// if (kp_prod2 == o)
// {
// xox=xox+1;
// }else{
// xox=xox+0;
// }
// <<<<<<< HEAD
// =======
// <<<<<<< HEAD
// <<<<<<< HEADs
// function TRRawatJalanColumModel2(){
//     return new Ext.grid.ColumnModel([
//              new Ext.grid.RowNumberer(),
//         {
//         	 id				: 'colkonsulrwj2',
//         	 header			: 'KONSUL',
//         	 dataIndex		: 'STATUS_KONSULTASI',
//         	 menuDisabled	: true,
//         	 hidden 		: true
//         },{
//         	 id				: 'coleskripsirwj2',
//         	 header			: 'Uraian',
//         	 dataIndex		: 'DESKRIPSI2',
//         	 menuDisabled	: true,
//         	 hidden 		: true
//         },{
//         	 id				: 'coleskripsirwj23',
//         	 header			: 'Kode',
//         	 dataIndex		: 'KD_PRODUK',
//         	 menuDisabled	: true,
//         	 hidden 		: true
//         },{
//         	id				: 'colKdProduk2',
//             header			: 'Kode Produk',
//             dataIndex		: 'KP_PRODUK',
//             width			: 100,
// 			menuDisabled	: true,
//             editor: new Ext.form.TextField({
// 				allowBlank: false,
// 				id:'fieldcolPenataJasaRWI',
// 				enableKeyEvents : true,
// 				listeners:{
// 					'specialkey' : function(a){
// 						if (Ext.EventObject.getKey() === 13){
// 							a.setValue(Ext.get('fieldcolPenataJasaRWI').dom.value.toUpperCase());
// 							Ext.Ajax.request({
// 								url:  baseURL + "index.php/main/functionIGD/getProdukKey",
// 								params: {
// 									kd_unit: Ext.getCmp('txtKdUnitRWJ').getValue(),
// 									kd_customer:vkode_customer,
// 									text:Ext.get('fieldcolPenataJasaRWI').dom.value.toUpperCase()
// 								},    
// 								success: function(o) {
// 									var cst = Ext.decode(o.responseText);
// 									if(cst.listData != null){
// 										var line = dsTRDetailKasirRWJList.getCount();
// 										var kp_prod2=cst.listData.kp_produk;
// 										var xox=0;
// 										for(var i = 0 ; i < line-1;i++){
// 											var o=dsTRDetailKasirRWJList.data.items[i].data.KP_PRODUK;
// 											if (kp_prod2 == o){
// 												xox=xox+1;
// 											}else{
// 												xox=xox+0;
// 											}	
// 										}
// 										if (xox === 0){
// 											var linePJasaRWJ = PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('KP_PRODUK',cst.listData.kp_produk);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('KD_PRODUK',cst.listData.kd_produk);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('DESKRIPSI',cst.listData.deskripsi);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('TGL_BERLAKU',cst.listData.tgl_berlaku);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('KD_KLAS',cst.listData.KD_KLAS);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('HARGA',cst.listData.tarifx);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('KD_TARIF',cst.listData.kd_tarif);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('STATUS_KONSULTASI',cst.listData.status_konsultasi);
// 											dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].set('QTY',1);
// 											currentJasaDokterHargaJP_KasirIGD=cst.listData.tarifx;
// 											var pJasaRWJUrut=0;
// 											if(linePJasaRWJ>=1){
// 												pJasaRWJUrut=toInteger(dsTRDetailKasirRWJList.data.items[linePJasaRWJ-1].data.URUT); 
// 											}
// 											dsTRDetailKasirRWJList.data.items[linePJasaRWJ].data.URUT               = pJasaRWJUrut+1;
// 											if (cst.listData.status_konsultasi==true){
// 												var ayaan='ya';
// 												cekKomponen(cst.listData.kd_produk,cst.listData.kd_tarif,Ext.getCmp('txtKdUnitRWJ').getValue(),pJasaRWJUrut,cst.listData.tarifx,ayaan);
// 											}else{
// 												PenataJasaRJ.form.Grid.produk.startEditing(linePJasaRWJ,8);
// 												senderProdukPJasaRWJ(
// 													Ext.get('txtNoTransaksiKasirrwj').dom.value, 
// 													cst.listData.kd_produk, 
// 													cst.listData.kd_tarif, 
// 													currentKdKasirRWJ, 
// 													pJasaRWJUrut, 
// 													cst.listData.kd_unit, 
// 													tglGridBawah_poli,
// 													cst.listData.tgl_berlaku,
// 													1,
// 													cst.listData.tarifx,
// 													true
// 												);
// 											}
// 											xox=0;
// 										}else{
// 											ShowPesanWarningRWJ('Produk sudah ada','Warning');
// 											xox=0;
// 										}
// 									}else{
// 										PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row,4);
// 									}
// 								}
// 							});
// 						}
// 					}
// 				}
// 			})
//         },{
//         	id			: 'colDeskripsiRWJ2',
//             header		: 'Item Transaksi',
//             dataIndex	: 'DESKRIPSI',
//             sortable	: false,
//             hidden		: false,
// 			menuDisabled: true,
//             width		: 200,
//             editor		: PenataJasaRJ.form.ComboBox.produk= Nci.form.Combobox.autoComplete({
// 				store	: PenataJasaRJ.form.DataStore.produk,
// 				select	: function(a,b,c){
// 					console.log(b);
// 					var linePJasaRWJ	= PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.KD_PRODUK=b.data.kd_produk;
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.KP_PRODUK=b.data.kp_produk;
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.DESKRIPSI=b.data.deskripsi;
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.KD_TARIF=b.data.kd_tarif;
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.HARGA=b.data.harga;
//     				PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.TGL_BERLAKU=b.data.tgl_berlaku;
// 					PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.JUMLAH=b.data.jumlah;
// 					PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.STATUS_KONSULTASI=b.data.status_konsultasi;
//     				PenataJasaRJ.form.Grid.produk.getView().refresh();
// 					var pJasaRWJUrut=0;
// 					if(linePJasaRWJ>=1){
// 						pJasaRWJUrut=toInteger(dsTRDetailKasirRWJList.data.items[linePJasaRWJ-1].data.URUT); 
// 					}
// 					dsTRDetailKasirRWJList.data.items[linePJasaRWJ].data.URUT               = pJasaRWJUrut+1;
// 					if(b.data.status_konsultasi==true){
// 						var ayaan='ya';
// 						senderProdukPJasaRWJ(
// 							Ext.get('txtNoTransaksiKasirrwj').dom.value, 
// 							b.data.kd_produk, 
// 							b.data.kd_tarif, 
// 							currentKdKasirRWJ, 
// 							pJasaRWJUrut, 
// 							Ext.get("txtKdUnitRWJ").dom.value, 
// 							tglGridBawah_poli,
// 							b.data.tgl_berlaku,
// 							1,
// 							b.data.harga,
// 							true
// 						);
// 						cekKomponen(b.data.kd_produk,b.data.kd_tarif,Ext.getCmp('txtKdUnitRWJ').getValue(),pJasaRWJUrut,b.data.harga,ayaan);
// 					}else{
// 						PenataJasaRJ.form.Grid.produk.startEditing(linePJasaRWJ,8);
// 						senderProdukPJasaRWJ(
// 							Ext.get('txtNoTransaksiKasirrwj').dom.value, 
// 							b.data.kd_produk, 
// 							b.data.kd_tarif, 
// 							currentKdKasirRWJ, 
// 							pJasaRWJUrut, 
// 							Ext.get("txtKdUnitRWJ").dom.value, 
// 							tglGridBawah_poli,
// 							b.data.tgl_berlaku,
// 							1,
// 							b.data.harga,
// 							true
// 						);
// 					}
// 				},
// 				param	: function(){
// 					var params={};
// 					params['kd_unit']=Ext.getCmp('txtKdUnitRWJ').getValue();//o.KD_UNIT;
// 					params['kd_customer']=vkode_customer;
// 					return params;
// 				},
// 				insert	: function(o){
// 					return {
// 						kd_produk        :o.kd_produk,
// 						deskripsi 		: o.deskripsi,
// 						harga			: o.tarifx,
// 						kd_tarif		: o.kd_tarif,
// 						tgl_berlaku		: o.tgl_berlaku,
// 						jumlah			: o.jumlah,
// 						kp_produk			: o.kp_produk,
// 						status_konsultasi	: o.status_konsultasi,
// 						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kp_produk+'</td><td width="180" align="left">'+o.deskripsi+'</td><td width="130">Rp. '+o.tarifx+'</td></tr></table>'
// 				    }
// 				},
// 				url		: baseURL + "index.php/main/functionRWJ/getProduk",
// 				valueField: 'deskripsi',
// 				displayField: 'text',
// 			})
// 	        },{
//                header: 'Tanggal Transaksi',
//                dataIndex: 'TGL_TRANSAKSI',
//                width:100,
// 		   		menuDisabled:true,
//                renderer: function(v, params, record){
// 					return record.data.TGL_TRANSAKSI;
// 				}
//             },{
//                 id: 'colProblemRWJ2',
//                 header: 'Qty',
//                width:'100%',
// 				align: 'right',
// 				menuDisabled:true,
//                 dataIndex: 'QTY',
// 				width:50,
//                 editor: new Ext.form.TextField({
//                     id:'fieldcolProblemRWJ2',
//                     allowBlank: true,
//                     enableKeyEvents : true,
//                     width:50,
// 					listeners:{ 
// 						'specialkey' : function(a, b){
// 							if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
// 								var lineKasirRWJ = PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
// 								var pJasaRWJUrut=toInteger(PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.URUT)-1;
// 								senderProdukPJasaRWJ(
// 									Ext.get('txtNoTransaksiKasirrwj').dom.value, 
// 									PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.KD_PRODUK, 
// 									PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.KD_TARIF, 
// 									currentKdKasirRWJ, 
// 									pJasaRWJUrut, 
// 									Ext.getCmp('txtKdUnitRWJ').getValue(), 
// 									tglGridBawah_poli,
// 									PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.TGL_BERLAKU,
// 									Ext.get('fieldcolProblemRWJ2').dom.value,
// 									PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.HARGA,
// 									false
// 								);
// 								PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
// 								PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row+1,4);
// 							}
// 						}
// 					}
//                 })
//             },{
//                 id: 'colDokterPoli',
//                 header: 'Pelaksana',
// 				align: 'left',
// 				//hidden: true,
// 				menuDisabled:true,
//                 dataIndex: 'JUMLAH_DOKTER_VISITE',
// 				listeners: {
// 					dblclick: function (dataview, index, item, e) {
// 						loaddatastoredokterVisite_REVISI();
// 						PilihDokterLookUpPJ_RWJ_REVISI();
// 					},
// 				},
//             },{
//                 id: 'colDokterPoli',
//                 header: 'status',
// 				align: 'left',
// 				hidden: true,
// 				width:50,
// 				menuDisabled:true,
//                 dataIndex: 'JUMLAH',
// 				editor:{
// 					xtype:'textfield',
// 				}
//             },{
// 				id: 'colHargaPoli',
// 	            header: 'Harga',
// 	            align: 'right',
// 	            dataIndex: 'HARGA',
// 	            menuDisabled:true,
// 	            width:50,
// 	            summaryType:function(records){
// 	                var sum=0;
// 	                for(var i=0,iLen=records.length;i<iLen;i++){sum+=toInt(records[i].data.f4);}
// 	                return sum;
// 	            },
// 				listeners:{
// 				dblclick: function(dataview, index, item, e) {
// 					console.log(dsTRDetailKasirRWJList.data.items[item].data);
// 					console.log(item);
// 					dataRowIndexDetail = item;
// 					CurrentNoTransaksi_KasirRWJ = dsTRDetailKasirRWJList.data.items[item].data.NO_TRANSAKSI;
// 					PilihEditTarifLookUp_KasirRWJ(dsTRDetailKasirRWJList.data.items[item].data);
// 				}
// 			}
//             },
// 			{
// 				id: 'colTotalPoli',
// 	            header: 'Total',
// 	            align: 'right',
// 	            dataIndex: '',
// 	            menuDisabled:true,
// 	            width:50,
// 	            renderer: function (v, params, record)
// 				{
// 					return record.data.HARGA *  record.data.QTY; //maya
// 				}
//             },
// 			{
//                 id: 'colImpactRWJ2',
//                 header: 'CR',
//                 dataIndex: 'IMPACT',
// 				hidden: true,
//                 editor: new Ext.form.TextField({
//                     id:'fieldcolImpactRWJ2',
//                     allowBlank: true,
//                     enableKeyEvents : true,
//                     width:30
//                 })
// =======
// =======
// >>>>>>> b3d98c2e05b045a01a98721cdea2dbac565f2cde
// >>>>>>> 8ffe52a02ac518e1fdd672b59f2548c563f6cd89
// }
// if (xox === 0)
// {
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('KP_PRODUK',cst.listData.kp_produk);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('KD_PRODUK',cst.listData.kd_produk);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('DESKRIPSI',cst.listData.deskripsi);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('TGL_BERLAKU',cst.listData.tgl_berlaku);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('KD_KLAS',cst.listData.KD_KLAS);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('HARGA',cst.listData.tarifx);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('KD_TARIF',cst.listData.kd_tarif);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('STATUS_KONSULTASI',cst.listData.status_konsultasi);
// dsTRDetailKasirRWJList.getRange()[CurrentKasirRWJ.row].set('QTY',1);
// currentJasaDokterHargaJP_KasirIGD=cst.listData.tarifx;
// var currenturut= CurrentKasirRWJ.row + 1;
// if (cst.listData.status_konsultasi==true){
// var ayaan='ya';
// cekKomponen(cst.listData.kd_produk,cst.listData.kd_tarif,Ext.getCmp('txtKdUnitRWJ').getValue(),currenturut,cst.listData.tarifx,ayaan);
// }else{
// PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row,8);
// }
// xox=0;
// }else{
// ShowPesanWarningRWJ('Produk sudah ada','Warning');
// xox=0;
// }
// }else{
// PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row,4);
// }
// }
// });
// }
// }
// }
// })
// },{
// id			: 'colDeskripsiRWJ2',
// header		:'Item Transaksi',
// dataIndex	: 'DESKRIPSI',
// sortable	: false,
// hidden		:false,
// menuDisabled:true,
// width		:200,
// editor		: PenataJasaRJ.form.ComboBox.produk= Nci.form.Combobox.autoComplete({
// store	: PenataJasaRJ.form.DataStore.produk,
// select	: function(a,b,c){
// var line	= PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_PRODUK=b.data.kd_produk;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.KP_PRODUK=b.data.kp_produk;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.DESKRIPSI=b.data.deskripsi;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_TARIF=b.data.kd_tarif;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.HARGA=b.data.harga;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.TGL_BERLAKU=b.data.tgl_berlaku;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.JUMLAH=b.data.jumlah;
// PenataJasaRJ.dsGridTindakan.getRange()[line].data.STATUS_KONSULTASI=b.data.status_konsultasi;
// PenataJasaRJ.form.Grid.produk.getView().refresh();
// var currenturut= line + 1;
// if(b.data.status_konsultasi==true){
// var ayaan='ya';
// cekKomponen(b.data.kd_produk,b.data.kd_tarif,Ext.getCmp('txtKdUnitRWJ').getValue(),currenturut,b.data.harga,ayaan);
// }else{
// PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row,8);
// }
// },
// param	: function(){
// var params={};
// params['kd_unit']=Ext.getCmp('txtKdUnitRWJ').getValue();//o.KD_UNIT;
// params['kd_customer']=vkode_customer;
// return params;
// },
// insert	: function(o){
// return {
// kd_produk        :o.kd_produk,
// deskripsi 		: o.deskripsi,
// harga			: o.tarifx,
// kd_tarif		: o.kd_tarif,
// tgl_berlaku		: o.tgl_berlaku,
// jumlah			: o.jumlah,
// kp_produk			: o.kp_produk,
// status_konsultasi	: o.status_konsultasi,
// text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kp_produk+'</td><td width="160" align="left">'+o.deskripsi+'</td><td width="130">'+o.klasifikasi+'</td></tr></table>'
// }
// },
// url		: baseURL + "index.php/main/functionRWJ/getProduk",
// valueField: 'deskripsi',
// displayField: 'text',
// })
// },{
// header: 'Tanggal Transaksi',
// dataIndex: 'TGL_TRANSAKSI',
// width:100,
// menuDisabled:true,
// renderer: function(v, params, record){
// return record.data.TGL_TRANSAKSI;
// }
// },{
// id: 'colHARGARWj2',
// header: 'Harga',
// align: 'right',
// hidden: true,
// menuDisabled:true,
// dataIndex: 'HARGA',
// renderer: function(v, params, record){
// return formatCurrency(record.data.HARGA);
// }
// },{
// id: 'colProblemRWJ2',
// header: 'Qty',
// width:'100%',
// align: 'right',
// menuDisabled:true,
// dataIndex: 'QTY',
// width:50,
// editor: new Ext.form.TextField({
// id:'fieldcolProblemRWJ2',
// allowBlank: true,
// enableKeyEvents : true,
// width:50,
// listeners:{
// 'specialkey' : function(a, b){
// if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
// PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
// PenataJasaRJ.form.Grid.produk.startEditing(CurrentKasirRWJ.row+1,4);
// }
// }
// }
// })
// },{
// id: 'colDokterPoli',
// header: 'Dokter',
// align: 'left',
// hidden: true,
// menuDisabled:true,
// dataIndex: 'DOKTER',
// },{
// id: 'colDokterPoli',
// header: 'status',
// align: 'left',
// hidden: true,
// width:50,
// menuDisabled:true,
// dataIndex: 'JUMLAH',
// editor:{
// xtype:'textfield',
// }
// },{
// id: 'colImpactRWJ2',
// header: 'CR',
// dataIndex: 'IMPACT',
// hidden: true,
// editor: new Ext.form.TextField({
// id:'fieldcolImpactRWJ2',
// allowBlank: true,
// enableKeyEvents : true,
// width:30
// })
// }
// ]
// );
// }
function TRRawatJalanColumModel2() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      id: "colkonsulrwj2",
      header: "KONSUL",
      dataIndex: "STATUS_KONSULTASI",
      menuDisabled: true,
      hidden: true,
    },
    {
      id: "coleskripsirwj2",
      header: "Uraian",
      dataIndex: "DESKRIPSI2",
      menuDisabled: true,
      hidden: true,
    },
    {
      id: "coleskripsirwj23",
      header: "Kode",
      dataIndex: "KD_PRODUK",
      menuDisabled: true,
      hidden: true,
    },
    {
      id: "colKdProduk2",
      header: "Kode Produk",
      dataIndex: "KP_PRODUK",
      width: 75,
      menuDisabled: true,
      editor: new Ext.form.TextField({
        allowBlank: false,
        id: "fieldcolPenataJasaRWI",
        enableKeyEvents: true,
        listeners: {
          specialkey: function (a) {
            if (Ext.EventObject.getKey() === 13) {
              a.setValue(
                Ext.get("fieldcolPenataJasaRWI").dom.value.toUpperCase()
              );
              Ext.Ajax.request({
                url: baseURL + "index.php/main/functionIGD/getProdukKey",
                params: {
                  kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
                  kd_customer: vkode_customer,
                  text: Ext.get(
                    "fieldcolPenataJasaRWI"
                  ).dom.value.toUpperCase(),
                },
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  if (cst.listData != null) {
                    var line = dsTRDetailKasirRWJList.getCount();
                    var kp_prod2 = cst.listData.kp_produk;
                    var xox = 0;
                    for (var i = 0; i < line - 1; i++) {
                      var o =
                        dsTRDetailKasirRWJList.data.items[i].data.KP_PRODUK;
                      if (kp_prod2 == o) {
                        xox = xox + 1;
                      } else {
                        xox = xox + 0;
                      }
                    }
                    if (xox === 0) {
                      var linePJasaRWJ =
                        PenataJasaRJ.form.Grid.produk.getSelectionModel()
                          .selection.cell[0];
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("KP_PRODUK", cst.listData.kp_produk);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("KD_PRODUK", cst.listData.kd_produk);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("DESKRIPSI", cst.listData.deskripsi);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set(
                          "TGL_BERLAKU",
                          cst.listData.tgl_berlaku
                        );
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("KD_KLAS", cst.listData.KD_KLAS);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("HARGA", cst.listData.tarifx);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("KD_TARIF", cst.listData.kd_tarif);
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set(
                          "STATUS_KONSULTASI",
                          cst.listData.status_konsultasi
                        );
                      dsTRDetailKasirRWJList
                        .getRange()
                        [linePJasaRWJ].set("QTY", 1);
                      currentJasaDokterHargaJP_KasirIGD = cst.listData.tarifx;
                      var pJasaRWJUrut = 0;
                      if (linePJasaRWJ >= 1) {
                        pJasaRWJUrut = toInteger(
                          dsTRDetailKasirRWJList.data.items[linePJasaRWJ - 1]
                            .data.URUT
                        );
                      }
                      dsTRDetailKasirRWJList.data.items[
                        linePJasaRWJ
                      ].data.URUT = pJasaRWJUrut + 1;
                      if (cst.listData.status_konsultasi == true) {
                        var ayaan = "ya";
                        cekKomponen(
                          cst.listData.kd_produk,
                          cst.listData.kd_tarif,
                          Ext.getCmp("txtKdUnitRWJ").getValue(),
                          pJasaRWJUrut,
                          cst.listData.tarifx,
                          ayaan
                        );
                      } else {
                        PenataJasaRJ.form.Grid.produk.startEditing(
                          linePJasaRWJ,
                          8
                        );
                        senderProdukPJasaRWJ(
                          Ext.get("txtNoTransaksiKasirrwj").dom.value,
                          cst.listData.kd_produk,
                          cst.listData.kd_tarif,
                          currentKdKasirRWJ,
                          pJasaRWJUrut,
                          cst.listData.kd_unit,
                          tglGridBawah_poli,
                          cst.listData.tgl_berlaku,
                          1,
                          cst.listData.tarifx,
                          true
                        );
                      }
                      xox = 0;
                    } else {
                      ShowPesanWarningRWJ("Produk sudah ada", "Warning");
                      xox = 0;
                    }
                  } else {
                    PenataJasaRJ.form.Grid.produk.startEditing(
                      CurrentKasirRWJ.row,
                      4
                    );
                  }
                },
              });
// <<<<<<< HEAD
// =======
// <<<<<<< HEAD
// // >>>>>>> 6801f17 (added new RME RWJ dan IGD 06-11-2023)
// =======
// >>>>>>> b3d98c2e05b045a01a98721cdea2dbac565f2cde
// >>>>>>> 8ffe52a02ac518e1fdd672b59f2548c563f6cd89
            }
          },
        },
      }),
    },
    {
      id: "colDeskripsiRWJ2",
      header: "Item Transaksi",
      dataIndex: "DESKRIPSI",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 400,
      editor: (PenataJasaRJ.form.ComboBox.produk =
        Nci.form.Combobox.autoComplete({
          store: PenataJasaRJ.form.DataStore.produk,
          select: function (a, b, c) {
            var linePJasaRWJ =
              PenataJasaRJ.form.Grid.produk.getSelectionModel().selection
                .cell[0];
            PenataJasaRJ.dsGridTindakan.getRange()[
              linePJasaRWJ
            ].data.KD_PRODUK = b.data.kd_produk;
            PenataJasaRJ.dsGridTindakan.getRange()[
              linePJasaRWJ
            ].data.KP_PRODUK = b.data.kp_produk;
            PenataJasaRJ.dsGridTindakan.getRange()[
              linePJasaRWJ
            ].data.DESKRIPSI = b.data.deskripsi;
            PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.KD_TARIF =
              b.data.kd_tarif;
            PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.HARGA =
              b.data.harga;
            PenataJasaRJ.dsGridTindakan.getRange()[
              linePJasaRWJ
            ].data.TGL_BERLAKU = b.data.tgl_berlaku;
            PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].data.JUMLAH =
              b.data.jumlah;
            PenataJasaRJ.dsGridTindakan.getRange()[
              linePJasaRWJ
            ].data.STATUS_KONSULTASI = b.data.status_konsultasi;
            PenataJasaRJ.form.Grid.produk.getView().refresh();
            var pJasaRWJUrut = 0;
            if (linePJasaRWJ >= 1) {
              pJasaRWJUrut = toInteger(
                dsTRDetailKasirRWJList.data.items[linePJasaRWJ - 1].data.URUT
              );
            }
            dsTRDetailKasirRWJList.data.items[linePJasaRWJ].data.URUT =
              pJasaRWJUrut + 1;
            if (b.data.status_konsultasi == true) {
              var ayaan = "ya";
              senderProdukPJasaRWJ(
                Ext.get("txtNoTransaksiKasirrwj").dom.value,
                b.data.kd_produk,
                b.data.kd_tarif,
                currentKdKasirRWJ,
                pJasaRWJUrut,
                Ext.get("txtKdUnitRWJ").dom.value,
                tglGridBawah_poli,
                b.data.tgl_berlaku,
                1,
                b.data.harga,
                true
              );
              cekKomponen(
                b.data.kd_produk,
                b.data.kd_tarif,
                Ext.getCmp("txtKdUnitRWJ").getValue(),
                pJasaRWJUrut,
                b.data.harga,
                ayaan
              );
            } else {
              PenataJasaRJ.form.Grid.produk.startEditing(linePJasaRWJ, 8);
              senderProdukPJasaRWJ(
                Ext.get("txtNoTransaksiKasirrwj").dom.value,
                b.data.kd_produk,
                b.data.kd_tarif,
                currentKdKasirRWJ,
                pJasaRWJUrut,
                Ext.get("txtKdUnitRWJ").dom.value,
                tglGridBawah_poli,
                b.data.tgl_berlaku,
                1,
                b.data.harga,
                true
              );
            }
          },
          param: function () {
            var params = {};
            params["kd_unit"] = Ext.getCmp("txtKdUnitRWJ").getValue(); //o.KD_UNIT;
            params["kd_customer"] = vkode_customer;
            return params;
          },
          insert: function (o) {
            return {
              kd_produk: o.kd_produk,
              deskripsi: o.deskripsi,
              harga: o.tarifx,
              kd_tarif: o.kd_tarif,
              tgl_berlaku: o.tgl_berlaku,
              jumlah: o.jumlah,
              kp_produk: o.kp_produk,
              status_konsultasi: o.status_konsultasi,
              text:
                '<table style="font-size: 11px;"><tr><td width="50">' +
                o.kp_produk +
                '</td><td width="180" align="left">' +
                o.deskripsi +
                '</td><td width="130">Rp. ' +
                o.tarifx +
                "</td></tr></table>",
            };
          },
          url: baseURL + "index.php/main/functionRWJ/getProduk",
          valueField: "deskripsi",
          displayField: "text",
        })),
    },
    {
      header: "Tanggal Transaksi",
      dataIndex: "TGL_TRANSAKSI",
      width: 75,
      menuDisabled: true,
      renderer: function (v, params, record) {
        return record.data.TGL_TRANSAKSI;
      },
    },
    {
      id: "colProblemRWJ2",
      header: "Qty",
      width: "100%",
      align: "right",
      menuDisabled: true,
      dataIndex: "QTY",
      width: 50,
      editor: new Ext.form.TextField({
        id: "fieldcolProblemRWJ2",
        allowBlank: true,
        enableKeyEvents: true,
        width: 50,
        listeners: {
          specialkey: function (a, b) {
            if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
              var lineKasirRWJ =
                PenataJasaRJ.form.Grid.produk.getSelectionModel().selection
                  .cell[0];
              var pJasaRWJUrut =
                toInteger(
                  PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.URUT
                ) - 1;
              senderProdukPJasaRWJ(
                Ext.get("txtNoTransaksiKasirrwj").dom.value,
                PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data
                  .KD_PRODUK,
                PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data
                  .KD_TARIF,
                currentKdKasirRWJ,
                pJasaRWJUrut,
                Ext.getCmp("txtKdUnitRWJ").getValue(),
                tglGridBawah_poli,
                PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data
                  .TGL_BERLAKU,
                Ext.get("fieldcolProblemRWJ2").dom.value,
                PenataJasaRJ.dsGridTindakan.data.items[lineKasirRWJ].data.HARGA,
                false
              );
              PenataJasaRJ.dsGridTindakan.insert(
                PenataJasaRJ.dsGridTindakan.getCount(),
                PenataJasaRJ.func.getNullProduk()
              );
              PenataJasaRJ.form.Grid.produk.startEditing(
                CurrentKasirRWJ.row + 1,
                4
              );
            }
          },
        },
      }),
    },
    {
      id: "colNamaDokterPoli",
      header: "Pelaksana",
      align: "left",
      width: 300,
      //hidden: true,
      // menuDisabled: true,
      // dataIndex: "JUMLAH_DOKTER_VISITE",
      dataIndex: "NAMA_DOKTER_VISITE",
      listeners: {
        dblclick: function (dataview, index, item, e) {
					console.log('testestes');
          loaddatastoredokterVisite_REVISI();
          PilihDokterLookUpPJ_RWJ_REVISI();
        },
      },
    },
    {
      id: "colDokterPoli",
      header: "status",
      align: "left",
      hidden: true,
      width: 50,
      menuDisabled: true,
      dataIndex: "JUMLAH",
      editor: {
        xtype: "textfield",
      },
    },
    {
      id: "colHargaPoli",
      header: "Harga",
      align: "right",
      dataIndex: "HARGA",
      menuDisabled: true,
      width: 50,
      summaryType: function (records) {
        var sum = 0;
        for (var i = 0, iLen = records.length; i < iLen; i++) {
          sum += toInt(records[i].data.f4);
        }
        return sum;
      },
      /* listeners: {
        dblclick: function(dataview, index, item, e){
          LookUp_UpdateTarifTindakan();
        }
      } */
      // HUDI
      // 12-05-2024
      editor: new Ext.form.TextField({
        id: "fieldcolEditTarif",
        allowBlank: true,
        enableKeyEvents: true,
        width: 50,
        listeners: {
          specialkey: function (a, b) {
            if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
              console.log(a.getValue());
              var linePJasaRWJ = PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
              PenataJasaRJ.dsGridTindakan.getRange()[linePJasaRWJ].set('HARGA',a.getValue());
              Ext.Ajax.request({
                url: baseURL + "index.php/main/functionRWJ/editTarifTindakan",
                params: {
                  kd_kasir : '01', // kd_kasir rwj
                  no_transaksi : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
                  urut : PenataJasaRJ.dsGridTindakan.data.items[linePJasaRWJ].data.URUT,
                  tgl_transaksi : PenataJasaRJ.dsGridTindakan.data.items[linePJasaRWJ].data.TGL_TRANSAKSI,
                  kd_produk : PenataJasaRJ.dsGridTindakan.data.items[linePJasaRWJ].data.KD_PRODUK,
                  harga : a.getValue()
                },
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  if(cst.success == true){
                    ShowPesanInfoRWJ("Edit Tarif berhasil disimpan", "Information");
                  }
                }
              });
            }
          }
        }
      })
    },
    {
      id: "colTotalPoli",
      header: "Total",
      align: "right",
      dataIndex: "",
      menuDisabled: true,
      width: 50,
      renderer: function (v, params, record) {
        return record.data.HARGA * record.data.QTY; //maya
      },
    },
    {
      id: "colImpactRWJ2",
      header: "CR",
      dataIndex: "IMPACT",
      hidden: true,
      editor: new Ext.form.TextField({
        id: "fieldcolImpactRWJ2",
        allowBlank: true,
        enableKeyEvents: true,
        width: 30,
      }),
    },
  ]);
}
function senderProdukPJasaRWJ(
  no_transaksi,
  kd_produk,
  kd_tarif,
  kd_kasir,
  urut,
  unit,
  tgl_transaksi,
  tgl_berlaku,
  quantity,
  harga,
  fungsi
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionIGD/insertDataProduk",
    params: {
      no_transaksi: no_transaksi,
      kd_produk: kd_produk,
      kd_tarif: kd_tarif,
      kd_kasir: kd_kasir,
      kd_dokter: Ext.getCmp("txtKdDokter").getValue(),
      urut: parseInt(urut) + 1,
      unit: unit,
      tgl_transaksi: tgl_transaksi,
      tgl_berlaku: tgl_berlaku,
      quantity: quantity,
      harga: harga,
      fungsi: fungsi,
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      /*if (cst.status === true || cst.action == "insert") {
				Ext.getCmp('KasirRWJ_gridTindakan').startEditing(line,10);
        	}
			else if (cst.status === true || cst.action == "update") {
				TambahBarisRWJ();
				Ext.getCmp('KasirRWJ_gridTindakan').startEditing(line+1,4);
        	}*/
      console.log(kd_produk);
      if (kd_produk == "3") {
        Ext.Ajax.request({
          url:
            baseURL + "index.php/rawat_inap/control_visite_dokter/insertDokter",
          params: {
            label: tmpKdJob,
            kd_job: tmpKdJob,
            no_transaksi: Ext.get("txtNoTransaksiKasirrwj").dom.value,
            tgl_transaksi: nowTglTransaksiGrid_poli.format("Y-m-d"),
            kd_produk: kd_produk,
            urut: parseInt(urut) + 1,
            // no_transaksi    : Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
            // tgl_transaksi   : getFormatTanggal(Ext.get('dtpTanggalDetransaksi').getValue()),
            // kd_produk       : currentJasaDokterKdProduk_RWJ,
            // urut            : cellSelecteddeskripsi.data.URUT,
            kd_kasir: kd_kasir,
            kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
            kd_dokter: Ext.getCmp("txtKdDokter").getValue(),
            kd_tarif: kd_tarif,
            tgl_berlaku: tgl_berlaku,
            group: 0,
          },
          success: function (response) {
            var cst = Ext.decode(response.responseText);
            var dataRowIndexDetail = parseInt(urut);
            console.log(dataRowIndexDetail);
            if (cst.data.JUMLAH_DOKTER > 0) {
              PenataJasaRJ.dsGridTindakan
                .getRange()
                [dataRowIndexDetail].set(
                  "DESKRIPSI",
                  cst.data.DESKRIPSI + " (" + cst.data.DAFTAR_DOKTER + ")"
                );
            } else {
              PenataJasaRJ.dsGridTindakan
                .getRange()
                [dataRowIndexDetail].set("DESKRIPSI", cst.data.DESKRIPSI);
            }
            PenataJasaRJ.dsGridTindakan
              .getRange()
              [dataRowIndexDetail].set(
                "JUMLAH_DOKTER_VISITE",
                cst.data.JUMLAH_DOKTER
              );
          },
        });
      }
      dsTRDetailKasirRWJList.getRange()[line].set("DOKTER", cst.dokter);
      console.log(harga * quantity);
      Ext.getCmp("txtFieldTotalHarga").setValue(
        parseInt(Ext.getCmp("txtFieldTotalHarga").getValue()) + harga * quantity
      );
    },
  });
}
function senderProdukDeletePJasaRWJ(
  kd_kasir,
  kd_produk,
  no_transaksi,
  urut,
  tgl_transaksi
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/hapusDataProduk_",
    params: {
      kd_produk: kd_produk,
      no_transaksi: no_transaksi,
      kd_kasir: kd_kasir,
      urut: urut,
      tgl_transaksi: tgl_transaksi,
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status == true) {
        dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
      } else if (cst.tahap == "kode user") {
        Ext.MessageBox.alert("Gagal", "Hapus Gagal, Kode User Tidak ada.");
      } else {
        Ext.MessageBox.alert("Gagal", "Hapus Gagal, Hubuungi IPDE.");
      }
    },
  });
}
function PilihDokterLookUp_RWJ(edit, kondisi) {
  var GridTrDokterColumnModel = new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "kd_component",
      dataIndex: "kd_component",
      width: 80,
      menuDisabled: true,
      hidden: true,
    },
    {
      header: "tgl_berlaku",
      dataIndex: "tgl_berlaku",
      width: 80,
      menuDisabled: true,
      hidden: true,
    },
    {
      header: "Komponent",
      dataIndex: "component",
      width: 200,
      menuDisabled: true,
      hidden: false,
    },
    {
      header: "kd_dokter",
      dataIndex: "kd_dokter",
      width: 80,
      menuDisabled: true,
      hidden: true,
    },
    {
      header: "Nama Dokter",
      dataIndex: "nama",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 250,
      editor: new Nci.form.Combobox.autoComplete({
        store: dsgridpilihdokterpenindak_RWJ,
        select: function (a, b, c) {
          var line = GridDokterTr_RWJ.getSelectionModel().selection.cell[0];
          dsGridJasaDokterPenindak_RWJ.getRange()[line].data.kd_dokter =
            b.data.kd_dokter;
          dsGridJasaDokterPenindak_RWJ.getRange()[line].data.nama = b.data.nama;
          GridDokterTr_RWJ.getView().refresh();
        },
        insert: function (o) {
          return {
            kd_dokter: o.kd_dokter,
            nama: o.nama,
            text:
              '<table style="font-size: 11px;"><tr><td width="50">' +
              o.kd_dokter +
              '</td><td width="200">' +
              o.nama +
              "</td></tr></table>",
          };
        },
        param: function () {
          var params = {};
          params["kd_unit"] = Ext.getCmp("txtKdUnitRWJ").getValue();
          params["kd_customer"] = vkode_customer;
          params["penjas"] = "rwj";
          return params;
        },
        url: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
        valueField: "nama_obat",
        displayField: "text",
        listWidth: 380,
      }),
      //getTrDokter(dsTrDokter)
    },
  ]);

  var fldDetail = [];
  dsGridJasaDokterPenindak_RWJ = new WebApp.DataStore({ fields: fldDetail });
  GridDokterTr_RWJ = new Ext.grid.EditorGridPanel({
    id: "GridDokterTr_RWJ",
    stripeRows: true,
    width: 487,
    height: 160,
    store: dsGridJasaDokterPenindak_RWJ,
    border: true,
    frame: false,
    autoScroll: true,
    cm: GridTrDokterColumnModel,
    listeners: {
      rowclick: function ($this, rowIndex, e) {
        //trcellCurrentTindakan_RWJ = rowIndex;
      },
      celldblclick: function (
        gridView,
        htmlElement,
        columnIndex,
        dataRecord
      ) {},
    },
    viewConfig: { forceFit: true },
  });

  var lebar = 500;
  var FormLookUDokter_RWJ = new Ext.Window({
    id: "winTRDokterPenindak_RWJ",
    title: "Pilih Dokter Penindak",
    closeAction: "destroy",
    width: 500,
    height: 220,
    border: false,
    resizable: false,
    iconCls: "Request",
    constrain: true,
    modal: true,
    items: [GridDokterTr_RWJ],
    tbar: [
      {
        xtype: "button",
        text: "Simpan",
        iconCls: "save",
        hideLabel: true,
        id: "BtnOktrDokter",
        handler: function () {
          savetransaksi(kondisi);
        },
      },
      "-",
    ],
    listeners: {},
  });

  FormLookUDokter_RWJ.show();
  if (edit == true) {
    GetgridEditDokterPenindakJasa_RWJ(
      currentJasaDokterKdProduk_RWJ,
      currentJasaDokterKdTarif_RWJ
    );
  } else {
    GetgridPilihDokterPenindakJasa_RWJ(
      currentJasaDokterKdProduk_RWJ,
      currentJasaDokterKdTarif_RWJ
    );
  }
}


function GetLookupAssetCMRWJ(str) {
  if (AddNewKasirRWJ === true) {
    var p = new mRecordRwj({
      DESKRIPSI2: "",
      KD_PRODUK: "",
      DESKRIPSI: "",
      KD_TARIF: "",
      HARGA: "",
      QTY: dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
      TGL_TRANSAKSI:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data
          .TGL_TRANSAKSI,
      DESC_REQ:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
      KD_TARIF:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
      URUT: dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT,
    });
    FormLookupKasirRWJ(str, dsTRDetailKasirRWJList, p, true, "", false);
  } else {
    var p = new mRecordRwj({
      DESKRIPSI2: "",
      KD_PRODUK: "",
      DESKRIPSI: "",
      KD_TARIF: "",
      HARGA: "",
      QTY: dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
      TGL_TRANSAKSI:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data
          .TGL_TRANSAKSI,
      DESC_REQ:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
      KD_TARIF:
        dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
      URUT: dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT,
    });
    FormLookupKasirRWJ(
      str,
      dsTRDetailKasirRWJList,
      p,
      false,
      CurrentKasirRWJ.row,
      false
    );
  }
}

function RecordBaruRWJ() {
  var p = new mRecordRwj({
    DESKRIPSI2: "",
    KD_PRODUK: "",
    DESKRIPSI: "",
    KD_TARIF: "",
    HARGA: "",
    QTY: "",
    TGL_TRANSAKSI: tanggaltransaksitampung,
    DESC_REQ: "",
    KD_TARIF: "",
    URUT: "",
  });
  return p;
}

function RefreshDataSetDiagnosa(medrec, unit, tgl) {
  var strKriteriaDiagnosa = "";
  strKriteriaDiagnosa =
    "A.kd_pasien = ~" +
    medrec +
    "~ and A.kd_unit=~" +
    unit +
    "~ and A.tgl_masuk in(~" +
    tgl +
    "~)";
  dsTRDetailDiagnosaList.load({
    params: {
      Skip: 0,
      Take: selectCountDiagnosa,
      Sort: "kd_penyakit",
      Sortdir: "ASC",
      target: "ViewDiagnosaRJPJ",
      param: strKriteriaDiagnosa,
    },
  });
  rowSelectedDiagnosa = undefined;
  return dsTRDetailDiagnosaList;
}

function RefreshDataSetAnamnese() {
  var strKriteriaDiagnosa = "";
  strKriteriaDiagnosa =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and mr_konpas.kd_unit=~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ and tgl_masuk = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
    

    var strKriteriaDiagnosa2 = "";
  strKriteriaDiagnosa2 =
    "LEFT JOIN MR_KONPAS_MAPPING MKM ON MKM.KD_PASIEN = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ AND MKM.KD_UNIT = ~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ AND MKM.tgl_masuk =  ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~ AND MKM.urut_masuk = ~" + Ext.get("txtKdUrutMasuk").getValue() + 
    "~ AND MKM.id_kondisi = A.id_kondisi LEFT JOIN MAP_KONPAS_RWJ D ON D.id_kondisi = A.id_kondisi AND D.kd_pasien = ~" + 
    Ext.get("txtNoMedrecDetransaksi").getValue() + "~ AND D.kd_unit= ~" + 
    Ext.get("txtKdUnitRWJ").getValue() + "~ AND D.urut_masuk= ~"+ 
    Ext.get("txtKdUrutMasuk").getValue() +"~ AND D.tgl_masuk= ~" + 
    Ext.get("dtpTanggalDetransaksi").dom.value + "~";

  dsTRDetailAnamneseList.load({
    params: {
      Skip: 0,
      Take: 50,
      Sort: "orderlist",
      Sortdir: "ASC",
      target: "viewkondisifisik",
      param: strKriteriaDiagnosa+"##"+strKriteriaDiagnosa2,
    },
  });
  rowSelectedDiagnosa = undefined;
  return dsTRDetailDiagnosaList;
}

function RefreshDataPemeriksaanMata() {
  var strKriteriaDiagnosa = "";
  strKriteriaDiagnosa =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and mr_eye.kd_unit=~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ and tgl_masuk = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
  dsTRDetailEyecareList.load({
    params: {
      Skip: 0,
      Take: 50,
      Sort: "orderlist",
      Sortdir: "ASC",
      target: "viewpemeriksaanmata",
      param: strKriteriaDiagnosa,
    },
  });
  rowSelectedDiagnosa = undefined;
  return dsTRDetailEyecareList;
}

function RefreshDataPemeriksaanGigi() {
  var strKriteriaDiagnosa = "";
  strKriteriaDiagnosa =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and mr_dental.kd_unit=~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ and tgl_masuk = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
  dsTRDetailDentalcareList.load({
    params: {
      Skip: 0,
      Take: 50,
      Sort: "orderlist",
      Sortdir: "ASC",
      target: "viewpemeriksaangigi",
      param: strKriteriaDiagnosa,
    },
  });
  rowSelectedDiagnosa = undefined;
  return dsTRDetailDentalcareList;
}

function RefreshDataPemeriksaanGigi2() {
  var strKriteriaDiagnosa = "";
  strKriteriaDiagnosa =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and mr_dental.kd_unit=~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ and tgl_masuk = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
  dsTRDetailDentalcareList2.load({
    params: {
      Skip: 0,
      Take: 50,
      Sort: "orderlist",
      Sortdir: "ASC",
      target: "viewpemeriksaangigi2",
      param: strKriteriaDiagnosa,
    },
  });
  rowSelectedDiagnosa = undefined;
  return dsTRDetailDentalcareList2;
}

function TRRWJInit(rowdata_AG) {
  console.log(rowdata_AG);
  AddNewKasirRWJ = false;
  Ext.get("txtNoTransaksiKasirrwj").dom.value = rowdata_AG.NO_TRANSAKSI;
  tanggaltransaksitampung = rowdata_AG.TANGGAL_TRANSAKSI;
  Ext.get("dtpTanggalDetransaksi").dom.value = ShowDate(
    rowdata_AG.TANGGAL_TRANSAKSI
  );
  Ext.get("txtTglLahirRWJ").dom.value = ShowDate(
    rowdata_AG.TGL_LAHIR
  );
  Ext.get("txtUsia").dom.value = rowdata_AG.UMUR.YEAR + " tahun";
  tgl_transaksidatanyarwj = rowdata_AG.TANGGAL_TRANSAKSI;
  unitlengkap = rowdata_AG.KD_UNIT + " [" + rowdata_AG.NAMA_UNIT + "]";
  console.log(unitlengkap);
  Ext.get("txtNoMedrecDetransaksi").dom.value = rowdata_AG.KD_PASIEN;
  Ext.get("txtNamaPasienDetransaksi").dom.value = rowdata_AG.NAMA;
  Ext.get("txtKdDokter").dom.value = rowdata_AG.KD_DOKTER;
  Ext.get("txtNamaDokter").dom.value = rowdata_AG.NAMA_DOKTER;
  Ext.get("txtKdUnitRWJ").dom.value = rowdata_AG.KD_UNIT;
  Ext.get("txtNamaUnit").dom.value = rowdata_AG.NAMA_UNIT;
  Ext.get("txtUnit").dom.value = unitlengkap;
  Ext.get("txtCustomer").dom.value = rowdata_AG.CUSTOMER;
  Ext.get("txtKDCustomer").dom.value = rowdata_AG.KD_CUSTOMER;
  Ext.get("txtKdUrutMasuk").dom.value = rowdata_AG.URUT_MASUK;

  // if(rowdata_AG.TGL_TINDAK != null && rowdata_AG.TGL_TINDAK !=''){
  // 	Ext.getCmp('textfieldTgl_RespSelesai').setValue(new Date(rowdata_AG.TGL_TINDAK));
  // }else{
  // 	Ext.getCmp('textfieldTgl_RespSelesai').setValue(new Date());
  // }
  var tmp_jam = new Date();
  tmp_jam = tmp_jam.format("H:i:s");
  if (rowdata_AG.JAM_DATANG != null && rowdata_AG.JAM_DATANG != "") {
    Ext.getCmp("txtJam_datang").setValue(rowdata_AG.JAM_DATANG);
  } else {
    Ext.getCmp("txtJam_datang").setValue(tmp_jam);
  }

  if (
    rowdata_AG.JAM_JADWAL_DOKTER != null &&
    rowdata_AG.JAM_JADWAL_DOKTER != ""
  ) {
    Ext.getCmp("dtpJadwal_Dokter").setValue(rowdata_AG.JAM_JADWAL_DOKTER);
  } else {
    Ext.getCmp("txtJam_datang").setValue(tmp_jam);
  }

  if (rowdata_AG.JAM_DITINDAK != null && rowdata_AG.JAM_DITINDAK != "") {
    Ext.getCmp("txtJam_diperiksa").setValue(rowdata_AG.JAM_DITINDAK);
  } else {
    Ext.getCmp("txtJam_datang").setValue(tmp_jam);
  }

  vkode_customer = rowdata_AG.KD_CUSTOMER;
  currentKdKasirRWJ = rowdata_AG.KD_KASIR;
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionLABPoliklinik/gettarif",
    params: {
      kd_customer: vkode_customer,
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      PenataJasaRJ.varkd_tarif = cst.kd_tarif;
      dokter_leb_rwj();
      dokter_rad_rwj();
      getproduk_PJRWJ();
    },
  });

  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_order_mng",
    params: {
      kd_pasien: rowdata_AG.KD_PASIEN,
      kd_unit: rowdata_AG.KD_UNIT,
      tgl_masuk_knj: rowdata_AG.TANGGAL_TRANSAKSI,
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        var jam_op = cst.jam_op.split(":");
        Ext.getCmp("TglOperasi_viJdwlOperasi").setValue(ShowDate(cst.tgl_op));
        Ext.getCmp("txtJam_viJdwlOperasi").setValue(jam_op[0]);
        Ext.getCmp("txtMenit_viJdwlOperasi").setValue(jam_op[1]);
        Ext.getCmp("cbo_viComboJenisTindakan_viJdwlOperasi").setValue(
          cst.kd_tindakan
        );
        Ext.getCmp("cbo_viComboKamar_viJdwlOperasi").setValue(cst.no_kamar);
      }
    },
  });

  ViewGridBawahpoliLab(
    rowdata_AG.NO_TRANSAKSI,
    Ext.getCmp("txtKdUnitRWJ").getValue(),
    rowdata_AG.TANGGAL_TRANSAKSI,
    rowdata_AG.URUT_MASUK
  );
  ViewGridBawahpoliRad(
    rowdata_AG.NO_TRANSAKSI,
    Ext.getCmp("txtKdUnitRWJ").getValue(),
    rowdata_AG.TANGGAL_TRANSAKSI,
    rowdata_AG.URUT_MASUK
  );
  // console.log(rowdata_AG.NO_TRANSAKSI);
  RefreshDataKasirRWJDetail(rowdata_AG.NO_TRANSAKSI);
  RefreshDataSetAnamnese();
  console.log(rowdata_AG);
  if (rowdata_AG.KD_UNIT === "221") {
    RefreshDataPemeriksaanMata();
  }
  if (rowdata_AG.KD_UNIT === "203") {
    RefreshDataPemeriksaanGigi();  
    RefreshDataPemeriksaanGigi2();  
  }
  var $this = this;
  PenataJasaRJ.dsComboObat = new WebApp.DataStore({
    fields: ["kd_prd", "nama_obat", "jml_stok_apt"],
  });
  PenataJasaRJ.dsComboObat.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewComboObatRJPJ",
      param: vkode_customer,
    },
  });
  RefreshDataSetDiagnosa(
    rowdata_AG.KD_PASIEN,
    rowdata_AG.KD_UNIT,
    rowdata_AG.TANGGAL_TRANSAKSI
  );

  Ext.Ajax.request({
    url: baseURL + "index.php/main/getcurrentshift",
    params: {
      command: "0",
      // parameter untuk url yang dituju (fungsi didalam controller)
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      tampungshiftsekarang = o.responseText;
      //alert(tampungshiftsekarang);
    },
  });
  viewGridIcd9_RWJ();
  viewGridRiwayatKunjunganPasien_RWJ();
}

function mEnabledRWJCM(mBol) {
  Ext.get("btnLookupRWJ").dom.disabled = mBol;
  Ext.get("btnHpsBrsRWJ_AG").dom.disabled = mBol;
}

function RWJAddNew() {
  AddNewKasirRWJ = true;
  Ext.get("txtNoTransaksiKasirrwj").dom.value = "";
  Ext.get("dtpTanggalDetransaksi").dom.value = nowTglTransaksi.format("d/M/Y");
  Ext.get("txtNoMedrecDetransaksi").dom.value = "";
  Ext.get("txtNamaPasienDetransaksi").dom.value = "";
  Ext.get("txtKdDokter").dom.value = undefined;
  Ext.get("txtNamaDokter").dom.value = "";
  Ext.get("txtKdUrutMasuk").dom.value = "";
  Ext.get("cboStatus_viKasirRwj").dom.value = "";
  rowSelectedKasirRWJ = undefined;
  dsTRDetailKasirRWJList.removeAll();
  mEnabledRWJCM(false);
}

function RefreshDataKasirRWJDetai2(data) {
  dsPjTrans2.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "kd_obat",
      Sortdir: "ASC",
      resep: "Y",
      target: "ViewResepRWJ",
      param:
        "KD_PASIEN='" +
        data.KD_PASIEN +
        "' AND KD_UNIT = '" +
        data.KD_UNIT +
        "' AND TGL_MASUK = '" +
        data.TANGGAL_TRANSAKSI +
        "'",
    },
  });
  return dsPjTrans2;
}

function RefreshDataKasirRWJDetail(no_transaksi) {
  var strKriteriaRWJ = "";
  console.log(TrKasirRWJ_AG);
  strKriteriaRWJ =
    '"no_transaksi" = ~' +
    no_transaksi +
    "~ and kd_kasir=~" +
    TrKasirRWJ_AG.setting.KD_KASIR +
    "~";
  dsTRDetailKasirRWJList.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "tgl_transaksi",
      Sortdir: "ASC",
      target: "ViewDetailRWJGridBawah",
      param: strKriteriaRWJ,
      mod_id: TrKasirRWJ_AG.mod_id,
    },
  });
  return dsTRDetailKasirRWJList;
}

//KALKUKASI TOTAL TINDAKAN
function HitungTotalTIndakanPenataJasa(dsTRDetailKasirRWJList) {
    var total = 0;
    console.log(dsTRDetailKasirRWJList);
    console.log(dsTRDetailKasirRWJList.getCount());
    for (var i = 0; i < dsTRDetailKasirRWJList.getCount(); i++) {
        console.log(dsTRDetailKasirRWJList.data);
        total =
            parseFloat(total) +
            parseFloat(
                dsTRDetailKasirRWJList.data.items[i].data.HARGA *
                dsTRDetailKasirRWJList.data.items[i].data.QTY
            );
    }
    console.log(total);
    Ext.getCmp("txtFieldTotalHarga").setValue(total);
}


function getParamDetailTransaksiRWJ(kondisi) {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    kondisinya: kondisi,
    kdDokter: Ext.get("txtKdDokter").getValue(),
    Tgl: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
    Shift: tampungshiftsekarang,
    List: getArrDetailTrRWJ(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: GetListCountDetailTransaksi(),
    Hapus: 1,
    Ubah: 0,
  };
  params.jmlObat = PenataJasaRJ.dsGridObat.getRange().length;
  params.urut_masuk = PenataJasaRJ.s1.data.URUT_MASUK;
  params.kd_pasien = PenataJasaRJ.s1.data.KD_PASIEN;
  console.log(PenataJasaRJ.dsGridObat);
  var list_obat = [];
  /*
	
    
    var variable_keperawatan  = gridPerawat.grid_store_hasil.store;
    console.log(gridPerawat.grid_store_hasil);
    for (var i = 0; i < variable_keperawatan.data.length; i++) {
        var data        = {};
        data.check_diagnosa   = variable_keperawatan.data.items[i].data.CHECK_DIAGNOSA;
        data.check_intervensi = variable_keperawatan.data.items[i].data.CHECK_INTERVENSI;
        data.evaluasi         = variable_keperawatan.data.items[i].data.EVALUASI;
        data.id_diagnosa      = variable_keperawatan.data.items[i].data.ID_DIAGNOSA;
        data.id_intervensi    = variable_keperawatan.data.items[i].data.ID_INTERVENSI;
        data.id_group         = variable_keperawatan.data.items[i].data.ID_GROUP;
        data.paraf            = variable_keperawatan.data.items[i].data.PARAF;
        data.group            = variable_keperawatan.data.items[i].data.group;

        keperawatan.push(data);
    }
	 */
  if (PenataJasaRJ.dsGridObat.getCount() > 0) {
    var line = 0;
    for (var i = 0, iLen = params.jmlObat; i < iLen; i++) {
      var o = PenataJasaRJ.dsGridObat.getRange()[i].data;
      if (o.racikan == false) {
        var data = {};
        data.kd_prd = o.kd_prd;
        data.jumlah = o.jumlah;
        data.signa = o.signa;
        data.cara_pakai = o.cara_pakai;
        data.verified = o.verified;
        data.takaran = o.takaran;
        data.kd_dokter = Ext.get("txtKdDokter").getValue();
        data.urut = o.urut;
        data.aturan_pakai = "";
        data.aturan_racik = "";
        data.kd_unit_far = o.kd_unit_far;
        data.kd_milik = o.kd_milik;
        data.no_racik = 0;
        data.racikan = 0;
        data.jumlah_racik = 0;
        data.satuan_racik = "";
        data.catatan_racik = "";
        data.order_mng = o.order_mng;
        data.id_mrresep = o.id_mrresep;

        // params['kd_prd'+line]         = o.kd_prd;
        // params['jumlah'+line]         = o.jumlah;
        // params['signa'+line]          = o.signa;
        // params['cara_pakai'+line]     = o.cara_pakai;
        // params['verified'+line]       = o.verified;
        // params['takaran'+line]        = o.takaran;
        // params['kd_dokter'+line]      = Ext.get('txtKdDokter').getValue();
        // params['urut' + line]         = o.urut;
        // params['aturan_pakai' + line] = '';
        // params['aturan_racik' + line] = '';
        // params['kd_unit_far' + line]  = o.kd_unit_far;
        // params['kd_milik' + line]     = o.kd_milik;
        // params['no_racik' + line]     = 0;
        // params['racikan' +line]       = 0;
        // params['jumlah_racik' +line]  = 0;
        // params['satuan_racik' +line]  ='';
        // params['catatan_racik' +line] ='';
        list_obat.push(data);
        line++;
      } else {
        console.log(o);
        var parse = JSON.parse(o.result);
        params.jmlObat = params.jmlObat - 1 + parse.length;
        for (var j = 0, jLen = parse.length; j < jLen; j++) {
          var k = parse[j];
          var data = {};
          data.kd_prd = k.kd_prd;
          data.jumlah = k.jumlah;
          data.signa = o.signa;
          data.cara_pakai = o.cara_pakai;
          data.verified = o.verified;
          data.takaran = o.takaran;
          data.kd_dokter = Ext.get("txtKdDokter").getValue();
          data.urut = k.urut;
          data.aturan_pakai = o.aturan_pakai;
          data.aturan_racik = o.aturan_racik;
          data.kd_unit_far = k.kd_unit_far;
          data.kd_milik = k.kd_milik;
          data.no_racik = o.kd_prd;
          data.racikan = 1;
          data.jumlah_racik = o.jumlah;
          data.satuan_racik = o.satuan;
          data.catatan_racik = k.catatan_racik;
          data.order_mng = o.order_mng;
          data.id_mrresep = o.id_mrresep;

          // params['kd_prd'+line]         = k.kd_prd;
          // params['jumlah'+line]         = k.jumlah;
          // params['signa'+line]          = o.signa;
          // params['cara_pakai'+line]     = o.cara_pakai;
          // params['verified'+line]       = o.verified;
          // params['takaran'+line]        = o.takaran;
          // params['kd_dokter'+line]      = Ext.get('txtKdDokter').getValue();
          // params['urut' + line]         = k.urut;
          // params['aturan_pakai' + line] = o.aturan_pakai;
          // params['aturan_racik' + line] = o.aturan_racik;
          // params['kd_unit_far' + line]  = k.kd_unit_far;
          // params['kd_milik' + line]     = k.kd_milik;
          // params['no_racik' + line]     = o.kd_prd;
          // params['racikan' +line]       = 1;
          // params['jumlah_racik' +line]  = o.jumlah;
          // params['satuan_racik' +line]  = o.satuan;
          // params['catatan_racik' +line] = k.catatan_racik;
          list_obat.push(data);
          line++;
        }
      }
    }
    params["list_obat"] = JSON.stringify(list_obat);
    params["resep"] = true;
  } else {
    params["list_obat"] = null;
    params["resep"] = false;
  }
  return params;
}

function getParamDetailTransaksiRWJ_(kondisi) {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    kondisinya: kondisi,
    kdDokter: Ext.get("txtKdDokter").getValue(),
    Tgl: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
    Shift: tampungshiftsekarang,
    List: getArrDetailTrRWJ(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: GetListCountDetailTransaksi(),
    Hapus: 1,
    Ubah: 0,
  };
  params.jmlObat = PenataJasaRJ.dsGridObat.getRange().length;
  params.urut_masuk = PenataJasaRJ.s1.data.URUT_MASUK;
  params.kd_pasien = PenataJasaRJ.s1.data.KD_PASIEN;
  console.log(PenataJasaRJ.dsGridObat);
  if (PenataJasaRJ.dsGridObat.getCount() > 0) {
    var line = 0;
    for (var i = 0, iLen = params.jmlObat; i < iLen; i++) {
      var o = PenataJasaRJ.dsGridObat.getRange()[i].data;
      if (o.racikan == false) {
        params["kd_prd" + line] = o.kd_prd;
        params["jumlah" + line] = o.jumlah;
        params["signa" + line] = o.signa;
        params["cara_pakai" + line] = o.cara_pakai;
        params["verified" + line] = o.verified;
        params["takaran" + line] = o.takaran;
        params["kd_dokter" + line] = Ext.get("txtKdDokter").getValue();
        params["urut" + line] = o.urut;
        params["aturan_pakai" + line] = "";
        params["aturan_racik" + line] = "";
        params["kd_unit_far" + line] = o.kd_unit_far;
        params["kd_milik" + line] = o.kd_milik;
        params["no_racik" + line] = 0;
        params["racikan" + line] = 0;
        params["jumlah_racik" + line] = 0;
        params["satuan_racik" + line] = "";
        params["catatan_racik" + line] = "";
        line++;
      } else {
        console.log(o);
        var parse = JSON.parse(o.result);
        params.jmlObat = params.jmlObat - 1 + parse.length;
        for (var j = 0, jLen = parse.length; j < jLen; j++) {
          var k = parse[j];
          params["kd_prd" + line] = k.kd_prd;
          params["jumlah" + line] = k.jumlah;
          params["signa" + line] = o.signa;
          params["cara_pakai" + line] = o.cara_pakai;
          params["verified" + line] = o.verified;
          params["takaran" + line] = o.takaran;
          params["kd_dokter" + line] = Ext.get("txtKdDokter").getValue();
          params["urut" + line] = k.urut;
          params["aturan_pakai" + line] = o.aturan_pakai;
          params["aturan_racik" + line] = o.aturan_racik;
          params["kd_unit_far" + line] = k.kd_unit_far;
          params["kd_milik" + line] = k.kd_milik;
          params["no_racik" + line] = o.kd_prd;
          params["racikan" + line] = 1;
          params["jumlah_racik" + line] = o.jumlah;
          params["satuan_racik" + line] = o.satuan;
          params["catatan_racik" + line] = k.catatan_racik;
          line++;
        }
      }
    }
    params["resep"] = true;
  } else {
    params["resep"] = false;
  }
  return params;
}

function getParamDetailAnamnese() {
  var params = {
    Table: "viewkondisifisik",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    UrutMasuk: Ext.get("txtKdUrutMasuk").getValue(),
    Anamnese: Ext.get("txtareaAnamnesis").getValue(),
    Catatan: Ext.get("txtareaAnamnesiscatatan").getValue(),
    Tgl: Ext.get("dtpTanggalDetransaksi").dom.value,
    List: getArrdetailAnamnese(),
  };
  return params;
}

function getParamDetailPemeriksaanMata() {
  var params = {
    Table: "viewpemeriksaanmata",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    UrutMasuk: Ext.get("txtKdUrutMasuk").getValue(),
    TestButaWarna: Ext.get("txtareaEyeCarecatatan").getValue(),
    Tgl: Ext.get("dtpTanggalDetransaksi").dom.value,
    List: getArrdetailPemeriksaanMata(),
  };
  console.log(params);
  return params;
}

function getParamDetailPemeriksaanGigi() {
  var params = {
    Table: "viewpemeriksaangigi",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    UrutMasuk: Ext.get("txtKdUrutMasuk").getValue(),
    Tgl: Ext.get("dtpTanggalDetransaksi").dom.value,
    ListAtas: getArrdetailPemeriksaanGigiAtas(),
    ListBawah: getArrdetailPemeriksaanGigiBawah(),
  };
  console.log(params);
  return params;
}

function getParamKonsultasi() {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnitAsal: Ext.get("txtKdUnitRWJ").getValue(),
    KdDokterAsal: Ext.get("txtKdDokter").getValue(),
    KdUnit: selectKlinikPoli,
    KdDokter: selectDokter,
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TglTransaksi: Ext.get("dtpTanggalDetransaksi").dom.value,
    KDCustomer: vkode_customer,
    UrutMasuk: Ext.getCmp("txtKdUrutMasuk").getValue(),
  };
  return params;
}

function GetListCountDetailTransaksi() {
  var x = 0;
  for (var i = 0; i < dsTRDetailKasirRWJList.getCount(); i++) {
    if (
      dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != "" ||
      dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI != ""
    ) {
      x += 1;
    }
  }
  return x;
}

function getTotalDetailProduk() {
  var TotalProduk = 0;
  var x = "";
  for (var i = 0; i < dsTRDetailKasirRWJList.getCount(); i++) {
    var recordterakhir;
    var y = "";
    var z = "@@##$$@@";
    recordterakhir = dsTRDetailKasirRWJList.data.items[i].data.DESC_REQ;
    TotalProduk = TotalProduk + recordterakhir;
    Ext.get("txtJumlah1EditData_viKasirRwj").dom.value =
      formatCurrency(TotalProduk);
    if (i === dsTRDetailKasirRWJList.getCount() - 1) {
      x += y;
    } else {
      x += y + "##[[]]##";
    }
  }
  return x;
}

function getArrDetailTrRWJ() {
  var x = "";
  for (var i = 0; i < PenataJasaRJ.dsGridTindakan.getCount(); i++) {
    if (
      PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK != "" &&
      PenataJasaRJ.dsGridTindakan.data.items[i].data.DESKRIPSI != ""
    ) {
      var urut = "x";
      if (PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT > 0) {
        urut = PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
      }
      var y = "";
      var z = "@@##$$@@";
      y = "URUT=" + PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.QTY;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.TGL_BERLAKU;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.HARGA;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_TARIF;
      y += z + urut;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.TGL_TRANSAKSI;
      y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.STATUS_KONSULTASI;
      // if (i === (PenataJasaRJ.dsGridTindakan.getCount()-1)){
      // 	x += y ;
      // }else{
      x += y + "##[[]]##";
      // }
    }
  }
  return x;
}

function getItemPanelInputRWJ(lebar) {
  var items = {
    layout: "fit",
    labelAlign: "right",
    bodyStyle: "padding:4px;",
    border: true,
    height: 136,
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          getItemPanelNoTransksiRWJ(lebar),
          getItemPanelmedrec(lebar),
          getItemPanelUnit(lebar),
          getItemPanelDokter(lebar),
        ],
      },
    ],
  };
  return items;
}

function getItemPanelUnit(lebar) {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.4,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Tanggal Lahir  ",
            name: "txtTglLahirRWJ",
            id: "txtTglLahirRWJ",
            readOnly: true,
            hidden: false,
            anchor: "99%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Unit  ",
            name: "txtKdUnitRWJ",
            id: "txtKdUnitRWJ",
            readOnly: true,
            hidden: true,
            anchor: "99%",
          },
        ],
      },
      {
        /* columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 1, */
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 55,
        items: [
          {
            xtype: "textfield",
            name: "txtNamaUnit",
            id: "txtNamaUnit",
            readOnly: true,
            hidden: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Unit  ",
            name: "txtUnit",
            id: "txtUnit",
            readOnly: true,
            anchor: "100%",
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 120,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Jam Diperiksa",
            name: "txtJam_diperiksa",
            id: "txtJam_diperiksa",
            readOnly: true,
            anchor: "100%",
          },
        ],
      },
    ],
  };
  return items;
}

function getItemPanelDokter(lebar) {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.4,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Usia Pasien",
            readOnly: true,
            name: "txtUsia",
            id: "txtUsia",
            anchor: "99%",
            listeners: {},
          },
          {
            xtype: "textfield",
            fieldLabel: "Kelompok Pasien",
            readOnly: true,
            name: "txtCustomer",
            id: "txtCustomer",
            anchor: "99%",
            listeners: {},
          },
          {
            xtype: "textfield",
            fieldLabel: "KD Customer",
            readOnly: true,
            hidden: true,
            name: "txtKDCustomer",
            id: "txtKDCustomer",
            anchor: "99%",
            listeners: {},
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 55,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Dokter  ",
            name: "txtKdDokter",
            id: "txtKdDokter",
            readOnly: true,
            anchor: "100%",
          },
        ]
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 2,
        items: [
          {
            xtype: "textfield",
            name: "txtNamaDokter",
            id: "txtNamaDokter",
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            name: "txtKdUrutMasuk",
            id: "txtKdUrutMasuk",
            readOnly: true,
            hidden: true,
            anchor: "100%",
          },
        ],
      },
    ],
  };
  return items;
}

function getItemPanelNoTransksiRWJ(lebar) {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.4,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "No. Transaksi ",
            name: "txtNoTransaksiKasirrwj",
            id: "txtNoTransaksiKasirrwj",
            emptyText: nmNomorOtomatis,
            readOnly: true,
            anchor: "99%",
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 55,
        items: [
          {
            xtype: "datefield",
            fieldLabel: "Tanggal ",
            id: "dtpTanggalDetransaksi",
            name: "dtpTanggalDetransaksi",
            format: "d/M/Y",
            readOnly: true,
            value: now,
            anchor: "100%",
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 120,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Jadwal Dokter ",
            id: "dtpJadwal_Dokter",
            name: "dtpJadwal_Dokter",
            // format: 'd/M/Y',
            readOnly: true,
            anchor: "100%",
          },
        ],
      },
    ],
  };
  return items;
}

function getItemPanelfilterData() {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.23,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [mComboJumlahShowviKasirRwj()],
      },
      {
        columnWidth: 0.2,
        layout: "form",
        border: false,
        labelWidth: 80,
        items: [
          {
            xtype: "button",
            text: "Refresh",
            iconCls: "refresh",
            width: 70,
            style: { "margin-left": "0px", "margin-top": "0px" },
            hideLabel: true,
            id: "btnRefreshPoliRWJ",
            handler: function () {
              RefreshDataFilterKasirRWJ();
              getTotKunjunganRWJ();
            },
          },
          // mComboUnit_viKasirRwj()
        ],
      },
      {
        columnWidth: 0.27,
        layout: "form",
        border: false,
        labelWidth: 100,
        items: [mComboStatusAntrian_viKasirRwj()],
      },
    ],
  };
  return items;
}

function mComboJumlahShowviKasirRwj() {
  var cboStatus_viKasirRwj = new Ext.form.ComboBox({
    id: "cboJumlahShow",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "90%",
    emptyText: "",
    fieldLabel: "Jumlah data",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [5, "5"],
        [10, "10"],
        [25, "25"],
        [50, "50"],
        [100, "100"],
        [150, "150"],
        [200, "200"],
      ],
    }),
    valueField: "Id",
    displayField: "displayText",
    value: 5,
    listeners: {
      select: function (a, b, c) {
        RefreshDataFilterKasirRWJ();
        // console.log(b.displayText);
      },
    },
  });
  return cboStatus_viKasirRwj;
}

function getItemPaneltgl_filter() {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.35,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "datefield",
            fieldLabel: "Tanggal ",
            id: "dtpTglAwalFilterRWJ",
            name: "dtpTglAwalFilterRWJ",
            value: now,
            anchor: "99%",
            format: "d/M/Y",
            altFormats: "dmy",
            listeners: {
              specialkey: function () {
                var tmpNoMedrec = Ext.get("txtFilterNomedrec").getValue();
                if (
                  Ext.EventObject.getKey() === 13 ||
                  Ext.EventObject.getKey() === 9
                ) {
                  RefreshDataFilterKasirRWJ();
                }
              },
            },
          },
        ],
      },
      { xtype: "tbtext", text: " s/d", cls: "left-label", width: 30 },
      {
        columnWidth: 0.35,
        layout: "form",
        border: false,
        labelWidth: 1,
        items: [
          {
            xtype: "datefield",
            id: "dtpTglAkhirFilterRWJ",
            name: "dtpTglAkhirFilterRWJ",
            format: "d/M/Y",
            value: now,
            anchor: "99%",
            listeners: {
              specialkey: function () {
                var tmpNoMedrec = Ext.get("txtFilterNomedrec").getValue();
                if (
                  Ext.EventObject.getKey() === 13 ||
                  Ext.EventObject.getKey() === 9 ||
                  tmpNoMedrec.length === 10
                ) {
                  RefreshDataFilterKasirRWJ();
                }
              },
            },
          },
        ],
      },
    ],
  };
  return items;
}
function getItemcombo_filter() {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.23,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [mComboStatusBayar_viKasirRwj()],
      },
      {
        columnWidth: 0.26,
        layout: "form",
        border: false,
        labelWidth: 80,
        items: [mComboUnit_viKasirRwj()],
      },
      {
        columnWidth: 0.21,
        layout: "form",
        border: false,
        labelWidth: 100,
        items: [mComboStatusPeriksa_viKasirRwj()],
      },
    ],
  };
  return items;
}

function getItemPanelmedrec(lebar) {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.4,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "No. Medrec",
            name: "txtNoMedrecDetransaksi",
            id: "txtNoMedrecDetransaksi",
            readOnly: true,
            anchor: "99%",
            listeners: {},
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 2,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "",
            readOnly: true,
            name: "txtNamaPasienDetransaksi",
            id: "txtNamaPasienDetransaksi",
            anchor: "100%",
            listeners: {},
          },
        ],
      },
      {
        columnWidth: 0.3,
        layout: "form",
        border: false,
        labelWidth: 120,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Jam Datang ke Poli",
            readOnly: true,
            name: "txtJam_datang",
            id: "txtJam_datang",
            anchor: "100%",
            listeners: {},
          },
        ],
      },
    ],
  };
  return items;
}

function RefreshDataKasirRWJ() {
  dsTRKasirRWJList.load({
    params: {
      Skip: 0,
      Take: selectCountKasirRWJ,
      Sort: "tgl_transaksi",
      Sortdir: "ASC",
      target: "ViewTrKasirRwj",
      param: "",
    },
  });
  rowSelectedKasirRWJ = undefined;
  return dsTRKasirRWJList;
}

function refeshkasirrwj() {
  dsTRKasirRWJList.load({
    params: {
      Skip: 0,
      Take: selectCountKasirRWJ,
      Sort: "",
      Sortdir: "ASC",
      target: "ViewTrKasirRwj",
      param: "",
    },
  });
  return dsTRKasirRWJList;
}
function refreshDataDepanPenjasRWJ() {
  var KataKunci = "";
  if (Ext.get("txtFilterNomedrec").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and   LOWER(kd_pasien) like  LOWER( ~" +
        Ext.get("txtFilterNomedrec").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and  LOWER(kd_pasien) like  LOWER( ~" +
        Ext.get("txtFilterNomedrec").getValue() +
        "%~)";
    }
  }
  if (Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and   LOWER(nama) like  LOWER( ~" +
        Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and  LOWER(nama) like  LOWER( ~" +
        Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (
    Ext.get("cboUNIT_viKasirRwj").getValue() != "" &&
    Ext.get("cboUNIT_viKasirRwj").getValue() != "All"
  ) {
    if (KataKunci == "") {
      KataKunci =
        " and  LOWER(nama_unit)like  LOWER(~" +
        Ext.get("cboUNIT_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and LOWER(nama_unit) like  LOWER(~" +
        Ext.get("cboUNIT_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and  LOWER(nama_dokter) like  LOWER(~" +
        Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and LOWER(nama_dokter) like  LOWER(~" +
        Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Posting") {
    if (KataKunci == "") {
      KataKunci = " and  posting_transaksi = TRUE";
    } else {
      KataKunci += " and posting_transaksi =  TRUE";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Belum Posting") {
    if (KataKunci == "") {
      KataKunci = " and  posting_transaksi = FALSE";
    } else {
      KataKunci += " and posting_transaksi =  FALSE";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Semua") {
    if (KataKunci == "") {
      KataKunci =
        " and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )";
    } else {
      KataKunci +=
        " and (posting_transaksi = FALSE OR posting_transaksi = TRUE )";
    }
  }
  if (Ext.get("dtpTglAwalFilterRWJ").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and (tgl_transaksi between '" +
        Ext.get("dtpTglAwalFilterRWJ").getValue() +
        "' and '" +
        Ext.get("dtpTglAkhirFilterRWJ").getValue() +
        "') ";
    } else {
      KataKunci +=
        " and (tgl_transaksi between '" +
        Ext.get("dtpTglAwalFilterRWJ").getValue() +
        "' and '" +
        Ext.get("dtpTglAkhirFilterRWJ").getValue() +
        "') ";
    }
  }
  if (Ext.get("cboStatusPeriksa_viKasirRwj").getValue() != "") {
    var statperiksa;
    if (Ext.get("cboStatusPeriksa_viKasirRwj").getValue() == "Semua") {
      statperiksa = "";
    } else if (
      Ext.get("cboStatusPeriksa_viKasirRwj").getValue() == "Sudah Periksa"
    ) {
      statperiksa = " and status_periksa = 1";
    } else {
      statperiksa = "and status_periksa <> 1";
    }

    if (KataKunci == "") {
      KataKunci = statperiksa;
    } else {
      KataKunci += statperiksa;
    }
  }
  /*Ext.Ajax.request({
			url: baseURL + "index.php/main/functionRWJ/refreshdatagridpenatajasaRWJ",					
			params: {KataKunciNya:KataKunci},
			method:'POST',
			failure: function(o){
				//ShowPesanWarningRWJ('Load data gagal', 'Gagal');
				//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					dsTRKasirRWJList.removeAll();
					var recs=[],
						recType=dsTRKasirRWJList.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRKasirRWJList.add(recs);
					
					grListTRRWJ.getView().refresh();
					//refeshkasirrwj();
				}else {
					ShowPesanWarningRWJ('Load data gagal', 'Gagal');
				}
			}
		});*/
}
function RefreshDataFilterKasirRWJ() {
  var KataKunci = "";
  if (Ext.get("txtFilterNomedrec").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and   LOWER(kd_pasien) like  LOWER( ~" +
        Ext.get("txtFilterNomedrec").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and  LOWER(kd_pasien) like  LOWER( ~" +
        Ext.get("txtFilterNomedrec").getValue() +
        "%~)";
    }
  }
  if (Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and   LOWER(nama) like  LOWER( ~" +
        Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and  LOWER(nama) like  LOWER( ~" +
        Ext.get("TxtFilterGridDataView_NAMA_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (
    Ext.get("cboUNIT_viKasirRwj").getValue() != "" &&
    Ext.get("cboUNIT_viKasirRwj").getValue() != "All"
  ) {
    if (KataKunci == "") {
      KataKunci =
        " and  LOWER(nama_unit)like  LOWER(~" +
        Ext.get("cboUNIT_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and LOWER(nama_unit) like  LOWER(~" +
        Ext.get("cboUNIT_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and  LOWER(nama_dokter) like  LOWER(~" +
        Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() +
        "%~)";
    } else {
      KataKunci +=
        " and LOWER(nama_dokter) like  LOWER(~" +
        Ext.get("TxtFilterGridDataView_DOKTER_viKasirRwj").getValue() +
        "%~)";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Posting") {
    if (KataKunci == "") {
      KataKunci = " and  posting_transaksi = 1";
    } else {
      KataKunci += " and posting_transaksi =  1";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Belum Posting") {
    if (KataKunci == "") {
      KataKunci = " and  posting_transaksi = 0";
    } else {
      KataKunci += " and posting_transaksi =  0";
    }
  }
  if (Ext.get("cboStatus_viKasirRwj").getValue() == "Semua") {
    if (KataKunci == "") {
      KataKunci = " and  (posting_transaksi = 0 OR posting_transaksi = 1 )";
    } else {
      KataKunci += " and (posting_transaksi = 0 OR posting_transaksi = 1 )";
    }
  }
  if (Ext.get("dtpTglAwalFilterRWJ").getValue() != "") {
    if (KataKunci == "") {
      KataKunci =
        " and (tgl_transaksi between ~" +
        Ext.get("dtpTglAwalFilterRWJ").getValue() +
        "~ and ~" +
        Ext.get("dtpTglAkhirFilterRWJ").getValue() +
        "~)";
    } else {
      KataKunci +=
        " and (tgl_transaksi between ~" +
        Ext.get("dtpTglAwalFilterRWJ").getValue() +
        "~ and ~" +
        Ext.get("dtpTglAkhirFilterRWJ").getValue() +
        "~)";
    }
  }
  if (Ext.get("cboStatusPeriksa_viKasirRwj").getValue() != "") {
    var statperiksa;
    if (Ext.get("cboStatusPeriksa_viKasirRwj").getValue() == "Semua") {
      statperiksa = "";
    } else if (
      Ext.get("cboStatusPeriksa_viKasirRwj").getValue() == "Sudah Periksa"
    ) {
      //statperiksa = " and status_periksa = 1";
      statperiksa = " and resdata.ap_status_antrian in(2,3)";
    } else {
      //statperiksa = "and status_periksa <> 1";
      //statperiksa = "and status_periksa <> 1";
      statperiksa = " and resdata.ap_status_antrian in(0,1)";
    }

    if (KataKunci == "") {
      KataKunci = statperiksa;
    } else {
      KataKunci += statperiksa;
    }
  }

  /*[0, 'Semua'],
				[1, 'Tunggu Berkas'],
				[2, 'Tunggu Panggilan'],
				[3, 'Sedang Dilayani'],
				[4, 'Selesai'],*/
  if (
    Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() != "Semua" ||
    Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() != 0
  ) {
    console.log(setting_tracer);
    if (setting_tracer == "f") {
      if (
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() ==
          "Tunggu Berkas" ||
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() == 1
      ) {
        KataKunci += "and ap_status_antrian = '0'";
      } else if (
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() ==
          "Tunggu Panggilan" ||
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() == 2
      ) {
        KataKunci += "and ap_status_antrian = '1'";
      } else if (
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() ==
          "Sedang Dilayani" ||
        Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue() == 3
      ) {
        KataKunci += "and ap_status_antrian = '2'";
      }
    } else {
      // KataKunci += " and ap_status_antrian = '0'";
    }
  } else {
    KataKunci += " and ap_status_antrian = '0'";
  }
  // KataKunci += "limit "+Ext.getCmp("cboJumlahShow").getValue();
  if (KataKunci != undefined || KataKunci != "") {
    dsTRKasirRWJList.load({
      params: {
        Skip: Ext.getCmp("cboJumlahShow").getValue(),
        Take: selectCountKasirRWJ,
        Sort: "tgl_transaksi",
        Sortdir: "ASC",
        target: "ViewTrKasirRwj",
        param: KataKunci,
        // limit:
      },
    });
    //getTotKunjunganRWJ();
  } else {
    // getTotKunjunganRWJ();
    dsTRKasirRWJList.load({
      params: {
        Skip: Ext.getCmp("cboJumlahShow").getValue(),
        Take: selectCountKasirRWJ,
        Sort: "tgl_transaksi",
        Sortdir: "ASC",
        target: "ViewTrKasirRwj",
        param: "",
      },
    });
  }
  getTotKunjunganRWJ();
  return dsTRKasirRWJList;
}

function Datasave_Konsultasi(mBol, callback) {
  if (ValidasiEntryKonsultasi(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/main/functionRWJ/KonsultasiPenataJasa",
      params: getParamKonsultasi(),
      failure: function (o) {
        ShowPesanWarningRWJ("Konsultasi ulang gagal", "Gagal");
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      },
      success: function (o) {
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          if (callback != undefined) callback(cst.id);
          ShowPesanInfoRWJ("Konsultasi berhasil disimpan", "Information");
          //Datasave_Konsultasi_SQL(cst.notrans)
          if (mBol === false) {
            RefreshDataKasirRWJDetail(
              Ext.get("txtNoTransaksiKasirrwj").dom.value
            );
          }
          //refeshkasirrwj();
        } else {
          ShowPesanWarningRWJ("Konsultasi ulang gagal", "Gagal");
        }
      },
    });
  } else {
    if (mBol === true) {
      return false;
    }
  }
}

function Datasave_KasirRWJ(mBol, jasa, kondisi) {
  if (ValidasiEntryCMRWJ(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan/Rawat_jalan/save",
      // url			: baseURL + "index.php/main/functionRWJ/savedetailpenyakit",
      params: getParamDetailTransaksiRWJ(kondisi),
      failure: function (o) {
        ShowPesanWarningRWJ(
          "Data Tidak berhasil disimpan hubungi admin",
          "Gagal"
        );
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      },
      success: function (o) {
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          ShowPesanInfoRWJ("Data berhasil disimpan", "Information");
          //RefreshDataFilterKasirRWJ();
          updateStatusPeriksa();
          //Datasave_KasirRWJ_SQL(cst.id_mrresep,jasa,kondisi);
          PenataJasaRJ.var_id_mrresep = cst.id_mrresep;

          var paramresep = {
            KD_PASIEN: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
            KD_UNIT: Ext.getCmp("txtKdUnitRWJ").getValue(),
            TANGGAL_TRANSAKSI: Ext.get("dtpTanggalDetransaksi").getValue(),
          };
          RefreshDataKasirRWJDetai2(paramresep);

          if (mBol === false) {
            RefreshDataKasirRWJDetail(
              Ext.get("txtNoTransaksiKasirrwj").dom.value
            );
          }

          //refreshDataDepanPenjasRWJ();

          if (jasa == true) {
            SimpanJasaDokterPenindak(
              currentJasaDokterKdProduk_RWJ,
              currentJasaDokterKdTarif_RWJ,
              currentJasaDokterUrutDetailTransaksi_RWJ,
              currentJasaDokterHargaJP_RWJ
            );
          }
        } else {
          ShowPesanWarningRWJ(
            "Data Tidak berhasil disimpan hubungi admin",
            "Gagal"
          );
        }
      },
    });
  } else {
    if (mBol === true) {
      return false;
    }
  }
}
function getParamok() {
  var jamOp =
    Ext.getCmp("txtJam_viJdwlOperasi").getValue() +
    ":" +
    Ext.getCmp("txtMenit_viJdwlOperasi").getValue() +
    ":00";
  var params = {
    TrKodeTranskasi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    kdDokter: Ext.getCmp("txtKdDokter").getValue(),
    jam_op: jamOp,

    tindakan: Ext.getCmp("cbo_viComboJenisTindakan_viJdwlOperasi").getValue(),
    kamar: Ext.getCmp("cbo_viComboKamar_viJdwlOperasi").getValue(),
    tgl_operasi: Ext.getCmp("TglOperasi_viJdwlOperasi").getValue(),
    kasir: "rawat jalan",
  };
  return params;
}

function Datasave_ok(mBol) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/kamar_operasi/functionKamarOperasi/save_order_mng",
    params: getParamok(),
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan Hubungi admin",
        "Gagal"
      );
      RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
    },
    success: function (o) {
      RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoRWJ("Jadwal berhasil dibuat", "Information");
        //RefreshDataFilterKasirRWJ();
        var paramresep = {
          KD_PASIEN: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
          KD_UNIT: Ext.getCmp("txtKdUnitRWJ").getValue(),
          TANGGAL_TRANSAKSI: Ext.get("dtpTanggalDetransaksi").getValue(),
        };
        RefreshDataKasirRWJDetai2(paramresep);
        DataSaveKamarOperasi_SQL(mBol);
        if (mBol === false) {
          RefreshDataKasirRWJDetail(
            Ext.get("txtNoTransaksiKasirrwj").dom.value
          );
        }
      } else if (cst.success === false && cst.cari === true) {
        ShowPesanWarningRWJ(
          "Pasien telah terjadwal di Kamar operasi ",
          "Gagal"
        );
      } else {
        ShowPesanWarningRWJ(
          "Data Tidak berhasil disimpan hubungi admin",
          "Gagal"
        );
      }
    },
  });
}

function Dataupdate_KasirRWJ(mBol) {
  if (ValidasiEntryCMRWJ(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/main/CreateDataObj",
      params: getParamDataupdateKasirRWJDetail(),
      success: function (o) {
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          ShowPesanInfoRWJ("Data berhasil disimpan", "Information");
          RefreshDataKasirRWJ();
          if (mBol === false) {
            RefreshDataKasirRWJDetail(
              Ext.get("txtNoTransaksiKasirrwj").dom.value
            );
          }
        } else if (cst.success === false && cst.pesan === 0) {
          ShowPesanWarningRWJ(nmPesanSimpanGagal, nmHeaderSimpanData);
        } else if (cst.success === false && cst.pesan === 1) {
          ShowPesanWarningRWJ(
            nmPesanSimpanGagal + " , This request had been approved / rejected",
            nmHeaderSimpanData
          );
        } else {
          ShowPesanErrorRWJ(nmPesanSimpanError, nmHeaderSimpanData);
        }
      },
    });
  } else {
    if (mBol === true) {
      return false;
    }
  }
}

function ValidasiEntryCMRWJ(modul, mBolHapus) {
  var x = 1;
  if (
    Ext.get("txtNoTransaksiKasirrwj").getValue() == "" ||
    Ext.get("txtNoMedrecDetransaksi").getValue() == "" ||
    Ext.get("txtNamaPasienDetransaksi").getValue() == "" ||
    Ext.get("txtNamaDokter").getValue() == "" ||
    Ext.get("dtpTanggalDetransaksi").getValue() == "" ||
    dsTRDetailKasirRWJList.getCount() === 0 ||
    Ext.get("txtKdDokter").dom.value === undefined
  ) {
    if (
      Ext.get("txtNoTransaksiKasirrwj").getValue() == "" &&
      mBolHapus === true
    ) {
      x = 0;
    } else if (Ext.get("txtNoMedrecDetransaksi").getValue() == "") {
      ShowPesanWarningRWJ(nmGetValidasiKosong("No. Medrec"), modul);
      x = 0;
    } else if (Ext.get("txtNamaPasienDetransaksi").getValue() == "") {
      ShowPesanWarningRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
      x = 0;
    } else if (Ext.get("dtpTanggalDetransaksi").getValue() == "") {
      ShowPesanWarningRWJ(nmGetValidasiKosong("Tanggal Kunjungan"), modul);
      x = 0;
    } else if (
      Ext.get("txtNamaDokter").getValue() == "" ||
      Ext.get("txtKdDokter").dom.value === undefined
    ) {
      ShowPesanWarningRWJ(nmGetValidasiKosong(nmDeptRequest), modul);
      x = 0;
    } else if (dsTRDetailKasirRWJList.getCount() === 0) {
      ShowPesanWarningRWJ(nmGetValidasiKosong(nmTitleDetailFormRequest), modul);
      x = 0;
    }
  }
  return x;
}

function ValidasiEntryKonsultasi(modul, mBolHapus) {
  var x = 1;
  if (
    Ext.get("cboPoliklinikRequestEntry").getValue() == "" ||
    Ext.get("cboDokterRequestEntry").getValue() == ""
  ) {
    if (
      Ext.get("cboPoliklinikRequestEntry").getValue() == "" &&
      mBolHapus === true
    ) {
      x = 0;
    } else if (Ext.get("cboDokterRequestEntry").getValue() == "") {
      ShowPesanWarningRWJ(nmGetValidasiKosong("No. Medrec"), modul);
      x = 0;
    }
  }
  return x;
}

function ShowPesanWarningRWJ(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.WARNING,
    width: 250,
  });
}

PenataJasaRJ.alertError = function (str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.ERROR,
    width: 250,
  });
};

function ShowPesanErrorRWJ(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.ERROR,
    width: 250,
  });
}

function ShowPesanInfoRWJ(str, modul) {
  Ext.MessageBox.show({
    title: modul,
    msg: str,
    buttons: Ext.MessageBox.OK,
    icon: Ext.MessageBox.INFO,
    width: 250,
  });
}

function DataDeleteKasirRWJ() {
  if (ValidasiEntryCMRWJ(nmHeaderHapusData, true) == 1) {
    Ext.Msg.show({
      title: nmHeaderHapusData,
      msg: nmGetValidasiHapus(nmTitleFormRequest),
      buttons: Ext.MessageBox.YESNO,
      width: 275,
      fn: function (btn) {
        if (btn == "yes") {
          Ext.Ajax.request({
            url: baseURL + "index.php/main/DeleteDataObj",
            params: getParamDetailTransaksiRWJ(),
            success: function (o) {
              var cst = Ext.decode(o.responseText);
              if (cst.success === true) {
                ShowPesanInfoRWJ("Data berhasil dihapus", "Information");
                RefreshDataKasirRWJ();
                RWJAddNew();
              } else if (cst.success === false && cst.pesan === 0) {
                ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
              } else if (cst.success === false && cst.pesan === 1) {
                ShowPesanWarningRWJ(
                  nmPesanHapusGagal + " , ",
                  nmHeaderHapusData
                );
              } else {
                ShowPesanErrorRWJ(nmPesanHapusError, nmHeaderHapusData);
              }
            },
          });
        }
      },
    });
  }
}

function GantiDokterLookUp(mod_id) {
  var FormDepanDokter = new Ext.Panel({
    id: mod_id,
    closable: true,
    region: "center",
    layout: "form",
    title: "Ganti Dokter",
    border: false,
    shadhow: true,
    autoScroll: false,
    iconCls: "Request",
    margins: "0 5 5 0",
    items: [DokterLookUp()],
    listeners: {
      afterrender: function () {},
    },
  });
  return FormDepanDokter;
}

function SuratKeteraganSakitLookUp(format) {
  FormSuratKeteranganSakitLookUp(format);
  /*var FormSuratKeteranganSakit = new Ext.Panel({
        id: "form_"+format,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Surat Keterangan Sakit',
        border: false,
        shadhow: true,
        autoScroll:false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
        	FormSuratKeteranganSakitLookUp(format)
        ],
        listeners:{
            'afterrender': function()
            {}
        }
    });
   	return FormSuratKeteranganSakit;*/
}

function DokterLookUp(rowdata_AG) {
  var lebar = 350;
  FormLookUpGantidokter = new Ext.Window({
    id: "gridDokter",
    title: "Ganti Dokter",
    closeAction: "destroy",
    width: lebar,
    height: 180,
    border: false,
    resizable: false,
    plain: true,
    layout: "fit",
    constrain: true,
    iconCls: "Request",
    modal: true,
    items: getFormEntryDokter(lebar),
    listeners: {
      activate: function () {
        if (varBtnOkLookupEmp === true) {
          Ext.get("txtKdDokter").dom.value =
            rowSelectedLookdokter.data.KD_DOKTER;
          Ext.get("txtNamaDokter").dom.value =
            rowSelectedLookdokter.data.NAMA_DOKTER;
          varBtnOkLookupEmp = false;
        }
      },
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {
        rowSelectedKasirDokter = undefined;
        // RefreshDataFilterKasirDokter();
      },
    },
  });
  FormLookUpGantidokter.show();
}

function FormSuratKeteranganSakitLookUp(format) {
  // DISINI
  var obj_hide_1 = false;
  var obj_hide_2 = true;
  var title_ = "Surat Keterangan Sakit";
  if (format == "SKB") {
    obj_hide_1 = true;
    obj_hide_2 = false;
    title_ = "Cetak Surat Keterangan Berbadan Sehat";
  }
  var lebar = 550;

  FormLookUpKeteranganSakit = new Ext.Window({
    id: "gridDokter",
    title: title_,
    closeAction: "destroy",
    width: lebar,
    border: false,
    resizable: true,
    // plain: true,
    layout: {
      type: "fit",
      align: "center",
    },
    bodyStyle: "padding:5px;",
    constrain: true,
    iconCls: "Request",
    modal: true,
    // items: getFormKeteranganSakit(lebar, format),
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Nomer Dokumen",
            name: "txtNomer",
            id: "txtNomer",
            value: "",
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Nama",
            name: "txtNamaPasien",
            id: "txtNamaPasien",
            value: rowSelectedKasirRWJ.data.NAMA,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Jenis Kelamin",
            name: "txtKelamin",
            id: "txtKelamin",
            value: rowSelectedKasirRWJ.json.JENIS_KELAMIN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Umur",
            name: "txtUmur",
            id: "txtUmur",
            value:
              rowSelectedKasirRWJ.json.UMUR.YEAR +
              " thn, " +
              rowSelectedKasirRWJ.json.UMUR.MONTH +
              " bln, " +
              rowSelectedKasirRWJ.json.UMUR.DAY +
              " hari",
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "No Rekam Medis",
            name: "txtMedrec",
            id: "txtMedrec",
            value: rowSelectedKasirRWJ.data.KD_PASIEN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Pekerjaan",
            name: "txtPekerjaan",
            id: "txtPekerjaan",
            value: rowSelectedKasirRWJ.data.PEKERJAAN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Alamat",
            name: "txtAlamat",
            id: "txtAlamat",
            value: rowSelectedKasirRWJ.data.ALAMAT,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Lama Istirahat",
            name: "txtIstirahat",
            id: "txtIstirahat",
            placeholder: "per hari",
            hidden: obj_hide_1,
            // value: rowSelectedKasirRWJ.data.ALAMAT,
            // readOnly:true,
            anchor: "100%",
            enableKeyEvents: true,
            listeners: {
              keyup: function () {
                var date = new Date();
                var tanggal = new Date(
                  Ext.getCmp("txtFirstPeriode").getValue()
                );
                tanggal.setTime(
                  tanggal.getTime() +
                    Ext.getCmp("txtIstirahat").getValue() * 24 * 60 * 60 * 1000
                );
                Ext.getCmp("txtLastPeriode").setValue(tanggal.format("Y-m-d"));
                // console.log(tanggal.format('Y-m-d'));
              },
            },
          },
          {
            xtype: "datefield",
            format: "Y-m-d",
            fieldLabel: "Dari tanggal",
            name: "txtFirstPeriode",
            id: "txtFirstPeriode",
            anchor: "100%",
            value: now,
            hidden: obj_hide_1,
            enableKeyEvents: true,
            listeners: {
              keyup: function () {
                var date = new Date();
                var tanggal = new Date(
                  Ext.getCmp("txtFirstPeriode").getValue()
                );
                tanggal.setTime(
                  tanggal.getTime() +
                    Ext.getCmp("txtIstirahat").getValue() * 24 * 60 * 60 * 1000
                );
                Ext.getCmp("txtLastPeriode").setValue(tanggal.format("Y-m-d"));
                // console.log(tanggal.format('Y-m-d'));
              },
            },
          },
          {
            xtype: "datefield",
            format: "Y-m-d",
            fieldLabel: "Sampai tanggal",
            name: "txtLastPeriode",
            id: "txtLastPeriode",
            anchor: "100%",
            hidden: obj_hide_1,
          },
          {
            xtype: "textfield",
            fieldLabel: "Berat Badan",
            name: "txtBeratBadan",
            id: "txtBeratBadan",
            anchor: "100%",
            hidden: obj_hide_2,
          },
          {
            xtype: "textfield",
            fieldLabel: "Tinggi Badan",
            name: "txtTinggiBadan",
            id: "txtTinggiBadan",
            anchor: "100%",
            hidden: obj_hide_2,
          },
          {
            xtype: "textfield",
            fieldLabel: "Tensi",
            name: "txtTensiPasien",
            id: "txtTensiPasien",
            anchor: "100%",
            hidden: obj_hide_2,
          },
          {
            xtype: "textarea",
            fieldLabel: "Pemeriksaan dalam keadaan",
            name: "txtKeadaanPasien",
            id: "txtKeadaanPasien",
            anchor: "100%",
            hidden: obj_hide_2,
            rows: 25,
          },
          {
            xtype: "textarea",
            fieldLabel: "Surat dapat dipergunakan untuk",
            name: "txtDipergunakanUntuk",
            id: "txtDipergunakanUntuk",
            anchor: "100%",
            rows: 25,
            hidden: obj_hide_2,
          },
        ],
      },
    ],
    fbar: [
      {
        xtype: "button",
        text: "Print",
        iconCls: "print",
        handler: function () {
          var params = {
            nomer_document: Ext.getCmp("txtNomer").getValue(),
            kd_pasien: Ext.getCmp("txtMedrec").getValue(),
            lama_inap: Ext.getCmp("txtIstirahat").getValue(),
            first_date: Ext.getCmp("txtFirstPeriode").getValue(),
            last_date: Ext.getCmp("txtLastPeriode").getValue(),
            kddokter: Ext.getCmp("txtKdDokter").getValue(),
            dokter: Ext.getCmp("txtNamaDokter").getValue(),
            kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
            berat_badan: Ext.getCmp("txtBeratBadan").getValue(),
            tinggi_badan: Ext.getCmp("txtTinggiBadan").getValue(),
            tensi_badan: Ext.getCmp("txtTensiPasien").getValue(),
            pemeriksaan: Ext.getCmp("txtKeadaanPasien").getValue(),
            dipergunakan: Ext.getCmp("txtDipergunakanUntuk").getValue(),
            tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
            urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
          };
          var form = document.createElement("form");
          form.setAttribute("method", "post");
          form.setAttribute("target", "_blank");
          if (format == "SKB") {
            form.setAttribute(
              "action",
              baseURL + "index.php/general/surat_keterangan_sehat/cetak"
            );
          } else {
            form.setAttribute(
              "action",
              baseURL + "index.php/general/surat_keterangan_sakit/cetak"
            );
          }
          // form.setAttribute("action", baseURL + "index.php/main/functionRWJ/cetakLab");
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", "data");
          hiddenField.setAttribute("value", Ext.encode(params));
          form.appendChild(hiddenField);
          document.body.appendChild(form);
          form.submit();
          FormLookUpKeteranganSakit.close();
        },
      },
      {
        xtype: "button",
        text: "Keluar",
        handler: function () {
          FormLookUpKeteranganSakit.close();
        },
      },
    ],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {
        // rowSelectedKasirDokter=undefined;
        // RefreshDataFilterKasirDokter();
      },
    },
  });
  Ext.Ajax.request({
    url: baseURL + "index.php/general/surat_keterangan_sakit/get_counter_map",
    params: {
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      format: format,
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      tanggal = new Date(now);
      month = convert_number_to_roman(parseInt(tanggal.getMonth() + 1));
      var year = tanggal.format("Y");
      Ext.getCmp("txtNomer").setValue(
        cst.format + "/" + format + "/" + month + "/RSSI/" + year
      );
    },
  });

  var link_surat = "";
  if (format == "SKB") {
    link_surat = baseURL + "index.php/general/surat_keterangan_sehat/get_data";
  } else {
    link_surat = baseURL + "index.php/general/surat_keterangan_sakit/get_data";
  }
  Ext.Ajax.request({
    url: link_surat,
    params: {
      kd_pasien: rowSelectedKasirRWJ.data.KD_PASIEN,
      kd_unit: rowSelectedKasirRWJ.data.KD_UNIT,
      tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
      format: format,
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status === true) {
        if (cst.data[0].NO_DOKUMENT != "") {
          Ext.getCmp("txtNomer").setValue(cst.data[0].NO_DOKUMENT);
        }

        if (format == "SKB") {
          if (cst.data[0].TINGGI_BADAN != "") {
            Ext.getCmp("txtTinggiBadan").setValue(cst.data[0].TINGGI_BADAN);
          }

          if (cst.data[0].BERAT_BADAN != "") {
            Ext.getCmp("txtBeratBadan").setValue(cst.data[0].BERAT_BADAN);
          }

          if (cst.data[0].TENSI != "") {
            Ext.getCmp("txtTensiPasien").setValue(cst.data[0].TENSI);
          }

          if (cst.data[0].PEMERIKSAAN != "") {
            Ext.getCmp("txtKeadaanPasien").setValue(cst.data[0].PEMERIKSAAN);
          }

          if (cst.data[0].DIPERGUNAKAN != "") {
            Ext.getCmp("txtDipergunakanUntuk").setValue(
              cst.data[0].DIPERGUNAKAN
            );
          }
        } else {
          if (cst.data[0].LAMA != "") {
            Ext.getCmp("txtIstirahat").setValue(cst.data[0].LAMA);
            var date = new Date();
            var tanggal = new Date(Ext.getCmp("txtFirstPeriode").getValue());
            tanggal.setTime(
              tanggal.getTime() +
                Ext.getCmp("txtIstirahat").getValue() * 24 * 60 * 60 * 1000
            );
            Ext.getCmp("txtLastPeriode").setValue(tanggal.format("Y-m-d"));
          }
        }
      }
    },
  });
  FormLookUpKeteranganSakit.show();
}

function FormSuratPengantarRawatInap() {
  // DISINI
  var obj_hide_1 = false;
  var obj_hide_2 = true;
  var title_ = "Surat Pengantar Rawat Inap";
  obj_hide_1 = true;
  obj_hide_2 = false;
  title_ = "Cetak Surat Pengantar Rawat Inap";
  var lebar = 550;

  FormLookUpPengantarRawatInap = new Ext.Window({
    id: "gridDokter",
    title: title_,
    closeAction: "destroy",
    width: lebar,
    border: false,
    resizable: true,
    // plain: true,
    layout: {
      type: "fit",
      align: "center",
    },
    bodyStyle: "padding:5px;",
    constrain: true,
    iconCls: "Request",
    modal: true,
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Kepada Yth.",
            name: "txtYangTerHormat",
            id: "txtYangTerHormat",
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Nama",
            name: "txtNamaPasien",
            id: "txtNamaPasien",
            value: rowSelectedKasirRWJ.data.NAMA,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Jenis Kelamin",
            name: "txtKelamin",
            id: "txtKelamin",
            value: rowSelectedKasirRWJ.json.JENIS_KELAMIN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Umur",
            name: "txtUmur",
            id: "txtUmur",
            value:
              rowSelectedKasirRWJ.json.UMUR.YEAR +
              " thn, " +
              rowSelectedKasirRWJ.json.UMUR.MONTH +
              " bln, " +
              rowSelectedKasirRWJ.json.UMUR.DAY +
              " hari",
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "No Rekam Medis",
            name: "txtMedrec",
            id: "txtMedrec",
            value: rowSelectedKasirRWJ.data.KD_PASIEN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Pekerjaan",
            name: "txtPekerjaan",
            id: "txtPekerjaan",
            value: rowSelectedKasirRWJ.data.PEKERJAAN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Alamat",
            name: "txtAlamat",
            id: "txtAlamat",
            value: rowSelectedKasirRWJ.data.ALAMAT,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textarea",
            fieldLabel: "Diet",
            name: "txtDiet",
            id: "txtDiet",
            anchor: "100%",
            rows: 25,
          },
        ],
      },
    ],
    fbar: [
      {
        xtype: "button",
        text: "Print",
        iconCls: "print",
        handler: function () {
          if (Ext.getCmp("txtYangTerHormat").getValue() == '') {
            ShowPesanWarningDiagnosa("Field Kepada Yth. Harap Diisi.", "Warning");
          } else {
            var params = {
            yth: Ext.getCmp("txtYangTerHormat").getValue(),
            kd_pasien: Ext.getCmp("txtMedrec").getValue(),
            kddokter: Ext.getCmp("txtKdDokter").getValue(),
            dokter: Ext.getCmp("txtNamaDokter").getValue(),
            kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
            diet: Ext.getCmp("txtDiet").getValue(),
            tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
            urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
          };
          var form = document.createElement("form");
          form.setAttribute("method", "post");
          form.setAttribute("target", "_blank");
            form.setAttribute(
              "action",
              baseURL + "index.php/general/surat_pengantar_rawat_inap/cetak"
            );
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", "data");
          hiddenField.setAttribute("value", Ext.encode(params));
          form.appendChild(hiddenField);
          document.body.appendChild(form);
          form.submit();
          FormLookUpPengantarRawatInap.close();
          }
        },
      },
      {
        xtype: "button",
        text: "Keluar",
        handler: function () {
          FormLookUpPengantarRawatInap.close();
        },
      },
    ],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {
        // rowSelectedKasirDokter=undefined;
        // RefreshDataFilterKasirDokter();
      },
    },
  });

  var link_surat = "";
    link_surat = baseURL + "index.php/general/surat_pengantar_rawat_inap/get_data";
  
  Ext.Ajax.request({
    url: link_surat,
    params: {
      kd_pasien: rowSelectedKasirRWJ.data.KD_PASIEN,
      kd_unit: rowSelectedKasirRWJ.data.KD_UNIT,
      tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status === true) {
        if (cst.data[0].YTH != "") {
          Ext.getCmp("txtYangTerHormat").setValue(cst.data[0].YTH);
        }
        if (cst.data[0].DIET != "") {
          Ext.getCmp("txtDiet").setValue(cst.data[0].DIET);
        }
      }
    },
  });
  FormLookUpPengantarRawatInap.show();
}

function FormSuratRujukan() {
  // DISINI
  var obj_hide_1 = false;
  var obj_hide_2 = true;
  var title_ = "Surat Rujukan";
  var lebar = 550;
  var initialHeight = 345;

  FormLookUpRujukan = new Ext.Window({
    id: "gridDokter",
    title: title_,
    closeAction: "destroy",
    width: lebar,
    height: initialHeight,
    border: false,
    resizable: true,
    // plain: true,
    layout: {
      type: "fit",
      align: "center",
    },
    bodyStyle: "padding:5px;",
    constrain: true,
    iconCls: "Request",
    modal: true,
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Kepada Yth.",
            name: "txtYangTerHormatRujukan",
            id: "txtYangTerHormatRujukan",
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Nama",
            name: "txtNamaPasienRujukan",
            id: "txtNamaPasienRujukan",
            value: rowSelectedKasirRWJ.data.NAMA,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Jenis Kelamin",
            name: "txtKelaminRujukan",
            id: "txtKelaminRujukan",
            value: rowSelectedKasirRWJ.json.JENIS_KELAMIN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Umur",
            name: "txtUmurRujukan",
            id: "txtUmurRujukan",
            value:
              rowSelectedKasirRWJ.json.UMUR.YEAR +
              " thn, " +
              rowSelectedKasirRWJ.json.UMUR.MONTH +
              " bln, " +
              rowSelectedKasirRWJ.json.UMUR.DAY +
              " hari",
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "No Rekam Medis",
            name: "txtMedrecRujukan",
            id: "txtMedrecRujukan",
            value: rowSelectedKasirRWJ.data.KD_PASIEN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Pekerjaan",
            name: "txtPekerjaanRujukan",
            id: "txtPekerjaanRujukan",
            value: rowSelectedKasirRWJ.data.PEKERJAAN,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Alamat",
            name: "txtAlamatRujukan",
            id: "txtAlamatRujukan",
            value: rowSelectedKasirRWJ.data.ALAMAT,
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "datefield",
            format: "Y-m-d",
            fieldLabel: "telah kami Rawat Dari tanggal",
            name: "txtdaritanggal",
            id: "txtdaritanggal",
            anchor: "100%",
            value: now
          },
          {
            xtype: "datefield",
            format: "Y-m-d",
            fieldLabel: "Sampai tanggal",
            name: "txtsampaitanggal",
            id: "txtsampaitanggal",
            anchor: "100%",
            value: now
          },
            mComboDiMohon(),
            mComboPoliKontrol(),
          {
            xtype: "datefield",
            format: "Y-m-d",
            fieldLabel: "Pada Tanggal",
            name: "txtPadaTanggal",
            id: "txtPadaTanggal",
            anchor: "100%",
            hidden: true
          },
        ],
      },
    ],
    fbar: [
      {
        xtype: "button",
        text: "Print",
        iconCls: "print",
        handler: function () {
          if (Ext.getCmp("txtYangTerHormatRujukan").getValue() == '') {
            ShowPesanWarningDiagnosa("Field Kepada Yth. Harap Diisi.", "Warning");
          } else {
            var params = {
            yth: Ext.getCmp("txtYangTerHormatRujukan").getValue(),
            kd_pasien: Ext.getCmp("txtMedrecRujukan").getValue(),
            kddokter: Ext.getCmp("txtKdDokter").getValue(),
            dokter: Ext.getCmp("txtNamaDokter").getValue(),
            kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
            tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
            urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
            dari: Ext.getCmp("txtdaritanggal").getValue(),
            sampai: Ext.getCmp("txtsampaitanggal").getValue(),
            mohon: Ext.getCmp("cboDiMohon").getValue(),
            kontrolke: Ext.getCmp("cboPoliklinikKontrolEntry").getValue(),
            kembalipada: Ext.getCmp("txtPadaTanggal").getValue()
          };
          var form = document.createElement("form");
          form.setAttribute("method", "post");
          form.setAttribute("target", "_blank");
            form.setAttribute(
              "action",
              baseURL + "index.php/general/surat_rujukan/cetak"
            );
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", "data");
          hiddenField.setAttribute("value", Ext.encode(params));
          form.appendChild(hiddenField);
          document.body.appendChild(form);
          form.submit();
          FormLookUpRujukan.close();
          }
        },
      },
      {
        xtype: "button",
        text: "Keluar",
        handler: function () {
          FormLookUpRujukan.close();
        },
      },
    ],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {
        // rowSelectedKasirDokter=undefined;
        // RefreshDataFilterKasirDokter();
      },
    },
  });

  var link_surat = "";
    link_surat = baseURL + "index.php/general/surat_rujukan/get_data";
  
  Ext.Ajax.request({
    url: link_surat,
    params: {
      kd_pasien: rowSelectedKasirRWJ.data.KD_PASIEN,
      kd_unit: rowSelectedKasirRWJ.data.KD_UNIT,
      tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      console.log(cst);
      if (cst.status === true) {
        var newHeight = 345;
        var additionalHeight = 75;
        if (cst.data[0].YTH != "") {
          Ext.getCmp("txtYangTerHormatRujukan").setValue(cst.data[0].YTH);
        }
        if (cst.data[0].DARI != "") {
          var darii = cst.data[0].DARI;
          var dariii = darii.substring(0, 10);
          Ext.getCmp("txtdaritanggal").setValue(dariii);
        }
        if (cst.data[0].SAMPAI != "") {
          var sampaii = cst.data[0].SAMPAI;
          var sampaiii = sampaii.substring(0, 10);
          Ext.getCmp("txtsampaitanggal").setValue(sampaiii);
        }
        if (cst.data[0].MOHON != "") {
          selectCountDiMohon = cst.data[0].MOHON;
          Ext.getCmp("cboDiMohon").setValue(cst.data[0].MOHON);
          if (selectCountDiMohon == 2) {
            console.log('muncul');
            Ext.getCmp('cboPoliklinikKontrolEntry').show();
            Ext.getCmp('txtPadaTanggal').show();
            newHeight += additionalHeight; // additionalHeight should be the height added when components are shown
          } else {
            console.log('ilang');
            Ext.getCmp('cboPoliklinikKontrolEntry').hide();
            Ext.getCmp('txtPadaTanggal').hide();
            newHeight = newHeight;
          }
          if (cst.data[0].MOHON == "2") {
            polipilihanpasien = cst.data[0].KONTROLKE;
            var kemb = cst.data[0].KEMBALIPADA;
            var kembalipada = kemb.substring(0, 10);
            Ext.getCmp("cboPoliklinikKontrolEntry").setValue(cst.data[0].KONTROLKE);
            Ext.getCmp("txtPadaTanggal").setValue(kembalipada);
          }
        }
      }
    },
  });
  FormLookUpRujukan.show();
}

function mComboPoliKontrol() {
	var cboPoliklinikKontrolEntry = new Ext.form.ComboBox({
		id: 'cboPoliklinikKontrolEntry',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		hidden: true,
		forceSelection: true,
		emptyText: 'Pilih Poliklinik...',
		fieldLabel: 'Poliklinik ',
		align: 'Right',
		enableKeyEvents: true,
		store: dsPilihUnit,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		anchor: '100%',
		tabIndex: 29,
		listeners: {
			select: function (a, b, c) {
				polipilihanpasien = b.data.KD_UNIT;
			},
		}
	});
	return cboPoliklinikKontrolEntry;
}

function mComboDiMohon() {
  var cboDiMohon = new Ext.form.ComboBox({
    id: "cboDiMohon",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "99.9%",
    emptyText: "",
    fieldLabel: "Mohon untuk",
    store: new Ext.data.ArrayStore({
      id: 0,
      fields: ["Id", "displayText"],
      data: [
        [1, "Perawatan dan pengobatan/ tindakan selanjutnya"],
        [2, "Kontrol Kembali"],
        [3, "Observasi"],
      ],
    }),
    valueField: "Id",
    displayField: "displayText",
    value: 1,
    listeners: {
      select: function (a, b, c) {
        selectCountDiMohon = b.data.Id;
        console.log(selectCountDiMohon);
        var newHeight = 345; // initialHeight should be the default height of the window
        var additionalHeight = 75;
        if (selectCountDiMohon == 2) {
          console.log('muncul');
          Ext.getCmp('cboPoliklinikKontrolEntry').show();
          Ext.getCmp('txtPadaTanggal').show();
          newHeight += additionalHeight; // additionalHeight should be the height added when components are shown
        } else {
          console.log('ilang');
          Ext.getCmp('cboPoliklinikKontrolEntry').hide();
          Ext.getCmp('txtPadaTanggal').hide();
          newHeight = newHeight;
        }

        FormLookUpRujukan.setHeight(newHeight);
        // RefreshDataFilterKasirRWJ();
      },
    },
  });
  return cboDiMohon;
}

function convert_number_to_roman(input) {
  if (input < 1 || input > 3999) {
    return "Invalid Roman Number Value";
  } else {
    var s = "";
    while (input >= 1000) {
      s += "M";
      input -= 1000;
    }
    while (input >= 900) {
      s += "CM";
      input -= 900;
    }
    while (input >= 500) {
      s += "D";
      input -= 500;
    }
    while (input >= 400) {
      s += "CD";
      input -= 400;
    }
    while (input >= 100) {
      s += "C";
      input -= 100;
    }
    while (input >= 90) {
      s += "XC";
      input -= 90;
    }
    while (input >= 50) {
      s += "L";
      input -= 50;
    }
    while (input >= 40) {
      s += "XL";
      input -= 40;
    }
    while (input >= 10) {
      s += "X";
      input -= 10;
    }
    while (input >= 9) {
      s += "IX";
      input -= 9;
    }
    while (input >= 5) {
      s += "V";
      input -= 5;
    }
    while (input >= 4) {
      s += "IV";
      input -= 4;
    }
    while (input >= 1) {
      s += "I";
      input -= 1;
    }
    return s;
  }
}

function getItemPanelButtonGantidokter(lebar) {
  var items = {
    layout: "column",
    border: false,
    height: 39,
    anchor: "100%",
    style: { "margin-top": "-1px" },
    items: [
      {
        layout: "hBox",
        width: 310,
        border: false,
        bodyStyle: "padding:5px 0px 5px 5px",
        defaults: { margins: "3 3 3 3" },
        anchor: "90%",
        layoutConfig: {
          align: "middle",
          pack: "end",
        },
        items: [
          {
            xtype: "button",
            text: "Simpan",
            width: 100,
            style: { "margin-left": "0px", "margin-top": "0px" },
            hideLabel: true,
            id: "btnOkGantiDokter",
            handler: function () {
              GantiDokter(false);
              //FormLookUpGantidokter.close();
            },
          },
          {
            xtype: "button",
            text: "Tutup",
            width: 70,
            hideLabel: true,
            id: "btnCancelGantidokter",
            handler: function () {
              FormLookUpGantidokter.close();
            },
          },
        ],
      },
    ],
  };
  return items;
}

function getFormEntryDokter(lebar) {
  var pnlTRGantiDokter = new Ext.FormPanel({
    id: "PanelTRDokter",
    fileUpload: true,
    region: "north",
    layout: "column",
    bodyStyle: "padding:10px 10px 10px 10px",
    height: 190,
    anchor: "100%",
    width: lebar,
    border: false,
    items: [
      getItemPanelInputGantidokter(lebar),
      getItemPanelButtonGantidokter(lebar),
    ],
    tbar: [],
  });
  var FormDepanDokter = new Ext.Panel({
    id: "FormDepanDokter",
    region: "center",
    width: "100%",
    anchor: "100%",
    layout: "form",
    title: "",
    bodyStyle: "padding:15px",
    border: true,
    bodyStyle: "background:#FFFFFF;",
    shadhow: true,
    items: [pnlTRGantiDokter],
  });
  return FormDepanDokter;
}

function getFormKeteranganSakit(lebar, format) {
  var obj_hide_1 = false;
  var obj_hide_2 = true;
  if (format == "SKB") {
    obj_hide_1 = true;
    obj_hide_2 = false;
  }
  var pnlTRGantiDokter = new Ext.FormPanel({
    id: "PanelTRDokter",
    fileUpload: true,
    region: "north",
    layout: "column",
    bodyStyle: "padding:10px 10px 10px 10px",
    height: 500,
    anchor: "100%",
    width: lebar,
    border: false,
    items: [
      {
        layout: "hbox",
        anchor: "100%",
        width: lebar - 35,
        labelAlign: "right",
        bodyStyle: "padding:10px 10px 10px 0px",
        border: false,
        height: 580,
        items: [
          {
            columnWidth: 0.9,
            width: lebar - 35,
            labelWidth: 100,
            layout: "form",
            border: false,
            items: [
              {
                layout: "column",
                border: false,
                items: [
                  {
                    columnWidth: 1.0,
                    layout: "form",
                    labelWidth: 100,
                    border: false,
                    items: [
                      {
                        xtype: "textfield",
                        fieldLabel: "Nomer Dokumen",
                        name: "txtNomer",
                        id: "txtNomer",
                        value: "",
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Nama",
                        name: "txtNamaPasien",
                        id: "txtNamaPasien",
                        value: rowSelectedKasirRWJ.data.NAMA,
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Jenis Kelamin",
                        name: "txtKelamin",
                        id: "txtKelamin",
                        value: rowSelectedKasirRWJ.json.JENIS_KELAMIN,
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Umur",
                        name: "txtUmur",
                        id: "txtUmur",
                        value:
                          rowSelectedKasirRWJ.json.UMUR.YEAR +
                          " thn, " +
                          rowSelectedKasirRWJ.json.UMUR.MONTH +
                          " bln, " +
                          rowSelectedKasirRWJ.json.UMUR.DAY +
                          " hari",
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "No Rekam Medis",
                        name: "txtMedrec",
                        id: "txtMedrec",
                        value: rowSelectedKasirRWJ.data.KD_PASIEN,
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Pekerjaan",
                        name: "txtPekerjaan",
                        id: "txtPekerjaan",
                        value: rowSelectedKasirRWJ.data.PEKERJAAN,
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Alamat",
                        name: "txtAlamat",
                        id: "txtAlamat",
                        value: rowSelectedKasirRWJ.data.ALAMAT,
                        readOnly: true,
                        anchor: "100%",
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Lama Istirahat",
                        name: "txtIstirahat",
                        id: "txtIstirahat",
                        placeholder: "per hari",
                        hidden: obj_hide_1,
                        // value: rowSelectedKasirRWJ.data.ALAMAT,
                        // readOnly:true,
                        anchor: "100%",
                        enableKeyEvents: true,
                        listeners: {
                          keyup: function () {
                            var date = new Date();
                            var tanggal = new Date(
                              Ext.getCmp("txtFirstPeriode").getValue()
                            );
                            tanggal.setTime(
                              tanggal.getTime() +
                                Ext.getCmp("txtIstirahat").getValue() *
                                  24 *
                                  60 *
                                  60 *
                                  1000
                            );
                            Ext.getCmp("txtLastPeriode").setValue(
                              tanggal.format("Y-m-d")
                            );
                            // console.log(tanggal.format('Y-m-d'));
                          },
                        },
                      },
                      {
                        xtype: "datefield",
                        format: "Y-m-d",
                        fieldLabel: "Dari tanggal",
                        name: "txtFirstPeriode",
                        id: "txtFirstPeriode",
                        anchor: "100%",
                        value: now,
                        hidden: obj_hide_1,
                        enableKeyEvents: true,
                        listeners: {
                          keyup: function () {
                            var date = new Date();
                            var tanggal = new Date(
                              Ext.getCmp("txtFirstPeriode").getValue()
                            );
                            tanggal.setTime(
                              tanggal.getTime() +
                                Ext.getCmp("txtIstirahat").getValue() *
                                  24 *
                                  60 *
                                  60 *
                                  1000
                            );
                            Ext.getCmp("txtLastPeriode").setValue(
                              tanggal.format("Y-m-d")
                            );
                            // console.log(tanggal.format('Y-m-d'));
                          },
                        },
                      },
                      {
                        xtype: "datefield",
                        format: "Y-m-d",
                        fieldLabel: "Sampai tanggal",
                        name: "txtLastPeriode",
                        id: "txtLastPeriode",
                        anchor: "100%",
                        hidden: obj_hide_1,
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Berat Badan",
                        name: "txtBeratBadan",
                        id: "txtBeratBadan",
                        anchor: "100%",
                        hidden: obj_hide_2,
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Tinggi Badan",
                        name: "txtTinggiBadan",
                        id: "txtTinggiBadan",
                        anchor: "100%",
                        hidden: obj_hide_2,
                      },
                      {
                        xtype: "textfield",
                        fieldLabel: "Tensi",
                        name: "txtTensiPasien",
                        id: "txtTensiPasien",
                        anchor: "100%",
                        hidden: obj_hide_2,
                      },
                      {
                        xtype: "textarea",
                        fieldLabel: "Pemeriksaan dalam keadaan",
                        name: "txtKeadaanPasien",
                        id: "txtKeadaanPasien",
                        anchor: "100%",
                        hidden: obj_hide_2,
                        rows: 25,
                      },
                      {
                        xtype: "textarea",
                        fieldLabel: "Surat dapat dipergunakan untuk",
                        name: "txtDipergunakanUntuk",
                        id: "txtDipergunakanUntuk",
                        anchor: "100%",
                        rows: 25,
                        hidden: obj_hide_2,
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    tbar: [],
  });
  var FormDepanDokter = new Ext.Panel({
    id: "FormDepanDokter",
    region: "center",
    width: "100%",
    anchor: "100%",
    layout: "form",
    title: "",
    bodyStyle: "padding:15px",
    border: true,
    bodyStyle: "background:#FFFFFF;",
    shadhow: true,
    items: [pnlTRGantiDokter],
  });
  return FormDepanDokter;
}

function getItemPanelInputGantidokter(lebar) {
  var items = {
    layout: "fit",
    anchor: "100%",
    width: lebar - 35,
    labelAlign: "right",
    bodyStyle: "padding:10px 10px 10px 0px",
    border: false,
    height: 95,
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [getItemPanelNoTransksiDokter(lebar)],
      },
    ],
  };
  return items;
}

function ValidasiEntryTutupDokter(modul, mBolHapus) {
  var x = 1;
  if (
    Ext.get("txtNilaiDokter").getValue() == "" ||
    Ext.get("txtNilaiDokterSelanjutnya").getValue() == ""
  ) {
    if (Ext.get("txtNilaiDokter").getValue() == "" && mBolHapus === true) {
      x = 0;
    } else if (Ext.get("txtNilaiDokterSelanjutnya").getValue() === "") {
      x = 0;
      if (mBolHapus === false) {
        ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan), modul);
      }
    }
  }
  return x;
}

function getItemPanelNoTransksiDokter(lebar) {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 1.0,
        layout: "form",
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "textfield",
            fieldLabel: "Unit Asal ",
            name: "cmbUnitAsal",
            id: "cmbUnitAsal",
            value: Ext.get("txtNamaUnit").getValue(),
            readOnly: true,
            anchor: "100%",
          },
          {
            xtype: "textfield",
            fieldLabel: "Dokter Asal ",
            name: "cmbDokterAsal",
            id: "cmbDokterAsal",
            value: Ext.get("txtNamaDokter").getValue(),
            readOnly: true,
            anchor: "100%",
          },
          mComboDokterGantiEntry(),
        ],
      },
    ],
  };
  return items;
}

function mComboDokterGantiEntry() {
  var Field = ["KD_DOKTER", "NAMA"];
  dsDokterGantiEntry = new WebApp.DataStore({ fields: Field });
  var kDUnit = Ext.get("txtKdUnitRWJ").getValue();
  var kddokter = Ext.get("txtKdDokter").getValue();
  dsDokterGantiEntry.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "nama",
      Sortdir: "ASC",
      target: "ViewComboDokter",
      param:
        "where dk.kd_unit=~" +
        kDUnit +
        "~ and d.kd_dokter not in (~" +
        kddokter +
        "~)",
    },
  });
  var cboDokterGantiEntry = new Ext.form.ComboBox({
    id: "cboDokterRequestEntry",
    typeAhead: true,
    triggerAction: "all",
    name: "txtdokter",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih Dokter...",
    fieldLabel: "Dokter Baru",
    align: "Right",
    store: dsDokterGantiEntry,
    valueField: "KD_DOKTER",
    displayField: "NAMA",
    anchor: "100%",
    listeners: {
      select: function (a, b, c) {
        selectDokter = b.data.KD_DOKTER;
        NamaDokter = b.data.NAMA;
        Ext.get("txtKdDokter").dom.value = b.data.KD_DOKTER;
      },
      render: function (c) {
        c.getEl().on(
          "keypress",
          function (e) {
            if (e.getKey() == 13) Ext.getCmp("kelPasien").focus();
          },
          c
        );
      },
    },
  });
  return cboDokterGantiEntry;
}

function GantiDokter(mBol) {
  if (ValidasiGantiDokter(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: WebAppUrl.UrlUpdateData,
      params: getParamGantiDokter(),
      failure: function (o) {
        ShowPesanErrorRWJ("Ganti Dokter Gagal", "Ganti Dokter");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          RefreshDataFilterKasirRWJ();
          //GantiDokter_SQL();
          ShowPesanInfoRWJ("Berhasil ganti dokter", "Ganti Dokter");
          //refreshDataDepanPenjasRWJ();
          //refeshkasirrwj();
          Ext.get("txtKdDokter").dom.value = selectDokter;
          Ext.get("txtNamaDokter").dom.value = NamaDokter;
          FormLookUpGantidokter.close();
        } else if (cst.success === false && cst.pesan === 0) {
          ShowPesanErrorRWJ("Ganti Dokter Gagal", "Ganti Dokter");
        } else {
          ShowPesanErrorRWJ("Ganti Dokter Gagal", "Ganti Dokter");
        }
      },
    });
  } else {
    if (mBol === true) {
      return false;
    }
  }
}

function ValidasiGantiDokter(modul, mBolHapus) {
  var x = 1;
  if (Ext.get("cboDokterRequestEntry").getValue() == "") {
    if (Ext.get("cboDokterRequestEntry").getValue() === "") {
      x = 0;
      if (mBolHapus === false) {
        ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan), modul);
      }
    }
  }
  return x;
}

/*function getParamGantiDokter(){
    var params ={
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter : Ext.getCmp('cboDokterRequestEntry').getValue(),
		kodebagian : 2,
		urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		no_trans:Ext.getCmp('txtNoTransaksiKasirrwj').getValue()
	};
    return params;
};*/

function getParamGantiDokter() {
  var params = {
    Table: "ViewGantiDokter",
    TxtMedRec: rowSelectedKasirRWJ.data.KD_PASIEN,
    TxtTanggal: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
    KdUnit: rowSelectedKasirRWJ.data.KD_UNIT,
    KdDokter: Ext.getCmp("cboDokterRequestEntry").getValue(),
    kodebagian: 2,
    urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
    no_trans: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
  };
  return params;
}

function KelompokPasienLookUp(rowdata_AG) {
  var lebar = 440;
  FormLookUpsdetailTRKelompokPasien = new Ext.Window({
    id: "gridKelompokPasien",
    title: "Ganti Kelompok Pasien",
    closeAction: "destroy",
    width: lebar,
    height: 260,
    border: false,
    resizable: false,
    plain: false,
    constrain: true,
    layout: "fit",
    iconCls: "Request",
    modal: true,
    items: getFormEntryTRKelompokPasien(lebar),
    fbar: [
      {
        xtype: "button",
        text: "Simpan",
        width: 70,
        style: { "margin-left": "0px", "margin-top": "0px" },
        hideLabel: true,
        id: "btnOkKelompokPasien",
        handler: function () {
          Datasave_Kelompokpasien();
        },
      },
      {
        xtype: "button",
        text: "Tutup",
        width: 70,
        hideLabel: true,
        id: "btnCancelKelompokPasien",
        handler: function () {
          FormLookUpsdetailTRKelompokPasien.close();
        },
      },
    ],
    listeners: {},
  });
  FormLookUpsdetailTRKelompokPasien.show();
  KelompokPasienbaru();
}

function getFormEntryTRKelompokPasien(lebar) {
  var pnlTRKelompokPasien = new Ext.FormPanel({
    id: "PanelTRKelompokPasien",
    fileUpload: true,
    region: "north",
    layout: "column",
    bodyStyle: "padding:10px 10px 10px 10px",
    height: 250,
    anchor: "100%",
    width: lebar,
    border: false,
    items: [getItemPanelInputKelompokPasien(lebar)],
  });
  var FormDepanKelompokPasien = new Ext.Panel({
    id: "FormDepanKelompokPasien",
    region: "center",
    width: "100%",
    anchor: "100%",
    layout: "form",
    title: "",
    bodyStyle: "padding:15px",
    border: true,
    bodyStyle: "background:#FFFFFF;",
    shadhow: true,
    items: [pnlTRKelompokPasien],
  });
  return FormDepanKelompokPasien;
}

function getItemPanelInputKelompokPasien(lebar) {
  var items = {
    layout: "fit",
    anchor: "100%",
    width: lebar - 35,
    labelAlign: "right",
    bodyStyle: "padding:10px 10px 10px 0px",
    border: false,
    height: 170,
    items: [
      {
        columnWidth: 0.9,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          getKelompokpasienlama(lebar),
          getItemPanelNoTransksiKelompokPasien(lebar),
        ],
      },
    ],
  };
  return items;
}

function KelompokPasienbaru() {
  jeniscus = 0;
  KelompokPasienAddNew = true;
  Ext.getCmp("cboKelompokpasien").show();
  Ext.getCmp("txtNoSJP").disable();
  Ext.getCmp("txtNoAskes").disable();
  RefreshDatacombo(jeniscus);
  Ext.get("txtCustomerLama").dom.value = Ext.get("txtCustomer").dom.value;
}

function RefreshDatacombo(jeniscus) {
  ds_customer_viDaftar.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "",
      Sortdir: "",
      target: "ViewComboKontrakCustomer",
      param:
        "jenis_cust=~" +
        jeniscus +
        "~ and kontraktor.kd_customer not in(~" +
        vkode_customer +
        "~)",
    },
  });
  return ds_customer_viDaftar;
}

function mComboKelompokpasien() {
  var Field_poli_viDaftar = ["KD_CUSTOMER", "CUSTOMER"];
  ds_customer_viDaftar = new WebApp.DataStore({ fields: Field_poli_viDaftar });
  ds_customer_viDaftar.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "",
      Sortdir: "",
      target: "ViewComboKontrakCustomer",
      param: "jenis_cust=~" + jeniscus + "~",
    },
  });
  var cboKelompokpasien = new Ext.form.ComboBox({
    id: "cboKelompokpasien",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "Pilih...",
    fieldLabel: labelisi,
    align: "Right",
    anchor: "95%",
    store: ds_customer_viDaftar,
    valueField: "KD_CUSTOMER",
    displayField: "CUSTOMER",
    listeners: {
      select: function (a, b, c) {
        selectSetKelompokpasien = b.data.displayText;
        selectKdCustomer = b.data.KD_CUSTOMER;
        selectNamaCustomer = b.data.CUSTOMER;
      },
    },
  });
  return cboKelompokpasien;
}

function getKelompokpasienlama(lebar) {
  var items = {
    Width: lebar,
    height: 40,
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.99,
        layout: "form",
        Width: lebar - 10,
        labelWidth: 130,
        border: false,
        items: [
          {
            xtype: "tbspacer",
            height: 2,
          },
          {
            xtype: "textfield",
            fieldLabel: "Kelompok Pasien Asal",
            name: "txtCustomerLama",
            id: "txtCustomerLama",
            labelWidth: 130,
            width: 100,
            anchor: "95%",
          },
        ],
      },
    ],
  };
  return items;
}

function getItemPanelNoTransksiKelompokPasien(lebar) {
  var items = {
    Width: lebar,
    height: 120,
    layout: "column",
    border: false,
    items: [
      {
        columnWidth: 0.99,
        layout: "form",
        Width: lebar - 10,
        labelWidth: 130,
        border: false,
        items: [
          {
            xtype: "tbspacer",
            height: 3,
          },
          {
            xtype: "combo",
            fieldLabel: "Kelompok Pasien Baru",
            id: "kelPasien",
            editable: false,
            store: new Ext.data.ArrayStore({
              id: 0,
              fields: ["Id", "displayText"],
              data: [
                [1, "Perseorangan"],
                [2, "Perusahaan"],
                [3, "Asuransi"],
              ],
            }),
            displayField: "displayText",
            mode: "local",
            width: 100,
            triggerAction: "all",
            emptyText: "Pilih Salah Satu...",
            selectOnFocus: true,
            anchor: "95%",
            listeners: {
              select: function (a, b, c) {
                if (b.data.displayText == "Perseorangan") {
                  jeniscus = "0";
                  Ext.getCmp("txtNoSJP").disable();
                  Ext.getCmp("txtNoAskes").disable();
                } else if (b.data.displayText == "Perusahaan") {
                  jeniscus = "1";
                  Ext.getCmp("txtNoSJP").disable();
                  Ext.getCmp("txtNoAskes").disable();
                } else if (b.data.displayText == "Asuransi") {
                  jeniscus = "2";
                  Ext.getCmp("txtNoSJP").enable();
                  Ext.getCmp("txtNoAskes").enable();
                }
                RefreshDatacombo(jeniscus);
              },
            },
          },
          {
            columnWidth: 0.99,
            layout: "form",
            border: false,
            labelWidth: 130,
            items: [mComboKelompokpasien()],
          },
          {
            xtype: "textfield",
            fieldLabel: "No. SJP",
            maxLength: 200,
            name: "txtNoSJP",
            id: "txtNoSJP",
            width: 100,
            anchor: "95%",
          },
          {
            xtype: "textfield",
            fieldLabel: "No. Askes",
            maxLength: 200,
            name: "txtNoAskes",
            id: "txtNoAskes",
            width: 100,
            anchor: "95%",
          },
        ],
      },
    ],
  };
  return items;
}

function Datasave_Kelompokpasien(mBol) {
  if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/main/functionRWJ/UpdateKdCustomer",
      params: getParamKelompokpasien(),
      failure: function (o) {
        ShowPesanWarningRWJ("Simpan kelompok pasien gagal", "Gagal");
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      },
      success: function (o) {
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        Ext.get("txtCustomer").dom.value = selectNamaCustomer;
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          ShowPesanInfoRWJ("Ganti kelompok pasien berhasil", "Information");
          //Datasave_Kelompokpasien_SQL();
          //RefreshDataKasirRWJ();
          if (mBol === false) {
            RefreshDataKasirRWJDetail(
              Ext.get("txtNoTransaksiKasirrwj").dom.value
            );
          }
          FormLookUpsdetailTRKelompokPasien.close();
        } else {
          ShowPesanWarningRWJ("Simpan kelompok pasien gagal", "Gagal");
        }
      },
    });
  } else {
    if (mBol === true) {
      return false;
    }
  }
}

function getParamKelompokpasien() {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodePasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    KdDokter: Ext.get("txtKdDokter").dom.value,
    TglTransaksi: Ext.get("dtpTanggalDetransaksi").dom.value,
    KDCustomer: selectKdCustomer,
    TglTransaksi: Ext.get("dtpTanggalDetransaksi").dom.value,
    KDNoSJP: Ext.get("txtNoSJP").dom.value,
    KDNoAskes: Ext.get("txtNoAskes").dom.value,
    UrutMasuk: Ext.getCmp("txtKdUrutMasuk").getValue(),
  };
  return params;
}

function ValidasiEntryUpdateKelompokPasien(modul, mBolHapus) {
  var x = 1;
  if (
    Ext.get("kelPasien").getValue() == "" ||
    Ext.get("kelPasien").dom.value === undefined ||
    Ext.get("cboKelompokpasien").getValue() == "" ||
    Ext.get("cboKelompokpasien").dom.value === undefined
  ) {
    if (Ext.get("kelPasien").getValue() == "" && mBolHapus === true) {
      ShowPesanWarningRWJ(nmGetValidasiKosong("Kelompok pasien"), modul);
      x = 0;
    }
    if (Ext.get("cboKelompokpasien").getValue() == "" && mBolHapus === true) {
      ShowPesanWarningRWJ(nmGetValidasiKosong("Combo kelompok pasien"), modul);
      x = 0;
    }
  }
  return x;
}

function setpostingtransaksi(notransaksi) {
  Ext.Msg.show({
    title: "Posting",
    msg: "Kirim Data Transaksi ini Ke Kasir ? ",
    buttons: Ext.MessageBox.YESNO,
    width: 250,
    fn: function (btn) {
      if (btn === "yes") {
        Ext.Ajax.request({
          url: baseURL + "index.php/main/posting",
          params: {
            _notransaksi: notransaksi,
          },
          success: function (o) {
            var cst = Ext.decode(o.responseText);
            if (cst.success === true) {
              updateAntrianSedangDilayani(3);
              RefreshDataFilterKasirRWJ();
              ShowPesanInfoDiagnosa("Posting Berhasil Dilakukan", "Posting");
              FormLookUpsdetailTRrwj.close();
            } else if (cst.success === false && cst.pesan === 0) {
              ShowPesanWarningDiagnosa(nmPesanHapusGagal, "Posting");
            } else {
              ShowPesanWarningDiagnosa(nmPesanHapusError, "Posting");
            }
          },
        });
      }
    },
  });
}
// ================================================================================================================================================================================================
function setdisablebutton_PJ_RWJ() {
  Ext.getCmp("btnLookUpKonsultasi_viKasirIgd").disable();
  // Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
  Ext.getCmp("btngantipasien_rwj").disable();
  Ext.getCmp("btnposting_pj_rwj").disable();
  Ext.getCmp("btnLookupRWJ").disable();
  Ext.getCmp("btnSimpanRWJ_AG").disable();
  Ext.getCmp("btnHpsBrsRWJ_AG").disable();
  // Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').disable();
  // Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').disable();
  // Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').disable();

  Ext.getCmp("BtnTambahTindakanTrPenJasRWJ").disable();
  Ext.getCmp("BtnSimpanTindakanTrPenJasRWJ").disable();
  Ext.getCmp("btnHpsBrsIcd9_RWJ").disable();

  Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").disable();
  Ext.getCmp("btnsimpanPemeriksaanMata").disable();
  Ext.getCmp("btnsimpanPemeriksaanGigi").disable();

  Ext.getCmp("BtnTambahObatTrKasirRwj").disable();
  Ext.getCmp("BtnHapusObatTrKasirRwj").disable();

  Ext.getCmp("btnLookUpEditDokterPenindak_RWJ").disable();
  Ext.getCmp("btnbarisRad_PJ_RWJ").disable();
  Ext.getCmp("btnSimpanRad_PJ_RWJ").disable();
  Ext.getCmp("btnHpsBrsRad_PJ_RWJ").disable();

  Ext.getCmp("RJPJBtnAddLab_PJ_RWJ").disable();
  Ext.getCmp("RJPJBtnSaveLab_PJ_RWJ").disable();
  Ext.getCmp("RJPJBtnDelLab_PJ_RWJ").disable();
  Ext.getCmp("btnsimpanop_PJ_RWJ").disable();
}

function setenablebutton_PJ_RWJ() {
  Ext.getCmp("btnLookUpKonsultasi_viKasirIgd").enable();
  // Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
  Ext.getCmp("btngantipasien_rwj").enable();
  Ext.getCmp("btnposting_pj_rwj").enable();
  //if(Ext.getCmp('btnLookupRWJ').enable != undefined){
  //	Ext.getCmp('btnLookupRWJ').enable();
  //}

  Ext.getCmp("btnSimpanRWJ_AG").enable();
  Ext.getCmp("btnHpsBrsRWJ_AG").enable();
  Ext.getCmp("btnHpsBrsDiagnosa_PJ_RWJ").enable();
  Ext.getCmp("btnSimpanDiagnosa_PJ_RWJ").enable();
  Ext.getCmp("btnLookupDiagnosa_PJ_RWJ").enable();

  Ext.getCmp("BtnTambahTindakanTrPenJasRWJ").enable();
  Ext.getCmp("BtnSimpanTindakanTrPenJasRWJ").enable();
  Ext.getCmp("btnHpsBrsIcd9_RWJ").enable();

  Ext.getCmp("btnsimpanAnamnese_PJ_RWJ").enable();
  Ext.getCmp("btnsimpanPemeriksaanMata").enable();
  Ext.getCmp("btnsimpanPemeriksaanGigi").enable();

  // Ext.getCmp('BtnTambahObatTrKasirRwj').enable();

  // Ext.getCmp('BtnHapusObatTrKasirRwj').enable();
  Ext.getCmp("btnLookUpEditDokterPenindak_RWJ").enable();
  Ext.getCmp("btnbarisRad_PJ_RWJ").enable();
  Ext.getCmp("btnSimpanRad_PJ_RWJ").enable();
  Ext.getCmp("btnHpsBrsRad_PJ_RWJ").enable();

  Ext.getCmp("RJPJBtnAddLab_PJ_RWJ").enable();
  Ext.getCmp("RJPJBtnSaveLab_PJ_RWJ").enable();
  Ext.getCmp("RJPJBtnDelLab_PJ_RWJ").enable();
  Ext.getCmp("btnsimpanop_PJ_RWJ").enable();
}

var mRecordRad = Ext.data.Record.create([
  { name: "ID_RADKONSUL", mapping: "ID_RADKONSUL" },
  { name: "KD_PRODUK", mapping: "KD_PRODUK" },
  { name: "KD_KLAS", mapping: "KD_KLAS" },
  { name: "DESKRIPSI", mapping: "DESKRIPSI" },
  { name: "KD_DOKTER", mapping: "KD_DOKTER" },
]);

function TambahBarisRad() {
  var x = true;
  if (x === true) {
    var p = RecordBaruRad();
    dsRwJPJRAD.insert(dsRwJPJRAD.getCount(), p);
  }
}

function RecordBaruRad() {
  var p = new mRecordRad({
    ID_RADKONSUL: "",
    KD_PRODUK: "",
    KD_KLAS: "",
    KD_TARIF: "",
    DESKRIPSI: "",
    KD_DOKTER: "",
  });
  return p;
}

function radData() {
  dsrad = new WebApp.DataStore({
    fields: ["KD_PRODUK", "KD_KLAS", "KLASIFIKASI", "DESKRIPSI", "KD_DOKTER"],
  });
  dsrad.load({
    params: {
      Skip: 0,
      Take: 50,
      target: "ViewProdukRad",
      param: "",
    },
  });
  return dsrad;
}

function getRadtest() {
  var radCombobox = new Ext.form.ComboBox({
    id: "cboRadRequest",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    hideTrigger: true,
    forceSelection: true,
    selectOnFocus: true,
    fieldLabel: "Kode Test",
    align: "Right",
    store: radData(),
    valueField: "KD_PRODUK",
    displayField: "KD_PRODUK",
    anchor: "95%",
    listeners: {
      select: function (a, b, c) {
        dsRwJPJRAD.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
        dsRwJPJRAD.data.items[CurrentRad.row].data.DESKRIPSI = b.data.DESKRIPSI;
        dsRwJPJRAD.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
      },
    },
  });
  return radCombobox;
}

function getRadDesk() {
  var radComboboxDesk = new Ext.form.ComboBox({
    id: "cboRadRequestDesk",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    hideTrigger: true,
    forceSelection: true,
    selectOnFocus: true,
    fieldLabel: "Kode Test",
    align: "Right",
    store: radData(),
    valueField: "DESKRIPSI",
    displayField: "DESKRIPSI",
    anchor: "95%",
    listeners: {
      select: function (a, b, c) {
        dsRwJPJRAD.data.items[CurrentRad.row].data.KD_PRODUK = b.data.KD_PRODUK;
        dsRwJPJRAD.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
        dsRwJPJRAD.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
        dsRwJPJRAD.data.items[CurrentRad.row].data.KLASIFIKASI =
          b.data.KLASIFIKASI;
      },
    },
  });
  return radComboboxDesk;
}

function getRadDokter() {
  var radText = new Ext.form.TextField({
    id: "RadRequestDok",
    readonly: true,
    disable: true,
    store: radData(),
    value: "USERNAME",
    valueField: "USERNAME",
    displayField: "USERNAME",
    anchor: "95%",
    listeners: {},
  });
  return radText;
}

function getRadKelas() {
  var radText = new Ext.form.ComboBox({
    id: "RadRequestDok",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    hideTrigger: true,
    forceSelection: true,
    selectOnFocus: true,
    fieldLabel: "Kode Test",
    align: "Right",
    store: radData(),
    valueField: "KD_KLASS",
    displayField: "KD_KLASS",
    anchor: "95%",
    listeners: {},
  });
  return radText;
}

function datasavepoliklinikrad(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: getParamDetailPoliklinikRad(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoDiagnosa("Data berhasil disimpan", "Information");
        if (mBol === false) {
          RefreshDataSetDiagnosa(
            Ext.get("txtNoMedrecDetransaksi").dom.value,
            Ext.get("txtKdUnitRWJ").dom.value,
            Ext.get("dtpTanggalDetransaksi").dom.value
          );
        }
      } else if (cst.success === false && cst.pesan === 0) {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanWarningDiagnosa(nmPesanSimpanGagal, nmHeaderSimpanData);
      } else {
        RefreshDataSetDiagnosa(
          Ext.get("txtNoMedrecDetransaksi").dom.value,
          Ext.get("txtKdUnitRWJ").dom.value,
          Ext.get("dtpTanggalDetransaksi").dom.value
        );
        ShowPesanErrorDiagnosa(nmPesanSimpanError, nmHeaderSimpanData);
      }
    },
  });
}

function getParamDetailPoliklinikRad() {
  var params = {
    Table: "CrudpoliRad",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    UrutMasuk: Ext.get("txtKdUrutMasuk").getValue(),
    Tgl: Ext.get("dtpTanggalDetransaksi").dom.value,
    List: getArrdetailRadiologi(),
  };
  return params;
}

function getArrdetailRadiologi() {
  var x = "",
    y = "",
    z = "::",
    hasil;
  for (var k = 0; k < dsRwJPJRAD.getCount(); k++) {
    if (dsRwJPJRAD.data.items[k].data.KD_PRODUK == null) {
      x += "";
    } else {
      hasil = dsRwJPJRAD.data.items[k].data.KD_PRODUK;
      y = dsRwJPJRAD.data.items[k].data.KD_DOKTER;
      y += z + hasil;
    }
    x += y + "<>";
  }
  return x;
}

function HapusBarisRad() {
  if (cellselectedrad != undefined) {
    Ext.Msg.show({
      title: nmHapusBaris,
      msg: "Anda yakin akan menghapus",
      buttons: Ext.MessageBox.YESNO,
      fn: function (btn) {
        if (btn == "yes") {
          if (
            cellselectedrad.data.KD_PRODUK != "" &&
            cellselectedrad.data.DESKRIPSI != ""
          ) {
            dsRwJPJRAD.removeAt(CurrentRad.row);
            DataDeleteRad();
          } else {
            ShowPesanWarningRWJ("Pilih record ", "Hapus data");
          }
        }
      },
      icon: Ext.MessageBox.QUESTION,
    });
  }
}

function DataDeleteRad() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/DeleteDataObj",
    params: getParamDataDeleteRad(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoRWJ("Data berhasil dihapus", "Information");
        dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        cellSelecteddeskripsi = undefined;
        RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
        AddNewKasirRWJ = false;
      } else if (cst.success === false && cst.pesan === 0) {
        ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
      } else {
        ShowPesanWarningRWJ(nmPesanHapusError, nmHeaderHapusData);
      }
    },
  });
}

function getParamDataDeleteRad() {
  var params = {
    Table: "CrudpoliRad",
    Kdproduk: CurrentRad.data.data.KD_PRODUK,
    idradkonsul: CurrentRad.data.data.ID_RADKONSUL,
  };
  return params;
}

function RefreshDataSetRadiologi() {
  var strKriteriaRadiologi = "";
  strKriteriaRadiologi =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and kd_unit=~" +
    Ext.get("txtKdUnitRWJ").getValue() +
    "~ and tgl_masuk = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
  dsRwJPJRAD.load({
    params: {
      Skip: 0,
      Take: 50,
      Sort: "id_konsul",
      Sortdir: "ASC",
      target: "CrudpoliRad",
      param: strKriteriaRadiologi,
    },
  });
  return dsRwJPJRAD;
}

function pj_req_radhasil(
  kd_pasien,
  kd_unit,
  tgl_masuk,
  urut_masuk,
  kd_produk,
  urut
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRADPoliklinik/gethasiltest",
    params: {
      kd_pasien: kd_pasien,
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      kd_produk: kd_produk,
      urut: urut,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.hasil === "null" || cst.hasil === null || cst.hasil === "") {
        Ext.getCmp("textareapopuphasil_pjrwj").setValue(
          "--Hasil Radiologi tidak ditemukan--"
        );
      } else {
        Ext.getCmp("textareapopuphasil_pjrwj").setValue(cst.hasil);
      }
    },
  });
}

function getParamDetailTransaksiLAB() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/getcurrentshift",
    params: {
      command: "0",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      tampungshiftsekarang = o.responseText;
    },
  });
  var params = {
    mod_id: TrKasirRWJ_AG.mod_id,
    KdPasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    KdDokter: PenataJasaRJ.var_kd_dokter_leb,
    KdDokter_mr_tindakan: Ext.get("txtKdDokter").dom.value,
    kd_kasir: "default_kd_kasir_rwj",
    Modul: "rwj",
    Tgl: "",
    TglTransaksiAsal: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    KdCusto: vkode_customer,
    TmpCustoLama: "",
    Shift: tampungshiftsekarang,
    List: getArrPoliLab(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: PenataJasaRJ.ds3.getCount(),
    unit: 41,
    TmpNotransaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    KdKasirAsal: TrKasirRWJ_AG.setting.KD_KASIR,
    KdSpesial: "",
    Kamar: "",
    NoSJP: "",
    pasienBaru: 0,
    listTrDokter: [],
  };
  return params;
}

function getParamDetailTransaksiRAD() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/getcurrentshift",
    params: {
      command: "0",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      tampungshiftsekarang = o.responseText;
    },
  });
  var params = {
    NoTrans: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    KdPasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    KdDokter: PenataJasaRJ.var_kd_dokter_rad,
    KdDokter_mr_tindakan: Ext.get("txtKdDokter").dom.value,
    kd_kasir: "default_kd_kasir_rwj",
    mod_id: TrKasirRWJ_AG.mod_id,
    Modul: "rwj",
    Tgl: "",
    TglTransaksiAsal: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    KdCusto: vkode_customer,
    TmpCustoLama: "",
    Shift: tampungshiftsekarang,
    Modul: "rwj",
    List: getArrPoliRad(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: PenataJasaRJ.ds3.getCount(),
    unit: 5,
    TmpNotransaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    KdKasirAsal: "01",
    KdSpesial: "",
    Kamar: "",
    NoSJP: "",
    pasienBaru: 0,
    listTrDokter: [],
    unitaktif: "rwj",
  };
  return params;
}

function ViewGridBawahpoliRad(
  no_transaksi,
  kd_unit,
  tgl_transaksi,
  urut_masuk
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRADPoliklinik/getItemPemeriksaan",
    params: {
      no_transaksi: no_transaksi,
      kdunit: kd_unit,
      tgltrx: tgl_transaksi,
      urutmasuk: urut_masuk,
      kasirmana: "rwj",
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        dsRwJPJRAD.removeAll();
        var recs = [],
          recType = dsRwJPJRAD.recordType;

        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsRwJPJRAD.add(recs);

        // PenataJasaRJ.pj_req_rad.getView().refresh();
      }
    },
  });
}

function ViewGridBawahpoliLab(
  no_transaksi,
  kd_unit,
  tgl_transaksi,
  urut_masuk
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionLABPoliklinik/getItemPemeriksaan",
    params: {
      no_transaksi: no_transaksi,
      kd_unit: kd_unit,
      tgltrx: tgl_transaksi,
      urutmasuk: urut_masuk,
      kasirmana: "rwj",
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        PenataJasaRJ.ds3.removeAll();
        var recs = [],
          recType = PenataJasaRJ.ds3.recordType;

        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        PenataJasaRJ.ds3.add(recs);
        // PenataJasaRJ.grid3.getView().refresh();
      }
    },
  });
}
/* 
function ViewGridBawahpoliLab(kd_pasien,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getItemPemeriksaan",
			params: {no_transaksi:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					PenataJasaRJ.ds3.removeAll();
					var recs=[],
						recType=PenataJasaRJ.ds3.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					PenataJasaRJ.ds3.add(recs);
					
					PenataJasaRJ.grid3.getView().refresh();
				} 
			}
		}
		
	)
}; */

function getArrPoliLab() {
  var x = "";
  var arr = [];
  for (var i = 0; i < PenataJasaRJ.ds3.getCount(); i++) {
    console.log(PenataJasaRJ.ds3.data.items[i].data);
    var o = {};
    var y = "";
    var z = "@@##$$@@";

    o["URUT"] = PenataJasaRJ.ds3.data.items[i].data.urut;
    o["KD_PRODUK"] = PenataJasaRJ.ds3.data.items[i].data.kd_produk;
    o["QTY"] = PenataJasaRJ.ds3.data.items[i].data.qty;
    o["TGL_TRANSAKSI"] = PenataJasaRJ.ds3.data.items[i].data.tgl_transaksi;
    o["TGL_BERLAKU"] = PenataJasaRJ.ds3.data.items[i].data.tgl_berlaku;
    o["HARGA"] = PenataJasaRJ.ds3.data.items[i].data.harga;
    o["KD_UNIT"] = PenataJasaRJ.ds3.data.items[i].data.kd_unit;
    o["KD_TARIF"] = PenataJasaRJ.ds3.data.items[i].data.kd_tarif;
    o["NO_TRANSAKSI_BAWAH"] = PenataJasaRJ.ds3.data.items[i].data.no_transaksi;
    o["cito"] = PenataJasaRJ.ds3.data.items[i].data.cito;
    o["CATATAN_DOKTER"] = PenataJasaRJ.ds3.data.items[i].data.catatan_dokter;
    ;
    arr.push(o);
  }

  return Ext.encode(arr);
}

function getArrPoliRad() {
  var x = "";
  var arr = [];
  for (var i = 0; i < dsRwJPJRAD.getCount(); i++) {
    console.log(dsRwJPJRAD.data.items[i].data);
    var o = {};
    var y = "";
    var z = "@@##$$@@";
    o["URUT"] = dsRwJPJRAD.data.items[i].data.urut;
    o["KD_PRODUK"] = dsRwJPJRAD.data.items[i].data.kd_produk;
    o["QTY"] = dsRwJPJRAD.data.items[i].data.qty;
    o["TGL_TRANSAKSI"] = dsRwJPJRAD.data.items[i].data.tgl_transaksi;
    o["TGL_BERLAKU"] = dsRwJPJRAD.data.items[i].data.tgl_berlaku;
    o["HARGA"] = dsRwJPJRAD.data.items[i].data.harga;
    o["KD_UNIT"] = dsRwJPJRAD.data.items[i].data.kd_unit;
    o["KD_TARIF"] = dsRwJPJRAD.data.items[i].data.kd_tarif;
    o["NO_TRANSAKSI_BAWAH"] = dsRwJPJRAD.data.items[i].data.no_transaksi;
    o["cito"] = dsRwJPJRAD.data.items[i].data.cito;
    o["CATATAN_DOKTER"] = dsRwJPJRAD.data.items[i].data.catatan_dokter;
    arr.push(o);
  }
  return Ext.encode(arr);
}

function getFormatTanggal(date) {
  tanggal = new Date(date);
  month = "" + parseInt(tanggal.getMonth() + 1);
  day = "" + tanggal.getDate();
  year = "" + tanggal.getFullYear();
  if (month.length < 2) {
    month = "0" + parseInt(tanggal.getMonth() + 1);
  }
  if (day.length < 2) {
    day = "0" + tanggal.getDate();
  }
  return [year, month, day].join("-");
}

function GetDTGridHasilLab_PJRWJ() {
  var fm = Ext.form;
  var fldDetailHasilLab = [
    "KLASIFIKASI",
    "DESKRIPSI",
    "KD_LAB",
    "KD_TEST",
    "ITEM_TEST",
    "SATUAN",
    "NORMAL",
    "NORMAL_W",
    "NORMAL_A",
    "NORMAL_B",
    "COUNTABLE",
    "MAX_M",
    "MIN_M",
    "MAX_F",
    "MIN_F",
    "MAX_A",
    "MIN_A",
    "MAX_B",
    "MIN_B",
    "KD_METODE",
    "HASIL",
    "KET",
    "KD_UNIT_ASAL",
    "NAMA_UNIT_ASAL",
    "URUT",
    "METODE",
    "JUDUL_ITEM",
  ];
  PenataJasaRJ.dshasilLabRWJ = new WebApp.DataStore({
    fields: fldDetailHasilLab,
  });
  PenataJasaRJ.gridhasil_lab_PJRWJ = new Ext.grid.EditorGridPanel({
    title: "Detail Hasil Lab",
    stripeRows: true,
    store: PenataJasaRJ.dshasilLabRWJ,
    border: true,
    flex: 1,
    style: "margin-top:-1px;",
    columnLines: true,
    autoScroll: true,
    cm: new Ext.grid.ColumnModel([
      {
        id: Nci.getId(),
        header: "Kode Tes",
        dataIndex: "KD_TEST",
        width: 80,
        menuDisabled: true,
        hidden: true,
      },
      {
        header: "Item Pemeriksaan",
        dataIndex: "JUDUL_ITEM",
        width: 150,
        hidden: true,
        menuDisabled: true,
        style: {
          color: "red",
        },
      },
      {
        id: Nci.getId(),
        header: "Pemeriksaan",
        dataIndex: "ITEM_TEST",
        sortable: false,
        hidden: false,
        menuDisabled: true,
        width: 200,
      },
      {
        id: Nci.getId(),
        header: "Metode",
        dataIndex: "METODE",
        sortable: false,
        align: "center",
        hidden: false,
        menuDisabled: true,
        width: 100,
      },
      {
        id: Nci.getId(),
        header: "Hasil",
        dataIndex: "HASIL",
        sortable: false,
        hidden: false,
        menuDisabled: true,
        width: 100,
        align: "right",
      },
      {
        id: Nci.getId(),
        header: "Normal",
        dataIndex: "NORMAL",
        sortable: false,
        hidden: false,
        align: "center",
        menuDisabled: true,
        width: 100,
      },
      {
        header: "Satuan",
        dataIndex: "SATUAN",
        sortable: false,
        hidden: false,
        menuDisabled: true,
        width: 100,
      },
      {
        header: "Keterangan",
        dataIndex: "KET",
        width: 250,
      },
      {
        header: "Kode Lab",
        dataIndex: "KD_LAB",
        width: 250,
        hidden: true,
      },
    ]),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridhasil_lab_PJRWJ;
}
function GetGridRiwayatKunjunganPasien_RWJ() {
  var fldDetail = [
    "kd_pasien",
    "nama",
    "TGL_MASUK",
    "kd_unit",
    "nama_unit",
    "URUT_MASUK",
    "kd_kasir",
    "no_transaksi",
    "KD_DOKTER",
  ];
  dsTRRiwayatKunjuganPasien = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatKunjungan = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatKunjunganPasien",
    store: dsTRRiwayatKunjuganPasien,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    // height:265,
    // width:150,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, cell, rec) {
          cellSelectedriwayatkunjunganpasien =
            dsTRRiwayatKunjuganPasien.getAt(cell);
          CurrentSelectedRiwayatKunjunanPasien.cell = cell;
          CurrentSelectedRiwayatKunjunanPasien =
            cellSelectedriwayatkunjunganpasien.data;

          currentRiwayatKunjunganPasien_TglMasuk_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK;
          currentRiwayatKunjunganPasien_KdUnit_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.kd_unit;
          currentRiwayatKunjunganPasien_UrutMasuk_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK;
          currentRiwayatKunjunganPasien_KdKasir_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.kd_kasir;
          currentRiwayatKunjunganPasien_NoTrans_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi;
          currentRiwayatKunjunganPasien_KdDokter_RWJ =
            CurrentSelectedRiwayatKunjunanPasien.KD_DOKTER;

          var kd_unit_rwi = CurrentSelectedRiwayatKunjunanPasien.kd_unit;
          if (
            CurrentSelectedRiwayatKunjunanPasien.kd_unit_kamar != null &&
            CurrentSelectedRiwayatKunjunanPasien.kd_unit_kamar != ""
          ) {
            kd_unit_rwi = CurrentSelectedRiwayatKunjunanPasien.kd_unit_kamar;
          }

          viewAnamnese_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_unit,
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_kasir,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );

          dsTRRiwayatDiagnosa_RWJ.removeAll();
          viewGridRiwayatDiagnosa_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_unit,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );

          dsTRRiwayatAnamnese_RWJ.removeAll();

          dsTRRiwayatTindakan_RWJ.removeAll();
          viewGridRiwayatTindakan_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_unit,
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_kasir,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );

          dsTRRiwayatObat_RWJ.removeAll();
          viewGridRiwayatObat_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            kd_unit_rwi,
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );

          dsTRRiwayatLab_RWJ.removeAll();
          viewGridRiwayatLab_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            kd_unit_rwi,
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );

          dsTRRiwayatRad_RWJ.removeAll();
          viewGridRiwayatRad_RWJ(
            CurrentSelectedRiwayatKunjunanPasien.TGL_MASUK,
            kd_unit_rwi,
            CurrentSelectedRiwayatKunjunanPasien.URUT_MASUK,
            CurrentSelectedRiwayatKunjunanPasien.kd_kasir,
            CurrentSelectedRiwayatKunjunanPasien.no_transaksi
          );
        },
      },
    }),
    cm: DetailGridRiwayatKunjunganColumModel(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatKunjungan;
}

function DetailGridRiwayatKunjunganColumModel() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Tgl Kunjungan",
      dataIndex: "TGL_MASUK",
      width: 70,
      menuDisabled: true,
      renderer: function (value, metaData, record, rowIndex, colIndex, store) {
        return ShowDate(record.data.TGL_MASUK);
      },
    },
    {
      header: "Unit",
      dataIndex: "nama_unit",
      menuDisabled: true,
      width: 100,
    },
    {
      header: "kd_unit",
      dataIndex: "kd_unit",
      hidden: true,
    },
    {
      header: "kd_pasien",
      dataIndex: "kd_pasien",
      hidden: true,
    },
  ]);
}

function GetGridRiwayatDiagnosa_RWJ() {
  var fldDetail = ["penyakit", "stat_diag", "kasus", "kd_penyakit"];
  dsTRRiwayatDiagnosa_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatDiagnosa = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatDiagnosa_RWJ",
    store: dsTRRiwayatDiagnosa_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          /* cellSelectedriwayatkunjunganpasien = dsTRRiwayatDiagnosa_RWJ.getAt(row);
                        CurrentSelectedRiwayatKunjunanPasien.row = row;
                        CurrentSelectedRiwayatKunjunanPasien.data = cellSelectedriwayatkunjunganpasien; */
        },
      },
    }),
    cm: DetailGridRiwayatDiagnosaColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatDiagnosa;
}

function GetGridAnamnese_RWJ() {
  var fldDetail = ["kondisi", "nilai", "satuan"];
  dsTRRiwayatAnamnese_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatAnamnese = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatAnamnese_RWJ",
    store: dsTRRiwayatAnamnese_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          /* cellSelectedriwayatkunjunganpasien = dsTRRiwayatDiagnosa_RWJ.getAt(row);
                        CurrentSelectedRiwayatKunjunanPasien.row = row;
                        CurrentSelectedRiwayatKunjunanPasien.data = cellSelectedriwayatkunjunganpasien; */
        },
      },
    }),
    cm: DetailGridAnamneseColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatAnamnese;
}

function DetailGridAnamneseColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Kondisi",
      dataIndex: "kondisi",
      menuDisabled: true,
      width: 100,
    },
    {
      header: "Nilai",
      dataIndex: "nilai",
      width: 60,
    },
    {
      header: "Satuan",
      dataIndex: "satuan",
      width: 60,
    },
  ]);
}

function DetailGridRiwayatDiagnosaColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Penyakit",
      dataIndex: "penyakit",
      menuDisabled: true,
      width: 100,
    },
    {
      header: "Status diagnosa",
      dataIndex: "stat_diag",
      width: 60,
    },
    {
      header: "Kasus",
      dataIndex: "kasus",
      width: 60,
    },
    {
      header: "kd_penyakit",
      dataIndex: "kd_penyakit",
      hidden: true,
    },
  ]);
}

function GetGridRiwayatTindakan_RWJ() {
  var fldDetail = ["kd_produk", "deskripsi"];
  dsTRRiwayatTindakan_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatTindakan = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatTindakan_RWJ",
    store: dsTRRiwayatTindakan_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: DetailGridRiwayatTindakanColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatTindakan;
}

function DetailGridRiwayatTindakanColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Deskripsi tindakan",
      dataIndex: "deskripsi",
      menuDisabled: true,
      width: 100,
    },
    {
      header: "kd_produk",
      dataIndex: "kd_produk",
      hidden: true,
    },
  ]);
}

function GetGridRiwayatObat_RWJ() {
  var fldDetail = ["kd_prd", "nama_obat"];
  dsTRRiwayatObat_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatObat = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatObat_RWJ",
    store: dsTRRiwayatObat_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: DetailGridRiwayatObatColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatObat;
}

function DetailGridRiwayatObatColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Nama Obat",
      dataIndex: "nama_obat",
      menuDisabled: true,
      width: 100,
    },
    {
      header: "kd_prd",
      dataIndex: "kd_prd",
      hidden: true,
    },
  ]);
}

function GetGridRiwayatLab_RWJ() {
  var fldDetail = [
    "klasifikasi",
    "deskripsi",
    "kd_lab",
    "kd_test",
    "item_test",
    "satuan",
    "normal",
    "normal_w",
    "normal_a",
    "normal_b",
    "countable",
    "max_m",
    "min_m",
    "max_f",
    "min_f",
    "max_a",
    "min_a",
    "max_b",
    "min_b",
    "kd_metode",
    "hasil",
    "ket",
    "kd_unit_asal",
    "nama_unit_asal",
    "urut",
    "metode",
    "judul_item",
    "ket_hasil",
  ];
  dsTRRiwayatLab_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatLab = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatLab_RWJ",
    store: dsTRRiwayatLab_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    cm: DetailGridRiwayatLabColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatLab;
}

function DetailGridRiwayatLabColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Kode Tes",
      dataIndex: "kd_test",
      width: 80,
      menuDisabled: true,
      hidden: true,
    },
    {
      header: "Item Pemeriksaan",
      dataIndex: "judul_item",
      width: 150,
      menuDisabled: true,
      renderer: function (value, metaData, record, rowIndex, colIndex, store) {
        metaData.style = "font-color:#ffb3b3;";
        return value;
      },
    },
    {
      header: "Pemeriksaan",
      dataIndex: "item_test",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 200,
    },
    {
      header: "Metode",
      dataIndex: "metode",
      sortable: false,
      align: "center",
      hidden: false,
      menuDisabled: true,
      width: 90,
    },
    {
      header: "Hasil",
      dataIndex: "hasil",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 90,
      align: "right",
    },
    {
      header: "Normal",
      dataIndex: "normal",
      sortable: false,
      hidden: false,
      align: "center",
      menuDisabled: true,
      width: 90,
    },
    {
      header: "Ket Hasil",
      dataIndex: "ket_hasil",
      sortable: false,
      hidden: false,
      align: "center",
      menuDisabled: true,
      width: 70,
    },
    {
      header: "Satuan",
      dataIndex: "satuan",
      sortable: false,
      hidden: false,
      menuDisabled: true,
      width: 70,
    },
    {
      header: "Keterangan",
      dataIndex: "ket",
      width: 90,
    },
    {
      header: "Kode Lab",
      dataIndex: "kd_lab",
      width: 250,
      hidden: true,
    },
  ]);
}

function GetGridRiwayatRad_RWJ() {
  var fldDetail = [
    "kd_produk",
    "deskripsi",
    "kd_tarif",
    "harga",
    "qty",
    "desc_req",
    "cito",
    "tgl_berlaku",
    "no_transaksi",
    "urut",
    "desc_status",
    "tgl_transaksi",
    "jumlah",
    "kd_dokter",
    "namadok",
    "lunas",
    "kd_pasien",
    "urutkun",
    "tglkun",
    "kdunitkun",
    "hasil",
  ];
  dsTRRiwayatRad_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridRiwayatRad = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridTabRiwayatRad_RWJ",
    store: dsTRRiwayatRad_RWJ,
    border: false,
    columnLines: true,
    // frame: false,
    autoScroll: true,
    height: 300,
    // width:770,
    // anchor: '100%',
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {},
      },
    }),
    tbar: [
      {
        xtype: "button",
        text: "Hasil Scan",
        id: "BtnHasilScan_RWJ",
        iconCls: "Edit_Tr",
        handler: function () {
          var dsPacs_RWJ = new WebApp.DataStore({
            fields: [
              "NO_TRANSAKSI",
              "KD_KASIR",
              "TGL_TRANSAKSI",
              "TINDAKAN",
              "PATIENT_ID",
              "STUDY_UID",
            ],
          });
          dsPacs_RWJ.load({
            params: {
              Skip: 0,
              Take: 50,
              target: "ViewPacsList",
              param:
                " kunjungan.kd_pasien = ~" +
                Ext.getCmp("txtNoMedrecDetransaksi").getValue() +
                "~ AND kunjungan.kd_unit = ~" +
                CurrentSelectedRiwayatKunjunanPasien.kd_unit +
                "~ AND kunjungan.tgl_masuk = ~" +
                CurrentSelectedRiwayatKunjunanPasien.tgl_masuk +
                "~ AND kunjungan.urut_masuk=~" +
                CurrentSelectedRiwayatKunjunanPasien.urut_masuk +
                "~",
            },
            callback: function () {
              if (dsPacs_RWJ.data.length == 0) {
                ShowPesanWarningRWJ(
                  "Tidak ada hasil pemeriksaan",
                  "Peringatan"
                );
                setLookUp_TransaksiRadiologi_RWJ.close();
              }
            },
          });
          var grid = new Ext.grid.EditorGridPanel({
            stripeRows: true,
            id: "GridList_PACS_RWJ",
            store: dsPacs_RWJ,
            border: false,
            columnLines: true,
            autoScroll: true,
            height: 200,
            sm: new Ext.grid.RowSelectionModel(),
            cm: new Ext.grid.ColumnModel([
              new Ext.grid.RowNumberer(),
              {
                header: "No Transaksi",
                dataIndex: "NO_TRANSAKSI",
                width: 80,
                menuDisabled: true,
                hidden: false,
              },
              {
                header: "KD Kasir",
                dataIndex: "KD_KASIR",
                width: 80,
                menuDisabled: true,
                hidden: true,
              },
              {
                header: "Tgl Masuk",
                dataIndex: "TGL_TRANSAKSI",
                width: 100,
                menuDisabled: true,
              },
              {
                header: "Tindakan",
                dataIndex: "TINDAKAN",
                width: "100%",
                menuDisabled: true,
              },
              {
                header: "Patient ID",
                dataIndex: "PATIENT_ID",
                hidden: true,
              },
              {
                header: "Study ID",
                dataIndex: "STUDY_UID",
                hidden: true,
              },
            ]),
            listeners: {
              rowdblclick: function (sm, row, rec) {
                var selection_data = sm.selModel.selections.items[0].data;
                var url_file =
                  "http://10.11.0.35:8080/oviyam/viewer.html?patientID=" +
                  selection_data.PATIENT_ID +
                  "&studyUID=" +
                  selection_data.STUDY_UID +
                  "&serverName=PACS";

                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("target", "_blank");
                form.setAttribute("action", url_file);
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "data");
                hiddenField.setAttribute("value", "");
                form.appendChild(hiddenField);
                document.body.appendChild(form);
                form.submit();
              },
            },
          });

          var setLookUp_TransaksiRadiologi_RWJ = new Ext.Window({
            id: "IdsetLookUp_TransaksiRadiologi_RWJ",
            name: "IdsetLookUp_TransaksiRadiologi_RWJ",
            title: "Hasil Scan Radiologi",
            closeAction: "destroy",
            width: 410,
            height: 200,
            resizable: false,
            autoScroll: true,
            border: true,
            constrain: true,
            iconCls: "Studi_Lanjut",
            modal: true,
            items: [grid],
            fbar: [
              {
                xtype: "button",
                text: "Batal",
                width: 70,
                hideLabel: true,
                id: "btnKeluar_HasilScan_RWJ",
                handler: function () {
                  setLookUp_TransaksiRadiologi_RWJ.close();
                },
              },
            ],
          });

          setLookUp_TransaksiRadiologi_RWJ.show();
        },
      },
    ],
    cm: DetailGridRiwayatRadColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridRiwayatRad;
}

function DetailGridRiwayatRadColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "kd_produk",
      dataIndex: "kd_produk",
      width: 80,
      menuDisabled: true,
      hidden: true,
    },
    {
      header: "Deskripsi pemeriksaan radiologi",
      dataIndex: "deskripsi",
      width: 100,
      menuDisabled: true,
    },
    {
      header: "Hasil",
      dataIndex: "hasil",
      width: 150,
      menuDisabled: true,
    },
  ]);
}

function viComboKamar_viJdwlOperasi() {
  var Field = ["kd_unit", "no_kamar", "nama_kamar", "jumlah_bed", "digunakan"];
  dsvComboKamarOperasiJadwalOperasiOK = new WebApp.DataStore({ fields: Field });
  dsComboKamarOperasiJadwalOperasiOK();
  var cbo_viComboKamar_viJdwlOperasi = new Ext.form.ComboBox({
    id: "cbo_viComboKamar_viJdwlOperasi",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "Pilih Kamar..",
    fieldLabel: "Kamar",
    width: 230,
    store: dsvComboKamarOperasiJadwalOperasiOK,
    valueField: "no_kamar",
    displayField: "nama_kamar",
    listeners: {
      select: function (a, b, c) {},
    },
  });
  return cbo_viComboKamar_viJdwlOperasi;
}
function dsComboKamarOperasiJadwalOperasiOK() {
  dsvComboKamarOperasiJadwalOperasiOK.load({
    params: {
      Skip: 0,
      Take: "",
      Sort: "",
      Sortdir: "ASC",
      target: "vComboKamarOperasiJadwalOperasiOK",
      //param:'kd_spesial='+kriteria
    },
  });
  return dsvComboKamarOperasiJadwalOperasiOK;
}
function getItemTrPenJasRWJ_Batas() {
  var items = {
    layout: "column",
    border: false,
    items: [
      {
        layout: "absolute",
        bodyStyle: "padding: 0px 0px 0px 0px",
        border: false,
        width: 100,
        height: 5,
        anchor: "100% 100%",
        items: [],
      },
    ],
  };
  return items;
}

function getTotKunjunganRWJ() {
  var tglAwal = nowTglTransaksiGrid_poli.format("Y-m-d");
  var tglAkhir = nowTglTransaksiGrid_poli.format("Y-m-d");
  if (
    Ext.isDefined(Ext.getCmp("dtpTglAwalFilterRWJ")) &&
    Ext.isDefined(Ext.getCmp("dtpTglAkhirFilterRWJ"))
  ) {
    tglAwal = Ext.getCmp("dtpTglAwalFilterRWJ").getValue();
    tglAkhir = Ext.getCmp("dtpTglAkhirFilterRWJ").getValue();
  }
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getTotKunjungan",
    // params		: {query:""},
    params: {
      tglAwal: tglAwal,
      tglAkhir: tglAkhir,
    },
    failure: function (o) {
      // ShowPesanErrorRWJ("Hubungi Admin", "Gagal");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        Ext.getCmp("txtTotKunjunganHariIni_TrKasirRWJ").setValue(
          cst.totalkunjungan
        );
      } else {
        ShowPesanErrorRWJ("Gagal menampilkan total kunjungan", "Gagal");
      }
    },
  });
}

function updateStatusPeriksa() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/updateStatusPeriksa",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      tgl_masuk: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Gagal");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanErrorRWJ("Gagal update status pemeriksaan", "Gagal");
      }
    },
  });
}

function datasave_TrPenJasRWJ() {
  if (ValidasiEntryIcd9_RWJ(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/main/functionRWJ/saveIcd9",
      params: getParamIcd9RWJ(),
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
          ShowPesanInfoRWJ("Simpan berhasil", "Information");
          //datasave_TrPenJasRWJ_SQL();
          viewGridIcd9_RWJ();
          //refreshDataDepanPenjasRWJ();
          //refeshkasirrwj();
        } else {
          ShowPesanErrorRWJ("Gagal Menyimpan Data ini", "Error");
        }
      },
    });
  }
}

function ValidasiEntryIcd9_RWJ(modul, mBolHapus) {
  var x = 1;
  if (dsTRDetailDiagnosaListIcd9.getCount() > 0) {
    for (var i = 0; i < dsTRDetailDiagnosaListIcd9.getCount(); i++) {
      var o = dsTRDetailDiagnosaListIcd9.getRange()[i].data;
      if (o.kd_icd9 == undefined || o.kd_icd9 == "") {
        ShowPesanWarningRWJ(
          "No. Icd 9  masih kosong, periksa kembali daftar Icd 9!",
          "Warning"
        );
        x = 0;
      }

      for (var j = 0; j < dsTRDetailDiagnosaListIcd9.getCount(); j++) {
        var p = dsTRDetailDiagnosaListIcd9.getRange()[j].data;
        if (i != j && o.kd_icd9 == p.kd_icd9) {
          ShowPesanWarningRWJ(
            "No. Icd 9 tidak boleh sama, periksa kembali daftar Icd 9!",
            "Warning"
          );
          x = 0;
          break;
        }
      }
    }
  } else {
    ShowPesanWarningRWJ(
      "Daftar Icd 9 tidak boleh kosong, minimal 1 data!",
      "Warning"
    );
    x = 0;
  }

  return x;
}

function getParamIcd9RWJ() {
  var params = {
    kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    tgl_masuk: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    urut_masuk: Ext.getCmp("txtKdUrutMasuk").getValue(),
    no_transaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    kd_kasir: currentKdKasirRWJ,
  };

  params["jumlah"] = dsTRDetailDiagnosaListIcd9.getCount();
  var prevurut = 0;
  for (var i = 0; i < dsTRDetailDiagnosaListIcd9.getCount(); i++) {
    var urut = 0;
    if (
      dsTRDetailDiagnosaListIcd9.data.items[i].data.urut == "" ||
      dsTRDetailDiagnosaListIcd9.data.items[i].data.urut == undefined
    ) {
      urut = parseInt(prevurut) + 1;
    } else {
      urut = dsTRDetailDiagnosaListIcd9.data.items[i].data.urut;
    }
    params["kd_icd9-" + i] =
      dsTRDetailDiagnosaListIcd9.data.items[i].data.kd_icd9;
    params["urut-" + i] = urut;

    prevurut = urut;
  }

  return params;
}

function viewGridIcd9_RWJ() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgridicd9",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      tgl_masuk: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRDetailDiagnosaListIcd9.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRDetailDiagnosaListIcd9.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRDetailDiagnosaListIcd9.add(recs);
        // PenataJasaRJ.gridIcd9.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca daftar icd 9", "Error");
      }
    },
  });
}

function viewGridRiwayatKunjunganPasien_RWJ() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWI/viewgridriwayatkunjungan",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatKunjuganPasien.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatKunjuganPasien.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatKunjuganPasien.add(recs);
        // PenataJasaRJ.gridRiwayatKunjungan.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat kunjungan", "Error");
      }
    },
  });
}

function viewAnamnese_RWJ(tgl_masuk, kd_unit, urut_masuk, no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewanamnese",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        Ext.getCmp("txtAmnase_RWJ").setValue(cst.anamnese);
        Ext.getCmp("txtStatus_RWJ").setValue(cst.status);
        Ext.getCmp("txtCatatan_RWJ").setValue(cst.catatan);

        dsTRRiwayatAnamnese_RWJ.removeAll();
        var recs = [],
          recType = dsTRRiwayatAnamnese_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatAnamnese_RWJ.add(recs);
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat diagnosa", "Error");
      }
    },
  });
}

function viewGridRiwayatDiagnosa_RWJ(tgl_masuk, kd_unit, no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgridriwayatdiagnosa",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatDiagnosa_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatDiagnosa_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatDiagnosa_RWJ.add(recs);
        // PenataJasaRJ.gridRiwayatDiagnosa.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat diagnosa", "Error");
      }
    },
  });
}

function viewGridRiwayatTindakan_RWJ(
  tgl_masuk,
  kd_unit,
  urut_masuk,
  kd_kasir,
  no_transaksi
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgridriwayattindakan",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      kd_kasir: kd_kasir,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatTindakan_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatTindakan_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatTindakan_RWJ.add(recs);
        // PenataJasaRJ.gridRiwayatTindakan.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat tindakan", "Error");
      }
    },
  });
}

function viewGridRiwayatObat_RWJ(tgl_masuk, kd_unit, urut_masuk, no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWI/viewgridriwayatobat",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatObat_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatObat_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatObat_RWJ.add(recs);
        // PenataJasaRJ.gridRiwayatObat.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat obat", "Error");
      }
    },
  });
}

function viewGridRiwayatLab_RWJ(tgl_masuk, kd_unit, urut_masuk, no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionIGD/viewgridriwayatlab",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatLab_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatLab_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatLab_RWJ.add(recs);
        // PenataJasaRJ.gridRiwayatLab.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat laboratorium", "Error");
      }
    },
  });
}

function viewGridRiwayatRad_RWJ(
  tgl_masuk,
  kd_unit,
  urut_masuk,
  kd_kasir,
  no_transaksi
) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionIGD/viewgridriwayatrad",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: kd_unit,
      tgl_masuk: tgl_masuk,
      urut_masuk: urut_masuk,
      kd_kasir: kd_kasir,
      no_transaksi: no_transaksi,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsTRRiwayatRad_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsTRRiwayatRad_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsTRRiwayatRad_RWJ.add(recs);
        // PenataJasaRJ.gridRiwayatRad.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca riwayat radiologi", "Error");
      }
    },
  });
}

function panelScanBerkas_RWJ(noMedrec) {
  win_scan_berkas_rwj = new Ext.Window({
    id: "win_scan_berkas_rwj",
    title: "Scan Berkas Pasien",
    closeAction: "destroy",
    width: 340,
    height: 120,
    border: false,
    resizable: false,
    plain: true,
    layout: "fit",
    iconCls: "icon_lapor",
    modal: true,
    items: [subPanelScanBerkas_RWJ(noMedrec)],
    fbar: [],
    listeners: {
      show: function () {
        blRefreshPenataJasa = false;
      },
      hide: function () {
        blRefreshPenataJasa = true;
      },
      activate: function () {
        Ext.getCmp("txtKdPasienScanBerkas_RWJ").focus();
      },
      afterShow: function () {
        Ext.getCmp("txtKdPasienScanBerkas_RWJ").focus();
      },
    },
  });
  win_scan_berkas_rwj.show();
  Ext.getCmp("txtKdPasienScanBerkas_RWJ").focus(true, 1000);
}

function subPanelScanBerkas_RWJ(noMedrec) {
  if (noMedrec == undefined) {
    noMedrec = "";
  }
  var items = {
    layout: "form",
    border: false,
    bodyStyle: "padding: 5px",
    items: [
      {
        layout: "absolute",
        bodyStyle: "padding: 10px ",
        border: false,
        height: 110,
        anchor: "100% 100%",
        items: [
          {
            x: 0,
            y: 0,
            xtype: "textfield",
            width: 317,
            height: 55,
            value: noMedrec,
            tabIndex: 0,
            name: "txtKdPasienScanBerkas_RWJ",
            id: "txtKdPasienScanBerkas_RWJ",
            emptyText: "No. Medrec",
            style: { "font-size": "32px", "text-align": "center" },
            listeners: {
              specialkey: function () {
                if (Ext.EventObject.getKey() === 13) {
                  var tmpNoMedrec = Ext.get(
                    "txtKdPasienScanBerkas_RWJ"
                  ).getValue();
                  if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
                    var tmpgetNoMedrec = formatnomedrec(
                      Ext.get("txtKdPasienScanBerkas_RWJ").getValue()
                    );
                    Ext.getCmp("txtKdPasienScanBerkas_RWJ").setValue(
                      tmpgetNoMedrec
                    );
                    updateAntrianRWJ();
                    //win_scan_berkas_rwj.close();
                  } else {
                    updateAntrianRWJ();
                  }
                }
              },
            },
          },
          {
            x: 0,
            y: 57,
            xtype: "label",
            flex: 1,
            style: {
              color: "darkblue",
            },
            text: "*) Tekan enter untuk update, jika scan barcode tidak berfungsi",
          },
        ],
      },
    ],
  };
  return items;
}

function panelScanBerkasPilihUnit_RWJ() {
  win_scan_berkas_pilih_unit_rwj = new Ext.Window({
    id: "win_scan_berkas_pilih_unit_rwj",
    title: "Pilih unit yang dituju",
    closeAction: "destroy",
    width: 200,
    height: 90,
    border: false,
    resizable: false,
    plain: true,
    layout: "fit",
    iconCls: "icon_lapor",
    modal: true,
    items: [comboUnitScanBerkas_RWJ()],
    fbar: [
      {
        xtype: "button",
        text: "Ok",
        width: 70,
        hideLabel: true,
        id: "btnOkUpdateUnitScanBerkasRWJ",
        handler: function () {
          updateUnitAntrianRWJ();
          refreshDataDepanPenjasRWJ();
        },
      },
      {
        xtype: "button",
        text: "Cancel",
        width: 70,
        hideLabel: true,
        id: "btnCancelUpdateUnitScanBerkasRWJ",
        handler: function () {
          win_scan_berkas_pilih_unit_rwj.close();
        },
      },
    ],
    listeners: {
      activate: function () {
        load_data_unit_scan_berkas_RWJ();
      },
      afterShow: function () {},
    },
  });

  win_scan_berkas_pilih_unit_rwj.show();
  load_data_unit_scan_berkas_RWJ();
}

function comboUnitScanBerkas_RWJ() {
  var Field = ["kd_unit", "nama_unit"];

  dsunitscanberkasrwj = new WebApp.DataStore({ fields: Field });

  load_data_unit_scan_berkas_RWJ();
  cbounitscanberkasrwj = new Ext.form.ComboBox({
    id: "cbounitscanberkasrwj",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    anchor: "100%",
    store: dsunitscanberkasrwj,
    valueField: "kd_unit",
    displayField: "nama_unit",
    listeners: {},
  });
  return cbounitscanberkasrwj;
}

function load_data_unit_scan_berkas_RWJ(param) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getunitscanberkas",
    params: {
      kd_pasien: Ext.getCmp("txtKdPasienScanBerkas_RWJ").getValue(),
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      cbounitscanberkasrwj.store.removeAll();
      var cst = Ext.decode(o.responseText);

      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = dsunitscanberkasrwj.recordType;
        var o = cst["listData"][i];

        recs.push(new recType(o));
        dsunitscanberkasrwj.add(recs);
      }
    },
  });
}

function updateAntrianRWJ() {
  /*
		PERBARUAN TANGGAL 2017-08-23
		OLEH 	: HADAD 
		ALASAN 	: ADA VALIDASI PADA SAAT PERPINDAHAN STATUS DENGAN MEDREC YANG SAMA
	*/
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/cek_data_status",
    params: {
      kd_pasien: Ext.getCmp("txtKdPasienScanBerkas_RWJ").getValue(),
      kd_unit: kode_unit_scanberkas,
      id_antrian: Ext.getCmp("cboStatusAntrian_viKasirRwj").getValue(),
      tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
    },
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status === true) {
        if (cst.id_status_antrian == 0) {
          Ext.Ajax.request({
            url: baseURL + "index.php/main/functionRWJ/updatestatusantrian",
            params: {
              kd_pasien: Ext.getCmp("txtKdPasienScanBerkas_RWJ").getValue(),
              kd_unit: cst.kd_unit,
              tgl_masuk: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
            },
            failure: function (o) {
              ShowPesanErrorRWJ("Hubungi Admin", "Error");
            },
            success: function (o) {
              var cst = Ext.decode(o.responseText);
              tmpKdUnit_TrKasir = "";
              if (cst.success === true) {
                // ShowPesanInfoRWJ('Scan berhasil','Information')
                Ext.getCmp("txtKdPasienScanBerkas_RWJ").setValue("");
                //refeshkasirrwj();
                // updateAntrianSedangDilayani(1);
                RefreshDataFilterKasirRWJ();
                win_scan_berkas_rwj.close();
                // Ext.getCmp('txtFilterNomedrec').focus(true,1000);
                // win_scan_berkas_rwj.close();
              } else if (cst.status == "gagal") {
                ShowPesanWarningRWJ("Kode pasien tidak ada", "Error");
              } else if (cst.status == "double") {
                Ext.Msg.show({
                  title: "Warning",
                  width: 300,
                  msg: "Ditemukan lebih dari 1 berkas pasien, tekan OK untuk melanjutkan memilih unit tujuan!",
                  buttons: Ext.MessageBox.OKCANCEL,
                  fn: function (btn) {
                    if (btn == "ok") {
                      panelScanBerkasPilihUnit_RWJ();
                    }
                  },
                  icon: Ext.MessageBox.QUESTION,
                });
              } else {
                ShowPesanWarningRWJ(
                  "Berkas pasien ini sudah discan!",
                  "Warning"
                );
              }
            },
          });
        } else if (cst.id_status_antrian == 1) {
          var tmp_kd_pasien = Ext.getCmp(
            "txtKdPasienScanBerkas_RWJ"
          ).getValue();
          // console.log("Update");

          for (var i = 0; i < dsTRKasirRWJList.data.length; i++) {
            if (
              tmp_kd_pasien == dsTRKasirRWJList.data.items[i].data.KD_PASIEN
            ) {
              // console.log(dsTRKasirRWJList.data.items[i]);
              tmp_data = dsTRKasirRWJList.data.items[i];
              // console.log(tmp_data);
              updateAntrianSedangDilayani_revisi(2, tmp_data);
            }
          }
          Ext.getCmp("txtKdPasienScanBerkas_RWJ").setValue("");
          // win_scan_berkas_rwj.close();

          /*Ext.Msg.show({
						title:'Konfirmasi',
						msg: ' Apakah pasien sudah ada ?',
						buttons: Ext.MessageBox.YESNO,
						fn: function (btn){
							if (btn =='yes'){
								for (var i = 0; i < dsTRKasirRWJList.data.length; i++) {

									if (tmp_kd_pasien == dsTRKasirRWJList.data.items[i].data.KD_PASIEN) {
										console.log(dsTRKasirRWJList.data.items[i]);
										objRWJAG=dsTRKasirRWJList.data.items[i].data;
                						PenataJasaRJ.s1 = dsTRKasirRWJList.data.items[i];

										cekTransferTindakanPenjasRWJ(true,dsTRKasirRWJList.data.items[i]);
										RWJLookUp(dsTRKasirRWJList.data.items[i].data);
										updateAntrianSedangDilayani(2);
										refreshDataDepanPenjasRWJ();
									}
								}
							}
						},
						icon: Ext.MessageBox.QUESTION
					});*/
        } else {
          ShowPesanWarningRWJ("Pasien sudah dilayani", "Informasi");
        }
      } else {
      }
    },
  });
}

function updateAntrianSedangDilayani(status) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/main/functionRWJ/updatestatusantrian_sedangdilayani",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      tgl_masuk: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
      urut_masuk: rowSelectedKasirRWJ.data.URUT_MASUK,
      status: status,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Gagal");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanErrorRWJ("Gagal update status pemeriksaan", "Gagal");
      }
    },
  });
}

function updateAntrianSedangDilayani_revisi(status, tmp_data) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/main/functionRWJ/updatestatusantrian_sedangdilayani",
    params: {
      kd_pasien: tmp_data.data.KD_PASIEN,
      kd_unit: tmp_data.data.KD_UNIT,
      tgl_masuk: tmp_data.data.TANGGAL_TRANSAKSI,
      urut_masuk: tmp_data.data.URUT_MASUK,
      status: status,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Gagal");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        RefreshDataFilterKasirRWJ();
      } else {
        ShowPesanErrorRWJ("Gagal update status pemeriksaan", "Gagal");
      }
    },
  });
}

function updateUnitAntrianRWJ() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/updateunitantrian",
    params: {
      kd_pasien: Ext.getCmp("txtKdPasienScanBerkas_RWJ").getValue(),
      kd_unit: Ext.getCmp("cbounitscanberkasrwj").getValue(),
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        ShowPesanInfoRWJ("Scan berhasil", "Information");
        Ext.getCmp("txtKdPasienScanBerkas_RWJ").setValue("");
        win_scan_berkas_pilih_unit_rwj.close();
        //refeshkasirrwj();
      } else {
        ShowPesanErrorRWJ("Gagal update status berkas pasien", "Error");
      }
    },
  });
}

function LookupPilihDokterPenindak_RWJ(rowdata_AG) {
  setLookUpPilihDokterPenindak_RWJ = new Ext.Window({
    id: "setLookUpPilihDokterPenindak_RWJ",
    name: "setLookUpPilihDokterPenindak_RWJ",
    title: "Pilih Dokter Penindak Poli",
    closeAction: "destroy",
    width: 410,
    height: 130,
    resizable: false,
    autoScroll: false,
    border: true,
    constrain: true,
    iconCls: "Studi_Lanjut",
    modal: true,
    items: [PanelPilihDokterPenindak_RWJ()],
    fbar: [
      {
        xtype: "button",
        text: "OK",
        width: 70,
        hideLabel: true,
        id: "btnOKPilihDokterPenindak_RWJ",
        handler: function () {
          updateDokterPenindakPoli_RWJ(rowdata_AG);
        },
      },
      {
        xtype: "button",
        text: "Batal",
        width: 70,
        hideLabel: true,
        id: "btnBatalPilihDokterPenindak_RWJ",
        handler: function () {
          setLookUpPilihDokterPenindak_RWJ.close();
        },
      },
    ],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {},
    },
  });

  setLookUpPilihDokterPenindak_RWJ.show();
  loadDataDokterPenindakPoli_RWJ(rowdata_AG.KD_UNIT);
}

function PanelPilihDokterPenindak_RWJ() {
  var items = {
    layout: "form",
    border: true,
    bodyStyle: "padding: 5px",
    height: 195,
    items: [
      {
        layout: "absolute",
        bodyStyle: "padding: 10px ",
        border: true,
        width: 465,
        height: 40,
        anchor: "100% 23%",
        items: [
          {
            x: 10,
            y: 10,
            xtype: "label",
            text: "Pilih Dokter",
          },
          {
            x: 70,
            y: 10,
            xtype: "label",
            text: ":",
          },
          comboDokterPenindak_RWJ(),
        ],
      },
    ],
  };
  return items;
}

function loadDataDokterPenindakPoli_RWJ(param) {
  if (param === "" || param === undefined) {
    param = {
      text: "0",
    };
  }
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getDokterPoli",
    params: {
      kd_unit: param,
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      cboDokterPenindak_RWJ.store.removeAll();
      var cst = Ext.decode(o.responseText);

      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = dsDokterPenindak_RWJ.recordType;
        var o = cst["listData"][i];

        recs.push(new recType(o));
        dsDokterPenindak_RWJ.add(recs);
      }
    },
  });
}

function comboDokterPenindak_RWJ() {
  var Field = ["kd_dokter", "nama"];
  dsDokterPenindak_RWJ = new WebApp.DataStore({ fields: Field });
  loadDataDokterPenindakPoli_RWJ();
  cboDokterPenindak_RWJ = new Ext.form.ComboBox({
    x: 80,
    y: 10,
    id: "cboDokterPenindak_RWJ",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    width: 250,
    store: dsDokterPenindak_RWJ,
    valueField: "kd_dokter",
    displayField: "nama",
    listeners: {
      select: function (a, b, c) {},
    },
  });
  return cboDokterPenindak_RWJ;
}

function updateDokterPenindakPoli_RWJ(rowdata_AG) {
  if (Ext.getCmp("cboDokterPenindak_RWJ").getValue() != "") {
    Ext.Ajax.request({
      url: baseURL + "index.php/main/functionRWJ/updateDokterPenindak",
      params: {
        kd_pasien: rowdata_AG.KD_PASIEN,
        kd_unit: rowdata_AG.KD_UNIT,
        urut_masuk: rowdata_AG.URUT_MASUK,
        tgl_masuk: rowdata_AG.TANGGAL_TRANSAKSI,
        kd_dokter: Ext.getCmp("cboDokterPenindak_RWJ").getValue(),
      },
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin!", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);

        if (cst.success === true) {
          Ext.Msg.show({
            title: "Information",
            width: 300,
            msg: "Simpan dokter penindak berhasil",
            buttons: Ext.MessageBox.OK,
            fn: function (btn) {
              if (btn == "ok") {
                setLookUpPilihDokterPenindak_RWJ.close();
                RWJLookUp(rowdata_AG);
                //refreshDataDepanPenjasRWJ();
                //refeshkasirrwj();
              }
            },
            icon: Ext.MessageBox.QUESTION,
          });
          //ShowPesanInfoRWJ('Simpan dokter penindak berhasil','Information')
        } else {
          ShowPesanErrorRWJ("Gagal menyimpan dokter penindak", "Error");
        }
      },
    });
  } else {
    ShowPesanWarningRWJ("Pilih dokter penindak untuk melanjutkan!", "Warning");
  }
}

function LookupLastHistoryDiagnosa_RWJ() {
  setLookUpLastHistoryDiagnosa_RWJ = new Ext.Window({
    id: "setLookUpLastHistoryDiagnosa_RWJ",
    name: "setLookUpLastHistoryDiagnosa_RWJ",
    title: "History diagnosa pasien",
    closeAction: "destroy",
    width: 465,
    height: 185,
    resizable: false,
    autoScroll: false,
    border: true,
    constrain: true,
    iconCls: "Studi_Lanjut",
    modal: true,
    items: [PanelGridLastHistoryDiagnosa_RWJ()],
    listeners: {
      activate: function () {},
      afterShow: function () {
        this.activate();
      },
      deactivate: function () {},
    },
  });

  setLookUpLastHistoryDiagnosa_RWJ.show();
  viewGridLastHistoryDiagnosa_RWJ();
}

function PanelGridLastHistoryDiagnosa_RWJ() {
  var fldDetail = ["PENYAKIT", "status_diag", "kasuss", "KD_PENYAKIT"];
  dsLastHistoryDiagnosa_RWJ = new WebApp.DataStore({ fields: fldDetail });

  PenataJasaRJ.gridLastHistoryDiagnosa = new Ext.grid.EditorGridPanel({
    stripeRows: true,
    id: "GridLastHistoryDiagnosa_RWJ",
    store: dsLastHistoryDiagnosa_RWJ,
    border: true,
    columnLines: true,
    frame: false,
    autoScroll: true,
    height: 153,
    width: 451,
    anchor: "100%",
    sm: new Ext.grid.CellSelectionModel({
      singleSelect: true,
      listeners: {
        cellselect: function (sm, row, rec) {
          /* cellSelectedriwayatkunjunganpasien = dsTRRiwayatDiagnosa_RWJ.getAt(row);
                        CurrentSelectedRiwayatKunjunanPasien.row = row;
                        CurrentSelectedRiwayatKunjunanPasien.data = cellSelectedriwayatkunjunganpasien; */
        },
      },
    }),
    cm: DetailGridLastHistoryDiagnosaColumModel_RWJ(),
    viewConfig: { forceFit: true },
  });
  return PenataJasaRJ.gridLastHistoryDiagnosa;
}
function DetailGridLastHistoryDiagnosaColumModel_RWJ() {
  return new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    {
      header: "Penyakit",
      dataIndex: "PENYAKIT",
      menuDisabled: true,
      width: 120,
    },
    {
      header: "Status diagnosa",
      dataIndex: "status_diag",
      width: 50,
    },
    {
      header: "Kasus",
      dataIndex: "kasuss",
      width: 40,
    },
    {
      header: "kd_penyakit",
      dataIndex: "KD_PENYAKIT",
      hidden: true,
    },
  ]);
}

function viewGridLastHistoryDiagnosa_RWJ() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgridlasthistorydiagnosa",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsLastHistoryDiagnosa_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsLastHistoryDiagnosa_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsLastHistoryDiagnosa_RWJ.add(recs);
        PenataJasaRJ.gridLastHistoryDiagnosa.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca history diagnosa", "Error");
      }
    },
  });
}

function GetgridEditDokterPenindakJasa_RWJ(kd_produk, kd_tarif) {
  console.log(kd_produk);
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgrideditjasadokterpenindak",
    params: {
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      urut: currentJasaDokterUrutDetailTransaksi_RWJ,
      kd_kasir: currentKdKasirRWJ,
      no_transaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
      tgl_transaksi: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
      kd_tarif: kd_tarif,
      kd_produk: kd_produk,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsGridJasaDokterPenindak_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        if (cst.totalrecords > 0) {
          var recs = [],
            recType = dsGridJasaDokterPenindak_RWJ.recordType;
          for (var i = 0; i < cst.ListDataObj.length; i++) {
            recs.push(new recType(cst.ListDataObj[i]));
          }
          dsGridJasaDokterPenindak_RWJ.add(recs);
          GridDokterTr_RWJ.getView().refresh();
        } else {
          GetgridPilihDokterPenindakJasa_RWJ(
            currentJasaDokterKdProduk_RWJ,
            currentJasaDokterKdTarif_RWJ
          );
        }
      } else {
        ShowPesanErrorRWJ("Gagal membaca history diagnosa", "Error");
      }
    },
  });
}

function SimpanJasaDokterPenindak(kd_produk, kd_tarif, urut, harga) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/savejasadokterpenindak",
    params: getParamsJasaDokterPenindak(urut, harga),
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      //dsGridJasaDokterPenindak_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        ShowPesanInfoRWJ("Dokter penindak berhasil disimpan", "Information");
        GetgridEditDokterPenindakJasa_RWJ(
          currentJasaDokterKdProduk_RWJ,
          currentJasaDokterKdTarif_RWJ
        );
      } else {
        ShowPesanErrorRWJ("Gagal membaca history diagnosa", "Error");
      }
    },
  });
}

function getParamsJasaDokterPenindak(urut, harga) {
  var params = {
    kd_kasir: currentKdKasirRWJ,
    no_transaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    tgl_transaksi: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    urut: urut,
    harga: harga,
  };
  params["jumlah"] = dsGridJasaDokterPenindak_RWJ.getCount();
  for (var i = 0; i < dsGridJasaDokterPenindak_RWJ.getCount(); i++) {
    params["kd_component-" + i] =
      dsGridJasaDokterPenindak_RWJ.data.items[i].data.kd_component;
    params["kd_dokter-" + i] =
      dsGridJasaDokterPenindak_RWJ.data.items[i].data.kd_dokter;
  }
  return params;
}

function loaddatagridpilihdokterpenindak_RWJ(param) {
  if (param === "" || param === undefined) {
    param = {
      text: "0",
      // parameter untuk url yang dituju (fungsi didalam controller)
    };
  }
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
    params: param,
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      gridcbopilihdokterpenindak_RWJ.store.removeAll();
      var cst = Ext.decode(o.responseText);

      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = dsgridpilihdokterpenindak_RWJ.recordType;
        var o = cst["listData"][i];

        recs.push(new recType(o));
        dsgridpilihdokterpenindak_RWJ.add(recs);
      }
    },
  });
}

function cekKomponen(kd_produk, kd_tarif, kd_unit, urut, harga, kondisi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/cekKomponen",
    params: {
      kd_produk: kd_produk,
      kd_tarif: kd_tarif,
      kd_unit: kd_unit,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        if (cst.komponen > 0) {
          currentJasaDokterKdTarif_RWJ = kd_tarif;
          currentJasaDokterKdProduk_RWJ = kd_produk;
          currentJasaDokterUrutDetailTransaksi_RWJ = urut;
          currentJasaDokterHargaJP_RWJ = harga;
          PilihDokterLookUp_RWJ(false, kondisi);
        }
      } else {
        ShowPesanErrorRWJ("Gagal cek komponen pelayanan", "Error");
      }
    },
  });
}

function GetgridPilihDokterPenindakJasa_RWJ(kd_produk, kd_tarif) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
    params: {
      kd_produk: kd_produk,
      kd_tarif: kd_tarif,
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      dsGridJasaDokterPenindak_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        var recs = [],
          recType = dsGridJasaDokterPenindak_RWJ.recordType;
        for (var i = 0; i < cst.ListDataObj.length; i++) {
          recs.push(new recType(cst.ListDataObj[i]));
        }
        dsGridJasaDokterPenindak_RWJ.add(recs);
        GridDokterTr_RWJ.getView().refresh();
      } else {
        ShowPesanErrorRWJ("Gagal membaca history diagnosa", "Error");
      }
    },
  });
}

function savetransaksi(kondisi) {
  var e = false;
  if (PenataJasaRJ.dsGridTindakan.getRange().length > 0) {
    for (
      var i = 0, iLen = PenataJasaRJ.dsGridTindakan.getRange().length;
      i < iLen;
      i++
    ) {
      var o = PenataJasaRJ.dsGridTindakan.getRange()[i].data;
      if (o.QTY == "" || o.QTY == 0 || o.QTY == null) {
        PenataJasaRJ.alertError(
          'Tindakan Yang Diberikan : "Qty" Pada Baris Ke-' +
            (i + 1) +
            ", Wajib Diisi.",
          "Peringatan"
        );
        e = true;
        break;
      }
    }
  } else {
    PenataJasaRJ.alertError("Isi Tindakan Yang Diberikan", "Peringatan");
    e = true;
  }
  var listObat = {};
  for (
    var i = 0, iLen = PenataJasaRJ.dsGridObat.getRange().length;
    i < iLen;
    i++
  ) {
    var o = PenataJasaRJ.dsGridObat.getRange()[i].data;
    if (listObat[o.kd_prd] !== undefined) {
      PenataJasaRJ.alertError(
        'Terapi Obat : "' + o.nama_obat + '" Tidak Boleh Sama.',
        "Peringatan"
      );
      e = true;
      break;
    }
    if (o.nama_obat == "" || o.nama_obat == null) {
      PenataJasaRJ.alertError(
        'Terapi Obat : "Nama Obat" Pada Baris Ke-' + (i + 1) + ", Wajib Diisi.",
        "Peringatan"
      );
      e = true;
      break;
    }
    if (o.jumlah == "" || o.jumlah == 0 || o.jumlah == null) {
      PenataJasaRJ.alertError(
        'Terapi Obat : "Qty" Pada Baris Ke-' + (i + 1) + ", Wajib Diisi.",
        "Peringatan"
      );
      e = true;
      break;
    }
    // if(o.cara_pakai == ''||  o.cara_pakai == null){
    // PenataJasaRJ.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
    // e=true;
    // break;
    // }
    if (o.verified == "" || o.verified == null) {
      PenataJasaRJ.alertError(
        'Terapi Obat : "Verified" Baris Ke-' + (i + 1) + ", Wajib Diisi.",
        "Peringatan"
      );
      e = true;
      break;
    }
    listObat[o.kd_prd] = true;
  }
  if (e == false) {
    Datasave_KasirRWJ(false, true, kondisi);
  }
}

/* ================================================INSERT SQL=========================================================== */
function Datasave_Diagnosa_SQL(mBol) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/rawat_jalan_sql/functionRWJ/saveDiagnosaPoliklinik",
    params: getParamDetailTransaksiDiagnosa2(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else if (cst.success === false && cst.pesan === 0) {
        ShowPesanWarningDiagnosa("Error simpan SQL", nmHeaderSimpanData);
      } else {
        ShowPesanErrorDiagnosa("Error simpan SQL", nmHeaderSimpanData);
      }
    },
  });
}

function DataDeleteDiagnosaDetail_SQL() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/DeleteDataObj",
    params: getParamDataDeleteDiagnosaDetail_SQL(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanWarningDiagnosa("Error delete SQL", nmHeaderHapusData);
      }
    },
  });
}

function getParamDataDeleteDiagnosaDetail_SQL() {
  var params = {
    Table: "SQLViewDiagnosa",
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    TglMasuk: CurrentDiagnosa.data.data.TGL_MASUK,
    KdPenyakit: CurrentDiagnosa.data.data.KD_PENYAKIT,
    UrutMasuk: CurrentDiagnosa.data.data.URUT_MASUK,
    Urut: CurrentDiagnosa.data.data.URUT,
  };
  return params;
}

function datasave_TrPenJasRWJ_SQL() {
  if (ValidasiEntryIcd9_RWJ(nmHeaderSimpanData, false) == 1) {
    Ext.Ajax.request({
      url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/saveIcd9",
      params: getParamIcd9RWJ(),
      failure: function (o) {
        ShowPesanErrorRWJ("Hubungi Admin", "Error");
      },
      success: function (o) {
        var cst = Ext.decode(o.responseText);
        if (cst.success === true) {
        } else {
          ShowPesanErrorRWJ("Gagal Menyimpan SQL Tindakan", "Error");
        }
      },
    });
  }
}

function hapusICD9_SQL(kd_icd9, urut) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/hapusBarisGridIcd",
    params: {
      kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      tgl_masuk: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
      urut_masuk: Ext.getCmp("txtKdUrutMasuk").getValue(),
      no_transaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
      kd_kasir: currentKdKasirRWJ,
      kd_icd9: kd_icd9,
      urut: urut,
    },
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanErrorRWJ("Gagal menghapus tindakan SQL", "Error");
      }
    },
  });
}

function Datasave_KasirRWJ_SQL(id_mrresep, jasa, kondisi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savedetailpenyakit",
    params: getParamDetailTransaksiRWJ_SQL(id_mrresep, kondisi),
    failure: function (o) {
      ShowPesanWarningRWJ("Error SQL hubungi admin!", "Gagal");
    },
    success: function (o) {
      RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        if (jasa == true) {
          SimpanJasaDokterPenindak_SQL(
            currentJasaDokterKdProduk_RWJ,
            currentJasaDokterKdTarif_RWJ,
            currentJasaDokterUrutDetailTransaksi_RWJ,
            currentJasaDokterHargaJP_RWJ
          );
        }
      } else {
        ShowPesanWarningRWJ(
          "Data SQL Tidak berhasil disimpan hubungi admin",
          "Gagal"
        );
      }
    },
  });
}

function getParamDetailTransaksiRWJ_SQL(id_mrresep, kondisi) {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnit: Ext.get("txtKdUnitRWJ").getValue(),
    kondisinya: kondisi,
    kdDokter: Ext.get("txtKdDokter").getValue(),
    Tgl: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
    Shift: tampungshiftsekarang,
    List: getArrDetailTrRWJ(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: GetListCountDetailTransaksi(),
    Hapus: 1,
    Ubah: 0,
    id_mrresep: id_mrresep,
  };
  params.jmlObat = PenataJasaRJ.dsGridObat.getRange().length;
  params.urut_masuk = PenataJasaRJ.s1.data.URUT_MASUK;
  params.kd_pasien = PenataJasaRJ.s1.data.KD_PASIEN;
  if (PenataJasaRJ.dsGridObat.getCount() > 0) {
    for (var i = 0, iLen = params.jmlObat; i < iLen; i++) {
      var o = PenataJasaRJ.dsGridObat.getRange()[i].data;
      params["kd_prd" + i] = o.kd_prd;
      params["jumlah" + i] = o.jumlah;
      params["cara_pakai" + i] = o.cara_pakai;
      params["verified" + i] = o.verified;
      params["racikan" + i] = o.racikan;
      params["kd_dokter" + i] = o.kd_dokter;
      params["urut" + i] = o.urut;
    }
    params["resep"] = true;
  } else {
    params["resep"] = false;
  }
  return params;
}

function SimpanJasaDokterPenindak_SQL(kd_produk, kd_tarif, urut, harga) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/rawat_jalan_sql/functionRWJ/savejasadokterpenindak",
    params: getParamsJasaDokterPenindak(urut, harga),
    failure: function (o) {
      ShowPesanErrorRWJ("Hubungi Admin", "Error");
    },
    success: function (o) {
      //dsGridJasaDokterPenindak_RWJ.removeAll();
      var cst = Ext.decode(o.responseText);

      if (cst.success === true) {
        ShowPesanInfoRWJ("Dokter penindak berhasil disimpan", "Information");
        GetgridEditDokterPenindakJasa_RWJ();
      } else {
        ShowPesanErrorRWJ("Gagal membaca history diagnosa", "Error");
      }
    },
  });
}

function saveRujukanLab_SQL(no_transaksi) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/rawat_jalan_sql/functionLABPoliklinik/savedetaillab",
    params: getParamDetailTransaksiLAB_SQL(no_transaksi),
    failure: function (o) {
      ShowPesanWarningRWJ(
        "SQL Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
      //PenataJasaRJ.var_kd_dokter_leb="";
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else if (cst.success === false && cst.cari === false) {
        ShowPesanWarningRWJ(
          "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya - SQL",
          "Gagal"
        );
      } else {
        //PenataJasaRJ.var_kd_dokter_leb="";
        ShowPesanWarningRWJ(
          "Data Tidak berhasil disimpan hubungi admin  - SQL",
          "Gagal"
        );
      }
    },
  });
}

function getParamDetailTransaksiLAB_SQL(no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/getcurrentshift",
    params: {
      command: "0",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      tampungshiftsekarang = o.responseText;
    },
  });
  var params = {
    KdPasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    KdDokter: PenataJasaRJ.var_kd_dokter_leb,
    KdDokter_mr_tindakan: Ext.get("txtKdDokter").dom.value,
    kd_kasir: "default_kd_kasir_rwj",
    Modul: "rwj",
    Tgl: "",
    TglTransaksiAsal: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    KdCusto: vkode_customer,
    TmpCustoLama: "",
    Shift: tampungshiftsekarang,
    List: getArrPoliLab(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: PenataJasaRJ.ds3.getCount(),
    unit: 41,
    TmpNotransaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    newnotrans: no_transaksi,
    KdKasirAsal: "01",
    KdSpesial: "",
    Kamar: "",
    NoSJP: "",
    pasienBaru: 0,
    listTrDokter: [],
    unitaktif: "rwj",
  };
  return params;
}

function saveRujukanRad_SQL(no_transaksi) {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/rawat_jalan_sql/functionRADPoliklinik/savedetailrad",
    params: getParamDetailTransaksiRAD_SQL(no_transaksi),
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Data Tidak berhasil disimpan hubungi admin",
        "Gagal"
      );
      PenataJasaRJ.var_kd_dokter_rad = "";
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");
        var o = gridPenataJasaTabItemTransaksiRWJ
          .getSelectionModel()
          .getSelections()[0].data;
        var par =
          " A.kd_pasien=~" +
          o.KD_PASIEN +
          "~ AND A.kd_unit=~" +
          o.KD_UNIT +
          "~ AND tgl_masuk=~" +
          o.TANGGAL_TRANSAKSI +
          "~ AND urut_masuk=" +
          o.URUT_MASUK;
        //	ViewGridBawahpoliLab(o.KD_PASIEN);
        ViewGridBawahpoliRad(
          o.NO_TRANSAKSI,
          Ext.getCmp("txtKdUnitRWJ").getValue(),
          o.TANGGAL_TRANSAKSI,
          o.URUT_MASUK
        );
      } else if (cst.success === false && cst.cari === false) {
        //PenataJasaRJ.var_kd_dokter_rad="";
        ShowPesanWarningRWJ(
          "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya",
          "Gagal"
        );
      } else {
        //PenataJasaRJ.var_kd_dokter_rad="";
        ShowPesanWarningRWJ(
          "Data Tidak berhasil disimpan hubungi admin",
          "Gagal"
        );
      }
    },
  });
}

function getParamDetailTransaksiRAD_SQL(no_transaksi) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/getcurrentshift",
    params: {
      command: "0",
    },
    failure: function (o) {
      var cst = Ext.decode(o.responseText);
    },
    success: function (o) {
      tampungshiftsekarang = o.responseText;
    },
  });
  var params = {
    newnotrans: no_transaksi,
    KdPasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
    KdUnit: Ext.getCmp("txtKdUnitRWJ").getValue(),
    KdDokter: PenataJasaRJ.var_kd_dokter_rad,
    KdDokter_mr_tindakan: Ext.get("txtKdDokter").dom.value,
    kd_kasir: "default_kd_kasir_rwj",
    Modul: "rwj",
    Tgl: "",
    TglTransaksiAsal: Ext.getCmp("dtpTanggalDetransaksi").getValue(),
    KdCusto: vkode_customer,
    TmpCustoLama: "",
    Shift: tampungshiftsekarang,
    Modul: "rwj",
    List: getArrPoliRad(),
    JmlField: mRecordRwj.prototype.fields.length - 4,
    JmlList: PenataJasaRJ.ds3.getCount(),
    unit: 5,
    TmpNotransaksi: Ext.getCmp("txtNoTransaksiKasirrwj").getValue(),
    KdKasirAsal: "01",
    KdSpesial: "",
    Kamar: "",
    NoSJP: "",
    pasienBaru: 0,
    listTrDokter: [],
    unitaktif: "rwj",
  };
  return params;
}

function hapusBarisGridResepOnline_SQL(kd_prd, id_mrresep, urut) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/hapusBarisGridObat",
    params: { kd_prd: kd_prd, id_mrresep: id_mrresep, urut: urut },
    failure: function (o) {
      ShowPesanErrorRWJ("SQL, Hubungi Admin", "Error");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanErrorRWJ("SQL, Gagal melakukan penghapusan!", "Error");
      }
    },
  });
}

function deleteKasirRWJDetail_SQL() {
  Ext.Ajax.request({
    url:
      baseURL + "index.php/rawat_jalan_sql/functionRWJ/DeleteDataKasirDetail",
    params: getParamDataDeleteKasirRWJDetail(),
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanWarningRWJ(
          "SQL, Gagal menghapus data ini!",
          nmHeaderHapusData
        );
      }
    },
  });
}

function DataSaveKamarOperasi_SQL(mBol) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/save_order_mng",
    params: getParamok(),
    failure: function (o) {
      ShowPesanWarningRWJ("SQL, Hubungi admin!", "Gagal");
    },
    success: function (o) {
      RefreshDataKasirRWJDetail(Ext.get("txtNoTransaksiKasirrwj").dom.value);
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoRWJ("Data berhasil disimpan", "Information");
        //RefreshDataFilterKasirRWJ();
        var paramresep = {
          KD_PASIEN: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
          KD_UNIT: Ext.getCmp("txtKdUnitRWJ").getValue(),
          TANGGAL_TRANSAKSI: Ext.get("dtpTanggalDetransaksi").getValue(),
        };
        RefreshDataKasirRWJDetai2(paramresep);
        if (mBol === false) {
          RefreshDataKasirRWJDetail(
            Ext.get("txtNoTransaksiKasirrwj").dom.value
          );
        }
      } else if (cst.success === false && cst.cari === true) {
        ShowPesanWarningRWJ(
          "Pasien telah terjadwal di Kamar operasi ",
          "Gagal"
        );
      } else {
        ShowPesanWarningRWJ(
          "SQL, Data Tidak berhasil disimpan hubungi admin",
          "Gagal"
        );
      }
    },
  });
}

function Datasave_Konsultasi_SQL(no_transaksi_konsul) {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/KonsultasiPenataJasa",
    params: getParamKonsultasi_SQL(no_transaksi_konsul),
    failure: function (o) {
      ShowPesanWarningRWJ(
        "SQL, Error konsultasi ulang. Hubungi admin!",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        ShowPesanWarningRWJ("Konsultasi ulang gagal", "Gagal");
      }
    },
  });
}

function getParamKonsultasi_SQL(no_transaksi_konsul) {
  var params = {
    Table: "ViewTrKasirRwj",
    TrKodeTranskasi: Ext.get("txtNoTransaksiKasirrwj").getValue(),
    KdUnitAsal: Ext.get("txtKdUnitRWJ").getValue(),
    KdDokterAsal: Ext.get("txtKdDokter").getValue(),
    KdUnit: selectKlinikPoli,
    KdDokter: selectDokter,
    KdPasien: Ext.get("txtNoMedrecDetransaksi").getValue(),
    TglTransaksi: Ext.get("dtpTanggalDetransaksi").dom.value,
    KDCustomer: vkode_customer,
    new_no_transaksi_konsul: no_transaksi_konsul,
  };
  return params;
}

function GantiDokter_SQL() {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/gantidokter",
    params: getParamGantiDokter(),
    failure: function (o) {
      ShowPesanErrorRWJ("SQL, Error, Hubungi Admin!", "Ganti Dokter");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        /* FormDepanDokter.close();
				FormLookUpGantidokter.close(); */
      } else {
        ShowPesanErrorRWJ("SQL, Gagal ganti Dokter!", "Ganti Dokter");
      }
    },
  });
}

function Datasave_Kelompokpasien_SQL() {
  Ext.Ajax.request({
    url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/UpdateKdCustomer",
    params: getParamKelompokpasien(),
    failure: function (o) {
      ShowPesanWarningRWJ("SQL, Error ganti customer, Hubungi Admin!", "Gagal");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
      } else {
        //ShowPesanWarningRWJ('SQL, Simpan kelompok pasien gagal!', 'Gagal');
      }
    },
  });
}
function printPJasaRWJKartuPasien() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: dataPJasaRwjcetakKartuPasien(),
    failure: function (o) {
      Ext.MessageBox.alert("Data tidak berhasil di Cetak ", "Cetak Data");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        Ext.MessageBox.alert("Kartu Pasien Segera di Cetak", "Cetak Data");
      } else if (cst.success === false && cst.pesan === 0) {
        Ext.MessageBox.alert(
          "Data tidak berhasil di Cetak " + cst.pesan,
          "Cetak Data"
        );
      } else {
        Ext.MessageBox.alert(
          "Data tidak berhasil di Cetak " + cst.pesan,
          "Cetak Data"
        );
      }
    },
  });
}
function dataPJasaRwjcetakKartuPasien() {
  var paramskartupasienpendaftaran = {
    Table: "kartupasienprinting",
    NoMedrec: Ext.get("txtNoMedrecDetransaksi").getValue(),
    NamaPasien: "",
    Alamat: "",
    Poli: "",
    TanggalMasuk: "",
    KdDokter: "", //Ext.getCmp('cboDokterRequestEntry').getValue(),
  };
  return paramskartupasienpendaftaran;
}
function getParamsCekTindakan_PenjasRWJ(data) {
  var params = {
    kd_kasir: data.KD_KASIR,
    no_transaksi: data.NO_TRANSAKSI,
    tgl_transaksi: data.TANGGAL_TRANSAKSI,
    urut_masuk: data.URUT_MASUK,
    kd_pasien: data.KD_PASIEN,
    kd_unit: data.KD_UNIT,
  };
  return params;
}
function cekTransferTindakanPenjasRWJ(reload, data) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/cek_transfer_tindakan",
    params: getParamsCekTindakan_PenjasRWJ(data),
    failure: function (o) {
      ShowPesanWarningRWJ("Migrasi data detail transaksi gagal !", "WARNING");
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.status === true && reload === true) {
        RefreshDataKasirRWJDetail(cst.no_transaksi);
      } else {
        RefreshDataKasirRWJDetail(cst.no_transaksi);
      }
    },
  });
}
RefreshDataFilterKasirRWJ();

// HUDI
// 21-05-2024
// -------------------------------------------------------------
function PreviewHasilLabLookUp(value) {
  var lebar = 320;
  FormLookUpPreviewHasilLab = new Ext.Window({
    id: "gridPreviewHasilLab",
    title: "Preview Hasil Laboratorium",
    closeAction: "destroy",
    width: lebar,
    height: 180,
    border: false,
    resizable: false,
    plain: false,
    constrain: true,
    layout: "fit",
    iconCls: "Request",
    modal: true,
    items: getFormEntryPreviewHasilLab(lebar),
    fbar: [
      {
        xtype: "button",
        text: "Preview",
        width: 70,
        style: { "margin-left": "0px", "margin-top": "0px" },
        hideLabel: true,
        id: "btnOkPreviewHasilLab",
        handler: function () {
          if(Ext.getCmp('cboNoTransaksiLab').getValue() != ''){
            var no_transaksi = Ext.getCmp('cboNoTransaksiLab').getValue();
            var kd_pasien = Ext.getCmp('txtKdPasien_PreviewHasilLab').getValue();
            var tgl_masuk = Ext.getCmp('txtTglTransaksi_PreviewHasilLab').getValue();
            var url_preview = baseURL+"index.php/main/cetaklaporanRadLab/Laporan_Laboratorium_preview";
            PreviewHasilLab(url_preview, no_transaksi, kd_pasien, tgl_masuk);
          }else{
            ShowPesanErrorRWJ("No Transaksi belum dipilih.", "Preview Error");
          }
        },
      },
      {
        xtype: "button",
        text: "Tutup",
        width: 70,
        hideLabel: true,
        id: "btnCancelPreviewHasilLab",
        handler: function () {
          FormLookUpPreviewHasilLab.close();
        },
      },
    ],
    listeners: {},
  });
  FormLookUpPreviewHasilLab.show();
  // console.log(value);
  Ext.getCmp('txtKdPasien_PreviewHasilLab').setValue(value.KD_PASIEN);
  Ext.getCmp("txtTglTransaksi_PreviewHasilLab").setValue(ShowDate(value.TANGGAL_TRANSAKSI));
}

function getFormEntryPreviewHasilLab(lebar) {
  var pnlPreviewHasilLab = new Ext.FormPanel({
    id: "PanelTRKelompokPasien",
    fileUpload: true,
    region: "north",
    layout: "column",
    bodyStyle: "padding:10px 10px 10px 10px",
    height: 250,
    anchor: "100%",
    width: lebar,
    border: false,
    items: [
      getItemPanelPreviewHasilLab(lebar)
    ],
  });
  var FormDepanPreviewHasilLab = new Ext.Panel({
    id: "FormDepanKelompokPasien",
    region: "center",
    width: "100%",
    anchor: "100%",
    layout: "form",
    title: "",
    bodyStyle: "padding:15px",
    border: true,
    bodyStyle: "background:#FFFFFF;",
    shadhow: true,
    items: [pnlPreviewHasilLab],
  });
  return FormDepanPreviewHasilLab;
}

function getItemPanelPreviewHasilLab(lebar) {
  var items = {
    layout: "fit",
    anchor: "100%",
    width: lebar - 35,
    labelAlign: "right",
    bodyStyle: "padding:10px 10px 10px 0px",
    border: false,
    height: 170,
    items: [
      {
        // columnWidth: 0.7,
        width: lebar - 35,
        labelWidth: 100,
        layout: "form",
        border: false,
        items: [
          // getKelompokpasienlama(lebar),
          getItemPanelPreviewHasilLaboratorium(lebar),
        ],
      },
    ],
  };
  return items;
}


function getItemPanelPreviewHasilLaboratorium(lebar) {
  var items = {
    Width: lebar,
    height: 120,
    layout: "column",
    border: false,
    items: [
      {
        // columnWidth: 0.7,
        layout: "form",
        Width: lebar - 10,
        labelWidth: 100,
        border: false,
        items: [
          {
            xtype: "tbspacer",
            height: 3,
          },
          mComboNoTransaksiLab(),
          {
            xtype: "textfield",
            fieldLabel: "Kd pasien",
            maxLength: 200,
            name: "txtKdPasien_PreviewHasilLab",
            id: "txtKdPasien_PreviewHasilLab",
            width: 100,
            anchor: "95%",
          },
          {
            xtype: "textfield",
            id: "txtTglTransaksi_PreviewHasilLab",
            name: "txtTglTransaksi_PreviewHasilLab",
            fieldLabel: "Tgl Transaksi",
            maxLength: 200,
            width: 100,
            anchor: "95%",
          }
        ],
      },
    ],
  };
  return items;
}

function mComboNoTransaksiLab() {
  var strKriteriaGetNoTransaksiLab = "";
  strKriteriaGetNoTransaksiLab =
    "kd_pasien = ~" +
    Ext.get("txtNoMedrecDetransaksi").getValue() +
    "~ and kd_unit=~" +
    41 +
    "~ and tgl_transaksi = ~" +
    Ext.get("dtpTanggalDetransaksi").dom.value +
    "~";
  var Field = ["DATA_VALUE", "DATA_NAME"];
  dsNoTransaksiLaboratoriumEntry = new WebApp.DataStore({ fields: Field });
  dsNoTransaksiLaboratoriumEntry.load({
    params: {
      Skip: 0,
      Take: 1000,
      Sort: "",
      Sortdir: "ASC",
      target: "ViewNoTransaksiLab",
      param: strKriteriaGetNoTransaksiLab,
    },
  });

  var cboNoTransaksiLab = new Ext.form.ComboBox({
    id: "cboNoTransaksiLab",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    selectOnFocus: true,
    forceSelection: true,
    emptyText: "",
    labelWidth: 80,
    fieldLabel: "No Transaksi Lab ",
    align: "Left",
    store: dsNoTransaksiLaboratoriumEntry,
    valueField: "DATA_VALUE",
    displayField: "DATA_NAME",
    anchor: "95%",
    listeners: {
      select: function (a, b, c) {
        selectDokter = b.data.KD_DOKTER;
      },
      render: function (c) {
        c.getEl().on(
          "keypress",
          function (e) {
            if (e.getKey() == 13)
              // atau
              Ext.getCmp("kelPasien").focus();
          },
          c
        );
      },
    },
  });
  return cboNoTransaksiLab;
}

function PreviewHasilLab(url, no_transaksi, kd_pasien, tgl_masuk) {
  /* var params = {
          Tgl: Ext.get('dPopupTglMasuk').getValue(),
          KdPasien: Ext.get('TxtPopupMedrec').getValue(),
          Nama: Ext.get('TxtPopupNamaPasien').getValue(),        
          JenisKelamin: Ext.get('TxtJK').getValue(),
          Ttl:Ext.get('dPopupTglLahirPasien').getValue(),
          Umur:tmpumur,
          Alamat: Ext.get('TxtPopupAlamat').getValue(),
          Poli: Ext.get('TxtPopPoli').getValue(),
          urutmasuk: Ext.get('TxtUrutMasuk').getValue(),
          Dokter:Ext.get('TxtNmDokter').getValue(),
          NamaUnitAsal:tmpnama_unit_asal,
          KdDokterAsal:tmpkd_dokter_asal,
          JamMasuk:tmpjam_masuk,
          NamaDokterAsal:tmpnama_dokter_asal
      }; */

  var module_rawat = "RWJ";
  new Ext.Window({
    title: "Preview Hasil Laboratorium",
    width: 1000,
    height: 600,
    constrain: true,
    modal: true,
    // html: "<iframe  style ='width: 100%; height: 100%;' src='" + url + "/" + no_transaksi + "/" + kd_kasir + "'></iframe>",
    html: "<iframe  style ='width: 100%; height: 100%;' src='" + url + "/" + no_transaksi + "/" + kd_pasien + "/" + module_rawat + "'></iframe>",
    /* tbar: [
      {
        xtype: "button",
        text: "Cetak Direct",
        iconCls: "print",
        handler: function () {
          window.open(
            baseURL +
              "index.php/laporan/lap_billing/print_pdf/" +
              no_transaksi +
              "/" +
              kd_kasir,
            "_blank"
          );
        },
      },
    ], */
  }).show();
}
// --------------------------------------------------------------

/*
	UPDATE LOOKUP TINDAKAN 
	OLEH : HADAD AL GOJALI 
	2017 - 08 - 08
 */
function GetPodukLookUp(rowdata_AG) {
  LoadDataStoreProduk("", "", rowdata_AG);
  var chkgetTindakanPenjasRad = new Ext.grid.CheckColumn({
    xtype: "checkcolumn",
    width: 25,
    sortable: false,
    id: "check1",
    dataIndex: "checkProduk",
    editor: {
      xtype: "checkbox",
      cls: "x-grid-checkheader-editor",
    },
    listeners: {
      checkchange: function (column, recordIndex, checked) {},
    },
  });
  var GridTrDokterColumnModel = new Ext.grid.ColumnModel([
    new Ext.grid.RowNumberer(),
    /*
		{
			header			: 'KD PRODUK',
			dataIndex		: 'kd_produk',
			width			: 25,
			menuDisabled	: true,
			hidden 			: true
		},*/
    {
      header: "KP PRODUK",
      dataIndex: "kp_produk",
      id: "col_kp_produk",
      width: 15,
      menuDisabled: true,
      hidden: false,
      filter: {},
    },
    {
      header: "DESKRIPSI",
      dataIndex: "deskripsi",
      id: "col_deskripsi",
      width: 65,
      menuDisabled: true,
      hidden: false,
      filter: {},
    },
    {
      header: "TARIF",
      dataIndex: "tarifx",
      width: 35,
      menuDisabled: true,
      hidden: false,
    },
    chkgetTindakanPenjasRad,
  ]);
  GridDokterTr_RWI = new Ext.grid.EditorGridPanel({
    id: "GridDokterTr_RWI",
    stripeRows: true,
    width: "100%",
    height: "100%",
    store: dsDataStoreGridPoduk,
    border: true,
    frame: false,
    autoScroll: true,
    plugins: [new Ext.ux.grid.FilterRow()],
    selModel: new Ext.grid.RowSelectionModel({
      singleSelect: true,
      listeners: {},
    }),
    cm: GridTrDokterColumnModel,
    listeners: {
      rowclick: function ($this, rowIndex, e) {
        //trcellCurrentTindakan_RWJ = rowIndex;
      },
      celldblclick: function (
        gridView,
        htmlElement,
        columnIndex,
        dataRecord
      ) {},
    },
    viewConfig: { forceFit: true },
  });
  var lebar = 500;
  FormLookupProduk = new Ext.Window({
    id: "getProdukLookup",
    title: "Daftar produk",
    closeAction: "destroy",
    width: lebar,
    height: 260,
    border: false,
    resizable: true,
    plain: false,
    constrain: true,
    layout: "fit",
    iconCls: "Request",
    modal: true,
    items: GridDokterTr_RWI,
    fbar: [
      {
        xtype: "button",
        text: "Simpan",
        width: 70,
        style: { "margin-left": "0px", "margin-top": "0px" },
        hideLabel: true,
        id: "btnGetProduk",
        handler: function () {
          console.log(rowdata_AG);
          if (rowdata_AG == undefined) {
            var linePJasaRWJ = PenataJasaRJ.form.Grid.produk.store.data.length;
            for (var i = 0; i < dsDataStoreGridPoduk.getCount(); i++) {
              if (dsDataStoreGridPoduk.data.items[i].data.checkProduk == true) {
                tmp_data_produk = dsDataStoreGridPoduk.data.items[i].data;
                var status_produk = false;
                var index_produk = linePJasaRWJ;
                for (var j = 0; j < dsTRDetailKasirRWJList.totalLength; j++) {
                  if (
                    dsTRDetailKasirRWJList.data.items[j].data.KD_PRODUK ==
                    tmp_data_produk.kd_produk
                  ) {
                    status_produk = true;
                    index_produk = j;
                    break;
                  } else {
                    status_produk = false;
                    index_produk = linePJasaRWJ;
                  }
                }
                if (status_produk == false) {
                  PenataJasaRJ.dsGridTindakan.insert(
                    PenataJasaRJ.dsGridTindakan.getCount(),
                    PenataJasaRJ.func.getNullProduk()
                  );
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("KP_PRODUK", tmp_data_produk.kp_produk);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("KD_PRODUK", tmp_data_produk.kd_produk);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("DESKRIPSI", tmp_data_produk.deskripsi);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set(
                      "TGL_BERLAKU",
                      tmp_data_produk.tgl_berlaku
                    );
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("KD_KLAS", tmp_data_produk.KD_KLAS);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("HARGA", tmp_data_produk.tarifx);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("KD_TARIF", tmp_data_produk.kd_tarif);
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set(
                      "STATUS_KONSULTASI",
                      tmp_data_produk.status_konsultasi
                    );
                  dsTRDetailKasirRWJList.getRange()[index_produk].set("QTY", 1);
                  linePJasaRWJ++;
                } else {
                  var tmp_qty = 0;
                  tmp_qty = dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].get("QTY");
                  tmp_qty = parseInt(tmp_qty) + 1;
                  dsTRDetailKasirRWJList
                    .getRange()
                    [index_produk].set("QTY", tmp_qty);
                }
              }
            }
            Datasave_KasirRWJ(false, false);
            FormLookupProduk.close();
          } else if (rowdata_AG == "LAB") {
            var linePJasaRWJ = PenataJasaRJ.grid3.store.data.length;
            for (var i = 0; i < dsDataStoreGridPoduk.getCount(); i++) {
              if (dsDataStoreGridPoduk.data.items[i].data.checkProduk == true) {
                tmp_data_produk = dsDataStoreGridPoduk.data.items[i].data;
                var status_produk = false,
                  index_produk = linePJasaRWJ;
                for (var j = 0; j < PenataJasaRJ.ds3.totalLength; j++) {
                  console.log(PenataJasaRJ.ds3.getRange()[j].data);
                  if (
                    PenataJasaRJ.ds3.getRange()[j].data.kd_produk ==
                    tmp_data_produk.kd_produk
                  ) {
                    status_produk = true;
                    index_produk = j;
                    break;
                  } else {
                    status_produk = false;
                    index_produk = linePJasaRWJ;
                  }
                }
                if (status_produk == false) {
                  PenataJasaRJ.ds3.insert(
                    PenataJasaRJ.ds3.getCount(),
                    PenataJasaRJ.func.getNullLab()
                  );
                  PenataJasaRJ.ds3.getRange()[index_produk].data.deskripsi =
                    tmp_data_produk.deskripsi;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.uraian =
                    tmp_data_produk.deskripsi;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.kd_unit =
                    tmp_data_produk.kd_unit;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.kd_tarif =
                    tmp_data_produk.kd_tarif;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.kd_produk =
                    tmp_data_produk.kd_produk;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.tgl_transaksi =
                    tanggaltransaksitampung;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.tgl_berlaku =
                    tmp_data_produk.tgl_berlaku;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.urut =
                    linePJasaRWJ + 1;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.harga =
                    tmp_data_produk.tarifx;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.qty = 1;
                  PenataJasaRJ.ds3.getRange()[index_produk].data.jumlah =
                    tmp_data_produk.tarifx;
                  PenataJasaRJ.grid3.getView().refresh();
                  linePJasaRWJ++;
                }
              }
            }
            if (PenataJasaRJ.ds3.getCount() == 0) {
              PenataJasaRJ.alertError(
                "Laboratorium: Harap isi data Labolatorium.",
                "Peringatan"
              );
            } else {
              var e = false;
              for (
                var i = 0, iLen = PenataJasaRJ.ds3.getCount();
                i < iLen;
                i++
              ) {
                if (
                  PenataJasaRJ.ds3.getRange()[i].data.kd_produk == "" ||
                  PenataJasaRJ.ds3.getRange()[i].data.kd_produk == null
                ) {
                  PenataJasaRJ.alertError(
                    "Laboratorium: Nilai normal item " +
                      PenataJasaRJ.ds3.getRange()[i].data.deskripsi +
                      " belum tersedia",
                    "Peringatan"
                  );
                  e = true;
                  break;
                }
              }
              if (e == false) {
                Ext.Ajax.request({
                  url:
                    baseURL +
                    "index.php/main/functionLABPoliklinik/savedetaillab",
                  params: getParamDetailTransaksiLAB(),
                  failure: function (o) {
                    ShowPesanWarningRWJ(
                      "Data Tidak berhasil disimpan hubungi admin",
                      "Gagal"
                    );
                    //PenataJasaRJ.var_kd_dokter_leb="";
                  },
                  success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) {
                      ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");
                      //saveRujukanLab_SQL(cst.notrans);
                      var o = gridPenataJasaTabItemTransaksiRWJ
                        .getSelectionModel()
                        .getSelections()[0].data;
                      var par =
                        " A.kd_pasien=~" +
                        o.KD_PASIEN +
                        "~ AND A.kd_unit=~" +
                        o.KD_UNIT +
                        "~ AND tgl_masuk=~" +
                        o.TANGGAL_TRANSAKSI +
                        "~ AND urut_masuk=" +
                        o.URUT_MASUK;
                      ViewGridBawahpoliLab(
                        o.NO_TRANSAKSI,
                        Ext.getCmp("txtKdUnitRWJ").getValue(),
                        o.TANGGAL_TRANSAKSI,
                        o.URUT_MASUK
                      );
                    } else if (cst.success === false && cst.cari === false) {
                      //PenataJasaRJ.var_kd_dokter_leb="";
                      ShowPesanWarningRWJ(
                        "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya",
                        "Gagal"
                      );
                    } else {
                      //PenataJasaRJ.var_kd_dokter_leb="";
                      ShowPesanWarningRWJ(
                        "Data Tidak berhasil disimpan hubungi admin",
                        "Gagal"
                      );
                    }
                  },
                });
              }
            }
            FormLookupProduk.close();
          } else if (rowdata_AG == "RAD") {
            var linePJasaRWJ = PenataJasaRJ.pj_req_rad.store.data.length;
            for (var i = 0; i < dsDataStoreGridPoduk.getCount(); i++) {
              if (dsDataStoreGridPoduk.data.items[i].data.checkProduk == true) {
                tmp_data_produk = dsDataStoreGridPoduk.data.items[i].data;
                var status_produk = false,
                  index_produk = linePJasaRWJ;
                for (var j = 0; j < dsRwJPJRAD.totalLength; j++) {
                  console.log(dsRwJPJRAD.getRange()[j].data);
                  if (
                    dsRwJPJRAD.getRange()[j].data.kd_produk ==
                    tmp_data_produk.kd_produk
                  ) {
                    status_produk = true;
                    index_produk = j;
                    break;
                  } else {
                    status_produk = false;
                    index_produk = linePJasaRWJ;
                  }
                }
                if (status_produk == false) {
                  dsRwJPJRAD.insert(
                    dsRwJPJRAD.getCount(),
                    PenataJasaRJ.func.getNullLab()
                  );
                  dsRwJPJRAD.getRange()[index_produk].data.deskripsi =
                    tmp_data_produk.deskripsi;
                  dsRwJPJRAD.getRange()[index_produk].data.uraian =
                    tmp_data_produk.deskripsi;
                  dsRwJPJRAD.getRange()[index_produk].data.kd_tarif =
                    tmp_data_produk.kd_tarif;
                  dsRwJPJRAD.getRange()[index_produk].data.kd_unit =
                    tmp_data_produk.kd_unit;
                  dsRwJPJRAD.getRange()[index_produk].data.kd_produk =
                    tmp_data_produk.kd_produk;
                  dsRwJPJRAD.getRange()[index_produk].data.tgl_transaksi =
                    tanggaltransaksitampung;
                  dsRwJPJRAD.getRange()[index_produk].data.tgl_berlaku =
                    tmp_data_produk.tgl_berlaku;
                  dsRwJPJRAD.getRange()[index_produk].data.urut =
                    linePJasaRWJ + 1;
                  dsRwJPJRAD.getRange()[index_produk].data.harga =
                    tmp_data_produk.tarifx;
                  dsRwJPJRAD.getRange()[index_produk].data.qty = 1;
                  dsRwJPJRAD.getRange()[index_produk].data.jumlah =
                    tmp_data_produk.tarifx;
                  PenataJasaRJ.pj_req_rad.getView().refresh();
                  linePJasaRWJ++;
                }
              }
            }
            if (dsRwJPJRAD.getCount() == 0) {
              PenataJasaRJ.alertError(
                "Radiologi: Harap isi data Radiologi.",
                "Peringatan"
              );
            } else {
              var e = false;
              for (var i = 0, iLen = dsRwJPJRAD.getCount(); i < iLen; i++) {
                if (
                  dsRwJPJRAD.getRange()[i].data.kd_produk == "" ||
                  dsRwJPJRAD.getRange()[i].data.kd_produk == null
                ) {
                  PenataJasaRJ.alertError(
                    "Radiologi: Nilai normal item " +
                      PenataJasaRJ.ds3.getRange()[i].data.deskripsi +
                      " belum tersedia",
                    "Peringatan"
                  );
                  e = true;
                  break;
                }
              }
              if (e == false) {
                // if (PenataJasaRJ.var_kd_dokter_rad==="" || PenataJasaRJ.var_kd_dokter_rad===undefined){
                // ShowPesanWarningRWJ('harap Isi salah satu dokter pada kolom dokter', 'Gagal');
                // }else{
                Ext.Ajax.request({
                  url:
                    baseURL +
                    "index.php/main/functionRADPoliklinik/savedetailrad",
                  params: getParamDetailTransaksiRAD(),
                  failure: function (o) {
                    ShowPesanWarningRWJ(
                      "Data Tidak berhasil disimpan hubungi admin",
                      "Gagal"
                    );
                    PenataJasaRJ.var_kd_dokter_rad = "";
                  },
                  success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) {
                      ShowPesanInfoDiagnosa("Data Berhasil Disimpan", "Info");
                      //saveRujukanRad_SQL(cst.NO_TRANS);
                      var o = gridPenataJasaTabItemTransaksiRWJ
                        .getSelectionModel()
                        .getSelections()[0].data;
                      var par =
                        " A.kd_pasien=~" +
                        o.KD_PASIEN +
                        "~ AND A.kd_unit=~" +
                        o.KD_UNIT +
                        "~ AND tgl_masuk=~" +
                        o.TANGGAL_TRANSAKSI +
                        "~ AND urut_masuk=" +
                        o.URUT_MASUK;
                      //	ViewGridBawahpoliLab(o.KD_PASIEN);
                      ViewGridBawahpoliRad(
                        o.NO_TRANSAKSI,
                        Ext.getCmp("txtKdUnitRWJ").getValue(),
                        o.TANGGAL_TRANSAKSI,
                        o.URUT_MASUK
                      );
                    } else if (cst.success === false && cst.cari === false) {
                      //PenataJasaRJ.var_kd_dokter_rad="";
                      ShowPesanWarningRWJ(
                        "Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya",
                        "Gagal"
                      );
                    } else {
                      //PenataJasaRJ.var_kd_dokter_rad="";
                      ShowPesanWarningRWJ(
                        "Data Tidak berhasil disimpan hubungi admin",
                        "Gagal"
                      );
                    }
                  },
                });
                // }
              }
            }
            FormLookupProduk.close();
          }
        },
      },
      {
        xtype: "button",
        text: "Tutup",
        width: 70,
        hideLabel: true,
        id: "btnCancelKelompokPasien",
        handler: function () {
          FormLookupProduk.close();
        },
      },
    ],
    listeners: {},
  });
  FormLookupProduk.show();
  FormLookupProduk.center();
}

function LoadDataStoreProduk(kp_produk, deskripsi, mod) {
  var Field = ["kd_produk", "deskripsi", "tarifx", "kp_produk"];
  DataStoreProduk_KasirRWJ = new WebApp.DataStore({ fields: Field });
  dsDataStoreGridPoduk.removeAll();
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getProdukList",
    params: {
      mod_id: TrKasirRWJ_AG.mod_id,
      modul: mod,
      kd_unit: Ext.getCmp("txtKdUnitRWJ").getValue(),
      customer: Ext.getCmp("txtCustomer").getValue(),
      kp_produk: kp_produk,
      deskripsi: deskripsi,
    },

    success: function (response) {
      //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
      var cst = Ext.decode(response.responseText);
      //dsDataPerawatPenindak_KASIR_RWI.load(myData);
      for (var i = 0, iLen = cst["data"].length; i < iLen; i++) {
        var recs = [],
          recType = DataStoreProduk_KasirRWJ.recordType;
        var o = cst["data"][i];
        recs.push(new recType(o));
        dsDataStoreGridPoduk.add(recs);
      }
    },
  });
}

function LoadDataUser() {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/functionRWJ/getDataUser",
    params: {
      kd_unit: null,
    },

    success: function (response) {
      var cst = Ext.decode(response.responseText);
      setting_tracer = cst.result_data[0].set_tracer;
      console.log(setting_tracer);
      if (setting_tracer == "0") {
        Ext.getCmp("cboStatusAntrian_viKasirRwj").enable();
        Ext.getCmp("cboStatusAntrian_viKasirRwj").setValue("Semua");
      } else {
        Ext.getCmp("cboStatusAntrian_viKasirRwj").setValue("Tunggu Berkas");
        Ext.getCmp("cboStatusAntrian_viKasirRwj").disable();
      }
      RefreshDataFilterKasirRWJ();
    },
  });
}

shortcut.set({
  code: "main",
  list: [
    {
      key: "f2",
      fn: function () {
        /*if (Ext.getCmp('cboUNIT_viKasirRwj').getValue()  == 'All') {
					ShowPesanWarningRWJ("Pilih dahulu poli tujuan", "Informasi");
				}else{
				}*/
        Ext.getCmp("btnScanBerkasRWJ").el.dom.click();
      },
    },
    {
      key: "f7",
      fn: function () {
        var tmpNoMedrec = Ext.get("txtFilterNomedrec").getValue();
        if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
          var tmpgetNoMedrec = formatnomedrec(
            Ext.get("txtFilterNomedrec").getValue()
          );
          Ext.getCmp("txtFilterNomedrec").setValue(tmpgetNoMedrec);
          // updateAntrianRWJ();
        }
        // var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue());
        // Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
        var url =
          baseURL +
          "index.php/rawat_jalan/cetaklabelpasien/cetak/" +
          Ext.get("txtFilterNomedrec").getValue();
        new Ext.Window({
          title: "Label Barcode",
          width: 900,
          height: 500,
          constrain: true,
          modal: true,
          html:
            "<iframe style='width: 100%; height: 100%;' src='" +
            url +
            "'></iframe>",
        }).show();
      },
    },
    {
      key: "esc",
      fn: function () {
        Ext.getCmp("txtFilterNomedrec").setValue("");
        Ext.getCmp("txtFilterNomedrec").focus();
      },
    },
  ],
});

function mComboStatusAntrian_viKasirRwj() {
  var cboStatusAntrian_viKasirRwj = new Ext.form.ComboBox({
    id: "cboStatusAntrian_viKasirRwj",
    typeAhead: true,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    fieldLabel: "Status Antrian",
    align: "Right",
    anchor: "100%",
    store: new Ext.data.ArrayStore({
      fields: ["id", "displayText"],
      data: [
        [0, "Semua"],
        //[1, 'Tunggu Berkas'],
        [2, "Tunggu Panggilan"],
        [3, "Sedang Dilayani"],
        [4, "Selesai"],
      ],
    }),
    valueField: "id",
    displayField: "displayText",
    value: "Semua",
    listeners: {
      select: function (a, b, c) {
        RefreshDataFilterKasirRWJ();
      },
    },
  });
  return cboStatusAntrian_viKasirRwj;
}

function SuratRawatLookUp_rwj(status) {
  var lebar = 440;
  FormLookUpSuratRawat = new Ext.Window({
    id: "gridSuratRawat",
    title: "Surat Rawat",
    closeAction: "destroy",
    width: lebar,
    border: false,
    height: 360,
    resizable: true,
    plain: true,
    layout: "fit",
    iconCls: "Request",
    modal: true,
    items: [getPanelSurat_rwj()],
    // getFormEntrySuratRawat_igd(lebar),
    listeners: {},
  });
  if (status == "Rawat Inap Ruangan" || status == "26" || status == 26) {
    FormLookUpSuratRawat.show();
  }
  // KelompokPasienbaru_igd();
}

function getPanelSurat_rwj() {
  var simple = new Ext.FormPanel({
    labelWidth: 75, // label settings here cascade unless overridden
    // url:'save-form.php',
    frame: false,
    bodyStyle: "padding:5px 5px 0",
    width: "100%",
    defaultType: "textfield",
    items: [
      comboDokterSpesial(),
      new Ext.form.TextField({
        id: "iTextNomerDokumen",
        fieldLabel: "Nomer&nbsp; ",
        width: "100%",
        readOnly: true,
      }),
      new Ext.form.TextArea({
        id: "iTextAreaDengan",
        fieldLabel: "Diagnosa &nbsp; ",
        width: "100%",
        autoScroll: true,
        hidden: false,
        height: 70,
      }),
      new Ext.form.TextArea({
        id: "iTextAreaTerapi",
        fieldLabel: "Rencana Tindakan &nbsp; ",
        width: "100%",
        autoScroll: true,
        hidden: false,
        height: 70,
      }),
      new Ext.form.TextArea({
        id: "iTextAreaTerapiDiberikan",
        fieldLabel: "Terapi yang diberikan &nbsp; ",
        width: "100%",
        autoScroll: true,
        hidden: false,
        height: 70,
      }),
    ],
    buttons: [
      {
        text: "print",
        handler: function (sm, row, rec) {
          var params = {
            no_transaksi: objRWJAG.NO_TRANSAKSI,
            kd_kasir: objRWJAG.KD_KASIR,
            no_dokumen: Ext.getCmp("iTextNomerDokumen").getValue(),
            kd_dokter_rwj: Ext.getCmp("txtKdDokter").getValue(),
            kd_dokter_rwi: Ext.getCmp("iComboDokterRWI").getValue(),
            kd_pasien: Ext.getCmp("txtNoMedrecDetransaksi").getValue(),
            dengan: Ext.getCmp("iTextAreaDengan").getValue(),
            terapi: Ext.getCmp("iTextAreaTerapi").getValue(),
            terapi_berikan: Ext.getCmp("iTextAreaTerapiDiberikan").getValue(),
          };

          var form = document.createElement("form");
          form.setAttribute("method", "post");
          form.setAttribute("target", "_blank");
          form.setAttribute(
            "action",
            baseURL + "index.php/general/surat_rawat/cetak"
          );
          // form.setAttribute("action", baseURL + "index.php/main/functionRWJ/cetakLab");
          var hiddenField = document.createElement("input");
          hiddenField.setAttribute("type", "hidden");
          hiddenField.setAttribute("name", "data");
          hiddenField.setAttribute("value", Ext.encode(params));
          form.appendChild(hiddenField);
          document.body.appendChild(form);
          form.submit();
        },
      },
      {
        text: "Cancel",
        handler: function (sm, row, rec) {
          FormLookUpSuratRawat.close();
        },
      },
    ],
    listeners: {
      afterrender: function () {
        Ext.Ajax.request({
          url: baseURL + "index.php/general/surat_rawat/get_data",
          params: {
            no_transaksi: objRWJAG.NO_TRANSAKSI,
            kd_kasir: objRWJAG.KD_KASIR,
          },
          failure: function (o) {},
          success: function (o) {
            var cst = Ext.decode(o.responseText);
            if (cst.status === false) {
              Ext.Ajax.request({
                url: baseURL + "index.php/general/surat_rawat/get_counter_map",
                params: {
                  kd_unit: objRWJAG.KD_UNIT,
                  format: "SR",
                },
                failure: function (o) {},
                success: function (o) {
                  var cst = Ext.decode(o.responseText);
                  tanggal = new Date(now);
                  month = convert_number_to_roman(
                    parseInt(tanggal.getMonth() + 1)
                  );
                  var year = tanggal.format("Y");
                  Ext.getCmp("iTextNomerDokumen").setValue(
                    cst.format_number +
                      "/" +
                      cst.format +
                      "/" +
                      month +
                      "/" +
                      cst.rs +
                      "/" +
                      year
                  );
                },
              });
            } else {
              Ext.getCmp("iComboDokterRWI").setValue(cst.dokter);
              Ext.getCmp("iTextNomerDokumen").setValue(cst.no_dokumen);
              Ext.getCmp("iTextAreaDengan").setValue(cst.diagnosa);
              Ext.getCmp("iTextAreaTerapi").setValue(cst.tindakan);
              Ext.getCmp("iTextAreaTerapiDiberikan").setValue(cst.terapi);
            }
          },
        });
      },
    },
  });
  return simple;
}

function comboDokterSpesial() {
  var $this = this;
  $this.ds_dokter_spesial = new WebApp.DataStore({
    fields: ["KD_DOKTER", "NAMA"],
  });
  Ext.Ajax.request({
    url: baseURL + "index.php/general/surat_rawat/get_data_dokter",
    params: {
      kd_unit: 0,
    },
    failure: function (o) {},
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      for (var i = 0, iLen = cst["listData"].length; i < iLen; i++) {
        var recs = [],
          recType = $this.ds_dokter_spesial.recordType;
        var o = cst["listData"][i];

        recs.push(new recType(o));
        $this.ds_dokter_spesial.add(recs);
      }
    },
  });

  $this.iComboDokter = new Ext.form.ComboBox({
    id: "iComboDokterRWI",
    typeAhead: true,
    hidden: false,
    triggerAction: "all",
    lazyRender: true,
    mode: "local",
    emptyText: "",
    width: "95%",
    store: $this.ds_dokter_spesial,
    valueField: "NAMA",
    displayField: "NAMA",
    value: "",
    fieldLabel: "Dokter &nbsp;",
    listeners: {
      select: function (a, b, c) {},
    },
  });

  return $this.iComboDokter;
}

function CrudData(controller) {
  Ext.Ajax.request({
    url: baseURL + "index.php/main/CreateDataObj",
    params: parameter(controller),
    failure: function (o) {
      ShowPesanWarningRWJ(
        "Proses Saving Tidak berhasil silahkan hubungi admin",
        "Gagal"
      );
    },
    success: function (o) {
      var cst = Ext.decode(o.responseText);
      if (cst.success === true) {
        ShowPesanInfoRWJ("Proses Saving Berhasil", "Save");
        tmpediting = "false";
      } else {
        ShowPesanWarningRWJ(
          "Proses Saving Tidak berhasil silahkan hubungi admin",
          "Gagal"
        );
      }
    },
  });
}
function action_asessmen_setEnab(kd_askep,enab,datas){
  var dat=datas.getRange();
  for(var i=0,iLen= dat.length; i<iLen;i++){
      if(dat[i].data.kd_askep==kd_askep){
          console.log('fefefe');
          console.log(enab);
          dat[i].set('enab',enab);
          dat[i].set('nilai',null);
          dat[i].set('nilai_text',null);
      }
  }
}
function action_asessmen_check(kd_askep,datas){
  console.log('check');
  var dat=datas.getRange();
  console.log(dat);
  console.log(kd_askep);
  for(var i=0,iLen= dat.length; i<iLen;i++){
      if(dat[i].data.length != 'undefined' && dat[i].data.length != undefined){
          var dtch=dat[i].data;
          console.log(dtch);
          for(var j=0,jLen=dtch.length; j<jLen;j++){
              if(dtch[j].data.kd_askep==kd_askep){
                  console.log('cece');
                  console.log(dtch[j].data);
                  if(dtch[j].data.nilai=='X'){
                      dtch[j].set('nilai','');
                      dtch[j].set('nilai_text','');
                      if(dtch[j].data.enable_no != null && dtch[j].data.enable_no!=''){
                          var kdnya=dtch[j].data.enable_no.split(',');
                          for(var i=0,iLen=kdnya.length; i<iLen;i++){
                              action_asessmen_setEnab(kdnya[i],1,datas);
                          }
                      }
                      if(dtch[j].data.disable_no != null && dtch[j].data.disable_no!=''){
                          var kdnya=dtch[j].data.disable_no.split(',');
                          for(var i=0,iLen=kdnya.length; i<iLen;i++){
                              action_asessmen_setEnab(kdnya[i],0,datas);
                          }
                      }
                  }else{
                      dtch[j].set('nilai','X');
                      dtch[j].set('nilai_text','X');
                      if(dtch[j].data.enable_yes != null && dtch[j].data.enable_yes!=''){
                          var kdnya=dtch[j].data.enable_yes.split(',');
                          for(var i=0,iLen=kdnya.length; i<iLen;i++){
                              action_asessmen_setEnab(kdnya[i],1,datas);
                          }
                      }
                      if(dtch[j].data.disable_yes != null && dtch[j].data.disable_yes!=''){
                          var kdnya=dtch[j].data.disable_yes.split(',');
                          for(var i=0,iLen=kdnya.length; i<iLen;i++){
                              action_asessmen_setEnab(kdnya[i],0,datas);
                          }
                      }
                  }
              }
          }
      }else{
          if(dat[i].data.kd_askep==kd_askep){
              if(dat[i].data.nilai=='X'){
                  dat[i].set('nilai','');
                  dat[i].set('nilai_text','');
                  if(dat[i].data.enable_no != null && dat[i].data.enable_no!=''){
                      var kdnya=dat[i].data.enable_no.split(',');
                      for(var i=0,iLen=kdnya.length; i<iLen;i++){
                          action_asessmen_setEnab(kdnya[i],1,datas);
                      }
                  }
                  if(dat[i].data.disable_no != null && dat[i].data.disable_no!=''){
                      var kdnya=dat[i].data.disable_no.split(',');
                      for(var i=0,iLen=kdnya.length; i<iLen;i++){
                          action_asessmen_setEnab(kdnya[i],0,datas);
                      }
                  }
              }else{
                  dat[i].set('nilai','X');
                  dat[i].set('nilai_text','X');
                  if(dat[i].data.enable_yes != null && dat[i].data.enable_yes!=''){
                      var kdnya=dat[i].data.enable_yes.split(',');
                      for(var i=0,iLen=kdnya.length; i<iLen;i++){
                          action_asessmen_setEnab(kdnya[i],1,datas);
                      }
                  }
                  if(dat[i].data.disable_yes != null && dat[i].data.disable_yes!=''){
                      var kdnya=dat[i].data.disable_yes.split(',');
                      for(var i=0,iLen=kdnya.length; i<iLen;i++){
                          action_asessmen_setEnab(kdnya[i],0,datas);
                      }
                  }
              }
          }
      }
  }
}
function parameter(controller) {
  var kd_askep = [];
  var nilai = [];
  var nilai_text = [];
  var enab = [];
  var params = {
    Table: controller,
    KD_PASIEN: rowSelectedKasirRWJ.data.KD_PASIEN,
    KD_UNIT: rowSelectedKasirRWJ.data.KD_UNIT,
    URUT_MASUK: rowSelectedKasirRWJ.data.URUT_MASUK,
    TGL_MASUK: rowSelectedKasirRWJ.data.TANGGAL_TRANSAKSI,
    JAM_MASUK: "00:00:00",
    TGL_TINDAK: "2018-11-19",
    JAM_TINDAK: "00:00:00",
    JAM_JADWAL: "00:00:00",
  };
  dat = dataSource_AsesmenRWJ3.getRange();
  for (var i = 0, iLen = dat.length; i < iLen; i++) {
    if (dat[i].data.saved == 1) {
      enab.push(dat[i].data.enab);
      kd_askep.push(dat[i].data.kd_askep);
      nilai.push(dat[i].data.nilai);
      nilai_text.push(dat[i].data.nilai_text);
    }
  }
  dat = dataSource_AsesmenRWJ4.getRange();
  for (var i = 0, iLen = dat.length; i < iLen; i++) {
    if (dat[i].data.saved == 1) {
      enab.push(dat[i].data.enab);
      kd_askep.push(dat[i].data.kd_askep);
      nilai.push(dat[i].data.nilai);
      nilai_text.push(dat[i].data.nilai_text);
    }
  }
  params["kd_askep[]"] = kd_askep;
  params["nilai[]"] = nilai;
  params["nilai_text[]"] = nilai_text;
  params["enab[]"] = enab;
  return params;
}

function GetDTLPreviewBilling(url, no_transaksi, kd_kasir) {
  new Ext.Window({
    title: "Preview Billing",
    width: 1000,
    height: 600,
    constrain: true,
    modal: true,
    html:
      "<iframe  style ='width: 100%; height: 100%;' src='" +
      url +
      "/" +
      no_transaksi +
      "/" +
      kd_kasir +
      "'></iframe>",
    tbar: [
      {
        xtype: "button",
        text: "Cetak Direct",
        iconCls: "print",
        handler: function () {
          window.open(
            baseURL +
              "index.php/laporan/lap_billing/print_pdf/" +
              no_transaksi +
              "/" +
              kd_kasir,
            "_blank"
          );
        },
      },
    ],
  }).show();
}
