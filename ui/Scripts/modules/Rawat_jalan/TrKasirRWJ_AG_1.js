var dsrad;
var cellCurrentTindakan;
var cellselectedrad;
var CurrentRad ={
    data: Object,
    details: Array,
    row: 0
};
var dsLookProdukList_dokter_rad;
var dsLookProdukList_dokter_leb;
var PenataJasaRJ={};
var CurrentKasirRWJ ={
    data: Object,
    details: Array,
    row: 0
};
var nowTglTransaksiGrid_poli = new Date();
var tglGridBawah_poli = nowTglTransaksiGrid_poli.format("d/M/Y");
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create([
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
]);

var CurrentDiagnosa ={
    data: Object,
    details: Array,
    row: 0
};
var CurrentDiagnosaIcd9 ={
    data: Object,
    details: Array,
    row: 0
};
var CurrentSelectedRiwayatKunjunanPasien ={
    data: Object,
    details: Array,
    row: 0
};
var dsLookProdukList_rad;
var combo;
var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create([
   {name: 'KASUS', mapping:'KASUS'},
   {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
   {name: 'PENYAKIT', mapping:'PENYAKIT'},
   {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
]);

var currentRiwayatKunjunganPasien_TglMasuk_RWJ;
var currentRiwayatKunjunganPasien_KdUnit_RWJ;
var currentRiwayatKunjunganPasien_UrutMasuk_RWJ;
var currentRiwayatKunjunganPasien_KdKasir_RWJ;
var currentRiwayatKunjunganPasien_NoTrans_RWJ;
var currentRiwayatKunjunganPasien_KdDokter_RWJ;

var setLookUpPilihDokterPenindak_RWJ;
var dsDokterPenindak_RWJ;
var setLookUpLastHistoryDiagnosa_RWJ;
var dsLastHistoryDiagnosa_RWJ;
var currentJasaDokterKdTarif_RWJ;
var currentJasaDokterKdProduk_RWJ;
var currentJasaDokterUrutDetailTransaksi_RWJ;
var currentJasaDokterHargaJP_RWJ;
var dsGridJasaDokterPenindak_RWJ;
var dsgridpilihdokterpenindak_RWJ;
dsgridpilihdokterpenindak_RWJ	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_dokter','nama'],
	data: []
});
var GridDokterTr_RWJ;

var cbounitscanberkasrwj;
var kdUnitLab_PenjasRWJ='41';
var dsTRRiwayatKunjuganPasien;
var dsTRRiwayatDiagnosa_RWJ;
var dsTRRiwayatTindakan_RWJ;
var dsTRRiwayatObat_RWJ;
var dsTRRiwayatLab_RWJ;
var dsTRRiwayatRad_RWJ;
var cellSelectedriwayatkunjunganpasien;
var dsTRDetailDiagnosaList;
var dsTRDetailDiagnosaListIcd9;
var dsTRDetailAnamneseList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var cellSelecteddeskripsiIcd9;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var selectCountStatusPeriksa_viKasirRwj;
var selectKlinikPoli;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var dsTRKasirRWJList;
var dsTRDetailKasirRWJList;
var dsPjTrans2;
var dsRwJPJRAD;
var dsCmbRwJPJDiag;
var dsCmbRwJPJIcd9;
var AddNewKasirRWJ = true;
var selectCountKasirRWJ = 50;
var currentKdKasirRWJ;

var rowSelectedKasirRWJ;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var anamnese;
var CurrentAnamnesese;
var vkode_customer;
var CurrentKasirRWJSelection;
CurrentPage.page = getPanelRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
// Asep
PenataJasaRJ.dsComboObat;
PenataJasaRJ.iComboObat;
PenataJasaRJ.iComboVerifiedObat;
PenataJasaRJ.gridObat;
var gridPenataJasaTabItemTransaksiRWJ;
PenataJasaRJ.pj_req_rad;
PenataJasaRJ.pj_req__obt;
PenataJasaRJ.varkd_tarif;
PenataJasaRJ.grid2;
PenataJasaRJ.gridIcd9;
PenataJasaRJ.gridRiwayatKunjungan;
PenataJasaRJ.gridRiwayatDiagnosa;
PenataJasaRJ.gridRiwayatTindakan;
PenataJasaRJ.gridRiwayatObat;
PenataJasaRJ.gridRiwayatLab;
PenataJasaRJ.gridRiwayatRad;
PenataJasaRJ.gridhasil_lab_PJRWJ;
PenataJasaRJ.gridLastHistoryDiagnosa;
PenataJasaRJ.grid3;//untuk data grid di tab labolatorium
PenataJasaRJ.ds1;
PenataJasaRJ.ds2;
PenataJasaRJ.var_kd_dokter_leb;
PenataJasaRJ.ds3;//untuk data store grid di tab labolatorium
PenataJasaRJ.ds4=new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab']});
PenataJasaRJ.ds5=new WebApp.DataStore({ fields: ['id_status','status']});
PenataJasaRJ.dsGridObat;
PenataJasaRJ.dsGridTindakan;
PenataJasaRJ.s1;
PenataJasaRJ.btn1;
PenataJasaRJ.dshasilLabRWJ;
PenataJasaRJ.form={};
PenataJasaRJ.func={};
PenataJasaRJ.panelradiodetail_hasil;
PenataJasaRJ.form.Checkbox={};
PenataJasaRJ.form.Class={};
PenataJasaRJ.form.ComboBox={};
PenataJasaRJ.form.DataStore={};
PenataJasaRJ.form.Grid={};
PenataJasaRJ.form.Group={};
PenataJasaRJ.form.Group.print={};
PenataJasaRJ.form.Window={};
PenataJasaRJ.var_kd_dokter_rad;
PenataJasaRJ.var_id_mrresep;
PenataJasaRJ.form.DataStore.kdpenyakit=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_penyakit','penyakit'],data: []});
PenataJasaRJ.form.DataStore.penyakit=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_penyakit','penyakit'],data: []});
PenataJasaRJ.form.DataStore.produk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku','klasifikasi'],data: []});
PenataJasaRJ.form.DataStore.kdIcd9=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});
PenataJasaRJ.form.DataStore.deskripsi=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});

PenataJasaRJ.ComboVerifiedObat=function(){
	PenataJasaRJ.iComboVerifiedObat	= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			id		: 0,
			fields	: ['Id','displayText'],
			data	: [[0, 'Disetujui'],[1, 'Tdk Disetujui']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return PenataJasaRJ.iComboVerifiedObat;
};

PenataJasaRJ.classGridObat	= Ext.data.Record.create([
   {name: 'kd_prd', 	mapping: 'kd_prd'},
   {name: 'nama_obat', 	mapping: 'nama_obat'},
   {name: 'jumlah', 	mapping: 'jumlah'},
   {name: 'satuan', 	mapping: 'satuan'},
   {name: 'cara_pakai', mapping: 'cara_pakai'},
   {name: 'kd_dokter', 	mapping: 'kd_dokter'},
   {name: 'verified', 	mapping: 'verified'},
   {name: 'racikan', 	mapping: 'racikan'},
   {name: 'order_mng', 	mapping: 'order_mng'},
   {name: 'jml_stok_apt', 	mapping: 'jml_stok_apt'}
]);

PenataJasaRJ.classGrid3	= Ext.data.Record.create([
    {name: 'kd_produk', 	mapping: 'kd_produk'},
    {name: 'kd_klas', 	mapping: 'kd_klas'},
    {name: 'deskripsi', 	mapping: 'deskripsi'},
    {name: 'username', 	mapping: 'username'},
    {name: 'kd_lab', 	mapping: 'kd_lab'}
 ]);

PenataJasaRJ.form.Class.diagnosa= Ext.data.Record.create([
  {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
  {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
  {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
  {name: 'URUT', 	mapping: 'UURUTRUT'},
  {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
  {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
  {name: 'KASUS', 	mapping: 'KASUS'},
  {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
  {name: 'NOTE', 	mapping: 'NOTE'},
  {name: 'DETAIL', 	mapping: 'DETAIL'}
]);
PenataJasaRJ.form.Class.produk= Ext.data.Record.create([
  {name: 'KD_PRODUK', 	mapping: 'KD_PRODUK'},
  {name: 'DESKRIPSI', 	mapping: 'PDESKRIPSIENYAKIT'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DOKTER', 	mapping: 'DOKTER'},
  {name: 'TGL_TINDAKAN', 	mapping: 'TGL_TINDAKAN'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DESC_REQ', 	mapping: 'DESC_REQ'},
  {name: 'TGL_BERLAKU', 	mapping: 'TGL_BERLAKU'},
  {name: 'NO_TRANSAKSI', 	mapping: 'NO_TRANSAKSI'},
  {name: 'URUT', 	mapping: 'URUT'},
  {name: 'DESC_STATUS', 	mapping: 'DESC_STATUS'},
  {name: 'TGL_TRANSAKSI', 	mapping: 'TGL_TRANSAKSI'},
  {name: 'KD_TARIF', 	mapping: 'KD_TARIF'},
  {name: 'HARGA', 	mapping: 'HARGA'}
]);

PenataJasaRJ.func.getNullProduk= function(){
	console.log(gridPenataJasaTabItemTransaksiRWJ.getSelectionModel());
	var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
	return new PenataJasaRJ.form.Class.produk({
		KD_PRODUK	: '',
		DESKRIPSI	: '',
		QTY			: 1,
		DOKTER		: Ext.getCmp('txtNamaDokter').getValue(),
		TGL_TINDAKAN: '',
		QTY			: 1,
		DESC_REQ	: '',
		TGL_BERLAKU	: '',
		NO_TRANSAKSI: '',
		URUT		: '',
		DESC_STATUS	: '',
		TGL_TRANSAKSI: o.TANGGAL_TRANSAKSI,
		KD_TARIF	: '',
		HARGA		: '',
		JUMLAH		: '',
	});
};

PenataJasaRJ.func.getNullDiagnosa= function(){
	return new PenataJasaRJ.form.Class.diagnosa({
		KD_PENYAKIT	: '',
		PENYAKIT	: '',
		KD_PASIEN	: '',
		URUT		: 0,
		URUT_MASUK	: '',
		TGL_MASUK	: '',
		KASUS		: '',
		STAT_DIAG	: '',
		NOTE		: '',
		DETAIL		: 0
	});
};

 PenataJasaRJ.nullGridObat	= function(){
	return new PenataJasaRJ.classGridObat({
		kd_produk	: '',
		nama_obat	: '',
		jumlah		: 0,
		satuan		: '',
		cara_pakai	: '',
		kd_dokter	: '',
		verified	: 'Tidak Disetujui',
		racikan		: 0,
		order_mng	: 'Belum Dilayani',
		jml_stok_apt:0
	});
}; 

function hasilJumlah_rwj(qty){
	
	
	for(var i=0; i<dsPjTrans2.getCount() ; i++){


		var o=dsPjTrans2.getRange()[i].data;
		console.log(o);
		if(qty != undefined){
			if(o.jumlah <= o.jml_stok_apt){
			
			} else{
				o.jumlah=o.jml_stok_apt;
				PenataJasaRJ.pj_req__obt.getView().refresh();
				ShowPesanWarningRWJ('Jumlah obat melebihi stok yang tersedia','Warning');
				
			}
			
		}
		

	}
	
}


PenataJasaRJ.nullGrid3	= function(){
var tgltranstoday =new Date();
	return new PenataJasaRJ.classGrid3({

		kd_produk	 	: '',
		deskripsi		: '',
		kd_tarif		: '',
		harga			: '',
		qty				: '',
		tgl_berlaku		: '',
		urut			: '',
		tgl_transaksi	: tgltranstoday.format('Y/m/d'),
		no_transaksi	: '',
		kd_dokter		: '',
		
	});
};
PenataJasaRJ.comboObat	= function(){
	var $this	= this;
	$this.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
/* 	$this.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ',
			kdcustomer: vkode_customer
		} 
	}); */
	$this.iComboObat= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
		store			: $this.dsComboObat,
		valueField		: 'nama_obat',
		hideTrigger		: true,
		displayField	: 'nama_obat',
		value			: '',
		listeners		: {
			select	: function(a, b, c){
			console.log(b.json);
				var line	= $this.gridObat.getSelectionModel().selection.cell[0];
				$this.dsGridObat.getRange()[line].data.kd_prd=b.json.kd_prd;
				$this.dsGridObat.getRange()[line].data.satuan=b.json.satuan;
				$this.dsGridObat.getRange()[line].data.kd_dokter=b.json.nama;
				$this.dsGridObat.getRange()[line].data.nama_obat=b.json.nama_obat;
				$this.dsGridObat.getRange()[line].data.jml_stok_apt=b.json.jml_stok_apt;
				$this.gridObat.getView().refresh();
		    }
		}
	});
	return $this.iComboObat;
};
	
function getPanelRWJ(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT',
				'ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK',
				'POSTING_TRANSAKSI','ANAMNESE','CAT_FISIK','KD_KASIR',
				'ASAL_PASIEN','CARA_PENERIMAAN','STATUS_ANTRIAN','NO_ANTRIAN'];
    dsTRKasirRWJList = new WebApp.DataStore({ fields: Field });
    PenataJasaRJ.ds1=dsTRKasirRWJList;
    refeshkasirrwj();
	getTotKunjunganRWJ();
	var i = setInterval(function(){
		loadMask.hide();
		getTotKunjunganRWJ();
		loadMask.hide();
	}, 100000); 
    var grListTRRWJ = new Ext.grid.EditorGridPanel({
        stripeRows	: true,
        store		: dsTRKasirRWJList,
        anchor		: '100% 58%',
        columnLines	: true,
        autoScroll	: false,
        border		: true,
		sort 		: false,
        sm			: new Ext.grid.RowSelectionModel({
        	singleSelect: true,
            listeners	:{
                rowselect: function(sm, row, rec){
                    rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(row);
					PenataJasaRJ.s1=dsTRKasirRWJList.getAt(row);
                }
            }
        }),
        listeners	: {
            rowdblclick	: function (sm, ridx, cidx){
                rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(ridx);
                PenataJasaRJ.s1=PenataJasaRJ.ds1.getAt(ridx);
                if (rowSelectedKasirRWJ != undefined){
					/* if(rowSelectedKasirRWJ.data.KD_DOKTER == '' || rowSelectedKasirRWJ.data.KD_DOKTER == undefined){
						LookupPilihDokterPenindak_RWJ(rowSelectedKasirRWJ.data);
					} else{
						RWJLookUp(rowSelectedKasirRWJ.data);
					} */
					 Ext.Msg.show({
									title:'Konfirmasi',
									msg: ' Anda pasien sudah ada ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn){
										if (btn =='yes'){
											
											RWJLookUp(rowSelectedKasirRWJ.data);
											
											updateAntrianSedangDilayani();
											refreshDataDepanPenjasRWJ();
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
					
                    
                    console.log(rowSelectedKasirRWJ.data);
                }else{
                    RWJLookUp();
                }
            }
        },
		
		
        cm			: new Ext.grid.ColumnModel([ 
                 new Ext.grid.RowNumberer(),
			{
                header		: 'Status Posting',
                width		: 50,
                sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
                dataIndex	: 'POSTING_TRANSAKSI',
                id			: 'txtposting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					
                     switch (value){
                         case 't':
                             metaData.css = 'StatusHijau'; 
                             break;
                         case 'f':
                        	 metaData.css = 'StatusMerah';
                             break;
                     }
					 
                     return '';
                }
            },{
                header		: 'Nomor antrian',
                width		: 90,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'NO_ANTRIAN',
                id			: 'colRWNomorAntrianViewRWJ',
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                header		: 'Status antrian',
                width		: 90,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'STATUS_ANTRIAN',
                id			: 'colRWStatusAntrianViewRWJ',
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                id			: 'colReqIdViewRWJ',
                header		: 'No. Transaksi',
                dataIndex	: 'NO_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 80,
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                id			: 'colTglRWJViewRWJ',
                header		: 'Tgl Transaksi',
                dataIndex	: 'TANGGAL_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 75,
                renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					} else{
						metaData.style  ='background:#ffffff;';
					}
                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                }
            },{
                header		: 'No. Medrec',
                width		: 65,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'KD_PASIEN',
                id			: 'colRWJerViewRWJ',
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                header		: 'Pasien',
                width		: 190,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'NAMA',
                id			: 'colRWJerViewRWJ',
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					} else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                id			: 'colLocationViewRWJ',
                header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 170,
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            }, {
                id			: 'colDeptViewRWJ',
                header		: 'Dokter',
                dataIndex	: 'NAMA_DOKTER',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 150,
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					}  else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            },{
                id			: 'colImpactViewRWJ',
                header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
				sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 90,
				renderer:function(value, metaData, record, rowIndex, colIndex, store)
				{
					
					if(record.data.CARA_PENERIMAAN == '0'){
						metaData.style  ='background:#ffb3b3;';
					} else{
						metaData.style  ='background:#ffffff;';
					}
					
					return value;
					
				}
            }
        ]),
        viewConfig	:{forceFit: true},
        tbar		:[
            {
                id		: 'btnScanBerkasRWJ',
                text	: 'Scan Berkas',
                tooltip	: nmEditData,
                iconCls	: 'Edit_Tr',
                handler	: function(sm, row, rec){
                    panelScanBerkas_RWJ();
                }
            }
        ]
    });
    gridPenataJasaTabItemTransaksiRWJ=grListTRRWJ;
    var LegendViewpenatajasa= new Ext.Panel({
        id			: 'LegendViewpenatajasa',
        region		: 'center',
        border		: false,
        bodyStyle	: 'padding:0px 7px 0px 7px',
        layout		: 'column',
        frame		: true,
        anchor		: '100% 8.0001%',
        autoScroll	: false,
        items		:[
            {
                columnWidth	: .033,
                layout		: 'form',
                style		: {'margin-top':'-1px'},
                anchor		: '100% 8.0001%',
                border		: false,
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
            },{
                columnWidth	: .08,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Posting"
            },{
                columnWidth	: .033,
                layout		: 'form',
                style		:{'margin-top':'-1px'},
                border		: false,
                anchor		: '100% 8.0001%',
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
            },{
                columnWidth	: .1,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Belum posting"
            },
			{
				xtype: 'displayfield',				
				width: 115,								
				value: "Kunjungan hari ini : ",
				style:{'margin-top':'1px','font-weight': 'bold' },
			},
			
			{
				xtype: 'textfield',
				id: 'txtTotKunjunganHariIni_TrKasirRWJ',
				style:{'text-align':'right','font-weight': 'bold','color':'red'},
				width: 50,
				readOnly: true
			},
        ]
    });
    var FormDepanRWJ = new Ext.Panel({
        id			: mod_id,
        closable	: true,
        region		: 'center',
        layout		: 'form',
        title		: 'Penata Jasa ',
        border		: false,
        shadhow		: true,
        autoScroll	: false,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        items		: [            
 		   {
		       xtype		: 'panel',
			   plain		: true,
			   activeTab	: 0,
			   height		: 180,
			   defaults		: {
				   bodyStyle	:'padding:10px',
				   autoScroll	: true
			   },
			   items		: [
        		   {
						layout	: 'form',
						margins	: '0 5 5 0',
						border	: true ,
						items	:[
							 {
								 xtype		: 'textfield',
								 fieldLabel	: ' No. Medrec' + ' ',
								 id			: 'txtFilterNomedrec',
								 anchor 	: '70%',
								 onInit		: function() { },
								 listeners	: {
									 'specialkey' : function(){
										 var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
										 if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
											 if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
												 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue());
												 Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
												 //var tmpkriteria = getCriteriaFilter_viDaftar();
												 RefreshDataFilterKasirRWJ();
											 }else{
												 if (tmpNoMedrec.length === 10){
													 RefreshDataFilterKasirRWJ();
												 }else{
													 Ext.getCmp('txtFilterNomedrec').setValue('');
												 }
											 }
										 }
									 }
								 }
							},{	 
								xtype	: 'tbspacer',
								height	: 3
							},{
								xtype			: 'textfield',
								fieldLabel		: ' Pasien' + ' ',
								id				: 'TxtFilterGridDataView_NAMA_viKasirRwj',
								anchor 			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
							   		'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},{	 
								xtype	: 'tbspacer',
								height	: 3
							},{
								xtype			: 'textfield',
								fieldLabel		: ' Dokter' + ' ',
								id				: 'TxtFilterGridDataView_DOKTER_viKasirRwj',
								anchor			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
												  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},
							getItemcombo_filter(),getItemPaneltgl_filter()
						]
        		   	}		
				]
 		   	},
			grListTRRWJ,LegendViewpenatajasa]
	});
   return FormDepanRWJ;

};

function mComboStatusBayar_viKasirRwj(){
	var cboStatus_viKasirRwj = new Ext.form.ComboBox({
		id				: 'cboStatus_viKasirRwj',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '90%',
		emptyText		: '',
		fieldLabel		: 'Status Posting',
		store			: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
		}),
		valueField		: 'Id',
		displayField	: 'displayText',
		value			: selectCountStatusByr_viKasirRwj,
		listeners		:{
			select: function(a,b,c){
				selectCountStatusByr_viKasirRwj=b.data.displayText ;
				RefreshDataFilterKasirRWJ();
			}
		}
	});
	return cboStatus_viKasirRwj;
};
function mComboStatusPeriksa_viKasirRwj(){
	var cboStatusPeriksa_viKasirRwj = new Ext.form.ComboBox({
		id				: 'cboStatusPeriksa_viKasirRwj',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '99.9%',
		emptyText		: '',
		fieldLabel		: 'Status Periksa',
		store			: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [[1, 'Semua'],[2, 'Sudah Periksa'], [3, 'Belum Periksa']]
		}),
		valueField		: 'Id',
		displayField	: 'displayText',
		value			: 'Semua',
		listeners		:{
			select: function(a,b,c){
				selectCountStatusPeriksa_viKasirRwj=b.data.displayText ;
				RefreshDataFilterKasirRWJ();
			}
		}
	});
	return cboStatusPeriksa_viKasirRwj;
};

function mComboUnit_viKasirRwj(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwj.load({
	    params:{
		    Skip	: 0,
		    Take	: 1000,
            Sort	: 'kd_unit',
		    Sortdir	: 'ASC',
		    target	: 'ComboUnit',
            param	: ""
		}
	});
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox({
	    id				: 'cboUNIT_viKasirRwj',
	    typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
	    fieldLabel		: ' Poli Tujuan',
	    align			: 'Right',
	    width			: 100,
	    anchor			: '90%',
	    store			: dsunit_viKasirRwj,
	    valueField		: 'NAMA_UNIT',
	    displayField	: 'NAMA_UNIT',
		value			:'All',
	    listeners		:{
		    select: function(a, b, c){					       
		    	RefreshDataFilterKasirRWJ();
		    }
		}
	});
    return cboUNIT_viKasirRwj;
};

function RWJLookUp(rowdata){
	var i = setInterval(function(){
		loadMask.hide();
		RefreshDataKasirRWJDetai2(rowdata);
		loadMask.hide();
	}, 400000);
    var lebar = 850;
    FormLookUpsdetailTRrwj = new Ext.Window({
            id			: 'gridRWJ',
            title		: 'Penata Jasa Rawat Jalan',
            closeAction	: 'destroy',
            width		: lebar,
            height		: 605,
            border		: false,
            resizable	: false,
            plain		: true,
            constrain	: true,
            layout		: 'fit',
            iconCls		: 'Request',
            modal		: true,
            items		: getFormEntryTRRWJ(lebar,rowdata),
            listeners	: {
            	close: function(){	
            		RefreshDataFilterKasirRWJ();
					
        		},
				'beforeclose': function(){clearInterval(i);}
            }
    });
    FormLookUpsdetailTRrwj.show();
    dsCmbRwJPJDiag.loadData([],false);
	for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
		var recs    = [],
		recType = dsCmbRwJPJDiag.recordType;
		var o=PenataJasaRJ.ds2.getRange()[i].data;
		recs.push(new recType({
			Id        	:o.KD_PENYAKIT,
			displayText : o.KD_PENYAKIT
	    }));
		dsCmbRwJPJDiag.add(recs);
	}
	PenataJasaRJ.ds4.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboLabRJPJ'
		} 
	});
	var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
	var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
	/* PenataJasaRJ.ds3.load({
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewGridLabRJPJ',
			param	:par
		}
	}); */
	PenataJasaRJ.ds5.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboTindakanRJPJ'
		} 
	});
	var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
	var params={
		kd_pasien 	: o.KD_PASIEN,
		kd_unit 	: o.KD_UNIT,
		tgl_masuk	: o.TANGGAL_TRANSAKSI,
		urut_masuk	: o.URUT_MASUK
	};
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/getTindakan",
		params		: params,
		failure		: function(o){
			ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(cst.echo.id_status >=0){
					PenataJasaRJ.iCombo1.selectedIndex=cst.echo.id_status-1;
					PenataJasaRJ.iCombo1.setValue(PenataJasaRJ.ds5.getRange()[(cst.echo.id_status-1)].data.status);
					PenataJasaRJ.iTArea1.setValue(cst.echo.catatan);
				}
			}else{
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
			}
		}
	});
    if (rowdata == undefined){
        RWJAddNew();
    }else{
        TRRWJInit(rowdata);
    }
};


function mComboPoliklinik(lebar){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnit',
            //param	: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitRWJ').dom.value +'~)'
            param	: "LEFT (kd_unit,1)='2'"
        }
    });
    var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntry',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '95%',
        listeners:{
            select: function(a, b, c){
               loaddatastoredokter(b.data.KD_UNIT);
			   selectKlinikPoli=b.data.KD_UNIT;
            }
		}
    });
    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ kd_unit+ '~'
		}
    });
}

function mComboDokterRequestEntry(){
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
    var cboDokterRequestEntry = new Ext.form.ComboBox({
	    id				: 'cboDokterRequestEntry',
	    typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    selectOnFocus	: true,
        forceSelection	: true,
	    emptyText		: 'Pilih Dokter...',
	    labelWidth		: 80,
		fieldLabel		: 'Dokter      ',
	    align			: 'Right',
	    store			: dsDokterRequestEntry,
	    valueField		: 'KD_DOKTER',
	    displayField	: 'NAMA',
        anchor			: '95%',
	    listeners		:{
		    select	: function(a,b,c){
				selectDokter = b.data.KD_DOKTER;
            },
            render	: function(c){
                c.getEl().on('keypress', function(e) {
                    if(e.getKey() == 13) // atau
                        Ext.getCmp('kelPasien').focus();
                }, c);
            }
		}
    });
    return cboDokterRequestEntry;
};

PenataJasaRJ.gridrad=function(data){
    PenataJasaRJ.panelradiodetail_hasil = new Ext.Panel
        ({
		    id: Nci.getId(),
			title: 'Hasil Pemeriksaaan',
		    closable  : true,
		    layout    : 'column',
			width     : 815,
			height    : 150,
		    itemCls   : 'blacklabel',
		    bodyStyle : 'padding: 5px 5px 5px 5px',
		    border    : true,
		    shadhow   : true,
		    margins   : '5 5 5 5',
		    anchor    : '99%',
		    iconCls   : '',
		    items     : 
			[
				{
					columnWidth: .99,
					layout: 'form',
					border: false,
					height: 140,
					labelWidth: 2,
					items:
					[
						{  
							
							xtype: 'textarea',
							name: 'textarea',
							id: 'textareapopuphasil_pjrwj',
							anchor:'99% 80%',
							readOnly:true,
							
						}
					]
				}
			]
    }
    );
    
    return PenataJasaRJ.panelradiodetail_hasil;

}

function KonsultasiLookUp(rowdata,callback){
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window({
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					// width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														// fieldLabel:'Dokter ',
														name: 'txtKdunitKonsultasi',
														id: 'txtKdunitKonsultasi',
														// emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														// hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Konsultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																		Datasave_Konsultasi(false,callback);
																		
																	FormLookUpsdetailTRKonsultasi.close()	;
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close();
															}
														}
											 ]
											
											}
									
								]
			},
            listeners:
            {
            }
        }
    );
    FormLookUpsdetailTRKonsultasi.show();
      KonsultasiAddNew();
};

function KonsultasiAddNew(){
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnitRWJ').dom.value   ;
	Ext.get('txtnamaunitKonsultasi').dom.value=Ext.get('txtNamaUnit').dom.value;

}

function getFormEntryTRRWJ(lebar,data){
	var pnlTRRWJ = new Ext.FormPanel({
		id			: 'PanelTRRWJ',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        bodyStyle	: 'padding:10px 10px 10px 10px',
        height		: 189,
        anchor		: '100%',
        width		: lebar,
        border		: false,
        items		: [getItemPanelInputRWJ(lebar)],
        tbar		: [
			{
		        text: ' Konsultasi',
		        id:'btnLookUpKonsultasi_viKasirIgd',
		        iconCls: 'Konsultasi',
		        handler: function(){
					
		        	KonsultasiLookUp();
		        }
	    	},{
		        text: ' Ganti Dokter',
		        id:'btnLookUpGantiDokter_viKasirIgd',
		        iconCls: 'gantidok',
		        handler: function(){
				   GantiDokterLookUp();
		        }
	    	},{
		        text: 'Ganti Kelompok Pasien',
		        id:'btngantipasien_rwj',
		        iconCls: 'gantipasien',
		        handler: function(){
		        	KelompokPasienLookUp();
		        }
	    	},{
		        text: 'Posting Ke Kasir',
		        id:'btnposting_pj_rwj',
		        iconCls: 'gantidok',
		        handler: function(){
		        	setpostingtransaksi(data.NO_TRANSAKSI);
		        }
			},{
				
				xtype:'splitbutton',
				text:'Cetak',
				iconCls:'print',
				id:'btnPrint_Poliklinik',
				menu: [
				{
				text: 'Cetak Resep',
				iconCls:'print',
				id:'CetakResep',
				handler:function()
				{
				//window.open(baseURL + "index.php/main/resep/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()), "_blank");	
					var params={
							kd_pasien:data.KD_PASIEN,
							kd_unit:data.KD_UNIT,
							tgl:getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/main/resep/cetak");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
				}
				
				},
				{
				text: 'Cetak Tindakan',
				iconCls:'print',
				id:'CetakTindakan',
				handler:function()
				{
					var params={
							kd_pasien:data.KD_PASIEN,
							kd_unit:data.KD_UNIT,
							tgl:getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/main/tindakan/cetak");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
					
				//window.open(baseURL + "index.php/main/tindakan/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()), "_blank");	
				}
				
				},
				{
				text: 'Cetak Radiologi',
				iconCls:'print',
				id:'CetakRadiologi',
				handler:function()
				{
					//window.open(baseURL + "index.php/main/functionRWJ/cetakRad/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
					var params={
							kd_pasien:data.KD_PASIEN,
							kd_unit:data.KD_UNIT,
							tgl:getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()),
							notr:Ext.getCmp('txtNoTransaksiKasirrwj').getValue()
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/main/functionRWJ/cetakRad");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
				}
				
				},
				{
				text: 'Cetak Laboratorium',
				iconCls:'print',
				id:'CetakLab',
				handler:function()
				{
					//window.open(baseURL + "index.php/main/functionRWJ/cetakLab/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
					var params={
							kd_pasien:data.KD_PASIEN,
							kd_unit:data.KD_UNIT,
							tgl:getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()),
							notr:Ext.getCmp('txtNoTransaksiKasirrwj').getValue()
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/main/functionRWJ/cetakLab");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
				}
				
				},
				{
				text: 'Cetak Ringkasan Medis',
				iconCls:'print',
				id:'CetakRM',
				handler:function()
				{
					//window.open(baseURL + "index.php/main/ringkasan/cetak/"+data.KD_PASIEN+"/"+data.KD_UNIT+"/"+getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue())+"/"+Ext.getCmp('txtNoTransaksiKasirrwj').getValue(), "_blank");
					var params={
							kd_pasien:data.KD_PASIEN,
							kd_unit:data.KD_UNIT,
							tgl:getFormatTanggal(Ext.getCmp('dtpTanggalDetransaksi').getValue()),
							notr:Ext.getCmp('txtNoTransaksiKasirrwj').getValue()
					} ;
					var form = document.createElement("form");
					form.setAttribute("method", "post");
					form.setAttribute("target", "_blank");
					form.setAttribute("action", baseURL + "index.php/main/ringkasan/cetak");
					var hiddenField = document.createElement("input");
					hiddenField.setAttribute("type", "hidden");
					hiddenField.setAttribute("name", "data");
					hiddenField.setAttribute("value", Ext.encode(params));
					form.appendChild(hiddenField);
					document.body.appendChild(form);
					form.submit();
				}
				
				},
				],
				handler:function(){	}
			}
        ]
    });
    var x;
	var GDtabDetailRWJ = new Ext.TabPanel({
        id			:'GDtabDetailRWJ',
        region		: 'center',
        activeTab	: 0,
		height		: 485,
		width 		: 815,
        anchor		: '100% 100%',
        border		: false,
        plain		: true,
        defaults	: {
            autoScroll	: true
		},
        items		: [
        	GetDTLTRAnamnesisGrid(),
        	GetDTLTRDiagnosaGrid(),
        	GetDTLTRRWJGrid(data),
        	PenataJasaRJ.getLabolatorium(data),
        	GetDTLTRRadiologiGrid(data),
        	PenataJasaRJ.ok_ok(data),
        	PenataJasaRJ.getTindakan(data),
        	PenataJasaRJ.riwayatPasien(data)],
        tbar		:[
			{
				text	: 'Jadwalkan Operasi',
		        id		: 'btnsimpanop_PJ_RWJ',
		        iconCls	: 'Konsultasi',
		        handler	: function(){
		        	var dateNow 	= Ext.util.Format.date(new Date(now), 'Y-m-d');
		        	var dateOperasi = Ext.util.Format.date(new Date(Ext.getCmp('TglOperasi_viJdwlOperasi').getValue()), 'Y-m-d');
		        	if (dateOperasi=='' || dateOperasi<dateNow) {
		        		ShowPesanWarningRWJ('Tanggal tidak bisa tanggal sebelumnya', 'Validasi');		
		        	}else if( Ext.getCmp('txtJam_viJdwlOperasi').getValue()>24 || Ext.getCmp('txtMenit_viJdwlOperasi').getValue()>59 || Ext.getCmp('txtJam_viJdwlOperasi').getValue()<0 || Ext.getCmp('txtMenit_viJdwlOperasi').getValue()<0){
						ShowPesanWarningRWJ('Ketentuan Jam salah ', 'Validasi');		
					}else if( Ext.getCmp('txtJam_viJdwlOperasi').getValue()==''|| Ext.getCmp('txtJam_viJdwlOperasi').getValue()=='Jam'|| Ext.getCmp('txtMenit_viJdwlOperasi').getValue()=='' || Ext.getCmp('txtMenit_viJdwlOperasi').getValue()=='Menit' ){
						ShowPesanWarningRWJ('Harap Isi Jam dan Menit ', 'Validasi');		
					}else if( Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').getValue()==''||Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').getValue()=='Pilih Jenis Tindakan..' ){
						ShowPesanWarningRWJ('Harap Isi Tindakan ', 'Validasi');
					}else if( Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue()==''||Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue()=='Pilih Jenis Tindakan..' ){
						ShowPesanWarningRWJ('Harap Isi Kamar Operasi ', 'Validasi');
					}else{
					Datasave_ok();
					}	
		        	
		        }
			},{
				text	: 'Simpan Anamnese',
		        id		: 'btnsimpanAnamnese_PJ_RWJ',
		        iconCls	: 'Konsultasi',
		        handler	: function(){
		        	Datasave_Anamnese(false);
					 //refeshkasirrwj();
		        }
			},{
				text	: 'tambah Item Pemeriksaan',
				id		: 'btnbarisRad_PJ_RWJ',
				tooltip	: nmLookup,
				iconCls	: 'add',
				handler	: function(){
					//TambahBarisRad();
					
					var records = new Array();
					records.push(new dsRwJPJRAD.recordType());
					dsRwJPJRAD.add(records);
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanRad_PJ_RWJ',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
						if(dsRwJPJRAD.getCount()==0){
								PenataJasaRJ.alertError('Radiologi: Harap isi data Labolatorium.','Peringatan');
							}else{
								var e=false;
								for(var i=0,iLen=dsRwJPJRAD.getCount();i<iLen ; i++){
									if(dsRwJPJRAD.getRange()[i].data.kd_produk=='' || dsRwJPJRAD.getRange()[i].data.kd_produk==null){
										PenataJasaRJ.alertError('Radiologi: Nilai normal item '+PenataJasaRJ.ds3.getRange()[i].data.deskripsi+' belum tersedia','Peringatan');
										e=true;
										break;
									}
								}if(e==false){
								if (PenataJasaRJ.var_kd_dokter_rad==="" || PenataJasaRJ.var_kd_dokter_rad===undefined)
								{
									ShowPesanWarningRWJ('harap Isi salah satu dokter pada kolom dokter', 'Gagal');
								 }else{
								  	Ext.Ajax.request({
										url			: baseURL + "index.php/main/functionRADPoliklinik/savedetailrad",
										params		: getParamDetailTransaksiRAD(),
										failure		: function(o){
											ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
											PenataJasaRJ.var_kd_dokter_rad="";
										},
										success		: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
												saveRujukanRad_SQL(cst.NO_TRANS);
												var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
												var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
													//	ViewGridBawahpoliLab(o.KD_PASIEN);
													ViewGridBawahpoliRad(o.NO_TRANSAKSI,Ext.getCmp('txtKdUnitRWJ').getValue(),o.TANGGAL_TRANSAKSI,o.URUT_MASUK);
											}else if(cst.success === false && cst.cari=== false)
											{
												//PenataJasaRJ.var_kd_dokter_rad="";
												ShowPesanWarningRWJ('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya', 'Gagal');
											}else{
												//PenataJasaRJ.var_kd_dokter_rad="";
												ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
											}
										}
									});
								  }
							    }
							}
				}
			}, {
                id:'btnHpsBrsRad_PJ_RWJ',
                text: 'Hapus item',
                tooltip: 'Hapus Baris',
                iconCls: 'RemoveRow',
                handler: function(){
							var line=PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];
							if(PenataJasaRJ.pj_req_rad.getSelectionModel().selection==null){
								ShowPesanWarningRWJ('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
							}else{
								Ext.Msg.show({
									title:nmHapusBaris,
				                   	msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + dsRwJPJRAD.getRange()[line].data.kd_produk ,
				                   	buttons: Ext.MessageBox.YESNO,
				                   	fn: function (btn){
				                   		if (btn =='yes'){
				                   			var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
									
											if (dsRwJPJRAD.getRange()[line].data.no_transaksi===""||dsRwJPJRAD.getRange()[line].data.no_transaksi===undefined)
											{
											dsRwJPJRAD.removeAt(line);
											PenataJasaRJ.pj_req_rad.getView().refresh();
											}else{
											    ShowPesanWarningRWJ('data Tidak dapat dihapus karena sudah tersimpan didatabase', 'Gagal');
											}
				                        }
				                   	},
				                   	icon: Ext.MessageBox.QUESTION
				                });
							}
                }
            },
				PenataJasaRJ.btn1= new Ext.Button({
					text	: 'Tambah Item Pemeriksaan',
					id		: 'RJPJBtnAddLab_PJ_RWJ',
					tooltip	: nmLookup,
					iconCls	: 'add',
					handler	: function(){
						PenataJasaRJ.ds3.insert(PenataJasaRJ.ds3.getCount(),PenataJasaRJ.nullGrid3());
					}
				}),
				PenataJasaRJ.btn2= new Ext.Button({
					text	: 'Simpan',
					id		: 'RJPJBtnSaveLab_PJ_RWJ',
					tooltip	: nmLookup,
					iconCls	: 'save',
						handler	: function(){
							if(PenataJasaRJ.ds3.getCount()==0){
								PenataJasaRJ.alertError('Laboratorium: Harap isi data Labolatorium.','Peringatan');
							}else{
								var e=false;
								for(var i=0,iLen=PenataJasaRJ.ds3.getCount();i<iLen ; i++){
									if(PenataJasaRJ.ds3.getRange()[i].data.kd_produk=='' || PenataJasaRJ.ds3.getRange()[i].data.kd_produk==null){
										PenataJasaRJ.alertError('Laboratorium: Nilai normal item '+PenataJasaRJ.ds3.getRange()[i].data.deskripsi+' belum tersedia','Peringatan');
										e=true;
										break;
									}
								}
								if(e==false){
								if (PenataJasaRJ.var_kd_dokter_leb==="" || PenataJasaRJ.var_kd_dokter_leb===undefined)
									{
									ShowPesanWarningRWJ('harap Isi salah satu dokter pada kolom dokter', 'Gagal');
									}else{
										Ext.Ajax.request({
											url			: baseURL + "index.php/main/functionLABPoliklinik/savedetaillab",
											params		: getParamDetailTransaksiLAB(),
											failure		: function(o){
												ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
												//PenataJasaRJ.var_kd_dokter_leb="";
											},success		: function(o){
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) {
													ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
													saveRujukanLab_SQL(cst.notrans);
													var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
													var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
													ViewGridBawahpoliLab(o.NO_TRANSAKSI,Ext.getCmp('txtKdUnitRWJ').getValue(),o.TANGGAL_TRANSAKSI,o.URUT_MASUK);
												}else if(cst.success === false && cst.cari=== false)
												{
													//PenataJasaRJ.var_kd_dokter_leb="";
													ShowPesanWarningRWJ('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya', 'Gagal');
												}else{
													//PenataJasaRJ.var_kd_dokter_leb="";
													ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
												}
											}
										});
									}
							    }
							}
						}
					}),
					PenataJasaRJ.btn3= new Ext.Button({
						text	: 'Hapus',
						id		: 'RJPJBtnDelLab_PJ_RWJ',
						tooltip	: nmLookup,
						iconCls	: 'RemoveRow',
						handler	: function(){
							var line=PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
							if(PenataJasaRJ.grid3.getSelectionModel().selection==null){
								ShowPesanWarningRWJ('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
							}else{
								Ext.Msg.show({
									title:nmHapusBaris,
				                   	msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + PenataJasaRJ.ds3.getRange()[line].data.kd_produk ,
				                   	buttons: Ext.MessageBox.YESNO,
				                   	fn: function (btn){
				                   		if (btn =='yes'){
				                   			var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
											/* var params={
												kd_pasien 	: o.KD_PASIEN,
												kd_unit 	: o.KD_UNIT,
												tgl_masuk	: o.TANGGAL_TRANSAKSI,
												urut_masuk	: o.URUT_MASUK,
												kd_produk	: PenataJasaRJ.ds3.getRange()[line].data.kd_produk
											}; */
											/* Ext.Ajax.request({
												url			: baseURL + "index.php/main/functionRWJ/deletelaboratorium",
												params		: params,
												failure		: function(o){
													ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
												},
												success		: function(o){
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) {
														
														ShowPesanInfoDiagnosa('Data Berhasil Dihapus', 'Info');
														var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
														var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
														/* PenataJasaRJ.ds3.load({
															params	: { 
																Skip	: 0, 
																Take	: 50, 
																target	:'ViewGridLabRJPJ',
																param	:par
															}
														});
													}else{
														ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
													}
												}
											}); */
											if (PenataJasaRJ.ds3.getRange()[line].data.no_transaksi===""||PenataJasaRJ.ds3.getRange()[line].data.no_transaksi===undefined)
											{
											PenataJasaRJ.ds3.removeAt(line);
											PenataJasaRJ.grid3.getView().refresh();
											}else{
											     ShowPesanWarningRWJ('data Tidak dapat dihapus karena sudah tersimpan didatabase', 'Gagal');
												 }
				                        }
				                   	},
				                   	icon: Ext.MessageBox.QUESTION
				                });
							}
						}
					}),
					PenataJasaRJ.btn4= new Ext.Button({
						text	: 'Simpan',
						id		: 'RJPJBtnSaveTin',
						tooltip	: nmLookup,
						iconCls	: 'save',
						handler	: function(){
							var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
							if(PenataJasaRJ.iCombo1.selectedIndex>-1 || PenataJasaRJ.iTArea1.getValue()==''){
								var params={
									kd_pasien 	: o.KD_PASIEN,
									kd_unit 	: o.KD_UNIT,
									tgl_masuk	: o.TANGGAL_TRANSAKSI,
									urut_masuk	: o.URUT_MASUK,
									id_status	: PenataJasaRJ.ds5.getRange()[PenataJasaRJ.iCombo1.selectedIndex].data.id_status,
									catatan		: PenataJasaRJ.iTArea1.getValue()
								};
								Ext.Ajax.request({
									url			: baseURL + "index.php/main/functionRWJ/saveTindakan",
									params		: params,
									failure		: function(o){
										ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
									},
									success		: function(o){
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											console.log(PenataJasaRJ.ds5.getRange()[PenataJasaRJ.iCombo1.selectedIndex].data.id_status);
											console.log(PenataJasaRJ.iTArea1.getValue());
											ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
											
											//refeshkasirrwj();
										}else{
											ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
										}
									}
								});
							}else{
								ShowPesanWarningRWJ('Status/Catatan tidak boleh kosong.', 'Peringatan');
							}
						}
					})
        ],
		listeners:{
               tabchange : function (panel, tab) {
				if (tab.id == 'tabDiagnosa'){
					dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
					Ext.getCmp('catLainGroup').hide();
					Ext.getCmp('txtneoplasma').hide();
					Ext.getCmp('txtkecelakaan').hide();
					Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').show();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').show();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').show();
				 
	                 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
				 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				 
				 	PenataJasaRJ.btn4.hide();
				}else if(tab.id == 'tabTransaksi'){
					 Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					 Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').show();
					 Ext.getCmp('btnSimpanRWJ').show();
					 Ext.getCmp('btnLookupRWJ').show();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				 
				}else if (tab.id == 'tabradiologi'){
					 Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').show();
         			 Ext.getCmp('btnSimpanRad_PJ_RWJ').show();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').show();

				}else if(tab.id == 'tabAnamnses'){
					 Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').show();
					 Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
					 
					 PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabLaboratorium'){
					PenataJasaRJ.btn1.show();
					PenataJasaRJ.btn2.show();
					PenataJasaRJ.btn3.show();
					Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					
					Ext.getCmp('btnHpsBrsRWJ').hide();
					Ext.getCmp('btnSimpanRWJ').hide();
					Ext.getCmp('btnLookupRWJ').hide();
					
					Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				 
					PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabjadwalop'){
					Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnsimpanop_PJ_RWJ').show();
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				 
				}else if(tab.id=='tabTindakan'){
					Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					 Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.show();
					 
					 Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					 Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					 Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				 
				}else if(tab.id == 'panelRiwayatAllKunjunganPasienRWJ'){
					Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').hide();
					Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').hide();
					Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').hide();
					Ext.getCmp('btnsimpanop_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsRWJ').hide();
					Ext.getCmp('btnSimpanRWJ').hide();
					Ext.getCmp('btnLookupRWJ').hide();

					PenataJasaRJ.btn1.hide();
					PenataJasaRJ.btn2.hide();
					PenataJasaRJ.btn3.hide();

					PenataJasaRJ.btn4.hide();

					Ext.getCmp('btnbarisRad_PJ_RWJ').hide();
					Ext.getCmp('btnSimpanRad_PJ_RWJ').hide();
					Ext.getCmp('btnHpsBrsRad_PJ_RWJ').hide();
				}
			}
        }
	});
   var pnlTRRWJ2 = new Ext.FormPanel({
            id: 'PanelTRRWJ2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:360,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRWJ]
    });
    var FormDepanRWJ = new Ext.Panel({
		    id: 'FormDepanRWJ',
		    region: 'center',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
			resizable:false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRWJ,pnlTRRWJ2	]
		});
	
	if( data.POSTING_TRANSAKSI == 't'){
		setdisablebutton_PJ_RWJ();
	}else{
		setenablebutton_PJ_RWJ();	
	}
    return FormDepanRWJ;
};

function RecordBaruDiagnosa(){
	var p = new mRecordDiagnosa({
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    'URUT_MASUK':''
		});
	return p;
};

function HapusBarisDiagnosa(){
    if ( cellSelecteddeskripsi != undefined ){
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != ''){
            Ext.Msg.show( {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn) {
                       if (btn =='yes'){
                    	   dsCmbRwJPJDiag.removeAt(PenataJasaRJ.grid2.getSelectionModel().selection.cell[0]);
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === ''){
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            } else{
                                if (btn =='yes'){
                                   DataDeleteDiagnosaDetail();
                                };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        } else{
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail(){
    Ext.Ajax.request({
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    ShowPesanInfoDiagnosa('Data berhasil dihapus', 'Information');
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
					DataDeleteDiagnosaDetail_SQL();
                } else{
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    );
};

function getParamDataDeleteDiagnosaDetail(){
    var params = {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT
    };
    return params;
};

function getParamDetailTransaksiDiagnosa2(){
	var o=rowSelectedKasirRWJ.data;
	//console.log(rowSelectedKasirRWJ.data);
    var params ={
		Table:'ViewTrDiagnosa',
		KdPasien: o.KD_PASIEN,
		KdUnit: o.KD_UNIT,
		UrutMasuk:o.URUT_MASUK,
		Tgl: o.TANGGAL_TRANSAKSI,
		JmlList:GetListCountDetailDiagnosa()
	};
    var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		params['URUT_MASUK'+i]=dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
		params['KD_PENYAKIT'+i]=dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
		params['STAT_DIAG'+i]=dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
		params['KASUS'+i]=dsTRDetailDiagnosaList.data.items[i].data.KASUS;
		params['DETAIL'+i]=dsTRDetailDiagnosaList.data.items[i].data.DETAIL;
		params['NOTE'+i]=dsTRDetailDiagnosaList.data.items[i].data.NOTE;
	}	
    return params;
};

function GetListCountDetailDiagnosa(){
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != ''){
			x += 1;
		};
	}
	return x;
};

function getArrDetailTrDiagnosa(){
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != ''){
			var y='';
			var z='@@##$$@@';
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS;
			if (i === (dsTRDetailDiagnosaList.getCount()-1)){
				x += y ;
			}else{
				x += y + '##[[]]##';
			};
		};
	}	
	return x;
};

function getArrdetailAnamnese(){
	var x = '';
		var y='';
		var z='::';
	var hasil;
	for(var k = 0; k < dsTRDetailAnamneseList.getCount(); k++){
		if (dsTRDetailAnamneseList.data.items[k].data.HASIL == null){
			x += '';
		}else{
			hasil = dsTRDetailAnamneseList.data.items[k].data.HASIL;
			y = dsTRDetailAnamneseList.data.items[k].data.ID_KONDISI;
			y += z + hasil;
		}
		x += y + '<>';
	}
	return x;
}

function Datasave_Diagnosa(mBol){	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/saveDiagnosaPoliklinik",
		params: getParamDetailTransaksiDiagnosa2(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoDiagnosa('Data berhasil disimpan', 'Information');
				updateStatusPeriksa();
				Datasave_Diagnosa_SQL();
				//refreshDataDepanPenjasRWJ();
				//RefreshDataFilterKasirRWJ();
				//refeshkasirrwj();
				if(mBol === false){
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				}
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function Datasave_Anamnese(mBol) {	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: getParamDetailAnamnese(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoAnamnese('Data berhasil disimpan', 'Information');
				//RefreshDataFilterKasirRWJ();
				if(mBol === false){
					RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				};
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorRWJ(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function ShowPesanWarningDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) 
{
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Anamnese Ada Masalah",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Gagal Disimpan",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Sukses diSimpan",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function GetDTLTRRadiologiGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Rujukan Radiologi',
		id:'tabradiologi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetGridRwJPJRad(data),PenataJasaRJ.gridrad(data)]
    });

	return tabTransaksi;
};

PenataJasaRJ.getLabolatorium=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Rujukan Laboratorium',
		id:'tabLaboratorium',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:240,
        width: 815,
        border: false,
        items: [$this.getGrid3(data),GetDTGridHasilLab_PJRWJ()]
    });
	return tabTransaksi;
};

PenataJasaRJ.getTindakan=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Tindak Lanjut',
		id:'tabTindakan',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [
			{
				layout	: 'column',
			    bodyStyle: 'margin-top: 10px;',
			    border	: false,
			   
				items:[{
					layout: 'form',
					labelWidth:100,
					labelAlign:'right',
				    border: false,
				    items	: [
							$this.iCombo1= new Ext.form.ComboBox({
								id				: 'iComboStatusTindakanRJPJ',
								typeAhead		: true,
							    triggerAction	: 'all',
							    lazyRender		: true,
							    mode			: 'local',
							    emptyText		: '',
							    width			: 300,
								store			: $this.ds5,
								valueField		: 'status',
								displayField	: 'status',
								value			: '',
								fieldLabel		: 'Status &nbsp;',
								listeners		: {
									select	: function(a, b, c){
										if(b.json.id_status==4){
											KonsultasiLookUp(null,function(id){
												//Ext.getCmp('txtnamaunitKonsultasi').setValue(data.NAMA_UNIT);
												var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
												var params={
													kd_pasien 	: o.KD_PASIEN,
													kd_unit 	: o.KD_UNIT,
													tgl_masuk	: o.TANGGAL_TRANSAKSI,
													urut_masuk	: o.URUT_MASUK,
													id_status	: 4,
													catatan		: PenataJasaRJ.iTArea1.getValue(),
													kd_unit_tujuan: id
												};
												Ext.Ajax.request({
													url			: baseURL + "index.php/main/functionRWJ/saveTindakan",
													params		: params,
													failure		: function(o){
														ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
													},
													success		: function(o){
														var cst = Ext.decode(o.responseText);
														if (cst.success === true) {
														}else{
															ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
														}
													}
												});
											});
										}
								    }
								}
							}),
							$this.iTArea1= new Ext.form.TextArea({
								id			: 'iTextAreaCatLabRJPJ',
								fieldLabel	: 'Catatan &nbsp; ',
								width       : 500,
					            autoScroll  : true,
					            height      : 80
							})
						]}
				]
			}
        ]
    });
	return tabTransaksi;
};

PenataJasaRJ.ok_ok=function(data){
	var $this=this;
	var tab_OK = new Ext.Panel({
		id:'tabjadwalop',
        fileUpload: true,
		layout: 'Form',
		anchor: '100%',
		title: 'Order Jadwal Operasi',
	    width: 815,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
        items: [
				{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Operasi',
				anchor: '100%',
				width: 200,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglOperasi_viJdwlOperasi',						
						format: 'd/M/Y',
						width: 120,
						value:now,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viJdwlOperasi();								
								} 						
							}
						}
					},
					
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Jam Operasi',
				anchor: '100%',
				width: 200,
				items: 
				[
				{
					xtype: 'textfield',
					fieldLabel: 'Jam',
					id: 'txtJam_viJdwlOperasi',
					name: 'txtJam_viJdwlOperasi',
					width: 50,
					emptyText:'Jam',
					maxLength:2,
				},
				{
					xtype: 'textfield',
					fieldLabel: 'Menit',
					id: 'txtMenit_viJdwlOperasi',
					name: 'txtMenit_viJdwlOperasi',
					width: 50,
					emptyText:'Menit',
					maxLength:2,
				}
				]
			},viComboJenisTindakan_viJdwlOperasi(),
			viComboKamar_viJdwlOperasi()
			
        ]
    });
	return tab_OK;
};

PenataJasaRJ.riwayatPasien=function(data){
	var $this=this;
	
	var subPanel_accordionRiwayat_RWJ = new Ext.Panel({
		//layout:'accordion',
        layout: {
        type: 'accordion',
        titleCollapse: true,
        multi: true,
        fill: false,
        animate: false, 
        flex: 1
        },

        id: 'accordionNavigationContainerPenataJasaRWJ',
		width: 560,
		defaults: {
			// applied to each contained panel
			//bodyStyle: 'padding:15px'
		},
		layoutConfig: {
			// layout-specific configs go here
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [{
			title: 'Diagnosa',
			items:[
				GetGridRiwayatDiagnosa_RWJ(),
			]
		},{
			title: 'Tindakan / ICD 9',
			items:[
				GetGridRiwayatTindakan_RWJ(),
			]
		},{
			title: 'Obat',
			items:[
				GetGridRiwayatObat_RWJ(),
			]
		},{
			title: 'Laboratorium',
			items:[
				GetGridRiwayatLab_RWJ(),
			]
		},{
			title: 'Radiologi',
			items:[
				GetGridRiwayatRad_RWJ(),
			]
		}]
	});
	
	var subPanel_riwayatAmnase_RWJ = new Ext.Panel({
		id:'panelriwayatAmnaseRWJ',
		layout: 'absolute',
		anchor: '100%',
	    width: 550,
		height: 30,
		border:false,
		bodyStyle: 'padding:0px 0px 0px 0px',
        items: [
			{
				x: 5,
				y: 5,
				xtype: 'label',
				text: 'Anamnese'
			},
			{
				x: 70,
				y: 5,
				xtype: 'label',
				text: ':'
			},
			{	
				x: 80,
				y: 4,
				xtype: 'textfield',
				fieldLabel: 'Menit',
				id: 'txtAmnase_RWJ',
				name: 'txtAmnase_RWJ',
				width: 460,
				readOnly:true
			},
        ]
    });
	
	var panel_riwayatKunjunganPasien_RWJ = new Ext.Panel({
		id:'panelriwayatKunjunganPasienRWJ',
        fileUpload: true,
		layout: 'form',
		anchor: '100%',
		title: 'Kunjungan Pasien',
	    width: 250,
		height: 292,
	    bodyStyle: 'padding:0px 0px 0px 0px',
        items: [
			GetGridRiwayatKunjunganPasien_RWJ()	
			
        ]
    });
	
	var panel_riwayatPasien_RWJ = new Ext.Panel({
		id:'panelriwayatPasienRWJ',
		layout: 'vbox',
		anchor: '100%',
		title: 'Riwayat Pasien',
	    width: 560,
		height: 290,
		margins: '0 0 0 5',
        items: [
			subPanel_riwayatAmnase_RWJ,
			subPanel_accordionRiwayat_RWJ,
        ]
    });
	
	
	
	var panel_riwayatAllKunjunganPasien_RWJ = new Ext.Panel({
		id:'panelRiwayatAllKunjunganPasienRWJ',
        fileUpload: true,
		layout: 'hbox',
		anchor: '100%',
		title: 'Riwayat Pasien',
	    width: 815,
	    labelAlign: 'Left',
        items: [
			panel_riwayatKunjunganPasien_RWJ,
			panel_riwayatPasien_RWJ
			
        ],
		tbar:[
			{
				text	: 'Cetak Riwayat',
				id		: 'btnCetakRiwayatPasien_PJ_RWJ',
				tooltip	: nmLookup,
				iconCls	: 'print',
				handler	: function(){
					if(currentRiwayatKunjunganPasien_TglMasuk_RWJ == '' || currentRiwayatKunjunganPasien_TglMasuk_RWJ == undefined){
						ShowPesanWarningRWJ('Pilih riwayat kunjungan yang akan dicetak!','Warning')
					} else{
						var params={
							kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
							kd_unit:currentRiwayatKunjunganPasien_KdUnit_RWJ,
							kd_kasir:currentRiwayatKunjunganPasien_KdKasir_RWJ,
							urut_masuk:currentRiwayatKunjunganPasien_UrutMasuk_RWJ,
							tgl_masuk:currentRiwayatKunjunganPasien_TglMasuk_RWJ,
							kd_dokter:currentRiwayatKunjunganPasien_KdDokter_RWJ,
							no_transaksi:currentRiwayatKunjunganPasien_NoTrans_RWJ,
						} 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/rawat_jalan/functionRWJ/cetakRiwayatKunjungan");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
					}
				}
			},
		]
    });
	return panel_riwayatAllKunjunganPasien_RWJ;
};

function viComboJenisTindakan_viJdwlOperasi()
{
	var Field =['kd_tindakan','tindakan'];
    dsvComboTindakanOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboTindakanOperasiJadwalOperasiOK()
	  var cbo_viComboJenisTindakan_viJdwlOperasi = new Ext.form.ComboBox
		(
		
			{
				id:"cbo_viComboJenisTindakan_viJdwlOperasi",
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				emptyText:'Pilih Jenis Tindakan..',
				fieldLabel: "Jenis Tindakan",           
				width:230,
				store:dsvComboTindakanOperasiJadwalOperasiOK,
				valueField: 'kd_tindakan',
				displayField: 'tindakan',
				listeners:  
				{
				}
			}
		);
	return cbo_viComboJenisTindakan_viJdwlOperasi;
};


function dsComboTindakanOperasiJadwalOperasiOK(kriteria)
{
	dsvComboTindakanOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboTindakanOperasiJadwalOperasiOK',
					param:kriteria
                }			
            }
        );   
    return dsvComboTindakanOperasiJadwalOperasiOK;
}

PenataJasaRJ.getGrid3=function(data){
	var $this=this;
	PenataJasaRJ.ds3 = new WebApp.DataStore({ fields: ['kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','tgl_berlaku','kd_dokter','no_transaksi','urut','desc_status','tgl_transaksi','jumlah','namadok','lunas','kd_pasien','urutkun','tglkun','kdunitkun'] });
	PenataJasaRJ.grid3 = new Ext.grid.EditorGridPanel({
        title: 'Laboratorium',
		id:'grid3',
        stripeRows: true,
        height: 130,
		width:815,
        store: PenataJasaRJ.ds3,
        border: true,
        frame: true,
		autoScroll:true,
		   sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            var rowSelectedPJKasir = PenataJasaRJ.ds3.getAt(row);
							if(rowSelectedPJKasir.data.kd_pasien===undefined||rowSelectedPJKasir.data.kd_pasien==="")
							{}else{
							ViewGridDetailHasilLab(rowSelectedPJKasir.data.kd_pasien,rowSelectedPJKasir.data.tglkun,rowSelectedPJKasir.data.urutkun);
							}
						}
                    }
                }
            ),
        cm: $this.getModel1(),
        viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.grid3;
};

function ViewGridDetailHasilLab(kd_pasien,tgl_masuk,urut_masuk) 
{
    var strKriteriaHasilLab='';
    strKriteriaHasilLab = "LAB_hasil.Kd_Pasien = '" + kd_pasien + "' And LAB_hasil.Tgl_Masuk = '" + tgl_masuk + "'  and LAB_hasil.Urut_Masuk ="+ urut_masuk +"  and LAB_hasil.kd_unit= '"+ kdUnitLab_PenjasRWJ +"' order by lab_test.kd_lab,lab_test.kd_test asc";
   
    PenataJasaRJ.dshasilLabRWJ.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewGridHasilLab',
			    param: strKriteriaHasilLab
			}
		}
	);
    return PenataJasaRJ.dshasilLabRWJ;
};

function GetGridRwJPJRad(data){
	var fldDetail = ['kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','cito','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah','kd_dokter','namadok','lunas','kd_pasien','urutkun','tglkun','kdunitkun'];
	dsRwJPJRAD = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaRJ.pj_req_rad = new Ext.grid.EditorGridPanel({
        title: 'Radiologi',
		id:'gridRwJPJRad',
        stripeRows: true,
        height: 130,
        store: dsRwJPJRAD,
        border: true,
        frame: true,
        width:815,
        anchor: '100%',
        autoScroll:true,
		 sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    var rowSelectedPJKasir_rad = dsRwJPJRAD.getAt(row);
                    	if(rowSelectedPJKasir_rad.data.kd_pasien===undefined||rowSelectedPJKasir_rad.data.kd_pasien==="")
							{}else{
								pj_req_radhasil(rowSelectedPJKasir_rad.data.kd_pasien,
								rowSelectedPJKasir_rad.data.kdunitkun,
								rowSelectedPJKasir_rad.data.tglkun,
								rowSelectedPJKasir_rad.data.urutkun,
								rowSelectedPJKasir_rad.data.kd_produk,
								rowSelectedPJKasir_rad.data.urut);
							}
				
                }
          }
        }),
        cm: getModelRwJPJRad(),
        viewConfig:{forceFit: true}
    });
    
    return PenataJasaRJ.pj_req_rad;
}
PenataJasaRJ.getModel1=function(){
//    var Field = ['KD_DOKTER','NAMA'];
var fldDetail = ['KD_DOKTER','NAMA'];
dsLookProdukList_dokter_leb = new WebApp.DataStore({ fields: fldDetail })
PenataJasaRJ.form.ComboBox.dok_lab= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLookProdukList_dokter_leb,
        		valueField		: 'NAMA',
        		hideTrigger		: true,
        		displayField	: 'NAMA',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
						var line = PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
						console.log(b.data);
						if(PenataJasaRJ.ds3.data.items[line].data.no_transaksi==="" || PenataJasaRJ.ds3.data.items[line].data.no_transaksi===undefined)
						{
						PenataJasaRJ.ds3.data.items[line].data.namadok=b.data.NAMA;
						PenataJasaRJ.var_kd_dokter_leb=b.data.KD_DOKTER;
						PenataJasaRJ.grid3.getView().refresh();
						}else{
						ViewGridBawahpoliLab(PenataJasaRJ.ds3.data.items[line].data.no_transaksi,Ext.getCmp('txtKdUnitRWJ').getValue());
						ShowPesanWarningRWJ('dokter tidak bisa di ganti karena data sudah tersimpan ', 'Warning');
						}
        		    }
        		}
        	});


	var $this=this;
	return new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
		  {
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
					renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
						record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
								({
								id: 'cboKasus2',
								typeAhead: true,
								triggerAction: 'all',
								lazyRender: true,
								mode: 'local',
								selectOnFocus: true,
								forceSelection: true,
								emptyText: 'Silahkan Pilih...',
								width: 50,
								anchor: '95%',
								value: 1,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: ['Id', 'displayText'],
									data: [[1, 'Ya'], [2, 'Tidak']]
								}),
								valueField: 'displayText',
								displayField: 'displayText',
								value		: '',
									   
							})
					
				
		},{
            id			: Nci.getId(),
            header		: 'No Transaksi',
			width		: 60,
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'no_transaksi'	
        }, {
			id	 : Nci.getId(),
			header: 'Pembayaran',
			dataIndex: 'lunas',
			sortable: true,
			width: 60,
			align:'center',
			renderer: function(value, metaData, record, rowIndex, colIndex, store)
			{//console.log(metaData);
				 switch (value)
				 { 
					
					 case 't':
					 
							value = 'lunas'; //
							 break;
					 case 'f':
							value = 'belum '; // rejected

							 break;
				 }
				 return value;
			}
        }, {
			id			: Nci.getId(),
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width		: 35,
			menuDisabled: true,
            hidden		: false,
  
        },{
            id			: Nci.getId(),
            header		: 'Item lab',
            dataIndex	: 'deskripsi',
			 width		: 150,
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
		           editor:PenataJasaRJ.form.ComboBox.produk_labdesk=new Nci.form.Combobox.autoComplete({
					store	: PenataJasaRJ.form.DataStore.produk,
					select	: function(a,b,c){
						//console.log(b);
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/main/functionLAB/cekProduk",
								params:{kd_lab:b.data.kd_produk} ,
								failure: function(o)
								{
									ShowPesanErrorRWJ('Hubungi Admin', 'Error');
								},
								success: function(o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										var line = PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
										PenataJasaRJ.ds3.data.items[line].data.deskripsi=b.data.deskripsi;
										PenataJasaRJ.ds3.data.items[line].data.uraian=b.data.uraian;
										PenataJasaRJ.ds3.data.items[line].data.kd_tarif=b.data.kd_tarif;
										PenataJasaRJ.ds3.data.items[line].data.kd_produk=b.data.kd_produk;
										PenataJasaRJ.ds3.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
										PenataJasaRJ.ds3.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
										PenataJasaRJ.ds3.data.items[line].data.harga=b.data.harga;
										PenataJasaRJ.ds3.data.items[line].data.qty=b.data.qty;
										PenataJasaRJ.ds3.data.items[line].data.jumlah=b.data.jumlah;
										PenataJasaRJ.grid3.getView().refresh();
									}
									else
									{
										var line = PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
										ShowPesanInfoRWJ('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
										PenataJasaRJ.ds3.data.items[line].data.kd_produk="";
										PenataJasaRJ.grid3.getView().refresh();
									};
								}
							}

						)
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							jumlah			: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					param	: function(){
					var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit']=o.KD_UNIT;
					params['kd_customer']=o.KD_CUSTOMER;
					params['penjas'] = 'rwj';
					return params;
				},
					url		: baseURL + "index.php/main/functionLAB/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				})
			
        },{		id			: Nci.getId(),
				header: 'Tanggal Berkunjung',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah_poli;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");
						
							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}
						
					}
				}
            },{//no_transaksi
			id			: Nci.getId(),
        	header		: 'QTY',
            dataIndex	: 'qty',
            width		: 100,
			menuDisabled: true,
            hidden		: true,
			editor: new Ext.form.NumberField (
					{allowBlank: false}
   
                ),
        },{
            id			: Nci.getId(),
            header		: 'Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'namadok',
			editor:PenataJasaRJ.form.ComboBox.dok_lab
        }/* ,{
            id			: Nci.getId(),
            header		: 'Kode Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'kd_dokter',
		
        } */
    ]);
};

function getproduk_PJRWJ()
{

var str='LOWER(tarif.kd_tarif)=LOWER(~'+PenataJasaRJ.varkd_tarif+'~) and tarif.kd_unit= ~5~ ' 

dsLookProdukList_rad.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'tgl_transaksi',
                        Sortdir: 'ASC',
                        target: 'LookupProduk',
                        param: str
                    }
            }
	);
 return dsLookProdukList_rad;
}



function dokter_leb_rwj()
{
    dsLookProdukList_dokter_leb.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41' order by d.nama asc"
                    }
            }
	);
 return dsLookProdukList_dokter_leb;
}


function dokter_rad_rwj()
{
    dsLookProdukList_dokter_rad.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '5'"
                    }
            }
	);
 return dsLookProdukList_dokter_rad;
}

function getModelRwJPJRad(){

var flddokterradio= ['KD_DOKTER','NAMA'];
dsLookProdukList_dokter_rad = new WebApp.DataStore({ fields: flddokterradio })
PenataJasaRJ.form.ComboBox.dok_rad= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLookProdukList_dokter_rad,
        		valueField		: 'NAMA',
        		hideTrigger		: true,
        		displayField	: 'NAMA',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
						var line = PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];
						
						if(dsRwJPJRAD.data.items[line].data.no_transaksi==="" || dsRwJPJRAD.data.items[line].data.no_transaksi===undefined)
						{
						dsRwJPJRAD.data.items[line].data.namadok=b.data.NAMA;
						PenataJasaRJ.var_kd_dokter_rad=b.data.KD_DOKTER;
						PenataJasaRJ.pj_req_rad.getView().refresh();
						}else{

						ViewGridBawahpoliRad(dsRwJPJRAD.data.items[line].data.no_transaksi);
						ShowPesanWarningRWJ('dokter tidak bisa di ganti karena data sudah tersimpan ', 'Warning');
						}
        		    }
        		}
        	});

var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU','CHEK','JUMLAH'];
dsLookProdukList_rad = new WebApp.DataStore({ fields: fldDetail })
PenataJasaRJ.form.ComboBox.produk_rab= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLookProdukList_rad,
        		valueField		: 'DESKRIPSI',
        		hideTrigger		: true,
        		displayField	: 'DESKRIPSI',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
        				var line= PenataJasaRJ.pj_req_rad.getSelectionModel().selection.cell[0];
						
						dsRwJPJRAD.data.items[line].data.deskripsi2=b.data.DESKRIPSI;
        				dsRwJPJRAD.data.items[line].data.kd_tarif=b.data.KD_TARIF;
        				dsRwJPJRAD.data.items[line].data.deskripsi=b.data.DESKRIPSI;
        				dsRwJPJRAD.data.items[line].data.tgl_berlaku=b.data.TGL_BERLAKU;
						dsRwJPJRAD.data.items[line].data.qty=1;
						dsRwJPJRAD.data.items[line].data.kd_produk=b.data.KD_PRODUK;
						dsRwJPJRAD.data.items[line].data.harga=b.data.TARIF;
						dsRwJPJRAD.data.items[line].data.jumlah=b.data.JUMLAH;
						dsRwJPJRAD.data.items[line].data.no_transaksi="";
						PenataJasaRJ.pj_req_rad .getView().refresh();
						//Ext.getCmp('btnSimpanPenJasRad').enable();
        				
        		    }
        		}
        	});
//'kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah'
	return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		  {
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
					renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
						record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
								({
								id: 'cboKasus1',
								typeAhead: true,
								triggerAction: 'all',
								lazyRender: true,
								mode: 'local',
								selectOnFocus: true,
								forceSelection: true,
								emptyText: 'Silahkan Pilih...',
								width: 50,
								anchor: '95%',
								value: 1,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: ['Id', 'displayText'],
									data: [[1, 'Ya'], [2, 'Tidak']]
								}),
								valueField: 'displayText',
								displayField: 'displayText',
								value		: '',
									   
							})
					
				
		},
		{
            id			: Nci.getId(),
            header		: 'No Transaksi',
			width		: 60,
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'no_transaksi'	
        },{
			id	 : Nci.getId(),
			header: 'Pembayaran',
			dataIndex: 'lunas',
			sortable: true,
			width: 60,
			align:'center',
			renderer: function(value, metaData, record, rowIndex, colIndex, store)
			{//console.log(metaData);
				 switch (value)
				 { 
					
					 case 't':
					 
							value = 'lunas'; //
							 break;
					 case 'f':
							value = 'belum '; // rejected

							 break;
				 }
				 return value;
			}
        },{
			id			: Nci.getId(),
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width:30,
			menuDisabled:true,
            hidden:false,
			editor:getRadtest()
        },
		
		{
            id			: Nci.getId(),
            header		: 'Item Rad',
			dataIndex	: 'deskripsi',
			hidden		: false,
			menuDisabled:true,
			width		:150,
			editor		:PenataJasaRJ.form.ComboBox.produk_rab
        },
		{		id			: Nci.getId(),
				header		: 'Tanggal Berkunjung',
				dataIndex	: 'tgl_transaksi',
				width		: 130,
				menuDisabled:true,
				renderer	: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah_poli;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");
						
							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}
						
					}
				}
         },{
            id	: Nci.getId(),
            header: 'DOKTER',
			dataIndex: 'namadok',
			menuDisabled:true,
			hidden: false,
			width:100,
			editor:PenataJasaRJ.form.ComboBox.dok_rad
        },{
			id			: Nci.getId(),
        	header		: 'QTY',
            dataIndex	: 'qty',
            width		: 100,
			menuDisabled: true,
            hidden		: true,
			editor: new Ext.form.NumberField (
					{allowBlank: false}
   
                ) }
    ]);
}

function GetDTLTRAnamnesisGrid() {
	var pnlTRAnamnese = new Ext.Panel({
			title: 'Anamnese',
			id:'tabAnamnses',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            // bodyStyle: 'padding:10px 10px 10px 10px',
            height:100,
            anchor: '100%',
            width: 260,
            border: false,
            items: [	GetDTLTRAnamnesis(),textareacatatanAnemneses()],
			tbar:[
			{
			    xtype: 'textarea',
			    fieldLabel:'Anamnesis  ',
			    name: 'txtareaAnamnesis',
			    id: 'txtareaAnamnesis',
				width:812,
				readOnly:false,
			    anchor: '99%'
			}
		]
    });
	return pnlTRAnamnese;
}

function textareacatatanAnemneses(){
	var TextAreaCatatanAnamnese = new Ext.Panel({
		title: 'Catatan Fisik',
		id:'tabtextAnamnses',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        // bodyStyle: 'padding:10px 10px 10px 10px',
        height:130,
        anchor: '100%',
        width: 815,
        border: false,
        items: [	
			{
				xtype: 'textarea',
			    fieldLabel:'Catatan',
			    name: 'txtareaAnamnesiscatatan',
			    id: 'txtareaAnamnesiscatatan',
				width:815,
				height:85,
				readOnly:false,
			    anchor: '100%'
			}
		]
	});
	return TextAreaCatatanAnamnese;
}

function GetDTLTRAnamnesis() {
    var Anamfied = ['ID_KONDISI','KONDISI','SATUAN','ORDERLIST','KD_UNIT','HASIL'];
    dsTRDetailAnamneseList = new WebApp.DataStore({ fields: Anamfied });
    var gridDTLTRAnamnese = new Ext.grid.EditorGridPanel({
		id:'tabcatAnamnses',
        stripeRows: true,
        store: dsTRDetailAnamneseList,
        border: true,
        columnLines: true,
        frame: false,
		height:120,
        anchor: '100%',
        autoScroll:true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                }
            }
        }),
        cm: TRAnamneseColumModel(),
		viewConfig:{forceFit: true}
    });
    return gridDTLTRAnamnese;
};

function TRAnamneseColumModel() {
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer()
       ,{
			id			: 'colKondisi',
			header 		: 'KONDISI',
			dataindex	: 'KONDISI',
			menuDisbaled: true,
			width		: 100,
			hidden		: false
		},{
            id			: 'colNilai',
            header		: 'NILAI',
            dataIndex	: 'HASIL',
			menuDisabled: true,
			hidden		: false,
			width		: 80,
			editor		: new Ext.form.TextField({
				id				: 'textnilai',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Masukan Nilai...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				valueField		: 'displayText',
				displayField	: 'displayText',
				value			: '',
				listeners		:{}
			})
        },{
            id			: 'colkdunit',
            header		: 'KD UNIT',
            dataIndex	: 'KD_UNIT',
			menuDisabled: true,
			hidden		: true,
			width		: 80
        },{
            id			: 'colSatuan',
            header		: 'SATUAN',
            dataIndex	: 'SATUAN',
			menuDisabled: true,
			width		: 80
        },{
            id			: 'colorderlist',
            header		: 'ORDER LIST',
            dataIndex	: 'ORDERLIST',
			menuDisabled: true,
			hidden		: true,
			width		: 80
        }
    ]);
};

function GetDTLTRDiagnosaGrid(){
	var pnlTRDiagnosa = new Ext.Panel({
		title		: 'Diagnosa',
		id			: 'tabDiagnosa',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        height		: 100,
        anchor		: '100%',
        width		: 815,
        border		: false,
        items		: [GetDTLTRDiagnosaGridFirst(),FieldKeteranganDiagnosa(),getItemTrPenJasRWJ_Batas(),GetDTLTRDiagnosaGridSecondIcd9()]
    });
	return pnlTRDiagnosa;
}

function FieldKeteranganDiagnosa(){
	dsCmbRwJPJDiag = new Ext.data.ArrayStore({
		id: 0,
		fields:[
			'Id',
			'displayText'
		],
		data: []
	});
    var items ={
	    layout: 'column',
	    border: true,
	    width: 815,
	    bodyStyle:'margin-top:5px;padding: 5px;',
	    items:[
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					combo = new Ext.form.ComboBox({
						id:'cmbRwJPJDiag',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						editable: false,
						mode: 'local',
						width: 150,
						emptyText:'',
						fieldLabel: 'Kode Penyakit &nbsp;',
						store: dsCmbRwJPJDiag,
						valueField: 'Id',
						displayField: 'displayText',
						listeners:{
							select: function(){
								if(this.getValue() != ''){
									Ext.getCmp('catLainGroup').show();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==2){
												Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtkecelakaan').show();
												Ext.getCmp('txtneoplasma').hide();
												Ext.get('cbxkecelakaan').dom.checked=true;
											}else if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==1){
												Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtneoplasma').show();
												Ext.getCmp('txtkecelakaan').hide();
												Ext.get('cbxneoplasma').dom.checked=true;
											}else{
												Ext.get('cbxlain').dom.checked=true;
												Ext.getCmp('txtkecelakaan').hide();
												Ext.getCmp('txtneoplasma').hide();
											}
										}
									}
								}
									
							}
						}
					}),
				]
			},
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					{
						xtype: 'radiogroup',
						width:300,
						fieldLabel: 'Catatan Lain &nbsp;',
						id:'catLainGroup',
						name: 'mycbxgrp',
						columns: 3,
						items: [
							{ 
								id: 'cbxlain', 
								boxLabel: 'Lain-lain', 
								name: 'mycbxgrp', 
								width:70, 
								inputValue: 1
							},{ 
								id: 'cbxneoplasma', 
								boxLabel: 'Neoplasma', 
								name: 'mycbxgrp',  
								width:100, 
								inputValue: 2 
							},{ 
								id: 'cbxkecelakaan', 
								boxLabel: 'Kecelakaan', 
								name: 'mycbxgrp', 
								width:100, 
								inputValue: 3 
							}
					   ],
					     listeners: {
		             		change: function(radiogroup, radio){
		             			if(Ext.getDom('cbxlain').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											dsTRDetailDiagnosaList.getRange()[j].data.DETAIL='';
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=0;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxneoplasma').checked == true){
									Ext.getCmp('txtneoplasma').show();
									Ext.getCmp('txtneoplasma').setValue('');
									Ext.getCmp('txtkecelakaan').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxkecelakaan').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').show();
									Ext.getCmp('txtkecelakaan').setValue('');
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}
							}
		                }
					},
				]
			},
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					{
							  xtype: 'textfield',
							  fieldLabel:'Neoplasma &nbsp;',
							  name: 'txtneoplasma',
							  id: 'txtneoplasma',
							  hidden:true,
							  width:600,
							  listeners:{
								  blur: function(){
									  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
										  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
												if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
													dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
													dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
												}
											}
									  }
									  	
								  }
							  }
					       },{
					        	xtype: 'textfield',
					        	fieldLabel:'Kecelakaan / Keracunan &nbsp;',
					        	name: 'txtkecelakaan',
					        	id: 'txtkecelakaan',
					        	hidden:true,
					        	width:600,
						    	listeners:{
									  blur: function(){
										  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
											  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
													if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
														dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
														dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
														Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													}
												}
										  }
										  	
									  }
								  }
					        }
				]
			}
		]
	};
	return items;
}

PenataJasaRJ.form.Class.diagnosa	= Ext.data.Record.create([
   {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
   {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
   {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
   {name: 'URUT', 	mapping: 'URUT'},
   {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
   {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
   {name: 'KASUS', 	mapping: 'KASUS'},
   {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
   {name: 'NOTE', 	mapping: 'NOTE'}
]);

function GetDTLTRDiagnosaGridFirst(){
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG','NOTE','DETAIL'];
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaRJ.ds2=dsTRDetailDiagnosaList;
    RefreshDataSetDiagnosa(PenataJasaRJ.s1.data.KD_PASIEN,PenataJasaRJ.s1.data.KD_UNIT,PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI);
    PenataJasaRJ.grid2 = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'tabDiagnosaGrid',
        store: PenataJasaRJ.ds2,
        border: true,
        columnLines: true,
        frame: false,
        anchor: '100% 100%',
        autoScroll:true,
		height:100,
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                        CurrentDiagnosa.row = row;
                        CurrentDiagnosa.data = cellSelecteddeskripsi;
                    }
                }
            }
        ),
		tbar:[
			{
				text	: 'Tambah ICD 10',
				id		: 'btnLookupDiagnosa_PJ_RWJ',
				tooltip	: nmLookup,
				iconCls	: 'add',
				handler	: function(){
					PenataJasaRJ.ds2.insert(PenataJasaRJ.ds2.getCount(),PenataJasaRJ.func.getNullDiagnosa());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanDiagnosa_PJ_RWJ',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					if (dsTRDetailDiagnosaList.getCount() > 0 ){
						var e=false;
						for(var i=0,iLen=dsTRDetailDiagnosaList.getCount(); i<iLen; i++){
							var o=dsTRDetailDiagnosaList.getRange()[i].data;
							if(o.STAT_DIAG=='' || o.STAT_DIAG==null){
								PenataJasaRJ.alertError('Diagnosa : Diagnosa Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
							if(o.KASUS=='' || o.KASUS==null){
								PenataJasaRJ.alertError('Diagnosa : Kasus Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
						}
						if(e==false){
							Datasave_Diagnosa(false);
						}
					}
				}
			},{
	            id		:'btnHpsBrsDiagnosa_PJ_RWJ',
	            text	: 'Hapus item',
	            tooltip	: 'Hapus Baris',
	            iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailDiagnosaList.getCount() > 0 ){
                        if (cellSelecteddeskripsi != undefined){
                        	if(CurrentDiagnosa != undefined){
                                HapusBarisDiagnosa();
                            }
                        }else{
                            ShowPesanWarning('Pilih record ','Hapus data');
                        }
                    }
                }
			},
			{
				xtype: 'tbseparator'
			},
			{
	            id		:'btnHistoryDiagnosa_PJ_RWJ',
	            text	: 'History diagnosa',
	            tooltip	: 'History diagnosa',
	            iconCls	: 'find',
                handler	: function(){
                    LookupLastHistoryDiagnosa_RWJ();
                }
			},
		],
        cm: TRDiagnosaColumModel(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.grid2;
}

function TRDiagnosaColumModel(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
            id			: Nci.getId(),
            header		: 'No.ICD 10',
            dataIndex	: 'KD_PENYAKIT',
            width		: 70,
			menuDisabled: true,
            hidden		: false,
			editor		: PenataJasaRJ.form.ComboBox.penyakit= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.kdpenyakit,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
    				PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT=b.data.kd_penyakit;
    				PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT=b.data.penyakit;
    				PenataJasaRJ.grid2.getView().refresh();
    				dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
				},
				insert	: function(o){
					return {
						kd_penyakit        	:o.kd_penyakit,
						penyakit 			: o.penyakit,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="200">'+o.penyakit+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getNoIcdPenyakit",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 250
			})
        },{
            id			: Nci.getId(),
            header		: 'Penyakit',
            dataIndex	: 'PENYAKIT',
			menuDisabled: true,
			width		: 200,
			editor		: PenataJasaRJ.form.ComboBox.tindakan= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.penyakit,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
    				PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT=b.data.kd_penyakit;
    				PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT=b.data.penyakit;
    				PenataJasaRJ.grid2.getView().refresh();
    				dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
				},
				insert	: function(o){
					return {
						kd_penyakit        	:o.kd_penyakit,
						penyakit 			: o.penyakit,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="200">'+o.penyakit+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getPenyakit",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 250
			})
        },{
            id: Nci.getId(),
            header: 'kd_pasien',
            dataIndex: 'KD_PASIEN',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut',
            dataIndex: 'URUT',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut masuk',
            dataIndex: 'URUT_MASUK',
			hidden:true
            
        },{
            id: Nci.getId(),
            header: 'tgl masuk',
            dataIndex: 'TGL_MASUK',
			hidden:true
        },{
            id			: Nci.getId(),
            header		: 'Diagnosa',
            width		: 130,
			menuDisabled: true,
            dataIndex	: 'STAT_DIAG',
            editor		: new Ext.form.ComboBox ( {
				id				: Nci.getId(),
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	:['Id','displayText'],
					data	: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id: 'colKasusDiagnosa',
            header: 'Kasus',
            width:130,
			menuDisabled:true,
            dataIndex: 'KASUS',
            editor: new Ext.form.ComboBox({
				id				: 'cboKasus',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	: ['Id','displayText'],
					data	: [[1, 'Baru'],[2, 'Lama']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id			: 'colNote',
            header		: 'Note',
            dataIndex	: 'NOTE',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        },{
            id			: 'colKdProduk',
            header		: 'Detail',
            dataIndex	: 'DETAIL',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        }
    ]);
}

function GetDTLTRDiagnosaGridSecondIcd9(){
	var fldDetail = ['kd_icd9','deskripsi','urut'];
    dsTRDetailDiagnosaListIcd9 = new WebApp.DataStore({ fields: fldDetail });
    //PenataJasaRJ.ds2=dsTRDetailDiagnosaListIcd9;
   // RefreshDataSetDiagnosa(PenataJasaRJ.s1.data.KD_PASIEN,PenataJasaRJ.s1.data.KD_UNIT,PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI);
    PenataJasaRJ.gridIcd9 = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'tabDiagnosaICD9Grid',
        store: dsTRDetailDiagnosaListIcd9,
        border: true,
        columnLines: true,
        frame: false,
       // anchor: '100% 100%',
        autoScroll:true,
		height:120,
		width:815,
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        cellSelecteddeskripsiIcd9 = dsTRDetailDiagnosaListIcd9.getAt(row);
                        CurrentDiagnosaIcd9.row = row;
                        CurrentDiagnosaIcd9.data = cellSelecteddeskripsiIcd9;
                    }
                }
            }
        ),
		tbar:[
			{
				text: 'Tambah ICD 9',
				id: 'BtnTambahTindakanTrPenJasRWJ',
				iconCls: 'add',
				border:true,
				handler: function(){
					var records = new Array();
					records.push(new dsTRDetailDiagnosaListIcd9.recordType());
					dsTRDetailDiagnosaListIcd9.add(records);
				},
			},
			{
				text: 'Simpan',
				id: 'BtnSimpanTindakanTrPenJasRWJ',
				iconCls: 'save',
				border:true,
				handler: function(){
					datasave_TrPenJasRWJ();
				}
			},
			{
	            id		:'btnHpsBrsIcd9_RWJ',
	            text	: 'Hapus item',
	            tooltip	: 'Hapus Baris',
	            iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailDiagnosaListIcd9.getCount() > 0 ){
                        if (cellSelecteddeskripsiIcd9 != undefined){
                        	if(CurrentDiagnosaIcd9 != undefined){
                                var line = PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
								var o = dsTRDetailDiagnosaListIcd9.getRange()[line].data;
								var vkd_icd9=o.kd_icd9;
								var vurut=o.urut;
								if(dsTRDetailDiagnosaListIcd9.getCount()>0){
									Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
										if (button == 'yes'){
											if(dsTRDetailDiagnosaListIcd9.getRange()[line].data.urut != undefined){
												Ext.Ajax.request
												({
													url: baseURL + "index.php/main/functionRWJ/hapusBarisGridIcd",
													params:{
														kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
														kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
														tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(), 
														urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
														no_transaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
														kd_kasir:currentKdKasirRWJ,
														kd_icd9:o.kd_icd9,
														urut:o.urut
													},
													failure: function(o){
														ShowPesanErrorRWJ('Hubungi Admin', 'Error');
													},	
													success: function(o){
														var cst = Ext.decode(o.responseText);
														if (cst.success === true) {
															dsTRDetailDiagnosaListIcd9.removeAt(line);
															PenataJasaRJ.gridIcd9.getView().refresh();
															hapusICD9_SQL(vkd_icd9,vurut);
														}
														else{
															ShowPesanErrorRWJ('Gagal menghapus data ini', 'Error');
														};
													}
												})
											}else{
												dsTRDetailDiagnosaListIcd9.removeAt(line);
												PenataJasaRJ.gridIcd9.getView().refresh();
											}
										} 
										
									});
								} else{
									ShowPesanErrorRWJ('Tidak ada data yang dapat dihapus','Error');
								}
                            }
                        }else{
                            ShowPesanWarningRWJ('Pilih record ','Hapus data');
                        }
                    }
                }
			},
		],
        cm: TRDiagnosaIcd9ColumModel(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridIcd9;
}

function TRDiagnosaIcd9ColumModel(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            id			: Nci.getId(),
            header		: 'No.ICD 9',
            dataIndex	: 'kd_icd9',
            width		: 70,
			menuDisabled: true,
            hidden		: false,
			editor		: PenataJasaRJ.form.ComboBox.kdicd9= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.kdIcd9,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
    				dsTRDetailDiagnosaListIcd9.getRange()[line].data.kd_icd9=b.data.kd_icd9;
    				dsTRDetailDiagnosaListIcd9.getRange()[line].data.deskripsi=b.data.deskripsi;
    				PenataJasaRJ.gridIcd9.getView().refresh();
					
				},
				insert	: function(o){
					return {
						kd_icd9        		: o.kd_icd9,
						deskripsi 			: o.deskripsi,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_icd9+'</td><td width="200">'+o.deskripsi+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getIcd9",
				valueField: 'kd_icd9',
				displayField: 'text',
				listWidth: 250
			})
        },{
            id			: Nci.getId(),
            header		: 'Deskripsi ICD 9',
            dataIndex	: 'deskripsi',
			menuDisabled: true,
			width		: 200,
			editor		: PenataJasaRJ.form.ComboBox.deskripsi= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.deskripsi,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.gridIcd9.getSelectionModel().selection.cell[0];
    				dsTRDetailDiagnosaListIcd9.getRange()[line].data.kd_icd9=b.data.kd_icd9;
    				dsTRDetailDiagnosaListIcd9.getRange()[line].data.deskripsi=b.data.deskripsi;
    				PenataJasaRJ.gridIcd9.getView().refresh();
					
				},
				insert	: function(o){
					return {
						kd_icd9        		: o.kd_icd9,
						deskripsi 			: o.deskripsi,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_icd9+'</td><td width="200">'+o.deskripsi+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getIcd9",
				valueField: 'deskripsi',
				displayField: 'text',
				listWidth: 250
			})
        },{
            header: 'Urut',
            dataIndex: 'urut',
			hidden:true
        },
    ]);
}

function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',
        items:[
	        {
	            x:0,
	            y:60,
	            xtype: 'textarea',
	            id:'TxtHistoriDeleteDataPasien',
	            hideLabel: true,
	            name: 'msg',
	            anchor: '100% 100%' 
	        }
        ]
    });
}

function TambahBarisRWJ(){
    var x=true;
    if (x === true){
        var p = RecordBaruRWJ();
        dsTRDetailKasirRWJList.insert(dsTRDetailKasirRWJList.getCount(), p);
    }
}

function HapusBarisRWJ(){
    if ( cellSelecteddeskripsi != undefined ){
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != ''){
          	var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok'){
					variablehistori=combo;
					DataDeleteKasirRWJDetail();
					dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
				}
			});
               
        }else{
            dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        }
    }
}

function DataDeleteKasirRWJDetail(){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:  getParamDataDeleteKasirRWJDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoRWJ('Data berhasil dihapus', 'Information');
				deleteKasirRWJDetail_SQL();
                dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                cellSelecteddeskripsi=undefined;
                RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }else if (cst.success === false && cst.produktr  === true ){
                ShowPesanWarningRWJ('Produk Transfer Tidak dapat dihapus', nmHeaderHapusData);
				 RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					AddNewKasirRWJ = false;
            }else{
                ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
				 RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }
        }
    });
}

function getParamDataDeleteKasirRWJDetail(){
    var params ={
		Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirRWJ.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirRWJ.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitRWJ').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirRWJ.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirRWJ.data.data.HARGA,
		TrKdProduk :	 CurrentKasirRWJ.data.data.KD_PRODUK,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Hapus:2
    }
    return params;
}

function getParamDataupdateKasirRWJDetail(){
	console.log(CurrentKasirRWJ.data);
    var params ={
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Qty: CurrentKasirRWJ.data.data.QTY,
        Ubah:1
    };
    return params;
}

function GetDTLTRRWJGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Transaksi',
		id:'tabTransaksi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetDTLTRRWJGridSecond(data),GetDTLTRRWJGridFirst(data)],
		tbar:[
			{
				text	: 'Tambah item',
				id		: 'btnLookupRWJ',
				tooltip	: nmLookup,
				iconCls	: 'add',
				handler	: function(){
					PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanRWJ',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					var e=false;
					if(PenataJasaRJ.dsGridTindakan.getRange().length > 0){
						for(var i=0,iLen=PenataJasaRJ.dsGridTindakan.getRange().length; i<iLen ; i++){
							var o=PenataJasaRJ.dsGridTindakan.getRange()[i].data;
							if(o.QTY == '' || o.QTY==0 || o.QTY == null){
								PenataJasaRJ.alertError('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
								e=true;
								break;
							}
							
						}
					}else{
						PenataJasaRJ.alertError('Isi Tindakan Yang Diberikan','Peringatan');
						e=true;
					}
					for(var i=0,iLen=PenataJasaRJ.dsGridObat.getRange().length; i<iLen ; i++){
						var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
						if(o.nama_obat == '' || o.nama_obat == null){
							PenataJasaRJ.alertError('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
							PenataJasaRJ.alertError('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.cara_pakai == ''||  o.cara_pakai == null){
							PenataJasaRJ.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.verified == '' || o.verified==null){
							PenataJasaRJ.alertError('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
					}
					if(e==false){
						Datasave_KasirRWJ(false,false);
					}
				}
			},{
                id		:'btnHpsBrsRWJ',
                text	: 'Hapus item',
                tooltip	: 'Hapus Baris',
                iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailKasirRWJList.getCount() > 0 ){
                        if (cellSelecteddeskripsi != undefined){
                            if(CurrentKasirRWJ != undefined){
                                    HapusBarisRWJ();
                            }
                        }else{
                            ShowPesanWarningRWJ('Pilih record ','Hapus data');
                        }
                    }
                }
            },
			'-',
			{
                id		:'btnLookUpEditDokterPenindak_RWJ',
                text	: 'Edit Dokter Penindak',
                iconCls	: 'Edit_Tr',
                handler	: function(){
					if(currentJasaDokterKdProduk_RWJ == '' || currentJasaDokterKdProduk_RWJ == undefined){
						ShowPesanWarningIGD('Pilih item dokter penindak yang akan diedit!','Error');
					} else{
						if(currentJasaDokterUrutDetailTransaksi_RWJ == 0 || currentJasaDokterUrutDetailTransaksi_RWJ == undefined){
							ShowPesanErrorRWJ('Item ini belum ada dokter penindaknya!','Error');
						} else{
							PilihDokterLookUp_RWJ(true);
						}
					}
                }
            },
		]
    });
	return tabTransaksi;
}

function GetDTLTRRWJGridFirst(data){
	var fldDetail = ['kd_prd','nama_obat','jumlah','satuan','cara_pakai','kd_dokter','nama','verified','urut','racikan','jml_stok_apt','order_mng','id_mrresep'];
	dsPjTrans2 = new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetai2(data) ;
		PenataJasaRJ.pj_req__obt = new Ext.grid.EditorGridPanel({
        title: 'Resep Online',
		id:'PjTransGrid1',
        stripeRows: true,
        height: 160,
        store: dsPjTrans2,
        border: true,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
		listeners:{
			
		},
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
                    CurrentKasirRWJ.row = row;
                    CurrentKasirRWJ.data = cellSelecteddeskripsi;
					
					CurrentKasirRWJSelection=sm.selection.record;
					
					PenataJasaRJ.var_id_mrresep=CurrentKasirRWJSelection.data.id_mrresep;
					
                },
				beforecellselect: function(sm, row, rec){
					/* console.log(sm);
					console.log(row);
					console.log(rec); */
				},
            }
        }),
        cm: TRRawatJalanColumModel(),
        viewConfig:{forceFit: true},
        tbar:[
			{
				text: 'Tambah Obat',
				id: 'BtnTambahObatTrKasirRwj',
				iconCls: 'add',
				handler: function(){
					PenataJasaRJ.dsGridObat.insert(PenataJasaRJ.dsGridObat.getCount(),PenataJasaRJ.nullGridObat());
				}
			},{
				text: 'Hapus',
				id: 'BtnHapusObatTrKasirRwj',
				iconCls: 'RemoveRow',
				handler: function(){
					var line = PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0];
					var o = PenataJasaRJ.dsGridObat.getRange()[line].data;
					Ext.Msg.confirm('Warning', 'Apakah data obat ini akan dihapus?', function(button){
						if (button == 'yes'){
							if(PenataJasaRJ.dsGridObat.getRange()[line].data.kd_prd != undefined){
								Ext.Ajax.request
								(
									{
										url: baseURL + "index.php/main/functionRWJ/hapusBarisGridObat",
										params:{kd_prd:o.kd_prd, id_mrresep:PenataJasaRJ.var_id_mrresep,urut:o.urut} ,
										failure: function(o)
										{
											ShowPesanErrorRWJ('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												hapusBarisGridResepOnline_SQL(o.kd_prd,PenataJasaRJ.var_id_mrresep,o.urut);
												PenataJasaRJ.dsGridObat.removeAt(line);
												PenataJasaRJ.gridObat.getView().refresh();
											}
											else 
											{
												ShowPesanErrorRWJ('Gagal melakukan penghapusan', 'Error');
											};
										}
									}
									
								)
							}else{
								PenataJasaRJ.dsGridObat.removeAt(line);
								PenataJasaRJ.gridObat.getView().refresh();
							}
						} 
						
					}); 
					/* Ext.Msg.show({
	                   title:nmHapusBaris,
	                   msg: 'Anda yakin akan menghapus data Obat ini?',
	                   buttons: Ext.MessageBox.YESNO,
	                   fn: function (btn)
	                   {
	                       if (btn =='yes')
	                        {
								PenataJasaRJ.dsGridObat.removeAt(PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0]);
								PenataJasaRJ.gridObat.getView().refresh();
	                        }
	                   },
	                   icon: Ext.MessageBox.QUESTION
	                }); */
				}
			}
        ]}
    );
    PenataJasaRJ.gridObat=PenataJasaRJ.pj_req__obt;
    PenataJasaRJ.dsGridObat=dsPjTrans2;
    return PenataJasaRJ.pj_req__obt;
}

function GetDTLTRRWJGridSecond(){
	var fldDetail	= ['KD_PRODUK','DESKRIPSI','QTY','DOKTER','TGL_TINDAKAN','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','KD_TARIF','HARGA','JUMLAH_DOKTER','JUMLAH','DOKTER'];
	dsTRDetailKasirRWJList	= new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetail() ;
    PenataJasaRJ.dsGridTindakan	= dsTRDetailKasirRWJList;
    PenataJasaRJ.form.Grid.produk	= new Ext.grid.EditorGridPanel({
        title		: 'Item Transaksi',
		id			: 'PjTransGrid2',
		stripeRows	: true,
		height		: 130,
        store		: PenataJasaRJ.dsGridTindakan,
        border		: true,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        sm			: new Ext.grid.CellSelectionModel({
	        singleSelect: true,
	        listeners	: {
	            cellselect	: function(sm, row, rec){
	                cellSelecteddeskripsi	= dsTRDetailKasirRWJList.getAt(row);
	                CurrentKasirRWJ.row	= row;
	                CurrentKasirRWJ.data	= cellSelecteddeskripsi;
					
					currentJasaDokterKdTarif_RWJ=cellSelecteddeskripsi.data.KD_TARIF;
					currentJasaDokterKdProduk_RWJ=cellSelecteddeskripsi.data.KD_PRODUK;
					currentJasaDokterUrutDetailTransaksi_RWJ=cellSelecteddeskripsi.data.URUT;
					currentJasaDokterHargaJP_RWJ=cellSelecteddeskripsi.data.HARGA;
	            }
	        },
			
        }),
        cm			: TRRawatJalanColumModel2(),
        viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				cellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
        	//console.log(columnIndex);
			console.log(PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.JUMLAH);
			if(columnIndex == 7 && (PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.JUMLAH != '' || PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.JUMLAH_DOKTER != '' )){
				//alert('test');
        	}
   		  }
		}
    });
    
    return PenataJasaRJ.form.Grid.produk;
};

function TRRawatJalanColumModel(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: Nci.getId(),
        	header		: 'KD.Obat',
            dataIndex	: 'kd_prd',
            width		: 80,
			menuDisabled: true,
            hidden		: false
        },{
			id			: Nci.getId(),
        	header		: 'Nama Obat',
            dataIndex	: 'nama_obat',
            width		: 150,
			menuDisabled: true,
            hidden		: false,
            editor		: PenataJasaRJ.comboObat()
        },{
           id			: Nci.getId(),
            header		: 'Qty',
            dataIndex	: 'jumlah',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
            width		: 50,
            editor		: new Ext.form.NumberField({
				id				: 'txtQty',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%',
				listeners       :{
								blur: function(a){
									var line	= this.index;
							       	if(a.getValue()==0){
								ShowPesanWarningRWJ('Qty obat belum di isi', 'Warning');
								}else{
								hasilJumlah_rwj(a.getValue());
								}
									
								},
								focus: function(a){
								
									this.index=PenataJasaRJ.pj_req__obt.getSelectionModel().selection.cell[0]
								}
						
					}
			})
        },{
            id			: Nci.getId(),
            header		: 'stok',
            dataIndex	: 'jml_stok_apt',
            sortable	: false,
            hidden		: true,
			menuDisabled: true,
            width		: 50
           
        },{
            id			: Nci.getId(),
            header		: 'Satuan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'satuan'	
        },{
			id			: Nci.getId(),
            header		: 'Cara Pakai',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'cara_pakai'	,
        	editor		: new Ext.form.TextField({
				id				: 'txtcarapakai',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: Nci.getId(),
            header		: 'Dokter',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'nama',
			hidden		: true
        },{
            id			: Nci.getId(),
            header		: 'Verified',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'verified',
            editor		: PenataJasaRJ.ComboVerifiedObat()
        },{
			id			: Nci.getId(),
            header		: 'Racikan',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'racikan',
            editor		: new Ext.form.NumberField({
				id				: 'txtRacikan',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: Nci.getId(),
            header		: 'Order',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'order_mng',
			renderer	: function(v, params, record)
			{
				if  (record.data.order_mng==='Dilayani')
				{
					Ext.getCmp('BtnTambahObatTrKasirRwj').disable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').disable();
				}
				else
				{
					Ext.getCmp('BtnTambahObatTrKasirRwj').enable();
					Ext.getCmp('BtnHapusObatTrKasirRwj').enable();
				}
				
				return record.data.order_mng;
			}
        },
		{
            id			: Nci.getId(),
            header		: 'urut',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'urut'
        },
    ]);
}

function TRRawatJalanColumModel2(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
        	 id				: 'coleskripsirwj2',
        	 header			: 'Uraian',
        	 dataIndex		: 'DESKRIPSI2',
        	 menuDisabled	: true,
        	 hidden 		: true
        },{
        	id				: 'colKdProduk2',
            header			: 'Kode Produk',
            dataIndex		: 'KD_PRODUK',
            width			: 100,
			menuDisabled	: true,
            hidden			: true
        },{
        	id			: 'colDeskripsiRWJ2',
            header		:'Item Transaksi',
            dataIndex	: 'DESKRIPSI',
            sortable	: false,
            hidden		:false,
			menuDisabled:true,
            width		:200,
            editor		: PenataJasaRJ.form.ComboBox.produk= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.produk,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_PRODUK=b.data.kd_produk;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.DESKRIPSI=b.data.deskripsi;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_TARIF=b.data.kd_tarif;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.HARGA=b.data.harga;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.TGL_BERLAKU=b.data.tglberlaku;
					PenataJasaRJ.dsGridTindakan.getRange()[line].data.JUMLAH=b.data.jumlah;
					console.log(b);
					console.log(PenataJasaRJ.dsGridTindakan.getRange()[line]);
    				PenataJasaRJ.form.Grid.produk.getView().refresh();
					
					var currenturut= line + 1;
					cekKomponen(b.data.kd_produk,b.data.kd_tarif,Ext.getCmp('txtKdUnitRWJ').getValue(),currenturut,b.data.harga);
				},
				param	: function(){
					var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit']=o.KD_UNIT;
					params['kd_customer']=o.KD_CUSTOMER;
					return params;
				},
				insert	: function(o){
					return {
						kd_produk        :o.kd_produk,
						deskripsi 		: o.deskripsi,
						harga			: o.tarifx,
						kd_tarif		: o.kd_tarif,
						tglberlaku		: o.tglberlaku,
						jumlah			: o.jumlah,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_produk+'</td><td width="160" align="left">'+o.deskripsi+'</td><td width="130">'+o.klasifikasi+'</td></tr></table>'
						//text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_produk+'</td><td width="50">'+o.deskripsi+'</td></tr><td width="80">'+o.klasifikasi+'</td></table>'
				    }
				},
				url		: baseURL + "index.php/main/functionRWJ/getProduk",
				valueField: 'deskripsi',
				displayField: 'text',
			})
	        },{
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width:100,
		   		menuDisabled:true,
               renderer: function(v, params, record){
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },{
                id: 'colHARGARWj2',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
				renderer: function(v, params, record){
					return formatCurrency(record.data.HARGA);
				}	
            },{
                id: 'colProblemRWJ2',
                header: 'Qty',
               width:'100%',
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
				width:50,
                editor: new Ext.form.TextField({
                    id:'fieldcolProblemRWJ2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:50,
					listeners:{ 
						'specialkey' : function(){
							//Dataupdate_KasirRWJ(false);
						}
					}
                })
            },
			{
                id: 'colDokterPoli',
                header: 'Dokter',
				align: 'left',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'DOKTER',
            },
			{
                id: 'colDokterPoli',
                header: 'status',
				align: 'left',
				hidden: true,
				width:50,
				menuDisabled:true,
                dataIndex: 'JUMLAH',
				editor:
				{
					xtype:'textfield',
				}
            },
			{
                id: 'colImpactRWJ2',
                header: 'CR',
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField({
                    id:'fieldcolImpactRWJ2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30
                })
            }
        ]
    );
};

function PilihDokterLookUp_RWJ(edit) 
{
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			header			: 'kd_component',
			dataIndex		: 'kd_component',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'tgl_berlaku',
			dataIndex		: 'tgl_berlaku',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'Komponent',
			dataIndex		: 'component',
			width			: 200,
			menuDisabled	: true,
			hidden 		: false
        },
        {
			header			: 'kd_dokter',
			dataIndex		: 'kd_dokter',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },{
            header			:'Nama Dokter',
            dataIndex		: 'nama',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 250,
            editor			: new Nci.form.Combobox.autoComplete({
				store	: dsgridpilihdokterpenindak_RWJ,
				select	: function(a,b,c){
					var line	= GridDokterTr_RWJ.getSelectionModel().selection.cell[0];
					dsGridJasaDokterPenindak_RWJ.getRange()[line].data.kd_dokter=b.data.kd_dokter;
					dsGridJasaDokterPenindak_RWJ.getRange()[line].data.nama=b.data.nama;
					GridDokterTr_RWJ.getView().refresh();
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
				valueField: 'nama_obat',
				displayField: 'text',
				listWidth: 380
			})
			//getTrDokter(dsTrDokter)
	    },
        ]
    );
	
	var fldDetail = [];
    dsGridJasaDokterPenindak_RWJ = new WebApp.DataStore({ fields: fldDetail });
	GridDokterTr_RWJ= new Ext.grid.EditorGridPanel({
		id			: 'GridDokterTr_RWJ',
		stripeRows	: true,
		width		: 487,
		height		: 160,
        store		: dsGridJasaDokterPenindak_RWJ,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				//trcellCurrentTindakan_RWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
 
	
    var lebar = 500;
    var FormLookUDokter_RWJ = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_RWJ',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				GridDokterTr_RWJ
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						savetransaksi();
					}
				},
				'-',
			],
            listeners:
            { 
            }
        }
    );
	FormLookUDokter_RWJ.show();
	if(edit == true){
		GetgridEditDokterPenindakJasa_RWJ()
	} else{
		GetgridPilihDokterPenindakJasa_RWJ(currentJasaDokterKdProduk_RWJ,currentJasaDokterKdTarif_RWJ);
	}
	
};

function GetLookupAssetCMRWJ(str){
	if (AddNewKasirRWJ === true){
		var p = new mRecordRwj({
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT
		});
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,true,'',false);
	}else{	
		var p = new mRecordRwj({
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
			'DESKRIPSI':'', 
			'KD_TARIF':'', 
			'HARGA':'',
			'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
			'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
			'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
			'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
			'URUT':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT
		});
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,false,CurrentKasirRWJ.row,false);
	};
};

function RecordBaruRWJ(){
	var p = new mRecordRwj({
		'DESKRIPSI2':'',
		'KD_PRODUK':'',
	    'DESKRIPSI':'', 
	    'KD_TARIF':'', 
	    'HARGA':'',
	    'QTY':'',
	    'TGL_TRANSAKSI':tanggaltransaksitampung, 
	    'DESC_REQ':'',
	    'KD_TARIF':'',
	    'URUT':''
	});
	return p;
}

function RefreshDataSetDiagnosa(medrec,unit,tgl){	
 	var strKriteriaDiagnosa='';
    strKriteriaDiagnosa = 'A.kd_pasien = ~' + medrec + '~ and A.kd_unit=~'+unit+'~ and A.tgl_masuk in(~'+tgl+'~)';
	dsTRDetailDiagnosaList.load({ 
		params:{ 
			Skip: 0, 
			Take: selectCountDiagnosa, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewDiagnosaRJPJ',
			param: strKriteriaDiagnosa
		} 
	});
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
}

function RefreshDataSetAnamnese(){	
 	var strKriteriaDiagnosa='';
   strKriteriaDiagnosa = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksi').getValue() + '~ and mr_konpas.kd_unit=~'+Ext.get('txtKdUnitRWJ').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi').dom.value+'~';
	dsTRDetailAnamneseList.load({ 
		params:{ 
			Skip: 0, 
			Take: 50, 
            Sort: 'orderlist',
			Sortdir: 'ASC', 
			target:'viewkondisifisik',
			param: strKriteriaDiagnosa
		} 
	});
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
}

function TRRWJInit(rowdata){
	console.log(rowdata);
    AddNewKasirRWJ = false;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitRWJ').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
	Ext.get('txtareaAnamnesis').dom.value = rowdata.ANAMNESE;
	Ext.get('txtareaAnamnesiscatatan').dom.value = rowdata.CAT_FISIK;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	currentKdKasirRWJ = rowdata.KD_KASIR;
	Ext.Ajax.request(
	{
	    url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
		 params: {
	        kd_customer: vkode_customer,
	    },
		failure: function(o)
		{},	    
	    success: function(o) {
			 var cst = Ext.decode(o.responseText);
			PenataJasaRJ.varkd_tarif=cst.kd_tarif;
			dokter_leb_rwj();
			dokter_rad_rwj();
			getproduk_PJRWJ();
			
			 }
	});
	
	Ext.Ajax.request(
	{
	    url:  baseURL + "index.php/rawat_jalan/functionRWJ/cek_order_mng",
		 params: {
	        kd_pasien: rowdata.KD_PASIEN,
			kd_unit: rowdata.KD_UNIT,
			tgl_masuk_knj: rowdata.TANGGAL_TRANSAKSI
	    },
		failure: function(o)
		{},	    
	    success: function(o) {
			 var cst = Ext.decode(o.responseText);
				if (cst.success===true)
				{
					var jam_op = cst.jam_op.split(":");
					Ext.getCmp('TglOperasi_viJdwlOperasi').setValue(ShowDate(cst.tgl_op));
					Ext.getCmp('txtJam_viJdwlOperasi').setValue(jam_op[0]);
					Ext.getCmp('txtMenit_viJdwlOperasi').setValue(jam_op[1]);
					Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').setValue(cst.kd_tindakan);
					Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').setValue(cst.no_kamar);
					
				}
			
			 }
	});
	
	ViewGridBawahpoliLab(rowdata.NO_TRANSAKSI,Ext.getCmp('txtKdUnitRWJ').getValue(),rowdata.TANGGAL_TRANSAKSI,rowdata.URUT_MASUK);
	ViewGridBawahpoliRad(rowdata.NO_TRANSAKSI,Ext.getCmp('txtKdUnitRWJ').getValue(),rowdata.TANGGAL_TRANSAKSI,rowdata.URUT_MASUK);
	RefreshDataKasirRWJDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetAnamnese();
	var $this	= this;
	//PenataJasaIGD.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
	PenataJasaRJ.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ',
			kdcustomer: vkode_customer
		} 
	});
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);

		Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText
			//alert(tampungshiftsekarang);
			
	    }
	});
	viewGridIcd9_RWJ();
	viewGridRiwayatKunjunganPasien_RWJ();
}



function mEnabledRWJCM(mBol){
	 Ext.get('btnLookupRWJ').dom.disabled=mBol;
	 Ext.get('btnHpsBrsRWJ').dom.disabled=mBol;
}

function RWJAddNew() {
    AddNewKasirRWJ = true;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirRwj').dom.value= '';
	rowSelectedKasirRWJ=undefined;
	dsTRDetailKasirRWJList.removeAll();
	mEnabledRWJCM(false);
	

}

function RefreshDataKasirRWJDetai2(data){
    dsPjTrans2.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'kd_obat',
		    Sortdir: 'ASC',
		    target: 'ViewResepRWJ',
		    param: "KD_PASIEN='"+data.KD_PASIEN+"' AND KD_UNIT = '"+data.KD_UNIT+"' AND TGL_MASUK = '"+data.TANGGAL_TRANSAKSI+"'"
		}
	});
    return dsPjTrans2;
}

function RefreshDataKasirRWJDetail(no_transaksi) {
    var strKriteriaRWJ='';
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~01~";
    dsTRDetailKasirRWJList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewDetailRWJGridBawah',
		    param: strKriteriaRWJ
		}
	});
    return dsTRDetailKasirRWJList;
}

function getParamDetailTransaksiRWJ(){
    var params={
		Table			: 'ViewTrKasirRwj',
		TrKodeTranskasi	: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit			: Ext.get('txtKdUnitRWJ').getValue(),
		kdDokter		: Ext.get('txtKdDokter').getValue(),
		Tgl				: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
		Shift			: tampungshiftsekarang,
		List			: getArrDetailTrRWJ(),
		JmlField		: mRecordRwj.prototype.fields.length-4,
		JmlList			: GetListCountDetailTransaksi(),
		Hapus			: 1,
		Ubah			: 0
	};
    params.jmlObat	= PenataJasaRJ.dsGridObat.getRange().length;
    params.urut_masuk	= PenataJasaRJ.s1.data.URUT_MASUK;
    params.kd_pasien	= PenataJasaRJ.s1.data.KD_PASIEN;
	if(PenataJasaRJ.dsGridObat.getCount() > 0){
		for(var i=0, iLen= params.jmlObat ;i<iLen;i++){
			var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
			params['kd_prd'+i]	= o.kd_prd;
			params['jumlah'+i]	= o.jumlah;
			params['cara_pakai'+i]	= o.cara_pakai;
			params['verified'+i]	= o.verified;
			params['racikan'+i]	= o.racikan;
			params['kd_dokter'+i]	= o.kd_dokter;
			params['urut' + i]  = o.urut;
		}
		params['resep']  = true;
	} else{
		params['resep']  = false;
	}
    return params;
}

function getParamDetailAnamnese() {
    var params ={
		Table:'viewkondisifisik',
		KdPasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk : Ext.get('txtKdUrutMasuk').getValue(),
		Anamnese:Ext.get('txtareaAnamnesis').getValue(),
		Catatan:Ext.get('txtareaAnamnesiscatatan').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		List:getArrdetailAnamnese()
	};
    return params;
}

function getParamKonsultasi() {
    var params ={
		Table:'ViewTrKasirRwj', 
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRWJ').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer,
		UrutMasuk:Ext.getCmp('txtKdUrutMasuk').getValue()
	};
    return params;
}

function GetListCountDetailTransaksi(){
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++){
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI  != ''){
			x += 1;
		}
	}
	return x;
}

function getTotalDetailProduk(){
	var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++){		
		var recordterakhir;
		var y='';
		var z='@@##$$@@';
		recordterakhir=dsTRDetailKasirRWJList.data.items[i].data.DESC_REQ;
		TotalProduk=TotalProduk+recordterakhir;
		Ext.get('txtJumlah1EditData_viKasirRwj').dom.value=formatCurrency(TotalProduk);
		if (i === (dsTRDetailKasirRWJList.getCount()-1)){
			x += y ;
		}else{
			x += y + '##[[]]##';
		}
	}	
	return x;
}

function getArrDetailTrRWJ(){
	var x='';
	for(var i = 0 ; i < PenataJasaRJ.dsGridTindakan.getCount();i++){
		if (PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK != '' && PenataJasaRJ.dsGridTindakan.data.items[i].data.DESKRIPSI != ''){
			var y='';
			var z='@@##$$@@';
			y = 'URUT=' + PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
			y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK;
			y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.QTY;
			y += z + ShowDate(PenataJasaRJ.dsGridTindakan.data.items[i].data.TGL_BERLAKU);
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.HARGA;
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_TARIF;
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
			y += z + ShowDate(PenataJasaRJ.dsGridTindakan.data.items[i].data.TGL_TRANSAKSI);
			if (i === (PenataJasaRJ.dsGridTindakan.getCount()-1)){
				x += y ;
			}else{
				x += y + '##[[]]##';
			}
		}
	}	
	return x;
}

function getItemPanelInputRWJ(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:[
					getItemPanelNoTransksiRWJ(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
}

function getItemPanelUnit(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitRWJ',
					    id: 'txtKdUnitRWJ',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelDokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners:{ 
							
						}
					}
				]
			},{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						readOnly:true,
					    anchor: '100%'
					},{
						xtype: 'textfield',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelNoTransksiRWJ(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirrwj',
					    id: 'txtNoTransaksiKasirrwj',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPaneltgl_filter(){
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterRWJ',
					    name: 'dtpTglAwalFilterRWJ',
					    value: now,
					    anchor: '99%',
						format: 'd/M/Y',
						altFormats: 'dmy',
					    listeners:{
						 	'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 ){
									RefreshDataFilterKasirRWJ();
								}
							}
						}
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[ 
					{
					    xtype: 'datefield',
					    id: 'dtpTglAkhirFilterRWJ',
					    name: 'dtpTglAkhirFilterRWJ',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '99%',
					   	listeners:{
						 	'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
									RefreshDataFilterKasirRWJ();
								}
							}
						}
					}
				]
			}
		]
	};
    return items;
};
function getItemcombo_filter() {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .23,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					mComboStatusBayar_viKasirRwj()
				]
			},{
			    columnWidth: .26,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:[ 
					mComboUnit_viKasirRwj()
				]
			},
			{
			    columnWidth: .21,
			    layout: 'form',
			    border: false,
				labelWidth:100,
			    items:[ 
					mComboStatusPeriksa_viKasirRwj()
				]
			}
		]
	};
    return items;
}

function getItemPanelmedrec(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	};
    return items;
};


function RefreshDataKasirRWJ() {
    dsTRKasirRWJList.load({
        params:{
            Skip: 0,
            Take: selectCountKasirRWJ,
            Sort: 'tgl_transaksi',
            Sortdir: 'ASC',
            target:'ViewTrKasirRwj',
            param : ''
        }		
    });
    rowSelectedKasirRWJ = undefined;
    return dsTRKasirRWJList;
}

function refeshkasirrwj(){
	dsTRKasirRWJList.load({ 
		params:{   
			Skip: 0, 
			Take: selectCountKasirRWJ, 
             Sort: '',
			Sortdir: 'ASC', 
			target:'ViewTrKasirRwj',
			param : ''
		}			
	});   
	return dsTRKasirRWJList;
}
function refreshDataDepanPenjasRWJ()
{
	var KataKunci='';
 	if (Ext.get('txtFilterNomedrec').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}
	}
 	if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}
	}
 	if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'All'){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_unit)like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_unit) like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = TRUE';
		}else{
           	KataKunci += ' and posting_transaksi =  TRUE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = FALSE';
		}else{
            KataKunci += ' and posting_transaksi =  FALSE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Semua'){
		if (KataKunci == ''){
            KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}else{
            KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
	}
	if (Ext.get('dtpTglAwalFilterRWJ').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " and (tgl_transaksi between '" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "' and '" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "')";
		}else{
            KataKunci += " and (tgl_transaksi between '" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "' and '" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "')";
		}
	}
	if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() != ''){
		var statperiksa;
		if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() == 'Semua'){
			statperiksa = "";
		} else if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() == 'Sudah Periksa'){
			statperiksa = " and status_periksa = 1";
		} else{
			statperiksa = "and status_periksa <> 1";
		}
		
		if (KataKunci == ''){   
			KataKunci = statperiksa;
		}else{
            KataKunci += statperiksa;
		}
	}
	Ext.Ajax.request({
			url: baseURL + "index.php/main/functionRWJ/refreshdatagridpenatajasaRWJ",					
			params: {KataKunciNya:KataKunci,},
			failure: function(o){
				ShowPesanWarningRWJ('Load data gagal', 'Gagal');
				//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					dsTRKasirRWJList.removeAll();
					var recs=[],
						recType=dsTRKasirRWJList.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRKasirRWJList.add(recs);
					
					grListTRRWJ.getView().refresh();
					//refeshkasirrwj();
				}else {
					ShowPesanWarningRWJ('Load data gagal', 'Gagal');
				}
			}
		});
}
function RefreshDataFilterKasirRWJ() {
	var KataKunci='';
 	if (Ext.get('txtFilterNomedrec').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}
	}
 	if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}
	}
 	if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'All'){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_unit)like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_unit) like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = TRUE';
		}else{
           	KataKunci += ' and posting_transaksi =  TRUE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = FALSE';
		}else{
            KataKunci += ' and posting_transaksi =  FALSE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Semua'){
		if (KataKunci == ''){
            KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}else{
            KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
	}
	if (Ext.get('dtpTglAwalFilterRWJ').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}else{
            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}
	}
	if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() != ''){
		var statperiksa;
		if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() == 'Semua'){
			statperiksa = "";
		} else if(Ext.get('cboStatusPeriksa_viKasirRwj').getValue() == 'Sudah Periksa'){
			statperiksa = " and status_periksa = 1";
		} else{
			statperiksa = "and status_periksa <> 1";
		}
		
		if (KataKunci == ''){   
			KataKunci = statperiksa;
		}else{
            KataKunci += statperiksa;
		}
	}
    if (KataKunci != undefined ||KataKunci != '' ){  
		dsTRKasirRWJList.load({ 
			params:{   
				Skip: 0, 
				Take: selectCountKasirRWJ, 
                Sort: 'tgl_transaksi',
				Sortdir: 'ASC', 
				target:'ViewTrKasirRwj',
				param : KataKunci
			}			
		});  
		getTotKunjunganRWJ();
    }else{
		getTotKunjunganRWJ();
		dsTRKasirRWJList.load({ 
			params:{   
				Skip: 0, 
				Take: selectCountKasirRWJ, 
                Sort: 'tgl_transaksi',
				Sortdir: 'ASC', 
				target:'ViewTrKasirRwj',
				param : ''
			}			
		});   
	}
	return dsTRKasirRWJList;
}

function Datasave_Konsultasi(mBol,callback) {	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionRWJ/KonsultasiPenataJasa",					
			params: getParamKonsultasi(),
			failure: function(o){
				ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					if(callback != undefined)callback(cst.id);
						ShowPesanInfoRWJ('Konsultasi berhasil disimpan','Information');
						Datasave_Konsultasi_SQL(cst.notrans)
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
					//refeshkasirrwj();
				}else {
					ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function Datasave_KasirRWJ(mBol,jasa){	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url			: baseURL + "index.php/main/functionRWJ/savedetailpenyakit",
			params		: getParamDetailTransaksiRWJ(),
			failure		: function(o){
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},
			success		: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoRWJ("Data berhasil disimpan",'Information');
					//RefreshDataFilterKasirRWJ();
					updateStatusPeriksa();
					Datasave_KasirRWJ_SQL(cst.id_mrresep,jasa);
					PenataJasaRJ.var_id_mrresep=cst.id_mrresep;
					
					var paramresep ={
						KD_PASIEN:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
						KD_UNIT:Ext.getCmp('txtKdUnitRWJ').getValue(),
						TANGGAL_TRANSAKSI:Ext.get('dtpTanggalDetransaksi').getValue()
					};
					RefreshDataKasirRWJDetai2(paramresep);
					
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
					
					//refreshDataDepanPenjasRWJ();
					
					if(jasa == true){
						SimpanJasaDokterPenindak(currentJasaDokterKdProduk_RWJ,currentJasaDokterKdTarif_RWJ,currentJasaDokterUrutDetailTransaksi_RWJ,currentJasaDokterHargaJP_RWJ);
					}
				}else{
					ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}
function getParamok(){
	var jamOp=Ext.getCmp('txtJam_viJdwlOperasi').getValue()+":"+Ext.getCmp('txtMenit_viJdwlOperasi').getValue()+":00";
    var params={
		TrKodeTranskasi	: Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		KdUnit			: Ext.getCmp('txtKdUnitRWJ').getValue(),
		kdDokter		: Ext.getCmp('txtKdDokter').getValue(),
		jam_op			: jamOp,
		
		tindakan		: Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').getValue(),
		kamar			: Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue(),
		tgl_operasi		:Ext.getCmp('TglOperasi_viJdwlOperasi').getValue(),
		kasir			:'rawat jalan'
		
		
	};
   return params;
}

function Datasave_ok(mBol){	
		Ext.Ajax.request({
			url			: baseURL + "index.php/kamar_operasi/functionKamarOperasi/save_order_mng",
			params		: getParamok(),
			failure		: function(o){
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan Hubungi admin', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},
			success		: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoRWJ('Jadwal berhasil dibuat','Information');
					//RefreshDataFilterKasirRWJ();
					var paramresep ={
						KD_PASIEN:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
						KD_UNIT:Ext.getCmp('txtKdUnitRWJ').getValue(),
						TANGGAL_TRANSAKSI:Ext.get('dtpTanggalDetransaksi').getValue()
					};
					RefreshDataKasirRWJDetai2(paramresep);
					DataSaveKamarOperasi_SQL(mBol);
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else if(cst.success === false && cst.cari === true ){
				ShowPesanWarningRWJ('Pasien telah terjadwal di Kamar operasi ', 'Gagal');
				}
				else{
					ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
				}
			}
		});

}

function Dataupdate_KasirRWJ(mBol) {	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/CreateDataObj",
			params: getParamDataupdateKasirRWJDetail(),
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoRWJ('Data berhasil disimpan', 'Information');
					RefreshDataKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else if  (cst.success === false && cst.pesan===0){
					ShowPesanWarningRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
				}else if (cst.success === false && cst.pesan===1){
					ShowPesanWarningRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
				}else {
					ShowPesanErrorRWJ(nmPesanSimpanError,nmHeaderSimpanData);
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function ValidasiEntryCMRWJ(modul,mBolHapus){
	var x = 1;
	if((Ext.get('txtNoTransaksiKasirrwj').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirRWJList.getCount() === 0 || (Ext.get('txtKdDokter').dom.value  === undefined )){
		if (Ext.get('txtNoTransaksiKasirrwj').getValue() == '' && mBolHapus === true){
			x = 0;
		}else if (Ext.get('txtNoMedrecDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}else if (Ext.get('txtNamaPasienDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}else if (Ext.get('dtpTanggalDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokter').dom.value  === undefined){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}else if (dsTRDetailKasirRWJList.getCount() === 0){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		}
	}
	return x;
}

function ValidasiEntryKonsultasi(modul,mBolHapus){
	var x = 1;
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') ){
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true){
			x = 0;
		}else if (Ext.get('cboDokterRequestEntry').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	}
	return x;
}

function ShowPesanWarningRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

PenataJasaRJ.alertError	= function(str, modul){
	Ext.MessageBox.show({
	    title	: modul,
	    msg		: str,
	    buttons	: Ext.MessageBox.OK,
	    icon	: Ext.MessageBox.ERROR,
		width	: 250
	});
};

function ShowPesanErrorRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
}

function DataDeleteKasirRWJ(){
   if (ValidasiEntryCMRWJ(nmHeaderHapusData,true) == 1 ){
        Ext.Msg.show({
           title:nmHeaderHapusData,
           msg: nmGetValidasiHapus(nmTitleFormRequest) ,
           buttons: Ext.MessageBox.YESNO,
           width:275,
           fn: function (btn){
                if (btn =='yes'){
                    Ext.Ajax.request({
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDetailTransaksiRWJ(),
                        success: function(o){
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                ShowPesanInfoRWJ('Data berhasil dihapus', 'Information');
                                RefreshDataKasirRWJ();
                                RWJAddNew();
                            }else if (cst.success === false && cst.pesan===0){
                                ShowPesanWarningRWJ(nmPesanHapusGagal,nmHeaderHapusData);
                            }else if (cst.success === false && cst.pesan===1){
                                ShowPesanWarningRWJ(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                            }else{
                                ShowPesanErrorRWJ(nmPesanHapusError,nmHeaderHapusData);
                            }
                        }
                    });
                }
            }
        });
    }
}

function GantiDokterLookUp(mod_id) {
    var FormDepanDokter = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Ganti Dokter',
        border: false,
        shadhow: true,
        autoScroll:false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [DokterLookUp()],
        listeners:{
            'afterrender': function()
            {}
        }
    });
   	return FormDepanDokter;
}

function DokterLookUp(rowdata) {
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window({
        id: 'gridDokter',
        title: 'Ganti Dokter',
        closeAction: 'destroy',
        width: lebar,
        height: 180,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: getFormEntryDokter(lebar),
        listeners:{
            activate: function(){
                 if (varBtnOkLookupEmp === true){
                    Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                    Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                    varBtnOkLookupEmp=false;
                }
            },afterShow: function(){
                this.activate();
            },deactivate: function(){
                rowSelectedKasirDokter=undefined;
                // RefreshDataFilterKasirDokter();
            }
        }
    });
    FormLookUpGantidokter.show();
}

function getItemPanelButtonGantidokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig:{
					align: 'middle',
					pack:'end'
				},
				items:[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function(){
							GantiDokter(false);
							//FormLookUpGantidokter.close();	
						}
					},{
						xtype:'button',
						text:'Tutup' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelGantidokter',
						handler:function(){
							FormLookUpGantidokter.close();
						}
					}
				]
			}
		]
	};
    return items;
}

function getFormEntryDokter(lebar) {
    var pnlTRGantiDokter = new Ext.FormPanel({
        id: 'PanelTRDokter',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        bodyStyle: 'padding:10px 10px 10px 10px',
        height:190,
        anchor: '100%',
        width: lebar,
        border: false,
        items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
       	tbar:[]
    });
    var FormDepanDokter = new Ext.Panel({
	    id: 'FormDepanDokter',
	    region: 'center',
	    width: '100%',
	    anchor: '100%',
	    layout: 'form',
	    title: '',
	    bodyStyle: 'padding:15px',
	    border: true,
	    bodyStyle: 'background:#FFFFFF;',
	    shadhow: true,
	    items: [pnlTRGantiDokter]

	});
    return FormDepanDokter;
};

function getItemPanelInputGantidokter(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:[getItemPanelNoTransksiDokter(lebar)]
			}
		]
	};
    return items;
}

function ValidasiEntryTutupDokter(modul,mBolHapus){
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == '')){
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true){
			x=0;
		}else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === ''){
			x=0;
			if ( mBolHapus === false ){
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			}
		}
	}
	return x;
}

function getItemPanelNoTransksiDokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
}

function mComboDokterGantiEntry(){ 
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitRWJ').getValue();
	var kddokter = Ext.get('txtKdDokter').getValue();
   	dsDokterGantiEntry.load({
     	params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokter',
			param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
		}
    });
    var cboDokterGantiEntry = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterGantiEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				selectDokter = b.data.KD_DOKTER;
				NamaDokter = b.data.NAMA;
				Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER;
            },
            'render': function(c){
                c.getEl().on('keypress', function(e) {
                if(e.getKey() == 13)
                	Ext.getCmp('kelPasien').focus();
                }, c);
            }
		}
    });
    return cboDokterGantiEntry;
};

function GantiDokter(mBol){
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 ){
        Ext.Ajax.request({
           	url: WebAppUrl.UrlUpdateData,
        	params: getParamGantiDokter(),
			failure: function(o){
				 ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
			},	
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
					GantiDokter_SQL();
                    ShowPesanInfoRWJ('Berhasil ganti dokter','Ganti Dokter');
					//refreshDataDepanPenjasRWJ();
					//refeshkasirrwj();
					Ext.get('txtKdDokter').dom.value = selectDokter;
					Ext.get('txtNamaDokter').dom.value = NamaDokter;
                    FormLookUpGantidokter.close();
					
                }else if  (cst.success === false && cst.pesan===0){
                        ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                }else{
                        ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                }
            }
        });
    }else{
        if(mBol === true){
			return false;
        }
    }
}

function ValidasiGantiDokter(modul,mBolHapus){
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == '')){
	  	if (Ext.get('cboDokterRequestEntry').getValue() === ''){
			x=0;
			if ( mBolHapus === false ){
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			}
		}
	}
	return x;
}

function getParamGantiDokter(){
    var params ={
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter : Ext.getCmp('cboDokterRequestEntry').getValue(),
		kodebagian : 2,
		urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue()
	};
    return params;
};

function KelompokPasienLookUp(rowdata) {
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window({
        id: 'gridKelompokPasien',
        title: 'Ganti Kelompok Pasien',
        closeAction: 'destroy',
        width: lebar,
        height: 260,
        border: false,
        resizable: false,
        plain: false,
        constrain: true,
        layout: 'fit',
        iconCls: 'Request',
        modal: true,
        items: getFormEntryTRKelompokPasien(lebar),
		fbar:[
			{
				xtype:'button',
				text:'Simpan',
				width:70,
				style:{'margin-left':'0px','margin-top':'0px'},
				hideLabel:true,
				id: 'btnOkKelompokPasien',
				handler:function(){
					Datasave_Kelompokpasien();
				}
			},{
				xtype:'button',
				text:'Tutup',
				width:70,
				hideLabel:true,
				id: 'btnCancelKelompokPasien',
				handler:function(){
					FormLookUpsdetailTRKelompokPasien.close();
				}
			}
		],
        listeners:{
            
        }
    });
    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();
}

function getFormEntryTRKelompokPasien(lebar) {
    var pnlTRKelompokPasien = new Ext.FormPanel({
		id: 'PanelTRKelompokPasien',
		fileUpload: true,
		region: 'north',
		layout: 'column',
		bodyStyle: 'padding:10px 10px 10px 10px',
		height:250,
		anchor: '100%',
	    width: lebar,
	    border: false,
	    items: [getItemPanelInputKelompokPasien(lebar)],
	});
    var FormDepanKelompokPasien = new Ext.Panel({
	    id: 'FormDepanKelompokPasien',
	    region: 'center',
	    width: '100%',
	    anchor: '100%',
	    layout: 'form',
	    title: '',
	    bodyStyle: 'padding:15px',
	    border: true,
	    bodyStyle: 'background:#FFFFFF;',
	    shadhow: true,
	    items: [pnlTRKelompokPasien	]
	});
    return FormDepanKelompokPasien;
}

function getItemPanelInputKelompokPasien(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	
					
				]
			}
		]
	};
    return items;
}

function KelompokPasienbaru() {
	jeniscus=0;
    KelompokPasienAddNew = true;
    Ext.getCmp('cboKelompokpasien').show();
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value;
}

function RefreshDatacombo(jeniscus) {
    ds_customer_viDaftar.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: '',
            Sortdir: '',
            target:'ViewComboKontrakCustomer',
            param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
        }
    });
    return ds_customer_viDaftar;
}

function mComboKelompokpasien(){
	var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	ds_customer_viDaftar.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: '',
            Sortdir: '',
            target:'ViewComboKontrakCustomer',
            param: 'jenis_cust=~'+ jeniscus +'~'
        }
    });
    var cboKelompokpasien = new Ext.form.ComboBox({
		id:'cboKelompokpasien',
		typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih...',
        fieldLabel: labelisi,
        align: 'Right',
        anchor: '95%',
		store: ds_customer_viDaftar,
		valueField: 'KD_CUSTOMER',
        displayField: 'CUSTOMER',
		listeners:{
			'select': function(a,b,c){
				selectSetKelompokpasien=b.data.displayText ;
				selectKdCustomer=b.data.KD_CUSTOMER;
				selectNamaCustomer=b.data.CUSTOMER;
			
			}
		}
	});
	return cboKelompokpasien;
}

function getKelompokpasienlama(lebar) {
    var items ={
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: false,
			    items:[
					{	 
						xtype: 'tbspacer',
						height: 2
					},{
	                    xtype: 'textfield',
	                    fieldLabel: 'Kelompok Pasien Asal',
	                    name: 'txtCustomerLama',
	                    id: 'txtCustomerLama',
						labelWidth:130,
	                    width: 100,
	                    anchor: '95%'
	                 }
				]
			}
		]
	};
    return items;
}

function getItemPanelNoTransksiKelompokPasien(lebar) {
    var items ={
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: false,
			    items:[
					{	 
						xtype: 'tbspacer',
						height:3
					},{  
	                    xtype: 'combo',
	                    fieldLabel: 'Kelompok Pasien Baru',
	                    id: 'kelPasien',
	                     editable: false,
	                    store: new Ext.data.ArrayStore({
	                        id: 0,
	                        fields:['Id','displayText'],
	                      	data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
	                    }),
					  	displayField: 'displayText',
					  	mode: 'local',
					  	width: 100,
					  	triggerAction: 'all',
					  	emptyText: 'Pilih Salah Satu...',
					  	selectOnFocus: true,
					  	anchor: '95%',
					  	listeners:{
							'select': function(a, b, c){
								if(b.data.displayText =='Perseorangan'){
									jeniscus='0';
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();
								}else if(b.data.displayText =='Perusahaan'){
									jeniscus='1';
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();
								}else if(b.data.displayText =='Asuransi'){
									jeniscus='2';
									Ext.getCmp('txtNoSJP').enable();
									Ext.getCmp('txtNoAskes').enable();
								}
								RefreshDatacombo(jeniscus);
							}
						}
                  	},{
						columnWidth: .990,
						layout: 'form',
						border: false,
						labelWidth:130,
						items:[
							mComboKelompokpasien()
						]
					},{
                        xtype: 'textfield',
                        fieldLabel: 'No. SJP',
                        maxLength: 200,
                        name: 'txtNoSJP',
                        id: 'txtNoSJP',
                        width: 100,
                        anchor: '95%'
                 	},{
	                    xtype: 'textfield',
	                    fieldLabel: 'No. Askes',
	                    maxLength: 200,
	                    name: 'txtNoAskes',
	                    id: 'txtNoAskes',
	                    width: 100,
	                    anchor: '95%'
	                }
				]
			}
		]
	};
    return items;
}

function Datasave_Kelompokpasien(mBol) {	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request ({
			url: baseURL +  "index.php/main/functionRWJ/UpdateKdCustomer",	
			params: getParamKelompokpasien(),
			failure: function(o){
				ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				Ext.get('txtCustomer').dom.value = selectNamaCustomer;
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoRWJ('Ganti kelompok pasien berhasil','Information');
					Datasave_Kelompokpasien_SQL();
					RefreshDataKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
					FormLookUpsdetailTRKelompokPasien.close();
				}else{
						ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function getParamKelompokpasien(){
    var params ={
		Table:'ViewTrKasirRwj', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter:Ext.get('txtKdDokter').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value,
		UrutMasuk : Ext.getCmp('txtKdUrutMasuk').getValue(),
	};
    return params;
}

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus){
	var x = 1;
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined )||(Ext.get('cboKelompokpasien').getValue() == '') || (Ext.get('cboKelompokpasien').dom.value  === undefined )){
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) {
			ShowPesanWarningRWJ(nmGetValidasiKosong('Kelompok pasien'), modul);
			x = 0;
		}
		if (Ext.get('cboKelompokpasien').getValue() == '' && mBolHapus === true) {
			ShowPesanWarningRWJ(nmGetValidasiKosong('Combo kelompok pasien'), modul);
			x = 0;
		}
	}
	return x;
}

function setpostingtransaksi(notransaksi) {
	Ext.Msg.show({
	   title:'Posting',
	   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
	   buttons: Ext.MessageBox.YESNO,
	   width:250,
	   fn: function (btn){			
			if (btn === 'yes'){
				Ext.Ajax.request({
					url : baseURL + "index.php/main/posting",
					params: {
					_notransaksi : 	notransaksi
					},
					success: function(o){
						var cst = Ext.decode(o.responseText);
						if (cst.success === true){
							RefreshDataFilterKasirRWJ();
							ShowPesanInfoDiagnosa('Posting Berhasil Dilakukan','Posting');
							FormLookUpsdetailTRrwj.close();
						}else if (cst.success === false && cst.pesan===0){
							ShowPesanWarningDiagnosa(nmPesanHapusGagal,'Posting');
						}else{
							ShowPesanWarningDiagnosa(nmPesanHapusError,'Posting');
						}
					}
				});
			}
		}
	});
}
// ================================================================================================================================================================================================
function setdisablebutton_PJ_RWJ(){
	Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').disable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
	Ext.getCmp('btngantipasien_rwj').disable();
	Ext.getCmp('btnposting_pj_rwj').disable();	
	Ext.getCmp('btnLookupRWJ').disable();
	Ext.getCmp('btnSimpanRWJ').disable();
	Ext.getCmp('btnHpsBrsRWJ').disable();
	Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').disable();
	Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').disable();
	Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').disable();

	Ext.getCmp('BtnTambahTindakanTrPenJasRWJ').disable();
	Ext.getCmp('BtnSimpanTindakanTrPenJasRWJ').disable();
	Ext.getCmp('btnHpsBrsIcd9_RWJ').disable();

	Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').disable();

	Ext.getCmp('BtnTambahObatTrKasirRwj').disable();

	Ext.getCmp('BtnHapusObatTrKasirRwj').disable();
	Ext.getCmp('btnLookUpEditDokterPenindak_RWJ').disable();
	Ext.getCmp('btnbarisRad_PJ_RWJ').disable();
	Ext.getCmp('btnSimpanRad_PJ_RWJ').disable();
	Ext.getCmp('btnHpsBrsRad_PJ_RWJ').disable();

	Ext.getCmp('RJPJBtnAddLab_PJ_RWJ').disable();
	Ext.getCmp('RJPJBtnSaveLab_PJ_RWJ').disable();
	Ext.getCmp('RJPJBtnDelLab_PJ_RWJ').disable();
	Ext.getCmp('btnsimpanop_PJ_RWJ').disable();
}

function setenablebutton_PJ_RWJ(){
	Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').enable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
	Ext.getCmp('btngantipasien_rwj').enable();
	Ext.getCmp('btnposting_pj_rwj').enable();	
	
	Ext.getCmp('btnLookupRWJ').enable();
	Ext.getCmp('btnSimpanRWJ').enable();
	Ext.getCmp('btnHpsBrsRWJ').enable();
	Ext.getCmp('btnHpsBrsDiagnosa_PJ_RWJ').enable();
	Ext.getCmp('btnSimpanDiagnosa_PJ_RWJ').enable();
	Ext.getCmp('btnLookupDiagnosa_PJ_RWJ').enable();	

	Ext.getCmp('BtnTambahTindakanTrPenJasRWJ').enable();
	Ext.getCmp('BtnSimpanTindakanTrPenJasRWJ').enable();
	Ext.getCmp('btnHpsBrsIcd9_RWJ').enable();

	Ext.getCmp('btnsimpanAnamnese_PJ_RWJ').enable();

	Ext.getCmp('BtnTambahObatTrKasirRwj').enable();

	Ext.getCmp('BtnHapusObatTrKasirRwj').enable();
	Ext.getCmp('btnLookUpEditDokterPenindak_RWJ').enable();
	Ext.getCmp('btnbarisRad_PJ_RWJ').enable();
	Ext.getCmp('btnSimpanRad_PJ_RWJ').enable();
	Ext.getCmp('btnHpsBrsRad_PJ_RWJ').enable();
	
	Ext.getCmp('RJPJBtnAddLab_PJ_RWJ').enable();
	Ext.getCmp('RJPJBtnSaveLab_PJ_RWJ').enable();
	Ext.getCmp('RJPJBtnDelLab_PJ_RWJ').enable();
	Ext.getCmp('btnsimpanop_PJ_RWJ').enable();
}

var mRecordRad = Ext.data.Record.create([
   {name: 'ID_RADKONSUL', mapping:'ID_RADKONSUL'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'KD_KLAS', mapping:'KD_KLAS'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_DOKTER', mapping:'KD_DOKTER'}
]);

function TambahBarisRad(){
    var x=true;
    if (x === true) {
        var p = RecordBaruRad();
        dsRwJPJRAD.insert(dsRwJPJRAD.getCount(), p);
    }
}

function RecordBaruRad(){
	var p = new mRecordRad({
		'ID_RADKONSUL':'',
		'KD_PRODUK':'',
	    'KD_KLAS':'', 
	    'KD_TARIF':'', 
	    'DESKRIPSI':'',
	    'KD_DOKTER':''
	});
	return p;
};

function radData(){
	dsrad = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','KLASIFIKASI','DESKRIPSI','KD_DOKTER'] });
	dsrad.load({ 
		params: { 
			Skip: 0, 
			Take: 50, 
			target:'ViewProdukRad',
			param: ''
		} 
	});
	return dsrad;
}

function getRadtest(){
	var radCombobox = new Ext.form.ComboBox({
	   	id: 'cboRadRequest',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		fieldLabel: 'Kode Test',
		align: 'Right',
		store: radData(),
		valueField: 'KD_PRODUK',
		displayField: 'KD_PRODUK',
		anchor: '95%',
		listeners:{
			'select': function(a, b, c){
				dsRwJPJRAD.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
				dsRwJPJRAD.data.items[CurrentRad.row].data.DESKRIPSI = b.data.DESKRIPSI;
				dsRwJPJRAD.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
		 	}		
		}
	});
	return radCombobox;
}

function getRadDesk(){
	var radComboboxDesk = new Ext.form.ComboBox({
	   id: 'cboRadRequestDesk',
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   fieldLabel: 'Kode Test',
	   align: 'Right',
	   store: radData(),
	   valueField: 'DESKRIPSI',
	   displayField: 'DESKRIPSI',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			   dsRwJPJRAD.data.items[CurrentRad.row].data.KD_PRODUK = b.data.KD_PRODUK;
			   dsRwJPJRAD.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
			   dsRwJPJRAD.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
			   dsRwJPJRAD.data.items[CurrentRad.row].data.KLASIFIKASI = b.data.KLASIFIKASI;
		   }		
	   }
	});
	return radComboboxDesk;
}

function getRadDokter(){
	var radText = new Ext.form.TextField({
	   id			: 'RadRequestDok',
	   readonly		: true,
	   disable		: true,
	   store		: radData(),
	   value		: 'USERNAME',
	   valueField	: 'USERNAME',
	   displayField	: 'USERNAME',
	   anchor		: '95%',
	   listeners	: {}
	});
	return radText;
}

function getRadKelas(){
	var radText = new Ext.form.ComboBox({
	   id				: 'RadRequestDok',
	   typeAhead		: true,
	   triggerAction	: 'all',
	   lazyRender		: true,
	   mode				: 'local',
	   hideTrigger		: true,
	   forceSelection	: true,
	   selectOnFocus	: true,
	   fieldLabel		: 'Kode Test',
	   align			: 'Right',
	   store			: radData(),
	   valueField		: 'KD_KLASS',
	   displayField		: 'KD_KLASS',
	   anchor			: '95%',
	   listeners		: {}
	});
	return radText;
}

function datasavepoliklinikrad(mBol){	
	Ext.Ajax.request({
		url		: baseURL + "index.php/main/CreateDataObj",
		params	: getParamDetailPoliklinikRad(),
		success	: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoDiagnosa('Data berhasil disimpan', 'Information');
				if(mBol === false){
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				}
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function getParamDetailPoliklinikRad(){
    var params ={
		Table		: 'CrudpoliRad',
		KdPasien	: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit		: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk	: Ext.get('txtKdUrutMasuk').getValue(),
		Tgl			: Ext.get('dtpTanggalDetransaksi').dom.value,
		List		: getArrdetailRadiologi()
	};
	return params;
}

function getArrdetailRadiologi(){
	var x = '',y='',z='::',hasil;
	for(var k = 0; k < dsRwJPJRAD.getCount(); k++){
		if (dsRwJPJRAD.data.items[k].data.KD_PRODUK == null){
			x += '';
		}else{
			hasil = dsRwJPJRAD.data.items[k].data.KD_PRODUK;
			y = dsRwJPJRAD.data.items[k].data.KD_DOKTER;
			y += z + hasil;
		}
		x += y + '<>';
	}
	return x;
}

function HapusBarisRad(){
    if( cellselectedrad != undefined ){
    	Ext.Msg.show({
           title	: nmHapusBaris,
           msg		: 'Anda yakin akan menghapus' ,
           buttons	: Ext.MessageBox.YESNO,
           fn		: function (btn){
               if (btn =='yes'){
   					if (cellselectedrad.data.KD_PRODUK != '' && cellselectedrad.data.DESKRIPSI != ''){
   						dsRwJPJRAD.removeAt(CurrentRad.row);
					   	DataDeleteRad();
                    }else{
                    	ShowPesanWarningRWJ('Pilih record ','Hapus data');
                    }
               }
           },
           icon: Ext.MessageBox.QUESTION
        });
    }
}

function DataDeleteRad(){
    Ext.Ajax.request({
	    url		: baseURL + "index.php/main/DeleteDataObj",
	    params	: getParamDataDeleteRad(),
	    success	: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoRWJ('Data berhasil dihapus', 'Information');
                dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                cellSelecteddeskripsi=undefined;
                RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }else if (cst.success === false && cst.pesan === 0 ){
                ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
            }else{
                ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
            }
        }
    });
}

function getParamDataDeleteRad(){
    var params ={
		Table		: 'CrudpoliRad',
		Kdproduk 	: CurrentRad.data.data.KD_PRODUK,
		idradkonsul	: CurrentRad.data.data.ID_RADKONSUL
	};
	return params;
}

function RefreshDataSetRadiologi(){	
	var strKriteriaRadiologi='';
	strKriteriaRadiologi = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksi').getValue() + '~ and kd_unit=~'+Ext.get('txtKdUnitRWJ').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi').dom.value+'~';
	dsRwJPJRAD.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
            Sort	: 'id_konsul',
			Sortdir	: 'ASC', 
			target	: 'CrudpoliRad',
			param	: strKriteriaRadiologi
		} 
	});
	return dsRwJPJRAD;
}

function pj_req_radhasil(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_produk,urut){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRADPoliklinik/gethasiltest",
			params: {
			kd_pasien:kd_pasien,
			kd_unit:kd_unit,
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			kd_produk:kd_produk,
			urut:urut},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.hasil==="null"|| cst.hasil===null || cst.hasil==="")
				{
				ShowPesanWarningRWJ('Hasil Radiologi tidak ditemukan cek input hasil atau hubungi admin', 'cari hasil');
				
				}else{
				
				Ext.getCmp('textareapopuphasil_pjrwj').setValue(cst.hasil);
				}
			}
		}
		
	)

};


function getParamDetailTransaksiLAB() 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	KdPasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRWJ').getValue(),
		KdDokter:PenataJasaRJ.var_kd_dokter_leb,
		KdDokter_mr_tindakan:Ext.get('txtKdDokter').dom.value ,
		kd_kasir:'default_kd_kasir_rwj',
		Modul:'rwj',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		List:getArrPoliLab(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:PenataJasaRJ.ds3.getCount(),
		unit:41,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		KdKasirAsal:'01',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter: []
	};
    return params
};


function getParamDetailTransaksiRAD() 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	
		NoTrans:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		KdPasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRWJ').getValue(),
		KdDokter:PenataJasaRJ.var_kd_dokter_rad,
		KdDokter_mr_tindakan:Ext.get('txtKdDokter').dom.value ,
		kd_kasir:'default_kd_kasir_rwj',
		Modul:'rwj',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		Modul:'rwj',
		List:getArrPoliRad(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:PenataJasaRJ.ds3.getCount(),
		unit:5,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		KdKasirAsal:'01',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter	: [],
		unitaktif:'rwj'
	};
    return params
};


function ViewGridBawahpoliRad(no_transaksi,kd_unit,tgl_transaksi,urut_masuk) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRADPoliklinik/getItemPemeriksaan",
			params: {
					no_transaksi:no_transaksi,
					kdunit:kd_unit,
					tgltrx:tgl_transaksi,
					urutmasuk:urut_masuk,
					kasirmana:'rwj'
					},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsRwJPJRAD.removeAll();
					var recs=[],
						recType=dsRwJPJRAD.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsRwJPJRAD.add(recs);
					
					PenataJasaRJ.pj_req_rad.getView().refresh();
					
				
				} 
			}
		}
		
	)
};

function ViewGridBawahpoliLab(no_transaksi,kd_unit,tgl_transaksi,urut_masuk) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getItemPemeriksaan",
			params: {
				no_transaksi:no_transaksi,
				kd_unit:kd_unit,
				tgltrx:tgl_transaksi,
				urutmasuk:urut_masuk,
				kasirmana:'rwj'
			},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					PenataJasaRJ.ds3.removeAll();
					var recs=[],
						recType=PenataJasaRJ.ds3.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					PenataJasaRJ.ds3.add(recs);
					
					PenataJasaRJ.grid3.getView().refresh();
					
				} 
			}
		}
		
	)
};
/* 
function ViewGridBawahpoliLab(kd_pasien,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getItemPemeriksaan",
			params: {no_transaksi:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					PenataJasaRJ.ds3.removeAll();
					var recs=[],
						recType=PenataJasaRJ.ds3.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					PenataJasaRJ.ds3.add(recs);
					
					PenataJasaRJ.grid3.getView().refresh();
				} 
			}
		}
		
	)
}; */


function getArrPoliLab()
{var x='';
console.log (PenataJasaRJ.ds3.getCount());
	var arr=[];
	for(var i = 0 ; i < PenataJasaRJ.ds3.getCount();i++)
	{
	
			var o={};
			var y='';
			var z='@@##$$@@';
		
			o['URUT']= PenataJasaRJ.ds3.data.items[i].data.urut;
			o['KD_PRODUK']= PenataJasaRJ.ds3.data.items[i].data.kd_produk;
			o['QTY']= PenataJasaRJ.ds3.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= PenataJasaRJ.ds3.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= PenataJasaRJ.ds3.data.items[i].data.tgl_berlaku;
			o['HARGA']= PenataJasaRJ.ds3.data.items[i].data.harga;
			o['KD_TARIF']= PenataJasaRJ.ds3.data.items[i].data.kd_tarif;
			o['NO_TRANSAKSI_BAWAH']= PenataJasaRJ.ds3.data.items[i].data.no_transaksi;
			o['cito']= PenataJasaRJ.ds3.data.items[i].data.cito;
			arr.push(o);
		
	}	
	
	return Ext.encode(arr);
};


function getArrPoliRad()
{var x='';
console.log (dsRwJPJRAD.getCount());
	var arr=[];
	for(var i = 0 ; i < dsRwJPJRAD.getCount();i++)
	{
	
			var o={};
			var y='';
			var z='@@##$$@@';
		
			o['URUT']= dsRwJPJRAD.data.items[i].data.urut;
			o['KD_PRODUK']= dsRwJPJRAD.data.items[i].data.kd_produk;
			o['QTY']= dsRwJPJRAD.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsRwJPJRAD.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsRwJPJRAD.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsRwJPJRAD.data.items[i].data.harga;
			o['KD_TARIF']= dsRwJPJRAD.data.items[i].data.kd_tarif;
			o['NO_TRANSAKSI_BAWAH']= dsRwJPJRAD.data.items[i].data.no_transaksi;
			o['cito']= dsRwJPJRAD.data.items[i].data.cito;
			arr.push(o);
		
	}	
	
	return Ext.encode(arr);
};

function getFormatTanggal(date){
	tanggal = new Date(date);
	month = '' + parseInt(tanggal.getMonth()+1);
	day = '' +tanggal.getDate();
	year = ''+tanggal.getFullYear();
	if (month.length < 2){
		month =  '0' + parseInt(tanggal.getMonth()+1);
	}
	if (day.length < 2){
		day =  '0' + tanggal.getDate();
	}
	return [year,month,day].join('-');
}


function GetDTGridHasilLab_PJRWJ() 
{
	var fm = Ext.form;
    var fldDetailHasilLab = ['KLASIFIKASI', 'DESKRIPSI', 'KD_LAB', 'KD_TEST', 'ITEM_TEST', 'SATUAN', 'NORMAL', 'NORMAL_W',  
								'NORMAL_A', 'NORMAL_B', 'COUNTABLE', 'MAX_M', 'MIN_M', 'MAX_F', 'MIN_F', 'MAX_A', 'MIN_A', 
								'MAX_B', 'MIN_B', 'KD_METODE', 'HASIL', 'KET','KD_UNIT_ASAL','NAMA_UNIT_ASAL','URUT','METODE',
								'JUDUL_ITEM'];
	
    PenataJasaRJ.dshasilLabRWJ = new WebApp.DataStore({ fields: fldDetailHasilLab })
    PenataJasaRJ.gridhasil_lab_PJRWJ = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Hasil Lab',
			height: 160,
			width:815,
            stripeRows: true,
            store: PenataJasaRJ.dshasilLabRWJ,
			frame: false,
            border: true,
            columnLines: true,
            autoScroll:true,
           cm: new Ext.grid.ColumnModel
            (
			[
				{	id: Nci.getId(),
					header: 'Kode Tes',
					dataIndex: 'KD_TEST',
					width:80,
					menuDisabled:true,
					hidden:true
				},
				{
					header: 'Item Pemeriksaan',
					dataIndex: 'JUDUL_ITEM',
					width:150,
					menuDisabled:true,
					style : {
						color : 'red'
					}
				},
				{	id: Nci.getId(),
					header:'Pemeriksaan',
					dataIndex: 'ITEM_TEST',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{	id: Nci.getId(),
					header:'Metode',
					dataIndex: 'METODE',
					sortable: false,
					align: 'center',
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{	id: Nci.getId(),
					header:'Hasil',
					dataIndex: 'HASIL',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					align: 'right',
					
					
				},
				{	id: Nci.getId(),
					header:'Normal',
					dataIndex: 'NORMAL',
					sortable: false,
					hidden:false,
					align: 'center',
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Satuan',
					dataIndex: 'SATUAN',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Keterangan',
					dataIndex: 'KET',
					width:250,
					
					
				},
				{
					header:'Kode Lab',
					dataIndex: 'KD_LAB',
					width:250,
					hidden:true
					
				}

			]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return PenataJasaRJ.gridhasil_lab_PJRWJ;
};

function GetGridRiwayatKunjunganPasien_RWJ(){
	var fldDetail = ['kd_pasien','nama','tgl_masuk','kd_unit','nama_unit','urut_masuk','kd_kasir','no_transaksi','kd_dokter'];
    dsTRRiwayatKunjuganPasien = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatKunjungan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatKunjunganPasien',
        store: dsTRRiwayatKunjuganPasien,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:265,
		width:150,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, cell, rec){
                        cellSelectedriwayatkunjunganpasien = dsTRRiwayatKunjuganPasien.getAt(cell);
                        CurrentSelectedRiwayatKunjunanPasien.cell = cell;
                        CurrentSelectedRiwayatKunjunanPasien = cellSelectedriwayatkunjunganpasien.data;
						
						currentRiwayatKunjunganPasien_TglMasuk_RWJ = CurrentSelectedRiwayatKunjunanPasien.tgl_masuk;
						currentRiwayatKunjunganPasien_KdUnit_RWJ = CurrentSelectedRiwayatKunjunanPasien.kd_unit;
						currentRiwayatKunjunganPasien_UrutMasuk_RWJ = CurrentSelectedRiwayatKunjunanPasien.urut_masuk;
						currentRiwayatKunjunganPasien_KdKasir_RWJ = CurrentSelectedRiwayatKunjunanPasien.kd_kasir;
						currentRiwayatKunjunganPasien_NoTrans_RWJ = CurrentSelectedRiwayatKunjunanPasien.no_transaksi;
						currentRiwayatKunjunganPasien_KdDokter_RWJ = CurrentSelectedRiwayatKunjunanPasien.kd_dokter;
						
						viewAnamnese_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.urut_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_kasir,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
						
						dsTRRiwayatDiagnosa_RWJ.removeAll();
						viewGridRiwayatDiagnosa_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
						
						dsTRRiwayatTindakan_RWJ.removeAll();
						viewGridRiwayatTindakan_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.urut_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_kasir,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
                    
						dsTRRiwayatObat_RWJ.removeAll();
						viewGridRiwayatObat_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.urut_masuk,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
					
						dsTRRiwayatLab_RWJ.removeAll();
						viewGridRiwayatLab_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.urut_masuk,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
						
						dsTRRiwayatRad_RWJ.removeAll();
						viewGridRiwayatRad_RWJ(CurrentSelectedRiwayatKunjunanPasien.tgl_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_unit,CurrentSelectedRiwayatKunjunanPasien.urut_masuk,CurrentSelectedRiwayatKunjunanPasien.kd_kasir,CurrentSelectedRiwayatKunjunanPasien.no_transaksi);
					}
                }
            }
        ),
        cm: DetailGridRiwayatKunjunganColumModel(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatKunjungan;
}

function DetailGridRiwayatKunjunganColumModel(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            header		: 'Tgl Kunjungan',
            dataIndex	: 'tgl_masuk',
            width		: 70,
			menuDisabled: true,
			renderer:function(value, metaData, record, rowIndex, colIndex, store)
			{
				return ShowDate(record.data.tgl_masuk);
			}
        },{
            header		: 'Unit',
            dataIndex	: 'nama_unit',
			menuDisabled: true,
			width		: 100
        },{
            header: 'kd_unit',
            dataIndex: 'kd_unit',
			hidden:true
        },{
            header: 'kd_pasien',
            dataIndex: 'kd_pasien',
			hidden:true
        },
    ]);
}

function GetGridRiwayatDiagnosa_RWJ(){
	var fldDetail = ['penyakit','stat_diag','kasus','kd_penyakit'];
    dsTRRiwayatDiagnosa_RWJ = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatDiagnosa = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatDiagnosa_RWJ',
        store: dsTRRiwayatDiagnosa_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:115,
		width:556,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        /* cellSelectedriwayatkunjunganpasien = dsTRRiwayatDiagnosa_RWJ.getAt(row);
                        CurrentSelectedRiwayatKunjunanPasien.row = row;
                        CurrentSelectedRiwayatKunjunanPasien.data = cellSelectedriwayatkunjunganpasien; */
                    }
                }
            }
        ),
        cm: DetailGridRiwayatDiagnosaColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatDiagnosa;
}

function DetailGridRiwayatDiagnosaColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            header		: 'Penyakit',
            dataIndex	: 'penyakit',
			menuDisabled: true,
			width		: 100
        },{
            header: 'Status diagnosa',
            dataIndex: 'stat_diag',
			width		: 60
        },{
            header: 'Kasus',
            dataIndex: 'kasus',
			width		: 60
        },{
            header: 'kd_penyakit',
            dataIndex: 'kd_penyakit',
			hidden:true
        },
    ]);
}

function GetGridRiwayatTindakan_RWJ(){
	var fldDetail = ['kd_produk','deskripsi'];
    dsTRRiwayatTindakan_RWJ = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatTindakan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatTindakan_RWJ',
        store: dsTRRiwayatTindakan_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:115,
		width:556,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: DetailGridRiwayatTindakanColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatTindakan;
}

function DetailGridRiwayatTindakanColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            header		: 'Deskripsi tindakan',
            dataIndex	: 'deskripsi',
			menuDisabled: true,
			width		: 100
        },{
            header: 'kd_produk',
            dataIndex: 'kd_produk',
			hidden:true
        },
    ]);
}

function GetGridRiwayatObat_RWJ(){
	var fldDetail = ['kd_prd','nama_obat'];
    dsTRRiwayatObat_RWJ = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatObat = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatObat_RWJ',
        store: dsTRRiwayatObat_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:115,
		width:556,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: DetailGridRiwayatObatColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatObat;
}

function DetailGridRiwayatObatColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            header		: 'Nama Obat',
            dataIndex	: 'nama_obat',
			menuDisabled: true,
			width		: 100
        },{
            header: 'kd_prd',
            dataIndex: 'kd_prd',
			hidden:true
        },
    ]);
}

function GetGridRiwayatLab_RWJ(){
	var fldDetail = ['klasifikasi', 'deskripsi', 'kd_lab', 'kd_test', 'item_test', 'satuan', 'normal', 'normal_w',  
					'normal_a', 'normal_b', 'countable', 'max_m', 'min_m', 'max_f', 'min_f', 'max_a', 'min_a', 'max_b',
					'min_b', 'kd_metode', 'hasil', 'ket','kd_unit_asal','nama_unit_asal','urut','metode','judul_item','ket_hasil'];
    dsTRRiwayatLab_RWJ = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatLab = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatLab_RWJ',
        store: dsTRRiwayatLab_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:115,
		width:556,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: DetailGridRiwayatLabColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatLab;
}

function DetailGridRiwayatLabColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
			header: 'Kode Tes',
			dataIndex: 'kd_test',
			width:80,
			menuDisabled:true,
			hidden:true
		},
		{
			header: 'Item Pemeriksaan',
			dataIndex: 'judul_item',
			width:150,
			menuDisabled:true,
			renderer:function(value, metaData, record, rowIndex, colIndex, store)
			{
				metaData.style  ='font-color:#ffb3b3;';
				return value;
			}
		},
		{
			header:'Pemeriksaan',
			dataIndex: 'item_test',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:200
			
		},
		{
			header:'Metode',
			dataIndex: 'metode',
			sortable: false,
			align: 'center',
			hidden:false,
			menuDisabled:true,
			width:90
			
		},
		{
			header:'Hasil',
			dataIndex: 'hasil',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:90,
			align: 'right'
			
		},
		{
			header:'Normal',
			dataIndex: 'normal',
			sortable: false,
			hidden:false,
			align: 'center',
			menuDisabled:true,
			width:90
			
		},
		{
			header:'Ket Hasil',
			dataIndex: 'ket_hasil',
			sortable: false,
			hidden:false,
			align: 'center',
			menuDisabled:true,
			width:70
			
		},
		{
			header:'Satuan',
			dataIndex: 'satuan',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:70
			
		},
		{
			header:'Keterangan',
			dataIndex: 'ket',
			width:90
			
		},
		{
			header:'Kode Lab',
			dataIndex: 'kd_lab',
			width:250,
			hidden:true
			
		}
    ]);
}

function GetGridRiwayatRad_RWJ(){
	var fldDetail = ['kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','cito',
					'tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah',
					'kd_dokter','namadok','lunas','kd_pasien','urutkun','tglkun','kdunitkun',
					'hasil'];
    dsTRRiwayatRad_RWJ= new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridRiwayatRad = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridTabRiwayatRad_RWJ',
        store: dsTRRiwayatRad_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:115,
		width:556,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: DetailGridRiwayatRadColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridRiwayatRad;
}

function DetailGridRiwayatRadColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
			header: 'kd_produk',
			dataIndex: 'kd_produk',
			width:80,
			menuDisabled:true,
			hidden:true
		},
		{
			header: 'Deskripsi pemeriksaan radiologi',
			dataIndex: 'deskripsi',
			width:100,
			menuDisabled:true
		},
		{
			header: 'Hasil',
			dataIndex: 'hasil',
			width:150,
			menuDisabled:true
		},
    ]);
}

function viComboKamar_viJdwlOperasi()
{
	var Field =['kd_unit','no_kamar','nama_kamar','jumlah_bed','digunakan'];
    dsvComboKamarOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboKamarOperasiJadwalOperasiOK();
	var cbo_viComboKamar_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",     
            width:230,
            store:dsvComboKamarOperasiJadwalOperasiOK,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            listeners:  
            {
				'select': function(a,b,c)
				{
					
				},
            }
        }
    );
    return cbo_viComboKamar_viJdwlOperasi;
};
function dsComboKamarOperasiJadwalOperasiOK()
{
	dsvComboKamarOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboKamarOperasiJadwalOperasiOK',
					//param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboKamarOperasiJadwalOperasiOK;
}
function getItemTrPenJasRWJ_Batas()
{
    var items = {
        layout: 'column',
        border: false,
        items: [{
            layout: 'absolute',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: false,
            width:  100,
            height: 5,
            anchor: '100% 100%',
            items: []
        }]
    };
    return items;
};

function getTotKunjunganRWJ(){	
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/getTotKunjungan",
		params		: {query:""},
		failure		: function(o){
			ShowPesanErrorRWJ('Hubungi Admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				Ext.getCmp('txtTotKunjunganHariIni_TrKasirRWJ').setValue(cst.totalkunjungan);
			}
			else{
				ShowPesanErrorRWJ('Gagal menampilkan total kunjungan', 'Gagal');
			}
		}
	});
}

function updateStatusPeriksa(){	
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/updateStatusPeriksa",
		params		: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
			tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
			urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		},
		failure		: function(o){
			ShowPesanErrorRWJ('Hubungi Admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				
			}
			else{
				ShowPesanErrorRWJ('Gagal update status pemeriksaan', 'Gagal');
			}
		}
	});
}

function datasave_TrPenJasRWJ(){
	if (ValidasiEntryIcd9_RWJ(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/functionRWJ/saveIcd9",
				params: getParamIcd9RWJ(),
				failure: function(o)
				{
					ShowPesanErrorRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoRWJ('Simpan berhasil','Information');
						datasave_TrPenJasRWJ_SQL();
						viewGridIcd9_RWJ();
						//refreshDataDepanPenjasRWJ();
						//refeshkasirrwj();
						
					}
					else 
					{
						ShowPesanErrorRWJ('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function ValidasiEntryIcd9_RWJ(modul,mBolHapus)
{
	var x = 1;
	if(dsTRDetailDiagnosaListIcd9.getCount() > 0){
		for(var i=0; i<dsTRDetailDiagnosaListIcd9.getCount() ; i++){
			var o=dsTRDetailDiagnosaListIcd9.getRange()[i].data;
			if(o.kd_icd9 == undefined || o.kd_icd9 == ""){
				ShowPesanWarningRWJ('No. Icd 9  masih kosong, periksa kembali daftar Icd 9!', 'Warning');
				x = 0;
			}
			
			for(var j=0; j<dsTRDetailDiagnosaListIcd9.getCount() ; j++){
				var p=dsTRDetailDiagnosaListIcd9.getRange()[j].data;
				if(i != j && o.kd_icd9 == p.kd_icd9){
					ShowPesanWarningRWJ('No. Icd 9 tidak boleh sama, periksa kembali daftar Icd 9!', 'Warning');
					x = 0;
					break;
				}
			}
		}
	} else{
		ShowPesanWarningRWJ('Daftar Icd 9 tidak boleh kosong, minimal 1 data!', 'Warning');
		x = 0;
	}
	
	
	return x;
};

function getParamIcd9RWJ(){
	var params =
	{
		kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
		tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(), 
		urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		no_transaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		kd_kasir:currentKdKasirRWJ
	};
	
	params['jumlah']=dsTRDetailDiagnosaListIcd9.getCount();
	var prevurut=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaListIcd9.getCount();i++)
	{
		var urut=0;
		if(dsTRDetailDiagnosaListIcd9.data.items[i].data.urut == '' || dsTRDetailDiagnosaListIcd9.data.items[i].data.urut == undefined){
			urut = parseInt(prevurut)+1;
		} else{
			urut = dsTRDetailDiagnosaListIcd9.data.items[i].data.urut;
		}
		params['kd_icd9-'+i]=dsTRDetailDiagnosaListIcd9.data.items[i].data.kd_icd9;
		params['urut-'+i]=urut;
		
		prevurut = urut;
	}
	
    return params
}


function viewGridIcd9_RWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridicd9",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
			tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
			urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRDetailDiagnosaListIcd9.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRDetailDiagnosaListIcd9.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRDetailDiagnosaListIcd9.add(recs);
				PenataJasaRJ.gridIcd9.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca daftar icd 9', 'Error');
			};
		}
	});
}

function viewGridRiwayatKunjunganPasien_RWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayatkunjungan",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatKunjuganPasien.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatKunjuganPasien.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatKunjuganPasien.add(recs);
				PenataJasaRJ.gridRiwayatKunjungan.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat kunjungan', 'Error');
			};
		}
	});
}

function viewAnamnese_RWJ(tgl_masuk,kd_unit,urut_masuk,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewanamnese",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				Ext.getCmp('txtAmnase_RWJ').setValue(cst.anamnese);
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat diagnosa', 'Error');
			};
		}
	});
}

function viewGridRiwayatDiagnosa_RWJ(tgl_masuk,kd_unit,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayatdiagnosa",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatDiagnosa_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatDiagnosa_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatDiagnosa_RWJ.add(recs);
				PenataJasaRJ.gridRiwayatDiagnosa.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat diagnosa', 'Error');
			};
		}
	});
}

function viewGridRiwayatTindakan_RWJ(tgl_masuk,kd_unit,urut_masuk,kd_kasir,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayattindakan",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			kd_kasir:kd_kasir,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatTindakan_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatTindakan_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatTindakan_RWJ.add(recs);
				PenataJasaRJ.gridRiwayatTindakan.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat tindakan', 'Error');
			};
		}
	});
}

function viewGridRiwayatObat_RWJ(tgl_masuk,kd_unit,urut_masuk,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayatobat",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatObat_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatObat_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatObat_RWJ.add(recs);
				PenataJasaRJ.gridRiwayatObat.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat obat', 'Error');
			};
		}
	});
}


function viewGridRiwayatLab_RWJ(tgl_masuk,kd_unit,urut_masuk,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayatlab",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatLab_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatLab_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatLab_RWJ.add(recs);
				PenataJasaRJ.gridRiwayatLab.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat laboratorium', 'Error');
			};
		}
	});
}

function viewGridRiwayatRad_RWJ(tgl_masuk,kd_unit,urut_masuk,kd_kasir,no_transaksi){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridriwayatrad",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:kd_unit, 
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			kd_kasir:kd_kasir,
			no_transaksi:no_transaksi,
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsTRRiwayatRad_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsTRRiwayatRad_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsTRRiwayatRad_RWJ.add(recs);
				PenataJasaRJ.gridRiwayatRad.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat radiologi', 'Error');
			};
		}
	});
}

function panelScanBerkas_RWJ()
{
    win_scan_berkas_rwj = new Ext.Window
    (
        {
            id: 'win_scan_berkas_rwj',
            title: 'Scan Berkas Pasien',
            closeAction: 'destroy',
            width:340,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				subPanelScanBerkas_RWJ()
			],
			fbar:[
				
			],
			listeners:
			{
				activate: function()
				{
					Ext.getCmp('txtKdPasienScanBerkas_RWJ').focus();
				},
				afterShow: function()
				{
					Ext.getCmp('txtKdPasienScanBerkas_RWJ').focus();
				},
			}

		}
    );

    win_scan_berkas_rwj.show();
	Ext.getCmp('txtKdPasienScanBerkas_RWJ').focus();
};

function subPanelScanBerkas_RWJ(){
	var items = 
	{
		layout:'form',
		border: false,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				
				layout: 'absolute',
				bodyStyle: 'padding: 10px ',
				border: false,
				height: 110,
				anchor: '100% 100%',
				items:
				[
					
					{
						x:0,
						y:0,
						xtype: 'textfield',
						width : 317,	
						height : 55,
						tabIndex:0,
						name: 'txtKdPasienScanBerkas_RWJ',
						id: 'txtKdPasienScanBerkas_RWJ',
						emptyText: 'No. Medrec',
						style:{'font-size':'32px','text-align':'center'},
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var tmpNoMedrec = Ext.get('txtKdPasienScanBerkas_RWJ').getValue();
									if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
										var tmpgetNoMedrec = formatnomedrec(Ext.get('txtKdPasienScanBerkas_RWJ').getValue());
										Ext.getCmp('txtKdPasienScanBerkas_RWJ').setValue(tmpgetNoMedrec);
										
										updateAntrianRWJ();
									} else{
										updateAntrianRWJ();
									}
								};
							},
						}
					},
					{
						x:0,
						y:57,
						xtype: 'label',
						flex: 1,
						style : {
							color : 'darkblue'
						},
						text: '*) Tekan enter untuk update, jika scan barcode tidak berfungsi',
					},
				]
			},
		]		
	};
        return items;
}


function panelScanBerkasPilihUnit_RWJ()
{
    win_scan_berkas_pilih_unit_rwj = new Ext.Window
    (
        {
            id: 'win_scan_berkas_pilih_unit_rwj',
            title: 'Pilih unit yang dituju',
            closeAction: 'destroy',
            width:200,
            height: 90,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				comboUnitScanBerkas_RWJ()
			],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkUpdateUnitScanBerkasRWJ',
					handler: function()
					{		
						updateUnitAntrianRWJ()
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelUpdateUnitScanBerkasRWJ',
					handler: function()
					{
						win_scan_berkas_pilih_unit_rwj.close();
					}
				} 
			],
			listeners:
			{
				activate: function()
				{
					load_data_unit_scan_berkas_RWJ()
				},
				afterShow: function()
				{
					
				},
			}

		}
    );

    win_scan_berkas_pilih_unit_rwj.show();
	load_data_unit_scan_berkas_RWJ()
};

function comboUnitScanBerkas_RWJ()
{ 
	var Field = ['kd_unit','nama_unit'];

    dsunitscanberkasrwj = new WebApp.DataStore({ fields: Field });
	
	load_data_unit_scan_berkas_RWJ();
	cbounitscanberkasrwj= new Ext.form.ComboBox
	(
		{
			id	: 'cbounitscanberkasrwj',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			anchor: '100%',
			store	: dsunitscanberkasrwj,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:
			{
								
			}
		}
	);
	return cbounitscanberkasrwj;
};

function load_data_unit_scan_berkas_RWJ(param)
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionRWJ/getunitscanberkas",
		params:{
			kd_pasien:Ext.getCmp('txtKdPasienScanBerkas_RWJ').getValue(),
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbounitscanberkasrwj.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsunitscanberkasrwj.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsunitscanberkasrwj.add(recs);
			}
		}
	});
}

function updateAntrianRWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/updatestatusantrian",
		params: {
			kd_pasien:Ext.getCmp('txtKdPasienScanBerkas_RWJ').getValue(),
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoRWJ('Scan berhasil','Information')
				Ext.getCmp('txtKdPasienScanBerkas_RWJ').setValue('')
				refeshkasirrwj();
			} 
			else if(cst.status == 'gagal')
			{
				ShowPesanErrorRWJ('Gagal membaca riwayat radiologi', 'Error');
			} else if(cst.status == 'double'){
				Ext.Msg.show( {
					title: 'Warning',
					width:300,
					msg: 'Ditemukan lebih dari 1 berkas pasien, tekan OK untuk melanjutkan memilih unit tujuan!',
					buttons: Ext.MessageBox.OKCANCEL,
					fn: function (btn) {
						console.log(btn)
						if (btn =='ok'){
							panelScanBerkasPilihUnit_RWJ();
						};
					},
					icon: Ext.MessageBox.QUESTION
				});
				
			} else{
				ShowPesanWarningRWJ('Berkas pasien ini sudah discan!', 'Warning');
			};
		}
	});
}
function updateAntrianSedangDilayani(){	
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/updatestatusantrian_sedangdilayani",
		params		: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
			tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
			urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		},
		failure		: function(o){
			ShowPesanErrorRWJ('Hubungi Admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				
			}
			else{
				ShowPesanErrorRWJ('Gagal update status pemeriksaan', 'Gagal');
			}
		}
	});
}
function updateUnitAntrianRWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/updateunitantrian",
		params: {
			kd_pasien:Ext.getCmp('txtKdPasienScanBerkas_RWJ').getValue(),
			kd_unit:Ext.getCmp('cbounitscanberkasrwj').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoRWJ('Scan berhasil','Information')
				Ext.getCmp('txtKdPasienScanBerkas_RWJ').setValue('')
				win_scan_berkas_pilih_unit_rwj.close();
				//refeshkasirrwj();
			} else {
				ShowPesanErrorRWJ('Gagal update status berkas pasien', 'Error');
			};
		}
	});
}

function LookupPilihDokterPenindak_RWJ(rowdata){
    setLookUpPilihDokterPenindak_RWJ = new Ext.Window
    ({
        id: 'setLookUpPilihDokterPenindak_RWJ',
		name: 'setLookUpPilihDokterPenindak_RWJ',
        title: 'Pilih Dokter Penindak Poli', 
        closeAction: 'destroy',        
        width: 410,
        height: 130,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelPilihDokterPenindak_RWJ()
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKPilihDokterPenindak_RWJ',
				handler:function()
				{
					updateDokterPenindakPoli_RWJ(rowdata);
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalPilihDokterPenindak_RWJ',
				handler:function()
				{
					setLookUpPilihDokterPenindak_RWJ.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
             
            }
        }
    });

    setLookUpPilihDokterPenindak_RWJ.show();
	loadDataDokterPenindakPoli_RWJ(rowdata.KD_UNIT);
}

function PanelPilihDokterPenindak_RWJ(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 195,
		items:
		[
			{
				
				layout: 'absolute',
				bodyStyle: 'padding: 10px ',
				border: true,
				width: 465,
				height: 40,
				anchor: '100% 23%',
				items:
				[
					
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Pilih Dokter'
					},
					{
						x: 70,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					comboDokterPenindak_RWJ(),
				]
			},
		]		
	};
        return items;
}

function loadDataDokterPenindakPoli_RWJ(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getDokterPoli",
		params: {
			kd_unit:param,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboDokterPenindak_RWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsDokterPenindak_RWJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsDokterPenindak_RWJ.add(recs);
			}
		}
	});
};

function comboDokterPenindak_RWJ(){
	var Field = ['kd_dokter', 'nama'];
	dsDokterPenindak_RWJ = new WebApp.DataStore({fields: Field});
	loadDataDokterPenindakPoli_RWJ();
	cboDokterPenindak_RWJ = new Ext.form.ComboBox
	(
		{
			x: 80,
			y: 10,
			id: 'cboDokterPenindak_RWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			width:250,
			store: dsDokterPenindak_RWJ,
			valueField: 'kd_dokter',
			displayField: 'nama',
			listeners:
			{
				'select': function(a, b, c)
				{
				},
			}
		}
	)
	return cboDokterPenindak_RWJ;
};


function updateDokterPenindakPoli_RWJ(rowdata){
	if(Ext.getCmp('cboDokterPenindak_RWJ').getValue() != ''){
				Ext.Ajax.request
		({
			url: baseURL + "index.php/main/functionRWJ/updateDokterPenindak",
			params: {
				kd_pasien:rowdata.KD_PASIEN,
				kd_unit:rowdata.KD_UNIT,
				urut_masuk:rowdata.URUT_MASUK,
				tgl_masuk:rowdata.TANGGAL_TRANSAKSI,
				kd_dokter:Ext.getCmp('cboDokterPenindak_RWJ').getValue()
			},
			failure: function(o)
			{
				ShowPesanErrorRWJ('Hubungi Admin!', 'Error');
			},	
			success: function(o) 
			{   
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.Msg.show( {
						title: 'Information',
						width:300,
						msg: 'Simpan dokter penindak berhasil',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							console.log(btn)
							if (btn =='ok'){
								setLookUpPilihDokterPenindak_RWJ.close();
								RWJLookUp(rowdata);
								//refreshDataDepanPenjasRWJ();
								//refeshkasirrwj();
							};
						},
						icon: Ext.MessageBox.QUESTION
					});
					//ShowPesanInfoRWJ('Simpan dokter penindak berhasil','Information')
				} else {
					ShowPesanErrorRWJ('Gagal menyimpan dokter penindak', 'Error');
				};
			}
		});
	} else{
		ShowPesanWarningRWJ('Pilih dokter penindak untuk melanjutkan!', 'Warning');
	}
}

function LookupLastHistoryDiagnosa_RWJ(){
    setLookUpLastHistoryDiagnosa_RWJ = new Ext.Window
    ({
        id: 'setLookUpLastHistoryDiagnosa_RWJ',
		name: 'setLookUpLastHistoryDiagnosa_RWJ',
        title: 'History diagnosa pasien', 
        closeAction: 'destroy',        
        width: 465,
        height: 185,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelGridLastHistoryDiagnosa_RWJ()
		],
        listeners:
        {
            activate: function()
            {
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
             
            }
        }
    });

    setLookUpLastHistoryDiagnosa_RWJ.show();
	viewGridLastHistoryDiagnosa_RWJ();
}


function PanelGridLastHistoryDiagnosa_RWJ(){
	var fldDetail = ['penyakit','status_diag','kasuss','kd_penyakit'];
    dsLastHistoryDiagnosa_RWJ = new WebApp.DataStore({ fields: fldDetail });
	
    PenataJasaRJ.gridLastHistoryDiagnosa = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'GridLastHistoryDiagnosa_RWJ',
        store: dsLastHistoryDiagnosa_RWJ,
        border: true,
        columnLines: true,
        frame: false,
        autoScroll:true,
		height:153,
		width:451,
		anchor: '100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        /* cellSelectedriwayatkunjunganpasien = dsTRRiwayatDiagnosa_RWJ.getAt(row);
                        CurrentSelectedRiwayatKunjunanPasien.row = row;
                        CurrentSelectedRiwayatKunjunanPasien.data = cellSelectedriwayatkunjunganpasien; */
                    }
                }
            }
        ),
        cm: DetailGridLastHistoryDiagnosaColumModel_RWJ(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.gridLastHistoryDiagnosa;
}
function DetailGridLastHistoryDiagnosaColumModel_RWJ(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
            header		: 'Penyakit',
            dataIndex	: 'penyakit',
			menuDisabled: true,
			width		: 120
        },{
            header: 'Status diagnosa',
            dataIndex: 'status_diag',
			width		: 50
        },{
            header: 'Kasus',
            dataIndex: 'kasuss',
			width		: 40
        },{
            header: 'kd_penyakit',
            dataIndex: 'kd_penyakit',
			hidden:true
        },
    ]);
}

function viewGridLastHistoryDiagnosa_RWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridlasthistorydiagnosa",
		params: {
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsLastHistoryDiagnosa_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsLastHistoryDiagnosa_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsLastHistoryDiagnosa_RWJ.add(recs);
				PenataJasaRJ.gridLastHistoryDiagnosa.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function GetgridEditDokterPenindakJasa_RWJ(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgrideditjasadokterpenindak",
		params: {
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(),
			urut:currentJasaDokterUrutDetailTransaksi_RWJ,
			kd_kasir:currentKdKasirRWJ,
			no_transaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
			tgl_transaksi:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
			
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_RWJ.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_RWJ.add(recs);
					GridDokterTr_RWJ.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_RWJ(currentJasaDokterKdProduk_RWJ,currentJasaDokterKdTarif_RWJ);
				}
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function SimpanJasaDokterPenindak(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoRWJ('Dokter penindak berhasil disimpan','Information');
				GetgridEditDokterPenindakJasa_RWJ();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getParamsJasaDokterPenindak(urut,harga){
	var params = {
		kd_kasir:currentKdKasirRWJ,
		no_transaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		tgl_transaksi:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
		urut:urut,
		harga:harga
	};
	params['jumlah'] = dsGridJasaDokterPenindak_RWJ.getCount();
	for(var i = 0 ; i < dsGridJasaDokterPenindak_RWJ.getCount();i++)
	{
		params['kd_component-'+i]=dsGridJasaDokterPenindak_RWJ.data.items[i].data.kd_component;
		params['kd_dokter-'+i]=dsGridJasaDokterPenindak_RWJ.data.items[i].data.kd_dokter;
	}
	return params;
}

function loaddatagridpilihdokterpenindak_RWJ(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbopilihdokterpenindak_RWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridpilihdokterpenindak_RWJ.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridpilihdokterpenindak_RWJ.add(recs);
			}
		}
	});
}

function cekKomponen(kd_produk,kd_tarif,kd_unit,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/cekKomponen",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kd_unit
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.komponen > 0){
					currentJasaDokterKdTarif_RWJ=kd_tarif;
					currentJasaDokterKdProduk_RWJ=kd_produk;
					currentJasaDokterUrutDetailTransaksi_RWJ=urut;
					currentJasaDokterHargaJP_RWJ=harga;
					PilihDokterLookUp_RWJ()
				}
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal cek komponen pelayanan', 'Error');
			};
		}
	});
}

function GetgridPilihDokterPenindakJasa_RWJ(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue()
		},
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_RWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_RWJ.add(recs);
				GridDokterTr_RWJ.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function savetransaksi(){
	var e=false;
	if(PenataJasaRJ.dsGridTindakan.getRange().length > 0){
		for(var i=0,iLen=PenataJasaRJ.dsGridTindakan.getRange().length; i<iLen ; i++){
			var o=PenataJasaRJ.dsGridTindakan.getRange()[i].data;
			if(o.QTY == '' || o.QTY==0 || o.QTY == null){
				PenataJasaRJ.alertError('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
				e=true;
				break;
			}
			
		}
	}else{
		PenataJasaRJ.alertError('Isi Tindakan Yang Diberikan','Peringatan');
		e=true;
	}
	for(var i=0,iLen=PenataJasaRJ.dsGridObat.getRange().length; i<iLen ; i++){
		var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
		if(o.nama_obat == '' || o.nama_obat == null){
			PenataJasaRJ.alertError('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
			PenataJasaRJ.alertError('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.cara_pakai == ''||  o.cara_pakai == null){
			PenataJasaRJ.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
		if(o.verified == '' || o.verified==null){
			PenataJasaRJ.alertError('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
			e=true;
			break;
		}
	}
	if(e==false){
		Datasave_KasirRWJ(false,true);
	}
}





/* ================================================INSERT SQL=========================================================== */
function Datasave_Diagnosa_SQL(mBol){	
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/saveDiagnosaPoliklinik",
		params: getParamDetailTransaksiDiagnosa2(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				
			}else if  (cst.success === false && cst.pesan===0){
				ShowPesanWarningDiagnosa("Error simpan SQL",nmHeaderSimpanData);
			}else{
				
				ShowPesanErrorDiagnosa('Error simpan SQL',nmHeaderSimpanData);
			}
		}
	});
}

function DataDeleteDiagnosaDetail_SQL(){
    Ext.Ajax.request({
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail_SQL(),
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    
                } else{
                    ShowPesanWarningDiagnosa('Error delete SQL',nmHeaderHapusData);
                };
            }
        }
    );
};

function getParamDataDeleteDiagnosaDetail_SQL(){
    var params = {
        Table: 'SQLViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT
    };
    return params;
};

function datasave_TrPenJasRWJ_SQL(){
	if (ValidasiEntryIcd9_RWJ(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/saveIcd9",
				params: getParamIcd9RWJ(),
				failure: function(o)
				{
					ShowPesanErrorRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
					}
					else 
					{
						ShowPesanErrorRWJ('Gagal Menyimpan SQL Tindakan', 'Error');
					};
				}
			}
			
		)
	}
}

function hapusICD9_SQL(kd_icd9,urut){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/hapusBarisGridIcd",
		params:{
			kd_pasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
			kd_unit:Ext.getCmp('txtKdUnitRWJ').getValue(), 
			tgl_masuk:Ext.getCmp('dtpTanggalDetransaksi').getValue(), 
			urut_masuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
			no_transaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
			kd_kasir:currentKdKasirRWJ,
			kd_icd9:kd_icd9,
			urut:urut
		},
		failure: function(o){
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
			}
			else{
				ShowPesanErrorRWJ('Gagal menghapus tindakan SQL', 'Error');
			};
		}
	})
}

function Datasave_KasirRWJ_SQL(id_mrresep,jasa){
	Ext.Ajax.request({
		url			: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savedetailpenyakit",
		params		: getParamDetailTransaksiRWJ_SQL(id_mrresep),
		failure		: function(o){
			ShowPesanWarningRWJ('Error SQL hubungi admin!', 'Gagal');
		},
		success		: function(o){
			RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				if(jasa == true){
					SimpanJasaDokterPenindak_SQL(currentJasaDokterKdProduk_RWJ,currentJasaDokterKdTarif_RWJ,currentJasaDokterUrutDetailTransaksi_RWJ,currentJasaDokterHargaJP_RWJ);
				}
			}else{
				ShowPesanWarningRWJ('Data SQL Tidak berhasil disimpan hubungi admin', 'Gagal');
			}
		}
	});
}

function getParamDetailTransaksiRWJ_SQL(id_mrresep){
    var params={
		Table			: 'ViewTrKasirRwj',
		TrKodeTranskasi	: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit			: Ext.get('txtKdUnitRWJ').getValue(),
		kdDokter		: Ext.get('txtKdDokter').getValue(),
		Tgl				: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
		Shift			: tampungshiftsekarang,
		List			: getArrDetailTrRWJ(),
		JmlField		: mRecordRwj.prototype.fields.length-4,
		JmlList			: GetListCountDetailTransaksi(),
		Hapus			: 1,
		Ubah			: 0,
		id_mrresep		: id_mrresep,
	};
    params.jmlObat	= PenataJasaRJ.dsGridObat.getRange().length;
    params.urut_masuk	= PenataJasaRJ.s1.data.URUT_MASUK;
    params.kd_pasien	= PenataJasaRJ.s1.data.KD_PASIEN;
	if(PenataJasaRJ.dsGridObat.getCount() > 0){
		for(var i=0, iLen= params.jmlObat ;i<iLen;i++){
			var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
			params['kd_prd'+i]	= o.kd_prd;
			params['jumlah'+i]	= o.jumlah;
			params['cara_pakai'+i]	= o.cara_pakai;
			params['verified'+i]	= o.verified;
			params['racikan'+i]	= o.racikan;
			params['kd_dokter'+i]	= o.kd_dokter;
			params['urut' + i]  = o.urut;
		}
		params['resep']  = true;
	} else{
		params['resep']  = false;
	}
    return params;
}

function SimpanJasaDokterPenindak_SQL(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorRWJ('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_RWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoRWJ('Dokter penindak berhasil disimpan','Information');
				GetgridEditDokterPenindakJasa_RWJ();
			} 
			else 
			{
				ShowPesanErrorRWJ('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function saveRujukanLab_SQL(no_transaksi){
	Ext.Ajax.request({
		url			: baseURL + "index.php/rawat_jalan_sql/functionLABPoliklinik/savedetaillab",
		params		: getParamDetailTransaksiLAB_SQL(no_transaksi),
		failure		: function(o){
			ShowPesanWarningRWJ('SQL Data Tidak berhasil disimpan hubungi admin', 'Gagal');
			//PenataJasaRJ.var_kd_dokter_leb="";
		},success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				
			}else if(cst.success === false && cst.cari=== false)
			{
				ShowPesanWarningRWJ('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya - SQL', 'Gagal');
			}else{
				//PenataJasaRJ.var_kd_dokter_leb="";
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin  - SQL', 'Gagal');
			}
		}
	});
}

function getParamDetailTransaksiLAB_SQL(no_transaksi) 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	
		KdPasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRWJ').getValue(),
		KdDokter:PenataJasaRJ.var_kd_dokter_leb,
		KdDokter_mr_tindakan:Ext.get('txtKdDokter').dom.value ,
		kd_kasir:'default_kd_kasir_rwj',
		Modul:'rwj',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		List:getArrPoliLab(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:PenataJasaRJ.ds3.getCount(),
		unit:41,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		newnotrans:no_transaksi,
		KdKasirAsal:'01',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter: [],
		unitaktif:'rwj'
	};
    return params
};

function saveRujukanRad_SQL(no_transaksi){
	Ext.Ajax.request({
		url			: baseURL + "index.php/rawat_jalan_sql/functionRADPoliklinik/savedetailrad",
		params		: getParamDetailTransaksiRAD_SQL(no_transaksi),
		failure		: function(o){
			ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
			PenataJasaRJ.var_kd_dokter_rad="";
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
				var o=gridPenataJasaTabItemTransaksiRWJ.getSelectionModel().getSelections()[0].data;
				var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
					//	ViewGridBawahpoliLab(o.KD_PASIEN);
					ViewGridBawahpoliRad(o.NO_TRANSAKSI,Ext.getCmp('txtKdUnitRWJ').getValue(),o.TANGGAL_TRANSAKSI,o.URUT_MASUK);
			}else if(cst.success === false && cst.cari=== false)
			{
				//PenataJasaRJ.var_kd_dokter_rad="";
				ShowPesanWarningRWJ('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya', 'Gagal');
			}else{
				//PenataJasaRJ.var_kd_dokter_rad="";
				ShowPesanWarningRWJ('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
			}
		}
	});
}

function getParamDetailTransaksiRAD_SQL(no_transaksi) 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	
		newnotrans:no_transaksi,
		KdPasien:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitRWJ').getValue(),
		KdDokter:PenataJasaRJ.var_kd_dokter_rad,
		KdDokter_mr_tindakan:Ext.get('txtKdDokter').dom.value ,
		kd_kasir:'default_kd_kasir_rwj',
		Modul:'rwj',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		Modul:'rwj',
		List:getArrPoliRad(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:PenataJasaRJ.ds3.getCount(),
		unit:5,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirrwj').getValue(),
		KdKasirAsal:'01',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter	: [],
		unitaktif:'rwj'
	};
    return params
};


function hapusBarisGridResepOnline_SQL(kd_prd,id_mrresep,urut){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/hapusBarisGridObat",
			params:{kd_prd:kd_prd, id_mrresep:id_mrresep,urut:urut} ,
			failure: function(o)
			{
				ShowPesanErrorRWJ('SQL, Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
				}
				else 
				{
					ShowPesanErrorRWJ('SQL, Gagal melakukan penghapusan!', 'Error');
				};
			}
		}
		
	)
}

function deleteKasirRWJDetail_SQL(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/DeleteDataKasirDetail",
		params:  getParamDataDeleteKasirRWJDetail(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				
			}else{
				ShowPesanWarningRWJ('SQL, Gagal menghapus data ini!',nmHeaderHapusData);
			}
		}
	});
}

function DataSaveKamarOperasi_SQL(mBol){	
		Ext.Ajax.request({
			url			: baseURL + "index.php/rawat_jalan_sql/functionRWJ/save_order_mng",
			params		: getParamok(),
			failure		: function(o){
				ShowPesanWarningRWJ('SQL, Hubungi admin!', 'Gagal');
			},
			success		: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoRWJ('Data berhasil disimpan', 'Information');
					//RefreshDataFilterKasirRWJ();
					var paramresep ={
						KD_PASIEN:Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
						KD_UNIT:Ext.getCmp('txtKdUnitRWJ').getValue(),
						TANGGAL_TRANSAKSI:Ext.get('dtpTanggalDetransaksi').getValue()
					};
					RefreshDataKasirRWJDetai2(paramresep);
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else if(cst.success === false && cst.cari === true ){
					ShowPesanWarningRWJ('Pasien telah terjadwal di Kamar operasi ', 'Gagal');
				}
				else{
					ShowPesanWarningRWJ('SQL, Data Tidak berhasil disimpan hubungi admin', 'Gagal');
				}
			}
		});

}


function Datasave_Konsultasi_SQL(no_transaksi_konsul){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/KonsultasiPenataJasa",					
		params: getParamKonsultasi_SQL(no_transaksi_konsul),
		failure: function(o){
			ShowPesanWarningRWJ('SQL, Error konsultasi ulang. Hubungi admin!', 'Gagal');
		},	
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				
			}else {
				ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
			}
		}
	});
}

function getParamKonsultasi_SQL(no_transaksi_konsul) {
    var params ={
		Table:'ViewTrKasirRwj', 
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRWJ').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer,
		new_no_transaksi_konsul:no_transaksi_konsul
	};
    return params;
}


function GantiDokter_SQL(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/gantidokter",		
		params: getParamGantiDokter(),
		failure: function(o){
			 ShowPesanErrorRWJ('SQL, Error, Hubungi Admin!','Ganti Dokter');
		},	
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				/* FormDepanDokter.close();
				FormLookUpGantidokter.close(); */
			}else{
				ShowPesanErrorRWJ('SQL, Gagal ganti Dokter!','Ganti Dokter');
			}
		}
	});
}

function Datasave_Kelompokpasien_SQL(){
	Ext.Ajax.request ({
		url: baseURL +  "index.php/rawat_jalan_sql/functionRWJ/UpdateKdCustomer",	
		params: getParamKelompokpasien(),
		failure: function(o){
			ShowPesanWarningRWJ('SQL, Error ganti customer, Hubungi Admin!', 'Gagal');
		},	
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				
			}else{
				//ShowPesanWarningRWJ('SQL, Simpan kelompok pasien gagal!', 'Gagal');
			}
		}
	});
}