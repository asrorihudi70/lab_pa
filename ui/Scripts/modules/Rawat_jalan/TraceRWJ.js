
var now = new Date();
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = yyyy+'/'+mm+'/'+dd;
var dsSpesialisasiTracerRWJ;
var ListHasilTracerRWJ;
var rowSelectedHasilTracerRWJ;
var dsKelasTracerRWJ;
var dsKamarTracerRWJ;
var dsTracerRWJ;
var dataSource_TracerRWJ;
var TMPTANGGAL = now;
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};

var tmpNoMedrec;
var tmpNamaPasien;
var tmpAlamat;
var tmpPoli;
var tmpTanggalMasuk;
var tmpKdDokter;
var gridListHasilTracerRWJ;

var rowSelectedTracer_viDaftar;


CurrentPage.page = getPanelTracerRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelTracerRWJ(mod_id) {
    var i = setInterval(function(){
        loadMask.hide();
        load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 'false'  Order By k.jam_masuk DESC ,u.NAMA_UNIT, ps.nama asc"); //and ap.sts_ditemukan = 'false'
        loadMask.hide();
    }, 4000);

    var Field = ['PRINT', 'URUT', 'MEDREC', 'PASIEN',
                 'UNIT', 'TANGGAL','JAM_MASUK', 'CUSTOMER', 'UMUR',
                 'NO_ANTRI','DITEMUKAN','KD_UNIT','POLI','DOKTER','ALAMAT','NAMA_DOKTER'];

    dataSource_TracerRWJ = new WebApp.DataStore({
        fields: Field
    });
    // load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 'false'  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
    loadDataComboPencarianMedrec_TracerRWJ();
	gridListHasilTracerRWJ = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_TracerRWJ,
        id: 'gridTracerRWJ',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelectedTracer_viDaftar = undefined;
                    rowSelectedTracer_viDaftar = dataSource_TracerRWJ.getAt(row);
                    // CurrentData_viDaftar.row = row;
                    // CurrentData_viDaftar.data = rowSelectedTracer_viDaftar;
                    tmpNoMedrec = rowSelectedTracer_viDaftar.data.MEDREC;
                    tmpNamaPasien = rowSelectedTracer_viDaftar.data.PASIEN;
                    tmpAlamat = rowSelectedTracer_viDaftar.data.ALAMAT;
                    tmpPoli = rowSelectedTracer_viDaftar.data.KD_UNIT;
                    tmpTanggalMasuk = rowSelectedTracer_viDaftar.data.TANGGAL;
                    tmpKdDokter = rowSelectedTracer_viDaftar.data.DOKTER;

                    // console.log(tmpNoMedrec+' | '+tmpNamaPasien+' | '+tmpAlamat+' | '+tmpPoli+' | '+tmpTanggalMasuk+' | '+tmpKdDokter)
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelectedTracer_viDaftar = dataSource_TracerRWJ.getAt(ridx);
                console.log(rowSelectedTracer_viDaftar);
                var tmpkdpasien = rowSelectedTracer_viDaftar.data.MEDREC;
                var tmpkdunit = rowSelectedTracer_viDaftar.data.KD_UNIT;
                var tmpantrian = rowSelectedTracer_viDaftar.data.NO_ANTRI;
                var tmptgl_masuk = rowSelectedTracer_viDaftar.data.TANGGAL;
                var tmpkd_dokter = rowSelectedTracer_viDaftar.data.DOKTER;
                Ext.Msg.confirm("Konfirmasi", "Data Telah Ditemukan", function(btnText){
                    if(btnText === "no"){
                    }
                    else if(btnText === "yes"){
                        SavingData(tmpkdpasien,tmpkdunit,tmpantrian,tmptgl_masuk,tmpkd_dokter);
                    }
                }, this);
                // SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian);
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colprinttracertracerRWJ',
                        header: 'Print',
                        dataIndex: '',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
                        editor: {
                            xtype: 'checkbox',
                            id: 'cbstatusgrid',
                            listener:
                                    {
                                    }
                        },
                        renderer: function (value) {
                            if (value === true || value === 'Aktif')
                            {
                                return 'Ya';
                            } else
                            {
                                return 'Tidak';
                            }
                        }
                    },
                    {
                        id: 'colUrutTracerRWJ',
                        header: 'Urut',
                        dataIndex: 'URUT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colmedrecTracerRWJ',
                        header: 'medrec',
                        dataIndex: 'MEDREC',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colPasienTracerRWJ',
                        header: 'Pasien',
                        dataIndex: 'PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colUnitTracerRWJ',
                        header: 'Unit',
                        dataIndex: 'UNIT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colDokterTracerRWJ',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colnTanggalTracerRWJ',
                        header: 'Tanggal',
                        dataIndex: 'TANGGAL',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                     {
                        id: 'colnJamTracerRWJ',
                        header: 'Jam',
                        dataIndex: 'JAM_MASUK',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colCustomerTracerRWJ',
                        header: 'Customer',
                        dataIndex: 'CUSTOMER',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colUmurTracerRWJ',
                        header: 'Umur',
                        dataIndex: 'UMUR',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colAntrianTracerRWJ',
                        header: 'No. Antrian',
                        dataIndex: 'NO_ANTRI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colDitemukanTracerRWJ',
                        header: 'Ditemukan',
                        dataIndex: 'DITEMUKAN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
                        renderer: function (value) {
                            if (value === true)
                            {
                                return 'Ya';
                            } else
                            {
                                return 'Tidak';
                            }
                        }
                    }
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnprinttracertracerRWJ',
                text: 'Print',
                tooltip: 'Cetak',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    printtracertracer();
                }
            },
            {
                id: 'btnSimpanTracerRWJ',
                text: 'Report',
                tooltip: 'Report',
                iconCls: 'save',
                handler: function (sm, row, rec) {
                }
            }
        ]
    });
    var FormDepanTracerRWJ = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Tracer RWJ',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianTracerRWJ(),
            gridListHasilTracerRWJ
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboJenisPencarianStatusTracer').hide();
                Ext.getCmp('cboUnitTracer').hide();
                Ext.getCmp('cboJenisPencarian').setValue('Semua');
                Ext.getCmp('cboJumlahData').setValue('100');
                Ext.getCmp('txtAllData').setValue('Semua');
                Ext.getCmp('txtAllData').setDisabled(true);
            }
        }
    });
    return FormDepanTracerRWJ;
}
;
function getPanelPencarianTracerRWJ() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. medrec '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 150,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtkodeTracerRWJ',
                        width: 100,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoIIMedrec = Ext.get('txtkodeTracerRWJ').getValue()
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoIIMedrec.length === 10)
                                        {
                                            if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10)
                                            {
                                                var tmpgetNoIIMedrec = formatnomedrec(Ext.get('txtkodeTracerRWJ').getValue())
                                                Ext.getCmp('txtkodeTracerRWJ').setValue(tmpgetNoIIMedrec);
                                                var tmpkriteria = getCriteriaFilter_viDaftar();
                                                load_TracerRWJ(tmpkriteria);

                                            } else
                                            {
                                                if (tmpNoIIMedrec.length === 10)
                                                {
                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                    load_TracerRWJ(tmpkriteria);
                                                    
                                                } else
                                                    Ext.getCmp('txtNoMedrec').setValue('')
                                            }
                                        }
                                        // if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        // {
                                        //     var tmpkriteria = getCriteriaFilter_viDaftar();
                                        //     load_TracerRWJ(tmpkriteria);
                                        // }
                                    }
                                }
                    },
                    // {
                    //     x: 240,
                    //     y: 10,
                    //     xtype: 'label',
                    //     text: 'No. Medrec '
                    // },
                    // {
                    //     x: 310,
                    //     y: 10,
                    //     xtype: 'label',
                    //     text: ': '
                    // },
                    // mComboPencarianMedrecTracer(),
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Jenis Pencarian '
                    },
                    {
                        x: 140,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboJenisPencarianTracer(),
                    mComboJenisPencarianStatusTracer(),
                    mComboUnitTracer(),
                    {
                        x: 270,
                        y: 40,
                        xtype: 'textfield',
                        id: 'txtAllData',
                        value:'Semua',
                        width:100
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Banyak Data '
                    },
                    {
                        x: 140,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboJumlahDataTracer(),
					{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Range No. Medrec '
                    },
                    {
                        x: 140,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    ComboPencarianMedrec_TracerRWJ(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataTracerRWJ',
                        handler: function () {
                            // var tmpkriteria = getCriteriaFilter_viDaftar();
                            // load_TracerRWJ(tmpkriteria);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;



//combo dan load database

function datainit_TracerRWJ(rowdata)
{
    load_detsmp(rowdata.TANGGAL, rowdata.KD_UNIT, rowdata.NO_KAMAR, rowdata.KD_TracerRWJ, rowdata.KD_WAKTU);

    TMPTANGGAL = rowdata.TANGGAL;
    TMPKD_UNIT = rowdata.KD_UNIT;
    TMPNO_KAMAR = rowdata.NO_KAMAR;
    TMPKD_TracerRWJ = rowdata.KD_TracerRWJ;
    TMPKD_WAKTU = rowdata.KD_WAKTU;

    Ext.getCmp('dtpTanggalPelaksanaan').setValue(rowdata.TANGGAL);
    Ext.getCmp('cboSpesialisasiTracerRWJpopup').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('cbokelasTracerRWJpopup').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('cboKamarTracerRWJpopup').setValue(rowdata.NAMA_KAMAR);
    Ext.getCmp('cboTracerRWJTracerRWJPopUp').setValue(rowdata.TracerRWJ);
    Ext.getCmp('cboWaktuTracerRWJPopUp').setValue(rowdata.WAKTU);


}

function load_TracerRWJ(criteria)
{
    
    
    // var criteria = "k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 0  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc";
    
    dataSource_TracerRWJ.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'kd_TracerRWJ',
                                    Sortdir: 'ASC',
                                    target: 'ViewTracerRWJ',
                                    param: criteria
                                }
                    }
            );
    return dataSource_TracerRWJ;
}

function getCriteriaFilter_viDaftar()
{
    var tmpkode = Ext.getCmp('txtkodeTracerRWJ').getValue();
    var tmpJenisPencarian = Ext.getCmp('cboJenisPencarian').getValue();
    var tmpbanyakdata = Ext.getCmp('cboJumlahData').getValue();
    var tmpkriteria = "k.tgl_masuk = '"+today+"' "
    if (tmpkode !== '') {
        tmpkriteria += " And ps.Kd_Pasien = '"+tmpkode+"'"
    }
    if (tmpJenisPencarian === 'Unit')
    {
        tmpkriteria += " and k.kd_unit = '"+Ext.getCmp('cboUnitTracer').getValue()+"'";
    } else if (tmpJenisPencarian === 'Status') {}
    {
        if(Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Semua'){
            tmpkriteria += '';
        }else{
            if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Semua') {
                tmpkriteria +="";
            }else if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Ditemukan'){
                tmpkriteria += " and AP.STS_DITEMUKAN = 'true'";    
            }else{
                tmpkriteria += " and AP.STS_DITEMUKAN = 'FALSE'";
            }
            
        }
        
    }

    return tmpkriteria;
}
;


function SavingData(tmpkdpasien,tmpkdunit,tmpantrian,tmptgl_masuk,tmpkd_dokter)
{

    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWJ/updatestatusantrian",
                        params: {
                                    kd_pasien:tmpkdpasien,
                                    kd_unit :tmpkdunit,
                                    tgl_masuk: tmptgl_masuk,
                                    kd_dokter: tmpkd_dokter
                                },
                        failure: function (o)
                        {
                            ShowPesanWarningTracerRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 'false'  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoTracerRWJ('Proses Saving Berhasil', 'Save');
                                SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian);
                                load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 'false'  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                            }
                            else
                            {
                                ShowPesanWarningTracerRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 'false'  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");

                            }
                            ;
                        }
                    }
            );
}
;

function SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian){

    var params =
        {
            Table: 'ViewTracerRWJ',
            KDPASIEN: tmpkdpasien,
            KDUNIT: tmpkdunit,
            ANTRIAN:tmpantrian
        };

    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/CreateDataObj",
            params: params,
            failure: function (o)
            {
                ShowPesanWarningTracerRWJ('Error Database SQL !', '');
                // load_TracerRWJ("");
            },
            success: function (o)
            {
                // var cst = Ext.decode(o.responseText);
                // if (cst.success === true)
                // {
                //     ShowPesanInfoTracerRWJ('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
                //     load_TracerRWJ("");
                // } else if (cst.success === false && cst.pesan === 0)
                // {
                //     ShowPesanWarningTracerRWJ('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');
                //     load_TracerRWJ("");
                // }
            }
        }
    )
}

function ShowPesanWarningTracerRWJ(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoTracerRWJ(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningTracerRWJ('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoTracerRWJ('Data berhasil di hapus', 'Information');
                                dataSource_TracerRWJ.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusTracerRWJ').disable();
                                Ext.getCmp('btnBatalTracerRWJ').disable();
                                Ext.getCmp('btnSimpanTracerRWJ').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_TracerRWJ.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusTracerRWJ').disable();
                                    Ext.getCmp('btnBatalTracerRWJ').disable();
                                    Ext.getCmp('btnSimpanTracerRWJ').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningTracerRWJ('Gagal menghapus data', 'Error');
                                }
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepTracerRWJ',
                query: "kd_TracerRWJ = '" + dataSource_TracerRWJ.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}

function mComboUnitTracer()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsUnitTracer = new WebApp.DataStore({fields: Field});
    dsUnitTracer.load
    (
        {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'NAMA_UNIT',
                        Sortdir: 'ASC',
                        // target: 'ViewSetupUnit'
                        target: 'ComboUnit',
                        param: "kd_bagian=2 and type_unit=false"
                    }
        }
    )
    var cboUnitTracer = new Ext.form.ComboBox
            (
                    {
                        x: 270,
                        y: 40,
                        id: 'cboUnitTracer',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        emptyText: 'Pilih Unit',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsUnitTracer,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        // anchor: '20%',
                        width:170,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                        load_TracerRWJ(tmpkriteria);
                                    },
                                    // 'render': function (c) {
                                    //     c.getEl().on('keypress', function (e) {
                                    //         if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                    //             Ext.getCmp('cboKamarPengkajian').focus();
                                    //     }, c);
                                    // }

                                }
                    }
            );
    return cboUnitTracer;
}
;

function mComboJenisPencarianTracer()
{
    var cboJenisPencarian = new Ext.form.ComboBox
            (
                    {
                        x: 150,
                        y: 40,
                        id: 'cboJenisPencarian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        // emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [
                                            ['Semua', 'Semua'], ['Unit', 'Unit'],['Status', 'Status']
                                          ]
                                }
                        ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpdata = Ext.getCmp('cboJenisPencarian').getValue();
                                        if (tmpdata == 'Unit') {
                                          Ext.getCmp('cboJenisPencarianStatusTracer').hide();                                          
                                          Ext.getCmp('cboUnitTracer').show();
                                          Ext.getCmp('txtAllData').hide();
                                        } else if (tmpdata == 'Status')
                                        {
                                          Ext.getCmp('cboUnitTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').show();
                                          Ext.getCmp('txtAllData').hide();
                                        }else
                                        {
                                          Ext.getCmp('txtAllData').show();
                                          Ext.getCmp('cboUnitTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').setValue('Semua');

                                        }
                                    }

                                }
                    }
            );
    return cboJenisPencarian;
}
;

function mComboJenisPencarianStatusTracer()
{
    var cboJenisPencarianStatusTracer = new Ext.form.ComboBox
            (
                    {
                        x: 270,
                        y: 40,
                        id: 'cboJenisPencarianStatusTracer',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        // emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [
                                            ['Semua', 'Semua'], ['Ditemukan', 'Ditemukan'],['Blm Ditemukan', 'Blm Ditemukan']
                                          ]
                                }
                        ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                        load_TracerRWJ(tmpkriteria);
                                    }

                                }
                    }
            );
    return cboJenisPencarianStatusTracer;
}
;

function mComboPencarianMedrecTracer()
{
    var Field = ['KODE', 'NAMA'];
    dsPencarianMedrecTracer = new WebApp.DataStore({fields: Field});
    var cboPencarianMedrec = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 10,
                        id: 'cboPencarianMedrec',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsPencarianMedrecTracer,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        // Ext.getCmp('cboKamarPengkajian').setValue('');
                                        // loaddatastoreKamarPengkajian(b.data.kd_unit);
                                        // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        // load_pengkajian(tmpkriteriaPengkajian);
                                    },
                                    // 'render': function (c) {
                                    //     c.getEl().on('keypress', function (e) {
                                    //         if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                    //             Ext.getCmp('cboKamarPengkajian').focus();
                                    //     }, c);
                                    // }

                                }
                    }
            );
    return cboPencarianMedrec;
}
;

function mComboJumlahDataTracer()
{
    var cboJumlahData = new Ext.form.ComboBox
            (
                    {
                        x: 150,
                        y: 70,
                        id: 'cboJumlahData',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [
                                           ['100', '100'], ['200', '200'],['300', '300'], ['400', '400'],
                                           ['500', '500'], ['600', '600'],['700', '700'], ['800', '800'],
                                           ['900', '900'],['1000', '1000']
                                          ]
                                }
                        ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        // Ext.getCmp('cboKamarPengkajian').setValue('');
                                        // loaddatastoreKamarPengkajian(b.data.kd_unit);
                                        // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        // load_pengkajian(tmpkriteriaPengkajian);
                                    },

                                }
                    }
            );
    return cboJumlahData;
}
;

function printtracertracer()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/rawat_jalan/tracer/save",
                        params: datatracer(),
                        failure: function (o)
                        {
                            ShowPesanWarningTracerRWJ('Tracer segera di cetak ', 'Cetak Data');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoTracerRWJ('Tracer segera  di cetak', 'Cetak Data');
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningTracerRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningTracerRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            )
}

function datatracer()
{
    var paramstracependaftaran =
            {
                // Table: 'Tracer',
                NoMedrec: tmpNoMedrec,
                NamaPasien: tmpNamaPasien,
                Alamat: tmpAlamat,
                Poli: tmpPoli,
                TanggalMasuk: tmpTanggalMasuk,
                KdDokter: tmpKdDokter,
            }
    return paramstracependaftaran
}

function loadDataComboPencarianMedrec_TracerRWJ(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/tracer/gettracerpencarianmedrec",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboPencarianMedrec_TracerRWJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsPencarianMedrec_TracerRWJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsPencarianMedrec_TracerRWJ.add(recs);
			}
		}
	});
}


function ComboPencarianMedrec_TracerRWJ()
{
	var Field = ['no_urut','value'];
    dsPencarianMedrec_TracerRWJ = new WebApp.DataStore({fields: Field});
    cboPencarianMedrec_TracerRWJ = new Ext.form.ComboBox
	(
            {
                x: 150,
                y: 100,
                id:'cboPencarianMedrec_TracerRWJ',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'Silahkan Pilih...',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:220,
                store: dsPencarianMedrec_TracerRWJ,
                valueField: 'no_urut',
                displayField: 'value',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							loadCriteriaPencarianMedrec(b.data.no_urut);
							selectSetPilihan=b.data.displayText ;
                        }
                }
            }
	);
	return cboPencarianMedrec_TracerRWJ;
};

function loadCriteriaPencarianMedrec(no_urut){
	var statusditemukan='';
	if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Semua') {
		statusditemukan ="";
	}else if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Ditemukan'){
		statusditemukan = " and AP.STS_DITEMUKAN = 'true'";    
	}else{
		statusditemukan = " and AP.STS_DITEMUKAN = 'false'";
	}
	
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/tracer/getmedrecgrid",
		params: {
			no_urut : no_urut,
			status : statusditemukan,
			kd_unit : Ext.getCmp('cboUnitTracer').getValue()
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dataSource_TracerRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dataSource_TracerRWJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dataSource_TracerRWJ.add(recs);
				gridListHasilTracerRWJ.getView().refresh();
			} 
			else 
			{
				ShowPesanWarningTracerRWJ('Gagal pencarian range medrec', 'WARNING');
			};
		}
	});
}