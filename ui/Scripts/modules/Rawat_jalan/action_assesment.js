
function action_asessmen_CHANGERADIO(kd_askep,datas){
    if(kd_askep=='RWJ_ASKEP2_GIZI_BB_TIDAK' || kd_askep=='RWJ_ASKEP2_GIZI_BB_TIDAK_TAHU' || kd_askep=='RWJ_ASKEP2_GIZI_BB_YA' || 
        kd_askep=='RWJ_ASKEP2_GIZI_BB_YA1' || kd_askep=='RWJ_ASKEP2_GIZI_BB_YA2'  || kd_askep=='RWJ_ASKEP2_GIZI_BB_YA3' ||  kd_askep=='RWJ_ASKEP2_GIZI_BB_YA4' ||
        kd_askep=='RWJ_ASKEP2_GIZI_ASUPAN1' || kd_askep=='RWJ_ASKEP2_GIZI_ASUPAN2'){
        var jum=0;
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_BB_TIDAK_TAHU',datas)=='X'){
            jum+=2;
        }
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_BB_YA1',datas)=='X'){
            jum+=1;
        }
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_BB_YA2',datas)=='X'){
            jum+=2;
        }
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_BB_YA3',datas)=='X'){
            jum+=3;
        }
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_BB_YA4',datas)=='X'){
            jum+=4;
        }
        if(action_asessmen_getData('RWJ_ASKEP2_GIZI_ASUPAN2',datas)=='X'){
            jum+=1;
        }
        action_asessmen_setData('RWJ_ASKEP2_GIZI_SKOR',jum,jum,datas);
    }
}

function action_asessmen_RADIOLIST(dat,datas,idx){
    if(dat.enable_yes != null){
        var kd_askeps=dat.enable_yes.split(',');
        for(var i=0,iLen=kd_askeps.length; i<iLen;i++){
            var kd_askep=kd_askeps[i].split('.');
            if(idx==kd_askep[0]){
                action_asessmen_setEnab(kd_askep[1],1,datas);
            }
        }
    }
    if(dat.disable_yes != null){
        var kd_askeps=dat.disable_yes.split(',');
        for(var i=0,iLen=kd_askeps.length; i<iLen;i++){
            var kd_askep=kd_askeps[i].split('.');
            if(idx==kd_askep[0]){
                action_asessmen_setEnab(kd_askep[1],0,datas);
            }
        }
    }
}
function action_asessmen_getData(kd_askep,datas){
    var dat=datas.getRange();
    for(var i=0,iLen= dat.length; i<iLen;i++){
        if(dat[i].data.kd_askep==kd_askep){
            return dat[i].data.nilai;
            break;
        }
    }
}
function action_asessmen_setData(kd_askep,nilai,nilat_text,datas){
    var dat=datas.getRange();
    for(var i=0,iLen= dat.length; i<iLen;i++){
        if(dat[i].data.kd_askep==kd_askep){
            dat[i].set('nilai',nilai);
            dat[i].set('nilai_text',nilat_text);
            break;
        }
    }
}
function action_asessmen_radio(kd_askep,kd_grup,datas){
    var dat=datas.getRange();
    for(var i=0,iLen= dat.length; i<iLen;i++){
        if(dat[i].data.kd_grup==kd_grup){
            console.log(dat[i].data.kd_askep);
            console.log(kd_askep);
            console.log('-');
            if(dat[i].data.kd_askep==kd_askep){
                dat[i].set('nilai','X');
                dat[i].set('nilai_text','X');
                if(dat[i].data.enable_yes != null && dat[i].data.enable_yes!=''){
                    var kdnya=dat[i].data.enable_yes.split(',');
                    for(var j=0,jLen=kdnya.length; j<jLen;j++){
                        action_asessmen_setEnab(kdnya[j],1,datas);
                    }
                }
                if(dat[i].data.disable_yes != null && dat[i].data.disable_yes!=''){
                    var kdnya=dat[i].data.disable_yes.split(',');
                    for(var j=0,jLen=kdnya.length; j<jLen;j++){
                        action_asessmen_setEnab(kdnya[j],0,datas);
                    }
                }
            }else{
                dat[i].set('nilai','');
                dat[i].set('nilai_text','');
                if(dat[i].data.enable_no != null && dat[i].data.enable_no!=''){
                    var kdnya=dat[i].data.enable_no.split(',');
                    for(var j=0,jLen=kdnya.length; j<jLen;j++){
                        action_asessmen_setEnab(kdnya[j],1,datas);
                    }
                }
                if(dat[i].data.disable_no != null && dat[i].data.disable_no!=''){
                    var kdnya=dat[i].data.disable_no.split(',');
                    for(var j=0,jLen=kdnya.length; j<jLen;j++){
                        action_asessmen_setEnab(kdnya[j],0,datas);
                    }
                }
            }
        }
        
    }
}
function action_asessmen_setEnab(kd_askep,enab,datas){
    var dat=datas.getRange();
    for(var i=0,iLen= dat.length; i<iLen;i++){
        if(dat[i].data.kd_askep==kd_askep){
            dat[i].set('enab',enab);
            dat[i].set('nilai',null);
            dat[i].set('nilai_text',null);
        }
    }
}
function action_asessmen_check(kd_askep,datas){
    var dat=datas.getRange();
    for(var i=0,iLen= dat.length; i<iLen;i++){
        if(dat[i].data.kd_askep==kd_askep){
            if(dat[i].data.nilai=='X'){
                dat[i].set('nilai','');
                dat[i].set('nilai_text','');
                if(dat[i].data.enable_no != null && dat[i].data.enable_no!=''){
                    var kdnya=dat[i].data.enable_no.split(',');
                    for(var i=0,iLen=kdnya.length; i<iLen;i++){
                        action_asessmen_setEnab(kdnya[i],1,datas);
                    }
                }
                if(dat[i].data.disable_no != null && dat[i].data.disable_no!=''){
                    var kdnya=dat[i].data.disable_no.split(',');
                    for(var i=0,iLen=kdnya.length; i<iLen;i++){
                        action_asessmen_setEnab(kdnya[i],0,datas);
                    }
                }
            }else{
                dat[i].set('nilai','X');
                dat[i].set('nilai_text','X');
                if(dat[i].data.enable_yes != null && dat[i].data.enable_yes!=''){
                    var kdnya=dat[i].data.enable_yes.split(',');
                    for(var i=0,iLen=kdnya.length; i<iLen;i++){
                        action_asessmen_setEnab(kdnya[i],1,datas);
                    }
                }
                if(dat[i].data.disable_yes != null && dat[i].data.disable_yes!=''){
                    var kdnya=dat[i].data.disable_yes.split(',');
                    for(var i=0,iLen=kdnya.length; i<iLen;i++){
                        action_asessmen_setEnab(kdnya[i],0,datas);
                    }
                }
            }
        }
    }
}


function action_asessmen_TEXTLIST(dat,datas,val,e){
    // if(e.keyCode==13){
    
    if(e.key == "Enter"){
        var Field = ['id','text'];
        dataSource_AsesmenRWJLog = new Ext.data.ArrayStore({
            fields: Field
        });
        var dial=new Ext.Window({
            title: 'Pencarian',
            closeAction: 'destroy',
            resizable : false,
            layout: 'fit',
            width: 300,
            height: 300,
            constrain: true,
            modal: true,
            tbar:[
                {
                    xtype:'textfield',
                    id:'action_asessmen_TEXTLIST_text',
                    value:val,
                    listeners:{
                        keyup:function(){
                            alert();
                        }
                    }
                },{
                    xtype:'button',
                    id:'action_asessmen_TEXTLIST_button',
                    text:'Cari',
                    handler:function(){
                        Ext.Ajax.request({
                            url: baseURL + "index.php/rawat_jalan/control_askep_rwj/getDataList",
                            params: {
                                kd_askep    : dat.kd_askep,
                                val     : Ext.getCmp('action_asessmen_TEXTLIST_text').getValue(),
                            },
                            method:'GET',
                            failure: function(o){
                                ShowPesanErrorIGD('Hubungi Admin', 'Error');
                            },  
                            success: function(o) {   
                                var cst = Ext.decode(o.responseText);
                                dataSource_AsesmenRWJLog.loadData([],false);
                                for(var i=0; i<cst.length; i++){
                                    dataSource_AsesmenRWJLog.add(new dataSource_AsesmenRWJLog.recordType(cst[i]))
                                }
                                if(cst.length>0){
                                    console.log(Ext.getCmp('action_asessmen_TEXTLIST_grid').getSelectionModel());
                                    Ext.getCmp('action_asessmen_TEXTLIST_grid').getSelectionModel().selectFirstRow();
                                    Ext.getCmp('action_asessmen_TEXTLIST_grid').getView().focusRow(0);
                                }
                            }
                        });
                    }
                }
            ],
            items:[
                 new Ext.grid.EditorGridPanel({
                    stripeRows: true,
                    hideHeaders: true,
                    id:'action_asessmen_TEXTLIST_grid',
                    flex:1,
                    dat:dat,
                    enableKeyEvents: true,
                    datas:datas,
                    viewConfig: {
                        forceFit: true,
                        itemkeydown:function(){
                        }
                    },
                    viewConfig:{
                        listeners:{
                            
                        }
                    },
                    listeners:{
                        rowdblclick:function(a,b){
                            var datanya=dataSource_AsesmenRWJLog.getAt(b);
                            action_asessmen_setData(a.dat.kd_askep,datanya.data.id,datanya.data.text,a.datas);
                            dial.close();
                        },
                        keydown: function(e,v,b,c) {
                            if (e.keyCode==13) {
                                var grid=Ext.getCmp('action_asessmen_TEXTLIST_grid');
                                if(grid.getSelectionModel().selections.items.length>0){
                                    var datanya=grid.getSelectionModel().selections.items[0];
                                    // console.log(grid.dat.kd_askep);
                                    // console.log(datanya.data.id);
                                    // console.log(datanya.data.text);
                                    action_asessmen_setData(grid.dat.kd_askep,datanya.data.id,datanya.data.text,grid.datas);
                                    dial.close();
                                }
                            }
                        }
                    },
                    store: dataSource_AsesmenRWJLog,
                    columnLines: false,
                    autoScroll: true,
                    border: false,
                    sm: new Ext.grid.RowSelectionModel({
                        singleSelect: true,
                        listeners: {
                            rowselect: function (sm, row, rec) {
                            }
                        }
                    }),
                    cm: new Ext.grid.ColumnModel([
                        {
                            width: 300,
                            sortable: false,
                            hideable:true,
                            hidden:true,
                            menuDisabled:true,
                            dataIndex: 'id',
                        },{
                            dataIndex: 'text',
                            sortable: false,
                            hideable:false,
                            menuDisabled:true,
                            width: 300,
                        }
                    ] )
                })
            ]
        }).show();
        Ext.get('action_asessmen_TEXTLIST_button').dom.click();
    }
}