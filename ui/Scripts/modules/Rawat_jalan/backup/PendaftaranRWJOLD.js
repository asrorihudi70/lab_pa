var dataSource_viDaftar;
var selectCount_viDaftar=50;
var NamaForm_viDaftar="Pendaftaran ";
var mod_name_viDaftar="viDaftar";

var now_viDaftar= new Date();
var nowSerahIjazah_viDaftar= new Date();
var urutJenjang_viDaftar;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftar;
var rowSelected_viDaftar;
var setLookUps_viDaftar;
var ds_KABUPATEN_viKasirRwj;
var ds_KECAMATAN_viKasirRwj;
var BlnIsDetail;
var SELECTDATASTUDILANJUT;

var selectPoliPendaftaran;
var selectKelompokPoli;

var selectPendidikanPendaftaran;
var selectPekerjaanPendaftaran;
var selectPendaftaranStatusMarital;
var selectAgamaPendaftaran;
var mNoKunjungan_viKasir='';

var CurrentData_viDaftar =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDaftar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
datarefresh_viDaftar();
function dataGrid_viDaftar(mod_id)
{	
    var FieldMaster_viDaftar = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];

    dataSource_viDaftar = new WebApp.DataStore
	({
        fields: FieldMaster_viDaftar
    });
    
	datarefresh_viDaftar();
    
	var grData_viDaftar = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDaftar,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			plugins : [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDaftar = undefined;
							rowSelected_viDaftar = dataSource_viDaftar.getAt(row);
							CurrentData_viDaftar
							CurrentData_viDaftar.row = row;
							CurrentData_viDaftar.data = rowSelected_viDaftar.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viDaftar = dataSource_viDaftar.getAt(ridx);
					if (rowSelected_viDaftar != undefined)
					{
						setLookUp_viDaftar(rowSelected_viDaftar.data);
					}
					else
					{
						setLookUp_viDaftar();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftar',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					{
						id: 'colNMPASIEN_viDaftar',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					{
						id: 'colALAMAT_viDaftar',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					{
						id: 'colTglKunj_viDaftar',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					{
						id: 'colPoliTujuan_viDaftar',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
				]
				),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viDaftar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDaftar',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDaftar != undefined)
							{
								setLookUp_viDaftar(rowSelected_viDaftar.data)
							}
							else
							{								
								setLookUp_viDaftar();
							}
						}
					}
				]
			},
			// bbar:new WebApp.PaggingBar
			// (
				// {
					// displayInfo: true,
					// store: dataSource_viDaftar,
					// pageSize: selectCount_viDaftar,
					// displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					// emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				// }
			// ),		
			bbar : bbar_paging(mod_name_viDaftar, selectCount_viDaftar, dataSource_viDaftar),
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

    var FrmTabs_viDaftar = new Ext.Panel
    (
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: NamaForm_viDaftar,          
			border: false,  
			closable: true,
			iconCls: 'Studi_Lanjut',
			margins: '0 5 5 0',
			items: [grData_viDaftar],
			tbar:
			[
				{ xtype: 'tbtext', text: 'Tgl Kunjungan: ', cls: 'left-label', width: 90 },
				{
					xtype: 'datefield',
					id: 'txtfilterTglKunjAwal_viDaftar',
					value: now_viDaftar,
					format: 'd/M/Y',
					width: 120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								datarefresh_viDaftar();								
							} 						
						}
					}
				},
				{ xtype: 'tbtext', text: 's/d : ', cls: 'left-label', width: 30 },
				{
					xtype: 'datefield',
					id: 'txtfilterTglKunjAkhir_viDaftar',
					value: now_viDaftar,
					format: 'd/M/Y',
					width: 120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								datarefresh_viDaftar();								
							} 						
						}
					}
				},
				{ xtype: 'tbtext', text: 'Poli Tujuan : ', cls: 'left-label', width: 80 },
				mComboPoli_viDaftar(125, "ComboPoli_viDaftar"),				
				{ xtype: 'tbtext', text: 'No.Medrec : ', cls: 'left-label', width: 80 },
				{
					xtype: 'textfield',
					id: 'txtfilterNoRM_viDaftar',				
					width: 120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								datarefresh_viDaftar();								
							} 						
						}
					}
				},
				{ xtype: 'tbtext', text: 'Nama : ', cls: 'left-label', width: 80 },
				{
					xtype: 'textfield',
					id: 'txtfilterNama_viDaftar',				
					width: 150,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								datarefresh_viDaftar();								
							} 						
						}
					}
				},
				{ xtype: 'tbfill' },
				{
					xtype: 'button',
					id: 'btnRefreshFilter_viDaftar',
					iconCls: 'refresh',
					handler: function()
					{
						datarefresh_viDaftar();
					}
				}
				
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					datarefresh_viDaftar();
				}
			}
		}
    )

    datarefresh_viDaftar();
    return FrmTabs_viDaftar;
}

function mComboPoli_viDaftar(lebar,Nama_ID)
{
    /*var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target:'viUNIT',
                param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
	
    var cbo_Poli_viDaftar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			store: ds_Poli_viDaftar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )
    
    return cbo_Poli_viDaftar;*/
}

function setLookUp_viDaftar(rowdata)
{/*
    var lebar = 860;
    setLookUps_viDaftar = new Ext.Window
    (
    {
        id: 'SetLookUps_viDaftar',
		name: 'SetLookUps_viDaftar',
        title: NamaForm_viDaftar, 
        closeAction: 'destroy',        
        width: lebar,
        height: 580,//575,
        resizable:false,
		autoScroll: false,
       // border: true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftar(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDaftar=undefined;
                datarefresh_viDaftar();
				mNoKunjungan_viKasir = '';
            }
        }
    }
    );

    setLookUps_viDaftar.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftar();
		// Ext.getCmp('btnSimpan_viDaftar').disable();
		// Ext.getCmp('btnSimpanExit_viDaftar').disable();	
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viDaftar(rowdata);
    }*/
}

function getFormItemEntry_viDaftar(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftar = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,//lebar-55,
			border: false,
			items:[getItemPanelInputBiodata_viDaftar(lebar), getItemDataKunjungan_viDaftar(lebar)],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDaftar',
						handler: function(){
							dataaddnew_viDaftar();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar();
							if (x===undefined)
							{
								setLookUps_viDaftar.close();
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viDaftar',
						handler: function()
						{
							// BlnIsDetail=false;
							datadelete_viDaftar();
							datarefresh_viDaftar();
							
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype:'button',
						text:'Lookup Pasien',
						iconCls:'find',
						id:'btnLookUp_viDaftar',
						handler:function(){
							// FormLookupPasien();
							var strKriteria='';							
							strKriteria = getKriteriaCariLookup();
							FormLookupPasien(strKriteria,nFormPendaftaran,'',Ext.get('txtKDPasien_viDaftar').getValue(),Ext.get('txtNamaPs_viDaftar').getValue(),Ext.get('txtAlamat_viDaftar').getValue());
						}
					},					
					{
						xtype:'tbseparator'
					},
					{
						xtype:'button',
						text:'Print Kartu Pasien',
						iconCls:'print',
						id:'btnPrint_viDaftar',
						handler:function()
						{							
							if (mNoKunjungan_viKasir != '' )
							{														
								GetPrintKartu(mNoKunjungan_viKasir);								
							}
							else
							{								
								ShowPesanWarning_viDaftar('Data pasien tidak ada ' ,'Print kartu pasien');
							}
							
						}
					}
				]
			}//,items:
			
		}
    )

    return pnlFormDataBasic_viDaftar;*/
}
//------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------///
function datasave_viDaftar(mBol)
{	
    // if (ValidasiEntry_viDaftar('Simpan Data',false) == 1 )
    // {
    //     if (addNew_viDaftar == true)
    //     {
    //         Ext.Ajax.request
    //         (
				// {
				// 	url: "./Datapool.mvc/CreateDataObj",
				// 	params: dataparam_viDaftar(),
				// 	success: function(o)
				// 	{
				// 		var cst = Ext.decode(o.responseText);
				// 		if (cst.success === true)
				// 		{
				// 			ShowPesanInfo_viDaftar('Data berhasil di simpan','Simpan Data');
				// 			datarefresh_viDaftar();
				// 			// if(mBol === false)
				// 			// {
				// 				// Ext.get('txtID_viDaftar').dom.value=cst.ID_SETUP;
				// 			// };
				// 			addNew_viDaftar = false;
				// 			Ext.get('txtNoKunjungan_viDaftar').dom.value = cst.NO_KUNJUNGAN
				// 			Ext.get('txtKDPasien_viDaftar').dom.value = cst.KD_PASIEN
				// 			Ext.getCmp('btnSimpan_viDaftar').disable();	
				// 			Ext.getCmp('btnSimpanExit_viDaftar').disable();	
				// 			Ext.getCmp('btnDelete_viDaftar').enable();								
				// 		}
				// 		else if  (cst.success === false && cst.pesan===0)
				// 		{
				// 			ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				// 		}
				// 		else
				// 		{
				// 			ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				// 		}
				// 	}
				// }
    //         )
    //     }
    //     else
    //     {
    //         Ext.Ajax.request
    //         (
    //         {
    //             url: "./Datapool.mvc/UpdateDataObj",
    //             params: dataparam_viDaftar(),
    //             success: function(o)
    //             {
    //                 var cst = Ext.decode(o.responseText);
    //                 if (cst.success === true)
    //                 {
    //                     ShowPesanInfo_viDaftar('Data berhasil disimpan','Edit Data');
    //                     datarefresh_viDaftar();
    //                 }
    //                 else if  (cst.success === false && cst.pesan===0)
    //                 {
    //                     ShowPesanWarning_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
    //                 }
    //                 else
    //                 {
    //                     ShowPesanError_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
    //                 }
    //             }
    //         }
    //         )
    //     }
    // }
    // else
    // {
    //     if(mBol === true)
    //     {
    //         return false;
    //     }
    // }
    // return 0;
}

function datadelete_viDaftar()
{
    // // var DataHapus = Ext.get('txtNPP_viDaftar').getValue();
    // if (ValidasiEntry_viDaftar('Hapus Data',true) == 1 )
    // {
    //     Ext.Msg.show
    //     (
    //     {
    //         title:'Hapus Data',
    //          msg: "Akan menghapus data?" ,
    //         buttons: Ext.MessageBox.YESNO,
    //         width:300,
    //         fn: function (btn)
    //         {
    //             if (btn =='yes')
    //             {
    //                 Ext.Ajax.request
    //                 (
    //                 {
    //                     url: "./Datapool.mvc/DeleteDataObj",
    //                     params: dataparamDelete_viDaftar(), //dataparam_viDaftar(),
    //                     success: function(o)
    //                     {
    //                         var cst = Ext.decode(o.responseText);
    //                         if (cst.success === true)
    //                         {
    //                             ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');
    //                             datarefresh_viDaftar();
    //                             dataaddnew_viDaftar();
				// 				Ext.getCmp('btnSimpan_viDaftar').disable();
				// 				Ext.getCmp('btnSimpanExit_viDaftar').disable();	
				// 				Ext.getCmp('btnDelete_viDaftar').disable();	
								
    //                         }
    //                         else if (cst.success === false && cst.pesan===0)
    //                         {
    //                             ShowPesanWarning_viDaftar('Data tidak berhasil dihapus ' ,'Hapus Data');
    //                         }
    //                         else if (cst.success === false && cst.pesan===1)
    //                         {
    //                             ShowPesanError_viDaftar('Data tidak berhasil dihapus '  + cst.msg ,'Hapus Data');
    //                         }
    //                     }
    //                 }
    //                 )//end Ajax.request
    //             } // end if btn yes
    //         }// end fn
    //     }
    //     )//end msg.show
    // }
}
//-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viDaftar(modul,mBolHapus)
{
 //    var x = 1;
	
	// if (Ext.get('txtNamaPs_viDaftar').getValue() === '' || Ext.get('txtNamaPs_viDaftar').getValue() === undefined)
	// {
	// 	ShowPesanWarning_viDaftar("Nama belum terisi",modul);
	// 	x=0;
	// }
	// if (Ext.get('txtNoTlp_viDaftar').getValue() === '' || Ext.get('txtNoTlp_viDaftar').getValue() ===  undefined)
	// {
	// 	ShowPesanWarning_viDaftar("No Telp 1 belum terisi",modul);
	// 	x=0;
	// }
	// if (Ext.getCmp('ComboJK_viDaftar').getValue() === '' || Ext.getCmp('ComboJK_viDaftar').getValue() ===  undefined)	
	// {
	// 	ShowPesanWarning_viDaftar("Jenis kelamin belum terisi",modul);
	// 	x=0;
	// }   

	// if (Ext.getCmp('ComboGolDRH_viDaftar').getValue() === '' || Ext.getCmp('ComboGolDRH_viDaftar').getValue() ===  undefined)	
	// {
	// 	ShowPesanWarning_viDaftar("Golongan darah belum terisi",modul);
	// 	x=0;
	// }   
	// if (Ext.getCmp('ComboPoliDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboPoliDaftar_viDaftar').getValue() ===  undefined)
	// {
	// 	ShowPesanWarning_viDaftar("Poli belum terisi",modul);
	// 	x=0;
	// }
	// if (Ext.getCmp('ComboDokterDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboDokterDaftar_viDaftar').getValue() ===  undefined)
	// {
	// 	ShowPesanWarning_viDaftar("Dokter belum terisi",modul);
	// 	x=0;
	// }	
	// if (Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue() ===  undefined)	
	// {
	// 	ShowPesanWarning_viDaftar("Kelompok pasien belum terisi",modul);
	// 	x=0;
	// }   
	// if (Ext.getCmp('DtpTglLahir_viDaftar').getValue() > now_viDaftar )	
	// {
	// 	ShowPesanWarning_viDaftar("Tanggal lahir salah",modul);
	// 	x=0;
	// }   
 //    return x;
}

function ShowPesanWarning_viDaftar(str,modul)
{
  //   Ext.MessageBox.show
  //   (
		// {
		// 	title: modul,
		// 	msg:str,
		// 	buttons: Ext.MessageBox.OK,
		// 	icon: Ext.MessageBox.WARNING,
		// 	width :250
		// }
  //   )
}

function ShowPesanError_viDaftar(str,modul)
{
  //   Ext.MessageBox.show
  //   (
		// {
		// 	title: modul,
		// 	msg:str,
		// 	buttons: Ext.MessageBox.OK,
		// 	icon: Ext.MessageBox.ERROR,
		// 	width :250
		// }
  //   )
}

function ShowPesanInfo_viDaftar(str,modul)
{
  //   Ext.MessageBox.show
  //   (
		// {
		// 	title: modul,
		// 	msg:str,
		// 	buttons: Ext.MessageBox.OK,
		// 	icon: Ext.MessageBox.INFO,
		// 	width :250
		// }
  //   )
}

function datarefresh_viDaftar()
{
	// var criteria;
	// criteria = getCriteriaFilter_viDaftar()
 //    dataSource_viDaftar.load
 //    (
	// 	{
	// 		params:
	// 		{
	// 			Skip: 0,
	// 			Take: selectCount_viDaftar,
	// 			Sort: '',
	// 			Sortdir: 'ASC',
	// 			target:'ViViewKunjungan',
	// 			param: criteria
	// 		}
	// 	}
 //    );
 //    return dataSource_viDaftar;
}

function getCriteriaFilter_viDaftar()
{
	// var strKriteria = "";
	
	// strKriteria = " where K.KD_CUSTOMER ='" + strKD_CUST + "' ";
	// strKriteria += " and (K.TGL_KUNJUNGAN between '" + ShowDate(Ext.getCmp('txtfilterTglKunjAwal_viDaftar').getValue()) + "' ";
	// strKriteria += " and '" + ShowDate(Ext.getCmp('txtfilterTglKunjAkhir_viDaftar').getValue()) + "') ";
	
	
	// if(Ext.getCmp('ComboPoli_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and U.NAMA_UNIT LIKE '%" + Ext.get('ComboPoli_viDaftar').dom.value + "%' ";
	// }
	
	// if(Ext.getCmp('txtfilterNoRM_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and RIGHT(K.KD_PASIEN,10) LIKE '%" +  Ext.getCmp('txtfilterNoRM_viDaftar').getValue() + "%' ";
	// }

	// if(Ext.getCmp('txtfilterNama_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and PS.NAMA LIKE '%" +  Ext.getCmp('txtfilterNama_viDaftar').getValue() + "%' ";
	// }
	
	// return strKriteria;
}

function datainit_viDaftar(rowdata)
{
 //    addNew_viDaftar = false;
	
	// Ext.getCmp('txtKDPasien_viDaftar').setValue(rowdata.KD_PASIEN);
	// Ext.getCmp('txtNoKunjungan_viDaftar').setValue(rowdata.NO_KUNJUNGAN);		
	// Ext.getCmp('txtNamaPs_viDaftar').setValue(rowdata.NAMA);
	// Ext.get('txtchkPasienBaru_viDaftar').dom.checked = rowdata.PASIEN_BARU;
	// Ext.getCmp('txtTmpLahir_viDaftar').setValue(rowdata.TEMPAT_LAHIR);
	
	// Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(rowdata.TGL_LAHIR));
	
	// Ext.getCmp('txtUmurThn_viDaftar').setValue(rowdata.TAHUN);
	// Ext.getCmp('txtUmurBln_viDaftar').setValue(rowdata.BULAN);
	// Ext.getCmp('txtUmurHari_viDaftar').setValue(rowdata.HARI);
	
	// Ext.getCmp('ComboJK_viDaftar').setValue(rowdata.JENIS_KELAMIN);
	// Ext.getCmp('ComboGolDRH_viDaftar').setValue(rowdata.GOL_DARAH);
	// Ext.getCmp('txtNoTlp_viDaftar').setValue(rowdata.NO_TELP);
	// Ext.getCmp('txtHP_viDaftar').setValue(rowdata.NO_HP);
	// Ext.getCmp('txtAlamat_viDaftar').setValue(rowdata.ALAMAT);
	// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(rowdata.TGL_KUNJUNGAN));

	// Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowdata.KD_UNIT);
	// Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(rowdata.KD_DOKTER);
	// Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(rowdata.KD_KELOMPOK);	

	// Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(rowdata.KD_STS_MARITAL);
	// Ext.getCmp('ComboPekerjaan_viDaftar').setValue(rowdata.KD_PEKERJAAN);
	// Ext.getCmp('ComboAgama_viDaftar').setValue(rowdata.KD_AGAMA);
	// Ext.getCmp('ComboPendidikan_viDaftar').setValue(rowdata.KD_PENDIDIKAN);
	
	// Ext.getCmp('txtNadi_viDaftar').setValue(rowdata.NADI);
	// Ext.getCmp('txtchkAlergi_viDaftar').setValue(rowdata.ALERGI);
	// Ext.getCmp('txtTinggiBadan_viDaftar').setValue(rowdata.TINGGI_BADAN);
	// Ext.getCmp('txtBeratBadan_viDaftar').setValue(rowdata.BERAT_BADAN);
	// Ext.getCmp('txtTekananDarah_viDaftar').setValue(rowdata.TEKANAN_DRH);
	// Ext.getCmp('txtkeluhan_viDaftar').setValue(rowdata.KELUHAN);

	// Ext.get('ComboJK_viDaftar').dom.value = rowdata.JNS_KELAMIN;	
	// Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowdata.NAMA_UNIT;
	// Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowdata.DOKTER;
	// Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowdata.KELOMPOK;
	// Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowdata.STS_MARITAL;
	// Ext.get('ComboPekerjaan_viDaftar').dom.value = rowdata.PEKERJAAN;
	// Ext.get('ComboAgama_viDaftar').dom.value = rowdata.AGAMA;
	// Ext.get('ComboPendidikan_viDaftar').dom.value = rowdata.PENDIDIKAN;
	// Ext.getCmp('btnSimpan_viDaftar').disable();
	// Ext.getCmp('btnSimpanExit_viDaftar').disable();	
	// Ext.getCmp('btnDelete_viDaftar').enable();	
	// mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	
}


function dataaddnew_viDaftar()
{
 //    addNew_viDaftar = true;	
	// Ext.getCmp('txtKDPasien_viDaftar').setValue('');
	// Ext.getCmp('txtNoKunjungan_viDaftar').setValue('');	
	// Ext.getCmp('txtNamaPs_viDaftar').setValue('');
	// Ext.getCmp('txtTmpLahir_viDaftar').setValue('');
	// Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(now_viDaftar));
	// Ext.getCmp('txtUmurThn_viDaftar').setValue('');
	// Ext.getCmp('txtUmurBln_viDaftar').setValue('');
	// Ext.getCmp('txtUmurHari_viDaftar').setValue('');
	// Ext.getCmp('ComboJK_viDaftar').setValue(1);
	// Ext.getCmp('ComboGolDRH_viDaftar').setValue(0);
	// Ext.getCmp('txtNoTlp_viDaftar').setValue('');
	// Ext.getCmp('txtHP_viDaftar').setValue('');
	// Ext.getCmp('txtAlamat_viDaftar').setValue('');

	// Ext.getCmp('ComboPoliDaftar_viDaftar').setValue('');
	// Ext.getCmp('ComboDokterDaftar_viDaftar').setValue('');
	// Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue('');
	// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(now_viDaftar));

	// Ext.getCmp('txtNadi_viDaftar').setValue('');
	// Ext.getCmp('txtchkAlergi_viDaftar').setValue('');
	// Ext.getCmp('txtTinggiBadan_viDaftar').setValue('');
	// Ext.getCmp('txtBeratBadan_viDaftar').setValue('');
	// Ext.getCmp('txtTekananDarah_viDaftar').setValue('');
	// Ext.getCmp('txtkeluhan_viDaftar').setValue('');

	// Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue('');
	// Ext.getCmp('ComboPekerjaan_viDaftar').setValue('');
	// Ext.getCmp('ComboAgama_viDaftar').setValue('');
	// Ext.getCmp('ComboPendidikan_viDaftar').setValue('');
	// Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 1;
	// Ext.get('txtNamaPs_viDaftar').dom.focus();
	
	// Ext.getCmp('btnSimpan_viDaftar').enable();
	// Ext.getCmp('btnSimpanExit_viDaftar').enable();	
	// Ext.getCmp('btnDelete_viDaftar').disable();	
	// mNoKunjungan_viKasir = '';
	
 //    rowSelected_viDaftar   = undefined;
}
///---------------------------------------------------------------------------------------///

function dataparam_viDaftar()
{
		// var params_ViPendaftaran =
		// {
		// 	Table: 'viKunjungan',			
		// 	KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue(),
		// 	KD_UNIT: Ext.getCmp('ComboPoliDaftar_viDaftar').getValue(),
		// 	KD_DOKTER: Ext.getCmp('ComboDokterDaftar_viDaftar').getValue(),
		// 	KD_CUSTOMER: strKD_CUST,
		// 	KD_PASIEN: Ext.get('txtKDPasien_viDaftar').getValue(),
		// 	TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftar').getValue()),
		// 	TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftar').getValue(),
		// 	BERAT_BADAN : Ext.get('txtBeratBadan_viDaftar').getValue(),
		// 	TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftar').getValue(),
		// 	NADI : Ext.get('txtNadi_viDaftar').getValue(),
		// 	ALERGI :  Ext.get('txtchkAlergi_viDaftar').dom.checked,
		// 	KELUHAN : Ext.get('txtkeluhan_viDaftar').getValue(),  
		// 	PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftar').dom.checked,						
		// 	KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftar').getValue(), //PASIEN
		// 	KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftar').getValue(),
		// 	KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftar').getValue(),
		// 	KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftar').getValue(),
		// 	NAMA : Ext.get('txtNamaPs_viDaftar').getValue(),
		// 	TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftar').getValue(),
		// 	TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftar').getValue()),
		// 	JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftar').getValue(),
		// 	ALAMAT : Ext.get('txtAlamat_viDaftar').getValue(),
		// 	NO_TELP : Ext.get('txtNoTlp_viDaftar').getValue(),
		// 	NO_HP : Ext.get('txtHP_viDaftar').getValue(),
		// 	GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftar').getValue(),
		
		// }
	
  //   return params_ViPendaftaran
}

function dataparamDelete_viDaftar()
{
		// var paramsDelete_ViPendaftaran =
		// {
		// 	Table: 'viKunjungan',			
		// 	// KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue(),
		// 	// KD_UNIT: Ext.getCmp('ComboPoliDaftar_viDaftar').getValue(),
		// 	// KD_DOKTER: Ext.getCmp('ComboDokterDaftar_viDaftar').getValue(),
		// 	NO_KUNJUNGAN : Ext.get('txtNoKunjungan_viDaftar').getValue(), //Ext.getCmp('txtNoKunjungan_viDaftar').setValue(rowdata.NO_KUNJUNGAN);		
		// 	KD_CUSTOMER: strKD_CUST,
		// 	// KD_PASIEN: Ext.get('txtKDPasien_viDaftar').getValue(),
		// 	// TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftar').getValue()),
		// 	// TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftar').getValue(),
		// 	// BERAT_BADAN : Ext.get('txtBeratBadan_viDaftar').getValue(),
		// 	// TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftar').getValue(),
		// 	// NADI : Ext.get('txtNadi_viDaftar').getValue(),
		// 	// ALERGI :  Ext.get('txtchkAlergi_viDaftar').dom.checked,
		// 	// KELUHAN : Ext.get('txtkeluhan_viDaftar').getValue(),  
		// 	// PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftar').dom.checked,						
		// 	// KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftar').getValue(), //PASIEN
		// 	// KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftar').getValue(),
		// 	// KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftar').getValue(),
		// 	// KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftar').getValue(),
		// 	// NAMA : Ext.get('txtNamaPs_viDaftar').getValue(),
		// 	// TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftar').getValue(),
		// 	// TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftar').getValue()),
		// 	// JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftar').getValue(),
		// 	// ALAMAT : Ext.get('txtAlamat_viDaftar').getValue(),
		// 	// NO_TELP : Ext.get('txtNoTlp_viDaftar').getValue(),
		// 	// NO_HP : Ext.get('txtHP_viDaftar').getValue(),
		// 	// GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftar').getValue(),
		
		// }
	
  //   return paramsDelete_ViPendaftaran
}
//============================================ Grid Data ======================================

//-------------------------------------------- Hapus baris -------------------------------------
function HapusBarisNgajar(nBaris)
{
	// if (CurrentData_viDaftar.row >= 0
	// /*SELECTDATASTUDILANJUT.data.NO_SURAT_STLNJ != '' ||  SELECTDATASTUDILANJUT.data.TGL_MULAI_SURAT != '' ||
	// 	SELECTDATASTUDILANJUT.data.TGL_AKHIR_SURAT != '' || SELECTDATASTUDILANJUT.data.ID_DANA != '' ||
	// 	SELECTDATASTUDILANJUT.data.SURAT_DARI != '' || SELECTDATASTUDILANJUT.data.TGL_SURAT != '' || SELECTDATASTUDILANJUT.data.THN_PERKIRAAN_LULUS != ''*/) 
	// 	{
	// 		Ext.Msg.show
	// 		(
	// 			{
	// 				title: 'Hapus Baris',
	// 				msg: 'Apakah baris ini akan dihapus ?',
	// 				buttons: Ext.MessageBox.YESNO,
	// 				fn: function(btn) 
	// 				{
	// 					if (btn == 'yes') 
	// 					{
	// 						DataDeletebaris_viDaftar()
	// 						dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
	// 						SELECTDATASTUDILANJUT = undefined;
	// 					}
	// 				},
	// 				icon: Ext.MessageBox.QUESTION
	// 			}
	// 		);
	// 	}
	// else 
	// 	{
	// 		dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
	// 		SELECTDATASTUDILANJUT = undefined;
	// 	}
}

function DataDeletebaris_viDaftar() 
{
 //    Ext.Ajax.request
	// ({ url: "./Datapool.mvc/DeleteDataObj",
	// 	params: dataparam_viDaftar(),
	// 	success: function(o) 
	// 	{
	// 		var cst = o.responseText;
	// 		if (cst == '{"success":true}') 
	// 		{
	// 			ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');                
	// 		}
	// 		else
	// 		{ 
	// 			ShowPesanInfo_viDaftar('Data gagal dihapus','Hapus Data'); 
	// 		}
	// 	}
	// })       
};

var mRecord = Ext.data.Record.create
(
	[
		'NIP', 
		'ID_STUDI_LANJUT', 
		'NO_SURAT_STLNJ', 
		'TGL_MULAI_SURAT', 
		'TGL_AKHIR_SURAT', 
		'ID_DANA', 
		'SURAT_DARI', 
		'TGL_SURAT', 
		'THN_PERKIRAAN_LULUS'  
	]
);
//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------
function getArrDetail_viDaftar() 
{
 //    var x = '';
 //    for (var i = 0; i < dsDetailSL_viDaftar.getCount(); i++) 
	// {
 //        var y = '';
 //        var z = '@@##$$@@';
        
 //        y += 'NIP=' + Ext.get('txtNPP_viDaftar').getValue()
 //        y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftar').getValue()
 //        y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftar.data.items[i].data.NO_SURAT_STLNJ
		
	// 	/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT.length == 8) 
	// 	{	
			
	// 		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
	// 	}
	// 	else
	// 	{
		
	// 		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
	// 	}
		
	// 	if (dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT.length == 8) 
	// 	{		
	// 		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
	// 	}
	// 	else
	// 	{
	// 		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
	// 	}*/
	// 	y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
	// 	y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
 //        y += z + 'ID_DANA=' + dsDetailSL_viDaftar.data.items[i].data.ID_DANA
 //        y += z + 'SURAT_DARI=' + dsDetailSL_viDaftar.data.items[i].data.SURAT_DARI
 //        alert(dsDetailSL_viDaftar.data.items[i].data.ID_DANA)
		
	// 	/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT.length == 8) 
	// 	{		
	// 		y += z + 'TGL_SURAT=' + dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT
	// 	}
	// 	else
	// 	{
	// 		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
	// 	}*/
		
	// 	y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		
	// 	y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftar.data.items[i].data.THN_PERKIRAAN_LULUS
		
 //        if (i === (dsDetailSL_viDaftar.getCount() - 1)) 
	// 	{
 //            x += y
 //        }
 //        else {
 //            x += y + '##[[]]##'
 //        }
 //    }
 //    return x;
};
//---------------------------- end Split row ------------------------------
function DatarefreshDetailSL_viDaftar(rowdataaparam)
{
  //   dsDetailSL_viDaftar.load
  //   (
		// {
		// 	params:
		// 	{
		// 		Skip: 0,
		// 		Take: 50,
		// 		Sort: '',
		// 		Sortdir: 'ASC',
		// 		target:'viviewDetailStudiLanjut',
		// 		param: rowdataaparam
		// 	}
		// }
  //   );
  //   return dsDetailSL_viDaftar;
}


function getItemPanelInputBiodata_viDaftar(lebar) 
{
//     var items =
// 	{
// 	    layout: 'Form',
// 	    anchor: '100%',
// 	    width: lebar- 80,
// 	    labelAlign: 'top',
// 	    bodyStyle: 'padding:10px 10px 10px 0px',
// 		//labelAlign: 'top',
// 		//border:true,
// 		items:
// 		[			
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'No.Medrec',
// 				anchor: '100%',
// 				labelSeparator: '',
// 				width: 199,
// 				items: 
// 				[
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',
// 						width: 100,
// 						name: 'txtKDPasien_viDaftar',
// 						id: 'txtKDPasien_viDaftar',
// 						style:{'text-align':'right'},	
// 						listeners:
// 						{
// 							'specialkey': function() 
// 							{
// 								if (Ext.EventObject.getKey() === 13) 
// 								{
// 									GetPasienDaftar(1)									
// 									// var strKriteria='';
// 									// strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
// 									// FormLookupPasien(strKriteria,nFormPendaftaran,Ext.get('txtKDPasien_viDaftar').getValue());
									
// 								};
// 							}
// 						}
// 					},
// 					{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width: 35,
// 						value: 'Nama',
// 						fieldLabel: 'Label',
// 						id: 'lblnamaPS_viDaftar',
// 						name: 'lblnamaPS_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						hidden: true,
// 						width : 80,	
// 						name: 'txtNoKunjungan_viDaftar',
// 						id: 'txtNoKunjungan_viDaftar'
// 					},
				
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						width : 200,	
// 						name: 'txtNamaPs_viDaftar',
// 						id: 'txtNamaPs_viDaftar',	
// 						listeners:
// 						{
// 							'specialkey': function() 
// 							{
// 								if (Ext.EventObject.getKey() === 13) 
// 								{
// 									// GetPasienDaftar(1)									
// 									// var strKriteria;
// 									// strKriteria=" WHERE PS.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
// 									// FormLookupPasien(strKriteria,nFormPendaftaran);
									
// 								};
// 							}
// 						}
// 					},
// 						{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width:80,
// 						value: 'Nama Keluarga',
// 						fieldLabel: 'Label',
// 						id: 'lblnamaKeluargaPS_viDaftar',
// 						name: 'lblnamaKeluargaPS_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						//hidden: true,
// 						width : 180,	
// 						name: 'txtnamaKeluargaPS_viDaftar',
// 						id: 'txtnamaKeluargaPS_viDaftar'
// 					},{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width:70,
// 						value: 'Jns.Kelamin',
// 						fieldLabel: 'Label',
// 						id: 'lblJnsKelaminPS_viDaftar',
// 						name: 'lblJnsKelaminPS_viDaftar'
// 					},
// 					viComboJK(112,"ComboJK_viDaftar"),
// 				]
// 			},					
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'Tempat Lahir',
// 				name: 'compTmpLahir_viDaftar',
// 				id: 'compTmpLahir_viDaftar',
// 				items: 
// 				[
// 					{
// 						xtype: 'textfield',					
// 						fieldLabel: '',								
// 						anchor: '100%',
// 						name: 'txtTmpLahir_viDaftar',
// 						id: 'txtTmpLahir_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',				
// 						width: 54,								
// 						value: 'Tgl Lahir',
// 						fieldLabel: 'Label',
// 						id: 'lblTglLahir_viDaftar',
// 						name: 'lblTglLahir_viDaftar'
// 					},
// 					{
// 						xtype: 'datefield',					
// 						fieldLabel: '',
// 						name: 'DtpTglLahir_viDaftar',
// 						id: 'DtpTglLahir_viDaftar',
// 						format: 'd/M/Y',
// 						width: 120
// 					},
// 					{
// 						xtype: 'displayfield',
// 						width: 60,							
// 						value: 'Umur Thn',
// 						fieldLabel: 'Label',
// 						id: 'lblumrthn_viDaftar',
// 						name: 'lblumrthn_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',							
// 						fieldLabel: '',
// 						width: 55,
// 						readOnly:true,
// 						style:{'text-align':'right'},
// 						name: 'txtUmurThn_viDaftar',
// 						id: 'txtUmurThn_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',					
// 						width: 20,				
// 						value: 'Bln',
// 						fieldLabel: 'Label',
// 						id: 'lblumrbln_viDaftar',
// 						name: 'lblumrbln_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						fieldLabel: '',
// 						width: 55,
// 						readOnly:true,
// 						style:{'text-align':'right'},
// 						name: 'txtUmurBln_viDaftar',
// 						id: 'txtUmurBln_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',						
// 						width: 25,
// 						value: 'Hari',
// 						fieldLabel: 'Label',
// 						id: 'lblhari_viDaftar',
// 						name: 'lblhari_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',						
// 						fieldLabel: '',
// 						width: 50,
// 						readOnly:true,
// 						style:{'text-align':'right'},
// 						name: 'txtUmurHari_viDaftar',
// 						id: 'txtUmurHari_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',						
// 						width: 45,
// 						value: 'Agama',
// 						fieldLabel: 'Label',
// 						id: 'lblAgama_viDaftar',
// 						name: 'lblAgama_viDaftar'
// 					},Vicbo_Agama(120, "ComboAgama_viDaftar")
// 				]
// 			},	
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'Gol.Darah',
// 				name: 'GD_viDaftar',
// 				id: 'GD_viDaftar',
// 				// height: 40,
// 				items: 
// 				[
					 
					 
// 					viComboGolDRH(50,"ComboGolDRH_viDaftar"),
// 					{
// 						xtype: 'displayfield',								
// 						width: 80,
// 						value: 'Status Marital',
// 						fieldLabel: 'Label',
// 						id: 'lblStatusMarital_viDaftar',
// 						name: 'lblStatusMarital_viDaftar'
// 					},
// 					Vicbo_STS_MARITAL(120, "ComboSTS_MARITAL_viDaftar"),
									
// 					{
// 						xtype: 'checkbox',
// 						flex: 1,
// 						fieldLabel: '',
// 						width: 20,
// 						//disabled: true,
// 						name: 'txtchkWNI_viDaftar',
// 						id: 'txtchkWNI_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',
// 						value: 'WNI',
// 						flex: 1,
// 						style:{'text-align':'left'},
// 						fieldLabel: 'WNI',
// 						width: 73,						
// 						name: 'lblWNI_viDaftar',
// 						id: 'lblWNI_viDaftar'
// 					},
// {
// 						xtype: 'displayfield',								
// 						width: 50,
// 						value: 'Alamat',
// 						fieldLabel: 'Label',
// 						id: 'lblAlamat_viDaftar',
// 						name: 'lblAlamat_viDaftar'
// 					},					
// 					{
// 						xtype: 'textfield',
// 						name: 'txtAlamat_viDaftar',
// 						id: 'txtAlamat_viDaftar',
// 						autoScroll: true,				
// 						fieldLabel: 'Alamat ',						
// 						width: 200
// 					},	
// 					{
// 						xtype: 'displayfield',								
// 						width: 20,
// 						value: 'Telp.',
// 						fieldLabel: 'Label',
// 						id: 'lblnotlp_viDaftar',
// 						name: 'lblnotlp_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',								
// 						fieldLabel: '',						
// 						// anchor:'50%',
// 						width: 75,
// 						style:{'text-align':'right'},
// 						name: 'txtNoTlp_viDaftar',
// 						id: 'txtNoTlp_viDaftar',
// 						listeners:
// 						{
// 							'specialkey': function() 
// 							{
// 								if (Ext.EventObject.getKey() === 13) 
// 								{
// 									GetPasienDaftar(2)									
// 									// var strKriteria;
// 									// strKriteria=" WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
// 									// FormLookupPasien(strKriteria,nFormPendaftaran);
// 								};
// 							}
// 						}
// 					}
// 				]
// 			},			
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'Propinsi',
// 				name: 'Propinsi_viDaftar',
// 				id: 'Propinsi_viDaftar',
// 				// height: 40,
// 				items: 
// 				[
					 
					 
// 					mComboPropinsiPendaftaranRWJ(),
// 					{
// 						xtype: 'displayfield',								
// 						width: 80,
// 						value: 'Kabupaten',
// 						fieldLabel: 'Label',
// 						id: 'lblKabupaten_viDaftar',
// 						name: 'lblnKabupaten_viDaftar'
// 					},
// 					mComboKabupatenPendaftaranRWJ(),
// 					{
// 						xtype: 'displayfield',								
// 						width: 80,
// 						value: 'Kecamatan',
// 						fieldLabel: 'Label',
// 						id: 'lblKecamatan_viDaftar',
// 						name: 'lblnKecamatan_viDaftar'
// 					},
// 					mComboKECAMATANPendaftaranRWJ()
					
// 				]
// 			},
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'Pendidikan',
// 				name: 'Pendidikan_viDaftar',
// 				id: 'Pendidikan_viDaftar',
// 				// height: 40,
// 				items: 
// 				[			 
					 
// 					Vicbo_Pendidikan(100, "ComboPendidikan_viDaftar"),
// 					{
// 						xtype: 'displayfield',								
// 						width: 60,
// 						value: 'Pekerjaan',
// 						fieldLabel: 'Label',
// 						id: 'lblPekerjaan_viDaftar',
// 						name: 'lblnPekerjaan_viDaftar'
// 					},
// 					Vicbo_Pekerjaan(135, "ComboPekerjaan_viDaftar")	,
// 					{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width:55,
// 						value: 'Suami/Istri',
// 						fieldLabel: 'Label',
// 						id: 'lblSuamiIstriPS_viDaftar',
// 						name: 'lblSuamiIstriPS_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						//hidden: true,
// 						width : 100,	
// 						name: 'txtSuamiIstriPS_viDaftar',
// 						id: 'txtSuamiIstriPS_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width:30,
// 						value: 'Ayah',
// 						fieldLabel: 'Label',
// 						id: 'lblAyahPS_viDaftar',
// 						name: 'lblAyahPS_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						//hidden: true,
// 						width : 100,	
// 						name: 'txtAyahPS_viDaftar',
// 						id: 'txtAyahPS_viDaftar'
// 					},
// 					{
// 						xtype: 'displayfield',
// 						flex: 1,
// 						width:30,
// 						value: 'Ibu',
// 						fieldLabel: 'Label',
// 						id: 'lblIbuPS_viDaftar',
// 						name: 'lblIbuPS_viDaftar'
// 					},
// 					{
// 						xtype: 'textfield',
// 						flex: 1,
// 						fieldLabel: 'Label',								
// 						// anchor:'100%',
// 						//hidden: true,
// 						width : 100,	
// 						name: 'txtIbuPS_viDaftar',
// 						id: 'txtIbuPS_viDaftar'
// 					}
					
// 				]
// 			},
// 			{
// 				xtype: 'compositefield',
// 				fieldLabel: 'Pend.Ayah/Suami/Istri',
// 				name: 'PendidikanASI_viDaftar',
// 				id: 'PendidikanASI_viDaftar',
// 				// height: 40,
// 				items: 
// 				[			 
					 
// 					Vicbo_Pendidikan(150, "ComboPendidikanASI_viDaftar"),
// 					{
// 						xtype: 'displayfield',								
// 						width: 130,
// 						value: 'Pek.Ayah/Suami/Istri',
// 						fieldLabel: 'Label',
// 						id: 'lblPekerjaanASI_viDaftar',
// 						name: 'lblnPekerjaanASI_viDaftar'
// 					},
// 					Vicbo_Pekerjaan(185, "ComboPekerjaan_viDaftarASI")	
					
// 				]
// 			}

		
							
// 		]
// 	};
//     return items;
};

function getItemDataKunjungan_viDaftar(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'top',
	    bodyStyle: 'padding:10px 10px 10px 0px',		
		width: lebar-80,
		height:140,
	    items:
		[							
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl Kunjungan ',	
				labelAlign: 'top',
				labelSeparator: '',
				labelWidth:70,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					
					{
						xtype: 'datefield',								
						id: 'dtpKunjungan_viDaftar',
						format: 'd/M/Y',
						value: now_viDaftar,
						autoSchrol:false,
						width:280,
						onInit: function() { }				
					}	,
					{
					   xtype: 'displayfield',
					   value: 'Kelompok ',
					   style:{'margin-top':'2px', 'text-align':'right'},
					   width:100
					},
					Vicbo_Kelompok(250, "ComboKelompokDaftar_viDaftar")
					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Poli ',		
				labelAlign: 'top',
				labelSeparator: '',
				labelWidth:105,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					
					viCombo_POLI(250, "ComboPoliDaftar_viDaftar"),
					{
						xtype: 'displayfield',
						flex: 1,
						width:80,
						value: 'No.Register',
						fieldLabel: 'Label',
						id: 'lblNoRegPS_viDaftar',
						name: 'lblNoRegPS_viDaftar'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',								
						// anchor:'100%',
						//hidden: true,
						width : 100,	
						name: 'txtNoRegPS_viDaftar',
						id: 'txtNoRegPS_viDaftar'
					}
					,Vicbo_Kelompok(250, "ComboKelompokDaftar_viDaftar")
					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter ',	
				labelAlign: 'top',
				labelSeparator: '',
				labelWidth:70,
				defaults: 
				{
					flex: 1
				},
				items: 
				[				
					
					Vicbo_Dokter(262, "ComboDokterDaftar_viDaftar")
				]
			}
		]
	};
    return items;
};

function GetPasienDaftar(strCari)
{
	// Ext.Ajax.request
	//  (
	// 	{
 //            url: "./Module.mvc/ExecProc",
 //            params: 
	// 		{
 //                UserID: 'Admin',
 //                ModuleID: 'getKunjunganDataPasien',
	// 			Params:	getParamKunjunganKdPasien(strCari)
 //            },
 //            success: function(o) 
	// 		{
				
 //                var cst = Ext.decode(o.responseText);

	// 			if (cst.success === true) 
	// 			{					
	// 				Ext.getCmp('txtKDPasien_viDaftar').setValue(cst.KD_PASIEN);
	// 				Ext.getCmp('txtNamaPs_viDaftar').setValue(cst.NAMA);
	// 				Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 0;					
	// 				Ext.getCmp('txtTmpLahir_viDaftar').setValue(cst.TEMPAT_LAHIR);
	// 				Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(cst.TGL_LAHIR));					
	// 				Ext.getCmp('txtUmurThn_viDaftar').setValue(cst.TAHUN);
	// 				Ext.getCmp('txtUmurBln_viDaftar').setValue(cst.BULAN);
	// 				Ext.getCmp('txtUmurHari_viDaftar').setValue(cst.HARI);
	// 				Ext.getCmp('ComboJK_viDaftar').setValue(cst.JENIS_KELAMIN);
	// 				Ext.getCmp('ComboGolDRH_viDaftar').setValue(cst.GOL_DARAH);
	// 				Ext.getCmp('txtNoTlp_viDaftar').setValue(cst.NO_TELP);
	// 				Ext.getCmp('txtHP_viDaftar').setValue(cst.NO_HP);	
	// 				Ext.getCmp('txtAlamat_viDaftar').setValue(cst.ALAMAT);					
	// 				Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(cst.KD_UNIT);
	// 				Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(cst.KD_DOKTER);
	// 				Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(cst.KD_KELOMPOK);									
	// 				// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(cst.TGL_KUNJUNGAN));
	// 				Ext.getCmp('txtNadi_viDaftar').setValue(cst.NADI);
	// 				Ext.getCmp('txtchkAlergi_viDaftar').setValue(cst.ALERGI);
	// 				Ext.getCmp('txtTinggiBadan_viDaftar').setValue(cst.TINGGI_BADAN);
	// 				Ext.getCmp('txtBeratBadan_viDaftar').setValue(cst.BERAT_BADAN);
	// 				Ext.getCmp('txtTekananDarah_viDaftar').setValue(cst.TEKANAN_DARAH);
	// 				Ext.getCmp('txtkeluhan_viDaftar').setValue(cst.KELUHAN);
	// 				Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(cst.KD_STS_MARITAL);
	// 				Ext.getCmp('ComboPekerjaan_viDaftar').setValue(cst.KD_PEKERJAAN);
	// 				Ext.getCmp('ComboAgama_viDaftar').setValue(cst.KD_AGAMA);
	// 				Ext.getCmp('ComboPendidikan_viDaftar').setValue(cst.KD_PENDIDIKAN);
					
	// 				// Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowdata.KD_UNIT);
	// 				// Ext.get('ComboJK_viDaftar').dom.value = rowdata.JNS_KELAMIN;	
	// 				// Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowdata.NAMA_UNIT;
	// 				// Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowdata.DOKTER;
	// 				// Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowdata.KELOMPOK;
	// 				// Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowdata.STS_MARITAL;
	// 				// Ext.get('ComboPekerjaan_viDaftar').dom.value = rowdata.PEKERJAAN;
	// 				// Ext.get('ComboAgama_viDaftar').dom.value = rowdata.AGAMA;
	// 				// Ext.get('ComboPendidikan_viDaftar').dom.value = rowdata.PENDIDIKAN;
	// 			}
	// 			else
	// 			{
	// 				ShowPesanWarning_viDaftar('No.Medrec tidak di temukan '  + cst.pesan,'Informasi');
	// 				// Ext.get('txtNamaKRSMahasiswa').dom.value = '';								
	// 				// Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
	// 				// Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
	// 				// Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
	// 				// Ext.get('txtBarcode').dom.focus();

	// 				// var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
	// 					// if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
	// 					// {
	// 						// criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
	// 					// };
	// 					// FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNamaKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
	// 			};
 //            }

 //        }
	// );
}

function getParamKunjunganKdPasien(kdCari)
{					
	// var strKriteria = "";	
	// var x ="x";
		
	
	// if (kdCari == 1)
	// {	
	// 	if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
	// 	{
	// 		strKriteria += Ext.get('txtKDPasien_viDaftar').getValue() + "##";							
	// 	}	
	// }
	// else if(kdCari == 2) 
	// {
	// 	if(Ext.get('txtNoTlp_viDaftar').getValue() != '')
	// 	{
	// 		strKriteria += Ext.get('txtNoTlp_viDaftar').getValue() + "##";							
	// 	}	
	// }else
	// {
	// 	if(Ext.get('txtHP_viDaftar').getValue() != '')
	// 	{
	// 		strKriteria += Ext.get('txtHP_viDaftar').getValue() + "##";							
	// 	}	
	// };
	// strKriteria += strKD_CUST + "##"
	// strKriteria += kdCari + "##"
	
	// return strKriteria;
}

function getKriteriaCariLookup()
{					
	// var strKriteria = "";	
		
	// //strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
	
	// if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
	// {
	// 	strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%'"
	// }	
	
	// if(Ext.get('txtNamaPs_viDaftar').getValue() != '')	
	// {	
	// 	if (strKriteria != "")	
	// 	{
	// 		strKriteria += " AND x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
	// 	}
	// 	else
	// 	{
	// 		strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
	// 	}
	// }
	
	// // if(Ext.get('txtNoTlp_viDaftar').getValue() != '')	
	// // {	
	// 	// if (strKriteria != "")	
	// 	// {
	// 		// strKriteria += " OR PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
	// 	// }
	// 	// else
	// 	// {
	// 		// strKriteria += " WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
	// 	// }
	// // }
	
	// // if(Ext.get('txtHP_viDaftar').getValue() != '')	
	// // {	
	// 	// if (strKriteria != "")	
	// 	// {
	// 		// strKriteria += " OR PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
	// 	// }
	// 	// else
	// 	// {
	// 		// strKriteria += " WHERE PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
	// 	// }
	// // }
	
	// if(Ext.get('txtAlamat_viDaftar').getValue() != '')	
	// {	
	// 	if (strKriteria != "")	
	// 	{
	// 		strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
	// 	}
	// 	else
	// 	{
	// 		strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
	// 	}
	// }

	// if (strKriteria != "")	
	// {
	// 	strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	// }
	// else
	// {
	// 	strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	// }		
	
	// return strKriteria;
}

function GetPrintKartu(strCari)
{
	// Ext.Ajax.request
	//  (
	// 	{
 //            url: "./Module.mvc/ExecProc",
 //            params: 
	// 		{
 //                UserID: 'Admin',
 //                ModuleID: 'getCetakKartu',
	// 			Params:	strCari
 //            },
 //            success: function(o) 
	// 		{
				
 //                var cst = Ext.decode(o.responseText);

	// 			if (cst.success === true) 
	// 			{
	// 			}
	// 			else
	// 			{				

	// 			};
 //            }

 //        }
	// );
}


function mComboPropinsiPendaftaranRWJ()
{
 //    var Field_propinsi_viKasirRwj = ['KD_PROPINSI','PROPINSI'];
 //    ds_PROPINSI_viKasirRwj = new WebApp.DataStore({fields: Field_propinsi_viKasirRwj});
    
	// ds_PROPINSI_viKasirRwj.load
 //    (
 //        {
 //            params:
 //            {
 //                Skip: 0,
 //                Take: 1000,
 //                Sort: 'KD_PROPINSI',
 //                Sortdir: 'ASC',
 //                target:'viPROPINSI',
 //                param: ''
 //            }            
 //        }
 //    )
	
 //    var cbo_PROPINSI_viDaftarRwj = new Ext.form.ComboBox
 //    (
 //        {
 //            flex: 1,
 //            fieldLabel: 'PROPINSI',
	// 		valueField: 'KD_PROPINSI',
 //            displayField: 'PROPINSI',
	// 		store: ds_PROPINSI_viKasirRwj,
 //            width: 150,
 //            mode: 'local',
 //            typeAhead: true,
 //            triggerAction: 'all',                        
 //            name: 'PROPINSIdaftarRWJ',
 //            lazyRender: true,
 //            id: 'PROPINSIdaftarRWJ',
 //            listeners:
	// 		{
	// 			'select': function (a,b,c)
	// 			{
	// 			}
	// 		}
 //        }
 //    )
    
 //    return cbo_PROPINSI_viDaftarRwj;
}

function mComboKabupatenPendaftaranRWJ()
{
 //    var Field_Kabupaten_viKasirRwj = ['KD_KABUPATEN','KABUPATEN'];
 //    ds_KABUPATEN_viKasirRwj = new WebApp.DataStore({fields: Field_Kabupaten_viKasirRwj});
    
	// ds_PROPINSI_viKasirRwj.load
 //    (
 //        {
 //            params:
 //            {
 //                Skip: 0,
 //                Take: 1000,
 //                Sort: 'KD_KABUPATEN',
 //                Sortdir: 'ASC',
 //                target:'viKABUPATEN',
 //                param: ''
 //            }            
 //        }
 //    )
	
 //    var cbo_KABUPATEN_viDaftarRwj = new Ext.form.ComboBox
 //    (
 //        {
 //            flex: 1,
 //            fieldLabel: 'KABUPATEN',
	// 		valueField: 'KD_KABUPATEN',
 //            displayField: 'KABUPATEN',
	// 		store: ds_KABUPATEN_viKasirRwj,
 //            width: 200,
 //            mode: 'local',
 //            typeAhead: true,
 //            triggerAction: 'all',                        
 //            name: 'KABUPATENdaftarRWJ',
 //            lazyRender: true,
 //            id: 'KABUPATENdaftarRWJ',
 //            listeners:
	// 		{
	// 			'select': function (a,b,c)
	// 			{
	// 			}
	// 		}
 //        }
 //    )
    
 //    return cbo_KABUPATEN_viDaftarRwj;
}

function mComboKECAMATANPendaftaranRWJ()
{
 //    var Field_KECAMATAN_viKasirRwj = ['KD_KECAMATAN','KECAMATAN'];
 //    ds_KECAMATAN_viKasirRwj = new WebApp.DataStore({fields: Field_KECAMATAN_viKasirRwj});
    
	// ds_KECAMATAN_viKasirRwj.load
 //    (
 //        {
 //            params:
 //            {
 //                Skip: 0,
 //                Take: 1000,
 //                Sort: 'KD_KECAMATAN',
 //                Sortdir: 'ASC',
 //                target:'viKECAMATAN',
 //                param: ''
 //            }            
 //        }
 //    )
	
 //    var cbo_KECAMATAN_viDaftarRwj = new Ext.form.ComboBox
 //    (
 //        {
 //            flex: 1,
 //            fieldLabel: 'KECAMATAN',
	// 		valueField: 'KD_KECAMATAN',
 //            displayField: 'KECAMATAN',
	// 		store: ds_KECAMATAN_viKasirRwj,
 //            width: 200,
 //            mode: 'local',
 //            typeAhead: true,
 //            triggerAction: 'all',                        
 //            name: 'KECAMATANdaftarRWJ',
 //            lazyRender: true,
 //            id: 'KECAMATANdaftarRWJ',
 //            listeners:
	// 		{
	// 			'select': function (a,b,c)
	// 			{
	// 			}
	// 		}
 //        }
 //    )
    
 //    return cbo_KECAMATAN_viDaftarRwj;
}*/*/*/*/