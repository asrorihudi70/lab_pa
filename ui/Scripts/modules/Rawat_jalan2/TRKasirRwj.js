// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirRwj.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------
var tampungjumlah=0;
var cellSelectedRequestDet;
var rowSelectedTreeSetEquipmentKlas_produk;
var selectCountCatEqp=50;
var selectCountKlas_produkEqp=50;
var IdKlas_produkegorySetEquipmentKlas_produkView='0';
var dataSource_viKasirRwj;
var selectCount_viKasirRwj=50;
var NamaForm_viKasirRwj="Kasir RWJ ";
var mod_name_viKasirRwj="viKasirRwj";
var now_viKasirRwj= new Date();
var addNew_viKasirRwj;
var rowSelected_viKasirRwj;
var setLookUps_viKasirRwj;
var mNoKunjungan_viKasirRwj='';
var dsunit_viKasirRwj;
var StrTreeSetEquipment;
var valueUnit_viKasirRwj='';
var selectCountStatusByr_viKasirRwj;
var CurrentData_viKasirRwj =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viKasirRwj(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKasirRwj(mod_id_viKasirRwj)
{	
    // Field kiriman dari Project Net.
	var FieldMaster_viKasirRwj =['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER'];
     dataSource_viKasirRwj = new WebApp.DataStore({ fields: FieldMaster_viKasirRwj });
     DataRefresh_viKasirRwj();
    
	
	
    // Grid Kasir Rawat Jalan # --------------
 var GridDataView_viKasirRwj = new Ext.grid.EditorGridPanel
     (
        {
            id: 'GridDataView_viKasirRwj',
            stripeRows: true,
            store: dataSource_viKasirRwj,
            autoScroll: true,
            columnLines: true,
            border: false,
            anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                        {
                            rowselect: function(sm, row, rec)
                            {
                                rowSelected_viKasirRwj=undefined;
                                rowSelected_viKasirRwj = dataSource_viKasirRwj.getAt(row);
							
                            }
                        }
                }
            ),
            listeners:
		{
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viKasirRwj = dataSource_viKasirRwj.getAt(ridx);
                                if (rowSelected_viKasirRwj != undefined)
                                 {
                                    setLookUp_viKasirRwj(rowSelected_viKasirRwj.data);
                                 }
                                else
                                 {
                                    setLookUp_viKasirRwj();
                                 };
			}
		},
		//'',,'','',',''
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
					{
                        id: 'col_viKasirRwj',
                        header: 'No Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: true,
                        width: 100
                    },
                    {
                        id: 'col_viKasirRwj',
                        header: 'No Recmed',
                        dataIndex: 'KD_PASIEN',
                        sortable: true,
                        width: 100
                    },
                    {
			id: 'colTrKasirRwj',
			header: 'Nama',
			dataIndex: 'NAMA',
			width: 100,
			sortable: true
                    },
	             {
			id: 'colTrKasirRwj',
			header: 'Alamat',
			dataIndex: 'ALAMAT',
			width: 100,
			sortable: true
                },
	             {
			id: 'colTrKasirRwj',
			header: 'Tanggal',
			dataIndex: 'TANGGAL_TRANSAKSI',
			width: 100,
			sortable: true
                },
				{
			id: 'colTrKasirRwj',
			header: 'Dokter Pemeriksa',
			dataIndex: 'NAMA_DOKTER',
			width: 100,
			sortable: true
				},
				{
			id: 'colTrKasirRwj',
			header: 'Unit Tujuan',
			dataIndex: 'NAMA_UNIT',
			width: 100,
			sortable: true
                },
				{
			id: 'colTrKasirRwj',
			header: 'Kode Kelompok ',
			dataIndex: 'KD_CUSTOMER',
			//hidden : true,
			width: 100,
			sortable: true
                },
				{
			id: 'colTrKasirRwj',
			header: 'Kelompok Pasien',
			dataIndex: 'CUSTOMER',
			//hidden : true,
			width: 100,
			sortable: true
                },
				{
				id: 'colTrKasirRwj',
			header: 'Kode Unit',
			dataIndex: 'KD_UNIT',
			//hidden : true,
			width: 100,
			sortable: true
                }
			]
            ),			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viKasirRwj',
				items: 
				[
					{
						xtype: 'button',
						text: 'Lihat Transaksi',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viKasirRwj',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viKasirRwj != undefined)
							{
								setLookUp_viKasirRwj(rowSelected_viKasirRwj.data)
							}
							else
							{								
								setLookUp_viKasirRwj();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viKasirRwj, selectCount_viKasirRwj, dataSource_viKasirRwj),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viKasirRwj = new Ext.Panel
    (
		{
			title: NamaForm_viKasirRwj,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viKasirRwj,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viKasirRwj],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viKasirRwj,
		            columns: 9,
		            defaults: {
		            scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Status Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						mComboStatusBayar_viKasirRwj(),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viKasirRwj',
							//emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										//DfltFilterBtn_viKasirRwj = 1;
										DataRefresh_viKasirRwjFilter();
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viKasirRwj',
							value: now_viKasirRwj,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DataRefresh_viKasirRwjFilter();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(187, "ComboKelompok_viKasirRwj"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirRwj',
							//emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DataRefresh_viKasirRwjFilter();
						
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viKasirRwj',
							value: now_viKasirRwj,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
											DataRefresh_viKasirRwjFilter();
								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan 	: ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						mComboUnit_viKasirRwj(),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Dokter : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_DOKTER_viKasirRwj',
							//emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DataRefresh_viKasirRwjFilter();
						
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viKasirRwj',
							handler: function() 
							{					
							
								DataRefresh_viKasirRwjFilter();
							
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viKasirRwj;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viKasirRwj # --------------

function HapusBarisRequest()
{
    if ( cellSelectedRequestDet != undefined )
    {
        if (cellSelectedRequestDet.data.ASSET_MAINT != '' && cellSelectedRequestDet.data.ASSET_MAINT_ID != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentTRRequest.row + 1) + ' ' + nmOperatorDengan + ' ' + nmAsetIDRequest + ' : ' + cellSelectedRequestDet.data.ASSET_MAINT_ID + ' ' + nmOperatorAnd + ' ' + nmAsetNameRequest + ' : ' + cellSelectedRequestDet.data.ASSET_MAINT_NAME ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsDTLTRRequestList.data.items[CurrentTRRequest.row].data.ROW_REQ === '')
                            {
                                dsDTLTRRequestList.removeAt(CurrentTRRequest.row);
                            }
                            else
                            {
                                Ext.Msg.show
                                (
                                    {
                                       title:nmHapusBaris,
                                       msg: nmGetValidasiHapusBarisDatabase(),
                                       buttons: Ext.MessageBox.YESNO,
                                       fn: function (btn)
                                       {
                                            if (btn =='yes')
                                            {
                                                RequestDeleteDetail();
                                            };
                                       }
                                    }
                                )
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsDTLTRRequestList.removeAt(CurrentTRRequest.row);
        };
    }
};
function DataRefresh_viKasirRwj()
{ //var isiparam='co_status= false and kd_bagian =2 kunjungan.kd_unit ';
	dataSource_viKasirRwj.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCount_viKasirRwj,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target:'TrKasirRWJ',
					param : ''
				}
			}
		);

	rowSelected_viKasirRwj = undefined;
	return dataSource_viKasirRwj;
};


function DataRefresh_viKasirRwjFilter()
{
	var KataKunci='';


	
    if (Ext.get('TxtFilterGridDataView_NoMedrec_viKasirRwj').getValue() != '')
    {
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  kode_pasien like ~%' + Ext.get('TxtFilterGridDataView_NoMedrec_viKasirRwj').getValue() + '%~';
		}
		else
		{
		
                        KataKunci += ' and kode_pasien like  ~%' + Ext.get('TxtFilterGridDataView_NoMedrec_viKasirRwj').getValue() + '%~';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Bayar')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  co_status = true';
		}
		else
		{
		
                        KataKunci += ' and co_status =  true';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Bayar')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  co_status = false';
		}
		else
		{
	
                        KataKunci += ' and co_status =  false';
		};
		
		
	};
	
		
	if (Ext.get('txtfilterTglKunjAwal_viKasirRwj').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('txtfilterTglKunjAwal_viKasirRwj').getValue() + "~ and ~" + Ext.get('txtfilterTglKunjAkhir_viKasirRwj').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('txtfilterTglKunjAwal_viKasirRwj').getValue() + "~ and ~" + Ext.get('txtfilterTglKunjAkhir_viKasirRwj').getValue() + "~)";
		};
	
	};
	

    if (KataKunci != undefined )
    {
		dataSource_viKasirRwj.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCount_viKasirRwj,
                                        Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target:'TrKasirRWJ',
					param : KataKunci
				}
			}
		);
    }
	else
	{
		DataRefresh_viKasirRwj();
	};
};


/**
*	Function : setLookUp_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viKasirRwj(rowdata)
{
    var lebar = 760;
    setLookUps_viKasirRwj = new Ext.Window
    (
    {
        id: 'SetLookUps_viKasirRwj',
		name: 'SetLookUps_viKasirRwj',
        title: NamaForm_viKasirRwj, 
        closeAction: 'destroy',        
        width: 775,
        height: 610,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viKasirRwj(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
			deactivate: function()
				{
					
				}
        }
    }
    );

    setLookUps_viKasirRwj.show();
    if (rowdata == undefined)
    {
         dataaddnew_viKasirRwj();
		// Ext.getCmp('btnDelete_viKasirRwj').disable();	
    }
    else
    {
      datainit_viKasirRwj(rowdata);
    }
}


function datainit_viKasirRwj(rowdata)
{
		
   // addNew_viKasirRwj = false;
   
   //,
   
    Ext.get('txtNoMedrec_viKasirRwj').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtNama_viKasirRwj').dom.value = rowdata.NAMA;
    Ext.get('txtNotransfield_viKasirRwj').dom.value = rowdata.NO_TRANSAKSI;
	 Ext.get('txtUnit_viKasirRwj').dom.value = rowdata.KD_UNIT;
    Ext.get('txtNmUnit_viKasirRwj').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtKelompok_viKasirRwj').dom.value = rowdata.CUSTOMER;
	   Ext.get('txtDokter_viKasirRwj').dom.value = rowdata.KD_DOKTER;
	Ext.get('txtNmDokter_viKasirRwj').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('DtpTglLahirfield_viKasirRwj').dom.value = rowdata.TANGGAL_TRANSAKSI;
	
};
// End Function setLookUpGridDataView_viKasirRwj # --------------

/**
*	Function : getFormItemEntry_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viKasirRwj(lebar,rowdata)
{
    var pnlFormDataBasic_viKasirRwj = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viKasirRwj(lebar),
				//-------------- ## -------------- 
				getItemDataKunjungan_viKasirRwj(lebar), 
				//-------------- ## --------------
				getItemGridTransaksi_viKasirRwj(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					labelSeparator: '',
					//width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah1EditData_viKasirRwj',
                            name: 'txtJumlah1EditData_viKasirRwj',
							style:{'text-align':'right','margin-left':'420px'},
                            width: 100,
                            value: 55.799,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viKasirRwj',
                            name: 'txtJumlah2EditData_viKasirRwj',
							style:{'text-align':'right','margin-left':'416px'},
                            width: 100,
                            value: 55.799,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},
				//-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viKasirRwj',
						handler: function(){
						
							//dataaddnew_viKasirRwj();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viKasirRwj',
						handler: function()
						{
							TambahBarisRequest();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viKasirRwj',
						handler: function()
						{
							var x = datasave_viKasirRwj(addNew_viKasirRwj);
							datarefresh_viKasirRwj();
							if (x===undefined)
							{
								setLookUps_viKasirRwj.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viKasirRwj',
						handler: function()
						{
						//HapusBarisRequest();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Produk/Tindakan',
						        id:'btnLookUpProduk_viKasirRwj',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupProduk_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Lookup Konsultasi',
						        id:'btnLookUpKonsultasi_viKasirRwj',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupKonsultasi_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirRwj',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Pembayaran ',
		                iconCls:'print',
		                menu: 
		                [
		                	{
						        text: 'Pembayaran',
						        id:'btnPembayaran_viKasirRwj',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPembayaran_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Transfer ke Rawat Inap',
						        id:'btnTransferPembayaran_viKasirRwj',
						        iconCls: 'print',
						        handler: function(){
						            FormSetTransferPembayaran_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Edit Tarif',
						        id:'btnEditTarif_viKasirRwj',
						        iconCls: 'print',
						        handler: function(){
						            FormSetEditTarif_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Posting Manual',
						        id:'btnPostingManual_viKasirRwj',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPostingManual_viKasirRwj('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viKasirRwj;
}
// End Function getFormItemEntry_viKasirRwj # --------------








function getParamSetDokter()
{
/*

  

  urut integer NOT NULL,
   

  kd_customer character varying(10),
*/
    var params =
	{
        Table: 'viewtrrwj',
		KdKasirRwj: '01', 			//kd_kasir
	    KdTransakasiRwj: Ext.get('txtNotransfield_viKasirRwj').getValue(),//no_transaksi
	    TglTransaksiRwj: Ext.get('DtpTglLahirfield_viKasirRwj').getValue(),//tgl_transaksi
		KDUserRwj :'0',				//kd_user
		KDTarifRwj : 'TU',			//kd_tarif
		KDProduk :'112', 			//kd_produk
		KDUnit :'202', 				//kd_unit
		TGLbrlakuRwj :'2015-01-01',	//tgl_berlaku
		ChangeRwj    :true,		//charge
		AdjustRwj: false,		//adjust
		FolioRwj: 'A',			//folio
		QtyRwj: '1',			//qty
		HargaRwj:'50000',		//harga
		ShiftRWj : '1',			//shift
		KDokterRwj : '',		//kd_dokter
		Kd_Unit_trRwj : '',		//kd_unit_tr
		CitoRwj : '1',			//cito
		JsRwj : '0',		//js
		JpRwj : '0',		//jp
		No_FakturRwj : '',		//no_faktur
		FlagRwj : '0',		//flag
		TagRwj : false,		//tag
		Kd_customerRwj : '',		//no_faktur
		
	};
    return params
};



/**
*	Function : getItemPanelInputBiodata_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viKasirRwj(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viKasirRwj',
						id: 'txtNoMedrec_viKasirRwj',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 465,	
						name: 'txtNama_viKasirRwj',
						id: 'txtNama_viKasirRwj',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Register',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoregfield_viKasirRwj',
						id: 'txtNoregfield_viKasirRwj',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 246,	
						name: 'txtNotransfield_viKasirRwj',
						id: 'txtNotransfield_viKasirRwj',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
						
						{
						xtype: 'datefield',
						flex: 1,
						fieldLabel: 'Tanggal',														
						width : 110,	
						name: 'DtpTglLahirfield_viKasirRwj',
						id: 'DtpTglLahirfield_viKasirRwj',
						emptyText: 'Tanggal',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Unit',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 74,
						name: 'txtUnit_viKasirRwj',
						id: 'txtUnit_viKasirRwj',
						emptyText: 'Kode Unit',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 357,
						name: 'txtNmUnit_viKasirRwj',
						id: 'txtNmUnit_viKasirRwj',
						emptyText: 'Nama Unit',
					},
					//-------------- ## --------------
					{
					
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 110,
						name: 'txtKelompok_viKasirRwj',
						id: 'txtKelompok_viKasirRwj',
						emptyText: 'Kelompok',
					}
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 74,
						name: 'txtDokter_viKasirRwj',
						id: 'txtDokter_viKasirRwj',
						emptyText: 'Kode Dokter',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 533,
						name: 'txtNmDokter_viKasirRwj',
						id: 'txtNmDokter_viKasirRwj',
						emptyText: 'Nama Dokter',
					}
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viKasirRwj # --------------

/**
*	Function : getItemDataKunjungan_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viKasirRwj(lebar) 
{
		
		var rdJenisKunjungan_viKasirRwj = new Ext.form.RadioGroup
	({  
        fieldLabel: '',  
        columns: 2, 
        width: 400,
		border:false,
        items: 
		[  
			{
				boxLabel: 'Datang Senidri', 
				width:70,
				id: 'rdDatangSendiri_viKasirRwj', 
				name: 'rdDatangSendiri_viKasirRwj', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rujukan', 
				width:70,
				id: 'rdDatangRujukan_viKasirRwj', 
				name: 'rdDatangRujukan_viKasirRwj', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	//-----------
	
	//-------------- ## --------------

    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:110,
	    items:
		[	
				rdJenisKunjungan_viKasirRwj,
			{
				xtype: 'compositefield',
				fieldLabel: 'Dari',	
				labelAlign: 'Left',
				//labelSeparator: '',
				labelWidth:70,

				items: 
				[					
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: '',
						name: 'txtdari_viKasirRwj',
						id: 'txtdari_viKasirRwj',								
						width: 100,
						emptyText: 'Dari',
					},
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Nama:',
					   //style:{'margin-top':'2px', 'text-align':'right'},
					   width:40
					},
					Vicbo_Dokter(463, "ComboDariNama_viKasirRwj"),					
					//-------------- ## --------------
									
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter',		
				labelAlign: 'Left',
				labelWidth:105,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					Vicbo_Kelompok(403, "ComboKelompokDaftar_viKasirRwj"),
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Tgl Kunjungan:',
					   //style:{'margin-top':'2px', 'text-align':'right'},
					   width:90
					},
					{
						xtype: 'datefield',								
						id: 'dtpKunjungan_viKasirRwj',
						format: 'd/M/Y',
						value: now_viKasirRwj,
						autoSchrol:false,
						width:110,
						onInit: function() { }				
					},
					//-------------- ## --------------
												
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
function getRadioVendorApproveRequest()
{
	var items=
	{
		xtype: 'radiogroup',
		fieldLabel: nmRepairApproveCM + ' ',
		itemCls: 'x-check-group-alt',
		anchor:'99%',
		columns: 2,
		items: 
		[        
			{
				boxLabel: nmRdoIntApproveCM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '0', 
				id:'rdoInternalApproveReq',
				style: 
				{
					'margin-top': '3.5px'
				},
				handler: function() 
				{
					
				}
           },
			 {
				boxLabel: nmRdoExtApproveCM, 
				name: 'rdoVendorApproveReq', 
				inputValue: '1', 
				id:'rdoExternalApproveReq',
				style: 
				{
				    'margin-top': '3.5px'
				},
				handler: function() 
				{
				
				}
			}
		]
	};
	
	return items;
};

// End Function getItemDataKunjungan_viKasirRwj # --------------

/**
*	Function : getItemGridTransaksi_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viKasirRwj(lebar) 
{
    var items =
	{
		title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:265, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viKasirIgd()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viKasirRwj # --------------

/**
*	Function : gridDataViewEdit_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/
/*
: 'Operasi Kecil Gigi', : '2015-01-22', : '1', : '', : '55.799', : 1, : '55.799', : '0', : ''
*/

function showCols (grid) 
 {             grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Uraian'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Tanggal'), false);
				 grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Dok'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('CdTR'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Uraian'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Harga'), false);
				 grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('Qty'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DR'), false);
					 grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('CR'), false);
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('TAG'), false);
				//grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('menu'), false);
 };
function TambahBarisRequest()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRequest();
        dsDataGrdJab_viKasirRwj.insert(dsDataGrdJab_viKasirRwj.getCount(), p);
    };
};
function gridDataViewEdit_viKasirIgd()
{  
    var FieldGrdKasir_viKasirIgd = 
	[
	];
	
	

if (tampungjumlah == 0)	
{
	var datastoreGrid = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['code',  'NoFaktur',  'Uraian',  'Tanggal',  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
		}),
		

       
  	
	data: 
		[
			
				{code: '1', NoFaktur: '00000002', Uraian: 'Operasi Kecil Gigi', Tanggal: '2015-01-22', Dok: '1', CdTR: '', Harga: '55.799', Qty: 1, DR: '55.799', CR: '0', TAG: ''},
					{code: '', NoFaktur: '-', Uraian: 'BPJS', Tanggal: '2014-10-01', Dok: '1', CdTR: '', Harga: '', Qty: '', DR: '', CR: '55.799', TAG: ''}
			// {field1: 'C', field2: 'D'},
			// {field1: 'E', field2: 'E'}
			
		]
	});
	}
	else 	
{
	var datastoreGrid = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['code',  'NoFaktur',  'Uraian',  'Tanggal',  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
		}),
		

       
  	
	data: 
		[
    
			
				
			// {field1: 'E', field2: 'E'}
			
		]
	});
	};
    
   var grListSetMenu = new Ext.grid.EditorGridPanel
   (
    {
        xtype: 'editorgrid',
        id: 'grListSetMenu',
		// store: dsDataGrdJab_viKasirIgd,
		store: datastoreGrid,
        height: 220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:	
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			
			//-------------- ## --------------
			{
				dataIndex: 'NoFaktur',
				header: 'No. Faktur',
				sortable: true,
				//hidden :true,
				width: 70
			},
			//-------------- ## --------------			
			{			
				dataIndex: 'Uraian',
				header: 'Uraian',
				sortable: true,
				//hidden :true,
				width: 180
			},
			//-------------- ## --------------
			{
				dataIndex: 'Tanggal',
				header: 'Tanggal',
				sortable: true,
				//hidden :true,
				width: 70
				//renderer: function(v, params, record) 
				// {
					
				// }			
			},
			//-------------- ## --------------
			{
				dataIndex: 'Dok',
				header: 'Dok',
				align: 'right',
				sortable: true,
				//hidden :true,
				width: 30
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'CdTR',
				header: 'Cd. TR',
				sortable: true,
				//hidden :true,
				align: 'right',
				width: 0
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'Harga',
				header: 'Harga/Unit',
				align: 'right',
				sortable: true,
				//hidden :true,
				width: 100
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'Qty',
				header: 'Qty',
				sortable: true,
				align: 'right',
				width: 30,
				//hidden :true,
			    editor: new Ext.form.TextField
                (
				)
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'DR',
				header: 'DR',
				align: 'right',
				sortable: true,
				//hidden :true,
				width: 75
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'CR',
				header: 'CR',
				align: 'right',
				sortable: true,
				//hidden :true,
				width: 75
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'TAG',
				header: 'Tag',
				align: 'right',
				sortable: true,
				//hidden :true,
				width: 30
				// renderer: function(v, params, record) 
				// {
					
				// }	
			}
			//-------------- ## --------------
        ]
    }   
)	


    return grListSetMenu;
}
function RecordBaruRequest()
{
	var p = new mRecordRequest
	(
		{
			'ASSET_MAINT':'',
			'ASSET_MAINT_ID':'',
		    'ASSET_MAINT_NAME':'', 
		    'LOCATION_ID':'', 
		    'LOCATION':'',
		    'PROBLEM':'',
		    'REQ_FINISH_DATE':'', 
		    'DESC_REQ':'',
		    'IMPACT':'',
		    'ROW_REQ':''
		}
	);
	
	return p;
};

// End Function gridDataViewEdit_viKasirRwj # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viKasirRwj',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viKasirRwj',
						handler: function()
						{
						showCols(Ext.getCmp('grListSetMenu'));
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viKasirRwj',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viKasirRwj.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viKasirRwj; 
}
// End Function FormSetLookupProduk_viKasirRwj # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
	var lebar = 570;
	var itemsListProdukGridDataView_viKasirRwj = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viKasirRwj',
        id: 'PanelListProdukGridDataView_viKasirRwj',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1200,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_viKasirRwj(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_viKasirRwj(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_viKasirRwj;
}
// End Function getFormItemEntryProdukGridDataView_viKasirRwj # --------------

/**
*   Function : itemsTreeListProdukGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_viKasirRwj()
{	
		
	var treeKlas_produk_viKasirRwj= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:340,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriaSetEquipment_viKasirRwj=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriaSetEquipment_viKasirRwj.id != ' 0')
					{
						if (strTreeCriteriaSetEquipment_viKasirRwj.leaf == false)
						{
							var str='';
							//str=' and left(parent,' + n.attributes.id.length + ')=' + n.attributes.id
                                                        str='substring(parent,1, ' + n.attributes.id.length + ') = ~' + n.attributes.id + '~';
							RefreshDataSetEquipmentCat(str);
						}
						else
						{
							RefreshDataSetEquipmentCat('parent = ~' + strTreeCriteriaSetEquipment_viKasirRwj.id + '~');
						};
					}
					else
					{
						RefreshDataSetEquipmentCat('');
					};
				}
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:'Master Produk',
			id:IdKlas_produkegorySetEquipmentKlas_produkView,
			children: StrTreeSetEquipment,
			autoScroll: true
		}
	)  
  
  treeKlas_produk_viKasirRwj.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
	

    var pnlTreeFormDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_viKasirRwj',
		                	id: 'ckboxTreeSemuaUnit_viKasirRwj',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_viKasirRwj',
		                	id: 'ckboxTreePerUnit_viKasirRwj',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                treeKlas_produk_viKasirRwj,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viKasirRwj;
}
// End Function itemsTreeListProdukGridDataView_viKasirRwj # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function GetStrTreeSetEquipment()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetDataTreeSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeSetEquipment= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};
function getItemTreeGridTransaksiGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_viKasirRwj()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_viKasirRwj # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function RefreshDataSetEquipmentCat(str)
{
		
 if (str == undefined || str == 'parent = ~0~')
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berlaku= (~2014-03-01~)' ;
  }else
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berlaku= (~2014-03-01~) and '+ str;
  };
	dsDataTreeGrdJabGridDataView_viKasirRwj.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountCatEqp, 
				Sort: 'kd_klas', 
				Sortdir: 'ASC', 
				target:'ViewSetupCategory',
				//param: 'where type = ~D~ ' + str
                                param:  str
			} 
		}
	);
	rowSelectedSetEquipmentCat = undefined;
	return dsDataTreeGrdJabGridDataView_viKasirRwj;
};


function gridTreeDataViewEditGridDataView_viKasirRwj()
{
 var chkAktifSetEmployeeView = new Ext.grid.CheckColumn
	(
		{
			id: 'chkAktifSetEmployeeView',
			header: 'Pilih',
			align: 'center',
			disabled:true,
			dataIndex: '',
			width: 50,
			editor: new Ext.form.Checkbox
						(
							{
								id:'fieldcolProblemRequest',
								allowBlank: true,
								enableKeyEvents : true,
								align: 'center'
								//width:30
							}
						)
			
		}
	);

    var FieldTreeGrdKasirGridDataView_viKasirRwj = 
    [
	'KD_PRODUK','DESKRIPSI','KD_UNIT','NAMA_UNIT','MANUAL','KP_PRODUK',
	'KD_KAT','KD_KLAS','KLASIFIKASI','PARENT','KD_TARIF','TGL_BERLAKU','TARIF','TGL_BERAKHIR'
    ];
    
    dsDataTreeGrdJabGridDataView_viKasirRwj= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_viKasirRwj
    });
	RefreshDataSetEquipmentCat();
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_viKasirRwj,
        height: 395,
        selModel: new Ext.grid.CellSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
							rowSelectedSetEquipmentCat=undefined;
							rowSelectedSetEquipmentCat = dsDataTreeGrdJabGridDataView_viKasirRwj.getAt(row);
                    }
                }
            }
        ),
        columns: 
		
        [chkAktifSetEmployeeView,			{
                        id: 'colKodeSetUnit',
                        header: 'Nama Unit',                      
                        dataIndex: 'NAMA_UNIT',
                        sortable: true,
						hidden:true,
                        width: 250
                    },
					{
                        id: 'colKodeSetEquipmentCat',
                        header: 'Produk',                      
                        dataIndex: 'DESKRIPSI',
						
                        sortable: true,
                        width: 150
                    },
					
					{
					    id: 'colSetEquipmentCat',
					    header: 'Harga',					   
					    dataIndex: 'TARIF',
					    width: 150,
					    sortable: true,
						
					},					
		
					{
					    id: 'colSetType',
					    header: 'PARENT',					   
					    dataIndex: 'PARENT',
						hidden:true,
					    width: 80,
					    sortable: true
					}
				
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_viKasirRwj # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viKasirRwj',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viKasirRwj.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viKasirRwj; 
};

// End Function FormSetLookupKonsultasi_viKasirRwj # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viKasirRwj(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viKasirRwj;
}
// End Function getFormItemEntryLookupKonsultasi_viKasirRwj # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function getItemPanelInputKonsultasiDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboAsalUnit_viKasirRwj'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboKonsultasike_viKasirRwj'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokter_viKasirRwj'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viKasirRwj # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viKasirRwj',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viKasirRwj.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viKasirRwj; 
};

// End Function FormSetLookupGantiDokter_viKasirRwj # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viKasirRwj(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viKasirRwj;
}
// End Function getFormItemEntryLookupGantiDokter_viKasirRwj # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viKasirRwj',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viKasirRwj',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viKasirRwj',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterAsal_viKasirRwj'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterGanti_viKasirRwj'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viKasirRwj # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viKasirRwj',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viKasirRwj.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viKasirRwj; 
};

// End Function FormSetPembayaran_viKasirRwj # --------------

/**
*   Function : getFormItemEntryGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viKasirRwj(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viKasirRwj(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viKasirRwj',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viKasirRwj',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirRwj',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirRwj',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirRwj',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirRwj',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirRwj',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirRwj',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirRwj',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirRwj',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viKasirRwj;
}
// End Function getFormItemEntryGridDataView_viKasirRwj # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viKasirRwj',
				name: 'txtNoTransaksiPembayaranGridDataView_viKasirRwj',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viKasirRwj',
				name: 'txtNamaPasienPembayaranGridDataView_viKasirRwj',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranGridDataView_viKasirRwj'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranDuaGridDataView_viKasirRwj'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viKasirRwj # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viKasirRwj()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viKasirRwj # --------------

/**
*   Function : gridDataViewEditGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viKasirRwj()
{
    
    var FieldGrdKasirGridDataView_viKasirRwj = 
    [
	
    ];
    
    dsDataGrdJabGridDataView_viKasirRwj= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirRwj
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirRwj,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viKasirRwj # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viKasirRwj',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viKasirRwj.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viKasirRwj; 
};

// End Function FormSetTransferPembayaran_viKasirRwj # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirRwj',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viKasirRwj',
									name: 'DateTransaksiTransferPembayaran_viKasirRwj',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirRwj',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viKasirRwj',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirRwj',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirRwj',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtPAIDTransaksiTransferPembayaran_viKasirRwj',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirRwj',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirRwj',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirRwj',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
					        xtype: 'panel',
					        title: '',
					        border:false,
					        items: 
					        [           
					            viComboJenisTransaksi_MediSmart(320,'CmboAlasanTransfer_viKasirRwj'),
					            //-------------- ## -------------- 
					        ]
					    }
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viKasirRwj',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viKasirRwj',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viKasirRwj;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viKasirRwj # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viKasirRwj',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viKasirRwj.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viKasirRwj; 
};

// End Function FormSetEditTarif_viKasirRwj # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viKasirRwj(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viKasirRwj(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viKasirRwj',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viKasirRwj',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirRwj',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirRwj',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirRwj',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirRwj',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viKasirRwj;
}
// End Function getFormItemEntryEditTarifGridDataView_viKasirRwj # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viKasirRwj',
                name: 'txtProdukEditTarifGridDataView_viKasirRwj',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viKasirRwj # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viKasirRwj()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viKasirRwj # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viKasirRwj()
{
    
    var FieldGrdKasirGridDataView_viKasirRwj = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirRwj= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirRwj
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirRwj,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viKasirRwj # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viKasirRwj(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viKasirRwj = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viKasirRwj',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viKasirRwj.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viKasirRwj; 
};

// End Function FormSetPostingManual_viKasirRwj # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viKasirRwj(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viKasirRwj(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viKasirRwj(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viKasirRwj',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viKasirRwj',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viKasirRwj',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viKasirRwj',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viKasirRwj',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viKasirRwj',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viKasirRwj;
}
// End Function getFormItemEntryPostingManualGridDataView_viKasirRwj # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viKasirRwj',
                name: 'txtProdukPostingManualGridDataView_viKasirRwj',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viKasirRwj # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viKasirRwj(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viKasirRwj()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viKasirRwj # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viKasirRwj
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viKasirRwj()
{
    
    var FieldGrdKasirGridDataView_viKasirRwj = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirRwj= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirRwj
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirRwj,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}



function mComboUnit_viKasirRwj() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwj.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ID',
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirRwj',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmStatusRequest + ' ',
		    align: 'Right',
		    anchor:'40%',
		    store: dsunit_viKasirRwj,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			//value:'all',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{
		
			        //selectStatusCMRequestView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirRwj;
};

function mComboStatusBayar_viKasirRwj()
{
  var cboStatus_viKasirRwj = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirRwj',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'JENIS',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Bayar'], [2, 'Belum Bayar']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirRwj,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirRwj=b.data.displayText ;
					//RefreshDataSetDokter();
				}
			}
		}
	);
	return cboStatus_viKasirRwj;
};


// End Function gridDataViewEditGridPostingManualDataView_viKasirRwj # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir Rawat Jalan # --------------