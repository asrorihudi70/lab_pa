var dsrad;
var cellselectedrad;
var CurrentRad ={
    data: Object,
    details: Array,
    row: 0
};
var PenataJasaRJ={};
var CurrentKasirRWJ ={
    data: Object,
    details: Array,
    row: 0
};
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create([
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
]);

var CurrentDiagnosa ={
    data: Object,
    details: Array,
    row: 0
};

var combo;
var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create([
   {name: 'KASUS', mapping:'KASUS'},
   {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
   {name: 'PENYAKIT', mapping:'PENYAKIT'},
   {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
]);

var dsTRDetailDiagnosaList;
var dsTRDetailAnamneseList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var dsTRKasirRWJList;
var dsTRDetailKasirRWJList;
var dsPjTrans2;
var dsRwJPJLab;
var dsCmbRwJPJDiag;
var AddNewKasirRWJ = true;
var selectCountKasirRWJ = 50;

var rowSelectedKasirRWJ;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var anamnese;
var CurrentAnamnesese;
var vkode_customer;
CurrentPage.page = getPanelRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
// Asep
PenataJasaRJ.dsComboObat;
PenataJasaRJ.iComboObat;
PenataJasaRJ.iComboVerifiedObat;
PenataJasaRJ.gridObat;
PenataJasaRJ.grid1;
PenataJasaRJ.grid2;
PenataJasaRJ.grid3;//untuk data grid di tab labolatorium
PenataJasaRJ.ds1;
PenataJasaRJ.ds2;
PenataJasaRJ.ds3;//untuk data store grid di tab labolatorium
PenataJasaRJ.ds4=new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab']});
PenataJasaRJ.ds5=new WebApp.DataStore({ fields: ['id_status','status']});
PenataJasaRJ.dsGridObat;
PenataJasaRJ.dsGridTindakan;
PenataJasaRJ.s1;
PenataJasaRJ.btn1;

PenataJasaRJ.form={};
PenataJasaRJ.func={};

PenataJasaRJ.form.Checkbox={};
PenataJasaRJ.form.Class={};
PenataJasaRJ.form.ComboBox={};
PenataJasaRJ.form.DataStore={};
PenataJasaRJ.form.Grid={};
PenataJasaRJ.form.Group={};
PenataJasaRJ.form.Group.print={};
PenataJasaRJ.form.Window={};

PenataJasaRJ.form.DataStore.penyakit=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_penyakit','penyakit'],data: []});
PenataJasaRJ.form.DataStore.produk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});

PenataJasaRJ.ComboVerifiedObat=function(){
	PenataJasaRJ.iComboVerifiedObat	= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			id		: 0,
			fields	: ['Id','displayText'],
			data	: [[0, 'Verified'],[1, 'Not Verified']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return PenataJasaRJ.iComboVerifiedObat;
};

PenataJasaRJ.classGridObat	= Ext.data.Record.create([
   {name: 'kd_prd', 	mapping: 'kd_prd'},
   {name: 'nama_obat', 	mapping: 'nama_obat'},
   {name: 'jumlah', 	mapping: 'jumlah'},
   {name: 'satuan', 	mapping: 'satuan'},
   {name: 'cara_pakai', mapping: 'cara_pakai'},
   {name: 'kd_dokter', 	mapping: 'kd_dokter'},
   {name: 'verified', 	mapping: 'verified'},
   {name: 'racikan', 	mapping: 'racikan'}
]);

PenataJasaRJ.classGrid3	= Ext.data.Record.create([
    {name: 'kd_produk', 	mapping: 'kd_produk'},
    {name: 'kd_klas', 	mapping: 'kd_klas'},
    {name: 'deskripsi', 	mapping: 'deskripsi'},
    {name: 'username', 	mapping: 'username'},
    {name: 'kd_lab', 	mapping: 'kd_lab'}
 ]);

PenataJasaRJ.form.Class.diagnosa= Ext.data.Record.create([
  {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
  {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
  {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
  {name: 'URUT', 	mapping: 'UURUTRUT'},
  {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
  {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
  {name: 'KASUS', 	mapping: 'KASUS'},
  {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
  {name: 'NOTE', 	mapping: 'NOTE'},
  {name: 'DETAIL', 	mapping: 'DETAIL'}
]);
PenataJasaRJ.form.Class.produk= Ext.data.Record.create([
  {name: 'KD_PRODUK', 	mapping: 'KD_PRODUK'},
  {name: 'DESKRIPSI', 	mapping: 'PDESKRIPSIENYAKIT'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DOKTER', 	mapping: 'DOKTER'},
  {name: 'TGL_TINDAKAN', 	mapping: 'TGL_TINDAKAN'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DESC_REQ', 	mapping: 'DESC_REQ'},
  {name: 'TGL_BERLAKU', 	mapping: 'TGL_BERLAKU'},
  {name: 'NO_TRANSAKSI', 	mapping: 'NO_TRANSAKSI'},
  {name: 'URUT', 	mapping: 'URUT'},
  {name: 'DESC_STATUS', 	mapping: 'DESC_STATUS'},
  {name: 'TGL_TRANSAKSI', 	mapping: 'TGL_TRANSAKSI'},
  {name: 'KD_TARIF', 	mapping: 'KD_TARIF'},
  {name: 'HARGA', 	mapping: 'HARGA'}
]);

PenataJasaRJ.func.getNullProduk= function(){
	var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
	return new PenataJasaRJ.form.Class.diagnosa({
		KD_PRODUK	: '',
		DESKRIPSI	: '',
		QTY			: 0,
		DOKTER		: '',
		TGL_TINDAKAN: '',
		QTY			: 0,
		DESC_REQ	: '',
		TGL_BERLAKU	: '',
		NO_TRANSAKSI: '',
		URUT		: '',
		DESC_STATUS	: '',
		TGL_TRANSAKSI: o.TANGGAL_TRANSAKSI,
		KD_TARIF	: '',
		HARGA		: ''
	});
};

PenataJasaRJ.func.getNullDiagnosa= function(){
	return new PenataJasaRJ.form.Class.diagnosa({
		KD_PENYAKIT	: '',
		PENYAKIT	: '',
		KD_PASIEN	: '',
		URUT		: 0,
		URUT_MASUK	: '',
		TGL_MASUK	: '',
		KASUS		: '',
		STAT_DIAG	: '',
		NOTE		: '',
		DETAIL		: 0
	});
};

PenataJasaRJ.nullGridObat	= function(){
	return new PenataJasaRJ.classGridObat({
		kd_produk	: '',
		nama_obat	: '',
		jumlah		: 0,
		satuan		: '',
		cara_pakai	: '',
		kd_dokter	: '',
		verified	: 'Not Verified',
		racikan		: 0
	});
};

PenataJasaRJ.nullGrid3	= function(){
	return new PenataJasaRJ.classGrid3({
		kd_prd		: '',
		kd_klas		: '',
		deskripsi	: '',
		username	: '',
		kd_lab		: ''
	});
};
PenataJasaRJ.comboObat	= function(){
	var $this	= this;
	$this.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat'] });
	$this.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ'
		} 
	});
	$this.iComboObat= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
		store			: $this.dsComboObat,
		valueField		: 'nama_obat',
		hideTrigger		: true,
		displayField	: 'nama_obat',
		value			: '',
		listeners		: {
			select	: function(a, b, c){	
				var line	= $this.gridObat.getSelectionModel().selection.cell[0];
				$this.dsGridObat.getRange()[line].data.kd_prd=b.json.kd_prd;
				$this.dsGridObat.getRange()[line].data.satuan=b.json.satuan;
				$this.dsGridObat.getRange()[line].data.kd_dokter=b.json.nama;
				$this.dsGridObat.getRange()[line].data.nama_obat=b.json.nama_obat;
				$this.gridObat.getView().refresh();
		    }
		}
	});
	return $this.iComboObat;
};
	
function getPanelRWJ(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI','ANAMNESE','CAT_FISIK'];
    dsTRKasirRWJList = new WebApp.DataStore({ fields: Field });
    PenataJasaRJ.ds1=dsTRKasirRWJList;
    refeshkasirrwj();
    var grListTRRWJ = new Ext.grid.EditorGridPanel({
        stripeRows	: true,
        store		: dsTRKasirRWJList,
        anchor		: '100% 58%',
        columnLines	: true,
        autoScroll	: false,
        border		: true,
		sort 		: false,
        sm			: new Ext.grid.RowSelectionModel({
        	singleSelect: true,
            listeners	:{
                rowselect: function(sm, row, rec){
                    rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(row);
                }
            }
        }),
        listeners	: {
            rowdblclick	: function (sm, ridx, cidx){
                rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(ridx);
                PenataJasaRJ.s1=PenataJasaRJ.ds1.getAt(ridx);
                if (rowSelectedKasirRWJ != undefined){
                    RWJLookUp(rowSelectedKasirRWJ.data);
                    
                }else{
                    RWJLookUp();
                }
            }
        },
        cm			: new Ext.grid.ColumnModel([ 
                 new Ext.grid.RowNumberer(),
			{
                header		: 'Status Posting',
                width		: 50,
                sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
                dataIndex	: 'POSTING_TRANSAKSI',
                id			: 'txtposting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
                     switch (value){
                         case 't':
                             metaData.css = 'StatusHijau'; 
                             break;
                         case 'f':
                        	 metaData.css = 'StatusMerah';
                             break;
                     }
                     return '';
                }
            },{
                id			: 'colReqIdViewRWJ',
                header		: 'No. Transaksi',
                dataIndex	: 'NO_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 80
            },{
                id			: 'colTglRWJViewRWJ',
                header		: 'Tgl Transaksi',
                dataIndex	: 'TANGGAL_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 75,
                renderer	: function(v, params, record){
                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                }
            },{
                header		: 'No. Medrec',
                width		: 65,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'KD_PASIEN',
                id			: 'colRWJerViewRWJ'
            },{
                header		: 'Pasien',
                width		: 190,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'NAMA',
                id			: 'colRWJerViewRWJ'
            },{
                id			: 'colLocationViewRWJ',
                header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 170
            }, {
                id			: 'colDeptViewRWJ',
                header		: 'Dokter',
                dataIndex	: 'NAMA_DOKTER',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 150
            },{
                id			: 'colImpactViewRWJ',
                header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
				sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 90
            }
        ]),
        viewConfig	:{forceFit: true},
        tbar		:[
            {
                id		: 'btnEditRWJ',
                text	: nmEditData,
                tooltip	: nmEditData,
                iconCls	: 'Edit_Tr',
                handler	: function(sm, row, rec){
                    if (rowSelectedKasirRWJ != undefined){
                        RWJLookUp(rowSelectedKasirRWJ.data);
                    }else{
                    	ShowPesanWarningRWJ('Pilih data data tabel  ','Edit data');
                    }
                }
            }
        ]
    });
    PenataJasaRJ.grid1=grListTRRWJ;
    var LegendViewpenatajasa= new Ext.Panel({
        id			: 'LegendViewpenatajasa',
        region		: 'center',
        border		: false,
        bodyStyle	: 'padding:0px 7px 0px 7px',
        layout		: 'column',
        frame		: true,
        anchor		: '100% 8.0001%',
        autoScroll	: false,
        items		:[
            {
                columnWidth	: .033,
                layout		: 'form',
                style		: {'margin-top':'-1px'},
                anchor		: '100% 8.0001%',
                border		: false,
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
            },{
                columnWidth	: .08,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Posting"
            },{
                columnWidth	: .033,
                layout		: 'form',
                style		:{'margin-top':'-1px'},
                border		: false,
                anchor		: '100% 8.0001%',
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
            },{
                columnWidth	: .1,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Belum posting"
            }
        ]
    });
    var FormDepanRWJ = new Ext.Panel({
        id			: mod_id,
        closable	: true,
        region		: 'center',
        layout		: 'form',
        title		: 'Penata Jasa ',
        border		: false,
        shadhow		: true,
        autoScroll	: false,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        items		: [            
 		   {
		       xtype		: 'panel',
			   plain		: true,
			   activeTab	: 0,
			   height		: 180,
			   defaults		: {
				   bodyStyle	:'padding:10px',
				   autoScroll	: true
			   },
			   items		: [
        		   {
						layout	: 'form',
						margins	: '0 5 5 0',
						border	: true ,
						items	:[
							 {
								 xtype		: 'textfield',
								 fieldLabel	: ' No. Medrec' + ' ',
								 id			: 'txtFilterNomedrec',
								 anchor 	: '70%',
								 onInit		: function() { },
								 listeners	: {
									 'specialkey' : function(){
										 var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
										 if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
											 if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
												 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue());
												 Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
												 var tmpkriteria = getCriteriaFilter_viDaftar();
												 RefreshDataFilterKasirRWJ();
											 }else{
												 if (tmpNoMedrec.length === 10){
													 RefreshDataFilterKasirRWJ();
												 }else{
													 Ext.getCmp('txtFilterNomedrec').setValue('');
												 }
											 }
										 }
									 }
								 }
							},{	 
								xtype	: 'tbspacer',
								height	: 3
							},{
								xtype			: 'textfield',
								fieldLabel		: ' Pasien' + ' ',
								id				: 'TxtFilterGridDataView_NAMA_viKasirRwj',
								anchor 			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
							   		'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},{	 
								xtype	: 'tbspacer',
								height	: 3
							},{
								xtype			: 'textfield',
								fieldLabel		: ' Dokter' + ' ',
								id				: 'TxtFilterGridDataView_DOKTER_viKasirRwj',
								anchor			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
												  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},
							getItemcombo_filter(),getItemPaneltgl_filter()
						]
        		   	}		
				]
 		   	},
			grListTRRWJ,LegendViewpenatajasa]
	});
   return FormDepanRWJ;

};

function mComboStatusBayar_viKasirRwj(){
	var cboStatus_viKasirRwj = new Ext.form.ComboBox({
		id				: 'cboStatus_viKasirRwj',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		fieldLabel		: 'Status Posting',
		store			: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
		}),
		valueField		: 'Id',
		displayField	: 'displayText',
		value			: selectCountStatusByr_viKasirRwj,
		listeners		:{
			select: function(a,b,c){
				selectCountStatusByr_viKasirRwj=b.data.displayText ;
				RefreshDataFilterKasirRWJ();
			}
		}
	});
	return cboStatus_viKasirRwj;
};

function mComboUnit_viKasirRwj(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwj.load({
	    params:{
		    Skip	: 0,
		    Take	: 1000,
            Sort	: 'kd_unit',
		    Sortdir	: 'ASC',
		    target	: 'ComboUnit',
            param	: ""
		}
	});
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox({
	    id				: 'cboUNIT_viKasirRwj',
	    typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
	    fieldLabel		: ' Poli Tujuan',
	    align			: 'Right',
	    width			: 100,
	    anchor			: '100%',
	    store			: dsunit_viKasirRwj,
	    valueField		: 'NAMA_UNIT',
	    displayField	: 'NAMA_UNIT',
		value			:'All',
	    listeners		:{
		    select: function(a, b, c){					       
		    	RefreshDataFilterKasirRWJ();
		    }
		}
	});
    return cboUNIT_viKasirRwj;
};

function RWJLookUp(rowdata){
    var lebar = 850;
    FormLookUpsdetailTRrwj = new Ext.Window({
            id			: 'gridRWJ',
            title		: 'Penata Jasa Rawat Jalan',
            closeAction	: 'destroy',
            width		: lebar,
            height		: 600,
            border		: false,
            resizable	: false,
            plain		: true,
            constrain	: true,
            layout		: 'fit',
            iconCls		: 'Request',
            modal		: true,
            items		: getFormEntryTRRWJ(lebar,rowdata),
            listeners	: {
            	close: function(){	
            		RefreshDataFilterKasirRWJ();
        		}
            }
    });
    FormLookUpsdetailTRrwj.show();
    dsCmbRwJPJDiag.loadData([],false);
	for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
		var recs    = [],
		recType = dsCmbRwJPJDiag.recordType;
		var o=PenataJasaRJ.ds2.getRange()[i].data;
		recs.push(new recType({
			Id        	:o.KD_PENYAKIT,
			displayText : o.KD_PENYAKIT
	    }));
		dsCmbRwJPJDiag.add(recs);
	}
	PenataJasaRJ.ds4.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboLabRJPJ'
		} 
	});
	var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
	var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
	PenataJasaRJ.ds3.load({
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewGridLabRJPJ',
			param	:par
		}
	});
	PenataJasaRJ.ds5.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboTindakanRJPJ'
		} 
	});
	var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
	var params={
		kd_pasien 	: o.KD_PASIEN,
		kd_unit 	: o.KD_UNIT,
		tgl_masuk	: o.TANGGAL_TRANSAKSI,
		urut_masuk	: o.URUT_MASUK
	};
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/getTindakan",
		params		: params,
		failure		: function(o){
			ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(cst.echo.id_status >=0){
					PenataJasaRJ.iCombo1.selectedIndex=cst.echo.id_status-1;
					PenataJasaRJ.iCombo1.setValue(PenataJasaRJ.ds5.getRange()[(cst.echo.id_status-1)].data.status);
					PenataJasaRJ.iTArea1.setValue(cst.echo.catatan);
				}
			}else{
				ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			}
		}
	});
    if (rowdata == undefined){
        RWJAddNew();
    }else{
        TRRWJInit(rowdata);
    }
};


function mComboPoliklinik(lebar){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnit',
            param	: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitRWJ').dom.value +'~)'
        }
    });
    var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntry',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '95%',
        listeners:{
            select: function(a, b, c){
               loaddatastoredokter(b.data.KD_UNIT);
			   selectKlinikPoli=b.data.KD_UNIT;
            }
		}
    });
    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ kd_unit+ '~'
		}
    });
}

function mComboDokterRequestEntry(){
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
    var cboDokterRequestEntry = new Ext.form.ComboBox({
	    id				: 'cboDokterRequestEntry',
	    typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    selectOnFocus	: true,
        forceSelection	: true,
	    emptyText		: 'Pilih Dokter...',
	    labelWidth		: 80,
		fieldLabel		: 'Dokter      ',
	    align			: 'Right',
	    store			: dsDokterRequestEntry,
	    valueField		: 'KD_DOKTER',
	    displayField	: 'NAMA',
        anchor			: '95%',
	    listeners		:{
		    select	: function(a,b,c){
				selectDokter = b.data.KD_DOKTER;
            },
            render	: function(c){
                c.getEl().on('keypress', function(e) {
                    if(e.getKey() == 13) // atau
                        Ext.getCmp('kelPasien').focus();
                }, c);
            }
		}
    });
    return cboDokterRequestEntry;
};

function KonsultasiLookUp(rowdata,callback){
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window({
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            constrain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					// width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														// fieldLabel:'Dokter ',
														name: 'txtKdunitKonsultasi',
														id: 'txtKdunitKonsultasi',
														// emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														// hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Kosultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																		Datasave_Konsultasi(false,callback);
																		
																	FormLookUpsdetailTRKonsultasi.close()	;
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close();
															}
														}
											 ]
											
											}
									
								]
			},
            listeners:
            {
            }
        }
    );
    FormLookUpsdetailTRKonsultasi.show();
      KonsultasiAddNew();
};

function KonsultasiAddNew(){
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnitRWJ').dom.value   ;
	Ext.get('txtnamaunitKonsuldbltasi').dom.value=Ext.get('txtNamaUnit').dom.value;

}

function getFormEntryTRRWJ(lebar,data){
	var pnlTRRWJ = new Ext.FormPanel({
		id			: 'PanelTRRWJ',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        bodyStyle	: 'padding:10px 10px 10px 10px',
        height		: 189,
        anchor		: '100%',
        width		: lebar,
        border		: false,
        items		: [getItemPanelInputRWJ(lebar)],
        tbar		: [
			{
		        text: ' Konsultasi',
		        id:'btnLookUpKonsultasi_viKasirIgd',
		        iconCls: 'Konsultasi',
		        handler: function(){
		        	KonsultasiLookUp();
		        }
	    	},{
		        text: ' Ganti Dokter',
		        id:'btnLookUpGantiDokter_viKasirIgd',
		        iconCls: 'gantidok',
		        handler: function(){
				   GantiDokterLookUp();
		        }
	    	},{
		        text: 'Ganti Kelompok Pasien',
		        id:'btngantipasien',
		        iconCls: 'gantipasien',
		        handler: function(){
		        	KelompokPasienLookUp();
		        }
	    	},{
		        text: 'Posting Ke Kasir',
		        id:'btnposting',
		        iconCls: 'gantidok',
		        handler: function(){
		        	setpostingtransaksi(data.NO_TRANSAKSI);
		        }
			},{
				xtype:'splitbutton',
				text:'Cetak',
				iconCls:'print',
				id:'btnPrint_Poliklinik',
				handler:function(){	}
			}
        ]
    });
    var x;
	var GDtabDetailRWJ = new Ext.TabPanel({
        id			:'GDtabDetailRWJ',
        region		: 'center',
        activeTab	: 0,
		height		: 480,
		width 		: 815,
        anchor		: '100% 100%',
        border		: false,
        plain		: true,
        defaults	: {
            autoScroll	: true
		},
        items		: [GetDTLTRAnamnesisGrid(),GetDTLTRRWJGrid(data),GetDTLTRDiagnosaGrid(),PenataJasaRJ.getLabolatorium(data),GetDTLTRRadiologiGrid(data),PenataJasaRJ.getTindakan(data)],
        tbar		:[
			{
				text	: 'Simpan Anamnese',
		        id		: 'btnsimpanAnamnese',
		        iconCls	: 'Konsultasi',
		        handler	: function(){
		        	Datasave_Anamnese(false);	
		        }
			},{
				text	: 'Tambah Produk',
				id		: 'btnLookupRWJ',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanRWJ',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					var e=false;
					if(PenataJasaRJ.dsGridTindakan.getRange().length > 0){
						for(var i=0,iLen=PenataJasaRJ.dsGridTindakan.getRange().length; i<iLen ; i++){
							var o=PenataJasaRJ.dsGridTindakan.getRange()[i].data;
							if(o.QTY == '' || o.QTY==0 || o.QTY == null){
								PenataJasaRJ.alertError('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
								e=true;
								break;
							}
							
						}
					}else{
						PenataJasaRJ.alertError('Isi Tindakan Yang Diberikan','Peringatan');
						e=true;
					}
					for(var i=0,iLen=PenataJasaRJ.dsGridObat.getRange().length; i<iLen ; i++){
						var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
						if(o.nama_obat == '' || o.nama_obat == null){
							PenataJasaRJ.alertError('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
							PenataJasaRJ.alertError('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.cara_pakai == ''||  o.cara_pakai == null){
							PenataJasaRJ.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.verified == '' || o.verified==null){
							PenataJasaRJ.alertError('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
					}
					if(e==false){
						Datasave_KasirRWJ(false);
					}
				}
			},{
                id		:'btnHpsBrsRWJ',
                text	: 'Hapus Baris',
                tooltip	: 'Hapus Baris',
                iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailKasirRWJList.getCount() > 0 ){
                        if (cellSelecteddeskripsi != undefined){
                            if(CurrentKasirRWJ != undefined){
                                    HapusBarisRWJ();
                            }
                        }else{
                            ShowPesanWarningRWJ('Pilih record ','Hapus data');
                        }
                    }
                }
            },{
				text	: 'Tambah Diagnosa',
				id		: 'btnLookupDiagnosa',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					PenataJasaRJ.ds2.insert(PenataJasaRJ.ds2.getCount(),PenataJasaRJ.func.getNullDiagnosa());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanDiagnosa',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					if (dsTRDetailDiagnosaList.getCount() > 0 ){
						var e=false;
						for(var i=0,iLen=dsTRDetailDiagnosaList.getCount(); i<iLen; i++){
							var o=dsTRDetailDiagnosaList.getRange()[i].data;
							if(o.STAT_DIAG=='' || o.STAT_DIAG==null){
								PenataJasaRJ.alertError('Diagnosa : Diagnosa Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
							if(o.KASUS=='' || o.KASUS==null){
								PenataJasaRJ.alertError('Diagnosa : Kasus Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
						}
						if(e==false){
							Datasave_Diagnosa(false);
						}
					}
				}
			},{
	            id		:'btnHpsBrsDiagnosa',
	            text	: 'Hapus Baris',
	            tooltip	: 'Hapus Baris',
	            iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailDiagnosaList.getCount() > 0 ){
                        if (cellSelecteddeskripsi != undefined){
                        	if(CurrentDiagnosa != undefined){
                                HapusBarisDiagnosa();
                            }
                        }else{
                            ShowPesanWarning('Pilih record ','Hapus data');
                        }
                    }
                }
			},{
				text	: 'Tambah Baris',
				id		: 'btnbarisRad',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					TambahBarisRad();
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanRad',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					datasavepoliklinikrad();
				}
			}, {
                id:'btnHpsBrsRad',
                text: 'Hapus Baris',
                tooltip: 'Hapus Baris',
                iconCls: 'RemoveRow',
                handler: function(){
                    if (dsRwJPJLab.getCount() > 0 ){
                        if (cellselectedrad != undefined){
                            if(CurrentRad != undefined){
                                HapusBarisRad();
                            }
                        }else{
                            ShowPesanWarningRWJ('Pilih record ','Hapus data');
                        }
                    }
                }
            },
				PenataJasaRJ.btn1= new Ext.Button({
					text	: 'Tambah Baris',
					id		: 'RJPJBtnAddLab',
					tooltip	: nmLookup,
					iconCls	: 'find',
					handler	: function(){
						PenataJasaRJ.ds3.insert(PenataJasaRJ.ds3.getCount(),PenataJasaRJ.nullGrid3());
					}
				}),
				PenataJasaRJ.btn2= new Ext.Button({
					text	: 'Simpan',
					id		: 'RJPJBtnSaveLab',
					tooltip	: nmLookup,
					iconCls	: 'save',
						handler	: function(){
							if(PenataJasaRJ.ds3.getCount()==0){
								PenataJasaRJ.alertError('Laboratorium: Harap isi data Labolatorium.','Peringatan');
							}else{
								var e=false;
								for(var i=0,iLen=PenataJasaRJ.ds3.getCount();i<iLen ; i++){
									if(PenataJasaRJ.ds3.getRange()[i].data.kd_produk=='' || PenataJasaRJ.ds3.getRange()[i].data.kd_produk==null){
										PenataJasaRJ.alertError('Laboratorium: Data Laboratorium baris-'+(i+1)+' tidak Lengkap.','Peringatan');
										e=true;
										break;
									}
								}
								if(e==false){
									var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
									var params={
										kd_pasien 	: o.KD_PASIEN,
										kd_unit 	: o.KD_UNIT,
										tgl_masuk	: o.TANGGAL_TRANSAKSI,
										urut_masuk	: o.URUT_MASUK,
										jum			: PenataJasaRJ.ds3.getCount()
									};
									for(var i=0,iLen=PenataJasaRJ.ds3.getCount();i<iLen ; i++){
										params['kd_produk'+i]=PenataJasaRJ.ds3.getRange()[i].data.kd_produk;
										params['kd_lab'+i]=PenataJasaRJ.ds3.getRange()[i].data.kd_lab;
									}
									Ext.Ajax.request({
										url			: baseURL + "index.php/main/functionRWJ/savelaboratorium",
										params		: params,
										failure		: function(o){
											ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
										},
										success		: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
												ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
												PenataJasaRJ.ds3.load({
													target	:'ViewGridLabRJPJ',
													param	:par
												});
											}else{
												ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
											}
										}
									});
							    }
							}
						}
					}),
					PenataJasaRJ.btn3= new Ext.Button({
						text	: 'Hapus',
						id		: 'RJPJBtnDelLab',
						tooltip	: nmLookup,
						iconCls	: 'RemoveRow',
						handler	: function(){
							var line=PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
							if(PenataJasaRJ.grid3.getSelectionModel().selection==null){
								ShowPesanWarningRWJ('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
							}else{
								Ext.Msg.show({
									title:nmHapusBaris,
				                   	msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + PenataJasaRJ.ds3.getRange()[line].data.kd_produk ,
				                   	buttons: Ext.MessageBox.YESNO,
				                   	fn: function (btn){
				                   		if (btn =='yes'){
				                   			var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
											var params={
												kd_pasien 	: o.KD_PASIEN,
												kd_unit 	: o.KD_UNIT,
												tgl_masuk	: o.TANGGAL_TRANSAKSI,
												urut_masuk	: o.URUT_MASUK,
												kd_produk	: PenataJasaRJ.ds3.getRange()[line].data.kd_produk
											};
											Ext.Ajax.request({
												url			: baseURL + "index.php/main/functionRWJ/deletelaboratorium",
												params		: params,
												failure		: function(o){
													ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
												},
												success		: function(o){
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) {
														var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
														ShowPesanInfoDiagnosa('Data Berhasil Dihapus', 'Info');
														PenataJasaRJ.ds3.load({
															target	:'ViewGridLabRJPJ',
															param	:par
														});
													}else{
														ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
													}
												}
											});
											PenataJasaRJ.ds3.removeAt(line);
											PenataJasaRJ.grid3.getView().refresh();
				                        }
				                   	},
				                   	icon: Ext.MessageBox.QUESTION
				                });
							}
						}
					}),
					PenataJasaRJ.btn4= new Ext.Button({
						text	: 'Simpan',
						id		: 'RJPJBtnSaveTin',
						tooltip	: nmLookup,
						iconCls	: 'save',
						handler	: function(){
							var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
							if(PenataJasaRJ.iCombo1.selectedIndex>-1 || PenataJasaRJ.iTArea1.getValue()==''){
								var params={
									kd_pasien 	: o.KD_PASIEN,
									kd_unit 	: o.KD_UNIT,
									tgl_masuk	: o.TANGGAL_TRANSAKSI,
									urut_masuk	: o.URUT_MASUK,
									id_status	: PenataJasaRJ.ds5.getRange()[PenataJasaRJ.iCombo1.selectedIndex].data.id_status,
									catatan		: PenataJasaRJ.iTArea1.getValue()
								};
								Ext.Ajax.request({
									url			: baseURL + "index.php/main/functionRWJ/saveTindakan",
									params		: params,
									failure		: function(o){
										ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
									},
									success		: function(o){
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
										}else{
											ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
										}
									}
								});
							}else{
								ShowPesanWarningRWJ('Status/Catatan tidak boleh kosong.', 'Peringatan');
							}
						}
					})
        ],
		listeners:{
               tabchange : function (panel, tab) {
				if (tab.id == 'tabDiagnosa'){
					dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
					Ext.getCmp('catLainGroup').hide();
					Ext.getCmp('txtneoplasma').hide();
					Ext.getCmp('txtkecelakaan').hide();
					Ext.getCmp('btnsimpanAnamnese').hide();
					
	                 Ext.getCmp('btnHpsBrsDiagnosa').show();
					 Ext.getCmp('btnSimpanDiagnosa').show();
					 Ext.getCmp('btnLookupDiagnosa').show();
				 
	                 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
				 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 Ext.getCmp('btnbarisRad').hide();
					 Ext.getCmp('btnSimpanRad').hide();
					 Ext.getCmp('btnHpsBrsRad').hide();
				 
				 	PenataJasaRJ.btn4.hide();
				}else if(tab.id == 'tabTransaksi'){
					 Ext.getCmp('btnsimpanAnamnese').hide();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').show();
					 Ext.getCmp('btnSimpanRWJ').show();
					 Ext.getCmp('btnLookupRWJ').show();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad').hide();
					 Ext.getCmp('btnSimpanRad').hide();
					 Ext.getCmp('btnHpsBrsRad').hide();
				 
				}else if (tab.id == 'tabradiologi'){
					 Ext.getCmp('btnsimpanAnamnese').hide();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad').show();
         			 Ext.getCmp('btnSimpanRad').show();
					 Ext.getCmp('btnHpsBrsRad').show();

				}else if(tab.id == 'tabAnamnses'){
					 Ext.getCmp('btnsimpanAnamnese').show();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 Ext.getCmp('btnbarisRad').hide();
					 Ext.getCmp('btnSimpanRad').hide();
					 Ext.getCmp('btnHpsBrsRad').hide();
					 
					 PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabLaboratorium'){
					PenataJasaRJ.btn1.show();
					PenataJasaRJ.btn2.show();
					PenataJasaRJ.btn3.show();
					Ext.getCmp('btnsimpanAnamnese').hide();
					
					Ext.getCmp('btnHpsBrsDiagnosa').hide();
					Ext.getCmp('btnSimpanDiagnosa').hide();
					Ext.getCmp('btnLookupDiagnosa').hide();
					
					Ext.getCmp('btnHpsBrsRWJ').hide();
					Ext.getCmp('btnSimpanRWJ').hide();
					Ext.getCmp('btnLookupRWJ').hide();
					
					Ext.getCmp('btnbarisRad').hide();
					Ext.getCmp('btnSimpanRad').hide();
					Ext.getCmp('btnHpsBrsRad').hide();
				 
					PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabTindakan'){
					Ext.getCmp('btnsimpanAnamnese').hide();
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.show();
					 
					 Ext.getCmp('btnbarisRad').hide();
					 Ext.getCmp('btnSimpanRad').hide();
					 Ext.getCmp('btnHpsBrsRad').hide();
				 
				}
			}
        }
	});
   var pnlTRRWJ2 = new Ext.FormPanel({
            id: 'PanelTRRWJ2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:360,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRWJ]
    });
    var FormDepanRWJ = new Ext.Panel({
		    id: 'FormDepanRWJ',
		    region: 'center',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
			resizable:false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRWJ,pnlTRRWJ2	]
		});
	
	if( data.POSTING_TRANSAKSI == 't'){
		setdisablebutton();
	}else{
		setenablebutton();	
	}
    return FormDepanRWJ;
};

function RecordBaruDiagnosa(){
	var p = new mRecordDiagnosa({
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    'URUT_MASUK':''
		});
	return p;
};

function HapusBarisDiagnosa(){
    if ( cellSelecteddeskripsi != undefined ){
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != ''){
            Ext.Msg.show( {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn) {
                       if (btn =='yes'){
                    	   dsCmbRwJPJDiag.removeAt(PenataJasaRJ.grid2.getSelectionModel().selection.cell[0]);
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === ''){
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            } else{
                                if (btn =='yes'){
                                   DataDeleteDiagnosaDetail();
                                };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        } else{
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail(){
    Ext.Ajax.request({
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                  RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
                } else{
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    );
};

function getParamDataDeleteDiagnosaDetail(){
    var params = {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT
    };
    return params;
};

function getParamDetailTransaksiDiagnosa2(){
	var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
    var params ={
		Table:'ViewTrDiagnosa',
		KdPasien: o.KD_PASIEN,
		KdUnit: o.KD_UNIT,
		UrutMasuk:o.URUT_MASUK,
		Tgl: o.TANGGAL_TRANSAKSI,
		JmlList:GetListCountDetailDiagnosa()
	};
    var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		params['URUT_MASUK'+i]=dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
		params['KD_PENYAKIT'+i]=dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
		params['STAT_DIAG'+i]=dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
		params['KASUS'+i]=dsTRDetailDiagnosaList.data.items[i].data.KASUS;
		params['DETAIL'+i]=dsTRDetailDiagnosaList.data.items[i].data.DETAIL;
		params['NOTE'+i]=dsTRDetailDiagnosaList.data.items[i].data.NOTE;
	}	
    return params;
};

function GetListCountDetailDiagnosa(){
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != ''){
			x += 1;
		};
	}
	return x;
};

function getArrDetailTrDiagnosa(){
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++){
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != ''){
			var y='';
			var z='@@##$$@@';
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS;
			if (i === (dsTRDetailDiagnosaList.getCount()-1)){
				x += y ;
			}else{
				x += y + '##[[]]##';
			};
		};
	}	
	return x;
};

function getArrdetailAnamnese(){
	var x = '';
		var y='';
		var z='::';
	var hasil;
	for(var k = 0; k < dsTRDetailAnamneseList.getCount(); k++){
		if (dsTRDetailAnamneseList.data.items[k].data.HASIL == null){
			x += '';
		}else{
			hasil = dsTRDetailAnamneseList.data.items[k].data.HASIL;
			y = dsTRDetailAnamneseList.data.items[k].data.ID_KONDISI;
			y += z + hasil;
		}
		x += y + '<>';
	}
	return x;
}

function Datasave_Diagnosa(mBol){	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/saveDiagnosaPoliklinik",
		params: getParamDetailTransaksiDiagnosa2(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
				if(mBol === false){
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				}
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function Datasave_Anamnese(mBol) {	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: getParamDetailAnamnese(),
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoAnamnese(nmPesanSimpanSukses,nmHeaderSimpanData);
				//RefreshDataFilterKasirRWJ();
				if(mBol === false){
					RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				};
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningAnamnese(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorAnamnese(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function ShowPesanWarningDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) 
{
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Anamnese Ada Masalah",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Gagal Disimpan",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: "Data Sukses diSimpan",
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function GetDTLTRRadiologiGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Radiologi',
		id:'tabradiologi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetGridRwJPJRad(data)]
    });

	return tabTransaksi;
};

PenataJasaRJ.getLabolatorium=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Laboratorium',
		id:'tabLaboratorium',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [$this.getGrid3(data)]
    });
	return tabTransaksi;
};

PenataJasaRJ.getTindakan=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Tindak Lanjut',
		id:'tabTindakan',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [
			{
				layout	: 'column',
			    bodyStyle: 'margin-top: 10px;',
			    border	: false,
			   
				items:[{
					layout: 'form',
					labelWidth:100,
					labelAlign:'right',
				    border: false,
				    items	: [
							$this.iCombo1= new Ext.form.ComboBox({
								id				: 'iComboStatusTindakanRJPJ',
								typeAhead		: true,
							    triggerAction	: 'all',
							    lazyRender		: true,
							    mode			: 'local',
							    emptyText		: '',
							    width			: 300,
								store			: $this.ds5,
								valueField		: 'status',
								displayField	: 'status',
								value			: '',
								fieldLabel		: 'Status &nbsp;',
								listeners		: {
									select	: function(a, b, c){
										if(b.json.id_status==4){
											KonsultasiLookUp(null,function(id){
												var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
												var params={
													kd_pasien 	: o.KD_PASIEN,
													kd_unit 	: o.KD_UNIT,
													tgl_masuk	: o.TANGGAL_TRANSAKSI,
													urut_masuk	: o.URUT_MASUK,
													id_status	: 4,
													catatan		: PenataJasaRJ.iTArea1.getValue(),
													kd_unit_tujuan: id
												};
												Ext.Ajax.request({
													url			: baseURL + "index.php/main/functionRWJ/saveTindakan",
													params		: params,
													failure		: function(o){
														ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
													},
													success		: function(o){
														var cst = Ext.decode(o.responseText);
														if (cst.success === true) {
														}else{
															ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
														}
													}
												});
											});
										}
								    }
								}
							}),
							$this.iTArea1= new Ext.form.TextArea({
								id			: 'iTextAreaCatLabRJPJ',
								fieldLabel	: 'Catatan &nbsp; ',
								width       : 500,
					            autoScroll  : true,
					            height      : 80
							})
						]}
				]
			}
        ]
    });
	return tabTransaksi;
};

PenataJasaRJ.getGrid3=function(data){
	var $this=this;
	PenataJasaRJ.ds3 = new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab'] });
	PenataJasaRJ.grid3 = new Ext.grid.EditorGridPanel({
        title: 'Laboratorium',
		id:'grid3',
        stripeRows: true,
        height: 290,
        store: PenataJasaRJ.ds3,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        cm: $this.getModel1(),
        viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.grid3;
};

function GetGridRwJPJRad(data){
	
	var fldDetail = ['ID_RADKONSUL','KD_PRODUK','KD_KLAS','DESKRIPSI','KD_DOKTER'];
	dsRwJPJLab = new WebApp.DataStore({ fields: fldDetail });
    var gridDTLTRRWJ = new Ext.grid.EditorGridPanel({
        title: 'Radiologi',
		id:'gridRwJPJRad',
        stripeRows: true,
        height: 290,
        store: dsRwJPJLab,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
		 sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    cellselectedrad = dsRwJPJLab.getAt(row);
                    CurrentRad.row = row;
                    CurrentRad.data = cellselectedrad;
                }
          }
        }),
        cm: getModelRwJPJRad(),
        viewConfig:{forceFit: true}
    });
    
    return gridDTLTRRWJ;
}
PenataJasaRJ.getModel1=function(){
	var $this=this;
	return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: Nci.getId(),
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width		: 25,
			menuDisabled: true,
            hidden		: false,
            editor		: $this.iCombo1= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: $this.ds4,
        		valueField		: 'kd_produk',
        		hideTrigger		: true,
        		displayField	: 'kd_produk',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){	
        				var line	= $this.grid3.getSelectionModel().selection.cell[0];
        				$this.ds3.getRange()[line].data.kd_produk=b.json.kd_produk;
        				$this.ds3.getRange()[line].data.kd_klas=b.json.kd_klas;
        				$this.ds3.getRange()[line].data.deskripsi=b.json.deskripsi;
        				$this.ds3.getRange()[line].data.username=b.json.username;
        				$this.ds3.getRange()[line].data.kd_lab=b.json.kd_lab;
        				$this.grid3.getView().refresh();
        		    }
        		}
        	})
        },{
			id			: Nci.getId(),
        	header		: 'KELAS',
            dataIndex	: 'kd_klas',
            width		: 30,
			menuDisabled: true,
            hidden		: false
        },{
            id			: Nci.getId(),
            header		: 'DESKRIPSI',
            dataIndex	: 'deskripsi',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
			editor		: $this.iCombo2= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: $this.ds4,
        		valueField		: 'deskripsi',
        		hideTrigger		: true,
        		displayField	: 'deskripsi',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){	
        				var line	= $this.grid3.getSelectionModel().selection.cell[0];
        				$this.ds3.getRange()[line].data.kd_produk=b.json.kd_produk;
        				$this.ds3.getRange()[line].data.kd_klas=b.json.kd_klas;
        				$this.ds3.getRange()[line].data.deskripsi=b.json.deskripsi;
        				$this.ds3.getRange()[line].data.username=b.json.username;
        				$this.ds3.getRange()[line].data.kd_lab=b.json.kd_lab;
        				$this.grid3.getView().refresh();
        		    }
        		}
        	})
        },{
            id			: Nci.getId(),
            header		: 'DOKTER',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'username'	
        }
    ]);
};

function getModelRwJPJRad(){
	return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id: 'colCitoRwJPJRad',
        	header: 'CITO',
            dataIndex: 'ID_RADKONSUL',
            width:80,
			menuDisabled:true,
            hidden:true
        },{
			id: 'colKodeRwJPJRad',
        	header: 'Kode',
            dataIndex: 'KD_PRODUK',
            width:150,
			menuDisabled:true,
            hidden:false,
			editor:getRadtest()
        },{
            id: 'colKelasRwJPJRad',
            header:'KELAS',
			dataIndex: 'KD_KLAS',
			menuDisabled:true,
			hidden:false,
            width:50
        },{
            id: 'colDescRwJPJRad',
            header: 'DESKRIPSI',
			dataIndex: 'DESKRIPSI',
			hidden: false,
			menuDisabled:true,
			editor:getRadDesk()
        },{
            id: 'colDokterRwJPJRad',
            header: 'DOKTER',
			dataIndex: 'KD_DOKTER',
			menuDisabled:true,
			hidden: false,
			width:50
        }
    ]);
}

function GetDTLTRAnamnesisGrid() {
	var pnlTRAnamnese = new Ext.Panel({
			title: 'Anamnese',
			id:'tabAnamnses',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            // bodyStyle: 'padding:10px 10px 10px 10px',
            height:100,
            anchor: '100%',
            width: 260,
            border: false,
            items: [	GetDTLTRAnamnesis(),textareacatatanAnemneses()],
			tbar:[
			{
			    xtype: 'textarea',
			    fieldLabel:'Anamnesis  ',
			    name: 'txtareaAnamnesis',
			    id: 'txtareaAnamnesis',
				width:812,
				readOnly:false,
			    anchor: '99%'
			}
		]
    });
	return pnlTRAnamnese;
}

function textareacatatanAnemneses(){
	var TextAreaCatatanAnamnese = new Ext.Panel({
		title: 'Catatan Fisik',
		id:'tabtextAnamnses',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        // bodyStyle: 'padding:10px 10px 10px 10px',
        height:130,
        anchor: '100%',
        width: 815,
        border: false,
        items: [	
			{
				xtype: 'textarea',
			    fieldLabel:'Catatan',
			    name: 'txtareaAnamnesiscatatan',
			    id: 'txtareaAnamnesiscatatan',
				width:815,
				height:85,
				readOnly:false,
			    anchor: '100%'
			}
		]
	});
	return TextAreaCatatanAnamnese;
}

function GetDTLTRAnamnesis() {
    var Anamfied = ['ID_KONDISI','KONDISI','SATUAN','ORDERLIST','KD_UNIT','HASIL'];
    dsTRDetailAnamneseList = new WebApp.DataStore({ fields: Anamfied });
    var gridDTLTRAnamnese = new Ext.grid.EditorGridPanel({
		id:'tabcatAnamnses',
        stripeRows: true,
        store: dsTRDetailAnamneseList,
        border: true,
        columnLines: true,
        frame: false,
		height:120,
        anchor: '100%',
        autoScroll:true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                }
            }
        }),
        cm: TRAnamneseColumModel(),
		viewConfig:{forceFit: true}
    });
    return gridDTLTRAnamnese;
};

function TRAnamneseColumModel() {
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer()
       ,{
			id			: 'colKondisi',
			header 		: 'KONDISI',
			dataindex	: 'KONDISI',
			menuDisbaled: true,
			width		: 100,
			hidden		: false
		},{
            id			: 'colNilai',
            header		: 'NILAI',
            dataIndex	: 'HASIL',
			menuDisabled: true,
			hidden		: false,
			width		: 80,
			editor		: new Ext.form.TextField({
				id				: 'textnilai',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Masukan Nilai...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				valueField		: 'displayText',
				displayField	: 'displayText',
				value			: '',
				listeners		:{}
			})
        },{
            id			: 'colkdunit',
            header		: 'KD UNIT',
            dataIndex	: 'KD_UNIT',
			menuDisabled: true,
			hidden		: true,
			width		: 80
        },{
            id			: 'colSatuan',
            header		: 'SATUAN',
            dataIndex	: 'SATUAN',
			menuDisabled: true,
			width		: 80
        },{
            id			: 'colorderlist',
            header		: 'ORDER LIST',
            dataIndex	: 'ORDERLIST',
			menuDisabled: true,
			hidden		: true,
			width		: 80
        }
    ]);
};

function GetDTLTRDiagnosaGrid(){
	var pnlTRDiagnosa = new Ext.Panel({
		title		: 'Diagnosa',
		id			: 'tabDiagnosa',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        height		: 100,
        anchor		: '100%',
        width		: 815,
        border		: false,
        items		: [GetDTLTRDiagnosaGridFirst(),FieldKeteranganDiagnosa()]
    });
	return pnlTRDiagnosa;
}

function FieldKeteranganDiagnosa(){
	dsCmbRwJPJDiag = new Ext.data.ArrayStore({
		id: 0,
		fields:[
			'Id',
			'displayText'
		],
		data: []
	});
    var items ={
	    layout: 'column',
	    border: true,
	    width: 815,
	    bodyStyle:'margin-top:5px;padding: 5px;',
	    items:[
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					combo = new Ext.form.ComboBox({
						id:'cmbRwJPJDiag',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						editable: false,
						mode: 'local',
						width: 150,
						emptyText:'',
						fieldLabel: 'Kode Penyakit &nbsp;',
						store: dsCmbRwJPJDiag,
						valueField: 'Id',
						displayField: 'displayText',
						listeners:{
							select: function(){
								if(this.getValue() != ''){
									Ext.getCmp('catLainGroup').show();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==2){
												Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtkecelakaan').show();
												Ext.getCmp('txtneoplasma').hide();
												Ext.get('cbxkecelakaan').dom.checked=true;
											}else if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==1){
												Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtneoplasma').show();
												Ext.getCmp('txtkecelakaan').hide();
												Ext.get('cbxneoplasma').dom.checked=true;
											}else{
												Ext.get('cbxlain').dom.checked=true;
												Ext.getCmp('txtkecelakaan').hide();
												Ext.getCmp('txtneoplasma').hide();
											}
										}
									}
								}
									
							}
						}
					}),
					{
						xtype: 'radiogroup',
						width:300,
						fieldLabel: 'Catatan Lain &nbsp;',
						id:'catLainGroup',
						name: 'mycbxgrp',
						columns: 3,
						items: [
							{ 
								id: 'cbxlain', 
								boxLabel: 'Lain-lain', 
								name: 'mycbxgrp', 
								width:70, 
								inputValue: 1
							},{ 
								id: 'cbxneoplasma', 
								boxLabel: 'Neoplasma', 
								name: 'mycbxgrp',  
								width:100, 
								inputValue: 2 
							},{ 
								id: 'cbxkecelakaan', 
								boxLabel: 'Kecelakaan', 
								name: 'mycbxgrp', 
								width:100, 
								inputValue: 3 
							}
					   ],
					     listeners: {
		             		change: function(radiogroup, radio){
		             			if(Ext.getDom('cbxlain').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											dsTRDetailDiagnosaList.getRange()[j].data.DETAIL='';
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=0;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxneoplasma').checked == true){
									Ext.getCmp('txtneoplasma').show();
									Ext.getCmp('txtneoplasma').setValue('');
									Ext.getCmp('txtkecelakaan').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxkecelakaan').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').show();
									Ext.getCmp('txtkecelakaan').setValue('');
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}
							}
		                }
						},{
							  xtype: 'textfield',
							  fieldLabel:'Neoplasma &nbsp;',
							  name: 'txtneoplasma',
							  id: 'txtneoplasma',
							  hidden:true,
							  width:600,
							  listeners:{
								  blur: function(){
									  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
										  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
												if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
													dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
													dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
												}
											}
									  }
									  	
								  }
							  }
					       },{
					        	xtype: 'textfield',
					        	fieldLabel:'Kecelakaan / Keracunan &nbsp;',
					        	name: 'txtkecelakaan',
					        	id: 'txtkecelakaan',
					        	hidden:true,
					        	width:600,
						    	listeners:{
									  blur: function(){
										  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
											  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
													if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
														dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
														dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
														Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													}
												}
										  }
										  	
									  }
								  }
					        }
				]
			}
		]
	};
	return items;
}

PenataJasaRJ.form.Class.diagnosa	= Ext.data.Record.create([
   {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
   {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
   {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
   {name: 'URUT', 	mapping: 'URUT'},
   {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
   {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
   {name: 'KASUS', 	mapping: 'KASUS'},
   {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
   {name: 'NOTE', 	mapping: 'NOTE'}
]);

function GetDTLTRDiagnosaGridFirst(){
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG','NOTE','DETAIL'];
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaRJ.ds2=dsTRDetailDiagnosaList;
    RefreshDataSetDiagnosa(PenataJasaRJ.s1.data.KD_PASIEN,PenataJasaRJ.s1.data.KD_UNIT,PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI);
    PenataJasaRJ.grid2 = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'tabDiagnosaGrid',
        store: PenataJasaRJ.ds2,
        border: true,
        columnLines: true,
        frame: false,
        anchor: '100% 100%',
        autoScroll:true,
		height:150,
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                        CurrentDiagnosa.row = row;
                        CurrentDiagnosa.data = cellSelecteddeskripsi;
                    }
                }
            }
        ),
        cm: TRDiagnosaColumModel(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.grid2;
}

function TRDiagnosaColumModel(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
            id			: Nci.getId(),
            header		: 'No.ICD',
            dataIndex	: 'KD_PENYAKIT',
            width		: 70,
			menuDisabled: true,
            hidden		: false
        },{
            id			: Nci.getId(),
            header		: 'Penyakit',
            dataIndex	: 'PENYAKIT',
			menuDisabled: true,
			width		: 200,
			editor		: PenataJasaRJ.form.ComboBox.penyakit= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.penyakit,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.grid2.getSelectionModel().selection.cell[0];
    				PenataJasaRJ.ds2.getRange()[line].data.KD_PENYAKIT=b.data.kd_penyakit;
    				PenataJasaRJ.ds2.getRange()[line].data.PENYAKIT=b.data.penyakit;
    				PenataJasaRJ.grid2.getView().refresh();
    				dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
				},
				insert	: function(o){
					return {
						kd_penyakit        	:o.kd_penyakit,
						penyakit 			: o.penyakit,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="200">'+o.penyakit+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getPenyakit",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 250
			})
        },{
            id: Nci.getId(),
            header: 'kd_pasien',
            dataIndex: 'KD_PASIEN',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut',
            dataIndex: 'URUT',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut masuk',
            dataIndex: 'URUT_MASUK',
			hidden:true
            
        },{
            id: Nci.getId(),
            header: 'tgl masuk',
            dataIndex: 'TGL_MASUK',
			hidden:true
        },{
            id			: Nci.getId(),
            header		: 'Diagnosa',
            width		: 130,
			menuDisabled: true,
            dataIndex	: 'STAT_DIAG',
            editor		: new Ext.form.ComboBox ( {
				id				: Nci.getId(),
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	:['Id','displayText'],
					data	: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id: 'colKasusDiagnosa',
            header: 'Kasus',
            width:130,
			menuDisabled:true,
            dataIndex: 'KASUS',
            editor: new Ext.form.ComboBox({
				id				: 'cboKasus',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	: ['Id','displayText'],
					data	: [[1, 'Baru'],[2, 'Lama']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id			: 'colNote',
            header		: 'Note',
            dataIndex	: 'NOTE',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        },{
            id			: 'colKdProduk',
            header		: 'Detail',
            dataIndex	: 'DETAIL',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        }
    ]);
}

function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',
        items:[
	        {
	            x:0,
	            y:60,
	            xtype: 'textarea',
	            id:'TxtHistoriDeleteDataPasien',
	            hideLabel: true,
	            name: 'msg',
	            anchor: '100% 100%' 
	        }
        ]
    });
}

function TambahBarisRWJ(){
    var x=true;
    if (x === true){
        var p = RecordBaruRWJ();
        dsTRDetailKasirRWJList.insert(dsTRDetailKasirRWJList.getCount(), p);
    }
}

function HapusBarisRWJ(){
    if ( cellSelecteddeskripsi != undefined ){
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != ''){
          	var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok'){
					variablehistori=combo;
					DataDeleteKasirRWJDetail();
					dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
				}
			});
               
        }else{
            dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        }
    }
}

function DataDeleteKasirRWJDetail(){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:  getParamDataDeleteKasirRWJDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                cellSelecteddeskripsi=undefined;
                RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }else if (cst.success === false && cst.produktr  === true ){
                ShowPesanWarningRWJ('Produk Transfer Tidak dapat dihapus', nmHeaderHapusData);
				 RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					AddNewKasirRWJ = false;
            }else{
                ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
				 RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }
        }
    });
}

function getParamDataDeleteKasirRWJDetail(){
    var params ={
		Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirRWJ.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirRWJ.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitRWJ').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirRWJ.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirRWJ.data.data.HARGA,
		TrKdProduk :	 CurrentKasirRWJ.data.data.KD_PRODUK,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Hapus:2
    }
    return params;
}

function getParamDataupdateKasirRWJDetail(){
    var params ={
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Qty: CurrentKasirRWJ.data.data.QTY,
        Ubah:1
    };
    return params;
}

function GetDTLTRRWJGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Transaksi',
		id:'tabTransaksi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetDTLTRRWJGridSecond(data),GetDTLTRRWJGridFirst(data)]
    });
	return tabTransaksi;
}

function GetDTLTRRWJGridFirst(data){
	var fldDetail = ['kd_prd','nama_obat','jumlah','satuan','cara_pakai','kd_dokter','nama','verified','racikan'];
	dsPjTrans2 = new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetai2(data) ;
    var gridDTLTRRWJ = new Ext.grid.EditorGridPanel({
        title: 'Terapi Obat',
		id:'PjTransGrid1',
        stripeRows: true,
        height: 170,
        store: dsPjTrans2,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
                    CurrentKasirRWJ.row = row;
                    CurrentKasirRWJ.data = cellSelecteddeskripsi;
                }
            }
        }),
        cm: TRRawatJalanColumModel(),
        viewConfig:{forceFit: true},
        tbar:[
			{
				text: 'Tambah Obat',
				id: Nci.getId(),
				iconCls: 'find',
				handler: function(){
					PenataJasaRJ.dsGridObat.insert(PenataJasaRJ.dsGridObat.getCount(),PenataJasaRJ.nullGridObat());
				}
			},{
				text: 'Hapus',
				id: Nci.getId(),
				iconCls: 'RemoveRow',
				handler: function(){
					Ext.Msg.show({
	                   title:nmHapusBaris,
	                   msg: 'Anda yakin akan menghapus data Obat ini?',
	                   buttons: Ext.MessageBox.YESNO,
	                   fn: function (btn)
	                   {
	                       if (btn =='yes')
	                        {
	                    	   PenataJasaRJ.dsGridObat.removeAt(PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0]);
								PenataJasaRJ.gridObat.getView().refresh();
	                        }
	                   },
	                   icon: Ext.MessageBox.QUESTION
	                });
				}
			}
        ]}
    );
    PenataJasaRJ.gridObat=gridDTLTRRWJ;
    PenataJasaRJ.dsGridObat=dsPjTrans2;
    return gridDTLTRRWJ;
}

function GetDTLTRRWJGridSecond(){
	var fldDetail	= ['KD_PRODUK','DESKRIPSI','QTY','DOKTER','TGL_TINDAKAN','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','KD_TARIF','HARGA'];
	dsTRDetailKasirRWJList	= new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetail() ;
    PenataJasaRJ.dsGridTindakan	= dsTRDetailKasirRWJList;
    PenataJasaRJ.form.Grid.produk	= new Ext.grid.EditorGridPanel({
        title		: 'Tidakan Yang Diberikan',
		id			: 'PjTransGrid2',
		stripeRows	: true,
		height		: 130,
        store		: PenataJasaRJ.dsGridTindakan,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        sm			: new Ext.grid.CellSelectionModel({
	        singleSelect: true,
	        listeners	: {
	            cellselect	: function(sm, row, rec){
	                cellSelecteddeskripsi	= dsTRDetailKasirRWJList.getAt(row);
	                CurrentKasirRWJ.row	= row;
	                CurrentKasirRWJ.data	= cellSelecteddeskripsi;
	            }
	        }
        }),
        cm			: TRRawatJalanColumModel2(),
        viewConfig	: {forceFit: true}
    });
    
    return PenataJasaRJ.form.Grid.produk;
};

function TRRawatJalanColumModel(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: 'PjTransGrid1Code',
        	header		: 'KD.Obat',
            dataIndex	: 'kd_prd',
            width		: 80,
			menuDisabled: true,
            hidden		: false
        },{
			id			: 'PjTransGrid1Tin',
        	header		: 'Nama Obat',
            dataIndex	: 'nama_obat',
            width		: 150,
			menuDisabled: true,
            hidden		: false,
            editor		: PenataJasaRJ.comboObat()
        },{
            id			: 'PjTransGrid1Qty',
            header		: 'Qty',
            dataIndex	: 'jumlah',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
            width		: 50,
            editor		: new Ext.form.NumberField({
				id				: 'txtQty',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: 'PjTransGrid1Satuan',
            header		: 'Satuan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'satuan'	
        },{
            id			: 'PjTransGrid1CaraPakai',
            header		: 'Cara Pakai',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'cara_pakai'	,
        	editor		: new Ext.form.TextField({
				id				: 'txtcarapakai',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: 'PjTransGrid1Dokter',
            header		: 'Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'nama'	
        },{
            id			: 'PjTransGrid1Verified',
            header		: 'Verified',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'verified',
            editor		: PenataJasaRJ.ComboVerifiedObat()
        },{
            id			: 'PjTransGrid1Racikan',
            header		: 'Racikan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'racikan',
            editor		: new Ext.form.NumberField({
				id				: 'txtRacikan',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        }
    ]);
}

function TRRawatJalanColumModel2(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
        	 id				: 'coleskripsirwj2',
        	 header			: 'Uraian',
        	 dataIndex		: 'DESKRIPSI2',
        	 menuDisabled	: true,
        	 hidden 		: true
        },{
        	id				: 'colKdProduk2',
            header			: 'Kode Produk',
            dataIndex		: 'KD_PRODUK',
            width			: 100,
			menuDisabled	: true,
            hidden			: true
        },{
        	id			: 'colDeskripsiRWJ2',
            header		:'Nama Produk',
            dataIndex	: 'DESKRIPSI',
            sortable	: false,
            hidden		:false,
			menuDisabled:true,
            width		:250,
            editor		: PenataJasaRJ.form.ComboBox.produk= Nci.form.Combobox.autoComplete({
				store	: PenataJasaRJ.form.DataStore.produk,
				select	: function(a,b,c){
					var line	= PenataJasaRJ.form.Grid.produk.getSelectionModel().selection.cell[0];
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_PRODUK=b.data.kd_produk;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.DESKRIPSI=b.data.deskripsi;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.KD_TARIF='TU';
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.HARGA=b.data.harga;
    				PenataJasaRJ.dsGridTindakan.getRange()[line].data.TGL_BERLAKU=b.data.tglberlaku;
    				PenataJasaRJ.form.Grid.produk.getView().refresh();
				},
				param	: function(){
					var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit']=o.KD_UNIT;
					params['kd_customer']=o.KD_CUSTOMER;
					return params;
				},
				insert	: function(o){
					return {
						kd_produk        :o.kd_produk,
						deskripsi 		: o.deskripsi,
						harga			: o.tarifx,
						tglberlaku		: o.tglberlaku
				    }
				},
				url		: baseURL + "index.php/main/functionRWJ/getProduk",
				valueField: 'deskripsi'
			})
	        },{
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width:100,
		   		menuDisabled:true,
               renderer: function(v, params, record){
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },{
                id: 'colHARGARWj2',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
				renderer: function(v, params, record){
					return formatCurrency(record.data.HARGA);
				}	
            },{
                id: 'colProblemRWJ2',
                header: 'Qty',
               width:'100%',
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField({
                    id:'fieldcolProblemRWJ2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:100,
					listeners:{ 
						'specialkey' : function(){
							Dataupdate_KasirRWJ(false);
						}
					}
                })
            },{
                id: 'colImpactRWJ2',
                header: 'CR',
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField({
                    id:'fieldcolImpactRWJ2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30
                })
            }
        ]
    );
};

function GetLookupAssetCMRWJ(str){
	if (AddNewKasirRWJ === true){
		var p = new mRecordRwj({
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':''
		});
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,true,'',false);
	}else{	
		var p = new mRecordRwj({
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
			'DESKRIPSI':'', 
			'KD_TARIF':'', 
			'HARGA':'',
			'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
			'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
			'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
			'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
			'URUT':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT
		});
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,false,CurrentKasirRWJ.row,false);
	};
};

function RecordBaruRWJ(){
	var p = new mRecordRwj({
		'DESKRIPSI2':'',
		'KD_PRODUK':'',
	    'DESKRIPSI':'', 
	    'KD_TARIF':'', 
	    'HARGA':'',
	    'QTY':'',
	    'TGL_TRANSAKSI':tanggaltransaksitampung, 
	    'DESC_REQ':'',
	    'KD_TARIF':'',
	    'URUT':''
	});
	return p;
}

function RefreshDataSetDiagnosa(medrec,unit,tgl){	
 	var strKriteriaDiagnosa='';
    strKriteriaDiagnosa = 'A.kd_pasien = ~' + medrec + '~ and A.kd_unit=~'+unit+'~ and A.tgl_masuk in(~'+tgl+'~)';
	dsTRDetailDiagnosaList.load({ 
		params:{ 
			Skip: 0, 
			Take: selectCountDiagnosa, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewDiagnosaRJPJ',
			param: strKriteriaDiagnosa
		} 
	});
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
}

function RefreshDataSetAnamnese(){	
 	var strKriteriaDiagnosa='';
   strKriteriaDiagnosa = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksi').getValue() + '~ and mr_konpas.kd_unit=~'+Ext.get('txtKdUnitRWJ').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi').dom.value+'~';
	dsTRDetailAnamneseList.load({ 
		params:{ 
			Skip: 0, 
			Take: 50, 
            Sort: 'orderlist',
			Sortdir: 'ASC', 
			target:'viewkondisifisik',
			param: strKriteriaDiagnosa
		} 
	});
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
}

function TRRWJInit(rowdata){
    AddNewKasirRWJ = false;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitRWJ').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
	Ext.get('txtareaAnamnesis').dom.value = rowdata.ANAMNESE;
	Ext.get('txtareaAnamnesiscatatan').dom.value = rowdata.CAT_FISIK;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	RefreshDataKasirRWJDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetAnamnese();
	RefreshDataSetRadiologi();
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request({
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0'
	    },
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText;
	    }
	});
}

function mEnabledRWJCM(mBol){
	 Ext.get('btnLookupRWJ').dom.disabled=mBol;
	 Ext.get('btnHpsBrsRWJ').dom.disabled=mBol;
}

function RWJAddNew() {
    AddNewKasirRWJ = true;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirRwj').dom.value= '';
	rowSelectedKasirRWJ=undefined;
	dsTRDetailKasirRWJList.removeAll();
	mEnabledRWJCM(false);
	

}

function RefreshDataKasirRWJDetai2(data){
    dsPjTrans2.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'kd_obat',
		    Sortdir: 'ASC',
		    target: 'ViewResepRWJ',
		    param: "KD_PASIEN='"+data.KD_PASIEN+"' AND KD_UNIT = '"+data.KD_UNIT+"' AND TGL_MASUK = '"+data.TANGGAL_TRANSAKSI+"'"
		}
	});
    return dsPjTrans2;
}

function RefreshDataKasirRWJDetail(no_transaksi) {
    var strKriteriaRWJ='';
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~01~";
    dsTRDetailKasirRWJList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewDetailRWJGridBawah',
		    param: strKriteriaRWJ
		}
	});
    return dsTRDetailKasirRWJList;
}

function getParamDetailTransaksiRWJ(){
    var params={
		Table			: 'ViewTrKasirRwj',
		TrKodeTranskasi	: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit			: Ext.get('txtKdUnitRWJ').getValue(),
		Tgl				: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
		Shift			: tampungshiftsekarang,
		List			: getArrDetailTrRWJ(),
		JmlField		: mRecordRwj.prototype.fields.length-4,
		JmlList			: GetListCountDetailTransaksi(),
		Hapus			: 1,
		Ubah			: 0
	};
    params.jmlObat	= PenataJasaRJ.dsGridObat.getRange().length;
    params.urut_masuk	= PenataJasaRJ.s1.data.URUT_MASUK;
    params.kd_pasien	= PenataJasaRJ.s1.data.KD_PASIEN;
    for(var i=0, iLen= params.jmlObat ;i<iLen;i++){
    	var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
    	params['jumlah'+i]	= o.jumlah;
    	params['cara_pakai'+i]	= o.cara_pakai;
    	params['verified'+i]	= o.verified;
    	params['racikan'+i]	= o.racikan;
    	params['kd_dokter'+i]	= o.kd_dokter;
    }
    return params;
}

function getParamDetailAnamnese() {
    var params ={
		Table:'viewkondisifisik',
		KdPasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk : Ext.get('txtKdUrutMasuk').getValue(),
		Anamnese:Ext.get('txtareaAnamnesis').getValue(),
		Catatan:Ext.get('txtareaAnamnesiscatatan').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		List:getArrdetailAnamnese()
	};
    return params;
}

function getParamKonsultasi() {
    var params ={
		Table:'ViewTrKasirRwj', 
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRWJ').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer
	};
    return params;
}

function GetListCountDetailTransaksi(){
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++){
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI  != ''){
			x += 1;
		}
	}
	return x;
}

function getTotalDetailProduk(){
	var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++){		
		var recordterakhir;
		var y='';
		var z='@@##$$@@';
		recordterakhir=dsTRDetailKasirRWJList.data.items[i].data.DESC_REQ;
		TotalProduk=TotalProduk+recordterakhir;
		Ext.get('txtJumlah1EditData_viKasirRwj').dom.value=formatCurrency(TotalProduk);
		if (i === (dsTRDetailKasirRWJList.getCount()-1)){
			x += y ;
		}else{
			x += y + '##[[]]##';
		}
	}	
	return x;
}

function getArrDetailTrRWJ(){
	var x='';
	for(var i = 0 ; i < PenataJasaRJ.dsGridTindakan.getCount();i++){
		if (PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK != '' && PenataJasaRJ.dsGridTindakan.data.items[i].data.DESKRIPSI != ''){
			var y='';
			var z='@@##$$@@';
			y = 'URUT=' + PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
			y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_PRODUK;
			y += z + PenataJasaRJ.dsGridTindakan.data.items[i].data.QTY;
			y += z + ShowDate(PenataJasaRJ.dsGridTindakan.data.items[i].data.TGL_BERLAKU);
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.HARGA;
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.KD_TARIF;
			y += z +PenataJasaRJ.dsGridTindakan.data.items[i].data.URUT;
			if (i === (PenataJasaRJ.dsGridTindakan.getCount()-1)){
				x += y ;
			}else{
				x += y + '##[[]]##';
			}
		}
	}	
	return x;
}

function getItemPanelInputRWJ(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:[
					getItemPanelNoTransksiRWJ(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
}

function getItemPanelUnit(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitRWJ',
					    id: 'txtKdUnitRWJ',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelDokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners:{ 
							
						}
					}
				]
			},{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						readOnly:true,
					    anchor: '100%'
					},{
						xtype: 'textfield',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelNoTransksiRWJ(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirrwj',
					    id: 'txtNoTransaksiKasirrwj',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPaneltgl_filter(){
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterRWJ',
					    name: 'dtpTglAwalFilterRWJ',
					    value: now,
					    anchor: '99%',
						format: 'd/M/Y',
						altFormats: 'dmy',
					    listeners:{
						 	'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 ){
									RefreshDataFilterKasirRWJ();
								}
							}
						}
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[ 
					{
					    xtype: 'datefield',
					    id: 'dtpTglAkhirFilterRWJ',
					    name: 'dtpTglAkhirFilterRWJ',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '99%',
					   	listeners:{
						 	'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
									RefreshDataFilterKasirRWJ();
								}
							}
						}
					}
				]
			}
		]
	};
    return items;
};
function getItemcombo_filter() {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboStatusBayar_viKasirRwj()
				]
			},{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:[ 
					mComboUnit_viKasirRwj()
				]
			}
		]
	};
    return items;
}

function getItemPanelmedrec(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	};
    return items;
};


function RefreshDataKasirRWJ() {
    dsTRKasirRWJList.load({
        params:{
            Skip: 0,
            Take: selectCountKasirRWJ,
            Sort: 'tgl_transaksi',
            Sortdir: 'ASC',
            target:'ViewTrKasirRwj',
            param : ''
        }		
    });
    rowSelectedKasirRWJ = undefined;
    return dsTRKasirRWJList;
}

function refeshkasirrwj(){
	dsTRKasirRWJList.load({ 
		params:{   
			Skip: 0, 
			Take: selectCountKasirRWJ, 
             Sort: '',
			Sortdir: 'ASC', 
			target:'ViewTrKasirRwj',
			param : ''
		}			
	});   
	return dsTRKasirRWJList;
}

function RefreshDataFilterKasirRWJ() {
	var KataKunci='';
 	if (Ext.get('txtFilterNomedrec').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}
	}
 	if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		}
	}
 	if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'All'){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_unit)like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_unit) like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != ''){
		if (KataKunci == ''){
            KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}else{
            KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = TRUE';
		}else{
           	KataKunci += ' and posting_transaksi =  TRUE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Posting'){
		if (KataKunci == ''){
            KataKunci = ' and  posting_transaksi = FALSE';
		}else{
            KataKunci += ' and posting_transaksi =  FALSE';
		}
	}
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Semua'){
		if (KataKunci == ''){
            KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}else{
            KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
	}
	if (Ext.get('dtpTglAwalFilterRWJ').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}else{
            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}
	}
    if (KataKunci != undefined ||KataKunci != '' ){  
		dsTRKasirRWJList.load({ 
			params:{   
				Skip: 0, 
				Take: selectCountKasirRWJ, 
                Sort: 'tgl_transaksi',
				Sortdir: 'ASC', 
				target:'ViewTrKasirRwj',
				param : KataKunci
			}			
		});   
    }else{
		dsTRKasirRWJList.load({ 
			params:{   
				Skip: 0, 
				Take: selectCountKasirRWJ, 
                Sort: 'tgl_transaksi',
				Sortdir: 'ASC', 
				target:'ViewTrKasirRwj',
				param : ''
			}			
		});   
	}
	return dsTRKasirRWJList;
}

function Datasave_Konsultasi(mBol,callback) {	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionRWJ/KonsultasiPenataJasa",					
			params: getParamKonsultasi(),
			failure: function(o){
				ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					if(callback != undefined)callback(cst.id);
					ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else {
					ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function Datasave_KasirRWJ(mBol){	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url			: baseURL + "index.php/main/functionRWJ/savedetailpenyakit",
			params		: getParamDetailTransaksiRWJ(),
			failure		: function(o){
				ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},
			success		: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
					//RefreshDataFilterKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else{
					ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function Dataupdate_KasirRWJ(mBol) {	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/CreateDataObj",
			params: getParamDataupdateKasirRWJDetail(),
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
					RefreshDataKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else if  (cst.success === false && cst.pesan===0){
					ShowPesanWarningRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
				}else if (cst.success === false && cst.pesan===1){
					ShowPesanWarningRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
				}else {
					ShowPesanErrorRWJ(nmPesanSimpanError,nmHeaderSimpanData);
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function ValidasiEntryCMRWJ(modul,mBolHapus){
	var x = 1;
	if((Ext.get('txtNoTransaksiKasirrwj').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirRWJList.getCount() === 0 || (Ext.get('txtKdDokter').dom.value  === undefined )){
		if (Ext.get('txtNoTransaksiKasirrwj').getValue() == '' && mBolHapus === true){
			x = 0;
		}else if (Ext.get('txtNoMedrecDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}else if (Ext.get('txtNamaPasienDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}else if (Ext.get('dtpTanggalDetransaksi').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokter').dom.value  === undefined){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}else if (dsTRDetailKasirRWJList.getCount() === 0){
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		}
	}
	return x;
}

function ValidasiEntryKonsultasi(modul,mBolHapus){
	var x = 1;
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') ){
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true){
			x = 0;
		}else if (Ext.get('cboDokterRequestEntry').getValue() == ''){
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	}
	return x;
}

function ShowPesanWarningRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.WARNING,
		width:250
	});
}

PenataJasaRJ.alertError	= function(str, modul){
	Ext.MessageBox.show({
	    title	: modul,
	    msg		: str,
	    buttons	: Ext.MessageBox.OK,
	    icon	: Ext.MessageBox.ERROR,
		width	: 250
	});
};

function ShowPesanErrorRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.ERROR,
		width:250
	});
}

function ShowPesanInfoRWJ(str, modul) {
    Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
}

function DataDeleteKasirRWJ(){
   if (ValidasiEntryCMRWJ(nmHeaderHapusData,true) == 1 ){
        Ext.Msg.show({
           title:nmHeaderHapusData,
           msg: nmGetValidasiHapus(nmTitleFormRequest) ,
           buttons: Ext.MessageBox.YESNO,
           width:275,
           fn: function (btn){
                if (btn =='yes'){
                    Ext.Ajax.request({
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDetailTransaksiRWJ(),
                        success: function(o){
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                                RefreshDataKasirRWJ();
                                RWJAddNew();
                            }else if (cst.success === false && cst.pesan===0){
                                ShowPesanWarningRWJ(nmPesanHapusGagal,nmHeaderHapusData);
                            }else if (cst.success === false && cst.pesan===1){
                                ShowPesanWarningRWJ(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                            }else{
                                ShowPesanErrorRWJ(nmPesanHapusError,nmHeaderHapusData);
                            }
                        }
                    });
                }
            }
        });
    }
}

function GantiDokterLookUp(mod_id) {
    var FormDepanDokter = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Ganti Dokter',
        border: false,
        shadhow: true,
        autoScroll:false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [DokterLookUp()],
        listeners:{
            'afterrender': function()
            {}
        }
    });
   	return FormDepanDokter;
}

function DokterLookUp(rowdata) {
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window({
        id: 'gridDokter',
        title: 'Ganti Dokter',
        closeAction: 'destroy',
        width: lebar,
        height: 180,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: getFormEntryDokter(lebar),
        listeners:{
            activate: function(){
                 if (varBtnOkLookupEmp === true){
                    Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                    Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                    varBtnOkLookupEmp=false;
                }
            },afterShow: function(){
                this.activate();
            },deactivate: function(){
                rowSelectedKasirDokter=undefined;
                // RefreshDataFilterKasirDokter();
            }
        }
    });
    FormLookUpGantidokter.show();
}

function getItemPanelButtonGantidokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig:{
					align: 'middle',
					pack:'end'
				},
				items:[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function(){
							GantiDokter(false);
							FormLookUpGantidokter.close();	
						}
					},{
						xtype:'button',
						text:'Tutup' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelGantidokter',
						handler:function(){
							FormLookUpGantidokter.close();
						}
					}
				]
			}
		]
	};
    return items;
}

function getFormEntryDokter(lebar) {
    var pnlTRGantiDokter = new Ext.FormPanel({
        id: 'PanelTRDokter',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        bodyStyle: 'padding:10px 10px 10px 10px',
        height:190,
        anchor: '100%',
        width: lebar,
        border: false,
        items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
       	tbar:[]
    });
    var FormDepanDokter = new Ext.Panel({
	    id: 'FormDepanDokter',
	    region: 'center',
	    width: '100%',
	    anchor: '100%',
	    layout: 'form',
	    title: '',
	    bodyStyle: 'padding:15px',
	    border: true,
	    bodyStyle: 'background:#FFFFFF;',
	    shadhow: true,
	    items: [pnlTRGantiDokter]

	});
    return FormDepanDokter;
};

function getItemPanelInputGantidokter(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:[getItemPanelNoTransksiDokter(lebar)]
			}
		]
	};
    return items;
}

function ValidasiEntryTutupDokter(modul,mBolHapus){
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == '')){
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true){
			x=0;
		}else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === ''){
			x=0;
			if ( mBolHapus === false ){
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			}
		}
	}
	return x;
}

function getItemPanelNoTransksiDokter(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
}

function mComboDokterGantiEntry(){ 
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitRWJ').getValue();
	var kddokter = Ext.get('txtKdDokter').getValue();
   	dsDokterGantiEntry.load({
     	params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokter',
			param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
		}
    });
    var cboDokterGantiEntry = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterGantiEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				selectDokter = b.data.KD_DOKTER;
				NamaDokter = b.data.NAMA;
				Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER;
            },
            'render': function(c){
                c.getEl().on('keypress', function(e) {
                if(e.getKey() == 13)
                	Ext.getCmp('kelPasien').focus();
                }, c);
            }
		}
    });
    return cboDokterGantiEntry;
};

function GantiDokter(mBol){
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 ){
        Ext.Ajax.request({
           	url: WebAppUrl.UrlUpdateData,
        	params: getParamGantiDokter(),
			failure: function(o){
				 ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
			},	
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    ShowPesanInfoRWJ(nmPesanSimpanSukses,'Ganti Dokter');
					Ext.get('txtKdDokter').dom.value = selectDokter;
					Ext.get('txtNamaDokter').dom.value = NamaDokter;
					FormDepanDokter.close();
                    FormLookUpGantidokter.close();
                }else if  (cst.success === false && cst.pesan===0){
                        ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                }else{
                        ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                }
            }
        });
    }else{
        if(mBol === true){
                return false;
        }
    }
}

function ValidasiGantiDokter(modul,mBolHapus){
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == '')){
	  	if (Ext.get('cboDokterRequestEntry').getValue() === ''){
			x=0;
			if ( mBolHapus === false ){
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			}
		}
	}
	return x;
}

function getParamGantiDokter(){
    var params ={
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
	};
    return params;
};

function KelompokPasienLookUp(rowdata) {
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window({
        id: 'gridKelompokPasien',
        title: 'Ganti Kelompok Pasien',
        closeAction: 'destroy',
        width: lebar,
        height: 260,
        border: false,
        resizable: false,
        plain: false,
        constrain: true,
        layout: 'fit',
        iconCls: 'Request',
        modal: true,
        items: getFormEntryTRKelompokPasien(lebar),
        listeners:{
            
        }
    });
    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();
}

function getFormEntryTRKelompokPasien(lebar) {
    var pnlTRKelompokPasien = new Ext.FormPanel({
		id: 'PanelTRKelompokPasien',
		fileUpload: true,
		region: 'north',
		layout: 'column',
		bodyStyle: 'padding:10px 10px 10px 10px',
		height:250,
		anchor: '100%',
	    width: lebar,
	    border: false,
	    items: [getItemPanelInputKelompokPasien(lebar),getItemPanelButtonKelompokPasien(lebar)],
	   	tbar:[]
	});
    var FormDepanKelompokPasien = new Ext.Panel({
	    id: 'FormDepanKelompokPasien',
	    region: 'center',
	    width: '100%',
	    anchor: '100%',
	    layout: 'form',
	    title: '',
	    bodyStyle: 'padding:15px',
	    border: true,
	    bodyStyle: 'background:#FFFFFF;',
	    shadhow: true,
	    items: [pnlTRKelompokPasien	]
	});
    return FormDepanKelompokPasien;
}

function getItemPanelInputKelompokPasien(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	
					
				]
			}
		]
	};
    return items;
}

function getItemPanelButtonKelompokPasien(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: {
					align: 'middle',
					pack:'end'
				},
				items:[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkKelompokPasien',
						handler:function(){
							Datasave_Kelompokpasien();
							FormLookUpsdetailTRKelompokPasien.close();
						}
					},{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelKelompokPasien',
						handler:function(){
							FormLookUpsdetailTRKelompokPasien.close();
						}
					}
				]
			}
		]
	};
    return items;
}

function KelompokPasienbaru() {
	jeniscus=0;
    KelompokPasienAddNew = true;
    Ext.getCmp('cboKelompokpasien').show();
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value;
}

function RefreshDatacombo(jeniscus) {
    ds_customer_viDaftar.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: '',
            Sortdir: '',
            target:'ViewComboKontrakCustomer',
            param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
        }
    });
    return ds_customer_viDaftar;
}

function mComboKelompokpasien(){
	var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];
    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	ds_customer_viDaftar.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: '',
            Sortdir: '',
            target:'ViewComboKontrakCustomer',
            param: 'jenis_cust=~'+ jeniscus +'~'
        }
    });
    var cboKelompokpasien = new Ext.form.ComboBox({
		id:'cboKelompokpasien',
		typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih...',
        fieldLabel: labelisi,
        align: 'Right',
        anchor: '95%',
		store: ds_customer_viDaftar,
		valueField: 'KD_CUSTOMER',
        displayField: 'CUSTOMER',
		listeners:{
			'select': function(a,b,c){
				selectSetKelompokpasien=b.data.displayText ;
				selectKdCustomer=b.data.KD_CUSTOMER;
				selectNamaCustomer=b.data.CUSTOMER;
			
			}
		}
	});
	return cboKelompokpasien;
}

function getKelompokpasienlama(lebar) {
    var items ={
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:[
					{	 
						xtype: 'tbspacer',
						height: 2
					},{
	                    xtype: 'textfield',
	                    fieldLabel: 'Kelompok Pasien Asal',
	                    name: 'txtCustomerLama',
	                    id: 'txtCustomerLama',
						labelWidth:130,
	                    width: 100,
	                    anchor: '95%'
	                 }
				]
			}
		]
	};
    return items;
}

function getItemPanelNoTransksiKelompokPasien(lebar) {
    var items ={
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:[
					{	 
						xtype: 'tbspacer',
						height:3
					},{  
	                    xtype: 'combo',
	                    fieldLabel: 'Kelompok Pasien Baru',
	                    id: 'kelPasien',
	                     editable: false,
	                    store: new Ext.data.ArrayStore({
	                        id: 0,
	                        fields:['Id','displayText'],
	                      	data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
	                    }),
					  	displayField: 'displayText',
					  	mode: 'local',
					  	width: 100,
					  	triggerAction: 'all',
					  	emptyText: 'Pilih Salah Satu...',
					  	selectOnFocus: true,
					  	anchor: '95%',
					  	listeners:{
							'select': function(a, b, c){
								if(b.data.displayText =='Perseorangan'){
									jeniscus='0';
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();
								}else if(b.data.displayText =='Perusahaan'){
									jeniscus='1';
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();
								}else if(b.data.displayText =='Asuransi'){
									jeniscus='2';
									Ext.getCmp('txtNoSJP').enable();
									Ext.getCmp('txtNoAskes').enable();
								}
								RefreshDatacombo(jeniscus);
							}
						}
                  	},{
						columnWidth: .990,
						layout: 'form',
						border: false,
						labelWidth:130,
						items:[
							mComboKelompokpasien()
						]
					},{
                        xtype: 'textfield',
                        fieldLabel: 'No. SJP',
                        maxLength: 200,
                        name: 'txtNoSJP',
                        id: 'txtNoSJP',
                        width: 100,
                        anchor: '95%'
                 	},{
	                    xtype: 'textfield',
	                    fieldLabel: 'No. Askes',
	                    maxLength: 200,
	                    name: 'txtNoAskes',
	                    id: 'txtNoAskes',
	                    width: 100,
	                    anchor: '95%'
	                }
				]
			}
		]
	};
    return items;
}

function Datasave_Kelompokpasien(mBol) {	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request ({
			url: baseURL +  "index.php/main/functionRWJ/UpdateKdCustomer",	
			params: getParamKelompokpasien(),
			failure: function(o){
				ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},	
			success: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				Ext.get('txtCustomer').dom.value = selectNamaCustomer;
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
					RefreshDataKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else{
						ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
}

function getParamKelompokpasien(){
    var params ={
		Table:'ViewTrKasirRwj', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter:Ext.get('txtKdDokter').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
	};
    return params;
}

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus){
	var x = 1;
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined )||(Ext.get('cboKelompokpasien').getValue() == '') || (Ext.get('cboKelompokpasien').dom.value  === undefined )){
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) {
			ShowPesanWarningRWJ(nmGetValidasiKosong('Kelompok pasien'), modul);
			x = 0;
		}
		if (Ext.get('cboKelompokpasien').getValue() == '' && mBolHapus === true) {
			ShowPesanWarningRWJ(nmGetValidasiKosong('Combo kelompok pasien'), modul);
			x = 0;
		}
	}
	return x;
}

function setpostingtransaksi(notransaksi) {
	Ext.Msg.show({
	   title:'Posting',
	   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
	   buttons: Ext.MessageBox.YESNO,
	   width:250,
	   fn: function (btn){			
			if (btn === 'yes'){
				Ext.Ajax.request({
					url : baseURL + "index.php/main/posting",
					params: {
					_notransaksi : 	notransaksi
					},
					success: function(o){
						var cst = Ext.decode(o.responseText);
						if (cst.success === true){
							//RefreshDataFilterKasirRWJ();
							ShowPesanInfoDiagnosa('Posting Berhasil Dilakukan','Posting');
							FormLookUpsdetailTRrwj.close();
						}else if (cst.success === false && cst.pesan===0){
							ShowPesanWarningDiagnosa(nmPesanHapusGagal,'Posting');
						}else{
							ShowPesanWarningDiagnosa(nmPesanHapusError,'Posting');
						}
					}
				});
			}
		}
	});
}

function setdisablebutton(){
	Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').disable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
	Ext.getCmp('btngantipasien').disable();
	Ext.getCmp('btnposting').disable();	
	Ext.getCmp('btnLookupRWJ').disable();
	Ext.getCmp('btnSimpanRWJ').disable();
	Ext.getCmp('btnHpsBrsRWJ').disable();
	Ext.getCmp('btnHpsBrsDiagnosa').disable();
	Ext.getCmp('btnSimpanDiagnosa').disable();
	Ext.getCmp('btnLookupDiagnosa').disable();
}

function setenablebutton(){
	Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').enable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
	Ext.getCmp('btngantipasien').enable();
	Ext.getCmp('btnposting').enable();	
	
	Ext.getCmp('btnLookupRWJ').enable();
	Ext.getCmp('btnSimpanRWJ').enable();
	Ext.getCmp('btnHpsBrsRWJ').enable();
	Ext.getCmp('btnHpsBrsDiagnosa').enable();
	Ext.getCmp('btnSimpanDiagnosa').enable();
	Ext.getCmp('btnLookupDiagnosa').enable();	
}

var mRecordRad = Ext.data.Record.create([
   {name: 'ID_RADKONSUL', mapping:'ID_RADKONSUL'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'KD_KLAS', mapping:'KD_KLAS'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_DOKTER', mapping:'KD_DOKTER'}
]);

function TambahBarisRad(){
    var x=true;
    if (x === true) {
        var p = RecordBaruRad();
        dsRwJPJLab.insert(dsRwJPJLab.getCount(), p);
    }
}

function RecordBaruRad(){
	var p = new mRecordRad({
		'ID_RADKONSUL':'',
		'KD_PRODUK':'',
	    'KD_KLAS':'', 
	    'KD_TARIF':'', 
	    'DESKRIPSI':'',
	    'KD_DOKTER':''
	});
	return p;
};

function radData(){
	dsrad = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','DESKRIPSI','KD_DOKTER'] });
	dsrad.load({ 
		params: { 
			Skip: 0, 
			Take: 50, 
			target:'ViewProdukRad',
			param: ''
		} 
	});
	return dsrad;
}

function getRadtest(){
	var radCombobox = new Ext.form.ComboBox({
	   	id: 'cboRadRequest',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		fieldLabel: 'Kode Test',
		align: 'Right',
		store: radData(),
		valueField: 'KD_PRODUK',
		displayField: 'KD_PRODUK',
		anchor: '95%',
		listeners:{
			'select': function(a, b, c){
				dsRwJPJLab.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
				dsRwJPJLab.data.items[CurrentRad.row].data.DESKRIPSI = b.data.DESKRIPSI;
				dsRwJPJLab.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
		 	}		
		}
	});
	return radCombobox;
}

function getRadDesk(){
	var radComboboxDesk = new Ext.form.ComboBox({
	   id: 'cboRadRequestDesk',
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   fieldLabel: 'Kode Test',
	   align: 'Right',
	   store: radData(),
	   valueField: 'DESKRIPSI',
	   displayField: 'DESKRIPSI',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			   dsRwJPJLab.data.items[CurrentRad.row].data.KD_PRODUK = b.data.KD_PRODUK;
			   dsRwJPJLab.data.items[CurrentRad.row].data.KD_KLAS = b.data.KD_KLAS;
			   dsRwJPJLab.data.items[CurrentRad.row].data.KD_DOKTER = b.data.KD_DOKTER;
		   }		
	   }
	});
	return radComboboxDesk;
}

function getRadDokter(){
	var radText = new Ext.form.TextField({
	   id			: 'RadRequestDok',
	   readonly		: true,
	   disable		: true,
	   store		: radData(),
	   value		: 'USERNAME',
	   valueField	: 'USERNAME',
	   displayField	: 'USERNAME',
	   anchor		: '95%',
	   listeners	: {}
	});
	return radText;
}

function getRadKelas(){
	var radText = new Ext.form.ComboBox({
	   id				: 'RadRequestDok',
	   typeAhead		: true,
	   triggerAction	: 'all',
	   lazyRender		: true,
	   mode				: 'local',
	   hideTrigger		: true,
	   forceSelection	: true,
	   selectOnFocus	: true,
	   fieldLabel		: 'Kode Test',
	   align			: 'Right',
	   store			: radData(),
	   valueField		: 'KD_KLASS',
	   displayField		: 'KD_KLASS',
	   anchor			: '95%',
	   listeners		: {}
	});
	return radText;
}

function datasavepoliklinikrad(mBol){	
	Ext.Ajax.request({
		url		: baseURL + "index.php/main/CreateDataObj",
		params	: getParamDetailPoliklinikRad(),
		success	: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
				if(mBol === false){
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				}
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function getParamDetailPoliklinikRad(){
    var params ={
		Table		: 'CrudpoliRad',
		KdPasien	: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit		: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk	: Ext.get('txtKdUrutMasuk').getValue(),
		Tgl			: Ext.get('dtpTanggalDetransaksi').dom.value,
		List		: getArrdetailRadiologi()
	};
	return params;
}

function getArrdetailRadiologi(){
	var x = '',y='',z='::',hasil;
	for(var k = 0; k < dsRwJPJLab.getCount(); k++){
		if (dsRwJPJLab.data.items[k].data.KD_PRODUK == null){
			x += '';
		}else{
			hasil = dsRwJPJLab.data.items[k].data.KD_PRODUK;
			y = dsRwJPJLab.data.items[k].data.KD_DOKTER;
			y += z + hasil;
		}
		x += y + '<>';
	}
	return x;
}

function HapusBarisRad(){
    if( cellselectedrad != undefined ){
    	Ext.Msg.show({
           title	: nmHapusBaris,
           msg		: 'Anda yakin akan menghapus' ,
           buttons	: Ext.MessageBox.YESNO,
           fn		: function (btn){
               if (btn =='yes'){
   					if (cellselectedrad.data.KD_PRODUK != '' && cellselectedrad.data.DESKRIPSI != ''){
   						dsRwJPJLab.removeAt(CurrentRad.row);
					   	DataDeleteRad();
                    }else{
                    	ShowPesanWarningRWJ('Pilih record ','Hapus data');
                    }
               }
           },
           icon: Ext.MessageBox.QUESTION
        });
    }
}

function DataDeleteRad(){
    Ext.Ajax.request({
	    url		: baseURL + "index.php/main/DeleteDataObj",
	    params	: getParamDataDeleteRad(),
	    success	: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                cellSelecteddeskripsi=undefined;
                RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                AddNewKasirRWJ = false;
            }else if (cst.success === false && cst.pesan === 0 ){
                ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
            }else{
                ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
            }
        }
    });
}

function getParamDataDeleteRad(){
    var params ={
		Table		: 'CrudpoliRad',
		Kdproduk 	: CurrentRad.data.data.KD_PRODUK,
		idradkonsul	: CurrentRad.data.data.ID_RADKONSUL
	};
	return params;
}

function RefreshDataSetRadiologi(){	
	var strKriteriaRadiologi='';
	strKriteriaRadiologi = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksi').getValue() + '~ and kd_unit=~'+Ext.get('txtKdUnitRWJ').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi').dom.value+'~';
	dsRwJPJLab.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
            Sort	: 'id_konsul',
			Sortdir	: 'ASC', 
			target	: 'CrudpoliRad',
			param	: strKriteriaRadiologi
		} 
	});
	return dsRwJPJLab;
}

function getFormatTanggal(date){
	tanggal = new Date(date);
	month = '' + parseInt(tanggal.getMonth()+1);
	day = '' +tanggal.getDate();
	year = ''+tanggal.getFullYear();
	if (month.length < 2){
		month =  '0' + parseInt(tanggal.getMonth()+1);
	}
	if (day.length < 2){
		day =  '0' + tanggal.getDate();
	}
	return [year,month,day].join('-');
}