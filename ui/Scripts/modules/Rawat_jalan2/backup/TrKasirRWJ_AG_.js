var CurrentKasirRWJ =
{
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);


var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Bayar';
var dsTRKasirRWJList;
var dsTRDetailKasirRWJList;
var AddNewKasirRWJ = true;
var selectCountKasirRWJ = 50;
var now = new Date();
var rowSelectedKasirRWJ;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
//var FocusCtrlCMRWJ;
var vkode_customer
CurrentPage.page = getPanelRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelRWJ(mod_id) 
{
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK'];
    dsTRKasirRWJList = new WebApp.DataStore({ fields: Field });
    refeshkasirrwj();
    var grListTRRWJ = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirRWJList,
            anchor: '100% 91.9999%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(ridx);
                    if (rowSelectedKasirRWJ != undefined)
                    {
                        RWJLookUp(rowSelectedKasirRWJ.data);
                    }
                    else
                    {
                        RWJLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewRWJ',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 100
                    },
                    {
                        id: 'colTglRWJViewRWJ',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 105,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 85,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colRWJerViewRWJ'
                    },
					{
                        header: 'Pasien',
                        width: 200,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colRWJerViewRWJ'
                    },
                    {
                        id: 'colLocationViewRWJ',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 210
                    },
                    
                    {
                        id: 'colDeptViewRWJ',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
							hideable:false,
							menuDisabled:true,
                        width: 170
                    },
                    {
                        id: 'colImpactViewRWJ',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 110
                    }
                   
                ]
            ),
            tbar:
                [
                    {
                        id: 'btnEditRWJ',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirRWJ != undefined)
                            {
                                    RWJLookUp(rowSelectedKasirRWJ.data);
                            }
                            else
                            {
							ShowPesanWarningRWJ('Pilih data data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },' ', '-', 'Status bayar' + ' : ', ' ',
						mComboStatusBayar_viKasirRwj()
                    , ' ', '-', 'Tanggal Kunjungan' + ' : ', ' ',
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal Kunjungan' + ' ',
                        id: 'dtpTglAwalFilterRWJ',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        onInit: function() { }
                    }, ' ', '  ' + nmSd + ' ', ' ', {
                        xtype: 'datefield',
                        fieldLabel: nmSd + ' ',
                        id: 'dtpTglAkhirFilterRWJ',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','-', '' ,' ', ' ','' , ' ','' , ' ','' , ' ','' ,
							
					{
                    text: nmRefresh,
                    tooltip: nmRefresh,
                    iconCls: 'refresh',
                    anchor: '25%',
                    handler: function(sm, row, rec)
                    {
                       // rowSelectedKasirRWJ=undefined;
                        RefreshDataFilterKasirRWJ();
                    }
					},'', '', '' + ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
                ]
            }
	);
	
	

    var FormDepanRWJ = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa ',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [grListTRRWJ],
            tbar:
            [
              '' , '' , '' ,'' ,'' , '' , '' ,'' ,' ' , '' , '' ,'' ,' ' , '' , '' ,'' ,' ' , '' , '' ,'-','No. Medrec' + ' : ', ' ',
                {
                    xtype: 'textfield',
                   // fieldLabel: 'No. Medrec' + ' ',
                    id: 'txtFilterNomedrec',
                    width: 100,
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue())
                                             Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirRWJ();
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirRWJ();
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterNomedrec').setValue('')
                                            }
                                }
                            }

                        }
                },
				'-','Nama Pasien ', '-',
					
					{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirRwj',
							//emptyText: 'Nama',
							width: 150,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									        RefreshDataFilterKasirRWJ();

									} 						
								}
							}
						}, ' ', ' ',  ' ', ' ', '-','Poli tujuan', '-',
						
						mComboUnit_viKasirRwj(),
						'-','Doktor', '-',
				{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_DOKTER_viKasirRwj',
							//emptyText: 'Alamat',
							width: 150,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									RefreshDataFilterKasirRWJ();

						
									} 						
								}
							}
						}
            ],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
  

   return FormDepanRWJ

};


function mComboStatusBayar_viKasirRwj()
{
  var cboStatus_viKasirRwj = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirRwj',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    width: 100,
			emptyText:'',
			fieldLabel: 'JENIS',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Bayar'], [2, 'Belum Bayar']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirRwj,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirRwj=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirRWJ();

				}
			}
		}
	);
	return cboStatus_viKasirRwj;
};


function mComboUnit_viKasirRwj() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwj.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirRwj',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirRwj,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'all',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirRWJ();

			        //selectStatusCMRWJView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirRwj;
};



function RWJLookUp(rowdata) 
{
    var lebar = 600;
    FormLookUpsdetailTRrwj = new Ext.Window
    (
        {
            id: 'gridRWJ',
            title: 'Penata Jasa Rawat Jalan',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRRWJ(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRrwj.show();
    if (rowdata == undefined) 
	{
        RWJAddNew();
    }
    else 
	{
        TRRWJInit(rowdata)
    }

};


function mComboPoliklinik(lebar)
{

    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnit').dom.value +'~)'
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
			width: 170,
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastoredokter(b.data.KD_UNIT)
								   selectKlinikPoli=b.data.KD_UNIT
                                }
                   


		}
        }
    )

    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
}

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    labelWidth:80,
			fieldLabel: 'Dokter      ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
            anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
									selectDokter = b.data.KD_DOKTER;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function KonsultasiLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window
    (
        {
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					//	width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														//fieldLabel:'Dokter  ',
														name: 'txtKdunitKonsultasi',
														id: 'txtKdunitKonsultasi',
														//emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														//hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Kosultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																	Datasave_Konsultasi(false);
																	FormLookUpsdetailTRKonsultasi.close()	
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close()
															}
														}
											 ]
											
											
											}
											
											
										
									,
									
								]
			},
            listeners:
            {
              
            }
        }
    );

    FormLookUpsdetailTRKonsultasi.show();
  
      KonsultasiAddNew();
	

};
function KonsultasiAddNew() 
{
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnit').dom.value   ;
	Ext.get('txtnamaunitKonsultasi').dom.value=Ext.get('txtNamaUnit').dom.value;
	

};

function getFormEntryTRRWJ(lebar) 
{
    var pnlTRRWJ = new Ext.FormPanel
    (
        {
            id: 'PanelTRRWJ',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:189,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputRWJ(lebar)],
           tbar:
            [
              
		              
		                
					    	//-------------- ## --------------
        					{
						        text: ' Konsultasi',
						        id:'btnLookUpKonsultasi_viKasirIgd',
						        iconCls: 'Konsultasi',
						        handler: function()
								{
								KonsultasiLookUp()
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: ' Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirIgd',
						        iconCls: 'gantidok',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   GantiDokterLookUp();
						        },
					    	},
							{
						        text: 'Ganti Kelompok Pasien',
						        id:'btnganyipasien',
						        iconCls: 'gantipasien',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   KelompokPasienLookUp();
						        },
					    	}
					    	
		               
		          
               
            ]
        }
    );
var x;
 var GDtabDetailRWJ = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailRWJ',
        region: 'center',
        activeTab: 0,
		height:250,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [GetDTLTRRWJGrid(),GetDTLTRDiagnosaGrid()
				
		                //-------------- ## --------------
					],
        tbar:
                [
                       
                      
						{
							text: 'Tambah Produk',
							id: 'btnLookupRWJ',
							tooltip: nmLookup,
							iconCls: 'find',
							handler: function()
							{
						  
									var p = RecordBaruRWJ();
									var str='';
									if (Ext.get('txtKdDokter').dom.value  != undefined && Ext.get('txtKdDokter').dom.value  != '')
									{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
											str = 'kd_dokter = ~' + Ext.get('txtKdDokter').dom.value  + '~';
									};
									FormLookupKasirRWJ(str, dsTRDetailKasirRWJList, p, true, '', true);
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanRWJ',
								tooltip: nmSimpan,
								iconCls: 'save',
								handler: function()
									{
									
									/* if (dsTRDetailKasirRWJList.getCount() > 0 )
													{
															if (cellSelecteddeskripsi != undefined)
															{
																	if(CurrentKasirRWJ != undefined)
																	{
																			 Dataupdate_KasirRWJ(false);
																			RefreshDataFilterKasirRWJ();
																	}
															}
															else
															{
																	ShowPesanWarningRWJ('Pilih record lalu ubah qty  ','Ubah data');
															}
													}*/
													Datasave_KasirRWJ(false);
								   
								}
							},   {
                                id:'btnHpsBrsRWJ',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailKasirRWJList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentKasirRWJ != undefined)
                                                        {
                                                                HapusBarisRWJ();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningRWJ('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },
						{
							text: 'Tambah Diagnosa',
							id: 'btnLookupDiagnosa',
							tooltip: nmLookup,
							iconCls: 'find',
							handler: function()
							{
								
									var p = RecordBaruDiagnosa();
									var str='';
									FormLookupDiagnosa(str, dsTRDetailDiagnosaList, p, true, '', true);
						}
						},
                        
							 {
							text: 'Simpan',
							id: 'btnSimpanDiagnosa',
							tooltip: nmSimpan,
							iconCls: 'save',
							handler: function()
								{
								
								 if (dsTRDetailDiagnosaList.getCount() > 0 )
												{
														if (cellSelecteddeskripsi != undefined)
														{
																if(CurrentDiagnosa != undefined)
																{
																		 
																		Datasave_Diagnosa(false);
																		//FormLookUpsdetailTRDiagnosa.close();
																		//RefreshDataFilterDiagnosa();
																		
																}
														}
														else
														{
																ShowPesanWarningDiagnosa('Pilih record lalu ubah STAT_DIAG  ','Ubah data');
														}
												}
                       
								}
							},
							{
                                id:'btnHpsBrsDiagnosa',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                HapusBarisDiagnosa();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                                                }
                                        }
                                }
							}
                ],
		listeners:
            {
               'tabchange' : function (panel, tab) {
				//GDtabDetailRWJ.setActiveTab(1);
				if (x ==1)
				{
				 Ext.getCmp('btnLookupRWJ').hide()
                 Ext.getCmp('btnSimpanRWJ').hide()
				 Ext.getCmp('btnHpsBrsRWJ').hide()
                 Ext.getCmp('btnHpsBrsDiagnosa').show()
				  Ext.getCmp('btnSimpanDiagnosa').show()
				  Ext.getCmp('btnLookupDiagnosa').show()
				//alert('a');
					x=2;
					return x;
				}else 
				{ 	
				 Ext.getCmp('btnLookupRWJ').show()
                 Ext.getCmp('btnSimpanRWJ').show()
				 Ext.getCmp('btnHpsBrsRWJ').show()
                 Ext.getCmp('btnHpsBrsDiagnosa').hide()
				  Ext.getCmp('btnSimpanDiagnosa').hide()
				  Ext.getCmp('btnLookupDiagnosa').hide()
				 ,
				x=1;	
				return x;
				}
				
			}
		//bbar :			/**/
        }
		}
		
    );
	
   
   
   var pnlTRRWJ2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRRWJ2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:260,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRWJ
			
			]
        }
    );

	
   
   
    var FormDepanRWJ = new Ext.Panel
	(
		{
		    id: 'FormDepanRWJ',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRWJ,pnlTRRWJ2	
				

			]

		}
	);

    return FormDepanRWJ
};

function RecordBaruDiagnosa()
{
	var p = new mRecordDiagnosa
	(
		{
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		  //  'KD_TARIF':'', 
		   // 'HARGA':'',
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    //'DESC_REQ':'',
		   // 'KD_TARIF':'',
		    'URUT_MASUK':''
		}
	);
	
	return p;
};

function HapusBarisDiagnosa()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail()
{
    Ext.Ajax.request
    (
        {
            
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                  RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnit').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
                }
           
                else
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnit').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    )
};

function getParamDataDeleteDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnit').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT,
    };
	
    return params
};



function getParamDetailTransaksiDiagnosa2() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		
		
		KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnit').getValue(),
		UrutMasuk:Ext.get('txtKdUrutMasuk').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		List:getArrDetailTrDiagnosa(),
		JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetailDiagnosa()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS
			
			
			if (i === (dsTRDetailDiagnosaList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};




function Datasave_Diagnosa(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/saveDiagnosa",
					params: getParamDetailTransaksiDiagnosa2(),
					success: function(o) 
					{
	
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
						
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnit').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
								
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnit').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						
						else 
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnit').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	
};

function ShowPesanWarningDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};







function GetDTLTRDiagnosaGrid() 
{
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG'];
	
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsTRDetailDiagnosaList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                            CurrentDiagnosa.row = row;
                            CurrentDiagnosa.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel()
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdProduk',
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colePenyakitDiagnosa',
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
					menuDisabled:true,
				width:200
                
            }
            ,
			{
                id: 'colePasien',
                header: 'kd_pasien',
                dataIndex: 'KD_PASIEN',
				hidden:true
                
            }
			,
			{
                id: 'coleurut',
                header: 'urut',
                dataIndex: 'URUT',
				hidden:true
                
            }
			,
			{
                id: 'coleurutmasuk',
                header: 'urut masuk',
                dataIndex: 'URUT_MASUK',
				hidden:true
                
            }
            ,
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: 'TGL_MASUK',
				hidden:true
                
            }
            ,
            {
                id: 'colProblemDiagnosa',
                header: 'Diagnosa',
                width:130,
				menuDisabled:true,
				//align: 'right',
				//hidden :true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            },
            {
                id: 'colKasusDiagnosa',
                header: 'Kasus',
                width:130,
				//align: 'right',
				//hidden :true,
				menuDisabled:true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            }
			

        ]
    )
};

///---------------------------------------------------------------------------------------///
function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',

        items: 
            [
                {
                    x:0,
                    y:60,
                    xtype: 'textarea',
                    id:'TxtHistoriDeleteDataPasien',
                    hideLabel: true,
                    name: 'msg',
                    anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                }

            ]
    })};
function TambahBarisRWJ()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirRWJList.insert(dsTRDetailKasirRWJList.getCount(), p);
    };
};

function HapusBarisRWJ()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														//alert(variablehistori=text);
														// process text value and close...
														DataDeleteKasirRWJDetail();
														}
												});

												
                                         
                                
               
        }
        else
        {
            dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        };
    }
};

function DataDeleteKasirRWJDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirRWJDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                    AddNewKasirRWJ = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirRWJDetail()
{
    var params =
    {
		Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirRWJ.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirRWJ.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnit').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirRWJ.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirRWJ.data.data.HARGA,
		
		TrKdProduk :	 CurrentKasirRWJ.data.data.KD_PRODUK,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Hapus:2
    };
	
    return params
};

function getParamDataupdateKasirRWJDetail()
{
    var params =
    {
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirRWJ.data.data.URUT,

        Qty: CurrentKasirRWJ.data.data.QTY,
        Ubah:1
    };
	
    return params
};



function GetDTLTRRWJGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirRWJList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirRWJDetail() ;
    var gridDTLTRRWJ = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Transaksi',
            stripeRows: true,
            store: dsTRDetailKasirRWJList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
                            CurrentKasirRWJ.row = row;
                            CurrentKasirRWJ.data = cellSelecteddeskripsi;
                           // FocusCtrlCMRWJ='txtAset';
                        }
                    }
                }
            ),
            cm: TRRawatJalanColumModel()
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRRWJ;
};

function TRRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:320
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			   	menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirRWJ(false);
											//RefreshDataFilterKasirRWJ();
									        //RefreshDataFilterKasirRWJ();

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};

function GetLookupAssetCMRWJ(str)
{
	if (AddNewKasirRWJ === true)
	{
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':''
			}
		);
		
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,true,'',false);
	}
	else
	{	
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT
			}
		);
	
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,false,CurrentKasirRWJ.row,false);
	};
};

function RecordBaruRWJ()
{

	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function RefreshDataSetDiagnosa(medrec,unit,tgl)
{	
 var strKriteriaDiagnosa='';
    //strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = 'kd_pasien = ~' + medrec + '~ and kd_unit=~'+unit+'~ and tgl_masuk in(~'+tgl+'~)';
    //strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountDiagnosa, 
				//Sort: 'EMP_ID',
                Sort: 'kd_penyakit',
				Sortdir: 'ASC', 
				target:'ViewDiagnosa',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
};

function TRRWJInit(rowdata)
{
    AddNewKasirRWJ = false;
	
	Ext.get('txtNoTransaksiKasirrwj').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnit').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
//	Ext.get('txtCustomerLama').dom.value=rowdata.CUSTOMER;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	RefreshDataKasirRWJDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        //var cst = Ext.decode(o.responseText);
			tampungshiftsekarang=o.responseText
			//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
	//,,
	
	
};

function mEnabledRWJCM(mBol)
{
//	 Ext.get('btnSimpanRWJ').dom.disabled=mBol;
//	 Ext.get('btnSimpanKeluarRWJ').dom.disabled=mBol;
//	 Ext.get('btnHapusRWJ').dom.disabled=mBol;
	 Ext.get('btnLookupRWJ').dom.disabled=mBol;
//	 Ext.get('btnTambahBrsRWJ').dom.disabled=mBol;
	 Ext.get('btnHpsBrsRWJ').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function RWJAddNew() 
{
    AddNewKasirRWJ = true;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirRwj').dom.value= ''
	rowSelectedKasirRWJ=undefined;
	dsTRDetailKasirRWJList.removeAll();
	mEnabledRWJCM(false);
	

};

function RefreshDataKasirRWJDetail(no_transaksi) 
{
    var strKriteriaRWJ='';
    //strKriteriaRWJ = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~";
    //strKriteriaRWJ = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirRWJList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailRWJGridBawah',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailKasirRWJList;
};

///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRWJ() 
{
    var params =
	{
		Table:'ViewTrKasirRwj',
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnit').getValue(),
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		List:getArrDetailTrRWJ(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewTrKasirRwj', //data access listnya belum dibuat
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnitAsal : Ext.get('txtKdUnit').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer,
	};
    return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{		
			var recordterakhir;
			var y='';
			var z='@@##$$@@';
			
			
			recordterakhir=dsTRDetailKasirRWJList.data.items[i].data.DESC_REQ
			TotalProduk=TotalProduk+recordterakhir
			
			Ext.get('txtJumlah1EditData_viKasirRwj').dom.value=formatCurrency(TotalProduk);
			if (i === (dsTRDetailKasirRWJList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		
	}	
	
	return x;
};





function getArrDetailTrRWJ()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirRWJList.data.items[i].data.URUT
			y += z + dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirRWJList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirRWJList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirRWJList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirRWJList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirRWJList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailKasirRWJList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputRWJ(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiRWJ(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnit',
					    id: 'txtKdUnit',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiRWJ(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirrwj',
					    id: 'txtNoTransaksiKasirrwj',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirRWJ() 
{
    dsTRKasirRWJList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirRWJ,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirRwj',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirRWJ = undefined;
    return dsTRKasirRWJList;
};
function refeshkasirrwj()
{
dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : ''
				}			
			}
		);   
		return dsTRKasirRWJList;
}

function RefreshDataFilterKasirRWJ() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Bayar')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  co_status = true';
		}
		else
		{
		
                        KataKunci += ' and co_status =  true';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Bayar')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  co_status = false';
		}
		else
		{
	
                        KataKunci += ' and co_status =  false';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterRWJ').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirRWJList;
};


function Datasave_Konsultasi(mBol) 
{	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/KonsultasiPenataJasa",					
					params: getParamKonsultasi(),
					failure: function(o)
					{
					ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
					RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
						
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};



function Datasave_KasirRWJ(mBol) 
{	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/savedetailpenyakit",
					params: getParamDetailTransaksiRWJ(),
					failure: function(o)
					{
					ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					},	
					success: function(o) 
					{
						//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataKasirRWJ();
							RefreshDataFilterKasirRWJ();
							if(mBol === false)
							{
								//RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function Dataupdate_KasirRWJ(mBol) 
{	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdateKasirRWJDetail(),
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRWJ(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function ValidasiEntryCMRWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirrwj').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirRWJList.getCount() === 0 || (Ext.get('txtKdDokter').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiKasirrwj').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokter').dom.value  === undefined) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailKasirRWJList.getCount() === 0) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};

function ValidasiEntryKonsultasi(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') )
	{
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	};
	return x;
};




function ShowPesanWarningRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteKasirRWJ() 
{
   if (ValidasiEntryCMRWJ(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiRWJ(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataKasirRWJ();
                                        RWJAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningRWJ(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningRWJ(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorRWJ(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};


/*---LOOK UP GANTI DOKTER BESERTA ELEMENNYA----------------------------------*/

//function GantiDokterLookUp(rowdata)
function GantiDokterLookUp(mod_id) 
{
   
   

    var FormDepanDokter = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Ganti Dokter',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [DokterLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanDokter

};









function DokterLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window
    (
        {
            id: 'gridDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 180,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryDokter(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirDokter=undefined;
                    //RefreshDataFilterKasirDokter();
                }
            }
        }
    );

    FormLookUpGantidokter.show();
 

};
function getItemPanelButtonGantidokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function()
						{
						GantiDokter(false);
						FormLookUpGantidokter.close();	
						}
					},
					{
							xtype:'button',
							text:'Tutup' ,
							width:70,
							hideLabel:true,
							id: 'btnCancelGantidokter',
							handler:function() 
							{
								FormLookUpGantidokter.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryDokter(lebar) 
{
    var pnlTRGantiDokter = new Ext.FormPanel
    (
        {
            id: 'PanelTRDokter',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:190,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


	
   
   
  
   
   
   
   
    var FormDepanDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRGantiDokter
				

			]

		}
	);

    return FormDepanDokter
};
///---------------------------------------------------------------------------------------///


///---------------------------------------------------------------------------------------///


function getItemPanelInputGantidokter(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDokter(lebar)		
				]
			}
		]
	};
    return items;
};

function ValidasiEntryTutupDokter(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function getItemPanelNoTransksiDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
			
					mComboDokterGantiEntry()
				
					
				]
				
			
			}
			
			
		]
	}
    return items;
};


function mComboDokterGantiEntry()
{ 
	
	
    // Grid Kasir Rawat Jalan # --------------
 

	// Kriteria filter pada Grid # --------------
   
    //-------------- # End form filter # --------------

 var Field = ['KD_DOKTER','NAMA'];

    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnit').getValue();
	var kddokter = Ext.get('txtKdDokter').getValue();
   dsDokterGantiEntry.load
                (
                    {
                     params:
							{
											Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
											Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboDokter',
								param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
							}
                    }
                )

    var cboDokterGantiEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter Baru',
		    align: 'Right',
             
		    store: dsDokterGantiEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor:'100%',
		    listeners:
			{
			    'select': function(a,b,c)
				{

									selectDokter = b.data.KD_DOKTER;
									NamaDokter = b.data.NAMA;
									 Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);
	
    return cboDokterGantiEntry;

};

/*-----------Update Dokter----------------------------------*/
function GantiDokter(mBol)
{
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                   url: WebAppUrl.UrlUpdateData,
                                    params: getParamGantiDokter(),
									failure: function(o)
										{
										 ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
										},	
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoRWJ(nmPesanSimpanSukses,'Ganti Dokter');
													Ext.get('txtKdDokter').dom.value = selectDokter;
													Ext.get('txtNamaDokter').dom.value = NamaDokter;
													FormDepanDokter.close();
                                                    FormLookUpGantidokter.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                                            };
                                    }
                            }
                    )
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION TutupDokterSave

/*---------------------------------------------*/

function ValidasiGantiDokter(modul,mBolHapus)
{
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == ''))
	{
	  if (Ext.get('cboDokterRequestEntry').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};


function getParamGantiDokter()
{
    var params =
	{
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnit').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
		
	};
    return params
};

/*-----AKHIR LOOK UP GANTI DOKTER BESERTA ELEMENYNYA-----------------------------*/


/*------------AWAL LOOK UP GANTI KD CUSTOMER PASIEN BESERTA ELEMENNYA -------------- */

function KelompokPasienLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();

};


function getFormEntryTRKelompokPasien(lebar) 
{
    var pnlTRKelompokPasien = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKelompokPasien(lebar),getItemPanelButtonKelompokPasien(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	,
					
				]
			}
		]
	};
    return items;
};




function getItemPanelButtonKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkKelompokPasien',
						handler:function()
						{
					
					Datasave_Kelompokpasien();
					FormLookUpsdetailTRKelompokPasien.close();
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelKelompokPasien',
							handler:function() 
							{
								FormLookUpsdetailTRKelompokPasien.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function KelompokPasienbaru() 
{
	jeniscus=0;
    KelompokPasienAddNew = true;
	//Ext.getCmp('cboPerseorangan').show()
    Ext.getCmp('cboKelompokpasien').show()
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value
    //Ext.getCmp('cboPerusahaanRequestEntry').hide()
	

};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
	
   // rowSelectedKasirRWJ = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	//	title: 'Kelompok Pasien Lama',
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				//title: 'Kelompok Pasien Lama',
			    items:
				[
					
					
						{	 
															xtype: 'tbspacer',
														
															height: 2
						},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Kelompok Pasien Asal',
                                        //maxLength: 200,
                                        name: 'txtCustomerLama',
                                        id: 'txtCustomerLama',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
					
					
				]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
														{	 
															xtype: 'tbspacer',
															
															height:3
														},
									{  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien Baru',
                                    id: 'kelPasien',
                                     editable: false,
                                    //value: 'Perseorangan',
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
											'Id',
											'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
										  displayField: 'displayText',
										  mode: 'local',
										  width: 100,
										  forceSelection: true,
										  triggerAction: 'all',
										  emptyText: 'Pilih Salah Satu...',
										  selectOnFocus: true,
										  anchor: '95%',
										  listeners:
											 {
													'select': function(a, b, c)
												{
												if(b.data.displayText =='Perseorangan')
												{jeniscus='0'
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Perusahaan')
												{jeniscus='1';
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Asuransi')
												{jeniscus='2';
												Ext.getCmp('txtNoSJP').enable();
												Ext.getCmp('txtNoAskes').enable();
											}
												
												RefreshDatacombo(jeniscus);
												}

											}
                                  }, 
								  {
										columnWidth: .990,
										layout: 'form',
										border: false,
										labelWidth:130,
										items:
										[
															mComboKelompokpasien()
										]
									},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     },
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
									
									//mComboKelompokpasien
					
					
				]
			}
			
		]
	}
    return items;
};

function Datasave_Kelompokpasien(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 )
	{
		
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL +  "index.php/main/functionRWJ/UpdateKdCustomer",	
					params: getParamKelompokpasien(),
					failure: function(o)
					{
					ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
					RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						Ext.get('txtCustomer').dom.value = selectNamaCustomer;
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}

						else 
						{
								ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function getParamKelompokpasien() 
{
	
    var params =
	{
		
		Table:'ViewTrKasirRwj', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnit').getValue(),
		KdDokter:Ext.get('txtKdDokter').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
		
		
	};
    return params
};

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};


/*------------------------- AKHIR LOOK UP GANTI KD CUSTOMER PASIEN ------------------------------------*/

