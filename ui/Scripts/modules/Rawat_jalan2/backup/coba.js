var selectDepartPMPlanEntry;
var dsDepartPMPlanEntry;
var dataSource_viDaftar;
var NamaForm_viDaftar="Pendaftaran ";
var dsDTLTRPlanList;
var AddNewPlan = true;
var selectCount_viDaftar = 50;
var now_viDaftar = new Date();
var selectPMPlan;
var rowSelected_viDaftar;
var cellSelectedPlanDet;
var setLookUps_viDaftar;
var addNew_viDaftar;
var CurrentTRPlan =
{
    data: Object,
    details: Array,
    row: 0
};

CurrentPage.page = dataGrid_viDaftar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
datarefresh_viDaftar();

function dataGrid_viDaftar(mod_id) 
{
    var Field = 
    [
    	'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI' 
    ];
	
    dataSource_viDaftar = new WebApp.DataStore({ fields: Field });

    var grData_viDaftar = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dataSource_viDaftar,
		    anchor: '100% 100%',
		    columnLines: true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
							rowSelected_viDaftar = undefined;
							rowSelected_viDaftar = dataSource_viDaftar.getAt(row);
							CurrentData_viDaftar
							CurrentData_viDaftar.row = row;
							CurrentData_viDaftar.data = rowSelected_viDaftar.data;
					    }
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftar',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 100,
						filter:
						{
							type: 'int'
						}
					},
					{
						id: 'colNMPASIEN_viDaftar',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 100,
						filter:
						{}
					},
					{
						id: 'colALAMAT_viDaftar',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 100,
						sortable: true,
						filter: {}
					},
					{
						id: 'colTglKunj_viDaftar',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 100,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					{
						id: 'colPoliTujuan_viDaftar',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 100,
						sortable: true,
						filter: {}
					}
				]
				),
//penulisan bbar berbeda dengan di asp
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dataSource_viDaftar,
            pageSize: selectCount_viDaftar,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditPlan',
				    text: 'Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelected_viDaftar != undefined) 
						{
							setLookUp_viDaftar(rowSelected_viDaftar.data);
						}
						else 
						{
							setLookUp_viDaftar();
						}
				    }
				}, ' ', '-',
				
			]
		}
	);

    var FrmTabs_viDaftar = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: NamaForm_viDaftar,
		    border: false,
		    shadhow: true,
		    iconCls: 'Plan',
		    margins: '0 5 5 0',
		    items: [grData_viDaftar],
		    tbar:
			[
				// { xtype: 'tbtext', text: 'Tgl Kunjungan: ', cls: 'left-label', width: 90 },
				// {
				// 	xtype: 'datefield',
				// 	id: 'txtfilterTglKunjAwal_viDaftar',
				// 	value: now_viDaftar,
				// 	format: 'd/M/Y',
				// 	width: 120,
				// 	listeners:
				// 	{ 
				// 		'specialkey' : function()
				// 		{
				// 			if (Ext.EventObject.getKey() === 13) 
				// 			{
				// 				datarefresh_viDaftar();								
				// 			} 						
				// 		}
				// 	}
				// },
//pembuatan item form bisa menggunakan yang asp bisa juga yang php
				'Tgl Kunjungan: ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: 'Tgl Kunjungan: ',
				    id: 'txtfilterTglKunjAwal_viDaftar',
				    format: 'd/M/Y',
				    width: 120,
				    value: now_viDaftar,
					onInit: function() { }
				},' ',
				's/d : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: 's/d : ',
				    id: 'txtfilterTglKunjAkhir_viDaftar',
				    format: 'd/M/Y',
				    width: 120,
				    value: now_viDaftar,
				    onInit: function() { }
				},' ',
				'Poli Tujuan : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: 'Poli Tujuan : ',
				    id: 'txtpoli',
				    width: 200,
				    onInit: function() { }
				},' ',
				'Nama : ', ' ',
				{
				    xtype: 'textfield',
				    fieldLabel: 'Nama : ',
				    id: 'txtnama',
				    width: 200,
				    onInit: function() { }
				},' ',
				{
				    text: 'Refresh',
				    tooltip: 'Choose Transaction',
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) 
					{
				        datarefresh_viDaftar();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() 
				{
			    }
			}
		}
	);
	
    return FrmTabs_viDaftar

};


function setLookUp_viDaftar(rowdata) 
{
	 var lebar = 860;
    setLookUps_viDaftar = new Ext.Window
	(
		{
		    id: 'setLookUp_viDaftar',
		    title: NamaForm_viDaftar,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 580,
		    border: false,
			resizable:false,
			autoScroll: true,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'Plan',
		    modal: true,
		    items: getFormItemEntry_viDaftar(lebar),
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelected_viDaftar=undefined;
					datarefresh_viDaftar();
				}
			}
		}
	);

    setLookUps_viDaftar.show();
    if (rowdata == undefined) 
	{
        dataaddnew_viDaftar();
    }
    else 
	{
		datainit_viDaftar();
    }
};

function getFormItemEntry_viDaftar(lebar) 
{
	 var pnlTRPlan = new Ext.FormPanel
	(
		{
		    id: 'PanelTRPlan',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 10px 10px',
			height:146,
		    anchor: '100%',
		    width: lebar,
		    border: false,
		    items:
		    [

		    ],
		    // items: [getItemPanelInputPlan(lebar)],
		    tbar:
			[
				{
				    text: 'Add',
				    id: 'btnAdd_viDaftar',
				    tooltip: 'New Record',
				    iconCls: 'add',
				    handler: function() 
					{
				        addnew_viDaftar();
				    }
				}, '-',

				{
				    text: 'Save',
				    id: 'btnSimpan_viDaftar',
				    tooltip: 'Save Data ',
				    iconCls: 'save',
				    handler: function() 
					{
				        datasave_viDaftar(false);
				        RefreshDataPlanFilter(false);
				    }
				}, '-',
				{
				    text: 'Save & Close',
				    id: 'btnSimpanExit_viDaftar',
				    tooltip: 'Save Data & Close ',
				    iconCls: 'saveexit',
				    handler: function() 
					{
						var x =  datasave_viDaftar(true);
				         RefreshDataPlanFilter(false);
						if (x===undefined)
						{
							setLookUps_viDaftar.close();
						};    
				    }
				}, '-',
				{
				    text: 'Delete',
				    id: 'btnDelete_viDaftar',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
				        PlanDelete();
				        RefreshDataPlanFilter(false);
				    }
				}, '-',
				{
				    text: 'Lookup Data Pasien',
				    id: 'btnLookUp_viDaftar',
				    tooltip: 'Lookup Account',
				    iconCls: 'find',
				    handler: function() 
					{
						// var criteria;
				        // criteria = ' WHERE Kd_Unit_Kerja =~' + selectCMPlan + '~';
				        // FormLookupCashIn(criteria);
				    }
				}, 
				'-', '->', '-',
				{
				    text: 'Print Kartu Pasien',
					id:'btnPrint_viDaftar',
				    tooltip: 'Print',
				    iconCls: 'print',
				    handler: function()
					{ CetakPlan() }
				}
			]
		}
	); 

 var GDtabDetailPlan = new Ext.TabPanel   
	(
		{
			id:'GDtabDetailPlan',
	        region: 'center',
	        activeTab: 0,
	        anchor: '100% 69%',
			border:false,
	        plain: true,
	        defaults: 
			{
		        autoScroll: true
	        }
			
		}
	);
	
   
    var FormTRPlan = new Ext.Panel
	(
		{
		    id: 'FormTRPlan',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRPlan,GDtabDetailPlan]

		}
	);

    return FormTRPlan
};

function getItemPanelInputPlan(lebar) 
{
    var items =
	{
	  
	};
    return items;
};

function getItemPanelNoPlan2(lebar) 
{
	    var items =
	{
	   
	}
    return items;
};

function getItemPanelNoPlan3()
{
	    var items =
	{
	   
	}
    return items;
};

function getItemPanelNoPlan(lebar)
{
	    var items =
	{
	    
	}
    return items;
};

function datarefresh_viDaftar()
{
	var criteria;
	criteria = getCriteriaFilter_viDaftar()
    dataSource_viDaftar.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viDaftar,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViViewKunjungan',
				param: criteria
			}
		}
    );
    return dataSource_viDaftar;
};

function getCriteriaFilter_viDaftar()
{
};

function dataaddnew_viDaftar()
{
	addNew_viDaftar = true;
	rowSelected_viDaftar=undefined;

};

function datainit_viDaftar(rowdata)
{
	addNew_viDaftar = false;
};

function datasave_viDaftar(IsExit) 
{	
	
};

function GetDTLTRPlanGrid() 
{
};

function mComboDepartPMPlanEntry() 
{
};