var PenataJasaRJ={};
var dsrad;
var cellselectedrad;
var CurrentRad =
{
    data: Object,
    details: Array,
    row: 0
};
var CurrentKasirRWJ =
{
    data: Object,
    details: Array,
    row: 0
};
	
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create([
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
]);

var CurrentDiagnosa ={
    data: Object,
    details: Array,
    row: 0
};

var combo;
var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create([
   {name: 'KASUS', mapping:'KASUS'},
   {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
   {name: 'PENYAKIT', mapping:'PENYAKIT'},
   {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
]);

var dsTRDetailDiagnosaList;
var dsTRDetailAnamneseList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var dsTRKasirRWJList;
var dsTRDetailKasirRWJList;
var dsPjTrans2;
var dsRwJPJLab;
var dsCmbRwJPJDiag;
var AddNewKasirRWJ = true;
var selectCountKasirRWJ = 50;

var rowSelectedKasirRWJ;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRrwj;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var anamnese;
var CurrentAnamnesese;
//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Asep
PenataJasaRJ.dsComboObat;
PenataJasaRJ.iComboObat;
PenataJasaRJ.iComboVerifiedObat;
PenataJasaRJ.gridObat;
PenataJasaRJ.grid1;
PenataJasaRJ.grid2;
PenataJasaRJ.grid3;//untuk data grid di tab labolatorium
PenataJasaRJ.ds1;
PenataJasaRJ.ds2;
PenataJasaRJ.ds3;//untuk data store grid di tab labolatorium
PenataJasaRJ.ds4=new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab']});
PenataJasaRJ.ds5=new WebApp.DataStore({ fields: ['id_status','status']});
PenataJasaRJ.dsGridObat;
PenataJasaRJ.dsGridTindakan;
PenataJasaRJ.s1;
PenataJasaRJ.btn1;

PenataJasaRJ.ComboVerifiedObat=function(){
	PenataJasaRJ.iComboVerifiedObat	= new Ext.form.ComboBox({
		id				: '',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			id		: 0,
			fields	: ['Id','displayText'],
			data	: [[0, 'Verified'],[1, 'Not Verified']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return PenataJasaRJ.iComboVerifiedObat;
};

PenataJasaRJ.classGridObat	= Ext.data.Record.create([
   {name: 'kd_prd', 	mapping: 'kd_prd'},
   {name: 'nama_obat', 	mapping: 'nama_obat'},
   {name: 'jumlah', 	mapping: 'jumlah'},
   {name: 'satuan', 	mapping: 'satuan'},
   {name: 'cara_pakai', mapping: 'cara_pakai'},
   {name: 'kd_dokter', 	mapping: 'kd_dokter'},
   {name: 'verified', 	mapping: 'verified'},
   {name: 'racikan', 	mapping: 'racikan'}
]);

PenataJasaRJ.classGrid3	= Ext.data.Record.create([
    {name: 'kd_produk', 	mapping: 'kd_produk'},
    {name: 'kd_klas', 	mapping: 'kd_klas'},
    {name: 'deskripsi', 	mapping: 'deskripsi'},
    {name: 'username', 	mapping: 'username'},
    {name: 'kd_lab', 	mapping: 'kd_lab'}
 ]);

PenataJasaRJ.nullGridObat	= function(){
	return new PenataJasaRJ.classGridObat({
		kd_produk		: '',
		nama_obat	: '',
		jumlah		: 0,
		satuan		: '',
		cara_pakai	: '',
		kd_dokter	: '',
		verified	: 'Not Verified',
		racikan		: 0
	});
};

PenataJasaRJ.nullGrid3	= function(){
	return new PenataJasaRJ.classGrid3({
		kd_prd		: '',
		kd_klas		: '',
		deskripsi	: '',
		username	: '',
		kd_lab		: ''
	});
};
PenataJasaRJ.comboObat	= function(){
	var $this	= this;
	$this.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat'] });
	$this.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ'
		} 
	});
	$this.iComboObat= new Ext.form.ComboBox({
		id				: 'fieldAsetNameRWI',
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
		store			: $this.dsComboObat,
		valueField		: 'nama_obat',
		hideTrigger		: true,
		displayField	: 'nama_obat',
		value			: '',
		listeners		: {
			select	: function(a, b, c){	
				var line	= $this.gridObat.getSelectionModel().selection.cell[0];
				$this.dsGridObat.getRange()[line].data.kd_prd=b.json.kd_prd;
				$this.dsGridObat.getRange()[line].data.satuan=b.json.satuan;
				$this.dsGridObat.getRange()[line].data.kd_dokter=b.json.nama;
				$this.dsGridObat.getRange()[line].data.nama_obat=b.json.nama_obat;
				$this.gridObat.getView().refresh();
				console.log($this.s1);
		    }
		}
	});
	return $this.iComboObat;
};
	
function getPanelRWJ(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI','ANAMNESE','CAT_FISIK'];
    dsTRKasirRWJList = new WebApp.DataStore({ fields: Field });
    PenataJasaRJ.ds1=dsTRKasirRWJList;
    refeshkasirrwj();
    var grListTRRWJ = new Ext.grid.EditorGridPanel({
        stripeRows	: true,
        store		: dsTRKasirRWJList,
        anchor		: '100% 58%',
        columnLines	: true,
        autoScroll	: false,
        border		: true,
		sort 		: false,
        sm			: new Ext.grid.RowSelectionModel({
        	singleSelect: true,
            listeners	:{
                rowselect: function(sm, row, rec){
                    rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(row);
                }
            }
        }),
        listeners	: {
            rowdblclick	: function (sm, ridx, cidx){
                rowSelectedKasirRWJ = dsTRKasirRWJList.getAt(ridx);
                PenataJasaRJ.s1=PenataJasaRJ.ds1.getAt(ridx);
                if (rowSelectedKasirRWJ != undefined){
                    RWJLookUp(rowSelectedKasirRWJ.data);
                }else{
                    RWJLookUp();
                }
            }
        },
        cm			: new Ext.grid.ColumnModel([ 
                 new Ext.grid.RowNumberer(),
			{
                header		: 'Status Posting',
                width		: 150,
                sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
                dataIndex	: 'POSTING_TRANSAKSI',
                id			: 'txtposting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
                     switch (value){
                         case 't':
                             metaData.css = 'StatusHijau'; 
                             break;
                         case 'f':
                        	 metaData.css = 'StatusMerah';
                             break;
                     }
                     return '';
                }
            },
            {
                id			: 'colReqIdViewRWJ',
                header		: 'No. Transaksi',
                dataIndex	: 'NO_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 80
            },
            {
                id			: 'colTglRWJViewRWJ',
                header		: 'Tgl Transaksi',
                dataIndex	: 'TANGGAL_TRANSAKSI',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 75,
                renderer	: function(v, params, record){
                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                }
            },
			{
                header		: 'No. Medrec',
                width		: 65,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'KD_PASIEN',
                id			: 'colRWJerViewRWJ'
            },
			{
                header		: 'Pasien',
                width		: 190,
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                dataIndex	: 'NAMA',
                id			: 'colRWJerViewRWJ'
            },
            {
                id			: 'colLocationViewRWJ',
                header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 170
            },
            {
                id			: 'colDeptViewRWJ',
                header		: 'Dokter',
                dataIndex	: 'NAMA_DOKTER',
                sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 150
            },
            {
                id			: 'colImpactViewRWJ',
                header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
				sortable	: false,
				hideable	: false,
				menuDisabled: true,
                width		: 90
            }
        ]),
        viewConfig	:{forceFit: true},
        tbar		:[
            {
                id		: 'btnEditRWJ',
                text	: nmEditData,
                tooltip	: nmEditData,
                iconCls	: 'Edit_Tr',
                handler	: function(sm, row, rec){
                    if (rowSelectedKasirRWJ != undefined){
                        RWJLookUp(rowSelectedKasirRWJ.data);
                    }else{
                    	ShowPesanWarningRWJ('Pilih data data tabel  ','Edit data');
                    }
                }
            }
        ]
    });
    PenataJasaRJ.grid1=grListTRRWJ;
    var LegendViewpenatajasa= new Ext.Panel({
        id			: 'LegendViewpenatajasa',
        region		: 'center',
        border		: false,
        bodyStyle	: 'padding:0px 7px 0px 7px',
        layout		: 'column',
        frame		: true,
        anchor		: '100% 8.0001%',
        autoScroll	: false,
        items		:[
            {
                columnWidth	: .033,
                layout		: 'form',
                style		: {'margin-top':'-1px'},
                anchor		: '100% 8.0001%',
                border		: false,
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
            },
            {
                columnWidth	: .08,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Posting"
            },
            {
                columnWidth	: .033,
                layout		: 'form',
                style		:{'margin-top':'-1px'},
                border		: false,
                anchor		: '100% 8.0001%',
                html		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
            },
            {
                columnWidth	: .1,
                layout		: 'form',
                anchor		: '100% 8.0001%',
                style		: {'margin-top':'1px'},
                border		: false,
                html		: " Belum posting"
            }
        ]
    });
    var FormDepanRWJ = new Ext.Panel({
        id			: mod_id,
        closable	: true,
        region		: 'center',
        layout		: 'form',
        title		: 'Penata Jasa ',
        border		: false,
        shadhow		: true,
        autoScroll	: false,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        items		: [            
 		   {
		       xtype		: 'panel',
			   plain		: true,
			   activeTab	: 0,
			   height		: 180,
			   defaults		: {
				   bodyStyle	:'padding:10px',
				   autoScroll	: true
			   },
			   items		: [
        		   {
						layout	: 'form',
						margins	: '0 5 5 0',
						border	: true ,
						items	:[
							 {
								 xtype		: 'textfield',
								 fieldLabel	: ' No. Medrec' + ' ',
								 id			: 'txtFilterNomedrec',
								 anchor 	: '70%',
								 onInit		: function() { },
								 listeners	: {
									 'specialkey' : function(){
										 var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
										 if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
											 if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
												 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue());
												 Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
												 var tmpkriteria = getCriteriaFilter_viDaftar();
												 RefreshDataFilterKasirRWJ();
											 }else{
												 if (tmpNoMedrec.length === 10){
													 RefreshDataFilterKasirRWJ();
												 }else{
													 Ext.getCmp('txtFilterNomedrec').setValue('');
												 }
											 }
										 }
									 }
								 }
							},
							{	 
								xtype	: 'tbspacer',
								height	: 3
							},	
							{
								xtype			: 'textfield',
								fieldLabel		: ' Pasien' + ' ',
								id				: 'TxtFilterGridDataView_NAMA_viKasirRwj',
								anchor 			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
							   		'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},
							{	 
								xtype	: 'tbspacer',
								height	: 3
							},	
							{
								xtype			: 'textfield',
								fieldLabel		: ' Dokter' + ' ',
								id				: 'TxtFilterGridDataView_DOKTER_viKasirRwj',
								anchor			: '70%',
								enableKeyEvents	: true,
								listeners		: { 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
												  RefreshDataFilterKasirRWJ();
										}
									}
								}
							},
							getItemcombo_filter(),getItemPaneltgl_filter()
						]
        		   	}		
				]
 		   	},
			grListTRRWJ,LegendViewpenatajasa]
	});
   return FormDepanRWJ;

};


function mComboStatusBayar_viKasirRwj()
{
  var cboStatus_viKasirRwj = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirRwj',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			anchor :'96.8%',
			emptyText:'',
			fieldLabel: 'Status Posting',
			// width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirRwj,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirRwj=b.data.displayText ;
					// RefreshDataSetDokter();
					RefreshDataFilterKasirRWJ();

				}
			}
		}
	);
	return cboStatus_viKasirRwj;
};


function mComboUnit_viKasirRwj() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirRwj = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirRwj.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" // +"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirRwj = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirRwj',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		     fieldLabel:  ' Poli Tujuan',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirRwj,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirRWJ();

			        // selectStatusCMRWJView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirRwj;
};



function RWJLookUp(rowdata) 
{
    var lebar = 850;
    FormLookUpsdetailTRrwj = new Ext.Window
    (
        {
            id: 'gridRWJ',
            title: 'Penata Jasa Rawat Jalan',
            closeAction: 'destroy',
            width: lebar,
            height: 600,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRRWJ(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );
    
    FormLookUpsdetailTRrwj.show();
    if (rowdata == undefined) 
	{
        RWJAddNew();
    }
    else 
	{
        TRRWJInit(rowdata);
    }

};


function mComboPoliklinik(lebar)
{

    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitRWJ').dom.value +'~)'
            }
        }
    );

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
			width: 170,
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   // alert(b.data.KD_PROPINSI)
                                   loaddatastoredokter(b.data.KD_UNIT);
								   selectKlinikPoli=b.data.KD_UNIT;
                                }
                   


		}
        }
    );

    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    // Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                );
}

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    labelWidth:80,
			fieldLabel: 'Dokter      ',
		    align: 'Right',
                     
// anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
            anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
// var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    // alert("is");
									selectDokter = b.data.KD_DOKTER;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) // atau
															// Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function KonsultasiLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window
    (
        {
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					// width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														// fieldLabel:'Dokter ',
														name: 'txtKdunitKonsultasi',
														id: 'txtKdunitKonsultasi',
														// emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														// hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Kosultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																	Datasave_Konsultasi(false);
																	FormLookUpsdetailTRKonsultasi.close()	;
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close();
															}
														}
											 ]
											
											
											}
											
											
										
									,
									
								]
			},
            listeners:
            {
              
            }
        }
    );

    FormLookUpsdetailTRKonsultasi.show();
  
      KonsultasiAddNew();
	

};
function KonsultasiAddNew() 
{
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnitRWJ').dom.value   ;
	Ext.get('txtnamaunitKonsuldbltasi').dom.value=Ext.get('txtNamaUnit').dom.value;

};

function getFormEntryTRRWJ(lebar,data) 
{
    var pnlTRRWJ = new Ext.FormPanel
    (
        {
            id: 'PanelTRRWJ',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:189,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputRWJ(lebar)],
           tbar:
            [
              
		              
		                
					    	// -------------- ## --------------
        					{
						        text: ' Konsultasi',
						        id:'btnLookUpKonsultasi_viKasirIgd',
						        iconCls: 'Konsultasi',
						        handler: function()
								{
						        	KonsultasiLookUp();
						        }
					    	},
					    	// -------------- ## --------------
					    	{
						        text: ' Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirIgd',
						        iconCls: 'gantidok',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   GantiDokterLookUp();
						        }
					    	},
							{
						        text: 'Ganti Kelompok Pasien',
						        id:'btngantipasien',
						        iconCls: 'gantipasien',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								   KelompokPasienLookUp();
						        }
					    	},
							{
						        text: 'Posting Ke Kasir',
						        id:'btnposting',
						        iconCls: 'gantidok',
						        handler: function(){
						           // FormSetLookupGantiDokter_viKasirIgd('1','2');
								  setpostingtransaksi(data.NO_TRANSAKSI);
						        }
							}
					    	
		               
		          
               
            ]
        }
    );
    var x;
	var GDtabDetailRWJ = new Ext.TabPanel({
        id			:'GDtabDetailRWJ',
        region		: 'center',
        activeTab	: 0,
		height		: 480,
		width 		: 815,
        anchor		: '100% 100%',
        border		: false,
        plain		: true,
        defaults	: {
            autoScroll	: true
		},
        items		: [GetDTLTRAnamnesisGrid(),GetDTLTRRWJGrid(data),GetDTLTRDiagnosaGrid(),PenataJasaRJ.getLabolatorium(data),GetDTLTRRadiologiGrid(data),PenataJasaRJ.getTindakan(data)],
        tbar		:[
			{
				text	: 'Simpan Anamnese',
		        id		: 'btnsimpanAnamnese',
		        iconCls	: 'Konsultasi',
		        handler	: function(){
		        	Datasave_Anamnese(false);	
		        }
			},
            {
				text	: 'Tambah Produk',
				id		: 'btnLookupRWJ',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var p = RecordBaruRWJ();
					var str='';
					if (Ext.get('txtKdUnitRWJ').dom.value  != undefined && Ext.get('txtKdUnitRWJ').dom.value  != ''){
						str =  Ext.get('txtKdUnitRWJ').dom.value;
					};
					FormLookupKasirRWJ(str, dsTRDetailKasirRWJList, p, true, '', true);
				}
			},
			{
				text	: 'Simpan',
				id		: 'btnSimpanRWJ',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					var e=false;
					if(PenataJasaRJ.dsGridTindakan.getRange().length > 0){
						for(var i=0,iLen=PenataJasaRJ.dsGridTindakan.getRange().length; i<iLen ; i++){
							var o=PenataJasaRJ.dsGridTindakan.getRange()[i].data;
							if(o.QTY == '' || o.QTY==0 || o.QTY == null){
								PenataJasaRJ.alertError('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
								e=true;
								break;
							}
							
						}
					}else{
						PenataJasaRJ.alertError('Isi Tindakan Yang Diberikan','Peringatan');
						e=true;
					}
					for(var i=0,iLen=PenataJasaRJ.dsGridObat.getRange().length; i<iLen ; i++){
						var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
						if(o.nama_obat == '' || o.nama_obat == null){
							PenataJasaRJ.alertError('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
							PenataJasaRJ.alertError('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.cara_pakai == ''||  o.cara_pakai == null){
							PenataJasaRJ.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.verified == '' || o.verified==null){
							PenataJasaRJ.alertError('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
//						if(o.racikan == '' || o.racikan == null){
//							PenataJasaRJ.alertError('Terapi Obat : "Racikan" Baris Ke-'+(i+1)+', Wajib Diisi.','Gagal');
//							e=true;
//							break;
//						}
					}
					if(e==false){
						Datasave_KasirRWJ(false);
					}
				}
			},   
			{
                id		:'btnHpsBrsRWJ',
                text	: 'Hapus Baris',
                tooltip	: 'Hapus Baris',
                iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailKasirRWJList.getCount() > 0 ){
                        if (cellSelecteddeskripsi != undefined){
                            if(CurrentKasirRWJ != undefined){
                                    HapusBarisRWJ();
                            }
                        }else{
                            ShowPesanWarningRWJ('Pilih record ','Hapus data');
                        }
                    }
                }
            },
			{
				text	: 'Tambah Diagnosa',
				id		: 'btnLookupDiagnosa',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var p = RecordBaruDiagnosa();
					var str='';
					FormLookupDiagnosa(str, dsTRDetailDiagnosaList, p, true, '', true);
				}
			},
			{
				text	: 'Simpan',
				id		: 'btnSimpanDiagnosa',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
									if (dsTRDetailDiagnosaList.getCount() > 0 ){
										var e=false;
										for(var i=0,iLen=dsTRDetailDiagnosaList.getCount(); i<iLen; i++){
											var o=dsTRDetailDiagnosaList.getRange()[i].data;
											if(o.STAT_DIAG=='' || o.STAT_DIAG==null){
												PenataJasaRJ.alertError('Diagnosa : Diagnosa Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
												e=true;
												break;
											}
											if(o.KASUS=='' || o.KASUS==null){
												PenataJasaRJ.alertError('Diagnosa : Kasus Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
												e=true;
												break;
											}
										}
										if(e==false){
											Datasave_Diagnosa(false);
										}
//										console.log(dsTRDetailDiagnosaList.getRange());
// if (cellSelecteddeskripsi != undefined)
// {
// if(CurrentDiagnosa != undefined)
// {
																 
																
																// FormLookUpsdetailTRDiagnosa.close();
																// RefreshDataFilterDiagnosa();
																
// }
// }
// else
// {
// ShowPesanWarningDiagnosa('Pilih record lalu ubah STAT_DIAG ','Ubah data');
// }
										}
                       
								}
							},
							{
                                id:'btnHpsBrsDiagnosa',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                HapusBarisDiagnosa();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarning('Pilih record ','Hapus data');
                                                }
                                        }
                                }
							},
							{
							text: 'Tambah Baris',
							id: 'btnbarisRad',
							tooltip: nmLookup,
							iconCls: 'find',
							handler: function()
							{
						  TambahBarisRad();
								
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanRad',
								tooltip: nmSimpan,
								iconCls: 'save',
								handler: function()
									{
									
								   datasavepoliklinikrad();
								}
							},   {
                                id:'btnHpsBrsRad',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                 handler: function()
                                {
									
                                        if (dsRwJPJLab.getCount() > 0 )
                                        {
                                                if (cellselectedrad != undefined)
                                                {
                                                        if(CurrentRad != undefined)
                                                        {
                                                                HapusBarisRad();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningRWJ('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },
							
							
							PenataJasaRJ.btn1= new Ext.Button({
								text	: 'Tambah Baris',
								id		: 'RJPJBtnAddLab',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									PenataJasaRJ.ds3.insert(PenataJasaRJ.ds3.getCount(),PenataJasaRJ.nullGrid3());
								}
							}),
							PenataJasaRJ.btn2= new Ext.Button({
								text	: 'Simpan',
								id		: 'RJPJBtnSaveLab',
								tooltip	: nmLookup,
								iconCls	: 'save',
								handler	: function(){
									if(PenataJasaRJ.ds3.getCount()==0){
										PenataJasaRJ.alertError('Laboratorium: Harap isi data Labolatorium.','Peringatan');
									}else{
										var e=false;
										for(var i=0,iLen=PenataJasaRJ.ds3.getCount();i<iLen ; i++){
											if(PenataJasaRJ.ds3.getRange()[i].data.kd_produk=='' || PenataJasaRJ.ds3.getRange()[i].data.kd_produk==null){
												PenataJasaRJ.alertError('Laboratorium: Data Laboratorium baris-'+(i+1)+' tidak Lengkap.','Peringatan');
												e=true;
												break;
											}
										}
										if(e==false){
											var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
											var params={
												kd_pasien 	: o.KD_PASIEN,
												kd_unit 	: o.KD_UNIT,
												tgl_masuk	: o.TANGGAL_TRANSAKSI,
												urut_masuk	: o.URUT_MASUK,
												jum			: PenataJasaRJ.ds3.getCount()
											}
											for(var i=0,iLen=PenataJasaRJ.ds3.getCount();i<iLen ; i++){
												params['kd_produk'+i]=PenataJasaRJ.ds3.getRange()[i].data.kd_produk;
												params['kd_lab'+i]=PenataJasaRJ.ds3.getRange()[i].data.kd_lab;
											}
											Ext.Ajax.request({
												url			: baseURL + "index.php/main/functionRWJ/savelaboratorium",
												params		: params,
												failure		: function(o){
													ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
												},
												success		: function(o){
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) {
														var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
														ShowPesanInfoDiagnosa('Data Berhasil Disimpan', 'Info');
														PenataJasaRJ.ds3.load({
															target	:'ViewGridLabRJPJ',
															param	:par
														});
													}else{
														ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
													}
												}
											});
									    };
												
											
										}
									}
							}),
							PenataJasaRJ.btn3= new Ext.Button({
								text	: 'Hapus',
								id		: 'RJPJBtnDelLab',
								tooltip	: nmLookup,
								iconCls	: 'RemoveRow',
								handler	: function(){
									var line=PenataJasaRJ.grid3.getSelectionModel().selection.cell[0];
									if(PenataJasaRJ.grid3.getSelectionModel().selection==null){
										ShowPesanWarningRWJ('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
									}else{
										Ext.Msg.show
							            (
							                {
							                   title:nmHapusBaris,
							                   msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + PenataJasaRJ.ds3.getRange()[line].data.kd_produk ,
							                   buttons: Ext.MessageBox.YESNO,
							                   fn: function (btn)
							                   {
							                       if (btn =='yes')
							                        {
							                    	   var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
														var params={
															kd_pasien 	: o.KD_PASIEN,
															kd_unit 	: o.KD_UNIT,
															tgl_masuk	: o.TANGGAL_TRANSAKSI,
															urut_masuk	: o.URUT_MASUK,
															kd_produk	: PenataJasaRJ.ds3.getRange()[line].data.kd_produk
														}
														Ext.Ajax.request({
															url			: baseURL + "index.php/main/functionRWJ/deletelaboratorium",
															params		: params,
															failure		: function(o){
																ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
															},
															success		: function(o){
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) {
																	var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
																	ShowPesanInfoDiagnosa('Data Berhasil Dihapus', 'Info');
																	PenataJasaRJ.ds3.load({
																		target	:'ViewGridLabRJPJ',
																		param	:par
																	});
																}else{
																	ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
																}
															}
														});
														PenataJasaRJ.ds3.removeAt(line);
														PenataJasaRJ.grid3.getView().refresh();
							                        }
							                   },
							                   icon: Ext.MessageBox.QUESTION
							                }
							            );
										
									}
									
								}
							}),PenataJasaRJ.btn4= new Ext.Button({
								text	: 'Simpan',
								id		: 'RJPJBtnSaveTin',
								tooltip	: nmLookup,
								iconCls	: 'save',
								handler	: function(){
									alert();
								}
							}),
                ],
		listeners:{
               'tabchange' : function (panel, tab) {
				if (tab.id == 'tabDiagnosa'){
					Ext.getCmp('catLainGroup').hide();
					Ext.getCmp('txtneoplasma').hide();
					Ext.getCmp('txtkecelakaan').hide();
					Ext.getCmp('btnsimpanAnamnese').hide();
					dsCmbRwJPJDiag.loadData([],false);
					for(var i=0,iLen=PenataJasaRJ.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbRwJPJDiag.recordType;
						var o=PenataJasaRJ.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbRwJPJDiag.add(recs);
					}
		
	                 Ext.getCmp('btnHpsBrsDiagnosa').show();
					 Ext.getCmp('btnSimpanDiagnosa').show();
					 Ext.getCmp('btnLookupDiagnosa').show();
				 
	                 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
				 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
				 
				 	PenataJasaRJ.btn4.hide();
				}else if(tab.id == 'tabTransaksi'){
					 Ext.getCmp('btnsimpanAnamnese').hide();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').show();
					 Ext.getCmp('btnSimpanRWJ').show();
					 Ext.getCmp('btnLookupRWJ').show();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
				}else if(tab.id == 'tabAnamnses'){
					 Ext.getCmp('btnsimpanAnamnese').show();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabLaboratorium'){
					PenataJasaRJ.ds4.load({ 
						params	: { 
							Skip	: 0, 
							Take	: 50, 
							target	:'ViewComboLabRJPJ'
						} 
					});
					var o=PenataJasaRJ.grid1.getSelectionModel().getSelections()[0].data;
					var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
					PenataJasaRJ.ds3.load({
						params	: { 
							Skip	: 0, 
							Take	: 50, 
							target	:'ViewGridLabRJPJ',
							param	:par
						}
					});
					
					PenataJasaRJ.btn1.show();
					PenataJasaRJ.btn2.show();
					PenataJasaRJ.btn3.show();
					
					Ext.getCmp('btnsimpanAnamnese').hide();
					
					Ext.getCmp('btnHpsBrsDiagnosa').hide();
					Ext.getCmp('btnSimpanDiagnosa').hide();
					Ext.getCmp('btnLookupDiagnosa').hide();
					
					Ext.getCmp('btnHpsBrsRWJ').hide();
					Ext.getCmp('btnSimpanRWJ').hide();
					Ext.getCmp('btnLookupRWJ').hide();
					
					PenataJasaRJ.btn4.hide();
				}else if(tab.id=='tabTindakan'){
					PenataJasaRJ.ds5.load({ 
						params	: { 
							Skip	: 0, 
							Take	: 50, 
							target	:'ViewComboTindakanRJPJ'
						} 
					});
					
					Ext.getCmp('btnsimpanAnamnese').hide();
					 
					 Ext.getCmp('btnHpsBrsDiagnosa').hide();
					 Ext.getCmp('btnSimpanDiagnosa').hide();
					 Ext.getCmp('btnLookupDiagnosa').hide();
					 
					 Ext.getCmp('btnHpsBrsRWJ').hide();
					 Ext.getCmp('btnSimpanRWJ').hide();
					 Ext.getCmp('btnLookupRWJ').hide();
					 
					 PenataJasaRJ.btn1.hide();
					 PenataJasaRJ.btn2.hide();
					 PenataJasaRJ.btn3.hide();
					 
					 PenataJasaRJ.btn4.show();
				}
			}
		// bbar : /**/
        }
		}
		
    );
   
   var pnlTRRWJ2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRRWJ2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:360,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailRWJ
			
			]
        }
    );

	
   
   
    var FormDepanRWJ = new Ext.Panel
	(
		{
		    id: 'FormDepanRWJ',
		    region: 'center',
		   // width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
			resizable:false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRRWJ,pnlTRRWJ2	
				

			]

		}
	);
	
	if( data.POSTING_TRANSAKSI == 't')
	{
	setdisablebutton();
	}
	else
	{
	setenablebutton();	
	// Ext.getCmp('PanelTRRWJ').enable();
	}


    return FormDepanRWJ;
};

function RecordBaruDiagnosa()
{
	var p = new mRecordDiagnosa
	(
		{
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		  // 'KD_TARIF':'',
		   // 'HARGA':'',
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    // 'DESC_REQ':'',
		   // 'KD_TARIF':'',
		    'URUT_MASUK':''
		}
	);
	
	return p;
};

function HapusBarisDiagnosa()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != '')
        {
        	console.log(PenataJasaRJ.grid2.getSelectionModel());
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                    	   dsCmbRwJPJDiag.removeAt(PenataJasaRJ.grid2.getSelectionModel().selection.cell[0]);
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail()
{
    Ext.Ajax.request
    (
        {
            
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                  RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
                }
           
                else
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    );
};

function getParamDataDeleteDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT
    };
	
    return params;
};



function getParamDetailTransaksiDiagnosa2() 
{
	console.log();
    var params =
	{
		Table:'ViewTrDiagnosa',
		KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk:Ext.get('txtKdUrutMasuk').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
// List:getArrDetailTrDiagnosa(),
// JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa()
// jumDiag:dsTRDetailDiagnosaList.getCount(),
// Hapus:1,
// Ubah:0
	};
    
    var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		params['URUT_MASUK'+i]=dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
		params['KD_PENYAKIT'+i]=dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
		params['STAT_DIAG'+i]=dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
		params['KASUS'+i]=dsTRDetailDiagnosaList.data.items[i].data.KASUS;
		params['DETAIL'+i]=dsTRDetailDiagnosaList.data.items[i].data.DETAIL;
		params['NOTE'+i]=dsTRDetailDiagnosaList.data.items[i].data.NOTE;
// var y='';
// var z='@@##$$@@';
//			
// y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
// y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
// y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
// y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS;
//			
//			
// if (i === (dsTRDetailDiagnosaList.getCount()-1))
// {
// x += y ;
// }
// else
// {
// x += y + '##[[]]##';
// };
	}	
    return params;
};


function GetListCountDetailDiagnosa()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG;
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS;
			
			
			if (i === (dsTRDetailDiagnosaList.getCount()-1))
			{
				x += y ;
			}
			else
			{
				x += y + '##[[]]##';
			};
		};
	}	
	
	return x;
};

function getArrdetailAnamnese()
{
	var x = '';
		var y='';
		var z='::';
	var hasil;
	for(var k = 0; k < dsTRDetailAnamneseList.getCount(); k++)
	{
	
	if (dsTRDetailAnamneseList.data.items[k].data.HASIL == null)
		{
			// hasil = 0;
			x += '';
		}
		else
		{
			hasil = dsTRDetailAnamneseList.data.items[k].data.HASIL;
		
			y = dsTRDetailAnamneseList.data.items[k].data.ID_KONDISI;
			y += z + hasil;
			// y += z + dsTRDetailDiagnosaList.data.items[k].data.STAT_DIAG
			// y += z + dsTRDetailDiagnosaList.data.items[k].data.KASUS
			
			
			/*
			 * if (k === (dsTRDetailDiagnosaList.getCount()-1)) { x += y + '<>' }
			 * else { x += y + '<>' };
			 */
		}
		x += y + '<>';
		
	}
	
	return x;
	
}



function Datasave_Diagnosa(mBol){	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/saveDiagnosaPoliklinik",
		params: getParamDetailTransaksiDiagnosa2(),
		success: function(o){
	
			var cst = Ext.decode(o.responseText);
			
			if (cst.success === true) 
			{
				ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
				// RefreshDataDiagnosa();
				if(mBol === false)
				{
			
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
					
				};
			}
			else if  (cst.success === false && cst.pesan===0)
			{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
			}
			
			else 
			{
				RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
				ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
			};
		}
	});
};


function Datasave_Anamnese(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					// url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDetailAnamnese(),
					success: function(o) 
					{
	
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoAnamnese(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataFilterKasirRWJ();
							// RefreshDataDiagnosa();
							if(mBol === false)
							{
						
							RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							
								
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							RefreshDataSetAnamnese(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanWarningAnamnese(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						
						else 
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitRWJ').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanErrorAnamnese(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			);
		
	
};



function ShowPesanWarningDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function ShowPesanInfoAnamnese(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: "Data Anamnese Ada Masalah",
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: "Data Gagal Disimpan",
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoAnamnese(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: "Data Sukses diSimpan",
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};



function GetDTLTRRadiologiGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Radiologi',
		id:'tabradiologi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetGridRwJPJRad(data)]
    });

	return tabTransaksi;
};



PenataJasaRJ.getLabolatorium=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Laboratorium',
		id:'tabLaboratorium',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [$this.getGrid3(data)]
    });
	return tabTransaksi;
};

PenataJasaRJ.getTindakan=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Tindakan',
		id:'tabTindakan',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [
			{
				layout	: 'column',
			    bodyStyle: 'margin-top: 10px;',
			    border	: false,
			   
				items:[{
					 layout: 'form',
						labelWidth:150,
						labelAlign:'right',
					    border: false,
				       items	: [
							$this.iCombo1= new Ext.form.ComboBox({
								id				: 'iComboStatusTindakanRJPJ',
								typeAhead		: true,
							    triggerAction	: 'all',
							    lazyRender		: true,
							    mode			: 'local',
							    emptyText		: '',
							    width			: 300,
								store			: $this.ds5,
								valueField		: 'status',
								displayField	: 'status',
								value			: '',
								fieldLabel		: 'Status &nbsp;',
								listeners		: {
									select	: function(a, b, c){	
										alert(b.json.id_status);
		//								var line	= $this.gridObat.getSelectionModel().selection.cell[0];
		//								$this.dsGridObat.getRange()[line].data.kd_prd=b.json.kd_prd;
		//								$this.dsGridObat.getRange()[line].data.satuan=b.json.satuan;
		//								$this.dsGridObat.getRange()[line].data.kd_dokter=b.json.nama;
		//								$this.dsGridObat.getRange()[line].data.nama_obat=b.json.nama_obat;
		//								$this.gridObat.getView().refresh();
		//								console.log($this.s1);
								    }
								}
							})
						]}
				]
			}
        ]
    });
	return tabTransaksi;
};

PenataJasaRJ.getGrid3=function(data){
	var $this=this;
	PenataJasaRJ.ds3 = new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab'] });
	PenataJasaRJ.grid3 = new Ext.grid.EditorGridPanel({
        title: 'Laboratorium',
		id:'grid3',
        stripeRows: true,
        height: 290,
        store: PenataJasaRJ.ds3,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        cm: $this.getModel1(),
        viewConfig:{forceFit: true}
    });
    return PenataJasaRJ.grid3;
};

function GetGridRwJPJRad(data){
	
	var fldDetail = ['CITO','KODE','KELAS','DESKRIPSI','DOKTER'];
	dsRwJPJLab = new WebApp.DataStore({ fields: fldDetail });
    var gridDTLTRRWJ = new Ext.grid.EditorGridPanel({
        title: 'Radiologi',
		id:'gridRwJPJRad',
        stripeRows: true,
        height: 290,
        store: dsRwJPJLab,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        cm: getModelRwJPJRad(),
        viewConfig:{forceFit: true}
    });
    
    return gridDTLTRRWJ;
}
PenataJasaRJ.getModel1=function(){
	var $this=this;
	return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: 'col1',
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width		: 80,
			menuDisabled: true,
            hidden		: false,
            editor		: $this.iCombo1= new Ext.form.ComboBox({
        		id				: 'combo1',
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: $this.ds4,
        		valueField		: 'kd_produk',
        		hideTrigger		: true,
        		displayField	: 'kd_produk',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){	
        				var line	= $this.grid3.getSelectionModel().selection.cell[0];
        				$this.ds3.getRange()[line].data.kd_produk=b.json.kd_produk;
        				$this.ds3.getRange()[line].data.kd_klas=b.json.kd_klas;
        				$this.ds3.getRange()[line].data.deskripsi=b.json.deskripsi;
        				$this.ds3.getRange()[line].data.username=b.json.username;
        				$this.ds3.getRange()[line].data.kd_lab=b.json.kd_lab;
        				$this.grid3.getView().refresh();
        		    }
        		}
        	})
        },
        {
			id			: 'col2',
        	header		: 'KELAS',
            dataIndex	: 'kd_klas',
            width		: 100,
			menuDisabled: true,
            hidden		: false
        },
        {
            id			: 'col3',
            header		: 'DESKRIPSI',
            dataIndex	: 'deskripsi',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
			width		: 150,
			editor		: $this.iCombo2= new Ext.form.ComboBox({
        		id				: 'iCombo2',
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: $this.ds4,
        		valueField		: 'deskripsi',
        		hideTrigger		: true,
        		displayField	: 'deskripsi',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){	
        				var line	= $this.grid3.getSelectionModel().selection.cell[0];
        				$this.ds3.getRange()[line].data.kd_produk=b.json.kd_produk;
        				$this.ds3.getRange()[line].data.kd_klas=b.json.kd_klas;
        				$this.ds3.getRange()[line].data.deskripsi=b.json.deskripsi;
        				$this.ds3.getRange()[line].data.username=b.json.username;
        				$this.ds3.getRange()[line].data.kd_lab=b.json.kd_lab;
        				$this.grid3.getView().refresh();
        		    }
        		}
        	})
        },
        {
            id			: 'col4',
            header		: 'DOKTER',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'username'	
        }
    ]);
};

function getModelRwJPJRad(){
	
	return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id: 'colCitoRwJPJRad',
        	header: 'CITO',
            dataIndex: 'CITO',
            width:80,
			menuDisabled:true,
            hidden:false
        },
        {
			id: 'colKodeRwJPJRad',
        	header: 'Kode',
            dataIndex: 'KODE',
            width:150,
			menuDisabled:true,
            hidden:false
        },
        {
            id: 'colKelasRwJPJRad',
            header:'Kelas',
            dataIndex: 'KELAS',
            sortable: false,
            hidden:false,
			menuDisabled:true,
            width:50
        },
        {
            id: 'colDescRwJPJRad',
            header: 'Deskripsi',
			hidden: false,
			menuDisabled:true,
            dataIndex: 'DEKSRIPSI'	
        },
        {
            id: 'colDokterRwJPJRad',
            header: 'Dokter',
			hidden: false,
			menuDisabled:true
        }
    ]);
}

function GetDTLTRAnamnesisGrid() 
{
	var pnlTRAnamnese = new Ext.Panel
    (
        {
			title: 'Anamnese',
			id:'tabAnamnses',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            // bodyStyle: 'padding:10px 10px 10px 10px',
            height:100,
            anchor: '100%',
            width: 260,
            border: false,
            items: [	GetDTLTRAnamnesis(),textareacatatanAnemneses()
			
			// getItemAnamnese()
			
			],
			tbar:
              [
              		
						{
					    xtype: 'textarea',
					    fieldLabel:'Anamnesis  ',
					    name: 'txtareaAnamnesis',
					    id: 'txtareaAnamnesis',
						width:812,
						// emptyText:nmNomorOtomatis,
						readOnly:false,
					    anchor: '99%'
						},
		]
        }
    );
	

	return pnlTRAnamnese;
}


function textareacatatanAnemneses()
{
		
	var TextAreaCatatanAnamnese = new Ext.Panel
	(
	{
			title: 'Catatan Fisik',
			id:'tabtextAnamnses',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            // bodyStyle: 'padding:10px 10px 10px 10px',
            height:130,
            anchor: '100%',
            width: 815,
            border: false,
            items: [	
			{
						xtype: 'textarea',
					    fieldLabel:'Catatan',
					    name: 'txtareaAnamnesiscatatan',
					    id: 'txtareaAnamnesiscatatan',
						width:815,
						height:85,
						readOnly:false,
					    anchor: '100%'
			}
			]
	}
	
	);
	return TextAreaCatatanAnamnese;
	
}


function GetDTLTRAnamnesis() 
{
		
    var Anamfied = ['ID_KONDISI','KONDISI','SATUAN','ORDERLIST','KD_UNIT','HASIL'];
	
    dsTRDetailAnamneseList = new WebApp.DataStore({ fields: Anamfied });
	
    var gridDTLTRAnamnese = new Ext.grid.EditorGridPanel
    (
        {
            // title: 'Anamnese',
			id:'tabcatAnamnses',
            stripeRows: true,
            store: dsTRDetailAnamneseList,
            border: true,
            columnLines: true,
            frame: false,
			height:120,
            anchor: '100%',
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            // cellSelecteddeskripsi =
							// dsTRDetailAnamneseList.getAt(row);
                            // CurrentAnamnesese.row = row;
                            // CurrentAnamnesese.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRAnamneseColumModel(),
			
			
		
			
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRAnamnese;
};



function TRAnamneseColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
			{
				id: 'colKondisi',
				header : 'KONDISI',
				dataindex:'KONDISI',
				menuDisbaled : true,
				width:100,
				hidden:false
			},
			{
                id: 'colNilai',
                header: 'NILAI',
                dataIndex: 'HASIL',
				menuDisabled:true,
				hidden:false,
				width:80,
				   editor: new Ext.form.TextField
                (
                    {
							id:'textnilai',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Masukan Nilai...',
							// fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                )
            },
			{
                id: 'colkdunit',
                header: 'KD UNIT',
                dataIndex: 'KD_UNIT',
				menuDisabled:true,
				hidden:true,
				width:80
                
            },
			{
                id: 'colSatuan',
                header: 'SATUAN',
                dataIndex: 'SATUAN',
				menuDisabled:true,
				width:80
                
            },
			{
                id: 'colorderlist',
                header: 'ORDER LIST',
                dataIndex: 'ORDERLIST',
				menuDisabled:true,
				hidden:true,
				width:80
                
            },

        ]
		
	
		
    );
};


function GetDTLTRDiagnosaGrid() 
{
	var pnlTRDiagnosa = new Ext.Panel
    (
        {
			title: 'Diagnosa',
			id:'tabDiagnosa',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            // bodyStyle: 'padding:10px 10px 10px 10px',
            height:100,
            anchor: '100%',
            width: 815,
            border: false,
            items: [	GetDTLTRDiagnosaGridFirst(),FieldKeteranganDiagnosa()]
        }
    );
	

	return pnlTRDiagnosa;
}


function FieldKeteranganDiagnosa(){
	
	dsCmbRwJPJDiag = new Ext.data.ArrayStore({
		id: 0,
		fields:[
			'Id',
			'displayText'
		],
		data: []
	});
    var items ={
	    layout: 'column',
	    border: true,
	    width: 815,
	    bodyStyle:'margin-top:5px;padding: 5px;',
	    items:[
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					combo = new Ext.form.ComboBox({
						id:'cmbRwJPJDiag',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						editable: false,
						mode: 'local',
						width: 150,
						emptyText:'',
						fieldLabel: 'Kode Penyakit &nbsp;',
						store: dsCmbRwJPJDiag,
						valueField: 'Id',
						displayField: 'displayText',
						listeners:{
							select: function(){
								if(this.getValue() != ''){
									Ext.getCmp('catLainGroup').show();
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==2){
												Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtkecelakaan').show();
												Ext.getCmp('txtneoplasma').hide();
												Ext.get('cbxkecelakaan').dom.checked=true;
											}else if(dsTRDetailDiagnosaList.getRange()[j].data.NOTE==1){
												Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
												Ext.getCmp('txtneoplasma').show();
												Ext.getCmp('txtkecelakaan').hide();
												Ext.get('cbxneoplasma').dom.checked=true;
											}else{
												Ext.get('cbxlain').dom.checked=true;
												Ext.getCmp('txtkecelakaan').hide();
												Ext.getCmp('txtneoplasma').hide();
											}
										}
									}
								}
									
							}
						}
					}),
					{
						xtype: 'radiogroup',
						width:300,
						fieldLabel: 'Catatan Lain &nbsp;',
						id:'catLainGroup',
						name: 'mycbxgrp',
						columns: 3,
						items: [
							{ 
								id: 'cbxlain', 
								boxLabel: 'Lain-lain', 
								name: 'mycbxgrp', 
								width:70, 
								inputValue: 1
							 },
							{ 
								id: 'cbxneoplasma', 
								boxLabel: 'Neoplasma', 
								name: 'mycbxgrp',  
								width:100, 
								inputValue: 2 
							},
							{ 
								id: 'cbxkecelakaan', 
								boxLabel: 'Kecelakaan', 
								name: 'mycbxgrp', 
								width:100, 
								inputValue: 3 
							}
					   ],
					     listeners: {
		             		change: function(radiogroup, radio){
		             			if(Ext.getDom('cbxlain').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').hide();
									
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											dsTRDetailDiagnosaList.getRange()[j].data.DETAIL='';
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=0;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxneoplasma').checked == true){
									Ext.getCmp('txtneoplasma').show();
									Ext.getCmp('txtneoplasma').setValue('');
									Ext.getCmp('txtkecelakaan').hide();
									
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtneoplasma').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxkecelakaan').checked == true){
									Ext.getCmp('txtneoplasma').hide();
									Ext.getCmp('txtkecelakaan').show();
									Ext.getCmp('txtkecelakaan').setValue('');
									
									for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
											Ext.getCmp('txtkecelakaan').setValue(dsTRDetailDiagnosaList.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}
							}
		                }
						},
						{
							  xtype: 'textfield',
							  fieldLabel:'Neoplasma &nbsp;',
							  name: 'txtneoplasma',
							  id: 'txtneoplasma',
							  hidden:true,
							  width:600,
							  listeners:{
								  blur: function(){
									  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
										  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
												if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
													dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
													dsTRDetailDiagnosaList.getRange()[j].data.NOTE=1;
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
												}
											}
									  }
									  	
								  }
							  }
					       },
					        {
					        	xtype: 'textfield',
					        	fieldLabel:'Kecelakaan / Keracunan &nbsp;',
					        	name: 'txtkecelakaan',
					        	id: 'txtkecelakaan',
					        	hidden:true,
					        	width:600,
						    	listeners:{
									  blur: function(){
										  if(Ext.getCmp('cmbRwJPJDiag').getValue() != ''){
											  	for(var j=0,jLen=dsTRDetailDiagnosaList.getRange().length; j< jLen; j++){
													if(dsTRDetailDiagnosaList.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbRwJPJDiag').getValue()){
														dsTRDetailDiagnosaList.getRange()[j].data.DETAIL=this.getValue();
														dsTRDetailDiagnosaList.getRange()[j].data.NOTE=2;
														Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													}
												}
										  }
										  	
									  }
								  }
					        }
				
				]
			},
		]
	};
    
	return items;
}
//strKriteriaDiagnosa = 'kd_pasien = ~' + medrec + '~ and kd_unit=~'+unit+'~ and tgl_masuk in(~'+tgl+'~)';

function GetDTLTRDiagnosaGridFirst(){
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG','NOTE','DETAIL'];
	console.log(PenataJasaRJ.s1);
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaRJ.ds2=dsTRDetailDiagnosaList;
    RefreshDataSetDiagnosa(PenataJasaRJ.s1.data.KD_PASIEN,PenataJasaRJ.s1.data.KD_UNIT,PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI);
    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
			id:'tabDiagnosaGrid',
            store: dsTRDetailDiagnosaList,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100% 100%',
            autoScroll:true,
			height:150,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                            CurrentDiagnosa.row = row;
                            CurrentDiagnosa.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel(),
			
			viewConfig:{forceFit: true}
        }
		
		
    );
	
    PenataJasaRJ.grid2=gridDTLTRDiagnosa;

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel() 
{
    return new Ext.grid.ColumnModel
    (
	   
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdProduk',
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
				menuDisabled:true,
                hidden:false
            },
			{
                id: 'colePenyakitDiagnosa',
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
					menuDisabled:true,
				width:200
                
            }
            ,
			{
                id: 'colePasien',
                header: 'kd_pasien',
                dataIndex: 'KD_PASIEN',
				hidden:true
                
            }
			,
			{
                id: 'coleurut',
                header: 'urut',
                dataIndex: 'URUT',
				hidden:true
                
            }
			,
			{
                id: 'coleurutmasuk',
                header: 'urut masuk',
                dataIndex: 'URUT_MASUK',
				hidden:true
                
            }
            ,
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: 'TGL_MASUK',
				hidden:true
                
            }
            ,
            {
                id: 'colProblemDiagnosa',
                header: 'Diagnosa',
                width:130,
				menuDisabled:true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							// fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                )
				
				
              
            },
            {
                id: 'colKasusDiagnosa',
                header: 'Kasus',
                width:130,
				// align: 'right',
				// hidden :true,
				menuDisabled:true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							// fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                )
				
				
              
            },
            {
                id: 'colNote',
                header: 'Note',
                dataIndex: 'NOTE',
                width:70,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colKdProduk',
                header: 'Detail',
                dataIndex: 'DETAIL',
                width:70,
				menuDisabled:true,
                hidden:true
            },
			

        ]
    );
};

// /---------------------------------------------------------------------------------------///
function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',

        items: 
            [
                {
                    x:0,
                    y:60,
                    xtype: 'textarea',
                    id:'TxtHistoriDeleteDataPasien',
                    hideLabel: true,
                    name: 'msg',
                    anchor: '100% 100%'  // anchor width by percentage and
											// height by raw adjustment
                }

            ]
    });
}
function TambahBarisRWJ()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirRWJList.insert(dsTRDetailKasirRWJList.getCount(), p);
    };
};

function HapusBarisRWJ()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														// alert(variablehistori=text);
														// process text value
														// and close...
														DataDeleteKasirRWJDetail();
														dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
														}
												});

												
                                         
                                
               
        }
        else
        {
			// dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
            dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
        };
    }
};

function DataDeleteKasirRWJDetail()
{
    Ext.Ajax.request
    (
        {
                // url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirRWJDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirRWJList.removeAt(CurrentKasirRWJ.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
                    AddNewKasirRWJ = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningRWJ(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningRWJ(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    );
};

function getParamDataDeleteKasirRWJDetail()
{
    var params =
    {
		Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirRWJ.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirRWJ.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitRWJ').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirRWJ.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirRWJ.data.data.HARGA,
		
		TrKdProduk :	 CurrentKasirRWJ.data.data.KD_PRODUK,
        RowReq: CurrentKasirRWJ.data.data.URUT,
        Hapus:2
    };
	
    return params;
};

function getParamDataupdateKasirRWJDetail()
{
    var params =
    {
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentKasirRWJ.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasirRWJ.data.data.URUT,

        Qty: CurrentKasirRWJ.data.data.QTY,
        Ubah:1
    };
	
    return params;
};

function GetDTLTRRWJGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Transaksi',
		id:'tabTransaksi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetDTLTRRWJGridSecond(data),GetDTLTRRWJGridFirst(data)]
    });

	return tabTransaksi;
}

function GetDTLTRRWJGridFirst(data){
	
	var fldDetail = ['kd_prd','nama_obat','jumlah','satuan','cara_pakai','kd_dokter','nama','verified','racikan'];
	dsPjTrans2 = new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetai2(data) ;
    var gridDTLTRRWJ = new Ext.grid.EditorGridPanel({
        title: 'Terapi Obat',
		id:'PjTransGrid1',
        stripeRows: true,
        height: 170,
        store: dsPjTrans2,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    cellSelecteddeskripsi = dsTRDetailKasirRWJList.getAt(row);
                    CurrentKasirRWJ.row = row;
                    CurrentKasirRWJ.data = cellSelecteddeskripsi;
                }
            }
        }),
        cm: TRRawatJalanColumModel(),
        viewConfig:{forceFit: true},
        tbar:[
					{
						text: 'Tambah Obat',
						id: 'btnLookupRWJ',
						iconCls: 'find',
						handler: function(){
							PenataJasaRJ.dsGridObat.insert(PenataJasaRJ.dsGridObat.getCount(),PenataJasaRJ.nullGridObat());
						}
					},
					{
						text: 'Hapus',
						id: 'thuijk',
						iconCls: 'RemoveRow',
						handler: function()
						{
							Ext.Msg.show
				            (
				                {
				                   title:nmHapusBaris,
				                   msg: 'Anda yakin akan menghapus data Obat ini?',
				                   buttons: Ext.MessageBox.YESNO,
				                   fn: function (btn)
				                   {
				                       if (btn =='yes')
				                        {
				                    	   PenataJasaRJ.dsGridObat.removeAt(PenataJasaRJ.gridObat.getSelectionModel().selection.cell[0]);
											PenataJasaRJ.gridObat.getView().refresh();
				                        }
				                   },
				                   icon: Ext.MessageBox.QUESTION
				                }
				            );
						}
					}
            ]}
    );
    PenataJasaRJ.gridObat=gridDTLTRRWJ;
    PenataJasaRJ.dsGridObat=dsPjTrans2;
    return gridDTLTRRWJ;
};

function GetDTLTRRWJGridSecond(){
	var fldDetail	= ['KD_PRODUK','DESKRIPSI','QTY','DOKTER','TGL_TINDAKAN','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','KD_TARIF','HARGA'];
	dsTRDetailKasirRWJList	= new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirRWJDetail() ;
    var gridDTLTRRWJ1	= new Ext.grid.EditorGridPanel({
        title		: 'Tidakan Yang Diberikan',
		id			: 'PjTransGrid2',
		stripeRows	: true,
		height		: 130,
        store		: dsTRDetailKasirRWJList,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        sm			: new Ext.grid.CellSelectionModel({
	        singleSelect: true,
	        listeners	: {
	            cellselect	: function(sm, row, rec){
	                cellSelecteddeskripsi	= dsTRDetailKasirRWJList.getAt(row);
	                CurrentKasirRWJ.row	= row;
	                CurrentKasirRWJ.data	= cellSelecteddeskripsi;
	            }
	        }
        }),
        cm			: TRRawatJalanColumModel2(),
        viewConfig	: {forceFit: true}
    });
    PenataJasaRJ.dsGridTindakan	= dsTRDetailKasirRWJList;
    return gridDTLTRRWJ1;
};

function TRRawatJalanColumModel(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: 'PjTransGrid1Code',
        	header		: 'KD.Obat',
            dataIndex	: 'kd_prd',
            width		: 80,
			menuDisabled: true,
            hidden		: false
        },
        {
			id			: 'PjTransGrid1Tin',
        	header		: 'Nama Obat',
            dataIndex	: 'nama_obat',
            width		: 150,
			menuDisabled: true,
            hidden		: false,
            editor		: PenataJasaRJ.comboObat()
        },
        {
            id			: 'PjTransGrid1Qty',
            header		: 'Qty',
            dataIndex	: 'jumlah',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
            width		: 50,
            editor		: new Ext.form.NumberField({
				id				: 'txtQty',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },
        {
            id			: 'PjTransGrid1Satuan',
            header		: 'Satuan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'satuan'	
        },
        {
            id			: 'PjTransGrid1CaraPakai',
            header		: 'Cara Pakai',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'cara_pakai'	,
        	editor		: new Ext.form.TextField({
				id				: 'txtcarapakai',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },
        {
            id			: 'PjTransGrid1Dokter',
            header		: 'Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'nama'	
        },
        {
            id			: 'PjTransGrid1Verified',
            header		: 'Verified',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'verified',
            editor		: PenataJasaRJ.ComboVerifiedObat()
        },
        {
            id			: 'PjTransGrid1Racikan',
            header		: 'Racikan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'racikan',
            editor		: new Ext.form.NumberField({
				id				: 'txtRacikan',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },
    ]);
};

function TRRawatJalanColumModel2(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
        	 id				: 'coleskripsirwj2',
        	 header			: 'Uraian',
        	 dataIndex		: 'DESKRIPSI2',
        	 menuDisabled	: true,
        	 hidden 		: true
        },
        {
        	id				: 'colKdProduk2',
            header			: 'Kode Produk',
            dataIndex		: 'KD_PRODUK',
            width			: 100,
			menuDisabled	: true,
            hidden			: true
        },
        {
        	id			: 'colDeskripsiRWJ2',
            header		:'Nama Produk',
            dataIndex	: 'DESKRIPSI',
            sortable	: false,
            hidden		:false,
			menuDisabled:true,
            width		:250
        },
        {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
              width:100,
			   	menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj2',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
              // width:.10,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemRWJ2',
                header: 'Qty',
               width:'100%',
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ2',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:100,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirRWJ(false);
											// RefreshDataFilterKasirRWJ();
									        // RefreshDataFilterKasirRWJ();

								}
							}
                    }
                )
				
				
              
            },

            {
                id: 'colImpactRWJ2',
                header: 'CR',
               // width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ2',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    );
};

function GetLookupAssetCMRWJ(str)
{
	if (AddNewKasirRWJ === true)
	{
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':''
			}
		);
		
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,true,'',false);
	}
	else
	{	
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirRWJList.data.items[CurrentKasirRWJ.row].data.URUT
			}
		);
	
		FormLookupKasirRWJ(str,dsTRDetailKasirRWJList,p,false,CurrentKasirRWJ.row,false);
	};
};

function RecordBaruRWJ()
{

	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function RefreshDataSetDiagnosa(medrec,unit,tgl)
{	
 var strKriteriaDiagnosa='';
    // strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = 'A.kd_pasien = ~' + medrec + '~ and A.kd_unit=~'+unit+'~ and A.tgl_masuk in(~'+tgl+'~)';
    // strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountDiagnosa, 
				// Sort: 'EMP_ID',
                Sort: 'kd_penyakit',
				Sortdir: 'ASC', 
				target:'ViewDiagnosaRJPJ',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
};


function RefreshDataSetAnamnese()
{	
 var strKriteriaDiagnosa='';
    // strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
   strKriteriaDiagnosa = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksi').getValue() + '~ and mr_konpas.kd_unit=~'+Ext.get('txtKdUnitRWJ').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi').dom.value+'~';
    // strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailAnamneseList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 50, 
				// Sort: 'EMP_ID',
                Sort: 'orderlist',
				Sortdir: 'ASC', 
				target:'viewkondisifisik',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
};

function TRRWJInit(rowdata)
{
    AddNewKasirRWJ = false;
	
	Ext.get('txtNoTransaksiKasirrwj').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitRWJ').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
	Ext.get('txtareaAnamnesis').dom.value = rowdata.ANAMNESE;
	Ext.get('txtareaAnamnesiscatatan').dom.value = rowdata.CAT_FISIK;
// Ext.get('txtCustomerLama').dom.value=rowdata.CUSTOMER;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	RefreshDataKasirRWJDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetAnamnese();
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request(
	{
	    // url: "./home.mvc/getModule",
	    // url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        // UserID: 'Admin',
	        command: '0'
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        // var cst = Ext.decode(o.responseText);
			tampungshiftsekarang=o.responseText;
			// Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
	// ,,
	
	
};

function mEnabledRWJCM(mBol)
{
// Ext.get('btnSimpanRWJ').dom.disabled=mBol;
// Ext.get('btnSimpanKeluarRWJ').dom.disabled=mBol;
// Ext.get('btnHapusRWJ').dom.disabled=mBol;
	 Ext.get('btnLookupRWJ').dom.disabled=mBol;
// Ext.get('btnTambahBrsRWJ').dom.disabled=mBol;
	 Ext.get('btnHpsBrsRWJ').dom.disabled=mBol;
};


// /---------------------------------------------------------------------------------------///
function RWJAddNew() 
{
    AddNewKasirRWJ = true;
	Ext.get('txtNoTransaksiKasirrwj').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirRwj').dom.value= '';
	rowSelectedKasirRWJ=undefined;
	dsTRDetailKasirRWJList.removeAll();
	mEnabledRWJCM(false);
	

};

function RefreshDataKasirRWJDetai2(data){
    dsPjTrans2.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'kd_obat',
		    Sortdir: 'ASC',
		    target: 'ViewResepRWJ',
		    param: "KD_PASIEN='"+data.KD_PASIEN+"' AND KD_UNIT = '"+data.KD_UNIT+"' AND TGL_MASUK = '"+data.TANGGAL_TRANSAKSI+"'"
		}
	});
    return dsPjTrans2;
};

function RefreshDataKasirRWJDetail(no_transaksi) 
{
    var strKriteriaRWJ='';
    // strKriteriaRWJ = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~01~";
    // strKriteriaRWJ = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirRWJList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    // Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailRWJGridBawah',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailKasirRWJList;
};

// /---------------------------------------------------------------------------------------///



// /---------------------------------------------------------------------------------------///
function getParamDetailTransaksiRWJ(){
    var params={
		Table			: 'ViewTrKasirRwj',
		TrKodeTranskasi	: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit			: Ext.get('txtKdUnitRWJ').getValue(),
		// DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl				: PenataJasaRJ.s1.data.TANGGAL_TRANSAKSI,
		Shift			: tampungshiftsekarang,
		List			: getArrDetailTrRWJ(),
		JmlField		: mRecordRwj.prototype.fields.length-4,
		JmlList			: GetListCountDetailTransaksi(),
		Hapus			: 1,
		Ubah			: 0
	};
    params.jmlObat	= PenataJasaRJ.dsGridObat.getRange().length;
    params.urut_masuk	= PenataJasaRJ.s1.data.URUT_MASUK;
    params.kd_pasien	= PenataJasaRJ.s1.data.KD_PASIEN;
    console.log(PenataJasaRJ.s1.data);
    for(var i=0, iLen= params.jmlObat ;i<iLen;i++){
    	var o=PenataJasaRJ.dsGridObat.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
    	params['jumlah'+i]	= o.jumlah;
    	params['cara_pakai'+i]	= o.cara_pakai;
    	params['verified'+i]	= o.verified;
    	params['racikan'+i]	= o.racikan;
    	params['kd_dokter'+i]	= o.kd_dokter;
    }
    return params;
};


function getParamDetailAnamnese() 
{
    var params =
	{
		Table:'viewkondisifisik',
		KdPasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		UrutMasuk : Ext.get('txtKdUrutMasuk').getValue(),
		Anamnese:Ext.get('txtareaAnamnesis').getValue(),
		Catatan:Ext.get('txtareaAnamnesiscatatan').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		// Shift: tampungshiftsekarang,
		List:getArrdetailAnamnese()
		// JmlField: mRecordRwj.prototype.fields.length-4,
		// JmlList:GetListCountDetailTransaksi(),
		// Hapus:1,
		// Ubah:0
	};
    return params;
};


function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewTrKasirRwj', // data access listnya belum dibuat
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitRWJ').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer
	};
    return params;
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{		
			var recordterakhir;
			var y='';
			var z='@@##$$@@';
			
			
			recordterakhir=dsTRDetailKasirRWJList.data.items[i].data.DESC_REQ;
			TotalProduk=TotalProduk+recordterakhir;
			
			Ext.get('txtJumlah1EditData_viKasirRwj').dom.value=formatCurrency(TotalProduk);
			if (i === (dsTRDetailKasirRWJList.getCount()-1))
			{
				x += y ;
			}
			else
			{
				x += y + '##[[]]##';
			};
		
	}	
	
	return x;
};





function getArrDetailTrRWJ()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirRWJList.getCount();i++)
	{
		if (dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirRWJList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirRWJList.data.items[i].data.URUT;
			y += z + dsTRDetailKasirRWJList.data.items[i].data.KD_PRODUK;
			y += z + dsTRDetailKasirRWJList.data.items[i].data.QTY;
			y += z + ShowDate(dsTRDetailKasirRWJList.data.items[i].data.TGL_BERLAKU);
			y += z +dsTRDetailKasirRWJList.data.items[i].data.HARGA;
			y += z +dsTRDetailKasirRWJList.data.items[i].data.KD_TARIF;
			y += z +dsTRDetailKasirRWJList.data.items[i].data.URUT;
			
//			---
			if (i === (dsTRDetailKasirRWJList.getCount()-1))
			{
				x += y ;
			}
			else
			{
				x += y + '##[[]]##';
			};
		};
	}	
	
	return x;
};


function getItemPanelInputRWJ(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiRWJ(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitRWJ',
					    id: 'txtKdUnitRWJ',
						// emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						// emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
};

function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						// emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						// hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						// emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						// emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
};

function getItemPanelNoTransksiRWJ(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirrwj',
					    id: 'txtNoTransaksiKasirrwj',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
};

function getItemPaneltgl_filter() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterRWJ',
					    name: 'dtpTglAwalFilterRWJ',
						// readOnly : true,
					    value: now,
					    anchor: '99%',
						format: 'd/M/Y',
						altFormats: 'dmy',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 )
                                {
								
								RefreshDataFilterKasirRWJ();
								}
						}
						}
						// renderer: Ext.util.Format.dateRenderer('d/M/Y')
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					   // fieldLabel: 'Tanggal ',
					    id: 'dtpTglAkhirFilterRWJ',
					    name: 'dtpTglAkhirFilterRWJ',
					    format: 'd/M/Y',
						
						// readOnly : true,
					    value: now,
					    anchor: '99%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirRWJ();
								}
						}
						}
					}
				]
			}
		]
	};
    return items;
};
function getItemcombo_filter() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboStatusBayar_viKasirRwj()
				]
			},
			// {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:80,
			    items:
				[ 
		
					mComboUnit_viKasirRwj()
				]
			}
		]
	};
    return items;
};

function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						// hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	};
    return items;
};


function RefreshDataKasirRWJ() 
{
    dsTRKasirRWJList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirRWJ,
                Sort: 'tgl_transaksi',
                // Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirRwj',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirRWJ = undefined;
    return dsTRKasirRWJList;
};
function refeshkasirrwj()
{
dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					// Sort: 'no_transaksi',
                     Sort: '',
					// Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : ''
				}			
			}
		);   
		return dsTRKasirRWJList;
}

function RefreshDataFilterKasirRWJ() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwj').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirRwj').getValue() != '' && Ext.get('cboUNIT_viKasirRwj').getValue() != 'All')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~' + Ext.get('cboUNIT_viKasirRwj').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtFilterGridDataView_DOKTER_viKasirRwj').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Posting')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  posting_transaksi = TRUE';
		}
		else
		{
		
                        KataKunci += ' and posting_transaksi =  TRUE';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Belum Posting')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  posting_transaksi = FALSE';
		}
		else
		{
	
                        KataKunci += ' and posting_transaksi =  FALSE';
		};
		
		
	};
	
	if (Ext.get('cboStatus_viKasirRwj').getValue() == 'Semua')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
		else
		{
	
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterRWJ').getValue() != '')
	{
	
	
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterRWJ').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterRWJ').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					// Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					// Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirRWJList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirRWJ, 
					// Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					// Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirRWJList;
};


function Datasave_Konsultasi(mBol) 
{	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					// url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/KonsultasiPenataJasa",					
					params: getParamKonsultasi(),
					failure: function(o)
					{
					ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
					RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningRWJ('Konsultasi ulang gagal', 'Gagal');
						
						};
					}
				}
			);
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};



function Datasave_KasirRWJ(mBol){	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url			: baseURL + "index.php/main/functionRWJ/savedetailpenyakit",
			params		: getParamDetailTransaksiRWJ(),
			failure		: function(o){
				ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
			},
			success		: function(o){
				RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
					RefreshDataFilterKasirRWJ();
					if(mBol === false){
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					}
				}else{
					ShowPesanWarningRWJ('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		};
	};
	
};


function Dataupdate_KasirRWJ(mBol) 
{	
	if (ValidasiEntryCMRWJ(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					// url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdateKasirRWJDetail(),
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRWJ(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			);
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function ValidasiEntryCMRWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirrwj').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirRWJList.getCount() === 0 || (Ext.get('txtKdDokter').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiKasirrwj').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokter').dom.value  === undefined) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailKasirRWJList.getCount() === 0) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};

function ValidasiEntryKonsultasi(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') )
	{
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry').getValue() == '') 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	};
	return x;
};




function ShowPesanWarningRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

PenataJasaRJ.alertError	= function(str, modul){
	Ext.MessageBox.show({
	    title	: modul,
	    msg		: str,
	    buttons	: Ext.MessageBox.OK,
	    icon	: Ext.MessageBox.ERROR,
		width	: 250
	});
};

function ShowPesanErrorRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteKasirRWJ() 
{
   if (ValidasiEntryCMRWJ(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                // url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiRWJ(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoRWJ(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataKasirRWJ();
                                        RWJAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningRWJ(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningRWJ(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorRWJ(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        );
                    };
                }
            }
        );
    };
};


/*---LOOK UP GANTI DOKTER BESERTA ELEMENNYA----------------------------------*/

// function GantiDokterLookUp(rowdata)
function GantiDokterLookUp(mod_id) 
{
   
   

    var FormDepanDokter = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Ganti Dokter',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [DokterLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanDokter;

};









function DokterLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window
    (
        {
            id: 'gridDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 180,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryDokter(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirDokter=undefined;
                    // RefreshDataFilterKasirDokter();
                }
            }
        }
    );

    FormLookUpGantidokter.show();
 

};
function getItemPanelButtonGantidokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function()
						{
						GantiDokter(false);
						FormLookUpGantidokter.close();	
						}
					},
					{
							xtype:'button',
							text:'Tutup' ,
							width:70,
							hideLabel:true,
							id: 'btnCancelGantidokter',
							handler:function() 
							{
								FormLookUpGantidokter.close();
							}
					}
				]
			}
		]
	};
    return items;
};
function getFormEntryDokter(lebar) 
{
    var pnlTRGantiDokter = new Ext.FormPanel
    (
        {
            id: 'PanelTRDokter',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:190,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


	
   
   
  
   
   
   
   
    var FormDepanDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRGantiDokter
				

			]

		}
	);

    return FormDepanDokter;
};
// /---------------------------------------------------------------------------------------///


// /---------------------------------------------------------------------------------------///


function getItemPanelInputGantidokter(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDokter(lebar)		
				]
			}
		]
	};
    return items;
};

function ValidasiEntryTutupDokter(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function getItemPanelNoTransksiDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
			
					mComboDokterGantiEntry()
				
					
				]
				
			
			}
			
			
		]
	};
    return items;
};


function mComboDokterGantiEntry()
{ 
	
	
    // Grid Kasir Rawat Jalan # --------------
 

	// Kriteria filter pada Grid # --------------
   
    // -------------- # End form filter # --------------

 var Field = ['KD_DOKTER','NAMA'];

    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitRWJ').getValue();
	var kddokter = Ext.get('txtKdDokter').getValue();
   dsDokterGantiEntry.load
                (
                    {
                     params:
							{
											Skip: 0,
								Take: 1000,
								// Sort: 'DEPT_ID',
											Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboDokter',
								param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
							}
                    }
                );

    var cboDokterGantiEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter Baru',
		    align: 'Right',
             
		    store: dsDokterGantiEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor:'100%',
		    listeners:
			{
			    'select': function(a,b,c)
				{

									selectDokter = b.data.KD_DOKTER;
									NamaDokter = b.data.NAMA;
									 Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) // atau
															// Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);
	
    return cboDokterGantiEntry;

};

/*-----------Update Dokter----------------------------------*/
function GantiDokter(mBol)
{
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                   url: WebAppUrl.UrlUpdateData,
                                    params: getParamGantiDokter(),
									failure: function(o)
										{
										 ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
										},	
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoRWJ(nmPesanSimpanSukses,'Ganti Dokter');
													Ext.get('txtKdDokter').dom.value = selectDokter;
													Ext.get('txtNamaDokter').dom.value = NamaDokter;
													FormDepanDokter.close();
                                                    FormLookUpGantidokter.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorRWJ('Ganti Dokter Gagal','Ganti Dokter');
                                            };
                                    }
                            }
                    );
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};// END FUNCTION TutupDokterSave

/*---------------------------------------------*/

function ValidasiGantiDokter(modul,mBolHapus)
{
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == ''))
	{
	  if (Ext.get('cboDokterRequestEntry').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};


function getParamGantiDokter()
{
    var params =
	{
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
		
	};
    return params;
};

/*-----AKHIR LOOK UP GANTI DOKTER BESERTA ELEMENYNYA-----------------------------*/


/*------------AWAL LOOK UP GANTI KD CUSTOMER PASIEN BESERTA ELEMENNYA -------------- */

function KelompokPasienLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();

};


function getFormEntryTRKelompokPasien(lebar) 
{
    var pnlTRKelompokPasien = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKelompokPasien(lebar),getItemPanelButtonKelompokPasien(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien	
				
			]

		}
	);

    return FormDepanKelompokPasien;
};

function getItemPanelInputKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	,
					
				]
			}
		]
	};
    return items;
};




function getItemPanelButtonKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkKelompokPasien',
						handler:function()
						{
					
					Datasave_Kelompokpasien();
					FormLookUpsdetailTRKelompokPasien.close();
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelKelompokPasien',
							handler:function() 
							{
								FormLookUpsdetailTRKelompokPasien.close();
							}
					}
				]
			}
		]
	};
    return items;
};

function KelompokPasienbaru() 
{
	jeniscus=0;
    KelompokPasienAddNew = true;
	// Ext.getCmp('cboPerseorangan').show()
    Ext.getCmp('cboKelompokpasien').show();
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value;
    // Ext.getCmp('cboPerusahaanRequestEntry').hide()
	

};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    );
	
   // rowSelectedKasirRWJ = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    );
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	// title: 'Kelompok Pasien Lama',
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				// title: 'Kelompok Pasien Lama',
			    items:
				[
					
					
						{	 
															xtype: 'tbspacer',
														
															height: 2
						},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Kelompok Pasien Asal',
                                        // maxLength: 200,
                                        name: 'txtCustomerLama',
                                        id: 'txtCustomerLama',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
					
					
				]
			}
			
		]
	};
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
														{	 
															xtype: 'tbspacer',
															
															height:3
														},
									{  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien Baru',
                                    id: 'kelPasien',
                                     editable: false,
                                    // value: 'Perseorangan',
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
											'Id',
											'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
										  displayField: 'displayText',
										  mode: 'local',
										  width: 100,
										  forceSelection: true,
										  triggerAction: 'all',
										  emptyText: 'Pilih Salah Satu...',
										  selectOnFocus: true,
										  anchor: '95%',
										  listeners:
											 {
													'select': function(a, b, c)
												{
												if(b.data.displayText =='Perseorangan')
												{jeniscus='0';
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Perusahaan')
												{jeniscus='1';
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Asuransi')
												{jeniscus='2';
												Ext.getCmp('txtNoSJP').enable();
												Ext.getCmp('txtNoAskes').enable();
											}
												
												RefreshDatacombo(jeniscus);
												}

											}
                                  }, 
								  {
										columnWidth: .990,
										layout: 'form',
										border: false,
										labelWidth:130,
										items:
										[
															mComboKelompokpasien()
										]
									},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     },
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
									
									// mComboKelompokpasien
					
					
				]
			}
			
		]
	};
    return items;
};

function Datasave_Kelompokpasien(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 )
	{
		
		
			Ext.Ajax.request
			 (
				{
					// url: "./Datapool.mvc/CreateDataObj",
					url: baseURL +  "index.php/main/functionRWJ/UpdateKdCustomer",	
					params: getParamKelompokpasien(),
					failure: function(o)
					{
					ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
					RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
						Ext.get('txtCustomer').dom.value = selectNamaCustomer;
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirRWJ();
							if(mBol === false)
							{
								RefreshDataKasirRWJDetail(Ext.get('txtNoTransaksiKasirrwj').dom.value);
							};
						}

						else 
						{
								ShowPesanWarningRWJ('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			);
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function getParamKelompokpasien() 
{
	
    var params =
	{
		
		Table:'ViewTrKasirRwj', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwj').getValue(),
		KdUnit: Ext.get('txtKdUnitRWJ').getValue(),
		KdDokter:Ext.get('txtKdDokter').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
		
		
	};
    return params;
};

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined )||(Ext.get('cboKelompokpasien').getValue() == '') || (Ext.get('cboKelompokpasien').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('Kelompok pasien'), modul);
			x = 0;
		};
		if (Ext.get('cboKelompokpasien').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningRWJ(nmGetValidasiKosong('Combo kelompok pasien'), modul);
			x = 0;
		};
	};
	return x;
};


/*------------------------- AKHIR LOOK UP GANTI KD CUSTOMER PASIEN ------------------------------------*/

function setpostingtransaksi(notransaksi) 
{
	// if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
	// {
		Ext.Msg.show
		(
			{
			   title:'Posting',
			   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								// url: WebAppUrl.UrlDeleteData,
								url : baseURL + "index.php/main/posting",
								params: 
								{
								_notransaksi : 	notransaksi
								},
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										// refeshkasirrwj();
										RefreshDataFilterKasirRWJ();
										ShowPesanInfoDiagnosa('Posting Berhasil Dilakukan','Posting');
										// setLookUps_viDaftar.close();
										// dataaddnew_viJadwal();
										FormLookUpsdetailTRrwj.close();
										
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningDiagnosa(nmPesanHapusGagal,'Posting');
									}
									else 
									{
										ShowPesanWarningDiagnosa(nmPesanHapusError,'Posting');
									};
								}
							}
						);
					};
				}
			}
		);
	};
	



function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable();	

Ext.getCmp('btnLookupRWJ').disable();
Ext.getCmp('btnSimpanRWJ').disable();
Ext.getCmp('btnHpsBrsRWJ').disable();
Ext.getCmp('btnHpsBrsDiagnosa').disable();
Ext.getCmp('btnSimpanDiagnosa').disable();
Ext.getCmp('btnLookupDiagnosa').disable();
}

function setenablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirIgd').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirIgd').enable();
Ext.getCmp('btngantipasien').enable();
Ext.getCmp('btnposting').enable();	

Ext.getCmp('btnLookupRWJ').enable();
Ext.getCmp('btnSimpanRWJ').enable();
Ext.getCmp('btnHpsBrsRWJ').enable();
Ext.getCmp('btnHpsBrsDiagnosa').enable();
Ext.getCmp('btnSimpanDiagnosa').enable();
Ext.getCmp('btnLookupDiagnosa').enable();	
}
