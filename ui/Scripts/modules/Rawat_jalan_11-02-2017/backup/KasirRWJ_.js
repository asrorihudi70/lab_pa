
var CurrentHistory =
{
    data: Object,
    details: Array,
    row: 0
};
var tampungtypedata;
var tanggaltransaksitampung;
var mRecordKasirrwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var recordterakhir;
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryList;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRHistory;
var now = new Date();
var cellSelectedtutup;
var vkd_unit;

var nowTglTransaksi = new Date();

var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirrwjKasir='Lunas';
var dsTRKasirrwjKasirList;
var dsTRDetailKasirrwjKasirList;
var AddNewKasirrwjKasir = true;
var selectCountKasirrwjKasir = 50;
var now = new Date();
var rowSelectedKasirrwjKasir;

var FormLookUpsdetailTRKasirrwj;
var valueStatusCMKasirrwjView='All';
var nowTglTransaksi = new Date();
var dsComboBayar;
var vkode_customer;
var vflag;
 var gridDTLTRKasirrwj;
CurrentPage.page = getPanelKasirrwj(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirrwj(mod_id) 
{

    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT',
	'TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','FLAG','LUNAS','KET_PAYMENT','CARA_BAYAR','JENIS_PAY','KD_PAY','POSTING','TYPE_DATA','CO_STATUS'];
    dsTRKasirrwjKasirList = new WebApp.DataStore({ fields: Field });
    refeshKasirrwjKasir();
    var grListTRKasirrwj = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirrwjKasirList,
          
            columnLines: false,
            autoScroll:true,
			height:400,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
							 rowSelectedKasirrwjKasir = dsTRKasirrwjKasirList.getAt(row);
							
							
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
			           
                    rowSelectedKasirrwjKasir = dsTRKasirrwjKasirList.getAt(ridx);
				
                    if (rowSelectedKasirrwjKasir != undefined)
                    { 	if ( rowSelectedKasirrwjKasir.data.LUNAS=='f' )
							   {
									KasirrwjLookUp(rowSelectedKasirrwjKasir.data);
									
							   }else
							   {
							   ShowPesanWarningKasirrwj('Sudah Tutup Transaksi','Pembayaran');
							   }
                    }else{
					ShowPesanWarningKasirrwj('Silahkan Pilih data   ','Pembayaran');
					      }
                   
                },
				rowclick: function (sm, ridx, cidx)
                {
						cellSelectedtutup=rowSelectedKasirrwjKasir.data.NO_TRANSAKSI;
						//alert(rowSelectedKasirrwjKasir.data.LUNAS);
						if ( rowSelectedKasirrwjKasir.data.LUNAS=='t' && rowSelectedKasirrwjKasir.data.CO_STATUS =='t')
						   {
						   
						     Ext.getCmp('btnEditKasirrwj').disable();
						     Ext.getCmp('btnTutupTransaksiKasirrwj').disable();
						   	 Ext.getCmp('btnHpsBrsKasirrwj').disable();
							 
						   }
						   else if ( rowSelectedKasirrwjKasir.data.LUNAS=='f' && rowSelectedKasirrwjKasir.data.CO_STATUS =='f')
						   {
						        Ext.getCmp('btnTutupTransaksiKasirrwj').disable();
						   	    Ext.getCmp('btnHpsBrsKasirrwj').enable();
								Ext.getCmp('btnEditKasirrwj').enable();
						       
							}
							   else if ( rowSelectedKasirrwjKasir.data.LUNAS=='t' && rowSelectedKasirrwjKasir.data.CO_STATUS =='f')
						   {
						       Ext.getCmp('btnEditKasirrwj').disable();
						        Ext.getCmp('btnTutupTransaksiKasirrwj').enable();
						   	    Ext.getCmp('btnHpsBrsKasirrwj').enable();
						       
							}
                   RefreshDatahistoribayar(rowSelectedKasirrwjKasir.data.NO_TRANSAKSI);
         
                } 
                   
				
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewKasirrwj',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglKasirrwjViewKasirrwj',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colKasirrwjerViewKasirrwj'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colKasirrwjerViewKasirrwj'
                    },
                    {
                        id: 'colLocationViewKasirrwj',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewKasirrwj',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colunitViewKasirrwj',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					     {
                        id: 'colcustomerViewKasirrwj',
                        header: 'Kel. Pasien',
                        dataIndex: 'CUSTOMER',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					//CUSTOMER
					 {
                        id: 'colLUNAScoba',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: true,
                        width: 90,
                        align:'center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					 {
                        id: 'coltutuptr',
                        header: 'Tutup transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: true,
                        width: 90,
                        align:'center',
                       renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    }
                   
                ]
            ),
            tbar:
                [
                    {
                        id: 'btnEditKasirrwj',
                        text: 'Pembayaran',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirrwjKasir != undefined)
                            {
                                    KasirrwjLookUp(rowSelectedKasirrwjKasir.data);
                            }
                            else
                            {
							ShowPesanWarningKasirrwj('Pilih data tabel  ','Pembayaran');
                                    //alert('');
                            }
                        }, disabled :true
                    }, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
					{
								text: 'Tutup Transaksi',
								id: 'btnTutupTransaksiKasirrwj',
								tooltip: nmHapus,
								iconCls: 'remove',
								handler: function()
									{
									if(cellSelectedtutup=='' || cellSelectedtutup=='undefined')
									{
									ShowPesanWarningKasirrwj ('Pilih data tabel  ','Pembayaran');
									}
									 else
										{	
									
									   UpdateTutuptransaksi(false);	
								        Ext.getCmp('btnEditKasirrwj').disable();
										Ext.getCmp('btnTutupTransaksiKasirrwj').disable();
						   	            Ext.getCmp('btnHpsBrsKasirrwj').disable();
										}										
								    },
									disabled:true
					}
                ]
            }
	);
	
	var LegendViewCMRequest = new Ext.Panel
	(
            {
            id: 'LegendViewCMRequest',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Lunas"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Lunas"
                }
            ]

        }
    )

    var FormDepanKasirrwj = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Rawat Jalan',
            border: false,
            shadhow: true,
           // autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [grListTRKasirrwj,GetDTLTRHistoryGrid(),LegendViewCMRequest],
            tbar:
            [
              '-','No. Medrec' + ' : ', ' ',
                {
                    xtype: 'textfield',
                   // fieldLabel: 'No. Medrec' + ' ',
                    id: 'txtFilterNomedrec',
                    width: 100,
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue())
                                             Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirrwjKasir();
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirrwjKasir();
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterNomedrec').setValue('')
                                            }
                                }
                            }

                        }
                },
				'-','Nama Pasien ', '-',
					
					{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirrwjKasir',
							//emptyText: 'Nama',
							width: 100,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									        RefreshDataFilterKasirrwjKasir();

									} 						
								}
							}
						}, ' ', ' ',  ' ', ' ', '-','Poli tujuan', '-',
						
						mComboUnit_viKasirrwjKasir(),
						' ', '-', 'Status Lunas ' + ' : ', ' ',
						mComboStatusBayar_viKasirrwjKasir()
                    , ' ', '-', 'Tanggal Kunjungan' + ' : ', ' ',
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal Kunjungan' + ' ',
                        id: 'dtpTglAwalFilterKasirrwj',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        onInit: function() { }
                    }, ' ', '  ' + nmSd + ' ', ' ', {
                        xtype: 'datefield',
                        fieldLabel: nmSd + ' ',
                        id: 'dtpTglAkhirFilterKasirrwj',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },'' ,' ' , '' , '' ,'' ,
					' ' , '' , '' , ' ',
							
					{
                    text: nmRefresh,
                    tooltip: nmRefresh,
                    iconCls: 'refresh',
                    anchor: '25%',
                    handler: function(sm, row, rec)
                    {
                       // rowSelectedKasirrwjKasir=undefined;
                        RefreshDataFilterKasirrwjKasir();
                    }
					}
				
            ],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
  

   return FormDepanKasirrwj

};


function mComboStatusBayar_viKasirrwjKasir()
{
  var cboStatus_viKasirrwjKasir = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirrwjKasir',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    width: 100,
			emptyText:'',
			fieldLabel: 'JENIS',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirrwjKasir,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirrwjKasir=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirrwjKasir();

				}
			}
		}
	);
	return cboStatus_viKasirrwjKasir;
};


function mComboUnit_viKasirrwjKasir() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirrwjKasir = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirrwjKasir.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirrwjKasir = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirrwjKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirrwjKasir,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'all',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirrwjKasir();

			        //selectStatusCMKasirrwjView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirrwjKasir;
};



function KasirrwjLookUp(rowdata) 
{
    var lebar = 580;
    FormLookUpsdetailTRKasirrwj = new Ext.Window
    (
        {
            id: 'gridKasirrwj',
            title: 'Kasir Rawat Jalan',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKasirrwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKasirrwj.show();
    if (rowdata == undefined) 
	{
        KasirrwjAddNew();
    }
    else 
	{
        TRKasirrwjInit(rowdata)
    }

};



function getFormEntryTRKasirrwj(lebar) 
{
    var pnlTRKasirrwj = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirrwj',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:130,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKasirrwj(lebar)],
           tbar:
            [
             
            ]
        }
    );
 var x;
 var paneltotal = new Ext.Panel
	(
            {
            id: 'paneltotal',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            height:67,
            anchor: '100% 8.0000%',
            autoScroll:false,
            items:
            [
                
				
				{
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
		                {
                   
                    layout: 'form',
                    style:{'text-align':'right','margin-left':'370px'},
                    border: false,
                    html: " Total :"
						},
						{
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viKasirRwj',
                            name: 'txtJumlah2EditData_viKasirRwj',
							style:{'text-align':'right','margin-left':'390px'},
                            width: 82,
                            readOnly: true,
                        },
						
		                //-------------- ## --------------
					]
				},
				 {
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
                    layout: 'form',
						style:{'text-align':'right','margin-left':'370px'},
						border: false,
						html: " Bayar :"
						},
		                {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viKasirRwj',
                            name: 'txtJumlahEditData_viKasirRwj',
							style:{'text-align':'right','margin-left':'390px'},
                            width: 82,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										hitung_bayar();
									       // RefreshDataFilterKasirrwjKasir();

									} 						
								}
							}
							
                            //readOnly: true,
                        }
		                //-------------- ## --------------
					]
				}
            ]

        }
    )
 var GDtabDetailKasirrwj = new Ext.Panel   
    (
        {
        id:'GDtabDetailKasirrwj',
        region: 'center',
        activeTab: 0,
		height:350,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [GetDTLTRKasirrwjGrid(),paneltotal
				
		            
					],
        tbar:
                [
                       
                                             
							 {
								text: 'Simpan',
								id: 'btnSimpanKasirrwj',
								tooltip: nmSimpan,
								iconCls: 'save',
								handler: function()
									{
									
									
														Datasave_KasirrwjKasir(false);
														//Ext.getCmp('btnEditKasirrwj').disable();
														//Ext.getCmp('btnTutupTransaksiKasirrwj').disable();
														//Ext.getCmp('btnHpsBrsKasirrwj').disable();
								   
								}
							}
							
                        
							 
							
                ],
	
        }
		
		
    );
	
   
  
   var pnlTRKasirrwj2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirrwj2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:380,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailKasirrwj,
			
			
			]
        }
    );

	
   
   
    var FormDepanKasirrwj = new Ext.Panel
	(
		{
		    id: 'FormDepanKasirrwj',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
			height:380,
		    shadhow: true,
		    items: [pnlTRKasirrwj,pnlTRKasirrwj2,
			
				
			]
		}			
				

			

		
	);

    return FormDepanKasirrwj
};



function hitung_bayar()
{
var hargatotal=parseInt(recordterakhir);

var bayarpasien=parseInt(Ext.get('txtJumlahEditData_viKasirRwj').dom.value);
//alert(hargatotal+ ' '+)
if (bayarpasien=='')
{
bayarpasien=0;
}
var kembalian=hargatotal-bayarpasien
alert(kembalian)
}
 


function TambahBarisKasirrwj()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirrwj();
        dsTRDetailKasirrwjKasirList.insert(dsTRDetailKasirrwjKasirList.getCount(), p);
    };
};





function GetDTLTRHistoryGrid() 
{

    var fldDetail = ['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'];
	
    dsTRDetailHistoryList = new WebApp.DataStore({ fields: fldDetail })
		 
    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
    (
        {
            title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryList,
            border: false,
            columnLines: true,
            frame: false,
			height:150,
            anchor: '100%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHistoryList.getAt(row);
                            CurrentHistory.row = row;
                            CurrentHistory.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRHistoryColumModel()
			,

			      tbar:
                [
                       
                                             
							    {
                                id:'btnHpsBrsKasirrwj',
                                text: 'Hapus Pembayaran',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
								//hidden :true,
                                handler: function()
                                {
                                        if (dsTRDetailHistoryList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentHistory != undefined)
                                                        {
                                                                HapusBarisDetailbayar();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningKasirrwj('Pilih record ','Hapus data');
                                                }
                                        }
										 Ext.getCmp('btnEditKasirrwj').disable();
											Ext.getCmp('btnTutupTransaksiKasirrwj').disable();
										Ext.getCmp('btnHpsBrsKasirrwj').disable();
                                },
								disabled :true
                        }
                        
							 
							
                ]
             
        }
		
		
    );
	
	

    return gridDTLTRHistory;
};

function TRHistoryColumModel() 
{
    return new Ext.grid.ColumnModel
    (//'','','','','',''
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT',
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'DESKRIPSI',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME',	
				//hidden:true
                
				
				
              
            }
			

        ]
    )
};
function HapusBarisDetailbayar()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
		
           
                                          //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
																		if (variablehistori!='')
																		{
																		DataDeleteKasirrwjKasirDetail();
																		}
																		else
																		{
																			ShowPesanWarningKasirrwj('Silahkan isi alasan terlebih dahaulu','Keterangan');
																	
																		}
														}
														
												});
											

												
                                         
                                
               
        }
        else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        };
    }
};

function DataDeleteKasirrwjKasirDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionRWJ/deletedetail_bayar",
            params:  getParamDataDeleteKasirrwjKasirDetail(),
            success: function(o)
            {
				RefreshDatahistoribayar(Kdtransaksi);
				 RefreshDataFilterKasirrwjKasir();
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirrwj(nmPesanHapusSukses,nmHeaderHapusData);
								//alert(Kdtransaksi);					 
						
					//refeshKasirrwjKasir();
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirrwj(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirrwj(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirrwjKasirDetail()
{
    var params =
    {
	//'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
		//Table: 'ViewKasirRWJ',// 
        TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
		TrTglbayar:  CurrentHistory.data.data.TGL_BAYAR,
		Urut:  CurrentHistory.data.data.URUT
		
    };
	Kdtransaksi=CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};



function GetDTLTRKasirrwjGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','BAYARTR','DISCOUNT','PIUTANG'];
	
    dsTRDetailKasirrwjKasirList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirrwjKasirDetail() ;
  gridDTLTRKasirrwj = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Bayar',
            stripeRows: true,
			id: 'gridDTLTRKasirrwj',
            store: dsTRDetailKasirrwjKasirList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
			height:230,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        
                    }
                }
            ),
            cm: TRKasirRawatJalanColumModel()
        }
		
		
    );
	
	

    return gridDTLTRKasirrwj;
};

function TRKasirRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiKasirrwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiKasirrwj',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			    hidden:true,
			   menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colQtyKasirrwj',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
             
				
				
              
            },
            {
                id: 'colHARGAKasirrwj',
                header: 'Harga',
				align: 'right',
				hidden: false,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },

            {
                id: 'colPiutangKasirrwj',
                header: 'Puitang',
                width:80,
                dataIndex: 'PIUTANG',
				align: 'right',
				//hidden: false,
			
				 editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolPuitangRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						
                    }
                )
             ,
				renderer: function(v, params, record) 
							{
							
							//getTotalDetailProduk();
						
								return formatCurrency(record.data.PIUTANG);
							
						
							
							
							}
            },
			{
                id: 'colTunaiKasirrwj',
                header: 'Tunai',
                width:80,
                dataIndex: 'BAYARTR',
				align: 'right',
				hidden: false,
							 editor: new Ext.form.TextField
								(
									{
										id:'fieldcolTunaiRWJ',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								),
				renderer: function(v, params, record) 
							{
							
							getTotalDetailProduk();
					
							return formatCurrency(record.data.BAYARTR);
							
							
							}
				
            },
			{
                id: 'colDiscountKasirrwj',
                header: 'Discount',
                width:80,
                dataIndex: 'DISCOUNT',
				align: 'right',
				hidden: false,
				editor: new Ext.form.TextField
								(
									{
										id:'fieldcolDiscountRWJ',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								)
				,
					renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.DISCOUNT);
							
							
							}
							 
				
            }

        ]
    )
};




function RecordBaruKasirrwj()
{

	var p = new mRecordKasirrwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function TRKasirrwjInit(rowdata)
{
    AddNewKasirrwjKasir = false;
	
	vkd_unit = rowdata.KD_UNIT;
	RefreshDataKasirrwjKasirDetail(rowdata.NO_TRANSAKSI);
	Ext.get('txtNoTransaksiKasirrwjKasir').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('cboPembayaran').dom.value= rowdata.KET_PAYMENT;
	Ext.get('cboJenisByr').dom.value= rowdata.CARA_BAYAR; // take the displayField value 
	loaddatastorePembayaran(rowdata.JENIS_PAY);
	vkode_customer = rowdata.KD_CUSTOMER;
	tampungtypedata=0;
	tapungkd_pay='TU';
	jenispay=1;
	var vkode_customer = rowdata.LUNAS


	showCols(Ext.getCmp('gridDTLTRKasirrwj'));
	hideCols(Ext.getCmp('gridDTLTRKasirrwj'));
	vflag= rowdata.FLAG;
	//(rowdata.NO_TRANSAKSI;

	
	Ext.Ajax.request(
	{
	   
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
	
			tampungshiftsekarang=o.responseText




	    }
	
	});

	
	
};

function mEnabledKasirrwjCM(mBol)
{

	 Ext.get('btnLookupKasirrwj').dom.disabled=mBol;
	 Ext.get('btnHpsBrsKasirrwj').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function KasirrwjAddNew() 
{
    AddNewKasirrwjKasir = true;
	Ext.get('txtNoTransaksiKasirrwjKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	
	//Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirrwjKasir').dom.value= ''
	rowSelectedKasirrwjKasir=undefined;
	dsTRDetailKasirrwjKasirList.removeAll();
	mEnabledKasirrwjCM(false);
	

};

function RefreshDataKasirrwjKasirDetail(no_transaksi) 
{
    var strKriteriaKasirrwj='';
   
    strKriteriaKasirrwj = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirrwjKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayar',
			    param: strKriteriaKasirrwj
			}
		}
	);
    return dsTRDetailKasirrwjKasirList;
};

function RefreshDatahistoribayar(no_transaksi) 
{
    var strKriteriaKasirrwj='';
   
    strKriteriaKasirrwj = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewHistoryBayar',
			    param: strKriteriaKasirrwj
			}
		}
	);
    return dsTRDetailHistoryList;
};


///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirrwj() 
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};
		
    var params =
	{
	
	//	Table:'',
	
		kdUnit : vkd_unit,
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirrwjKasir').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		Flag:vflag,
		bayar: tapungkd_pay,
		
		Typedata: tampungtypedata,

		List:getArrDetailTrKasirrwj(),
		JmlField: mRecordKasirrwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		
		Hapus:1,
		Ubah:0
	};
    return params
};

function paramUpdateTransaksi()
{
 var params =
	{
   TrKodeTranskasi: cellSelectedtutup
	};
	return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirrwjKasirList.getCount();i++)
	{
		if (dsTRDetailKasirrwjKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirrwjKasirList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirrwjKasirList.getCount();i++)
	{		

		
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
		tampunggrid = parseInt(dsTRDetailKasirrwjKasirList.data.items[i].data.BAYARTR);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirRwj').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirrwjKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirRwj').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirrwjKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirrwjKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirRwj').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	
	return x;
};







function getArrDetailTrKasirrwj()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirrwjKasirList.getCount();i++)
	{
		if (dsTRDetailKasirrwjKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirrwjKasirList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirrwjKasirList.data.items[i].data.URUT
			y += z + dsTRDetailKasirrwjKasirList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirrwjKasirList.data.items[i].data.QTY
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.URUT
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.BAYARTR
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.PIUTANG
			y += z +dsTRDetailKasirrwjKasirList.data.items[i].data.DISCOUNT
			
			
			
			if (i === (dsTRDetailKasirrwjKasirList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputKasirrwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:110,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiKasirrwj(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar)	
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboJenisByrView()
				
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
				labelWidth:0.9,
			    border: false,
			    items:
				[mComboPembayaran()
	
				]
			}
		]
	}
    return items;
};



function loaddatastorePembayaran(jenis_pay)
{
          dsComboBayar.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboBayar',
								param: 'jenis_pay=~'+ jenis_pay+ '~'
							}
                   }
                )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
            anchor: '100%',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				},
				  

			}
		}
	);

    return cboPembayaran;
};


function mComboJenisByrView() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
		
                            Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ComboJenis',
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByr',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran      ',

		    align: 'Right',
		    anchor:'100%',
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
						 loaddatastorePembayaran(b.data.JENIS_PAY);
						 tampungtypedata=b.data.TYPE_DATA;
						 jenispay=b.data.JENIS_PAY;
						 showCols(Ext.getCmp('gridDTLTRKasirrwj'));
						 hideCols(Ext.getCmp('gridDTLTRKasirrwj'));
						getTotalDetailProduk();
						Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';
				
					
			   
				},
				  

			}
		}
	);
	
    return cboJenisByr;
};
    function hideCols (grid)
	{
		if (tampungtypedata==3)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==0)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==1)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			
		}
	};
      function showCols (grid) {   
	  	if (tampungtypedata==3)
			{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
		
			}
			else if(tampungtypedata==0)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
			}
			else if(tampungtypedata==1)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
			}
	
	};



function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiKasirrwj(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirrwjKasir',
					    id: 'txtNoTransaksiKasirrwjKasir',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec ',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirrwjKasir() 
{
    dsTRKasirrwjKasirList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirrwjKasir,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewKasirRWJ',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirrwjKasir = undefined;
    return dsTRKasirrwjKasirList;
};

function refeshKasirrwjKasir()
{
dsTRKasirrwjKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirrwjKasir, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirRWJ',
					param : ''
				}			
			}
		);   
		return dsTRKasirrwjKasirList;
}

function RefreshDataFilterKasirrwjKasir() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirrwjKasir').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirrwjKasir').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirrwjKasir').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirrwjKasir').getValue() != '' && Ext.get('cboUNIT_viKasirrwjKasir').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirrwjKasir').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirrwjKasir').getValue() + '%~)';
		};
	};

		
	if (Ext.get('cboStatus_viKasirrwjKasir').getValue() == 'Lunas')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  lunas = ~true~';
		}
		else
		{
		
                        KataKunci += ' and lunas =  ~true~';
		};
	
	};
		

		if (Ext.get('cboStatus_viKasirrwjKasir').getValue() == 'Semua')
	{
		
		
		
	};
	if (Ext.get('cboStatus_viKasirrwjKasir').getValue() == 'Belum Lunas')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  lunas = ~false~';
		}
		else
		{
	
                        KataKunci += ' and lunas =  ~false~';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterKasirrwj').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirrwj').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwj').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirrwj').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirrwj').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirrwjKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirrwjKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirRWJ',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirrwjKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirrwjKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirRWJ',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirrwjKasirList;
};



function Datasave_KasirrwjKasir(mBol) 
{	
	if (ValidasiEntryCMKasirrwj(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/savepembayaran",
					params: getParamDetailTransaksiKasirrwj(),
					failure: function(o)
					{
					ShowPesanWarningKasirrwj('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirrwjKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(Ext.get('txtNoTransaksiKasirrwjKasir').dom.value);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirrwj(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataKasirrwjKasir();
							if(mBol === false)
							{
									 RefreshDataFilterKasirrwjKasir();
							};
						}
						else 
						{
								ShowPesanWarningKasirrwj('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};
function UpdateTutuptransaksi(mBol) 
{	
	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/ubah_co_status_transksi",
					params: paramUpdateTransaksi(),
					failure: function(o)
					{
						ShowPesanWarningKasirrwj('Data gagal tersimpan segera hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirrwjKasir();
					},	
					success: function(o) 
					{
						//RefreshDatahistoribayar(Ext.get('cellSelectedtutup);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirrwj(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							//RefreshDataKasirrwjKasir();
							if(mBol === false)
							{
									 RefreshDataFilterKasirrwjKasir();
							};
							cellSelectedtutup='';
						}
						else 
						{
								ShowPesanWarningKasirrwj('Data gagal tersimpan segera hubungi Admin', 'Gagal');
								cellSelectedtutup='';
						};
					}
				}
			)
		
	
	
};

function ValidasiEntryCMKasirrwj(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirrwjKasir').getValue() == '') 
	
	|| (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
	|| (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
	|| (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
	|| dsTRDetailKasirrwjKasirList.getCount() === 0 
	|| (Ext.get('cboPembayaran').getValue() == '') 
	||(Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') )
	{
		if (Ext.get('txtNoTransaksiKasirrwjKasir').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboPembayaran').getValue() == ''||Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') 
		{
			ShowPesanWarningKasirrwj(('Data pembayaran tidak Boleh Kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirrwj(('Data no. Medrec tidak Boleh Kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirrwj('Nama pasien belum terisi', modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirrwj(('Data Tanggal Kunjungan tidak Boleh Kosong'), modul);
			x = 0;
		}
		//cboPembayaran
	
		else if (dsTRDetailKasirrwjKasirList.getCount() === 0) 
		{
			ShowPesanWarningKasirrwj('Data dalam tabel kosong',modul);
			x = 0;
		};
	};
	return x;
};




function ShowPesanWarningKasirrwj(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorKasirrwj(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoKasirrwj(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

