// Data Source
var dataSource_viDaftar;
var selectCount_viDaftar=50;
var NamaForm_viDaftar="Pendaftaran ";
var mod_name_viDaftar="viDaftar";

var now_viDaftar= new Date
var h=now_viDaftar.getHours();
var m=now_viDaftar.getMinutes();
var s=now_viDaftar.getSeconds();
var nowSerahIjazah_viDaftar= new Date();
var urutJenjang_viDaftar;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftar;
var rowSelected_viDaftar;
var setLookUps_viDaftar;
var ds_KABUPATEN_viKasirRwj;
var ds_KECAMATAN_viKasirRwj;
var tampung;
var BlnIsDetail;
var SELECTDATASTUDILANJUT;

var selectPoliPendaftaran;
var selectKelompokPoli;

var selectPendidikanPendaftaran;
var selectPekerjaanPendaftaran;
var selectPendaftaranStatusMarital;
var selectAgamaPendaftaran;
var mNoKunjungan_viKasir='1';
var selectSetJK;
var selectSetGolDarah;
var selectSetSatusMarital;
var selectSetKelompokPasien;
var selectSetRujukanDari;
var selectSetNamaRujukan;
var dsAgamaRequestEntry;
//var selectAgamaRequestEntry;
var dsPropinsiRequestEntry;
var selectPropinsiRequestEntry;
var dsKecamatanRequestEntry;
//var selectKecamtanRequestEntry;
var dsPendidikanRequestEntry;
var dsPekerjaanRequestEntry;
var dsDokterRequestEntry;

var CurrentData_viDaftar =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDaftar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
datarefresh_viDaftar();
function dataGrid_viDaftar(mod_id)
{	
    var FieldMaster_viDaftar = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];

    dataSource_viDaftar = new WebApp.DataStore({fields: FieldMaster_viDaftar});
    
	datarefresh_viDaftar();
    
	var grData_viDaftar = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDaftar,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDaftar = undefined;
							rowSelected_viDaftar = dataSource_viDaftar.getAt(row);
							CurrentData_viDaftar;
							CurrentData_viDaftar.row = row;
							CurrentData_viDaftar.data = rowSelected_viDaftar.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viDaftar = dataSource_viDaftar.getAt(ridx);
					if (rowSelected_viDaftar != undefined)
					{
						setLookUp_viDaftar(rowSelected_viDaftar.data);
					}
					else
					{
						setLookUp_viDaftar();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftar',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					{
						id: 'colNMPASIEN_viDaftar',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					{
						id: 'colALAMAT_viDaftar',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					{
						id: 'colTglKunj_viDaftar',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					{
						id: 'colPoliTujuan_viDaftar',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
				]
				),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viDaftar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDaftar',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDaftar != undefined)
							{
								setLookUp_viDaftar(rowSelected_viDaftar.data)
							}
							else
							{								
								setLookUp_viDaftar();
							}
						}
					},' ', '-',
                                        {
                                                xtype: 'checkbox',
                                                id: 'chkWithTglRequest',
                                                hideLabel:true,
                                                checked: false,
                                                handler: function()
                                                  {
                                                    if (this.getValue()===true)
                                                    {
                                                        Ext.get('txtfilterTglKunjAwal_viDaftar').dom.disabled=false;
                                                        Ext.get('txtfilterTglKunjAkhir_viDaftar').dom.disabled=false;

                                                    }
                                                    else
                                                    {
                                                    Ext.get('txtfilterTglKunjAwal_viDaftar').dom.disabled=true;
                                                    Ext.get('txtfilterTglKunjAkhir_viDaftar').dom.disabled=true;
                                                    };
                                                    }
                                            },' ', '-',
                                        {xtype: 'tbtext', text: 'Tgl Kunjungan: ', cls: 'left-label', width: 90},
                                            {
                                                xtype: 'datefield',
                                                id: 'txtfilterTglKunjAwal_viDaftar',
                                                value: now_viDaftar,
                                                format: 'd/M/Y',
                                                width: 120,
                                                listeners:
                                                {
						'specialkey' : function()
                                                    {
							if (Ext.EventObject.getKey() === 13)
							{
								datarefresh_viDaftar();
							}
                                                    }
                                                }
                                            },' ', '-',
                                        {xtype: 'tbtext', text: 's/d : ', cls: 'left-label', width: 30},
                                            {
                                                xtype: 'datefield',
                                                id: 'txtfilterTglKunjAkhir_viDaftar',
                                                value: now_viDaftar,
                                                format: 'd/M/Y',
                                                width: 120,
                                                listeners:
                                            {
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								datarefresh_viDaftar();
							}
						}
                                            }
                                        },				
				]
			},
			bbar : bbar_paging(mod_name_viDaftar, selectCount_viDaftar, dataSource_viDaftar),
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

    var FrmTabs_viDaftar = new Ext.Panel
    (
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: NamaForm_viDaftar,          
			border: false,  
			closable: true,
			iconCls: 'Studi_Lanjut',
			margins: '0 5 5 0',
			items: [grData_viDaftar],
			tbar:
			[
                                
				{xtype: 'tbtext', text: 'No.Medrec : ', cls: 'left-label', width: 80},
				{
					xtype: 'textfield',
					id: 'txtfilterNoRM_viDaftar',
					width: 120,
					listeners:
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								datarefresh_viDaftar();
							}
						}
					}
				},' ', '-',
				{xtype: 'tbtext', text: 'Nama : ', cls: 'left-label', width: 50},
				{
					xtype: 'textfield',
					id: 'txtfilterNama_viDaftar',				
					width: 150,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								datarefresh_viDaftar();								
							} 						
						}
					}
				},' ', '-',
                                {xtype: 'tbtext', text: 'Poli Tujuan : ', cls: 'left-label', width: 80},
                                  mComboPoli_viDaftar(125, "ComboPoli_viDaftar"),' ', '-',
                                {xtype: 'tbtext', text: 'Poli Tujuan : ', cls: 'left-label', width: 80},
                                {type: 'tbfill'},
				{
					xtype: 'button',
					id: 'btnRefreshFilter_viDaftar',
					iconCls: 'refresh',
					handler: function()
					{
						datarefresh_viDaftar();
					}
				}
                                
				
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					datarefresh_viDaftar();
				}
			}
		}
    )
    // datarefresh_viDaftar();
    return FrmTabs_viDaftar;
}

function setLookUp_viDaftar(rowdata)
{
    var lebar = 900;
    setLookUps_viDaftar = new Ext.Window
    (
    {
        id: 'SetLookUps_viDaftar',
        name: 'SetLookUps_viDaftar',
        title: NamaForm_viDaftar, 
        closeAction: 'destroy',        
        width: lebar,
        height: 650,//575,
        resizable:false,
		autoScroll: false,
       // border: true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftar(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
		if 
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDaftar=undefined;
                datarefresh_viDaftar();
		mNoKunjungan_viKasir = '';
            }
        }
    }
    );

    setLookUps_viDaftar.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftar();
		// Ext.getCmp('btnSimpan_viDaftar').disable();
		// Ext.getCmp('btnSimpanExit_viDaftar').disable();	
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viDaftar(rowdata);
    }
}

function getFormItemEntry_viDaftar(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftar = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                        autoScroll: true,
			width: lebar,//lebar-55,
                        height: 900,
			border: false,
//                        defaults:
//                           {
//                            autoScroll: true
//                           },
                        //labelAlign: 'top',
			items:[
                           getItemPanelInputBiodata_viDaftar(lebar),
                           getItemDataKunjungan_viDaftar(lebar),
                           getItemDataKunjungan_viDaftarbawah(),
                           getPenelItemDataKunjungan_viDaftar()
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDaftar',
						handler: function(){
							dataaddnew_viDaftar();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar();
							if (x===undefined)
							{
								setLookUps_viDaftar.close();
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viDaftar',
						handler: function()
						{
							// BlnIsDetail=false;
							datadelete_viDaftar();
							datarefresh_viDaftar();
							
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype:'button',
						text:'Lookup Pasien',
						iconCls:'find',
						id:'btnLookUp_viDaftar',
						handler:function(){
							// FormLookupPasien();
							var strKriteria='';							
							strKriteria = getKriteriaCariLookup();
							FormLookupPasien(strKriteria,nFormPendaftaran,'',Ext.get('txtKDPasien_viDaftar').getValue(),Ext.get('txtNamaPs_viDaftar').getValue(),Ext.get('txtAlamat_viDaftar').getValue());
						}
					},					
					{
						xtype:'tbseparator'
					},
					{
						xtype:'button',
						text:'Print Kartu Pasien',
						iconCls:'print',
						id:'btnPrint_viDaftar',
						handler:function()
						{							
							if (mNoKunjungan_viKasir != '' )
							{														
								GetPrintKartu(mNoKunjungan_viKasir);								
							}
							else
							{								
								ShowPesanWarning_viDaftar('Data pasien tidak ada ' ,'Print kartu pasien');
							}
							
						}
					}
				]
			}//,items:
			
		}
    )

    return pnlFormDataBasic_viDaftar;
}
//------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------///

function getItemPanelInputBiodata_viDaftar(lebar)
{
    var items =
	{
	    layout: 'column',
	    border: false,
            labelAlign: 'top',
	    items:
		[
			{
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'No. Medrec ',
					    name: 'txtNoRequest',
					    id: 'txtNoRequest',
                                            emptyText:'Otomatis',
                                            readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNama',
					    id: 'txtNama',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama Keluarga ',
					    name: 'txtNamaKeluarga',
					    id: 'txtNamatKeluarga',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					//{
//					    xtype: 'textfield',
//					    fieldLabel: 'Jenis Kelamin ',
//					    name: 'txtJK',
//					    id: 'txtJK',
//                                            emptyText:' ',
//					    anchor: '95%'
                                        //}
                                            mComboJK()
					
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Tempat Lahir ',
					    name: 'txtTempatLahir',
					    id: 'txtTempatLahir',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal Lahir ',
					    name: 'dtpTanggalLahir',
					    id: 'dptTanggalLahir',
                                            format: 'd/M/Y',
                                            value: now_viDaftar,
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Thn ',
					    name: 'txtThnLahir',
					    id: 'txtThnLahir',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Bln ',
					    name: 'txtBlnLahir',
					    id: 'txtBlnLahir',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Hari ',
					    name: 'txtHariLahir',
					    id: 'txtHariLahir',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .22,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Agama ',
//					    name: 'txtAgama',
//					    id: 'txtAgama',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                        mComboAgamaRequestEntry(),
				]
			},
                        {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:0,
                            
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'G. Darah ',
//					    name: 'txtGolDarah',
//					    id: 'txtGolDarah',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                    mComboGolDarah()
                                    
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,

			    items:
				[
					{
//                                                xtype: 'checkbox',
//                                                id: 'chkWithWniRequest',
//                                                fieldLabel: 'WNI',
//                                                checked: false,
//                                                anchor: '100%'
                                            xtype: 'textfield',
					    fieldLabel: 'Kewarganegaraan ',
					    name: 'txtWarga',
					    id: 'txtWarga',
                                            emptyText:' ',
					    anchor: '95%'

					}
//                                        {
//                                            xtype: 'checkboxgroup',
//                                            //fieldLabel: '',
//
//                                            items: [
//                                                    {boxLabel: 'WNI', name: 'cb-auto-1'}
//                                                   ]
//                                        }
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Status Marital ',
//					    name: 'txtStatusMarital',
//					    id: 'txtStatusMarital',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                    mComboSatusMarital()
				]
			},
                        
                        {
			    columnWidth: .70,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamat',
					    id: 'txtAlamat',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Propinsi ',
//					    name: 'txtPropinsi',
//					    id: 'txtPropinsi',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                    mComboPropinsiRequestEntry()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Kab/Kod ',
//					    name: 'txtKabKod',
//					    id: 'txtKabKod',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                    mComboKabupatenRequestEntry()
//                                    {
//                                    xtype: 'combobox',
//                                    id: 'combo1', // static ids are bad, I highly recommend removing this
//                                    fieldLabel: 'Type',
//                                    store: [[1, 'Single Store'], [2, 'Group'], [3, 'Sub-Group']],
//                                    editable: false,
//                                    listeners:{
//                                    change: function(combo, value) {
//                                    console.log(value);
//                                    groupStore.load({
//                                    params: {
//                                                id: value
//                                            }
//                                                     });
//                                                                     }
//                                              }
//                                    },
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Kecamatan ',
//					    name: 'txtKecamatan',
//					    id: 'txtKecamatan',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                      mComboKecamatanRequestEntry()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Pendidikan ',
//					    name: 'txtPendidikan',
//					    id: 'txtPendidikan',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                     mComboPendidikanRequestEntry()
				]
			},
                        {
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Pekerjaan ',
//					    name: 'txtPekerjaan',
//					    id: 'txtPekerjaan',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                    mComboPekerjaanRequestEntry()
				]
			},
                        {
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Suku Bangsa ',
//					    name: 'txtSukuBangsa',
//					    id: 'txtSukuBangsa',
//                                            emptyText:' ',
//					    anchor: '95%'
//					}
                                     mComboSukuRequestEntry()
                                        ]
                                        },
                                        ]
//                                        listeners:
//					{
//						activate: function()
//						{
//                                                    if (selectPropinsiRequestEntry != undefined)
//                                                    {
//                                                       Ext.get('txtNama').dom.value = 'satu'
//                                                    }
//                                                    else
//                                                    {
//                                                            Ext.get('txtNama').dom.value = 'dua'
//                                                            //tampung ='kd_kecamatan='+selectPropinsiRequestEntry
//                                                    }
//						}
//					}
        };
    return items;
};

function getItemDataKunjungan_viDaftar(lebar)
{
     var items =
	{
	    xtype:'fieldset',
            //checkboxToggle:true,
            title: 'Unit Perawatan',
            autoHeight:true,
            collapsible: true,
            //defaults: {width: 210},
            //defaultType: 'textfield',
            collapsed: false,
            items :
                [
                    getItemDataKunjungan_viDaftar1(),
                ]
        }
    return items;
};

function getItemDataKunjungan_viDaftar1()
{
        var items =
            {

                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
		[
			{
			    columnWidth: .15,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    name: 'dtpTanggal',
					    id: 'dptTanggal',
                                            format: 'd/M/Y',
                                            value: now_viDaftar,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
                                        xtype: 'textfield',
                                        id: 'txtJamKunjung',
                                        value: h+':'+m+':'+s,
					width: 120,
                                        anchor: '95%',
					listeners:
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								datarefresh_viDaftar();
							}
						}
					}
                                    }
				]
			},
                        {
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien ',
					    name: 'txtKelompokPasien',
					    id: 'txtKelompokPasien',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Poliklinik ',
					    name: 'txtPoliklinik',
					    id: 'txtPoliklinik',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Kosong ',
					    name: 'txtKosong',
					    id: 'txtKosong',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
//					{
//					    xtype: 'textfield',
//					    fieldLabel: 'Dokter ',
//					    name: 'txtDokter',
//					    id: 'txtDokter',
//                                            emptyText:' ',
//					    anchor: '50%'
//					}
                                    mComboDokterRequestEntry()
				]
			},
                ]

        };
    return items;
};

function getItemDataKunjungan_viDaftarbawah()
{
    var items =
	{
	    xtype:'fieldset',
            //checkboxToggle:true,
            title: 'Diagnosa',
            autoHeight:true,
            collapsible: true,
            //defaults: {width: 210},
            //defaultType: 'textfield',
            collapsed: true,
            items :
                [
                    getItemDataKunjungan_viDaftarbawah1(),
                ]
        }
    return items;
};

function getItemDataKunjungan_viDaftarbawah1()
{
        var items =
            {

                layout: 'column',
                border: false,
                items:
		[
			{
			    columnWidth: .80,
			    layout: 'form',
                            labelWidth:90,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Anamnese ',
					    name: 'txtAnamnese',
					    id: 'txtAnamnese',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .80,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alergi ',
					    name: 'txtAlergi',
					    id: 'txtAlergi',
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},                 
                ]

        };
    return items;
};

function getPenelItemDataKunjungan_viDaftar(lebar)
{
    var items =
	{
	    xtype:'fieldset',
            //checkboxToggle:true,
            title: 'Detail',
            autoHeight:true,
            collapsible: true,
            //defaults: {width: 210},
            //defaultType: 'textfield',
            collapsed: true,
            items :
                [
                    getPenelItemDataKunjungan_viDaftar1(lebar),
                ]
        }
    return items;
};

function getPenelItemDataKunjungan_viDaftar1(lebar)
{
    var items =
        {
                           xtype:'tabpanel',
                           plain:true,
                           activeTab: 0,
                           height:205,
                           //deferredRender: false,
                           defaults:
                           {
                            bodyStyle:'padding:10px',
                            autoScroll: true
                           },
                           items:[
                               {
                                title:'Penerimaan',
                                layout:'form',
                                items:
                                    [
                                        {
                                            xtype: 'fieldset',
                                            title: 'Rujukan',
                                            //Height: 50,
                                            items: [{
                                                        xtype: 'radiogroup',
                                                        //fieldLabel: 'Auto Layout',
                                                        items: [
                                                        {boxLabel: 'Perseorangan', name: 'rb-auto', inputValue: 1},
                                                        {boxLabel: 'Rujukan', name: 'rb-auto', inputValue: 2}
                                                                ]
                                                    }
                                            ]
                                        },
//                                       {
//                                            xtype: 'textfield',
//					    fieldLabel: 'Dari ',
//					    name: 'txtRujukanDari',
//					    id: 'txtRujukanDari',
//                                            width: 100
//                                        },
                                           mComboRujukanDari(),
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNamaPerujuk',
					    id: 'txtNamaPerujuk',
                                            width: 250
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamatPerujuk',
					    id: 'txtAlamatPerujuk',
                                            width: 500
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Kota ',
					    name: 'txtKotaPerujuk',
					    id: 'txtKotaPerujuk'
                                        },
                                    ]
                                },
                                {
                                 title:'Penanggung Jawab',
                                 layout:'form',
                                 items:
                                     [
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNamaPenanggungjawab',
					    id: 'txtNamaPenanggungjawab',
                                            width: 200
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamatPenanggungjawab',
					    id: 'txtAlamatPenanggungjawab',
                                            width: 300
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Kota ',
					    name: 'txtKotaPenanggungjawab',
					    id: 'txtKotaPenanggungjawab',
                                            width: 150
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Kode Pos ',
					    name: 'txtKdPosPerusahaanPenanggungjawab',
					    id: 'txtKdPosPerusahaanPenanggungjawab',
                                            width: 50
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Tlp ',
					    name: 'txtTlpPenanggungjawab',
					    id: 'txtTlpPenanggungjawab',
                                            width: 100
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Pekerjaan ',
					    name: 'txtPekerjaanPenanggungjawab',
					    id: 'txtPekerjaanPenanggungjawab',
                                            width: 100
                                        },
                                        {

                                            xtype: 'textfield',
					    fieldLabel: 'Nama Prsh ',
					    name: 'txtNamaPerusahaanPenanggungjawab',
					    id: 'txtNamaPerusahaanPenanggungjawab',
                                            width: 200
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Alamat Prsh ',
					    name: 'txtAlamatPrshPenanggungjawab',
					    id: 'txtAlamatPrshPenanggungjawab',
                                            width: 300
                                        },
                                        {
                                            xtype: 'textfield',
					    fieldLabel: 'Hub. Kel ',
					    name: 'txtHubKelPenanggungjawab',
					    id: 'txtHubKelPenanggungjawab',
                                            width: 100
                                        }
                                     ]
            }
            ]
        }

                //]
        //}
        return items;
};

function datasave_viDaftar(mBol)
{	
    if (ValidasiEntry_viDaftar('Simpan Data',false) == 1 )
    {
        if (addNew_viDaftar == true)
        {
            Ext.Ajax.request
            (
				{
					url: WebAppUrl.UrlSaveData,
					params: dataparam_viDaftar(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftar('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftar();
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftar').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftar = false;
							Ext.get('txtNoKunjungan_viDaftar').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtKDPasien_viDaftar').dom.value = cst.KD_PASIEN
							Ext.getCmp('btnSimpan_viDaftar').disable();	
							Ext.getCmp('btnSimpanExit_viDaftar').disable();	
							Ext.getCmp('btnDelete_viDaftar').enable();								
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
        }
        else
        {
            Ext.Ajax.request
            (
            {
                url: WebAppUrl.UrlUpdateData,
                params: dataparam_viDaftar(),
                success: function(o)
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viDaftar('Data berhasil disimpan','Edit Data');
                        datarefresh_viDaftar();
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        ShowPesanWarning_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                    else
                    {
                        ShowPesanError_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                }
            }
            )
        }
    }
    else
    {
        if(mBol === true)
        {
            return false;
        }
    }
}

function datadelete_viDaftar()
{
    // var DataHapus = Ext.get('txtNPP_viDaftar').getValue();
    if (ValidasiEntry_viDaftar('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
        {
            title:'Hapus Data',
             msg: "Akan menghapus data?" ,
            buttons: Ext.MessageBox.YESNO,
            width:300,
            fn: function (btn)
            {
                if (btn =='yes')
                {
                    Ext.Ajax.request
                    (
                    {
                        url: WebAppUrl.UrlDeleteData,
                        params: dataparamDelete_viDaftar(), //dataparam_viDaftar(),
                        success: function(o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');
                                datarefresh_viDaftar();
                                dataaddnew_viDaftar();
								Ext.getCmp('btnSimpan_viDaftar').disable();
								Ext.getCmp('btnSimpanExit_viDaftar').disable();	
								Ext.getCmp('btnDelete_viDaftar').disable();	
								
                            }
                            else if (cst.success === false && cst.pesan===0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil dihapus ' ,'Hapus Data');
                            }
                            else if (cst.success === false && cst.pesan===1)
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil dihapus '  + cst.msg ,'Hapus Data');
                            }
                        }
                    }
                    )//end Ajax.request
                } // end if btn yes
            }// end fn
        }
        )//end msg.show
    }
}
//-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viDaftar(modul,mBolHapus)
{
    var x = 1;
	
	if (Ext.get('txtNamaPs_viDaftar').getValue() === '' || Ext.get('txtNamaPs_viDaftar').getValue() === undefined)
	{
		ShowPesanWarning_viDaftar("Nama belum terisi",modul);
		x=0;
	}
	if (Ext.get('txtNoTlp_viDaftar').getValue() === '' || Ext.get('txtNoTlp_viDaftar').getValue() ===  undefined)
	{
		ShowPesanWarning_viDaftar("No Telp 1 belum terisi",modul);
		x=0;
	}
	if (Ext.getCmp('ComboJK_viDaftar').getValue() === '' || Ext.getCmp('ComboJK_viDaftar').getValue() ===  undefined)	
	{
		ShowPesanWarning_viDaftar("Jenis kelamin belum terisi",modul);
		x=0;
	}   

	if (Ext.getCmp('ComboGolDRH_viDaftar').getValue() === '' || Ext.getCmp('ComboGolDRH_viDaftar').getValue() ===  undefined)	
	{
		ShowPesanWarning_viDaftar("Golongan darah belum terisi",modul);
		x=0;
	}   
	if (Ext.getCmp('ComboPoliDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboPoliDaftar_viDaftar').getValue() ===  undefined)
	{
		ShowPesanWarning_viDaftar("Poli belum terisi",modul);
		x=0;
	}
	if (Ext.getCmp('ComboDokterDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboDokterDaftar_viDaftar').getValue() ===  undefined)
	{
		ShowPesanWarning_viDaftar("Dokter belum terisi",modul);
		x=0;
	}	
	if (Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue() === '' || Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue() ===  undefined)	
	{
		ShowPesanWarning_viDaftar("Kelompok pasien belum terisi",modul);
		x=0;
	}   
	if (Ext.getCmp('DtpTglLahir_viDaftar').getValue() > now_viDaftar )	
	{
		ShowPesanWarning_viDaftar("Tanggal lahir salah",modul);
		x=0;
	}   
    return x;
}

function ShowPesanWarning_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}

function datarefresh_viDaftar()
{
	var criteria;
	criteria = getCriteriaFilter_viDaftar()
    dataSource_viDaftar.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viDaftar,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViViewKunjungan',
				param: criteria
			}
		}
    );
    return dataSource_viDaftar;
}

function getCriteriaFilter_viDaftar()
{
	// var strKriteria = "";
	
	// strKriteria = " where K.KD_CUSTOMER ='" + strKD_CUST + "' ";
	// strKriteria += " and (K.TGL_KUNJUNGAN between '" + ShowDate(Ext.getCmp('txtfilterTglKunjAwal_viDaftar').getValue()) + "' ";
	// strKriteria += " and '" + ShowDate(Ext.getCmp('txtfilterTglKunjAkhir_viDaftar').getValue()) + "') ";
	
	
	// if(Ext.getCmp('ComboPoli_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and U.NAMA_UNIT LIKE '%" + Ext.get('ComboPoli_viDaftar').dom.value + "%' ";
	// }
	
	// if(Ext.getCmp('txtfilterNoRM_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and RIGHT(K.KD_PASIEN,10) LIKE '%" +  Ext.getCmp('txtfilterNoRM_viDaftar').getValue() + "%' ";
	// }

	// if(Ext.getCmp('txtfilterNama_viDaftar').getValue() != "")
	// {
	// 	strKriteria += " and PS.NAMA LIKE '%" +  Ext.getCmp('txtfilterNama_viDaftar').getValue() + "%' ";
	// }
	
	// return strKriteria;
}

function datainit_viDaftar(rowdata)
{
    addNew_viDaftar = false;
	
	Ext.getCmp('txtKDPasien_viDaftar').setValue(rowdata.KD_PASIEN);
	Ext.getCmp('txtNoKunjungan_viDaftar').setValue(rowdata.NO_KUNJUNGAN);		
	Ext.getCmp('txtNamaPs_viDaftar').setValue(rowdata.NAMA);
	Ext.get('txtchkPasienBaru_viDaftar').dom.checked = rowdata.PASIEN_BARU;
	Ext.getCmp('txtTmpLahir_viDaftar').setValue(rowdata.TEMPAT_LAHIR);
	
	Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(rowdata.TGL_LAHIR));
	
	Ext.getCmp('txtUmurThn_viDaftar').setValue(rowdata.TAHUN);
	Ext.getCmp('txtUmurBln_viDaftar').setValue(rowdata.BULAN);
	Ext.getCmp('txtUmurHari_viDaftar').setValue(rowdata.HARI);
	
	Ext.getCmp('ComboJK_viDaftar').setValue(rowdata.JENIS_KELAMIN);
	Ext.getCmp('ComboGolDRH_viDaftar').setValue(rowdata.GOL_DARAH);
	Ext.getCmp('txtNoTlp_viDaftar').setValue(rowdata.NO_TELP);
	Ext.getCmp('txtHP_viDaftar').setValue(rowdata.NO_HP);
	Ext.getCmp('txtAlamat_viDaftar').setValue(rowdata.ALAMAT);
	Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(rowdata.TGL_KUNJUNGAN));

	Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowdata.KD_UNIT);
	Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(rowdata.KD_DOKTER);
	Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(rowdata.KD_KELOMPOK);	

	Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(rowdata.KD_STS_MARITAL);
	Ext.getCmp('ComboPekerjaan_viDaftar').setValue(rowdata.KD_PEKERJAAN);
	Ext.getCmp('ComboAgama_viDaftar').setValue(rowdata.KD_AGAMA);
	Ext.getCmp('ComboPendidikan_viDaftar').setValue(rowdata.KD_PENDIDIKAN);
	
	Ext.getCmp('txtNadi_viDaftar').setValue(rowdata.NADI);
	Ext.getCmp('txtchkAlergi_viDaftar').setValue(rowdata.ALERGI);
	Ext.getCmp('txtTinggiBadan_viDaftar').setValue(rowdata.TINGGI_BADAN);
	Ext.getCmp('txtBeratBadan_viDaftar').setValue(rowdata.BERAT_BADAN);
	Ext.getCmp('txtTekananDarah_viDaftar').setValue(rowdata.TEKANAN_DRH);
	Ext.getCmp('txtkeluhan_viDaftar').setValue(rowdata.KELUHAN);

	Ext.get('ComboJK_viDaftar').dom.value = rowdata.JNS_KELAMIN;	
	Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowdata.NAMA_UNIT;
	Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowdata.DOKTER;
	Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowdata.KELOMPOK;
	Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowdata.STS_MARITAL;
	Ext.get('ComboPekerjaan_viDaftar').dom.value = rowdata.PEKERJAAN;
	Ext.get('ComboAgama_viDaftar').dom.value = rowdata.AGAMA;
	Ext.get('ComboPendidikan_viDaftar').dom.value = rowdata.PENDIDIKAN;
	Ext.getCmp('btnSimpan_viDaftar').disable();
	Ext.getCmp('btnSimpanExit_viDaftar').disable();	
	Ext.getCmp('btnDelete_viDaftar').enable();	
	mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	
}

function dataaddnew_viDaftar()
{
    addNew_viDaftar = true;
    dsPropinsiRequestEntry = undefined;
//	Ext.getCmp('txtKDPasien_viDaftar').setValue('');
//	Ext.getCmp('txtNoKunjungan_viDaftar').setValue('');
//	Ext.getCmp('txtNamaPs_viDaftar').setValue('');
//	Ext.getCmp('txtTmpLahir_viDaftar').setValue('');
//	Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(now_viDaftar));
//	Ext.getCmp('txtUmurThn_viDaftar').setValue('');
//	Ext.getCmp('txtUmurBln_viDaftar').setValue('');
//	Ext.getCmp('txtUmurHari_viDaftar').setValue('');
//	Ext.getCmp('ComboJK_viDaftar').setValue(1);
//	Ext.getCmp('ComboGolDRH_viDaftar').setValue(0);
//	Ext.getCmp('txtNoTlp_viDaftar').setValue('');
//	Ext.getCmp('txtHP_viDaftar').setValue('');
//	Ext.getCmp('txtAlamat_viDaftar').setValue('');
//
//	Ext.getCmp('ComboPoliDaftar_viDaftar').setValue('');
//	Ext.getCmp('ComboDokterDaftar_viDaftar').setValue('');
//	Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue('');
//	Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(now_viDaftar));
//
//	Ext.getCmp('txtNadi_viDaftar').setValue('');
//	Ext.getCmp('txtchkAlergi_viDaftar').setValue('');
//	Ext.getCmp('txtTinggiBadan_viDaftar').setValue('');
//	Ext.getCmp('txtBeratBadan_viDaftar').setValue('');
//	Ext.getCmp('txtTekananDarah_viDaftar').setValue('');
//	Ext.getCmp('txtkeluhan_viDaftar').setValue('');
//
//	Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue('');
//	Ext.getCmp('ComboPekerjaan_viDaftar').setValue('');
//	Ext.getCmp('ComboAgama_viDaftar').setValue('');
//	Ext.getCmp('ComboPendidikan_viDaftar').setValue('');
//	Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 1;
//	Ext.get('txtNamaPs_viDaftar').dom.focus();
//
//	Ext.getCmp('btnSimpan_viDaftar').enable();
//	Ext.getCmp('btnSimpanExit_viDaftar').enable();
//	Ext.getCmp('btnDelete_viDaftar').disable();
//	mNoKunjungan_viKasir = '';
	
    rowSelected_viDaftar   = undefined;
}
///---------------------------------------------------------------------------------------///

function dataparam_viDaftar()
{
		var params_ViPendaftaran =
		{
			Table: 'viKunjungan'
                        //kdpropinsi: dsPropinsiRequestEntry;
//			Kd_Kelompok: Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue(),
//			Kd_Unit: Ext.getCmp('ComboPoliDaftar_viDaftar').getValue(),
//			Kd_Dokter: Ext.getCmp('ComboDokterDaftar_viDaftar').getValue(),
//			Kd_Costumer: strKD_CUST,
//			Kd_Pasien: Ext.get('txtKDPasien_viDaftar').getValue(),
//			Tgl_Kunjungan: ShowDate(Ext.get('dtpKunjungan_viDaftar').getValue()),
//			Tinggi_Badan : Ext.get('txtTinggiBadan_viDaftar').getValue(),
//			Berat_BADAN : Ext.get('txtBeratBadan_viDaftar').getValue(),
//			Tekanan_Darah : Ext.get('txtTekananDarah_viDaftar').getValue(),
//			Nadi : Ext.get('txtNadi_viDaftar').getValue(),
//			Alergi :  Ext.get('txtchkAlergi_viDaftar').dom.checked,
//			Keluhan : Ext.get('txtkeluhan_viDaftar').getValue(),
//			Pasien_Baru : Ext.get('txtchkPasienBaru_viDaftar').dom.checked,
//			Kd_Pendidikan : Ext.getCmp('ComboPendidikan_viDaftar').getValue(), //PASIEN
//			Kd_Sts_Marital : Ext.getCmp('ComboSTS_MARITAL_viDaftar').getValue(),
//			Kd_Agama :  Ext.getCmp('ComboAgama_viDaftar').getValue(),
//			Kd_Pekerjaan : Ext.getCmp('ComboPekerjaan_viDaftar').getValue(),
//			Nama : Ext.get('txtNamaPs_viDaftar').getValue(),
//			Tempat_Lahir : Ext.get('txtTmpLahir_viDaftar').getValue(),
//			Tgl_Lahir : ShowDate(Ext.get('DtpTglLahir_viDaftar').getValue()),
//			Jenis_Kelamin: Ext.getCmp('ComboJK_viDaftar').getValue(),
//			Alamat : Ext.get('txtAlamat_viDaftar').getValue(),
//			No_Telp : Ext.get('txtNoTlp_viDaftar').getValue(),
//			No_Hp : Ext.get('txtHP_viDaftar').getValue(),
//			Gol_Darah : Ext.getCmp('ComboGolDRH_viDaftar').getValue()
//
		}
	
    return params_ViPendaftaran
}

function dataparamDelete_viDaftar()
{
		var paramsDelete_ViPendaftaran =
		{
			Table: 'viKunjungan',			
			// KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue(),
			// KD_UNIT: Ext.getCmp('ComboPoliDaftar_viDaftar').getValue(),
			// KD_DOKTER: Ext.getCmp('ComboDokterDaftar_viDaftar').getValue(),
			NO_KUNJUNGAN : Ext.get('txtNoKunjungan_viDaftar').getValue(), //Ext.getCmp('txtNoKunjungan_viDaftar').setValue(rowdata.NO_KUNJUNGAN);		
			KD_CUSTOMER: strKD_CUST
			// KD_PASIEN: Ext.get('txtKDPasien_viDaftar').getValue(),
			// TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftar').getValue()),
			// TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftar').getValue(),
			// BERAT_BADAN : Ext.get('txtBeratBadan_viDaftar').getValue(),
			// TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftar').getValue(),
			// NADI : Ext.get('txtNadi_viDaftar').getValue(),
			// ALERGI :  Ext.get('txtchkAlergi_viDaftar').dom.checked,
			// KELUHAN : Ext.get('txtkeluhan_viDaftar').getValue(),  
			// PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftar').dom.checked,						
			// KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftar').getValue(), //PASIEN
			// KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftar').getValue(),
			// KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftar').getValue(),
			// KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftar').getValue(),
			// NAMA : Ext.get('txtNamaPs_viDaftar').getValue(),
			// TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftar').getValue(),
			// TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftar').getValue()),
			// JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftar').getValue(),
			// ALAMAT : Ext.get('txtAlamat_viDaftar').getValue(),
			// NO_TELP : Ext.get('txtNoTlp_viDaftar').getValue(),
			// NO_HP : Ext.get('txtHP_viDaftar').getValue(),
			// GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftar').getValue(),
		
		}
	
    return paramsDelete_ViPendaftaran
}
//============================================ Grid Data ======================================

//-------------------------------------------- Hapus baris -------------------------------------
function HapusBarisNgajar(nBaris)
{
	if (CurrentData_viDaftar.row >= 0
	/*SELECTDATASTUDILANJUT.data.NO_SURAT_STLNJ != '' ||  SELECTDATASTUDILANJUT.data.TGL_MULAI_SURAT != '' ||
		SELECTDATASTUDILANJUT.data.TGL_AKHIR_SURAT != '' || SELECTDATASTUDILANJUT.data.ID_DANA != '' ||
		SELECTDATASTUDILANJUT.data.SURAT_DARI != '' || SELECTDATASTUDILANJUT.data.TGL_SURAT != '' || SELECTDATASTUDILANJUT.data.THN_PERKIRAAN_LULUS != ''*/) 
		{
			Ext.Msg.show
			(
				{
					title: 'Hapus Baris',
					msg: 'Apakah baris ini akan dihapus ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) 
					{
						if (btn == 'yes') 
						{
							DataDeletebaris_viDaftar()
							dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
							SELECTDATASTUDILANJUT = undefined;
						}
					},
					icon: Ext.MessageBox.QUESTION
				}
			);
		}
	else 
		{
			dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
			SELECTDATASTUDILANJUT = undefined;
		}
}

function DataDeletebaris_viDaftar() 
{
    Ext.Ajax.request
	({url: "./Datapool.mvc/DeleteDataObj",
		params: dataparam_viDaftar(),
		success: function(o) 
		{
			var cst = o.responseText;
			if (cst == '{"success":true}') 
			{
				ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');                
			}
			else
			{ 
				ShowPesanInfo_viDaftar('Data gagal dihapus','Hapus Data'); 
			}
		}
	})       
};

var mRecord = Ext.data.Record.create
(
	[
		'NIP', 
		'ID_STUDI_LANJUT', 
		'NO_SURAT_STLNJ', 
		'TGL_MULAI_SURAT', 
		'TGL_AKHIR_SURAT', 
		'ID_DANA', 
		'SURAT_DARI', 
		'TGL_SURAT', 
		'THN_PERKIRAAN_LULUS'  
	]
);
//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------
function getArrDetail_viDaftar() 
{
    var x = '';
    for (var i = 0; i < dsDetailSL_viDaftar.getCount(); i++) 
	{
        var y = '';
        var z = '@@##$$@@';
        
        y += 'NIP=' + Ext.get('txtNPP_viDaftar').getValue()
        y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftar').getValue()
        y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftar.data.items[i].data.NO_SURAT_STLNJ
		
		/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT.length == 8) 
		{	
			
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		}
		else
		{
		
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		}
		
		if (dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT.length == 8) 
		{		
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
		}
		else
		{
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
		}*/
		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
        y += z + 'ID_DANA=' + dsDetailSL_viDaftar.data.items[i].data.ID_DANA
        y += z + 'SURAT_DARI=' + dsDetailSL_viDaftar.data.items[i].data.SURAT_DARI
        alert(dsDetailSL_viDaftar.data.items[i].data.ID_DANA)
		
		/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT.length == 8) 
		{		
			y += z + 'TGL_SURAT=' + dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT
		}
		else
		{
			y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		}*/
		
		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		
		y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftar.data.items[i].data.THN_PERKIRAAN_LULUS
		
        if (i === (dsDetailSL_viDaftar.getCount() - 1)) 
		{
            x += y
        }
        else {
            x += y + '##[[]]##'
        }
    }
    return x;
};
//---------------------------- end Split row ------------------------------
function DatarefreshDetailSL_viDaftar(rowdataaparam)
{
    dsDetailSL_viDaftar.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 50,
				Sort: '',
				Sortdir: 'ASC',
				target:'viviewDetailStudiLanjut',
				param: rowdataaparam
			}
		}
    );
    return dsDetailSL_viDaftar;
}

function GetPasienDaftar(strCari)
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'getKunjunganDataPasien',
				Params:	getParamKunjunganKdPasien(strCari)
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{					
					Ext.getCmp('txtKDPasien_viDaftar').setValue(cst.KD_PASIEN);
					Ext.getCmp('txtNamaPs_viDaftar').setValue(cst.NAMA);
					Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 0;					
					Ext.getCmp('txtTmpLahir_viDaftar').setValue(cst.TEMPAT_LAHIR);
					Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(cst.TGL_LAHIR));					
					Ext.getCmp('txtUmurThn_viDaftar').setValue(cst.TAHUN);
					Ext.getCmp('txtUmurBln_viDaftar').setValue(cst.BULAN);
					Ext.getCmp('txtUmurHari_viDaftar').setValue(cst.HARI);
					Ext.getCmp('ComboJK_viDaftar').setValue(cst.JENIS_KELAMIN);
					Ext.getCmp('ComboGolDRH_viDaftar').setValue(cst.GOL_DARAH);
					Ext.getCmp('txtNoTlp_viDaftar').setValue(cst.NO_TELP);
					Ext.getCmp('txtHP_viDaftar').setValue(cst.NO_HP);	
					Ext.getCmp('txtAlamat_viDaftar').setValue(cst.ALAMAT);					
					Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(cst.KD_UNIT);
					Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(cst.KD_DOKTER);
					Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(cst.KD_KELOMPOK);									
					// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(cst.TGL_KUNJUNGAN));
					Ext.getCmp('txtNadi_viDaftar').setValue(cst.NADI);
					Ext.getCmp('txtchkAlergi_viDaftar').setValue(cst.ALERGI);
					Ext.getCmp('txtTinggiBadan_viDaftar').setValue(cst.TINGGI_BADAN);
					Ext.getCmp('txtBeratBadan_viDaftar').setValue(cst.BERAT_BADAN);
					Ext.getCmp('txtTekananDarah_viDaftar').setValue(cst.TEKANAN_DARAH);
					Ext.getCmp('txtkeluhan_viDaftar').setValue(cst.KELUHAN);
					Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(cst.KD_STS_MARITAL);
					Ext.getCmp('ComboPekerjaan_viDaftar').setValue(cst.KD_PEKERJAAN);
					Ext.getCmp('ComboAgama_viDaftar').setValue(cst.KD_AGAMA);
					Ext.getCmp('ComboPendidikan_viDaftar').setValue(cst.KD_PENDIDIKAN);
					
					// Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowdata.KD_UNIT);
					// Ext.get('ComboJK_viDaftar').dom.value = rowdata.JNS_KELAMIN;	
					// Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowdata.NAMA_UNIT;
					// Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowdata.DOKTER;
					// Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowdata.KELOMPOK;
					// Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowdata.STS_MARITAL;
					// Ext.get('ComboPekerjaan_viDaftar').dom.value = rowdata.PEKERJAAN;
					// Ext.get('ComboAgama_viDaftar').dom.value = rowdata.AGAMA;
					// Ext.get('ComboPendidikan_viDaftar').dom.value = rowdata.PENDIDIKAN;
				}
				else
				{
					ShowPesanWarning_viDaftar('No.Medrec tidak di temukan '  + cst.pesan,'Informasi');
					// Ext.get('txtNamaKRSMahasiswa').dom.value = '';								
					// Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBarcode').dom.focus();

					// var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
						// if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
						// {
							// criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
						// };
						// FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNamaKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
				};
            }

        }
	);
}

function getParamKunjunganKdPasien(kdCari)
{					
	var strKriteria = "";	
	var x ="x";
		
	
	if (kdCari == 1)
	{	
		if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtKDPasien_viDaftar').getValue() + "##";							
		}	
	}
	else if(kdCari == 2) 
	{
		if(Ext.get('txtNoTlp_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtNoTlp_viDaftar').getValue() + "##";							
		}	
	}else
	{
		if(Ext.get('txtHP_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtHP_viDaftar').getValue() + "##";							
		}	
	};
	strKriteria += strKD_CUST + "##"
	strKriteria += kdCari + "##"
	
	return strKriteria;
}

function getKriteriaCariLookup()
{					
	var strKriteria = "";	
		
	//strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
	
	if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
	{
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%'"
	}	
	
	if(Ext.get('txtNamaPs_viDaftar').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		}
	}
	
	// if(Ext.get('txtNoTlp_viDaftar').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
		// }
	// }
	
	// if(Ext.get('txtHP_viDaftar').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
		// }
	// }
	
	if(Ext.get('txtAlamat_viDaftar').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		}
	}

	if (strKriteria != "")	
	{
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	else
	{
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}		
	
	return strKriteria;
}

function GetPrintKartu(strCari)
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'getCetakKartu',
				Params:	strCari
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
				}
				else
				{				

				};
            }

        }
	);
}

function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
	(
		{
			id:'cboJK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis Kelamin ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK=b.data.displayText ;
				}
			}
		}
	);
	return cboJK;
};

function mComboGolDarah()
{
    var cboGolDarah = new Ext.form.ComboBox
	(
		{
			id:'cboGolDarah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Gol. Darah ',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, '-'], [2, 'A+'],[3, 'B+'], [4, 'AB+'],[5, 'O+'], [6, 'A-'], [7, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGolDarah,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGolDarah=b.data.displayText ;
				}
			}
		}
	);
	return cboGolDarah;
};

function mComboSatusMarital()
{
    var cboStatusMarital = new Ext.form.ComboBox
	(
		{
			id:'cboStatusMarital',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Status Marital ',
			width:110,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Blm Kawin'], [2, 'Kawin'],[3, 'Janda'], [4, 'Duda']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetSatusMarital,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetSatusMarital=b.data.displayText ;
				}
			}
		}
	);
	return cboStatusMarital;
};

function mComboKelompokPasien()
{
    var cboKelompokPasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokPasien',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Kelompok P ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Perseorangan'], [2, 'Perusahaan'],[3, 'Asuransi']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokPasien=b.data.displayText ;
				}
			}
		}
	);
	return cboKelompokPasien;
};

function mComboRujukanDari()
{
    var cboRujukanDari = new Ext.form.ComboBox
	(
		{
			id:'cboRujukanDari',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Dari ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Puskesmas'], [2, 'RS. Pemerintah'],[3, 'RS. Swasta'],[4, 'Dokter Praktek'],
                                       [5, 'Paramedik'], [6, 'Instansi Swasta'],[7, 'Kasus Polisi'],[8, 'Lain - lain']
                                      ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetRujukanDari,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetRujukanDari=b.data.displayText ;
				}
			}
		}
	);
	return cboRujukanDari;
};

function mComboNamaRujukan()
{
    var cboNamaRujukan = new Ext.form.ComboBox
	(
		{
			id:'cboNamaRujukan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Nama ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
                                       data: [[1, ''], [2, ''],[3, ''],[4, '']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetNamaRujukan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetNamaRujukan=b.data.displayText ;
				}
			}
		}
	);
	return cboNamaRujukan;
};

function mComboPoli_viDaftar(lebar,Nama_ID)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit'
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }
        }
    )

    var cbo_Poli_viDaftar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viDaftar,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftar;
}

function mComboPoli_viDaftar1(lebar,Nama_ID)
{
    var Field_poli_viDaftar1 = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar1 = new WebApp.DataStore({fields: Field_poli_viDaftar1});

	ds_Poli_viDaftar1.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit'
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }
        }
    )

    var cbo_Poli_viDaftar1 = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'NAMA_UNIT',
            displayField: 'KD_UNIT',
            store: ds_Poli_viDaftar1,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftar1;
}

function mComboAgamaRequestEntry()
{
    var Field = ['KD_AGAMA','AGAMA'];

    dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
    dsAgamaRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_agama',
			    Sortdir: 'ASC',
			    target: 'ViewComboAgama',
			    param: ''
			}
		}
	)

    var cboAgamaRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboAgamaRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Agama',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsAgamaRequestEntry,
		    valueField: 'KD_AGAMA',
		    displayField: 'AGAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectAgamaRequestEntry = b.data.KD_AGAMA;
			    }
			}
                }
	);

    return cboAgamaRequestEntry;
};

function mComboPropinsiRequestEntry()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
                    anchor: '95%'
//		    listeners:
//			{
//			    'change': function()
//				{
//                                    mComboKabupatenRequestEntry()
//                                }
//			}
                }
	);
        return cboPropinsiRequestEntry;
        
    
};

function mComboKabupatenRequestEntry()
{
      var Field = ['KD_KABUPATEN','KABUPATEN'];

      dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});
      dsKabupatenRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboKabupaten',
			    param: ''
			}
                    }
                )      
                             
   var cboKabupatenRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsKabupatenRequestEntry,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        selectKabupatenRequestEntry = b.data.KD_PROPINSI;
                                }
                                
			}
                }
	);
    return cboKabupatenRequestEntry;
};

function mComboKecamatanRequestEntry()
{
    var Field = ['KD_KECAMATAN','KECAMATAN'];

    dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});
    dsKecamatanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: ''
			}
		}
	)

    var cboKecamatanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Kecamatan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsKecamatanRequestEntry,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectKecamtanRequestEntry = b.data.KD_PROPINSI;
                                }
			}
                }
	);

    return cboKecamatanRequestEntry;
};

function mComboPendidikanRequestEntry()
{
    var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];

    dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pendidikan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPendidikan',
			    param: ''
			}
		}
	)

    var cboPendidikanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPendidikanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Pendidikan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPendidikanRequestEntry,
		    valueField: 'KD_PENDIDIKAN',
		    displayField: 'PENDIDIKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPendidikanRequestEntry = b.data.KD_PROPINSI;
                                }
			}
                }
	);

    return cboPendidikanRequestEntry;
};

function mComboPekerjaanRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPekerjaanRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                }
			}
                }
	);

    return cboPekerjaanRequestEntry;
};

function mComboSukuRequestEntry()
{
    var Field = ['KD_SUKU','SUKU'];

    dsSukuRequestEntry = new WebApp.DataStore({fields: Field});
    dsSukuRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_suku',
			    Sortdir: 'ASC',
			    target: 'ViewComboSuku',
			    param: ''
			}
		}
	)

    var cboSukuRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboSukuRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Suku ',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsSukuRequestEntry,
		    valueField: 'KD_SUKU',
		    displayField: 'SUKU',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectSukuRequestEntry = b.data.KD_PROPINSI;
                                }
			}
                }
	);

    return cboSukuRequestEntry;
};

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
    dsDokterRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_dokter',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: ''
			}
		}
	)

    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
		    fieldLabel: 'Dokter ',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    alert("is");
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};