// Data Source ExtJS # --------------

/**
*	Nama File 		: JadwalDokter.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Jadwal Dokter adalah proses untuk menentukan jadwal dokter yg bertugas di rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 26 November 2015
*	Oleh 			: ADE. S
*	Editing			: Melinda
*/

// Deklarasi Variabel pada Jadwal Dokter # --------------
var mRecordRequest = Ext.data.Record.create
(
    [
       {name: 'ASSET_MAINT', mapping:'ASSET_MAINT'},
       {name: 'ASSET_MAINT_ID', mapping:'ASSET_MAINT_ID'},
       {name: 'ASSET_MAINT_NAME', mapping:'ASSET_MAINT_NAME'},
       {name: 'LOCATION_ID', mapping:'LOCATION_ID'},
       {name: 'LOCATION', mapping:'LOCATION'},
       {name: 'PROBLEM', mapping:'PROBLEM'},
       {name: 'REQ_FINISH_DATE', mapping:'REQ_FINISH_DATE'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'IMPACT', mapping:'IMPACT'},
       {name: 'ROW_REQ', mapping:'ROW_REQ'}
    ]
);

var AddNewSetjadwal;
var selectCount_viJadwalDokterRWJ=50;
var NamaForm_viKasirRwj="Jadwal";
var mod_name_viKasirRwj="viKasirRwj";
var now_viKasirRwj= new Date();
var kddokteredit;
var AddNewRequest;
var selectKlinikPoli;
var selectDokter;


var dsSetJadwalDokterList;
var selectCountSetJadwalDokter=5;
var rowSelectedSetJadwalDokter;

var CurrentData_viKasirRwj =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = getPanelSetJadwalDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Jadwal Dokter # --------------

// Start Project Jadwal Dokter # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function RefreshDataSet_viJadwalDokterRWJ()
{	
	dsSetJadwalDokterList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCount_viJadwalDokterRWJ, 
				//Sort: 'EMP_ID',
                Sort: 'kd_dokter',
				Sortdir: 'ASC', 
				target:'ViewJadwalDokter',
				param: ''
			} 
		}
	);
	rowSelectedSetJadwalDokter = undefined;
	return dsSetJadwalDokterList;
};





function getPanelSetJadwalDokter(mod_id) 
{
    var Field = ['KD_DOKTER','KD_UNIT','NAMA_UNIT', 'NAMA', 'HARI','JAM'];
    dsSetJadwalDokterList = new WebApp.DataStore({ fields: Field });
	RefreshDataSet_viJadwalDokterRWJ();
	
    var grListSetJadwalDokter = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetJadwalDokter',
		    stripeRows: true,
		    store: dsSetJadwalDokterList,
			autoScroll: true,
		    columnLines: true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetJadwalDokter=undefined;
							rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(ridx);
					if (rowSelectedSetJadwalDokter != undefined)
					{
						setLookUp_viKasirRwj(rowSelectedSetJadwalDokter.data);
					}
					else
					{
						setLookUp_viKasirRwj();
					};
					
				}
			},
		    colModel: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colHari',
                        header: 'HARI',                      
                        dataIndex: 'HARI',
                        sortable: true,
                        width: 200,
						filter :{}
                    },
					{
					    id: 'colUnit',
					    header: 'NAMA_UNIT',					   
					    dataIndex: 'NAMA_UNIT',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colDokter',
					    header: 'DOKTER',					   
					    dataIndex: 'NAMA',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colKdDOKTER',
					    header: 'Kode Dokter',					   
					    dataIndex: 'KD_DOKTER',
					    width: 50,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colKdUnit',
					    header: 'Kode Unit',					   
					    dataIndex: 'KD_UNIT',
					    width: 50,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colJam',
					    header: 'Jam (PM/AM)',					   
					    dataIndex: 'JAM',
					    width: 100,
					    sortable: true,
						filter :{}
					},
									
                ]
			),
			bbar:new WebApp.PaggingBar({
				displayInfo: true,
				store: dsSetJadwalDokterList,
				pageSize: selectCountSetJadwalDokter,
				displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
				emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetJadwalDokter',
				    text: 'Buat Jadwal Baru',
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						setLookUp_viKasirRwj();
				    }
				},' ','-',
					{
						text: 'Refresh',
						iconCls: 'refresh',
						tooltip: 'refresh',
						id: 'btnEdit_viKasirRwj',
						handler: function(sm, row, rec)
						{
							RefreshDataSet_viJadwalDokterRWJ();
						}
					},
			]
		    //,viewConfig: { forceFit: true }
		}
	);


    var FormSetJadwalDokter = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Jadwal',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:0px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupJadwalDokter',
		    items: [grListSetJadwalDokter],
		    tbar:
			[
				
			]
		}
	);
    //END var FormSetJadwalDokter--------------------------------------------------

	RefreshDataSet_viJadwalDokterRWJ();
    return FormSetJadwalDokter ;
};


// End Function dataGrid_viKasirRwj # --------------


/**
*	Function : setLookUp_viJadwalDokterRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

//test buat popup jadwal baru
function setLookUp_viKasirRwj(rowdata)
{
    var lebar = 465;
    setLookUps_JadwalDokterRWJ = new Ext.Window({
		id: 'setLookUps_JadwalDokterRWJ',
		name: 'setLookUps_JadwalDokterRWJ',
		title: 'Buat Jadwal Baru Dokter', 
		closeAction: 'destroy',        
		width: lebar,
		height:210,
		resizable:false,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,		
		items: getFormItemEntry_viJadwalDokterRWJ(lebar,rowdata), //1
	});

    setLookUps_JadwalDokterRWJ.show();
    if (rowdata == undefined)
    {
        dataaddnew_viJadwalDokterRWJ();
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viJadwalDokterRWJ(rowdata);
    }
}

function dataaddnew_viJadwalDokterRWJ()
{
	Ext.get('cboHari_JadwalDokterRWJ').dom.value = '';
    Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').dom.value = '';
	Ext.get('cboDokter_JadwalDokterRWJ').dom.value = '';
	selectKlinikPoli = undefined;
	selectDokter = undefined;
}

function datainit_viJadwalDokterRWJ(rowdata) 
{	
    AddNewSetjadwal = false;
    Ext.get('cboHari_JadwalDokterRWJ').dom.value = rowdata.HARI;
	
    Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').dom.value = rowdata.NAMA_UNIT;
	loaddatastoredokter_JadwalDokterRWJ(rowdata.KD_UNIT);
	Ext.get('cboDokter_JadwalDokterRWJ').dom.value = rowdata.NAMA;
	Ext.get('txtKdPoli').dom.value = rowdata.KD_UNIT;
	Ext.get('txtKdDokter').dom.value = rowdata.KD_DOKTER;
	Ext.get('txtJam_JadwalDokterRWJ').dom.value = rowdata.JAM.substr(0,2);
	Ext.get('txtMenit_JadwalDokterRWJ').dom.value = rowdata.JAM.substr(3,2);
	kddokteredit = rowdata.KD_DOKTER;
	Kd_Unit: selectKlinikPoli;
	Kd_Dokter: selectDokter;
	if (rowdata.HARI === null)
	{
		Ext.get('cboHari_JadwalDokterRWJ').dom.value = '';
	}
	else
	{
		Ext.get('cboHari_JadwalDokterRWJ').dom.value = rowdata.HARI;
	};
	
	if (rowdata.KD_UNIT === null)
	{
			Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').dom.value = '';
	}
	else
	{
			Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').dom.value = rowdata.NAMA_UNIT;
	};
	
	if (rowdata.KD_DOKTER === null)
	{
		Ext.get('cboDokter_JadwalDokterRWJ').dom.value = '';
	}
	else
	{
		Ext.get('cboDokter_JadwalDokterRWJ').dom.value = rowdata.NAMA;
	};

};

function SetJadwalAddNew() 
{
    AddNewSetjadwal = true;   
	Ext.get('cboHari_JadwalDokterRWJ').dom.value = '';
    Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').dom.value = '';
	Ext.get('cboDokter_JadwalDokterRWJ').dom.value = '';
	Ext.get('txtMenit_JadwalDokterRWJ').dom.value = '';
	Ext.get('txtJam_JadwalDokterRWJ').dom.value = '';
	
};

function getFormItemEntry_viJadwalDokterRWJ(lebar,rowdata)
{
    var pnlFormDataBasic_viJadwalDokterRWJ = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			width: 450,
			height: 175,
			border: false,
			items:[
				getItemPanelInputJadwalBaru_viJadwalDokterRWJ()
			],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viJadwalDokterRWJ(false);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viJadwalDokterRWJ(true);
							
							if (x===undefined)
							{
								datasave_viJadwalDokterRWJ(true);
								setLookUps_JadwalDokterRWJ.close();
								RefreshDataSet_viJadwalDokterRWJ();
							}
						}
					},
                                        {
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus',
						iconCls: 'remove',
						id: 'btnDelete_viDaftar',
						handler: function()
						{
							SetJadwalDokterDelete();
							RefreshDataSet_viJadwalDokterRWJ();
						}
					},
					  {
						xtype: 'tbseparator'
					},
					
				]
			}
			
		}
    )

    return pnlFormDataBasic_viJadwalDokterRWJ;
}

function getItemPanelInputJadwalBaru_viJadwalDokterRWJ()
{
    var items =
	{
	    layout: 'column',
	    border: true,
		bodyStyle: 'padding:10px 10px 10px 10px',
	    items:
		[
			{
				columnWidth: .99,
				layout: 'form',
				labelWidth:90,
				border: false,
				items:
				[
					mComboHari_JadwalDokterRWJ(),
					mCombo2Poliklinik_JadwalDokterRWJ(),
					mCombojadwalDokter_JadwalDokterRWJ(),
				]
			},
			{
				columnWidth: .35,
				layout: 'form',
				border: false,
				labelWidth:90,
				items:
				[
					
					{
						xtype: 'textfield',
						fieldLabel: 'Jam Praktek',
						emptyText: 'Jam',
						id	: 'txtJam_JadwalDokterRWJ',
						anchor:'100%',
					}
				]
			},
			{
				columnWidth: .60,
				layout: 'form',
				border: false,
				labelWidth:1,
				items:
				[
					
					{
						xtype: 'textfield',
						emptyText: 'Menit',
						id	: 'txtMenit_JadwalDokterRWJ',
						width: 50,
						
					},
				]
			},
			{
				xtype: 'textfield',
				fieldLabel: nmRequestId + ' ',
				id: 'txtKdPoli',
				hidden:true,
				width: 100,
			},
			{
				xtype: 'textfield',
				fieldLabel: nmRequestId + ' ',
				id: 'txtKdDokter',
				hidden:true,
				width: 100,
			},
		]
    };
    return items;
};


function mComboHari_JadwalDokterRWJ()
{
    var cboHari_JadwalDokterRWJ = new Ext.form.ComboBox
	(
		{
			id:'cboHari_JadwalDokterRWJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Hari ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
					data: [[1, 'Senin'], [2, 'Selasa'],[3, 'Rabu'],[4, 'Kamis'],
						   [5, 'Jumat'], [6, 'Sabtu'],[7, 'Minggu']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:
			{
				'select': function(a,b,c)
				{
					
				}
			}
		}
	);
	return cboHari_JadwalDokterRWJ;
};

function mCombo2Poliklinik_JadwalDokterRWJ()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )

    var cbPolikliniktEntry_viJadwalDokterRWJ = new Ext.form.ComboBox
    (
        {
            id: 'cboPolikliniktEntry_viJadwalDokterRWJ',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width: 200,
            listeners:
			{
				'select': function(a, b, c)
				{
				   //alert(b.data.KD_UNIT)
				   loaddatastoredokter_JadwalDokterRWJ(b.data.KD_UNIT);
				   Ext.get('cboDokter_JadwalDokterRWJ').dom.value = '';
				}
			}
        }
    )

    return cbPolikliniktEntry_viJadwalDokterRWJ;
};


function loaddatastoredokter_JadwalDokterRWJ(kd_unit)
{
	dsDokterRequestEntry_JadwalDokterRWJ.load
	(
		{
			params:
			{
				Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
		}
	)
};



function mCombojadwalDokter_JadwalDokterRWJ()
{ 
	var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry_JadwalDokterRWJ = new WebApp.DataStore({fields: Field});
    var cboDokter_JadwalDokterRWJ = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokter_JadwalDokterRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Dokter ',
		    store: dsDokterRequestEntry_JadwalDokterRWJ,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			width: 200,
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectDokter = b.data.KD_DOKTER;
					Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER
				},
			}
		}
	);

    return cboDokter_JadwalDokterRWJ;

};

function SetJadwalDokterDelete() 
{
	Ext.Msg.show ({
		title:nmHeaderHapusData,
		msg: "Apakah anda yakin akan menghapus ini?" ,
		buttons: Ext.MessageBox.YESNO,
		width:250,
		fn: function (btn) 
		{			
			if (btn === 'yes'){
				Ext.Ajax.request({
					url: WebAppUrl.UrlDeleteData,
					params: getParamRequestDelete_viJadwalDokterRWJ(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true){
							RefreshDataSet_viJadwalDokterRWJ();
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanHapusSukses,nmHeaderHapusData);
							setLookUps_JadwalDokterRWJ.close();
							dataaddnew_viJadwalDokterRWJ();	
						}else if (cst.success === false && cst.pesan===0){
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanHapusGagal,nmHeaderHapusData);
						}else {
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanHapusError,nmHeaderHapusData);
						};
					}
				})
			};
		}
	})
};


//awal simpan detail
function getParamRequestSave_viJadwalDokterRWJ() 
{
    var params =
	{
		Table:'ViewJadwalDokter',
		Hari: Ext.get('cboHari_JadwalDokterRWJ').getValue(),
		Kd_Unit: Ext.getCmp('cboPolikliniktEntry_viJadwalDokterRWJ').getValue(),
		Kd_Dokter: Ext.getCmp('cboDokter_JadwalDokterRWJ').getValue(),
		jam:Ext.getCmp('txtJam_JadwalDokterRWJ').getValue(),
		menit:Ext.getCmp('txtMenit_JadwalDokterRWJ').getValue(),
	};
    return params
};

function getParamRequestDelete_viJadwalDokterRWJ() 
{
    var params =
	{
		Table:'ViewJadwalDokter',
		Hari: Ext.get('cboHari_JadwalDokterRWJ').getValue(),
		Kd_Dokter: Ext.get('txtKdDokter').getValue(),
		Kd_Unit: Ext.get('txtKdPoli').getValue(),
	};
    return params
};




function datasave_viJadwalDokterRWJ(mBol) 
{	
	if (ValidasiEntryCMRequest_viJadwalDokterRWJ(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewRequest == true) 
		{
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/main/CreateDataObj",					
					params: getParamRequestSave_viJadwalDokterRWJ(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataSet_viJadwalDokterRWJ();
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest_viJadwalDokterRWJ(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamRequestSave_viJadwalDokterRWJ(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSet_viJadwalDokterRWJ();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest_viJadwalDokterRWJ(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest_viJadwalDokterRWJ(nmPesanSimpanError,nmHeaderSimpanData);
						};
						RefreshDataSet_viJadwalDokterRWJ();
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ValidasiEntryCMRequest_viJadwalDokterRWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboHari_JadwalDokterRWJ').getValue() == '') || (Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').getValue() == '') || (Ext.get('cboDokter_JadwalDokterRWJ').getValue() == '') )
	{
		if (Ext.get('cboHari_JadwalDokterRWJ').getValue() == '') 
		{
			x = 0;
		}
		else if (Ext.get('cboPolikliniktEntry_viJadwalDokterRWJ').getValue() == '') 
		{
			ShowPesanWarningRequest_viJadwalDokterRWJ(nmGetValidasiKosong(nmRequestId), modul);
			x = 0;
		}
		else if (Ext.get('cboDokter_JadwalDokterRWJ').getValue() == '') 
		{
			ShowPesanWarningRequest_viJadwalDokterRWJ(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
	};
	return x;
};


function ShowPesanWarningRequest_viJadwalDokterRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRequest_viJadwalDokterRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

