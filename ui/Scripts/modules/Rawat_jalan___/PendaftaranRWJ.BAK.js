// Data Source
var dataSource_viDaftar;
var datasource_gettempat;
var datagetappto;
var jeniscus='0';
var namacbokelpasien='umum';
var selectCount_viDaftar=100;
var NamaForm_viDaftar="Pendaftaran RWJ";
var mod_name_viDaftar="viDaftar";
var cboAsuransi;
var now_viDaftar= new Date;
var Today_Pendaftaran = new Date;
var h=now_viDaftar.getHours();
var m=now_viDaftar.getMinutes();
var s=now_viDaftar.getSeconds();
var year=now_viDaftar.getFullYear();
var nowSerahIjazah_viDaftar= new Date();
var urutJenjang_viDaftar;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftar;
var rowSelected_viDaftar;
var tmp_tempat;
var setLookUps_viDaftar;
var ds_KABUPATEN_viKasirRwj;
var ds_KECAMATAN_viKasirRwj;
var tampung;
var BlnIsDetail;
var SELECTDATASTUDILANJUT;

var dataKabupaten;

var tmpnotransaksi;
var tmpkdkasir;

var selectPoliPendaftaran;
var selectKelompokPoli;

var selectPendidikanPendaftaran;
var selectPekerjaanPendaftaran;
var selectPendaftaranStatusMarital;
var selectAgamaPendaftaran;
var mNoKunjungan_viKasir='1';
var selectSetJK;
var selectSetPerseorangan;
var selectSetAsuransi;
var selectSetGolDarah;
var selectSetSatusMarital;
var selectSetKelompokPasien;
var selectSetRujukanDari;
var selectSetNamaRujukan;
var dsAgamaRequestEntry;
//var selectAgamaRequestEntry;
var dsPropinsiRequestEntry;
var selectPropinsiRequestEntry;
var dsKecamatanRequestEntry;
var dsKecamatanPenanggungJawab;
var dsKelurahanPenanggungJawab;
var dsKelurahanKtpPenanggungJawab;
//var selectKecamtanRequestEntry;
var dsPendidikanRequestEntry;
var dsPekerjaanRequestEntry;
var dsPekerjaanPenanggungJawabRequestEntry;
var dsDokterRequestEntry;
var dsKabupatenRequestEntry;
var dsKabupatenpenanggungjawab;
var dsKabupatenKtppenanggungjawab;
var selectSetWarga;
var selectSetHubunganKeluarga;
var txtedit = "Tambah Data";
var selectAgamaRequestEntry;
var selectPropinsiRequestEntry;
var selectKabupatenRequestEntry;
var selectKecamatanRequestEntry;
var tmpcriteriaRWJ = "transaksi.kd_kasir = '01'";


var CurrentData_viDaftar =
{
	data: Object,
	details: Array,
	row: 0
};


/*Ext.getDoc().on('keypress', function(event, target) {
    if (!event.altKey && event.shiftKey) {
        event.stopEvent();


        switch(event.getKey()) {
        //switch(Ext.EventObject.getKey()) {

            case event.F1  :
			if(Ext.get('txtNoRequest').getValue() != '' )
			{
            	 //alert("Delte");
				 GetPrintKartu();
			}
			else
			{
				alert('aaa');
			}
				break;
				 case event.F2  :
			setLookUp_viDaftar();
				break;

            // other cases...
        }

    }
});*/
	

CurrentPage.page = dataGrid_viDaftar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function dataGrid_viDaftar(mod_id)
{
       var FieldMaster_viDaftar = 
	[
                    'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
                    'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                    'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KD_KELURAHAN','KABUPATEN','KECAMATAN','PROPINSI',
                    'KD_KABUPATEN','KD_KECAMATAN','KD_PROPINSI','KD_PENDIDIKAN','KD_PEKERJAAN','KD_AGAMA'
	];

    dataSource_viDaftar = new WebApp.DataStore({fields: FieldMaster_viDaftar});
    
	//datarefresh_viDaftar(tmpcriteriaRWJ);
            
	var grData_viDaftar = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDaftar,
			autoScroll: true,
			columnLines: true,
			border:false,
                        trackMouseOver: true,
			anchor: '100% 79%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDaftar = undefined;
							rowSelected_viDaftar = dataSource_viDaftar.getAt(row);
							CurrentData_viDaftar;
							CurrentData_viDaftar.row = row;
							CurrentData_viDaftar.data = rowSelected_viDaftar.data;
                                                        Ext.getCmp('btntambah_viDaftar').hide()
                                                        Ext.getCmp('btnEdit_viDaftar').show()
                                                }
                                        }
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                        
                                        rowSelected_viDaftar = dataSource_viDaftar.getAt(ridx);
					if (rowSelected_viDaftar !== undefined)
					{
						
						setLookUp_viDaftar(rowSelected_viDaftar.data);
						
					}
					else
					{
						setLookUp_viDaftar();
					}
				},
                 containerclick : function(){
                                                Ext.getCmp('btntambah_viDaftar').show()
                                                Ext.getCmp('btnEdit_viDaftar').hide()
                                            }
                                },
                                

			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftar',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						hideable:false,
						menuDisabled:true,
						width: 15

					},
					{
						id: 'colNMPASIEN_viDaftar',
						header: 'Nama',
						dataIndex: 'NAMA',
						hideable:false,
						menuDisabled:true,
						sortable: true,
						width: 50

					},
					{
						id: 'colALAMAT_viDaftar',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 50,
						hideable:false,
						menuDisabled:true,
						sortable: true

					},
					{
						id: 'colTglKunj_viDaftar',
						header:'Tgl Lahir',
						dataIndex: 'TGL_LAHIR',
						width: 20,
						sortable: true,
						hideable:false,
						menuDisabled:true,
						format: 'd/M/Y',

						renderer: function(v, params, record)
						{

                                                       return ShowDate(record.data.TGL_LAHIR);
						}
					},
					{
						id: 'colPoliTujuan_viDaftar',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 20,
						hideable:false,
						menuDisabled:true,
						sortable: true
					}
				]
				),
                        tbar:
                            {
				xtype: 'toolbar',
				id: 'toolbar_viDaftar',
				items:
                                    [
                                            {
						xtype: 'button',
						text: 'Kunjungan',
                                                hidden : true,
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDaftar',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDaftar != undefined)
							{
                            setLookUp_viDaftar(rowSelected_viDaftar.data);                                
							}
							else
							{
								setLookUp_viDaftar();
                                                                if (Ext.get('txtTglLahir').getValue() != "")
                                                                {
                                                                    Ext.get('txtTglLahir').getValue(Ext.get('txtTglLahir').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.get('txtTglLahir').getValue('');
                                                                    }
                                                            if (Ext.get('txtAlamatPasien').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtAlamat').setValue(Ext.get('txtAlamatPasien').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtAlamat').setValue('');
                                                                    }
                                                            if (Ext.get('txtNamaPasien').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtNama').setValue(Ext.get('txtNamaPasien').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtNama').setValue('');
                                                                    }
							}
						}
					},
                                        {
						xtype: 'button',
						text: 'Pasien Baru',
						iconCls: 'Edit_Tr',
                                                hidden : false,
						tooltip: 'Edit Data',
						id: 'btntambah_viDaftar',
						handler: function(sm, row, rec)
						{
							 
							
                                                        setLookUp_viDaftar();
                                                            if (Ext.get('txtTglLahir').getValue() != "")
                                                                {
                                                                    var tmpTanggalLahir = ShowDate(Ext.get('txtTglLahir').getValue())
                                                                    Ext.get('txtTglLahir').getValue(tmpTanggalLahir);
                                                                }
                                                                else
                                                                    {
                                                                        Ext.get('txtTglLahir').getValue('');
                                                                    }
                                                            if (Ext.get('txtAlamatPasien').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtAlamat').setValue(Ext.get('txtAlamatPasien').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtAlamat').setValue('');
                                                                    }
                                                            if (Ext.get('txtNamaPasien').getValue() != "")
                                                                {
                                                                   var nama = Ext.get('txtNamaPasien').getValue();
																	Ext.getCmp('txtNama').setValue(nama);
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtNama').setValue('');
                                                                    }
																	
																	 
						}
					},' ', '-',
                                        {
                                                xtype: 'checkbox',
                                                id: 'chkWithTglRequest',
                                                 text: 'Select Bacon',
                                                hideLabel:false,
                                                checked: false,

                                                handler: function()
                                                  {
                                                    if (this.getValue()===true)
                                                    {
                                                        criteria = "kunjungan.tgl_masuk = " + "'" + now_viDaftar.format('Y/M/d') + "'" + "and " + tmpcriteriaRWJ;
                                                        datarefresh_viDaftar(criteria);
                                                    }
                                                    else
                                                    {
                                                        criteria = ""
                                                        datarefresh_viDaftar(criteria);
                                                    }
                                                  }
                                            },
                                            {xtype: 'tbtext', text: 'Data Pasien Hari Ini ', cls: 'left-label', width: 90}
									
                                    ]},

			bbar : bbar_paging(mod_name_viDaftar, selectCount_viDaftar, dataSource_viDaftar)
					,
			viewConfig: 
			{
				forceFit: true
			}
		}
		
    )
	//datarefresh_viDaftar(tmpcriteriaRWJ);
     var top = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [{
            layout:'column',
            items:[{
                columnWidth: .3,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrec',
                        id: 'txtNoMedrec',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue())
                                             Ext.getCmp('txtNoMedrec').setValue(tmpgetNoMedrec);
                                             var tmpkriteria = getCriteriaFilter_viDaftar();
                                             datarefresh_viDaftar(tmpkriteria);
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        tmpkriteria = getCriteriaFilter_viDaftar();
                                                        datarefresh_viDaftar(tmpkriteria);
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrec').setValue('')
                                            }
                                }
                            }

                        }

                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Tanggal Lahir (ddMMYYYY) ',
                        name: 'txtTglLahir',
                        id: 'txtTglLahir',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
								
								
                                var tmptanggal = Ext.get('txtTglLahir').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                       if (tmptanggal.length !== 0)
                                        {
                                            if(tmptanggal.length === 8)
                                                {
                                                    var tmptgl = ShowDateUbah(Ext.get('txtTglLahir').getValue())
                                                    Ext.getCmp('txtTglLahir').setValue(tmptgl);
                                                }
                                                else
                                                    {
                                                         if(tmptanggal.length === 10)
                                                            {
                                                                 var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                 datarefresh_viDaftar(tmpkriteria);
                                                            }
                                                            else
                                                                {
                                                                    alert("Format yang Anda masukan salah...");
                                                                }
                                                     }
                                        }
                                            tmpkriteria = getCriteriaFilter_viDaftar();
                                            datarefresh_viDaftar(tmpkriteria);
                                }
                            }

                        }

                    }
             ]
            },{
                columnWidth:.5,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Pasien ',
                        name: 'txtNamaPasien',
                        id: 'txtNamaPasien',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								      var tmpkriteria = getCriteriaFilter_viDaftar();
                                       datarefresh_viDaftar(tmpkriteria); 
							}
						}
                        }
                    },
                    {
                    xtype: 'textfield',
                    fieldLabel: 'Alamat Pasien ',
                    name: 'txtAlamatPasien',
                    enableKeyEvents: true,
                    id: 'txtAlamatPasien',
                    anchor: '95%',
                    listeners:
                        {
                          	'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
										  var tmpkriteria = getCriteriaFilter_viDaftar();
										   datarefresh_viDaftar(tmpkriteria); 
								}
							}
                        }
                }
             ]
            }]
        }]       
    });

    //top.render(document.body);

    var FrmTabs_viDaftar = new Ext.Panel
    (
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: NamaForm_viDaftar,          
			border: false,  
			closable: true,
			iconCls: 'Studi_Lanjut',
			margins: '5 5 5 5',
			
                        items: [ top , grData_viDaftar],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					datarefresh_viDaftar(tmpcriteriaRWJ);
				}
			}
		}
    )
    return FrmTabs_viDaftar;
}

function setLookUp_viDaftar(rowdata)
{
    var lebar = 900;
    setLookUps_viDaftar = new Ext.Window
    (
    {
        id: 'SetLookUps_viDaftar',
        name: 'SetLookUps_viDaftar',
        title: NamaForm_viDaftar, 
        closeAction: 'destroy',        
        width: lebar,
        height: 575,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftar(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				//alert(Ext.get('txtNamaPasien').getValue());
				Ext.getCmp('txtNama').focus(true, 10);
                Ext.getCmp('cboSukuRequestEntry').hide(); //suku di hide
                Ext.getCmp('txtJamKunjung').hide();
                Ext.getCmp('dptTanggal').hide();
                Ext.getCmp('txtNamaPeserta').hide();
                Ext.getCmp('txtNoAskes').hide();
                Ext.getCmp('txtNoSJP').hide();
                Ext.getCmp('cboPerseorangan').hide();
              
			  Ext.getCmp('cboAsuransi').show(); // show combo
				
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                if(rowdata !== undefined)
                {
                    Ext.getCmp('txtNama').disable();
                    Ext.getCmp('txtNoRequest').disable();
                    Ext.getCmp('txtNama').disable();
                    Ext.getCmp('txtNamaKeluarga').disable();
                    Ext.getCmp('txtTempatLahir').disable();
                    Ext.getCmp('cboPendidikanRequestEntry').disable();
                    Ext.getCmp('cboPekerjaanRequestEntry').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('txtAlamat').disable();
                    Ext.getCmp('cboAgamaRequestEntry').disable();
                    Ext.getCmp('cboGolDarah').disable();
                    Ext.getCmp('dtpTanggalLahir').disable();
                    Ext.getCmp('cboStatusMarital').disable();
                    Ext.getCmp('cboJK').disable();
                    Ext.getCmp('cboWarga').disable();
                    Ext.getCmp('cboPropinsiRequestEntry').disable();
                    Ext.getCmp('cboKabupatenRequestEntry').disable();
                    Ext.getCmp('cboKecamatanRequestEntry').disable();
                }
            },
            afterShow: function()
            {
                this.activate();
				
            },
            deactivate: function()
            {
                rowSelected_viDaftar=undefined;
                datarefresh_viDaftar(tmpcriteriaRWJ);
		mNoKunjungan_viKasir = '';
                Ext.getCmp('txtNoMedrec').setValue('');
                Ext.getCmp('txtTglLahir').setValue('');
                Ext.getCmp('txtAlamatPasien').setValue('');
                Ext.getCmp('txtNamaPasien').setValue('');
				      Ext.getCmp('btntambah_viDaftar').show()
                      Ext.getCmp('btnEdit_viDaftar').hide()
            }
        }
    }
    );

    setLookUps_viDaftar.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftar();
		// Ext.getCmp('btnSimpan_viDaftar').disable();
		// Ext.getCmp('btnSimpanExit_viDaftar').disable();	
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viDaftar(rowdata);
    }
}

// From view popup data daftar pasien / kunjungan
function getFormItemEntry_viDaftar(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftar = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                        autoScroll: true,
			width: lebar,//lebar-55,
                        height: 900,
			border: false,

                        items:[
                            getItemPanelInputBiodata_viDaftar(),
                            getPenelItemDataKunjungan_viDaftar(lebar)
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDaftar',
						handler: function(){
							dataaddnew_viDaftar();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar(tmpcriteriaRWJ);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viDaftar(addNew_viDaftar);
							datarefresh_viDaftar(tmpcriteriaRWJ);
							if (x===undefined)
							{
								setLookUps_viDaftar.close();
							}
						}
					},
                                        {
						xtype: 'tbseparator'
					},
					/*{
						xtype:'button',
						text:'Lookup Pasien',
						iconCls:'find',
						id:'btnLookUp_viDaftar',
						handler:function(){
							FormLookupPasien("","","","","","");
						}
					},				
					{
						xtype:'tbseparator'
					},*/	
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viDaftar',
						handler:function()
						{							
																				
                                                    GetPrintKartu();								
														
						},
                                                //handler: optionsHandler, // handle a click on the button itself
                                                menu: new Ext.menu.Menu({
                                                    items: [
                                                        // these items will render as dropdown menu items when the arrow is clicked:
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrintBill',
                                                            handler: function()
                                                            {
                                                                printbill()
//                                                                    datadelete_viDaftar();
//                                                                    datarefresh_viDaftar();

                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print ',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrint',
                                                            handler: function()
                                                            {
                                                                
                                                                 var criteria = 'kd_pasien = '+ Ext.get('txtNoRequest').getValue() +' And kd_unit = '+ Ext.getCmp('cboPoliklinikRequestEntry').getValue();
                                                                 ShowReport('0', 1010204, criteria);

                                                            }
                                                        },
                                                    ]
                                                })
					}
                                        
				]
			}
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viDaftar;
}
//-----------------------------end----------------------------------------------

//form view data biodata pasien
function getItemPanelInputBiodata_viDaftar() 
{
	
    var items =
	{
	    layout: 'column',
	    border: false,
            labelAlign: 'top',
	    items:
		[
                        {
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKecamatan',
					    id: 'txtTmpKecamatan',
                        emptyText:'',
                        hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKabupaten',
					    id: 'txtTmpKabupaten',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPropinsi',
					    id: 'txtTmpPropinsi',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPendidikan',
					    id: 'txtTmpPendidikan',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPekerjaan',
					    id: 'txtTmpPekerjaan',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpAgama',
					    id: 'txtTmpAgama',
                                            emptyText:'',
                                            hidden : true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'No. Medrec ',
					    name: 'txtNoRequest',
					    id: 'txtNoRequest',
                                            emptyText:'Automatic from the system...',
                                            readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNama',
					    id: 'txtNama',
						tabIndex:1,
                                            emptyText:' ',
                                            disabled:false,
					    anchor: '95%',
//                                            regex:/^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z\s?]{2,5}){1,25})*(\s*?;\s*?)*)*$/,
                                            enableKeyEvents: true,
                                            listeners:
 {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('txtNamaKeluarga').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama Keluarga ',
					    name: 'txtNamaKeluarga',
					    id: 'txtNamaKeluarga',
						tabIndex:2,
						
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboJK').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				
                            labelWidth:90,
			    items:
				[
                                  mComboJK()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Tempat Lahir ',
					    name: 'txtTempatLahir',
					    id: 'txtTempatLahir',
						tabIndex:4,
					
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('dtpTanggalLahir').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal Lahir ',
					    name: 'dtpTanggalLahir',
					    id: 'dtpTanggalLahir',
						tabIndex:5,
					         //format: 'M/d/Y',
                                            format : "d/M/Y",
                                            value: Today_Pendaftaran,
					    anchor: '95%',
                                            listeners:
                                                {
						'specialkey' : function()
                                                    {
														
														
														if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) // 9 Kode Kode Char Untuk Tab
{															 var tmptanggal = Ext.get('dtpTanggalLahir').getValue();
                                                            Ext.Ajax.request
                                                                ( {
                                                                           url: baseURL + "index.php/main/GetUmur",
                                                                           params: {
                                                                           TanggalLahir: ShowDateReal(tmptanggal)
                                                                           },
                                                                           success: function(o)
                                                                           {
																			//alert('test');   
                                                                           var tmphasil = o.responseText;
                                                                         // alert(tmphasil);
                                                                           var tmp = tmphasil.split(' ');
																		    //alert(tmp.length);
                                                                          if (tmp.length == 6)
                                                                                                {
                                                                                                    Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                    Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[4]);
                                                                                                }
                                                                                                else if(tmp.length == 4)
                                                                                                {
                                                                                                    if(tmp[1]== 'years' && tmp[3] == 'day')
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue(tmp[2]);  
                                                                                                    }else{
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[2]);
                                                                                                          }
                                                                                                }
                                                                                                else if(tmp.length == 2 )
                                                                                                {
																									
                                                                                                    if (tmp[1] == 'year' )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'years' )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'mon'  )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'mons'  )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else{
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[0]);
                                                                                                        }
                                                                                                }
																								
																								else if(tmp.length == 1)
                                                                                                {
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtHariLahir').setValue('1');
                                                                                                }else
                                                                                                {
                                                                                                    alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                                }
																		   }
                                                                           
                                                                           });
}
//                                                        var tmptanggal = Ext.get('dtpTanggalLahir').getValue()
//                                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
//                                                        {
////                                                               if (tmptanggal.length !== 0)
////                                                                {
////                                                                    if(tmptanggal.length === 8)
////                                                                        {
//                                                                            var tmptgl = ShowDateUbahdatetimeextjs(Ext.get('dtpTanggalLahir').getValue())
//                                                                            Ext.getCmp('dtpTanggalLahir').setValue(tmptgl);
//                                                                        }
//                                                                        else
//                                                                            {
//                                                                                 if(tmptanggal.length === 10)
//                                                                                    {
//                                                                                         var tmpkriteria = getCriteriaFilter_viDaftar();
//                                                                                         datarefresh_viDaftar(tmpkriteria);
//                                                                                    }
//                                                                                    else
//                                                                                        {
//                                                                                            alert("Format yang Anda masukan salah...");
//                                                                                        }
//                                                                             }
//                                                                }
//                                                        }
							/*if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) // 9 Kode Kode Char Untuk Tab
							{
                                                            var year1=Ext.get('dtpTanggalLahir').getValue();
                                                                var fullyearnow= now_viDaftar.format('d/M/Y');
                                                                var tmp = fullyearnow.substring(7, 11);
                                                                var tmp2 = year1.substring(7, 11);
                                                                var Umur = tmp - tmp2
//                                                                alert(year1)
//                                                                alert(fullyearnow)
                                                            if(tmp2 < tmp)
                                                                {
                                                                    Ext.getCmp('txtThnLahir').setValue(Umur);
                                                                    Ext.getCmp('txtBlnLahir').setValue('0');
                                                                    Ext.getCmp('txtHariLahir').setValue('0');
                                                                }
                                                                else if(tmp2 == tmp)
                                                                    {
                                                                        var tmpblnlahir = ShowNewDate(year1)
                                                                        var tmpblnsekarang = ShowNewDate(fullyearnow)
                                                                        //var bulan =  tmpblnsekarang - tmpblnlahir
                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                        //alert(tmpblnlahir)
                                                                        //alert(tmpblnsekarang)
//                                                                       var umurku = datediff(year1,fullyearnow,'Y')
//                                                                       alert(umurku)
                                                                    }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                    }
                                                                
								
							}*/
                                                    },
                                                   'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboAgamaRequestEntry').focus();
                                                    }, c);

                                                    }
                                                }

					}
				]
                                
			},
                          {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Thn ',
					    name: 'txtThnLahir',
					    id: 'txtThnLahir',
				tabIndex:6,
						
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Bln ',
					    name: 'txtBlnLahir',
					    id: 'txtBlnLahir',
						tabIndex:7,

						
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                                                {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
			
                            labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Hari ',
					    name: 'txtHariLahir',
					    id: 'txtHariLahir',
							tabIndex:8,

						
                                            emptyText:' ',
					    anchor: '95%'
					}
				]
			},
                        {
			    columnWidth: .22,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                 mComboAgamaRequestEntry(),
				]
			},
                        {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:0,
                            
			    items:
				[
                                 mComboGolDarah()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                  mComboWargaNegara()
                                ]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
				mComboSatusMarital()
				]
			},
                        
                        {
			    columnWidth: .70,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamat',
					    id: 'txtAlamat',
						tabIndex:13,
                                            emptyText:' ',
					    anchor: '95%',
                                            listeners: {
                                                    'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cboPropinsiRequestEntry').focus();
                                                    }, c);
                                                }
                                            }
					}
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPropinsiRequestEntry()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboKabupatenRequestEntry()
                                ]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                      mComboKecamatanRequestEntry()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                     mComboPendidikanRequestEntry()
				]
			},
                        {
			    columnWidth: .215,
			    layout: 'form',
			    border: false,
                            labelWidth:100,
			    items:
				[
                                    mComboPekerjaanRequestEntry()
				]
			},
                        {
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
                            
                            labelWidth:90,
			    items:
				[
                                     mComboSukuRequestEntry()
                                ]
                        },
                ]
        };
    return items;
};
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;

//sub form view data kunjungan pasien 1
function getItemDataKunjungan_viDaftar1()
{
   
        var items =
            {

                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
		[
			{
			    columnWidth: .15,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    name: 'dtpTanggal',
					    id: 'dptTanggal',
                                            format: 'd/M/Y',
                                            value: now_viDaftar,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
					{
                                        xtype: 'textfield',
                                        id: 'txtJamKunjung',
                                        value: h+':'+m+':'+s,
					width: 120,
                                        anchor: '95%',
					listeners:
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13)
							{
								 
							}
						}
					}
                                    }
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                    mComboPoliklinik(),
                                    mComboDokterRequestEntry()
				]
			},
                        {
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                  {  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien',
                                    id: 'kelPasien',
                                     editable: false,
                                    //value: 'Perseorangan',
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
						'Id',
						'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
                                  displayField: 'displayText',
                                  mode: 'local',
                                  width: 100,
                                  forceSelection: true,
                                  triggerAction: 'all',
                                  emptyText: 'Pilih Salah Satu...',
                                  selectOnFocus: true,
								  tabIndex:22,
                                  anchor: '95%',
                                  listeners:
                                     {
                                            'select': function(a, b, c) // show combo
                                        {
                                           Combo_Select(b.data.displayText);
										   if(b.data.displayText == 'Perseorangan')
										   {
											   Ext.getCmp('cboRujukanDariRequestEntry').hide();
											   Ext.getCmp('cboRujukanRequestEntry').hide();
											   jeniscus='0';
										   }
										   else if(b.data.displayText == 'Perusahaan')
										   {
											   Ext.getCmp('cboRujukanDariRequestEntry').show();
											   Ext.getCmp('cboRujukanRequestEntry').show();
											   jeniscus='1';
										   }
										   else if(b.data.displayText == 'Asuransi')
										   {
											  Ext.getCmp('cboRujukanDariRequestEntry').show();
											   Ext.getCmp('cboRujukanRequestEntry').show();  
											    jeniscus='2';
										   }
										   RefreshDatacombo(jeniscus);
										   
										   //alert(b.data.displayText);
                                        },
										  'render': function(c)
											{
												c.getEl().on('keypress', function(e) {
												if(e.getKey() == 13) //atau Ext.EventObject.ENTER
												Ext.getCmp('cboPerseorangan').focus();
												}, c);
											}

                                    }
                                  }
				]
			},
                        
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPerseorangan()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboPerusahaan()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    mComboAsuransi()
				]
			},
                        {
                            columnWidth: .20,
                            layout: 'form',
                            border: false,
                            labelWidth:90,
                            items:
                                [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Peserta',
                                        maxLength: 200,
                                        name: 'txtNamaPeserta',
                                        id: 'txtNamaPeserta',
                                        width: 100,
                                        anchor: '95%'
                                     }
                                ]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
			    items:
				[
                                     {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     }
				]
			}
                        
                        
                ]

        };
    return items;
};
//-------------------------end--------------------------------------------------
function RefreshDatacombo(jeniscus)  // show combo
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
	
   // rowSelectedKasirRWJ = undefined;
    return ds_customer_viDaftar;
};
//form data pasien tab
function getPenelItemDataKunjungan_viDaftar(lebar)
{
    var items =
	{
	   xtype:'tabpanel',
           plain:true,
           activeTab: 0,
           height:300,
           //deferredRender: false,
           defaults:
           {
            bodyStyle:'padding:10px',
            autoScroll: true
           },
           items:[
                    DataPanel1(),
                    DataPanel2(),
                    DataPanel3()
                            
                 ]
        }
    return items;
};
//------------------end---------------------------------------------------------

function subdatapanel1()
{
    var items=
    {
           xtype: 'fieldset',
            title: 'Rujukan',
            //Height: 50,
            items: [
                      {
                columnWidth: .80,
                layout: 'form',
                labelWidth:90,
                border: false,
                items:
                    [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Anamnese ',
                                name: 'txtAnamnese',
                                id: 'txtAnamnese',
                                emptyText:' ',
								tabIndex:24,
                                anchor: '95%'
								
                            }
                    ]
            },
            {
                columnWidth: .80,
                layout: 'form',
                border: false,
                labelWidth:90,
                items:
                    [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alergi ',
                                name: 'txtAlergi',
                                id: 'txtAlergi',
                                emptyText:' ',
								tabIndex:25,
                                anchor: '95%'
                            }
                    ]
            }  

            ]   
    };
    return items;
}

//sub form data pasien tab1
function DataPanel1()
{
        var items =
        {
            title:'Kunjungan',
            layout:'form',
			tabIndex:19,
            items:
                [
                    getItemDataKunjungan_viDaftar1(),
                    subdatapanel1()
                ]
        };
        return items;
}
//------------------end---------------------------------------------------------

//sub form data pasein tab2
function DataPanel2()
{
    var items =
        {
            title:'Penerimaan',
            layout:'form',
            items:
                [
                    {
                        xtype: 'fieldset',
                        title: 'Rujukan',
						
                        //Height: 50,
                        items: [{
                                    xtype: 'radiogroup',
                                    id: 'rbrujukan',
									tabIndex:26,
                                    //fieldLabel: 'Auto Layout',
                                    items: [
                                    {boxLabel: 'Datang Sendiri', name: 'rb-auto', inputValue: 1},
                                    {boxLabel: 'Rujukan', name: 'rb-auto', inputValue: 2}
                                            ]
                                }
                        ]
                    },
//                       mComboRujukanDari(),
                        mcomborujukandari(),
                        mComboRujukan(),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama ',
						tabIndex:29,
                        name: 'txtNamaPerujuk',
                        id: 'txtNamaPerujuk',
                        width: 250
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat ',
						tabIndex:30,
                        name: 'txtAlamatPerujuk',
                        id: 'txtAlamatPerujuk',
                        width: 500
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kota ',
						tabIndex:31,
                        name: 'txtKotaPerujuk',
                        id: 'txtKotaPerujuk'
                    }
                ]
                                
                               
                                 
        };
        return items;
};
//------------------end---------------------------------------------------------

//sub foem data pasien tab3
function DataPanel3()
{
    var items =
    {
	/*

  kd_kelurahan integer,
  kd_kelurahan_ktp integer,

 
  kd_pos_ktp character varying(10),
  email character varying(25),*/
       
                                 title:'Penanggung Jawab',
                                 layout:'column',
                                 items:
                                     [
									 {
											columnWidth: .50,
											layout: 'form',
											labelWidth:100,
											border: false,
											items:
											[
													{
														xtype: 'textfield',
														fieldLabel: 'Nama ',
														name: 'txtNamaPenanggungjawab',
														id: 'txtNamaPenanggungjawab',
														width: 200
													},
													mComboPropinsiPenanggungJawab()
													,
													mComboKabupatenpenanggungjawab()
													,
													mComboKecamatanPenanggungJawab()
													,
													mCombokelurahanPenanggungJawab(),
													{
														xtype: 'textfield',
														fieldLabel: 'Alamat ',
														name: 'txtAlamatPenanggungjawab',
														id: 'txtAlamatPenanggungjawab',
														width: 300
													},
													
													{

														xtype: 'textfield',
														fieldLabel: 'Kode Pos ',
														name: 'txtKdPosPerusahaanPenanggungjawab',
														id: 'txtKdPosPerusahaanPenanggungjawab',
														width: 50
													},
														
													{

														xtype: 'textfield',
														fieldLabel: 'Tlp ',
														name: 'txtTlpPenanggungjawab',
														id: 'txtTlpPenanggungjawab',
																			width: 100
													},
			//                                       
													mComboPekerjaanPenanggungJawabRequestEntry(),
													mComboPerusahaanPenanggungJawab()
													
	//                                      
											]
										},
										 {
											columnWidth: .50,
											layout: 'form',
											labelWidth:100,
											border: false,
											items:
											[
													{
														xtype: 'textfield',
														fieldLabel: 'No KTP ',
														name: 'txtNoKtpPenanggungjawab',
														id: 'txtNoKtpPenanggungjawab',
														width: 200
													},
													mComboPropinsiKTPPenanggungJawab(),
													mComboKabupatenKtppenanggungjawab(),
													mComboKecamatanKtpPenanggungJawab(),
													mCombokelurahanKtpPenanggungJawab(),
													{
														xtype: 'textfield',
														fieldLabel: 'Alamat KTP',
														name: 'txtalamatktpPenanggungjawab',
														id: 'txtalamatktpPenanggungjawab',
														width: 300
													}
													,{

														xtype: 'textfield',
														fieldLabel: 'Kode Pos KTP ',
														name: 'txtKdPosKtpPerusahaanPenanggungjawab',
														id: 'txtKdPosKtpPerusahaanPenanggungjawab',
														width: 50
													},
													{

														xtype: 'textfield',
														fieldLabel: 'No Handphone ',
														name: 'txtHpPenanggungjawab',
														id: 'txtHpPenanggungjawab',
														width: 100
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Alamat Prsh ',
														name: 'txtAlamatPrshPenanggungjawab',
														id: 'txtAlamatPrshPenanggungjawab',
														width: 300
													},
													mComboHubunganKeluarga()
			//                                      
											]
										}
									 ]
            
    };
    return items;
}
//------------------end---------------------------------------------------------

function datasave_viDaftar(mBol)
{	
    if (ValidasiEntry_viDaftar('Simpan Data',false) == 1 )
    {
        //alert('a')
        if (addNew_viDaftar == true)
        {
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparam_viDaftar(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							                                   

							//Ext.getCmp('btnDelete_viDaftar').enable();
							ShowPesanInfo_viDaftar('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftar(tmpcriteriaRWJ);
							addNew_viDaftar = false;
							Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
							//printbill();
                 
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
        }
        else
        {
            Ext.Ajax.request
            (
            {
                url: WebAppUrl.UrlUpdateData,
                params: dataparam_viDaftar(),
                success: function(o)
                {
                    //alert(o);
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viDaftar('Data berhasil disimpan','Edit Data');
                        datarefresh_viDaftar(tmpcriteriaRWJ);
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        ShowPesanWarning_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                    else
                    {
                        ShowPesanError_viDaftar('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                }
            }
            )
        }
    }
    else
    {
        if(mBol === true)
        {
            return false;
        }
    }
}

function datadelete_viDaftar()
{
    // var DataHapus = Ext.get('txtNPP_viDaftar').getValue();
    if (ValidasiEntry_viDaftar('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
        {
            title:'Hapus Data',
             msg: "Akan menghapus data?" ,
            buttons: Ext.MessageBox.YESNO,
            width:300,
            fn: function (btn)
            {
                if (btn =='yes')
                {
                    Ext.Ajax.request
                    (
                    {
                        url: WebAppUrl.UrlDeleteData,
                        params: dataparamDelete_viDaftar(), //dataparam_viDaftar(),
                        success: function(o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');
                                datarefresh_viDaftar(tmpcriteriaRWJ);
                                dataaddnew_viDaftar();
								Ext.getCmp('btnSimpan_viDaftar').disable();
								Ext.getCmp('btnSimpanExit_viDaftar').disable();	
								Ext.getCmp('btnDelete_viDaftar').disable();	
								
                            }
                            else if (cst.success === false && cst.pesan===0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil dihapus ' ,'Hapus Data');
                            }
                            else if (cst.success === false && cst.pesan===1)
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil dihapus '  + cst.msg ,'Hapus Data');
                            }
                        }
                    }
                    )//end Ajax.request
                } // end if btn yes
            }// end fn
        }
        )//end msg.show
    }
}
//-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viDaftar(modul,mBolHapus)
{
    var x = 1;
	
	if (Ext.get('txtNama').getValue() === " ")
	{
		ShowPesanWarning_viDaftar("Nama belum terisi...",modul);
		x=0;
	}
	if (Ext.get('txtNamaKeluarga').getValue() === " ")
	{
		ShowPesanWarning_viDaftar("Nama keluarga belum terisi...",modul);
		x=0;
	}
        if (Ext.getCmp('cboJK').getValue() === "")
	{
		ShowPesanWarning_viDaftar("Jenis Kelamin belum dipilih...",modul);
		x=0;
	}
	/*if (Ext.get('txtTempatLahir').getValue() === " ")
	{
		ShowPesanWarning_viDaftar("Tempat Lahir belum terisi...",modul);
		x=0;
	}*/
//        if (Ext.get('dtpTanggalLahir').getValue() > now_viDaftar.format('d/M/Y'))
//	{
//		ShowPesanWarning_viDaftar("Tanggal Lahir lebih besar dari tanggal sekarang...",modul);
//		x=0;
//	}
                            
                    
//                   Ext.getCmp('cboAgamaRequestEntry').getValue(),
//                    Ext.getCmp('cboGolDarah').getValue(),
//                   Ext.getCmp('cboStatusMarital').getValue(),
//                   StatusWargaNegara,
//                    Ext.get('txtAlamat').getValue(),
//                    Ext.getCmp('cboPropinsiRequestEntry').getValue(),
//                    Ext.getCmp('cboPendidikanRequestEntry').getValue(),
//                    Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
//                    Ext.get('txtNamaPeserta').getValue(),
//                    Ext.get('txtNoAskes').getValue(),
//                    Ext.get('txtNoSJP').getValue(),

    return x;
}

function ShowPesanWarning_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viDaftar(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}

function datarefresh_viDaftar(criteria)
{
	
    //criteria = getCriteriaFilter_viDaftar()

    dataSource_viDaftar.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viDaftar,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViewKunjungan',
				param: criteria
			}
		}
    );
    return dataSource_viDaftar;
}

function getCriteriaFilter_viDaftar()
{
      	 var strKriteria = "";

           if (Ext.get('txtNoMedrec').dom.value != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec').dom.value +"'" + "and " + tmpcriteriaRWJ;
            }
            if (Ext.get('txtTglLahir').dom.value != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir').dom.value +"'" + "and " + tmpcriteriaRWJ;
                    }
                    else
                        {
                            strKriteria += " and pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir').dom.value +"'" + "and " + tmpcriteriaRWJ;
                        }
                
            }
            if (Ext.get('txtAlamatPasien').dom.value != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien').dom.value +"%')" + "and " + tmpcriteriaRWJ;
                    }
                    else
                        {
                            strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien').dom.value +"%')" + "and " + tmpcriteriaRWJ;
                        }
                
            }
            if (Ext.get('txtNamaPasien').dom.value != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien').dom.value +"%')" + "and " + tmpcriteriaRWJ;
                    }
                    else
                        {
                            strKriteria += " and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien').dom.value +"%')" + "and " + tmpcriteriaRWJ;
                        }
                
            }
	
	 return strKriteria;
}

function datainit_viDaftar(rowdata)
{
        addNew_viDaftar = false;
        addNew_viDaftar = true;
        dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest').setValue(rowdata.KD_PASIEN);
	Ext.getCmp('txtNama').setValue(rowdata.NAMA);
	Ext.getCmp('txtNamaKeluarga').setValue(rowdata.NAMA_KELUARGA);
	Ext.getCmp('txtTempatLahir').setValue(rowdata.TEMPAT_LAHIR);
	Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
        Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
	Ext.getCmp('cboWarga').setValue(rowdata.WNI);
	Ext.getCmp('txtAlamat').setValue(rowdata.ALAMAT);
	Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
        Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
        Ext.get('dtpTanggalLahir').dom.value = ShowDate(rowdata.TGL_LAHIR);
        Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
        var tmpjk = "";
        var tmpwni = "";
        if (rowdata.JENIS_KELAMIN === "t")
            {
                tmpjk = "Laki - Laki";
            }
            else
                {
                    tmpjk = "Perempuan";
                }
        Ext.getCmp('cboJK').setValue(tmpjk);
        if (rowdata.WNI === "t")
            {
                tmpwni = "WNI";
            }
            else
                {
                    tmpwni = "WNA";
                }
        Ext.getCmp('cboWarga').setValue(tmpwni);
        //getdatatempat_viDaftar(rowdata.KD_PASIEN);
       
        Ext.getCmp('cboPropinsiRequestEntry').setValue(rowdata.PROPINSI);
        Ext.getCmp('cboKabupatenRequestEntry').setValue(rowdata.KABUPATEN);
        Ext.getCmp('cboKecamatanRequestEntry').setValue(rowdata.KECAMATAN);
        Ext.getCmp('txtTmpPropinsi').setValue(rowdata.KD_PROPINSI);
        Ext.getCmp('txtTmpKabupaten').setValue(rowdata.KD_KABUPATEN);
        Ext.getCmp('txtTmpKecamatan').setValue(rowdata.KD_KECAMATAN);
        Ext.getCmp('txtTmpPendidikan').setValue(rowdata.KD_PENDIDIKAN);
        Ext.getCmp('txtTmpPekerjaan').setValue(rowdata.KD_PEKERJAAN);
        Ext.getCmp('txtTmpAgama').setValue(rowdata.KD_AGAMA);
       	
				setUsia(ShowDate(rowdata.TGL_LAHIR));
	//Ext.getCmp('btnSimpan_viDaftar').disable();
	//Ext.getCmp('btnSimpanExit_viDaftar').disable();	
	//Ext.getCmp('btnDelete_viDaftar').enable();
        Ext.getCmp('btnUlang_viDaftar').enable();
	mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	
}

function dataaddnew_viDaftar()
{
	var nama = Ext.get('txtNamaPasien').getValue();
	var alamat = Ext.get('txtAlamatPasien').getValue();
	var tgl_lahir = Ext.get('txtTglLahir').getValue();
	Ext.getCmp('txtNama').focus('false', 10); 
    addNew_viDaftar = true;
    dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest').setValue('');
	Ext.getCmp('txtNama').setValue(nama);
	Ext.getCmp('txtNamaKeluarga').setValue('');
	Ext.getCmp('txtTempatLahir').setValue('');
	Ext.getCmp('txtThnLahir').setValue('');
	Ext.getCmp('txtBlnLahir').setValue('');
	Ext.getCmp('txtHariLahir').setValue('');
	Ext.getCmp('cboWarga').setValue('');
	Ext.getCmp('txtAlamat').setValue(alamat);
	Ext.getCmp('txtJamKunjung').setValue(h+':'+m+':'+s);
        Ext.getCmp('txtNamaPeserta').setValue('');
        Ext.getCmp('txtNoAskes').setValue('');
        Ext.getCmp('txtNoSJP').setValue('');
        Ext.getCmp('txtAnamnese').setValue('');
        Ext.getCmp('txtAlergi').setValue('');
        Ext.getCmp('txtNamaPerujuk').setValue('');
        Ext.getCmp('txtAlamatPerujuk').setValue('');
        Ext.getCmp('txtAlamatPenanggungjawab').setValue('');
        Ext.getCmp('txtKotaPenanggungjawab').setValue('');
        Ext.getCmp('txtKdPosPerusahaanPenanggungjawab').setValue('');
        Ext.getCmp('txtTlpPenanggungjawab').setValue('');
        Ext.getCmp('txtPekerjaanPenanggungjawab').setValue('');
        Ext.getCmp('txtNamaPerusahaanPenanggungjawab').setValue('');
        Ext.getCmp('txtAlamatPrshPenanggungjawab').setValue('');
        Ext.getCmp('txtHubKelPenanggungjawab').setValue('');
        Ext.getCmp('cboJK').setValue('');
	Ext.getCmp('cboAgamaRequestEntry').setValue('');
        Ext.getCmp('cboGolDarah').setValue('');
        Ext.getCmp('cboPropinsiRequestEntry').setValue('');
        Ext.getCmp('cboKabupatenRequestEntry').setValue('');
        Ext.getCmp('cboKecamatanRequestEntry').setValue('');
        Ext.getCmp('cboPendidikanRequestEntry').setValue('');
        Ext.getCmp('cboSukuRequestEntry').setValue('');
        Ext.getCmp('cboStatusMarital').setValue('');
//        Ext.getCmp('cboRujukanDari').setValue('');
        Ext.getCmp('cboDokterRequestEntry').setValue('');
        Ext.getCmp('cboPerseorangan').setValue('');
        Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
        Ext.getCmp('cboAsuransi').setValue('');
        Ext.getCmp('cboPoliklinikRequestEntry').setValue('');
        Ext.getCmp('dtpTanggalLahir').setValue(now_viDaftar);
        Ext.getCmp('cboPekerjaanRequestEntry').setValue
        Ext.getCmp('txtTmpPropinsi').setValue('');
        Ext.getCmp('txtTmpKabupaten').setValue('');
        Ext.getCmp('txtTmpKecamatan').setValue('');
        Ext.getCmp('txtTmpPendidikan').setValue('');
        Ext.getCmp('txtTmpPekerjaan').setValue('');
        Ext.getCmp('txtTmpAgama').setValue('');
	Ext.getCmp('btnSimpan_viDaftar').enable();
	Ext.getCmp('btnSimpanExit_viDaftar').enable();
//	Ext.getCmp('btnDelete_viDaftar').disable();
//        Ext.getCmp('btnUlang_viDaftar').disable();

//	mNoKunjungan_viKasir = '';
	
    rowSelected_viDaftar   = undefined;
}
///---------------------------------------------------------------------------------------///

function dataparam_viDaftar()
{
    var jenis_kelamin;
	var Kd_customer
    if (Ext.getCmp('cboJK').getValue() === 1)
    {
        jenis_kelamin = true;
    }
    else
        {
            jenis_kelamin = false;
        }
    var tmpwarga;    
    if (Ext.getCmp('cboWarga').getValue() === 'WNI')
    {
        tmpwarga = 'true';
    }
    else
        {
            tmpwarga = 'false';
        }
	if(Ext.getCmp('cboAsuransi').getValue() == '')
					{
					Kd_Customer= '0000000001';
					}
					else
					{
                    Kd_Customer= Ext.getCmp('cboAsuransi').getValue();
					}	

		var params_ViPendaftaran =
		{
      
                    Table: 'ViewKunjungan',
                    NoMedrec:  Ext.get('txtNoRequest').getValue(),
                    NamaPasien: Ext.get('txtNama').getValue(),
                    NamaKeluarga: Ext.get('txtNamaKeluarga').getValue(),
                    JenisKelamin: jenis_kelamin,
                    Tempatlahir: Ext.get('txtTempatLahir').getValue(),
                    TglLahir : Ext.get('dtpTanggalLahir').getValue(),
                    Agama: Ext.getCmp('cboAgamaRequestEntry').getValue(),
                    GolDarah: Ext.getCmp('cboGolDarah').getValue(),
                    StatusMarita: Ext.getCmp('cboStatusMarital').getValue(),
                    StatusWarga: tmpwarga,
                    Alamat : Ext.get('txtAlamat').getValue(),
                    No_Tlp: '',
                    
                    Pendidikan: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
                    Pekerjaan: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
                    NamaPeserta : Ext.get('txtNamaPeserta').getValue(),
                    KD_Asuransi : 1,
                    NoAskes : Ext.get('txtNoAskes').getValue(),
                    NoSjp : Ext.get('txtNoSJP').getValue(),
                    Kd_Suku: 0,
                    Jabatan : '',
                    Perusahaan: Ext.getCmp('cboPerusahaanRequestEntry').getValue(),
                    Perusahaan1 : 0,
                    Cek: Ext.get('kelPasien').getValue(),

                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    TanggalMasuk : Ext.get('dptTanggal').getValue(),
                    UrutMasuk:0,
                    JamKunjungan : now_viDaftar, //Ext.get('txtJamKunjung').getValue(),
                    CaraPenerimaan: 99,
                    KdRujukan: 0,
                    KdCustomer:Kd_Customer,
                    KdDokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
                    Baru: true,
					
					Shift: 1,
                    Karyawan: 0,
                    Kontrol: false,
                    Antrian:0,
                    NoSurat:'',
                    Alergi: Ext.get('txtAlergi').getValue(),
                    Anamnese: Ext.get('txtAnamnese').getValue(),

                    TahunLahir : Ext.get('txtThnLahir').getValue(),
                    BulanLahir : Ext.get('txtBlnLahir').getValue(),
                    HariLahir : Ext.get('txtHariLahir').getValue(),
                    //NamaPerujuk : Ext.get('txtNamaPerujuk').getValue(),
                    //AlamatPerujuk : Ext.get('txtAlamatPerujuk').getValue(),

                    Kota: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    Kd_Kecamatan: Ext.getCmp('cboKecamatanRequestEntry').getValue(),
                    Kelurahan: Ext.getCmp('cboPropinsiRequestEntry').getValue(),
                    AsalPasien: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    KDPROPINSI : Ext.getCmp('txtTmpPropinsi').getValue(),
                    KDKABUPATEN: Ext.getCmp('txtTmpKabupaten').getValue(),
                    KDKECAMATAN: Ext.getCmp('txtTmpKecamatan').getValue(),
                    
                    KDPENDIDIKAN : Ext.getCmp('txtTmpPendidikan').getValue(),
                    KDPEKERJAAN: Ext.getCmp('txtTmpPekerjaan').getValue(),
                    KDAGAMA: Ext.getCmp('txtTmpAgama').getValue() ,
				/*	,kd_perusahaan,
					kd_kelurahan,
					kd_pekerjaan,
					nama_pj ,
					alamat ,
					telepon ,
					kd_pos,
					jabatan,
					hubungan,
					alamat_ktp,
					no_hp,
					kd_pos_ktp,
					no_ktp*/
		};
    return params_ViPendaftaran
}

function dataparamDelete_viDaftar()
{
		var paramsDelete_ViPendaftaran =
		{
			Table: 'viKunjungan',			
			// KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftar_viDaftar').getValue(),
			// KD_UNIT: Ext.getCmp('ComboPoliDaftar_viDaftar').getValue(),
			// KD_DOKTER: Ext.getCmp('ComboDokterDaftar_viDaftar').getValue(),
			NO_KUNJUNGAN : Ext.get('txtNoKunjungan_viDaftar').getValue(), //Ext.getCmp('txtNoKunjungan_viDaftar').setValue(rowdata.NO_KUNJUNGAN);		
			KD_CUSTOMER: strKD_CUST
			// KD_PASIEN: Ext.get('txtKDPasien_viDaftar').getValue(),
			// TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftar').getValue()),
			// TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftar').getValue(),
			// BERAT_BADAN : Ext.get('txtBeratBadan_viDaftar').getValue(),
			// TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftar').getValue(),
			// NADI : Ext.get('txtNadi_viDaftar').getValue(),
			// ALERGI :  Ext.get('txtchkAlergi_viDaftar').dom.checked,
			// KELUHAN : Ext.get('txtkeluhan_viDaftar').getValue(),  
			// PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftar').dom.checked,						
			// KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftar').getValue(), //PASIEN
			// KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftar').getValue(),
			// KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftar').getValue(),
			// KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftar').getValue(),
			// NAMA : Ext.get('txtNamaPs_viDaftar').getValue(),
			// TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftar').getValue(),
			// TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftar').getValue()),
			// JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftar').getValue(),
			// ALAMAT : Ext.get('txtAlamat_viDaftar').getValue(),
			// NO_TELP : Ext.get('txtNoTlp_viDaftar').getValue(),
			// NO_HP : Ext.get('txtHP_viDaftar').getValue(),
			// GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftar').getValue(),
		
		}
	
    return paramsDelete_ViPendaftaran
}
//============================================ Grid Data ======================================

//-------------------------------------------- Hapus baris -------------------------------------
function HapusBarisNgajar(nBaris)
{
	if (CurrentData_viDaftar.row >= 0
	/*SELECTDATASTUDILANJUT.data.NO_SURAT_STLNJ != '' ||  SELECTDATASTUDILANJUT.data.TGL_MULAI_SURAT != '' ||
		SELECTDATASTUDILANJUT.data.TGL_AKHIR_SURAT != '' || SELECTDATASTUDILANJUT.data.ID_DANA != '' ||
		SELECTDATASTUDILANJUT.data.SURAT_DARI != '' || SELECTDATASTUDILANJUT.data.TGL_SURAT != '' || SELECTDATASTUDILANJUT.data.THN_PERKIRAAN_LULUS != ''*/) 
		{
			Ext.Msg.show
			(
				{
					title: 'Hapus Baris',
					msg: 'Apakah baris ini akan dihapus ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) 
					{
						if (btn == 'yes') 
						{
							DataDeletebaris_viDaftar()
							dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
							SELECTDATASTUDILANJUT = undefined;
						}
					},
					icon: Ext.MessageBox.QUESTION
				}
			);
		}
	else 
		{
			dsDetailSL_viDaftar.removeAt(CurrentData_viDaftar.row);
			SELECTDATASTUDILANJUT = undefined;
		}
}

function DataDeletebaris_viDaftar() 
{
    Ext.Ajax.request
	({url: "./Datapool.mvc/DeleteDataObj",
		params: dataparam_viDaftar(),
		success: function(o) 
		{
			var cst = o.responseText;
			if (cst == '{"success":true}') 
			{
				ShowPesanInfo_viDaftar('Data berhasil dihapus','Hapus Data');                
			}
			else
			{ 
				ShowPesanInfo_viDaftar('Data gagal dihapus','Hapus Data'); 
			}
		}
	})       
};

var mRecord = Ext.data.Record.create
(
	[
		'NIP', 
		'ID_STUDI_LANJUT', 
		'NO_SURAT_STLNJ', 
		'TGL_MULAI_SURAT', 
		'TGL_AKHIR_SURAT', 
		'ID_DANA', 
		'SURAT_DARI', 
		'TGL_SURAT', 
		'THN_PERKIRAAN_LULUS'  
	]
);
//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------
function getArrDetail_viDaftar() 
{
    var x = '';
    for (var i = 0; i < dsDetailSL_viDaftar.getCount(); i++) 
	{
        var y = '';
        var z = '@@##$$@@';
        
        y += 'NIP=' + Ext.get('txtNPP_viDaftar').getValue()
        y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftar').getValue()
        y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftar.data.items[i].data.NO_SURAT_STLNJ
		
		/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT.length == 8) 
		{	
			
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		}
		else
		{
		
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		}
		
		if (dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT.length == 8) 
		{		
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
		}
		else
		{
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
		}*/
		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_MULAI_SURAT)
		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_AKHIR_SURAT)
        y += z + 'ID_DANA=' + dsDetailSL_viDaftar.data.items[i].data.ID_DANA
        y += z + 'SURAT_DARI=' + dsDetailSL_viDaftar.data.items[i].data.SURAT_DARI
        //alert(dsDetailSL_viDaftar.data.items[i].data.ID_DANA)
		
		/*if (dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT.length == 8) 
		{		
			y += z + 'TGL_SURAT=' + dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT
		}
		else
		{
			y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		}*/
		
		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftar.data.items[i].data.TGL_SURAT)
		
		y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftar.data.items[i].data.THN_PERKIRAAN_LULUS
		
        if (i === (dsDetailSL_viDaftar.getCount() - 1)) 
		{
            x += y
        }
        else {
            x += y + '##[[]]##'
        }
    }
    return x;
};
//---------------------------- end Split row ------------------------------
function DatarefreshDetailSL_viDaftar(rowdataaparam)
{
    dsDetailSL_viDaftar.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 50,
				Sort: '',
				Sortdir: 'ASC',
				target:'viviewDetailStudiLanjut',
				param: rowdataaparam
			}
		}
    );
    return dsDetailSL_viDaftar;
}

function GetPasienDaftar(strCari)
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'getKunjunganDataPasien',
				Params:	getParamKunjunganKdPasien(strCari)
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{					
//					Ext.getCmp('txtKDPasien_viDaftar').setValue(cst.KD_PASIEN);
//					Ext.getCmp('txtNamaPs_viDaftar').setValue(cst.NAMA);
//					Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 0;
//					Ext.getCmp('txtTmpLahir_viDaftar').setValue(cst.TEMPAT_LAHIR);
//					Ext.getCmp('DtpTglLahir_viDaftar').setValue(ShowDate(cst.TGL_LAHIR));
//					Ext.getCmp('txtUmurThn_viDaftar').setValue(cst.TAHUN);
//					Ext.getCmp('txtUmurBln_viDaftar').setValue(cst.BULAN);
//					Ext.getCmp('txtUmurHari_viDaftar').setValue(cst.HARI);
//					Ext.getCmp('ComboJK_viDaftar').setValue(cst.JENIS_KELAMIN);
//					Ext.getCmp('ComboGolDRH_viDaftar').setValue(cst.GOL_DARAH);
//					Ext.getCmp('txtNoTlp_viDaftar').setValue(cst.NO_TELP);
//					Ext.getCmp('txtHP_viDaftar').setValue(cst.NO_HP);
//					Ext.getCmp('txtAlamat_viDaftar').setValue(cst.ALAMAT);
//					Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(cst.KD_UNIT);
//					Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(cst.KD_DOKTER);
//					Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(cst.KD_KELOMPOK);
//					// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(cst.TGL_KUNJUNGAN));
//					Ext.getCmp('txtNadi_viDaftar').setValue(cst.NADI);
//					Ext.getCmp('txtchkAlergi_viDaftar').setValue(cst.ALERGI);
//					Ext.getCmp('txtTinggiBadan_viDaftar').setValue(cst.TINGGI_BADAN);
//					Ext.getCmp('txtBeratBadan_viDaftar').setValue(cst.BERAT_BADAN);
//					Ext.getCmp('txtTekananDarah_viDaftar').setValue(cst.TEKANAN_DARAH);
//					Ext.getCmp('txtkeluhan_viDaftar').setValue(cst.KELUHAN);
//					Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(cst.KD_STS_MARITAL);
//					Ext.getCmp('ComboPekerjaan_viDaftar').setValue(cst.KD_PEKERJAAN);
//					Ext.getCmp('ComboAgama_viDaftar').setValue(cst.KD_AGAMA);
//					Ext.getCmp('ComboPendidikan_viDaftar').setValue(cst.KD_PENDIDIKAN);
					
					// Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowdata.KD_UNIT);
					// Ext.get('ComboJK_viDaftar').dom.value = rowdata.JNS_KELAMIN;	
					// Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowdata.NAMA_UNIT;
					// Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowdata.DOKTER;
					// Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowdata.KELOMPOK;
					// Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowdata.STS_MARITAL;
					// Ext.get('ComboPekerjaan_viDaftar').dom.value = rowdata.PEKERJAAN;
					// Ext.get('ComboAgama_viDaftar').dom.value = rowdata.AGAMA;
					// Ext.get('ComboPendidikan_viDaftar').dom.value = rowdata.PENDIDIKAN;
				}
				else
				{
					ShowPesanWarning_viDaftar('No.Medrec tidak di temukan '  + cst.pesan,'Informasi');
					// Ext.get('txtNamaKRSMahasiswa').dom.value = '';								
					// Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBarcode').dom.focus();

					// var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
						// if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
						// {
							// criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
						// };
						// FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNamaKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
				};
            }

        }
	);
}

function getParamKunjunganKdPasien(kdCari)
{					
	var strKriteria = "";	
	var x ="x";
		
	
	if (kdCari == 1)
	{	
		if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtKDPasien_viDaftar').getValue() + "##";							
		}	
	}
	else if(kdCari == 2) 
	{
		if(Ext.get('txtNoTlp_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtNoTlp_viDaftar').getValue() + "##";							
		}	
	}else
	{
		if(Ext.get('txtHP_viDaftar').getValue() != '')
		{
			strKriteria += Ext.get('txtHP_viDaftar').getValue() + "##";							
		}	
	};
	strKriteria += strKD_CUST + "##"
	strKriteria += kdCari + "##"
	
	return strKriteria;
}

function getKriteriaCariLookup()
{					
	var strKriteria = "";	
		
	//strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
	
	if(Ext.get('txtKDPasien_viDaftar').getValue() != '')
	{
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%'"
	}	
	
	if(Ext.get('txtNamaPs_viDaftar').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNamaPs_viDaftar').dom.value + "%'"
		}
	}
	
	// if(Ext.get('txtNoTlp_viDaftar').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftar').dom.value + "%'"
		// }
	// }
	
	// if(Ext.get('txtHP_viDaftar').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_HP like '%" + Ext.get('txtHP_viDaftar').dom.value + "%'"
		// }
	// }
	
	if(Ext.get('txtAlamat_viDaftar').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftar').dom.value + "%'"
		}
	}

	if (strKriteria != "")	
	{
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	else
	{
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}		
	
	return strKriteria;
}

function dataprint_viDaftar()
{
    var params_ViPendaftaran =
    {

        Table: 'ViewPrintBill',

        No_TRans: tmpnotransaksi,
        KdKasir : tmpkdkasir
//        NoMedrec:  Ext.get('txtNoRequest').getValue(),
//        NamaPasien: Ext.get('txtNama').getValue(),
//        TglLahir : Ext.get('dtpTanggalLahir').getValue(),
//        Alamat : Ext.get('txtAlamat').getValue(),
//
//        Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
//        TanggalMasuk : Ext.get('dptTanggal').getValue(),
//        KdDokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//        KdCustomer: Ext.getCmp('cboAsuransi').getValue()

    };
    return params_ViPendaftaran
}

function GetPrintKartu()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/LaporanDataObj",
            params: dataprint_viDaftar(),
            success: function(o) 
                        {

                var cst = Ext.decode(o.responseText);

                                if (cst.success === true) 
                                {
                                }
                                else
                                {				

                                };
                }

        }
    );
}

function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
	(
		{
			id:'cboJK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Jenis Kelamin...',
			fieldLabel: 'Jenis Kelamin ',
			width:145,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK=b.data.displayText ;
//                                        getdatajeniskelamin(b.data.displayText)
                                        //alert(jenis_kelamin)
				},
                                'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('txtTempatLahir').focus();
                                                    }, c);
                                                }
			}
		}
	);
	return cboJK;
};

function mComboWargaNegara()
{
    var cboWarga = new Ext.form.ComboBox
	(
		{
			id:'cboWarga',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
                        selectOnFocus:true,
                        forceSelection: true,
						tabIndex:11,
			fieldLabel: 'Warga Negara ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'WNI'], [2, 'WNA']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetWarga,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetWarga=b.data.displayText ;
                                        GetDataWargaNegara(b.data.displayText)
                                        //alert(StatusWargaNegara)
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboStatusMarital').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboWarga;
};

function mComboHubunganKeluarga()
{
    var cboHubunganKeluarga = new Ext.form.ComboBox
	(
		{
			id:'cboHubunganKeluarga',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
                        selectOnFocus:true,
                        forceSelection: true,
			fieldLabel: 'Hub.Keluarga',
			width:120,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Kep. Keluarga'], [2, 'Suami'],[3, 'Istri'], [4, 'Anak Kandung']
                                        ,[5, 'Anak Tiri'], [6, 'Anak Angkat'],[7, 'Ayah'], [8, 'Ibu'], [9, 'Mertua']
                                        ,[10, 'Sdr Kandung'], [11, 'Cucu'],[12, 'Famili'], [13, 'Pembantu']
                                        ,[14, 'Lain-Lain']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetHubunganKeluarga,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetHubunganKeluarga=b.data.displayText ;
                                        GetDataHubunganKeluarga(b.data.displayText)
                                        //alert(StatusHubunganKeluarga)
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboStatusMarital').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboHubunganKeluarga;
};


var StatusWargaNegara
function GetDataWargaNegara(warga)
{
    var tampung = warga
    if (tampung === 'WNI')
    {
        StatusWargaNegara = 'true'
    }
    else
        {
            StatusWargaNegara = 'false'
        }
}

function mComboGolDarah()
{
    var cboGolDarah = new Ext.form.ComboBox
	(
		{
			id:'cboGolDarah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:9,
                        //allowBlank: false,
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Gol. Darah...',
			fieldLabel: 'Gol. Darah ',
			width:50,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, '-'], [2, 'A+'],[3, 'B+'], [4, 'AB+'],[5, 'O+'], [6, 'A-'], [7, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGolDarah,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGolDarah=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboWarga').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboGolDarah;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			hidden:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:50,
                        anchor: '95%',
                        value:1,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboSatusMarital()
{
    var cboStatusMarital = new Ext.form.ComboBox
	(
		{
			id:'cboStatusMarital',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			tabIndex:12,
                        forceSelection: true,
                        emptyText:'Pilih Status...',
			fieldLabel: 'Status Marital ',
			width:110,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Blm Kawin'], [2, 'Kawin'],[3, 'Janda'], [4, 'Duda']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetSatusMarital,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetSatusMarital=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtAlamat').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboStatusMarital;
};

function mComboNamaRujukan()
{
    var cboNamaRujukan = new Ext.form.ComboBox
	(
		{
			id:'cboNamaRujukan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Rujukan...',
			fieldLabel: 'Nama ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
                                       data: [[1, ''], [2, ''],[3, ''],[4, '']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetNamaRujukan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetNamaRujukan=b.data.displayText ;
				}
			}
		}
	);
	return cboNamaRujukan;
};

function mComboPoli_viDaftar(lebar,Nama_ID)
{
    var Field_poli_viDaftar = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )

    var cbo_Poli_viDaftar = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viDaftar,
            width: lebar,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli...',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: 'cboPoliviDaftar',
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftar;
}

function mComboAgamaRequestEntry()
{
    var Field = ['KD_AGAMA','AGAMA'];

    dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
    dsAgamaRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_agama',
			    Sortdir: 'ASC',
			    target: 'ViewComboAgama',
			    param: ''
			}
		}
	)

    var cboAgamaRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboAgamaRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Agama...',
		    fieldLabel: 'Agama',
		    align: 'Right',
			tabIndex:9,
//		    anchor:'60%',
		    store: dsAgamaRequestEntry,
		    valueField: 'KD_AGAMA',
		    displayField: 'AGAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                    selectAgamaRequestEntry = b.data.KD_AGAMA;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboGolDarah').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboAgamaRequestEntry;
};

function mComboPropinsiRequestEntry()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    forceSelection: true,
                    emptyText:'Select a Propinsi...',
                    selectOnFocus:true,
                     //editable: false,
		    //emptyText: ' ',
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
                    anchor: '95%',
					tabIndex:14,
		    listeners:
			{
			    'select': function(a, b, c)
				{
					//console.log(a);
					//console.log(b);
					//console.log(c);
                                   //alert(b.data.KD_PROPINSI)
								  
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupaten(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                 Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupaten(dataKabupaten);
                                                }
												}, c);
                                            }

			}
                }
	);
        return cboPropinsiRequestEntry;
        
    
};


function mComboPropinsiPenanggungJawab()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    forceSelection: true,
                    emptyText:'Select a Propinsi...',
                    selectOnFocus:true,
                   
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
                    anchor: '95%',
					tabIndex:14,
		    listeners:
			{
			    'select': function(a, b, c)
				{
				
								  
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupatenpenanggungjawab(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                 Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupatenpenanggungjawab(dataKabupaten);
                                                }
												}, c);
                                            }

			}
                }
	);
        return cboPropinsiPenanggungJawab;
        
    
};



function mComboPropinsiKTPPenanggungJawab()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    forceSelection: true,
                    emptyText:'Select a Propinsi...',
                    selectOnFocus:true,
                   
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
                    anchor: '95%',
					tabIndex:14,
		    listeners:
			{
			    'select': function(a, b, c)
				{
				
								  
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupatenKtppenanggungjawab(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                 Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupatenKtppenanggungjawab(dataKabupaten);
                                                }
												}, c);
                                            }

			}
                }
	);
        return cboPropinsiKtpPenanggungJawab;
        
    
};



function loaddatastorekabupaten(kd_propinsi)
{
          dsKabupatenRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboKabupaten',
			    param: 'kd_propinsi='+ kd_propinsi
			}
                    }
                )
}

function loaddatastorekabupatenpenanggungjawab(kd_propinsi)
{
          dsKabupatenpenanggungjawab.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
								Sort: 'kd_propinsi',
								Sortdir: 'ASC',
								target: 'ViewComboKabupaten',
								param: 'kd_propinsi='+ kd_propinsi
							}
                    }
                )
}


function loaddatastorekabupatenKtppenanggungjawab(kd_propinsi)
{
          dsKabupatenKtppenanggungjawab.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
								Sort: 'kd_propinsi',
								Sortdir: 'ASC',
								target: 'ViewComboKabupaten',
								param: 'kd_propinsi='+ kd_propinsi
							}
                    }
                )
}

function mComboKabupatenRequestEntry()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

      var cboKabupatenRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
                    
		    store: dsKabupatenRequestEntry,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			tabIndex:15,
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                },
                                
								'render':function(c)
								{
								   c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                Ext.getCmp('cboKecamatanRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												var dataKecamatan = c.value;
                                                loaddatastorekecamatan(dataKecamatan);
                                                }
												}, c);
								}
                                
			}
                }
	);
    return cboKabupatenRequestEntry;
};


function mComboKabupatenpenanggungjawab()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenpenanggungjawab = new WebApp.DataStore({fields: Field});

    var cboKabupatenpenanggungjawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenpenanggungjawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
                    
		    store: dsKabupatenpenanggungjawab,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			tabIndex:15,
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastorekecamatanpenanggungjawab(b.data.KD_KABUPATEN);
                                },
                                
								'render':function(c)
								{
								   c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                Ext.getCmp('cboKecamatanRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.tab
												{
												var dataKecamatan = c.value;
                                                loaddatastorekecamatanpenanggungjawab(dataKecamatan);
                                                }
												}, 
												c);
								}
                                
			}
                }
	);
    return cboKabupatenpenanggungjawab;
};




function mComboKabupatenKtppenanggungjawab()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenKtppenanggungjawab = new WebApp.DataStore({fields: Field});

    var cboKabupatenKtppenanggungjawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenKtppenanggungjawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
                    
		    store: dsKabupatenKtppenanggungjawab,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			tabIndex:15,
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastoreKtpkecamatanpenanggungjawab(b.data.KD_KABUPATEN);
                                },
                                
								'render':function(c)
								{
								   c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                Ext.getCmp('cboKecamatanRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												var dataKecamatan = c.value;
                                                loaddatastoreKtpkecamatanpenanggungjawab(dataKecamatan);
                                                }
												}, c);
								}
                                
			}
                }
	);
    return cboKabupatenKtppenanggungjawab;
};
function loaddatastorekecamatan(kd_kabupaten)
{
    dsKecamatanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}

function loaddatastorekecamatanpenanggungjawab(kd_kabupaten)
{
    dsKecamatanPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}
function loaddatastoreKtpkecamatanpenanggungjawab(kd_kabupaten)
{
    dsKecamatanKtpPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}

function mComboKecamatanRequestEntry()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Select a Kecamatan...',
                    fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:16,
//		    anchor:'60%',
		    store: dsKecamatanRequestEntry,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
								loaddatastorekelurahanpenanggungjawab(b.data.KD_KECAMATAN)
											},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanRequestEntry;
};

function loaddatastorekelurahanpenanggungjawab(kd_kecamatan)
{
    dsKelurahanPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}

function loaddatastorekelurahanKtppenanggungjawab(kd_kecamatan)
{
    dsKelurahanKtpPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}

function mComboKecamatanPenanggungJawab()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:16,
//		    anchor:'60%',
		    store: dsKecamatanPenanggungJawab,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								loaddatastorekelurahanpenanggungjawab(b.data.KD_KECAMATAN);
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanPenanggungJawab;
};



function mCombokelurahanPenanggungJawab()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKelurahanPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cbokelurahanPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahanPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:16,
//		    anchor:'60%',
		    store: dsKelurahanPenanggungJawab,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cbokelurahanPenanggungJawab;
};


function mCombokelurahanKtpPenanggungJawab()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKelurahanKtpPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cbokelurahanKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahanKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:16,
//		    anchor:'60%',
		    store: dsKelurahanKtpPenanggungJawab,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cbokelurahanKtpPenanggungJawab;
};



function mComboKecamatanKtpPenanggungJawab()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanKtpPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:16,
//		    anchor:'60%',
		    store: dsKecamatanKtpPenanggungJawab,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
								loaddatastorekelurahanKtppenanggungjawab(b.data.KD_KECAMATAN);
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanKtpPenanggungJawab;
};



function mComboPendidikanRequestEntry()
{
    var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];

    dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pendidikan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPendidikan',
			    param: ''
			}
		}
	)

    var cboPendidikanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPendidikanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Pendidikan...',
		    fieldLabel: 'Pendidikan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPendidikanRequestEntry,
		    valueField: 'KD_PENDIDIKAN',
		    displayField: 'PENDIDIKAN',
			tabIndex:17,
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPendidikanRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPendidikanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	
   cboAsuransi = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Asuransi...',
                        fieldLabel: namacbokelpasien,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
                        displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.displayText ;
				}
			}
		}
	);
	return cboAsuransi;
};


function mComboPekerjaanRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Pekerjaan...',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
			tabIndex:18,
//		    anchor:'60%',
		    store: dsPekerjaanRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
                    anchor: '100%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboPoliklinikRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPekerjaanRequestEntry;
};

function mComboPekerjaanPenanggungJawabRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanPenanggungJawabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanPenanggungJawabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanPenanggungJawabRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanPenanggungJawabRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
                    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Pekerjaan...',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPekerjaanPenanggungJawabRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanPenanggungJawabRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPekerjaanPenanggungJawabRequestEntry;
};

function mComboSukuRequestEntry()
{
    var Field = ['KD_SUKU','SUKU'];

    dsSukuRequestEntry = new WebApp.DataStore({fields: Field});
    dsSukuRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_suku',
			    Sortdir: 'ASC',
			    target: 'ViewComboSuku',
			    param: ''
			}
		}
	)

    var cboSukuRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboSukuRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
                     editable: false,
		    fieldLabel: 'Suku ',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsSukuRequestEntry,
		    valueField: 'KD_SUKU',
		    displayField: 'SUKU',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectSukuRequestEntry = b.data.KD_PROPINSI;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13 || e.getKey() == 9 ) //atau Ext.EventObject.ENTER
                                     Ext.getCmp('setunit').collapsed = true;
                                    }, c);
                                }
			}
                }
	);

    return cboSukuRequestEntry;
};

function mComboPoliklinik()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
			tabIndex:20,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastoredokter(b.data.KD_UNIT)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboPoliklinikRequestEntry;
};

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
};

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter ',
		    align: 'Right',
                     tabIndex:21,
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftar,
			tabIndex:27,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: 'Rujukan ',
		    align: 'Right',
              tabIndex:28,       
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};

function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};

function mComboPerusahaanPenanggungJawab()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN','ALAMAT'];
    dsPerusahaanPenanggungJawabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanPenanggungJawabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanPenanggungJawabRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanPenanggungJawabRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
                    width: 120,
//		    anchor:'60%',
		    store: dsPerusahaanPenanggungJawabRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
                    anchor: '95%',
		    listeners:
			{
			   'select': function(a, b, c)
				{
                                    Ext.getCmp('txtAlamatPrshPenanggungjawab').setValue(b.data.ALAMAT);
                                }
			}
                }
	);

    return cboPerusahaanPenanggungJawabRequestEntry;
};

var rowSelectedLookPasien;
var rowSelectedLookSL;
var mWindowLookup;
var nFormPendaftaran=1;
var dsLookupPasienList;

var criteria ='';

function FormLookupPasien(criteria,nFormAsal,nName_ID,strRM,strNama,strAlamat)
{
    var vWinFormEntry = new Ext.Window
	(
		{
		    id: 'FormPasienLookup',
		    title: 'Lookup Pasien',
		    closable: true,
		    width: 600,//450,//
		    height: 400,
		    border: true,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'find',
		    modal: true,
		    items:
			[
			    // fnGetDTLGridLookUp(criteria, nFormAsal),
				fnGetDTLGridLookUpPas(criteria, nFormAsal,nName_ID),
				{
				    xtype: 'button',
				    text: 'Ok',
				    width: 70,
				    style: {'margin-left': '510px', 'margin-top': '7px'},
				    hideLabel: true,
				    id: 'btnOkpgw',
				    handler: function()
					{
						GetPasien(nFormAsal,nName_ID);
				    }
				}
			],
			tbar:
			{
				xtype: 'toolbar',
				items:
				[
					{
						xtype: 'tbtext',
						text: 'No. RM : '
					},
					{
						xtype: 'textfield',
						id: 'txtNoRMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}

					},
					{
						xtype: 'tbtext',
						text: 'Nama : '
					},
					{
						xtype: 'textfield',
						id: 'txtNamaMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
					xtype: 'tbtext',
					text: 'Alamat : '
					},
					{
						xtype: 'textfield',
						id: 'txtAlamatMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupCalonMHS',
						iconCls: 'refresh',
						handler: function()
						{
							// var criteria ='';
							criteria = getQueryCariPasien();
							RefreshDataLookupPasien(criteria);
						}
					}
				]
			}
			,
		    listeners:
				{
				    activate: function()
				    {
						Ext.get('txtNoRMLookupPasien').dom.value = strRM;
						Ext.get('txtNamaMLookupPasien').dom.value = strNama;
						Ext.get('txtAlamatMLookupPasien').dom.value = strAlamat;

					}
				}
		}
	);
    vWinFormEntry.show();
	mWindowLookup = vWinFormEntry;
};

///---------------------------------------------------------------------------------------///


function fnGetDTLGridLookUpPas(criteria,nFormAsal,nName_ID)
{
    var fldDetail =
	['KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
        'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
        'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KD_KELURAHAN','KABUPATEN','KECAMATAN','PROPINSI',
	];

	// var dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	dsLookupPasienList = new WebApp.DataStore({fields: fldDetail});

	RefreshDataLookupPasien(criteria);
	var vGridLookupPasienFormEntry = new Ext.grid.EditorGridPanel
	(
		{
			id:'vGridLookupPasienFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookupPasienList,
			height: 310, //330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			enableKeyEvents: true,
			border: false,
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedLookPasien = dsLookupPasienList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                    var nFormAsal = 1;
					GetPasien(nFormAsal,nName_ID);
					//setUsia(ShowDate(rowSelectedLookPasien.data.TGL_LAHIR));
				},

				'specialkey': function()
				{
					if (Ext.EventObject.getKey() == 13)
					{
						GetPasien(nFormAsal,nName_ID);
					}
				}
			},
			cm: fnGridLookPasienColumnModel(),
			viewConfig: {forceFit: true}
		});

	return vGridLookupPasienFormEntry;
};

function fnGridLookPasienColumnModel()
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colLookupPasien',
				header: "No. RM",
				dataIndex: 'KD_PASIEN',
				width: 200
			},
			{
				id: 'colLookupNamaPasien',
				header: "Nama",
				dataIndex: 'NAMA',
				width: 300
			},
			{
				id: 'colLookupAlamatPasien',
				header: "Alamat",
				dataIndex: 'ALAMAT',
				width: 300
			}

		]
	)
};

function GetPasien(nFormAsal,nName_ID)
{
	if (rowSelectedLookPasien != undefined || nName_ID != undefined)
	{
		if (nFormAsal === nFormPendaftaran)
		{

                        Ext.getCmp('txtNoRequest').setValue(rowSelectedLookPasien.data.KD_PASIEN);
                        Ext.getCmp('txtNama').setValue(rowSelectedLookPasien.data.NAMA);
                        Ext.getCmp('txtNamaKeluarga').setValue(rowSelectedLookPasien.data.NAMA_KELUARGA);
                        Ext.getCmp('txtTempatLahir').setValue(rowSelectedLookPasien.data.TEMPAT_LAHIR);
                        Ext.getCmp('cboPendidikanRequestEntry').setValue(rowSelectedLookPasien.data.PENDIDIKAN);
                        Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowSelectedLookPasien.data.PEKERJAAN);
                        Ext.getCmp('cboWarga').setValue(rowSelectedLookPasien.data.WNI);
                        Ext.getCmp('txtAlamat').setValue(rowSelectedLookPasien.data.ALAMAT);
                        Ext.getCmp('cboAgamaRequestEntry').setValue(rowSelectedLookPasien.data.AGAMA);
                        Ext.getCmp('cboGolDarah').setValue(rowSelectedLookPasien.data.GOL_DARAH);
                        Ext.get('dtpTanggalLahir').dom.value = ShowDate(rowSelectedLookPasien.data.TGL_LAHIR);
						var tglLahir = Ext.get('dtpTanggalLahir').dom.value;
                        Ext.getCmp('cboStatusMarital').setValue(rowSelectedLookPasien.data.STATUS_MARITA);
                        var tmpjk = "";
                        var tmpwni = "";
                        if (rowSelectedLookPasien.data.JENIS_KELAMIN === "t")
                            {
                                tmpjk = "Laki - Laki";
                            }
                            else
                                {
                                    tmpjk = "Perempuan";
                                }
                        Ext.getCmp('cboJK').setValue(tmpjk);
                        if (rowSelectedLookPasien.data.WNI === "t")
                            {
                                tmpwni = "WNI";
                            }
                            else
                                {
                                    tmpwni = "WNA";
                                }
                        Ext.getCmp('cboWarga').setValue(tmpwni);
                        //getdatatempat_viDaftar(rowdata.KD_PASIEN);

                        Ext.getCmp('cboPropinsiRequestEntry').setValue(rowSelectedLookPasien.data.PROPINSI);
                        Ext.getCmp('cboKabupatenRequestEntry').setValue(rowSelectedLookPasien.data.KABUPATEN);
                        Ext.getCmp('cboKecamatanRequestEntry').setValue(rowSelectedLookPasien.data.KECAMATAN);

                        Ext.getCmp('btnSimpan_viDaftar').disable();
                        Ext.getCmp('btnSimpanExit_viDaftar').disable();
                        Ext.getCmp('btnDelete_viDaftar').enable();
                        Ext.getCmp('btnUlang_viDaftar').enable();
						
//                        mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
		}

	}
	rowSelectedLookPasien=undefined;
	mWindowLookup.close();
}

function setUsia(Tanggal)
{

                                                            Ext.Ajax.request
                                                                ( {
                                                                           url: baseURL + "index.php/main/GetUmur",
                                                                           params: {
                                                                           TanggalLahir: ShowDateReal(Tanggal)
                                                                           },
                                                                           success: function(o)
                                                                           {
																			//alert('test');  
																			
                                                                           var tmphasil = o.responseText;
                                                                         // alert(tmphasil);
                                                                           var tmp = tmphasil.split(' ');
																		    //alert(tmp.length);
                                                                          if (tmp.length == 6)
                                                                                                {
                                                                                                    Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                    Ext.getCmp('txtBlnLahir').setValue(tmp[2]);
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[4]);
                                                                                                }
                                                                                                else if(tmp.length == 4)
                                                                                                {
                                                                                                    if(tmp[1]== 'years' && tmp[3] == 'day')
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue(tmp[2]);  
                                                                                                    }else{
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[2]);
                                                                                                          }
                                                                                                }
                                                                                                else if(tmp.length == 2 )
                                                                                                {
																									
                                                                                                    if (tmp[1] == 'year' )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'years' )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'mon'  )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else if (tmp[1] == 'mons'  )
                                                                                                    {
                                                                                                        Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                        Ext.getCmp('txtBlnLahir').setValue(tmp[0]);
                                                                                                        Ext.getCmp('txtHariLahir').setValue('0');
                                                                                                    }
																									else{
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtHariLahir').setValue(tmp[0]);
                                                                                                        }
                                                                                                }
																								
																								else if(tmp.length == 1)
                                                                                                {
                                                                                                    Ext.getCmp('txtThnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtBlnLahir').setValue('0');
                                                                                                    Ext.getCmp('txtHariLahir').setValue('1');
                                                                                                }else
                                                                                                {
                                                                                                    alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                                }
																		   }
                                                                           
                                                                           
                                                                           });


	
}


function getQueryCariPasien()
{
	var strKriteria = "";

	if(Ext.get('txtNoRMLookupPasien').getValue() != '')
	{
		strKriteria += " RIGHT(pasien.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	}

	if(Ext.get('txtNamaMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		}
	}

	if(Ext.get('txtAlamatMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		}
	}

	return strKriteria;
}

function RefreshDataLookupPasien(criteria)
{
	dsLookupPasienList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 100,
					Sortdir: 'ASC',
					target: 'ViewKunjungan',//'Viview_viDataPasien',
					param: criteria
				}
			}
		);

	return dsLookupPasienList;
}

function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtnamaPeserta') ;

   if(value == "Perseorangan")
   {    
		
        Ext.getCmp('txtNamaPeserta').hide()
        Ext.getCmp('txtNoAskes').hide()
        Ext.getCmp('txtNoSJP').hide()
		cboAsuransi.label.update('Perseorangan');
        //Ext.getCmp('cboPerseorangan').show()
		//cboAsuransi.labelEl.update(namacbokelpasien);
        Ext.getCmp('cboAsuransi').show()
       // Ext.getCmp('cboPerusahaanRequestEntry').hide()
        

   }
   else if(value == "Perusahaan")
   {    
   
    
        Ext.getCmp('txtNamaPeserta').hide()
        Ext.getCmp('txtNoAskes').hide()
        Ext.getCmp('txtNoSJP').hide()
      //  Ext.getCmp('cboPerseorangan').hide()
	    //Ext.getCmp('txtNama').setFieldLabel(namacbokelpasien);
		cboAsuransi.label.update('Perusahaan');
        Ext.getCmp('cboAsuransi').show()
     //   Ext.getCmp('cboPerusahaanRequestEntry').show()
	// cboAsuransi.labelEl.update(namacbokelpasien);
	 
        
   }
   else
       {
         Ext.getCmp('txtNamaPeserta').show()
         Ext.getCmp('txtNoAskes').show()
         Ext.getCmp('txtNoSJP').show()
    //     Ext.getCmp('cboPerseorangan').hide()
         Ext.getCmp('cboAsuransi').show()
		 cboAsuransi.label.update('Asuransi');
		 
        // Ext.getCmp('cboPerusahaanRequestEntry').hide()
         
       }
}

function printbill()
{
    Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparamreport_viDaftar(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftar('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftar(tmpcriteriaRWJ);
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftar').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftar = false;
							//Ext.get('txtNoKunjungan_viDaftar').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN
							Ext.getCmp('btnSimpan_viDaftar').disable();
							Ext.getCmp('btnSimpanExit_viDaftar').disable();
//							Ext.getCmp('btnDelete_viDaftar').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
}

function dataparamreport_viDaftar()
{
    var paramsreport_ViPendaftaran =
		{
      
                    Table: 'DirectPrinting',
                    No_TRans: tmpnotransaksi,
                    KdKasir : tmpkdkasir
//                    NoMedrec:  Ext.get('txtNoRequest').getValue(),
//                    KelPasien: Ext.getCmp('kelPasien').getValue(),
//                    Dokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//                    NamaPasien: Ext.get('txtNama').getValue(),
//                    Alamat : Ext.get('txtAlamat').getValue(),
//                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue()
		};
    return paramsreport_ViPendaftaran
}