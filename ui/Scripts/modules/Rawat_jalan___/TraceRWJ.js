var now = new Date();
var anow=now.format('Y-m-d');
var TraceRJ={};
TraceRJ.form={};
TraceRJ.func={};
TraceRJ.vars={};
TraceRJ.func.parent=TraceRJ;
TraceRJ.form.DataStore={};
TraceRJ.form.Grid={};
TraceRJ.form.Panel={};
TraceRJ.form.TextField={};
TraceRJ.form.DateField={};
TraceRJ.form.Button={};
TraceRJ.form.DataStore.trace=new WebApp.DataStore({fields:['TANGGAL','JAM','KD_PASIEN','NAMA_PASIEN','UNIT','DOKTER','ALAMAT']});

TraceRJ.func.getId=function(){
	$this=this.parent;
	if($this.vars.genNum == undefined)$this.vars.genNum=0;
	$this.vars.genNum+=1;
	return 'nci-tracer-rj-'+$this.vars.genNum;
};

TraceRJ.func.getInit=function(mod_id){
	$this=this.parent;
	$this.func.getTrace();
	setInterval(function(){
		$this.func.getTrace();
	},15000);
	
	var grid= new Ext.Panel({
        id			: $this.func.getId(),
        bodyStyle	: 'margin: 5px 5px;padding: 5px 0 0 5px;',
        layout		: 'column',
        border		: true,
        items		:[
			{
			    layout		: 'form',
			    labelWidth	: 150,
			    border		: false,
			    items		: [
         		   $this.form.TextField.txtSearch=new Ext.form.TextField({
         			   id			: $this.func.getId(),
         			   fieldLabel	: 'No. Medrec/Nama Pasien',
         			   width		: 150,
         			   enableKeyEvents: true,
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   console.log(typeof $this.form.TextField.txtSearch.getValue());
         						  $this.func.getTrace();
         					   }
         				   }
         			   }
         		   }),
				   /* {
						xtype: 'textfield',
						fieldLabel: 'Tanggal ',
						name: 'tglTracerRWJ',
						id	: 'tglTracerRWJ',
						format: 'd/M/Y',
						value: now,
						anchor: '95%',
						listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   //console.log(typeof $this.form.DateField.caritanggal.getValue());
         						  $this.func.getTrace();
         					   }
         				   }
         			   }
                   },  */
				   $this.form.DateField.caritanggal=new Ext.form.DateField({
         			   id			: 'tglTracerRWJ',
         			   fieldLabel	: 'Tanggal',
         			   width		: 150,
					   format		: 'd/M/Y',
					   value: anow,
         			   enableKeyEvents: true,
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
         						  $this.func.getTrace();
         					   }
         				   }
         			   }
         		   }),  
				   $this.form.Button.btnSearch= new Ext.Button({
					text	: 'Cari',
					id		: $this.func.getId(),
					tooltip	: nmLookup,
					iconCls	: 'find',
					handler	: function(){
						$this.func.getTrace();
					}
				}),
			    ]
			},
			
			{
                layout		: 'form',
                border		: false,
                html		: ' &nbsp; *Table Auto Refresh.'
            }
        ]
    });
	this.parent.form.Panel.trace= new Ext.Panel({
	    id			: mod_id,
	    closable	: true,
        layout		: 'form',
        title		: 'Tracer ',
        border		: false,
        shadhow		: true,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        autoScroll	: false,
	    items		:[
 		  	grid,
 		  	$this.func.getGridMain()
	    ]
	});
	return this.parent.form.Panel.trace;
};

TraceRJ.func.getGridMain=function(){
	$this=this.parent;
    $this.form.Grid.trace	= new Ext.grid.EditorGridPanel({
        title		: 'Tracer',
		id			: $this.func.getId(),
		stripeRows	: true,
//		height		: 130,
        store		: $this.form.DataStore.trace,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        cm			: new Ext.grid.ColumnModel([
                  new Ext.grid.RowNumberer(),
            {
    			id			: $this.func.getId(),
            	header		: 'Tanggal',
                dataIndex	: 'TANGGAL',
                width		: 80,
    			menuDisabled: true,
                hidden		: false
            },{
    			id			: $this.func.getId(),
            	header		: 'Jam',
                dataIndex	: 'JAM',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Kode Pasien',
                dataIndex	: 'KD_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Nama Pasien',
                dataIndex	: 'NAMA_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Unit',
                dataIndex	: 'UNIT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Dokter',
                dataIndex	: 'DOKTER',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false
            }
       ]),
        viewConfig	: {forceFit: true}
    });
    
    return $this.form.Grid.trace;
};

TraceRJ.func.getTrace=function(tglmasuk){
	$this=this.parent;
	var txtSearch='';
	var cariTanggal=anow;
	if($this.form.TextField.txtSearch != undefined){
		txtSearch=$this.form.TextField.txtSearch.getValue();
	}
	if($this.form.DateField.caritanggal != undefined){
		cariTanggal=$this.form.DateField.caritanggal.getValue().format('Y-m-d');
		console.log(cariTanggal);
	}  
	//console.log(Ext.get('tglTracerRWJ'));
	//console.log(anow);
	TraceRJ.form.DataStore.trace.load({ 
		params: { 
			Skip: 0, 
			Take: 0, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewGridTraceRJ',
			param: 'upper(kd_pasien) like upper(~%'+formatnomedrec(txtSearch)+'%~) or upper(nama_pasien) like upper(~%'+txtSearch+'%~) and tgl_masuk=~'+cariTanggal+'~'//or tgl_masuk=to_char(~'+cariTanggal+'~,"YYYY-m-d")
		} 
	});
	//
};

CurrentPage.page = TraceRJ.func.getInit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
console.log(TraceRJ);