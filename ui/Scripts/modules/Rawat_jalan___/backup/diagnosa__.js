var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};


var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);
var selectCountStatusByr_viDiagnosa;
var dsTRDiagnosaList;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();



		
function DiagnosaLookUp(medrec,notransaksi,tgltransaksi,namapasien,kdokter,namadokter,kdunit,namaunit,customer,urut_masuk) 
{
	//Nomedrec=medrec;
	//notransaksi
	//alert(Nomedrec);
    var lebar = 600;
    FormLookUpsdetailTRDiagnosa = new Ext.Window
    (
        {
            id: 'gridDiagnosa',
            title: 'Diagnosa',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRDiagnosa(lebar),
            listeners:
            {
              
            }
        }
    );

    FormLookUpsdetailTRDiagnosa.show();
    
        DiagnosaAddNew(medrec,notransaksi,tgltransaksi,namapasien,kdokter,namadokter,kdunit,namaunit,customer,urut_masuk);
 

};

function getFormEntryTRDiagnosa(lebar) 
{
    var pnlTRDiagnosa = new Ext.FormPanel
    (
        {
            id: 'PanelTRDiagnosa',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:170,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputDiagnosa(lebar)],
           tbar:
            [
               
               
            ]
        }
    );

 var GDtabDetailDiagnosa = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailDiagnosa',
        region: 'center',
        activeTab: 0,
		height:250,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [GetDTLTRDiagnosaGrid()
				
		                //-------------- ## --------------
					],
        tbar:
                [
                        
						{
                                id:'btnTambahBrsDiagnosa',
                                text: 'Tambah Baris',
                                tooltip: 'Tambah Baris',
                                iconCls: 'AddRow',
                                handler: function()
                                {
                                        TambahBarisDiagnosa();
                                }
                        },
                        '-',
                        {
                                id:'btnHpsBrsDiagnosa',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                HapusBarisDiagnosa();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },' ','-',
							 {
                    text: 'simpan',
                    id: 'btnSimpanDiagnosa',
                    tooltip: nmSimpan,
                    iconCls: 'save',
                    handler: function()
                        {
						
						 if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                 //Dataupdate_Diagnosa(false);
																Datasave_Diagnosa(false);
																FormLookUpsdetailTRDiagnosa.close();
																//RefreshDataFilterDiagnosa();
																
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningDiagnosa('Pilih record lalu ubah STAT_DIAG  ','Ubah data');
                                                }
                                        }
                       
                    }
                }, '-',
                {
                    text: nmLookup,
                    id: 'btnLookupDiagnosa',
                    tooltip: nmLookup,
                    iconCls: 'find',
                    handler: function()
                    {
                        //if (FocusCtrlCMDiagnosa === 'txtAset')
                        //{
                            var p = RecordBaruDiagnosa();
                            var str='';
                            if (Ext.get('txtKdDokterDiagnosa').dom.value  != undefined && Ext.get('txtKdDokterDiagnosa').dom.value  != '')
                            {
                                    //str = ' where kd_dokter =~' + Ext.get('txtKdDokterDiagnosa').dom.value  + '~';
                                    //str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokterDiagnosa').dom.value  + "~";
                                    str = 'kd_dokter = ~' + Ext.get('txtKdDokterDiagnosa').dom.value  + '~';
                            };
                            FormLookupDiagnosa(str, dsTRDetailDiagnosaList, p, true, '', true);
                        /*}
                        else if (FocusCtrlCMDiagnosa === 'txtReq')
                        {
                            //FormLookupEmployee(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
                            FormLookupEmployee('txtNoMedrectransaksiDiagnosa','txtNoMedrectransaksiDiagnosa','');
                            //FormLookupEmployee('txtNoMedrectransaksiDiagnosa','txtNoMedrectransaksiDiagnosa','',true,p,dsTRDetailDiagnosaList,true);
                        };
                    */}
                }
                ],
		//bbar :			/**/
        }
    );
	
   
   
   var pnlTRDiagnosa2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRDiagnosa2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:260,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailDiagnosa
			
			]
        }
    );
   
   
   
   
    var FormDepanDiagnosa = new Ext.Panel
	(
		{
		    id: 'FormDepanDiagnosa',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRDiagnosa,pnlTRDiagnosa2	,
				{
					xtype: 'compositefield',
					style:{'text-align':'right','margin-left':'290px'},
				//	fieldLabel: 'Total',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					//width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viDiagnosa',
                            name: 'txtJumlah2EditData_viDiagnosa',
							style:{'text-align':'right','margin-left':'480px'},
                            width: 80,
                           hidden:true,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah1EditData_viDiagnosa',
                            name: 'txtJumlah1EditData_viDiagnosa',
							style:{'text-align':'right','margin-left':'310px'},
                            width: 80,
                           hidden:true,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},

			]

		}
	);

    return FormDepanDiagnosa
};
///---------------------------------------------------------------------------------------///
function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',

        items: 
            [
                {
                    x:0,
                    y:60,
                    xtype: 'textarea',
                    id:'TxtHistoriDeleteDataPasien',
                    hideLabel: true,
                    name: 'msg',
                    anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                }

            ]
    })};
function TambahBarisDiagnosa()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruDiagnosa();
        dsTRDetailDiagnosaList.insert(dsTRDetailDiagnosaList.getCount(), p);
    };
};

function HapusBarisDiagnosa()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus' + ' ' + 'baris ke' + ' : ' + (CurrentDiagnosa.row + 1) + ' ' +  ' ' + 'dengan nama produk' + '  : ' + cellSelecteddeskripsi.data.DESKRIPSI ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
                    AddNewDiagnosa = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentDiagnosa.data.data.NO_TRANSAKSI,
        RowReq: CurrentDiagnosa.data.data.URUT_MASUK,
        Hapus:2
    };
	
    return params
};

function getParamDataupdateDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentDiagnosa.data.data.NO_TRANSAKSI,
        RowReq: CurrentDiagnosa.data.data.URUT_MASUK,
	//	TrKodeTranskasi: CurrentDiagnosa.data.data.NO_TRANSAKSI,
        STAT_DIAG: CurrentDiagnosa.data.data.STAT_DIAG,
        Ubah:1
    };
	
    return params
};

function getParamDetailTransaksiDiagnosa2() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		
		//TrKodeTranskasi: Ext.get('txtNoTransaksiDiagnosa').getValue(),
		KdPasien: Ext.get('txtNoMedrectransaksiDiagnosa').getValue(),
		KdUnit: Ext.get('txtKdUnitDiagnosa').getValue(),
		//KdUrut:Ext.get('txtKdUrut').getValue(),
		UrutMasuk:Ext.get('txtUrutMasuk').getValue(),
		//DeptId:Ext.get('txtKdDokterDiagnosa').dom.value ,
		Tgl: Ext.get('dtpTanggalDetransaksiDiagnosa').dom.value,
		List:getArrDetailTrDiagnosa(),
		JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamDatasavecoponent()
{
    var params =
    {
        Table: 'ViewdetailComponen',
        //TrKodeTranskasi: CurrentDiagnosa.data.data.NO_TRANSAKSI,
        //RowReq: CurrentDiagnosa.data.data.URUT_MASUK,
	//	TrKodeTranskasi: CurrentDiagnosa.data.data.NO_TRANSAKSI,
        //Qty: CurrentDiagnosa.data.data.QTY,
        //Ubah:1
    };
	
    return params
};
/*
 kd_penyakit character varying(12) NOT NULL,
  kd_pasien character varying(12) NOT NULL,
  kd_unit character varying(5) NOT NULL,
  tgl_masuk timestamp without time zone NOT NULL,
  urut_masuk smallint NOT NULL,
  urut smallint NOT NULL,
  stat_diag smallint NOT NULL,
  kasus boolean,
  tindakan smallint,
  perawatan smallint,
*/
  function GetDTLTRDiagnosaGrid() 
{
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KASUS','STAT_DIAG','NO_TRANSAKSI','URUT_MASUK','TGL_TRANSAKSI','KASUS'];
	
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail })
   //RefreshDataDiagnosaDetail() ;
    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Diagnosa',
            stripeRows: true,
            store: dsTRDetailDiagnosaList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                            CurrentDiagnosa.row = row;
                            CurrentDiagnosa.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel()
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'colKdProduk',
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
                hidden:false
            },
			{
                id: 'colePenyakitDiagnosa',
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
                width:200
                
            }
            ,
            {
                id: 'colProblemDiagnosa',
                header: 'Diagnosa',
                width:130,
				align: 'right',
				//hidden :true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            },
            {
                id: 'colKasusDiagnosa',
                header: 'Kasus',
                width:130,
				align: 'right',
				//hidden :true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            }

        ]
    )
};

function GetLookupAssetCMDiagnosa(str)
{
	if (AddNewDiagnosa === true)
	{
		var p = new mRecordDiagnosa
		(
			{
				'KASUS':'',
				'KD_PENYAKIT':'',
				'PENYAKIT':'', 
				//'KD_TARIF':'', 
				//'HARGA':'',
				'STAT_DIAG':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.STAT_DIAG,
				'TGL_TRANSAKSI':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.TGL_TRANSAKSI,
				//'DESC_REQ':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.DESC_REQ,
				//'KD_TARIF':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.KD_TARIF,
				'URUT_MASUK':''
			}
		);
		
		FormLookupDiagnosa(str,dsTRDetailDiagnosaList,p,true,'',false);
	}
	else
	{	
		var p = new mRecordDiagnosa
		(
			{
				'KASUS':'',
				'KD_PENYAKIT':'',
				'PENYAKIT':'', 
				//'KD_TARIF':'', 
				//'HARGA':'',
				'STAT_DIAG':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.STAT_DIAG,
				'TGL_TRANSAKSI':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.TGL_TRANSAKSI,
				//'DESC_REQ':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.DESC_REQ,
				//'KD_TARIF':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.KD_TARIF,
				'URUT_MASUK':dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK
			}
		);
	
		FormLookupDiagnosa(str,dsTRDetailDiagnosaList,p,false,CurrentDiagnosa.row,false);
	};
};

function RecordBaruDiagnosa()
{
	var p = new mRecordDiagnosa
	(
		{
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		  //  'KD_TARIF':'', 
		   // 'HARGA':'',
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':'', 
		    //'DESC_REQ':'',
		   // 'KD_TARIF':'',
		    'URUT_MASUK':''
		}
	);
	
	return p;
};



function mEnabledDiagnosaCM(mBol)
{

	 Ext.get('btnLookupDiagnosa').dom.disabled=mBol;
	 Ext.get('btnTambahBrsDiagnosa').dom.disabled=mBol;
	 Ext.get('btnHpsBrsDiagnosa').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function DiagnosaAddNew(medrec,notransaksi,tgltransaksi,namapasien,kdokter,namadokter,kdunit,namaunit,customer,urut_masuk) 
{
    AddNewDiagnosa = true;
	Ext.get('txtNoMedrectransaksiDiagnosa').dom.value=medrec;
	Ext.get('txtNoTransaksiDiagnosa').dom.value = notransaksi;
    Ext.get('dtpTanggalDetransaksiDiagnosa').dom.value = tgltransaksi;
	Ext.get('txtNamaPasienDiagnosa').dom.value = namapasien;
	Ext.get('txtKdDokterDiagnosa').dom.value   = kdokter;
	Ext.get('txtNamaDokterDiagnosa').dom.value = namadokter;
	Ext.get('txtKdUnitDiagnosa').dom.value=kdunit;
	Ext.get('txtNamaUnitDiagnosa').dom.value=namaunit;
	Ext.get('txtCustomerDiagnosa').dom.value=customer;
	Ext.get('txtUrutMasuk').dom.value=urut_masuk;
	rowSelectedDiagnosa=undefined;
	dsTRDetailDiagnosaList.removeAll();
	mEnabledDiagnosaCM(false);

};

function RefreshDataDiagnosaDetail(no_transaksi) 
{
    var strKriteriaDiagnosa='';
    //strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = "\"no_transaksi\" = ~" + no_transaksi + "~";
    //strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
   
    dsTRDetailDiagnosaList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailTRRWJ',
			    param: strKriteriaDiagnosa
			}
		}
	);
    return dsTRDetailDiagnosaList;
};

///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiDiagnosa() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		
		//TrKodeTranskasi: Ext.get('txtNoTransaksiDiagnosa').getValue(),
		KdPasien: Ext.get('txtNoMedrectransaksiDiagnosa').getValue(),
		KdUnit: Ext.get('txtKdUnitDiagnosa').getValue(),
		UrutMasuk : Ext.get('txtUrutMasuk').getValue(),
		//DeptId:Ext.get('txtKdDokterDiagnosa').dom.value ,
		Tgl: Ext.get('dtpTanggalDetransaksiDiagnosa').dom.value,
		List:getArrDetailTrDiagnosa(),
		JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function GetListCountDetailDiagnosa()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS
			//y += z + 'TGL_BERLAKU='+ ShowDate(dsTRDetailDiagnosaList.data.items[i].data.TGL_BERLAKU)
			//y += z + 'KD_TARIF='+dsTRDetailDiagnosaList.data.items[i].data.KD_TARIF
			//y += z + 'HARGA='+formatCurrency(dsTRDetailDiagnosaList.data.items[i].data.HARGA)
			
			
			if (i === (dsTRDetailDiagnosaList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputDiagnosa(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:155,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDiagnosa(lebar),getItemPanelmedrecDiagnosa(lebar),getItemPanelUnitDiagnosa(lebar) ,getItemPanelDokterDiagnosa(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnitDiagnosa(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitDiagnosa',
					    id: 'txtKdUnitDiagnosa',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnitDiagnosa',
					    id: 'txtNamaUnitDiagnosa',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelDokterDiagnosa(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokterDiagnosa',
					    id: 'txtKdDokterDiagnosa',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Customer Test ',
						//hideLabel:true,
					    name: 'txtCustomerDiagnosa',
					    id: 'txtCustomerDiagnosa',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokterDiagnosa',
					    id: 'txtNamaDokterDiagnosa',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: 'Urut Masuk ',
						//hideLabel:true,
					    name: 'txtUrutMasuk',
					    id: 'txtUrutMasuk',
					    anchor: '99%',
						hidden:true,
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiDiagnosa(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiDiagnosa',
					    id: 'txtNoTransaksiDiagnosa',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksiDiagnosa',
					    name: 'dtpTanggalDetransaksiDiagnosa',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedrecDiagnosa(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   ' No. Medrec',
					    name: 'txtNoMedrectransaksiDiagnosa',
					    id: 'txtNoMedrectransaksiDiagnosa',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtNamaPasienDiagnosa',
					    id: 'txtNamaPasienDiagnosa',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataDiagnosa() 
{

//refresh ganti target
    dsTRDiagnosaList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountDiagnosa,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirRwj',
                param : ''
            }		
        }
    );
	
    rowSelectedDiagnosa = undefined;
    return dsTRDiagnosaList;
};


function RefreshDataFilterDiagnosa() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viDiagnosa').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viDiagnosa').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viDiagnosa').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viDiagnosa').getValue() != '' && Ext.get('cboUNIT_viDiagnosa').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viDiagnosa').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viDiagnosa').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtFilterGridDataView_DOKTER_viDiagnosa').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viDiagnosa').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~%' + Ext.get('TxtFilterGridDataView_DOKTER_viDiagnosa').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viDiagnosa').getValue() == 'Bayar')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  co_status = true';
		}
		else
		{
		
                        KataKunci += ' and co_status =  true';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viDiagnosa').getValue() == 'Belum Bayar')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  co_status = false';
		}
		else
		{
	
                        KataKunci += ' and co_status =  false';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterDiagnosa').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterDiagnosa').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterDiagnosa').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterDiagnosa').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterDiagnosa').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined) 
    {  
		dsTRDiagnosaList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountDiagnosa, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirRwj',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
		//RefreshDataDiagnosa();
	};
    
	return dsTRDiagnosaList;
};



function Datasave_Diagnosa(mBol) 
{	
	if (ValidasiEntryCMDiagnosa(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewDiagnosa == true) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/saveDiagnosa",					
					params: getParamDetailTransaksiDiagnosa2(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
								Ext.get('txtNoTransaksiDiagnosa').dom.value=cst.ReqId;
								RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
							};
							AddNewDiagnosa = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionRWJ/saveDiagnosa",
					params: getParamDetailTransaksiDiagnosa2(),
					success: function(o) 
					{
						RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
								//RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function Dataupdate_Diagnosa(mBol) 
{	
	if (ValidasiEntryCMDiagnosa(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewDiagnosa == true) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",					
					params: getParamDataupdateDiagnosaDetail(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							//ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
								Ext.get('txtNoTransaksiDiagnosa').dom.value=cst.ReqId;
								RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
							};
							AddNewDiagnosa = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdateDiagnosaDetail(),
					success: function(o) 
					{
						RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
								RefreshDataDiagnosaDetail(Ext.get('txtNoTransaksiDiagnosa').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function ValidasiEntryCMDiagnosa(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiDiagnosa').getValue() == '') || (Ext.get('txtNoMedrectransaksiDiagnosa').getValue() == '') || (Ext.get('txtNamaPasienDiagnosa').getValue() == '') || (Ext.get('txtNamaDokterDiagnosa').getValue() == '') || (Ext.get('dtpTanggalDetransaksiDiagnosa').getValue() == '') || dsTRDetailDiagnosaList.getCount() === 0 || (Ext.get('txtKdDokterDiagnosa').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiDiagnosa').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrectransaksiDiagnosa').getValue() == '') 
		{
			ShowPesanWarningDiagnosa(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDiagnosa').getValue() == '') 
		{
			ShowPesanWarningDiagnosa(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksiDiagnosa').getValue() == '') 
		{
			ShowPesanWarningDiagnosa(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokterDiagnosa').getValue() == '' || Ext.get('txtKdDokterDiagnosa').dom.value  === undefined) 
		{
			ShowPesanWarningDiagnosa(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailDiagnosaList.getCount() === 0) 
		{
			ShowPesanWarningDiagnosa(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};


function ShowPesanWarningDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteDiagnosa() 
{
   if (ValidasiEntryCMDiagnosa(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiDiagnosa(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                                       // RefreshDataDiagnosa();
                                        DiagnosaAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningDiagnosa(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningDiagnosa(nmPesanHapusGagal + ' , ' + nmKonfirmasiRejected ,nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorDiagnosa(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};







