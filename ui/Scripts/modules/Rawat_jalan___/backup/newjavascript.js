 /* Date store */
var dataStore = new Ext.data.JsonStore({
    proxy: new Ext.data.HttpProxy({
        url: Ext.SERVER_URL + 'get_data',
        method: 'post'
    }),
    listeners :{
        load:function() {
            /**
            /* The code block at the end labeled
            /* 'And select a value after load store' can be written here.
            */
        }
    },
    fields: ['id',  'name'],
    totalProperty: 'totalCount',
    root: 'data'
});

{totalCount: 2, data:  [{"id":1,"name":"First Item"},{"id":2,"name":"Second Item"}] }
{
    xtype: 'combo',
    id: "domId",
    fieldLabel: 'Data Name',
    hiddenName: 'cb_data',
    store: dataStore,
    displayField: 'name',
    labelSeparator :'',
    valueField: 'id',
    typeAhead: true,
    autoSelect: true,
    mode: 'local',
    triggerAction: 'all',
    selectOnFocus: true,
    allowBlank: true,
    blankText: 'Select status',
    editable: false
}
 /* Default selection */
/* And select a value after load store */
var combo = Ext.getCmp("domId");
combo.getStore().on(
    "load",function() {
        var value = 2;
        
        if(value == 0) {
            var recordSelected = combo.getStore().getAt(0);
            combo.setValue(recordSelected.get('id'));
        } else {
            combo.setValue(value);
        }
    },
    this,
    {
        single: true
    }
);