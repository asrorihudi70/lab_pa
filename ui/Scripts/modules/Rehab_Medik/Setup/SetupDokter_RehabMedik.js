var dataSource_viRehabMedikSetupDokter;
var selectCount_viRehabMedikSetupDokter=50;
var NamaForm_viRehabMedikSetupDokter="SetupDokter";
var mod_name_viRehabMedikSetupDokter="SetupDokter";
var now_viRehabMedikSetupDokter= new Date();
var rowSelected_viRehabMedikSetupDokter;
var setLookUps_viRehabMedikSetupDokter;
var tanggal = now_viRehabMedikSetupDokter.format("d/M/Y");
var jam = now_viRehabMedikSetupDokter.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viRehabMedikSetupDokter;
var kodesetupDokter='';

var CurrentData_viRehabMedikSetupDokter =
{
	data: Object,
	details: Array,
	row: 0
};
var tmpkd_unit;
var default_Kd_Unit;
var dsunitRehabMedik_viSetupDokterRehabMedik;
var RehabMedikSetupDokter={};
RehabMedikSetupDokter.form={};
RehabMedikSetupDokter.func={};
RehabMedikSetupDokter.vars={};
RehabMedikSetupDokter.func.parent=RehabMedikSetupDokter;
RehabMedikSetupDokter.form.ArrayStore={};
RehabMedikSetupDokter.form.ComboBox={};
RehabMedikSetupDokter.form.DataStore={};
RehabMedikSetupDokter.form.Record={};
RehabMedikSetupDokter.form.Form={};
RehabMedikSetupDokter.form.Grid={};
RehabMedikSetupDokter.form.Panel={};
RehabMedikSetupDokter.form.TextField={};
RehabMedikSetupDokter.form.Button={};

RehabMedikSetupDokter.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viRehabMedikSetupDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viRehabMedikSetupDokter(mod_id_viRehabMedikSetupDokter){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viRehabMedikSetupDokter = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viRehabMedikSetupDokter = new WebApp.DataStore
	({
        fields: FieldMaster_viRehabMedikSetupDokter
    });
    dataGriRehabMedikSetupDokter();
    // Grid gizi Perencanaan # --------------
	GridDataView_viRehabMedikSetupDokter = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viRehabMedikSetupDokter,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							/*kodesetupDokter=rowSelected_viRehabMedikSetupDokter.data.kd_Dokter
							alert(kodesetupDokter);*/
							rowSelected_viRehabMedikSetupDokter = undefined;
							rowSelected_viRehabMedikSetupDokter = dataSource_viRehabMedikSetupDokter.getAt(row);
							CurrentData_viRehabMedikSetupDokter
							CurrentData_viRehabMedikSetupDokter.row = row;
							CurrentData_viRehabMedikSetupDokter.data = rowSelected_viRehabMedikSetupDokter.data;
							kodesetupDokter=rowSelected_viRehabMedikSetupDokter.data.kd_dokter;
							//DataInitRehabMedikSetupDokter(rowSelected_viRehabMedikSetupDokter.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupDokter=rowSelected_viRehabMedikSetupDokter.data.kd_Dokter;
					rowSelected_viRehabMedikSetupDokter = dataSource_viRehabMedikSetupDokter.getAt(ridx);
					console.log(rowSelected_viRehabMedikSetupDokter)
					if (rowSelected_viRehabMedikSetupDokter != undefined)
					{
						DataInitRehabMedikSetupDokter(rowSelected_viRehabMedikSetupDokter.data);
						//setLookUp_viRehabMedikSetupDokter(rowSelected_viRehabMedikSetupDokter.data);
					}
					else
					{
						//setLookUp_viRehabMedikSetupDokter();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Dokter',
						dataIndex: 'kd_dokter',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Dokter',
						dataIndex: 'nama',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Jenis Dokter',
						dataIndex: 'jenis_dokter',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'spesialisasi',
						dataIndex: 'spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viRehabMedikSetupDokter',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Dokter RehabMedik',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Dokter RehabMedik',
						id: 'btnEdit_viRehabMedikSetupDokter',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viRehabMedikSetupDokter != undefined)
							{
								DataInitRehabMedikSetupDokter(rowSelected_viRehabMedikSetupDokter.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viRehabMedikSetupDokter, selectCount_viRehabMedikSetupDokter, dataSource_viRehabMedikSetupDokter),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabRehabMedikSetupDokter = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viRehabMedikSetupDokter = new Ext.Panel
    (
		{
			title: NamaForm_viRehabMedikSetupDokter,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viRehabMedikSetupDokter,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabRehabMedikSetupDokter,
					GridDataView_viRehabMedikSetupDokter],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSetupDokter_viRehabMedikSetupDokter',
						handler: function(){
							AddNewRehabMedikSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viRehabMedikSetupDokter',
						handler: function()
						{
							loadMask.show();
							dataSave_viRehabMedikSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viRehabMedikSetupDokter',
						handler: function()
						{
							if (kodesetupDokter==='')
							{
								ShowPesanErrorRehabMedikSetupDokter('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viRehabMedikSetupDokter();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viRehabMedikSetupDokter',
						handler: function()
						{
							dataSource_viRehabMedikSetupDokter.removeAll();
							dataGriRehabMedikSetupDokter();
							kodesetupDokter='';
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viRehabMedikSetupDokter;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Dokter'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSetupDokter_RehabMedikSetupDokter',
								name: 'txtKdSetupDokter_RehabMedikSetupDokter',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningRehabMedikSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Dokter'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							RehabMedikSetupDokter.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: RehabMedikSetupDokter.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').setValue(b.data.kd_dokter);
									
									GridDataView_viRehabMedikSetupDokter.getView().refresh();
									
								},
								/* onShowList:function(a){
									dataSource_viRehabMedikSetupDokter.removeAll();
									
									var recs=[],
									recType=dataSource_viRehabMedikSetupDokter.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viRehabMedikSetupDokter.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_dokter   : o.kd_dokter,
										nama 		: o.nama,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/rehab_medik/functionSetupRM/getDokter",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtSpesialisasi_RehabMedikSetupDokter',
								name: 'txtSpesialisasi_RehabMedikSetupDokter',
								width: 150,
								tabIndex:3
							},{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Unit '
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},mComboUnitRehabMedik()
							
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function mComboUnitRehabMedik(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitRehabMedik_viSetupDokterRehabMedik= new WebApp.DataStore({ fields: Field });
    dsunitRehabMedik_viSetupDokterRehabMedik.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '7' and kd_unit in('74')"
		}
	});
    var cboUnitRehabMedik_viSetupDokterRehabMedik = new Ext.form.ComboBox({
		id: 'cboUnitRehabMedik_viSetupDokterRehabMedik',
		x: 130,
		y: 90,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitRehabMedik_viSetupDokterRehabMedik,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkd_unit = b.data.KD_UNIT;
				}
			}
	});
    return cboUnitRehabMedik_viSetupDokterRehabMedik;
}
//------------------end---------------------------------------------------------

function dataGriRehabMedikSetupDokter(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/rehab_medik/functionSetupRM/getDokterPenunjang",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorRehabMedikSetupDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					tmpkd_unit=cst.kd_unit;
					Ext.getCmp('cboUnitRehabMedik_viSetupDokterRehabMedik').setValue(cst.nama_unit);
					var recs=[],
						recType=dataSource_viRehabMedikSetupDokter.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viRehabMedikSetupDokter.add(recs);
					
					
					
					GridDataView_viRehabMedikSetupDokter.getView().refresh();
				}
				else 
				{
					ShowPesanErrorRehabMedikSetupDokter('Gagal membaca data Dokter', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viRehabMedikSetupDokter(){
	if (ValidasiSaveRehabMedikSetupDokter(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/rehab_medik/functionSetupRM/simpanSetupDokter",
				params: getParamSaveRehabMedikSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorRehabMedikSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoRehabMedikSetupDokter('Berhasil menyimpan data ini','Information');
						dataSource_viRehabMedikSetupDokter.removeAll();
						dataGriRehabMedikSetupDokter();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorRehabMedikSetupDokter('Gagal menyimpan data ini', 'Error');
						dataSource_viRehabMedikSetupDokter.removeAll();
						dataGriRehabMedikSetupDokter();
					};
				}
			}
			
		)
	}
}

function dataDelete_viRehabMedikSetupDokter(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/rehab_medik/functionSetupRM/hapusSetupDokter",
				params: getParamDeleteRehabMedikSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorRehabMedikSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoRehabMedikSetupDokter('Berhasil menghapus data ini','Information');
						AddNewRehabMedikSetupDokter()
						dataSource_viRehabMedikSetupDokter.removeAll();
						dataGriRehabMedikSetupDokter();
						kodesetupDokter='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorRehabMedikSetupDokter('Gagal menghapus data ini', 'Error');
						dataSource_viRehabMedikSetupDokter.removeAll();
						dataGriRehabMedikSetupDokter();
					};
				}
			}
			
		)
	
}

function AddNewRehabMedikSetupDokter(){
	Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').setValue('');
	RehabMedikSetupDokter.vars.nama.setValue('');
	RehabMedikSetupDokter.vars.nama.focus();
	Ext.getCmp('txtSpesialisasi_RehabMedikSetupDokter').setValue('');
};

function DataInitRehabMedikSetupDokter(rowdata){
	Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').setValue(rowdata.kd_dokter);
	RehabMedikSetupDokter.vars.nama.setValue(rowdata.nama);
	Ext.getCmp('txtSpesialisasi_RehabMedikSetupDokter').setValue(rowdata.spesialisasi);
};

function getParamSaveRehabMedikSetupDokter(){
	var	params =
	{
		KdSetupDokter:Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').getValue(),
		NamaSetupDokter:RehabMedikSetupDokter.vars.nama.getValue(),
		Spesialisasi:Ext.getCmp('txtSpesialisasi_RehabMedikSetupDokter').getValue(),
		kd_unit:tmpkd_unit
	}
   
    return params
};

function getParamDeleteRehabMedikSetupDokter(){
	var	params =
	{
		KdSetupDokter:kodesetupDokter,
		kd_unit:tmpkd_unit
	}
   
    return params
};

function ValidasiSaveRehabMedikSetupDokter(modul,mBolHapus){
	var x = 1;
	if(RehabMedikSetupDokter.vars.nama.getValue() === '' /*|| Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').getValue() ===''*/){
		if(RehabMedikSetupDokter.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningRehabMedikSetupDokter('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningRehabMedikSetupDokter('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdSetupDokter_RehabMedikSetupDokter').getValue().length > 3){
		ShowPesanWarningRehabMedikSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningRehabMedikSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRehabMedikSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRehabMedikSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};