var CurrentKasir_RehabMedik =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordKasirRM = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordKasirRM = Ext.data.Record.create
(
    [
       {name: 'cito', mapping:'cito'},
       {name: 'kd_tarif', mapping:'kd_tarif'},
       {name: 'kp_produk', mapping:'kp_produk'},
       {name: 'deskripsi', mapping:'deskripsi'},
       {name: 'tgl_transaksi', mapping:'tgl_transaksi'},
       {name: 'tgl_berlaku', mapping:'tgl_berlaku'},
       {name: 'harga', mapping:'harga'},
       {name: 'qty', mapping:'qty'},
       {name: 'jumlah', mapping:'jumlah'}
    ]
);
//---------TAMBAH BARU 27-SEPTEMBER-2017
var kirimanTanggalDariFormLookUpBuatJadwal ;
var dsDataDaftarJadwal_KASIR_RM;
var FormLookUpsdetailTRTransfer_Rehab_Medik;
var selectSetGolDarah;
var selectSetJK;
var ID_JENIS_KELAMIN;
var tombol_bayar='enable';
var tmpkd_unit = '';
var tmpkd_unit_tarif_mir='';
var FormLookUpsdetailTRPenjasKasirRM;
var tmpkd_unit_asal;
var tmpnama_unit = '';
var KasirKasirRMDataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});
var Trkdunit2;
var dsunitKasirRM_viKasir_RehabMedik;
var dsprinter_kasirKasirRM;
var combovaluesunittujuan = "";
var panelActiveDataPasien;
var ds_customer_viPJ_KasirRM;
var jeniscus_KasirRM;
var kodeCustomerFilterDepan;
var dsDokterRequestEntry;
var KodeNomorRM_PA='Semua';
var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var kodeunit;
var FormLookUpsdetailTRKelompokPasien_KasirRM
var tmp_kodeunitkamar_KasirRM;
var tmp_kdspesial_KasirRM;
var tmp_nokamar_KasirRM;
var tmplunas;
var dsTRDetailHistoryList_KasirRM;
var kodepasien;
var namapasien;
var namaunit;
var tmpkddokterpengirim;
var kodepay ;
var kdkasir;
var uraianpay;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var mRecordKasirKasirRM = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );

var AddNewKasirKasirRMKasir = true;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var tanggaltransaksitampung;
var kdkasirasal_KasirRM;
var notransaksiasal_KasirRM;
var kdcustomeraa;
var notransaksi;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -5);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var gridDTItemTest;

var tmphariini = nowTglTransaksiGrid.format("d/M/Y");
var tmp3harilalu = tigaharilalu.format("d/M/Y");

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirKasirRM='Belum Posting';
var selectCountStatusLunasByr_viKasirKasirRM='Belum Lunas';
var selectCountJenTr_viKasir_RehabMedik='Transaksi Baru';
var dsTRKasir_RehabMedikList;
var dsTRDetailKasir_RehabMedikList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitRehab_Medik;
var dsPeroranganRehab_MedikRequestEntry;
var dsPeroranganRehab_MedikRequestEntryDepan;

var jenisKelaminPasien;
var rowSelectedKasir_RehabMedik;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRKasirRM;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var kelompokPasien;
var tmpparams = '';
var grListKasir_RehabMedik;
//-----------------ABULHASSAN------------20_10_2017------ variable Lis
var namaCustomer_LIS='';
var namaKelompokPasien_LIS='';
//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovaluesKasirRM = '1';
var combovaluesunit = "";
var ComboValuesKamar = "";
var dsDokterRequestEntryKasirRM;
var FormLookUpsdetailTRGantiDokter_KasirRM;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryKasirRM = new WebApp.DataStore({fields: Field});
var vkode_customer;
CurrentPage.page = getPanelKasir_RehabMedik(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;


//membuat form
function getUnitDefault(opsi){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {text:''},
        failure: function (o){
            loadMask.hide();
            ShowPesanErrorKasir_RehabMedik('Gagal mendapatkan data Unit default', 'Kasir Rehab Medik');
        },
        success: function (o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                tmpkd_unit=cst.kd_unit;
                tmpnama_unit=cst.nama_unit;
                if (opsi==="tidak"){
                    getComboDokterRehab_Medik(cst.kd_unit);
                    // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').setValue(tmpnama_unit);
                }
                
            } else{
                ShowPesanErrorKasir_RehabMedik('Gagal mendapatkan data Unit default', 'Kasir Rehab Medik');
            }
        }
    });
}
function msg_box_alasanbukatrans_RM(data)
{
    var lebar = 250;
    form_msg_box_alasanbukatrans_RM = new Ext.Window
            (
                    {
                        id: 'alasan_bukatransLAb',
                        title: 'Alasan Buka Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
												{
													xtype: 'textfield',
													//fieldLabel: 'No. Transaksi ',
													name: 'txtAlasanbukatransRM',
													id: 'txtAlasanbukatransRM',
													emptyText: 'Alasan Buka Transaksi',
													anchor: '99%',
												},
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Buka',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
																		if (Ext.getCmp('txtAlasanbukatransRM').getValue().trim()==='' || Ext.getCmp('txtAlasanbukatransRM').getValue().trim==='Alasan Buka Transaksi')
																		{
																			ShowPesanWarningKasir_RehabMedik("Isi alasan buka transaksi !", "Perhatian");
																		}else{
																			Ext.Ajax.request
																			(
																					{
																						url: baseURL + "index.php/main/functionKasirPenunjang/bukatransaksi",
																						params: {
																							no_transaksi : data.notrans_bukatrans,
																							kd_kasir	 : data.kd_kasir_bukatrans,
																							tgl_transaksi: data.tgl_transaksi_bukatrans,
																							kd_pasien	 : data.kd_pasien_bukatrans,
																							urut_masuk	 : data.urut_masuk_bukatrans,
																							kd_unit 	 : data.kd_unit_bukatrans,
																							keterangan	 : Ext.getCmp('txtAlasanbukatransRM').getValue(),
																							kd_bagian_shift : 4
																						},
																						success: function (o)
																						{
																							//	RefreshDatahistoribayar_RM(Kdtransaksi);
																						   // RefreshDataFilterKasirRMKasir();
																							//RefreshDatahistoribayar_RM('0');
																							var cst = Ext.decode(o.responseText);
																							if (cst.success === true)
																							{
																								ShowPesanInfoKasir_RehabMedik("Proses Buka Transaksi Berhasil", nmHeaderHapusData);
																								Ext.getCmp('btnBukaTransaksiRM').disable();
																								validasiJenisTrKasirRM();
																								
																							} else {
																								ShowPesanWarningKasir_RehabMedik(nmPesanHapusError, nmHeaderHapusData);
																							}
																							;
																						}
																					}
																			)
																			form_msg_box_alasanbukatrans_RM.close();
																		}
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_bukatransLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbukatrans_RM.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbukatrans_RM.show();
}
;
function getPanelKasir_RehabMedik(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
                 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT_ASAL','KD_UNIT',
                 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 'TELEPON','CUSTOMER',
                 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','HP','SJP','NO_REGISTER'];
    dsTRKasir_RehabMedikList = new WebApp.DataStore({ fields: Field });

    // getUnitDefault("awal");
    var k="tr.tgl_transaksi >='"+tmp3harilalu+"' and tr.tgl_transaksi <='"+tmphariini+"' and left(u.kd_unit,1) IN ('3') ORDER BY tr.no_transaksi desc  limit 10";
    //getUnitDefault();
    refeshpenjasKasirRM(k);
    grListKasir_RehabMedik = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            id:'PanelKasir_RehabMedik',
            store: dsTRKasir_RehabMedikList,
            anchor: '100% 50%',
            columnLines: false,
            autoScroll:true,
            border: false,
            sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasir_RehabMedik = dsTRKasir_RehabMedikList.getAt(row);
                            vCustomer=rowSelectedKasir_RehabMedik.data.CUSTOMER;
                            vNoSEP=rowSelectedKasir_RehabMedik.data.SJP;
                            vKdUnit=rowSelectedKasir_RehabMedik.data.KD_UNIT;
                            vKdDokter=rowSelectedKasir_RehabMedik.data.KD_DOKTER;
                            vUrutMasuk=rowSelectedKasir_RehabMedik.data.URUT_MASUK;
                            vTanggal=rowSelectedKasir_RehabMedik.data.MASUK;
                            vKd_Pasien=rowSelectedKasir_RehabMedik.data.KD_PASIEN;
                            vNamaDokter=rowSelectedKasir_RehabMedik.data.DOKTER;
                            vNoTransaksi=rowSelectedKasir_RehabMedik.data.NO_TRANSAKSI;
                            vKdUnitDulu=vKdUnit;
                            if (rowSelectedKasir_RehabMedik.data.NAMA_UNIT_ASAL === '')
                            {
                                vNamaUnit=rowSelectedKasir_RehabMedik.data.NAMA_UNIT;
                            }else
                            {
                                vNamaUnit=rowSelectedKasir_RehabMedik.data.NAMA_UNIT_ASAL;
                            }
                            
                            //-------22-02-2017
                            if(Ext.get('cboJenisTr_viKasir_RehabMedik').getValue()=='Transaksi Baru'){
                                Ext.getCmp('btnHapusKunjunganKasirRM').disable();
                                Ext.getCmp('btnGantiKekompokPasienKasirRM').disable();
								Ext.getCmp('btnGantiDokterKasirRM').disable();
							   Ext.getCmp('btnBukaTransaksiRM').disable();	
                            }
                            else
                            {
                                
                                //alert(vKdUnit);
                                if (rowSelectedKasir_RehabMedik.data.LUNAS==='t' || rowSelectedKasir_RehabMedik.data.LUNAS==='true' || rowSelectedKasir_RehabMedik.data.LUNAS===true){
                                    Ext.getCmp('btnGantiKekompokPasienKasirRM').disable();
                                    Ext.getCmp('btnGantiDokterKasirRM').disable();
									if (rowSelectedKasir_RehabMedik.data.CO_STATUS==='t' || rowSelectedKasir_RehabMedik.data.CO_STATUS==='true' || rowSelectedKasir_RehabMedik.data.CO_STATUS===true){
										Ext.getCmp('btnBukaTransaksiRM').enable();
									}else{
										Ext.getCmp('btnBukaTransaksiRM').disable();
									}
									Ext.getCmp('btnHapusKunjunganKasirRM').disable();
                                }else{
                                    Ext.getCmp('btnGantiKekompokPasienKasirRM').enable();
                                    Ext.getCmp('btnGantiDokterKasirRM').enable();
									Ext.getCmp('btnBukaTransaksiRM').enable();	
                                    Ext.getCmp('btnHapusKunjunganKasirRM').enable();
                                }
                                
                            }
                            
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedKasir_RehabMedik = dsTRKasir_RehabMedikList.getAt(ridx);
                    noTransaksiPilihan = rowSelectedKasir_RehabMedik.data.NO_TRANSAKSI;
                    if (rowSelectedKasir_RehabMedik !== undefined)
                    {
                        Kasir_RehabMedikLookUp(rowSelectedKasir_RehabMedik.data);
                    }
                    else
                    {
                        Kasir_RehabMedikLookUp();
                        Ext.getCmp('cboDOKTER_viKasir_RehabMedik').disable();
                        Ext.getCmp('cboGDRKasirRM').disable();
                        Ext.getCmp('cboJKKasirRM').disable();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewKasirRM',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    }, 
                    {
                        id: 'colTglRWJViewKasirRM',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TGL_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_TRANSAKSI);

                            }
                    },
                    {
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colNoMedrecViewKasirRM'
                    },
                    {
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colNamaerViewKasirRM'
                    },
                    {
                        id: 'colLocationViewKasirRM',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewKasirRM',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colKdUnitAsalViewKasirRM',
                        header: 'KD UNIT',
                        dataIndex: 'KD_UNIT_ASAL',
                        sortable: false,
                        hideable:false,
                        hidden:true,
                        menuDisabled:true,
                        width: 90
                    },{
                        id: 'colImpactViewKasirRM',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtpostingKasirRM',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                        
                    },
                    {
                        id: 'colCustomerViewKasirRM',
                        header: 'Kelompok Pasien',
                        dataIndex: 'CUSTOMER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
                    {
                        id: 'colCoSTatusViewKasirRM',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                                case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					{
                        id: 'colBukaTransaksiViewKasirRM',
                        header: 'Status Tutup Transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    }
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditKasirRM',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasir_RehabMedik != undefined)
                            {
                                console.log(rowSelectedKasir_RehabMedik.data);
                                
                                    Kasir_RehabMedikLookUp(rowSelectedKasir_RehabMedik.data);
                            }
                            else
                            {
                            ShowPesanWarningKasir_RehabMedik('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{
                        id: 'btnHapusKunjunganKasirRM',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Remove',
                        disabled: true,
                        handler: function(sm, row, rec)
                        {
                            //alert();
                            if (rowSelectedKasir_RehabMedik != undefined)
                            {
                                Ext.MessageBox.confirm('Hapus Kunjungan', "Yakin Akan Hapus Kunjungan ini ?", function (btn){
                                    if (btn === 'yes') {
                                        msg_box_alasanbataltrans_KasirRM();
                                        Ext.getCmp('txtAlasanBatalTransKasirRM').focus();
                                        console.log(rowSelectedKasir_RehabMedik);
                                        
                                    }
                                });
                            }
                            else
                            {
                            ShowPesanWarningKasir_RehabMedik('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{                             
                        id: 'btnGantiKekompokPasienKasirRM', text: 'Ganti kelompok pasien', tooltip: 'Ganti kelompok pasien', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            //Button Ganti Kelompok;
                            panelActiveDataPasien = 0;
                            KelompokPasienLookUp_KasirRM();
                        }
                    },{                             
                        id: 'btnGantiDokterKasirRM', text: 'Ganti Dokter', tooltip: 'Ganti dokter pasien', iconCls: 'gantipasien', disabled:true,
                        handler: function () {
                            //Button Ganti Dokter;
                            panelActiveDataPasien = 1;
                            loaddatastoredokterKasirRM();
                            fnDlgKasirRMPasswordDulu();
                        }
                    },{								
						id: 'btnBukaTransaksiRM', text: 'Buka Transaksi', tooltip: 'Buka Transaksi', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							
							var parameter_bukatrans = {
								notrans_bukatrans 		: rowSelectedKasir_RehabMedik.data.NO_TRANSAKSI,
								kd_kasir_bukatrans 		: rowSelectedKasir_RehabMedik.data.KD_KASIR,
								kd_pasien_bukatrans 	: rowSelectedKasir_RehabMedik.data.KD_PASIEN,
								kd_unit_bukatrans 		: rowSelectedKasir_RehabMedik.data.KD_UNIT,
								tgl_transaksi_bukatrans : rowSelectedKasir_RehabMedik.data.TGL_TRANSAKSI,
								urut_masuk_bukatrans 	: rowSelectedKasir_RehabMedik.data.URUT_MASUK
							}
							msg_box_alasanbukatrans_RM(parameter_bukatrans);
						}
					},
					//------------TAMBAH BARU 27-September-2017
					{								
						id: 'btnEditDataPasienKasirRM', text: 'Edit data Pasien', tooltip: 'Edit data Pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							console.log(rowSelectedKasir_RehabMedik.data);
							var parameter_editpasien = {
								medrec 						: rowSelectedKasir_RehabMedik.data.KD_PASIEN,
								nama 						: rowSelectedKasir_RehabMedik.data.NAMA,
								urut_masuk 					: rowSelectedKasir_RehabMedik.data.URUT_MASUK,
								jk 							: rowSelectedKasir_RehabMedik.data.JENIS_KELAMIN,
								tgl_lahir 					: rowSelectedKasir_RehabMedik.data.TGL_LAHIR,
								alamat 						: rowSelectedKasir_RehabMedik.data.ALAMAT,
								hp 							: rowSelectedKasir_RehabMedik.data.HP,
								goldarah 					: rowSelectedKasir_RehabMedik.data.GOL_DARAH
							}
							setLookUpGridDataView_viKasirRM(parameter_editpasien);
							//msg_box_alasanbukatrans_RAD(parameter_bukatrans);
						}
					},
                ]
            }
    );
    
    
    //form depan awal dan judul tab
    var FormDepanKasir_RehabMedik = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: ' Kasir Rehab Medik',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelPenJasRehabMedikDepan(),
                        grListKasir_RehabMedik
                   ],
            listeners:
            {
                'afterrender': function()
                {
                    Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                    Ext.getCmp('cboStatusLunas_viKasir_RehabMedik').disable();
                }
            }
        }
    );
    
   return FormDepanKasir_RehabMedik;

};
/* 
    PERBARUAN GANTI DOKTER  
    OLEH    : ADIT
    TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_KasirRM(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_KasirRM = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_KasirRM(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_KasirRM.show();
    KelompokPasienbaru_KasirRM();

};

function KelompokPasienbaru_KasirRM() 
{
    jeniscus_KasirRM=0;
    KelompokPasienAddNew_KasirRM = true;
    Ext.getCmp('cboKelompokpasien_KasirRM').show()
    Ext.getCmp('txtCustomer_KasirRMLama').disable();
    Ext.get('txtCustomer_KasirRMLama').dom.value=vCustomer;
    Ext.get('txtKasirRMNoSEP').dom.value = vNoSEP;
    
    RefreshDatacombo_KasirRM(jeniscus_KasirRM);
};

function getFormEntryTRKelompokPasien_KasirRM(lebar) 
{
    var pnlTRKelompokPasien_KasirRM = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_KasirRM',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputKelompokPasien_KasirRM(lebar),
                    getItemPanelButtonKelompokPasien_KasirRM(lebar)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasienKasirRM = new Ext.Panel
    (
        {
            id: 'FormDepanKelompokPasienKasirRM',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: false,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_KasirRM   
                
            ]

        }
    );

    return FormDepanKelompokPasienKasirRM
};
function getItemPanelButtonKelompokPasien_KasirRM(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        height:30,
        anchor:'100%',
        style:{'margin-top':'-1px'},
        items:
        [
            {
                layout: 'hBox',
                width:400,
                border: false,
                bodyStyle: 'padding:5px 0px 5px 5px',
                defaults: { margins: '3 3 3 3' },
                anchor: '90%',
                layoutConfig: 
                {
                    align: 'middle',
                    pack:'end'
                },
                items:
                [
                    {
                        xtype:'button',
                        text:'Simpan',
                        width:70,
                        style:{'margin-left':'0px','margin-top':'0px'},
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function()
                        {
                            if(panelActiveDataPasien == 0){
                                Datasave_Kelompokpasien_KasirRM();
                            }
                            if(panelActiveDataPasien == 1){
                                Datasave_GantiDokter_KasirRM();
                            }
                            
                        }
                    },
                    {
                        xtype:'button',
                        text:'Tutup',
                        width:70,
                        hideLabel:true,
                        id:Nci.getId(),
                        handler:function() 
                        {
                            if(panelActiveDataPasien == 0){
                                FormLookUpsdetailTRKelompokPasien_KasirRM.close();
                            }
                            
                            if(panelActiveDataPasien == 1){
                                FormLookUpsdetailTRGantiDokter_KasirRM.close();
                            }
                            
                        }
                    }
                ]
            }
        ]
    }
    return items;
};
function Datasave_GantiDokter_KasirRM(mBol) 
{   
    //console.log(Ext.get('cboDokterRequestEntry').getValue());
    if((Ext.get('cboDokterRequestEntryKasirRM').getValue() == '') || (Ext.get('cboDokterRequestEntryKasirRM').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntryKasirRM').dom.value  === 'Pilih Dokter...'))
    {
        ShowPesanWarningKasir_RehabMedik('Dokter baru harap diisi', "Informasi");
    }else{
        Ext.Ajax.request
        (
            {
                url: baseURL +  "index.php/main/functionIGD/UpdateGantiDokter", 
                params: getParamKelompokpasien_KasirRM(),
                failure: function(o)
                {
                    ShowPesanWarningKasir_RehabMedik('Simpan dokter pasien gagal', 'Gagal');
                },  
                success: function(o) 
                {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) 
                    {
                        panelActiveDataPasien = 1;
                        FormLookUpsdetailTRGantiDokter_KasirRM.close();
                        validasiJenisTrKasirRM();
                        Ext.getCmp('btnGantiKekompokPasienKasirRM').disable();    
                        Ext.getCmp('btnGantiDokterKasirRM').disable();   
						Ext.getCmp('btnBukaTransaksiRM').disable();						
                        ShowPesanInfoKasir_RehabMedik("Mengganti dokter pasien berhasil", "Success");
                    }else 
                    {
                        ShowPesanWarningKasir_RehabMedik('Simpan dokter pasien gagal', 'Gagal');
                    };
                }
            }
        ) 
    }
    
};
function ValidasiEntryUpdateKelompokPasien_KasirRM(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('kelPasien_KasirRM').getValue() == '') || (Ext.get('kelPasien_KasirRM').dom.value  === undefined ))
    {
        if (Ext.get('kelPasien_KasirRM').getValue() == '' && mBolHapus === true) 
        {
            ShowPesanWarningKasir_RehabMedik(nmGetValidasiKosong('Kelompok Pasien'), modul);
            x = 0;
        }
    };
    return x;
};

function ValidasiEntryUpdateGantiDokter_KasirRM(modul,mBolHapus)
{
    var x = 1;
    
    if((Ext.get('cboDokterRequestEntryKasirRM').getValue() == '') || (Ext.get('cboDokterRequestEntryKasirRM').dom.value  === undefined ))
    {
        ShowPesanWarningKasir_RehabMedik(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
        x = 0;
    };
    return x;
};
function getParamKelompokpasien_KasirRM(combo) 
{
    var params;
    if(panelActiveDataPasien == 0){
        params = {
            KDCustomer  : selectKdCustomer,
            KDNoSJP     : Ext.get('txtKasirRMNoSEP').getValue(),
            KDNoAskes   : Ext.get('txtKasirRMNoAskes').getValue(),
            KdPasien    : vKd_Pasien,
            TglMasuk    : vTanggal,
            KdUnit      : vKdUnit,
            UrutMasuk   : vUrutMasuk,
            KdDokter    : vKdDokter,
            alasan      : combo,
        }
    }else if(panelActiveDataPasien == 1 || panelActiveDataPasien == 2){
        params = {
            KdPasien    : vKd_Pasien,
            TglMasuk    : vTanggal,
            KdUnit      : vKdUnit,
            UrutMasuk   : vUrutMasuk,
            KdDokter    : vKdDokter,
            NoTransaksi : vNoTransaksi,
            KdKasir     : vKdKasir,
            KdUnitDulu  : vKdUnitDulu,
        }
    }else if(panelActiveDataPasien == 'undefined'){
        params = { data : "null", }
    }else{
        params = { data : "null", }
    }
    return params
};
function Datasave_Kelompokpasien_KasirRM(mBol) 
{   
    if (ValidasiEntryUpdateKelompokPasien_KasirRM(nmHeaderSimpanData,false) == 1 )
    {           
		var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
				if (combo!='') {
					Ext.Ajax.request
					(
						{
							url: baseURL +  "index.php/main/functionIGD/UpdateGantiKelompok",   
							params: getParamKelompokpasien_KasirRM(combo),
							failure: function(o)
							{
								ShowPesanWarningKasir_RehabMedik('Simpan kelompok pasien gagal', 'Gagal');
							},  
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									panelActiveDataPasien = 1;
									FormLookUpsdetailTRKelompokPasien_KasirRM.close();
									validasiJenisTrKasirRM();
									Ext.getCmp('btnGantiKekompokPasienKasirRM').disable();    
									Ext.getCmp('btnGantiDokterKasirRM').disable();    
									Ext.getCmp('btnBukaTransaksiRM').disable();	
									ShowPesanInfoKasir_RehabMedik("Mengganti kelompok pasien berhasil", "Success");
								}else 
								{
									panelActiveDataPasien = 1;
									ShowPesanWarningKasir_RehabMedik('Simpan kelompok pasien gagal', 'Gagal');
								};
							}
						}
					 )
				}else{
                    ShowPesanWarningKasir_RehabMedik('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
			}
		});
            
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
    
};
function getItemPanelInputKelompokPasien_KasirRM(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                KasirRMelWidth:100,
                layout: 'form',
                border: false,
                items:
                [
                    getKelompokpasienlama_KasirRM(lebar), 
                    getItemPanelNoTransksiKelompokPasien_KasirRM(lebar)   ,
                    
                ]
            }
        ]
    };
    return items;
};
function getKelompokpasienlama_KasirRM(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: false,
                items:
                [{   
                        xtype: 'tbspacer',
                        height: 2
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Kelompok Pasien Asal',
                            name: 'txtCustomer_KasirRMLama',
                            id: 'txtCustomer_KasirRMLama',
                            labelWidth:130,
                            editable: false,
                            width: 100,
                            anchor: '95%'
                         }
                    ]
            }
            
        ]
    }
    return items;
};
function getItemPanelNoTransksiKelompokPasien_KasirRM(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: false,
                items:
                [{   
                        xtype: 'tbspacer',
                        height:3
                    },{ 
                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien_KasirRM',
                        editable: false,
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                 {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {jeniscus_KasirRM='0'
                                            //Ext.getCmp('txtKasirRMNoSEP').disable();
                                            //Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Perusahaan')
                                        {jeniscus_KasirRM='1';
                                            //Ext.getCmp('txtKasirRMNoSEP').disable();
                                            //Ext.getCmp('txtRWJNoAskes').disable();
                                            }
                                        else if(b.data.displayText =='Asuransi')
                                        {jeniscus_KasirRM='2';
                                            //Ext.getCmp('txtKasirRMNoSEP').enable();
                                            //Ext.getCmp('txtRWJNoAskes').enable();
                                        }
                                        
                                        RefreshDatacombo_KasirRM(jeniscus_KasirRM);
                                    }

                                }
                        },{
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                                mComboKelompokpasien_KasirRM()
                            ]
                        },{
                            xtype: 'textfield',
                            fieldLabel:'No SEP  ',
                            name: 'txtKasirRMNoSEP',
                            id: 'txtKasirRMNoSEP',
                            width: 100,
                            anchor: '99%'
                         }, {
                             xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtKasirRMNoAskes',
                            id: 'txtKasirRMNoAskes',
                            width: 100,
                            anchor: '99%'
                         }
                                    
                ]
            }
            
        ]
    }
    return items;
};

function mComboKelompokpasien_KasirRM()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_KasirRM = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    if (jeniscus_KasirRM===undefined || jeniscus_KasirRM==='')
    {
        jeniscus_KasirRM=0;
    }
    ds_customer_viPJ_KasirRM.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_KasirRM +'~'
            }
        }
    )
    var cboKelompokpasien_KasirRM = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien_KasirRM',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viPJ_KasirRM,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien_KasirRM;
};


function RefreshDatacombo_KasirRM(jeniscus_KasirRM) 
{
    var kosong;
    ds_customer_viPJ_KasirRM.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_KasirRM +'~ '
            }
        }
    )
    
    return ds_customer_viPJ_KasirRM;
};

/* =============================================================================================================================================== */
/* 
    PERBARUAN GANTI DOKTER  
    OLEH    : ADIT
    TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_KasirRM(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_KasirRM = new Ext.Window
    (
        {
            id: 'idGantiDokterKasirRM',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_KasirRM(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_KasirRM.show();
    GantiPasien_KasirRM();

};

function GantiPasien_KasirRM() 
{
    //RefreshDatacombo_rwj(jeniscus_KasirRM);
};

function getFormEntryTRGantiDokter_KasirRM(lebar) 
{
    var pnlTRKelompokPasien_KasirRM = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiKasirRM',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
                    getItemPanelInputGantiDokter_KasirRM(lebar),
                    getItemPanelButtonKelompokPasien_KasirRM(lebar)
            ],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokterKasirRM = new Ext.Panel
    (
        {
            id: 'FormDepanGantiDokterKasirRM',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: false,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: [pnlTRKelompokPasien_KasirRM
                
            ]

        }
    );

    return FormDepanGantiDokterKasirRM
};

function getItemPanelInputGantiDokter_KasirRM(lebar) 
{
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding:10px 10px 10px 0px',
        border:false,
        height:170,
        items:
        [
            {
                columnWidth: .9,
                width: lebar -35,
                labelWidth:100,
                layout: 'form',
                border: false,
                items:
                [   
                    {
                        xtype: 'textfield',
                        fieldLabel:  'Unit Asal ',
                        name: 'txtUnitAsal_DataPasienKasirRM',
                        id: 'txtUnitAsal_DataPasienKasirRM',
                        value:vNamaUnit,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },{
                        xtype: 'textfield',
                        fieldLabel: 'Dokter Asal ',
                        name: 'txtDokterAsal_DataPasienKasirRM',
                        id: 'txtDokterAsal_DataPasienKasirRM',
                        value:vNamaDokter,
                        readOnly:true,
                        width: 100,
                        anchor: '99%'
                    },
                    mComboDokterGantiEntryKasirRM()
                ]
            }
        ]
    };
    return items;
};

function loaddatastoredokterKasirRM(){
    dsDokterRequestEntryKasirRM.load({
         params :{
            Skip    : 0,
            Take    : 1000,
            Sort    : 'nama',
            Sortdir : 'ASC',
            target  : 'ViewDokterPenunjang',
            param   : 'kd_unit=~'+ vKdUnit+ '~'
        }
    });
}
function mComboDokterGantiEntryKasirRM(){ 
    /* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryKasirRM = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntryKasirRM = new Ext.form.ComboBox({
        id: 'cboDokterRequestEntryKasirRM',
        typeAhead: true,
        triggerAction: 'all',
        name:'txtdokter',
        lazyRender: true,
        mode: 'local',
        selectOnFocus:true,
        forceSelection: true,
        emptyText:'Pilih Dokter...',
        fieldLabel: 'Dokter Baru',
        align: 'Right',
        store: dsDokterRequestEntryKasirRM,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        anchor:'100%',
        listeners:{
            'select': function(a,b,c){
                vKdDokter = b.data.KD_DOKTER;
            },
        }
    });
    return cboDokterGantiEntryKasirRM;
};
function mComboStatusBayar_viKasir_RehabMedik()
{
  var cboStatus_viKasir_RehabMedik = new Ext.form.ComboBox
    (
        {
                    id:'cboStatus_viKasir_RehabMedik',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirKasirRM,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirKasirRM=b.data.displayText ;
                            }
                    }
        }
    );
    return cboStatus_viKasir_RehabMedik;
};
function mComboStatusLunas_viKasir_RehabMedik()
{
  var cboStatusLunas_viKasir_RehabMedik = new Ext.form.ComboBox
    (
        {
                    id:'cboStatusLunas_viKasir_RehabMedik',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirKasirRM,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirKasirRM=b.data.displayText ;
                                    validasiJenisTrKasirRM();
                            }
                    }
        }
    );
    return cboStatusLunas_viKasir_RehabMedik;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viKasir_RehabMedik()
{
  var cboJenisTr_viKasir_RehabMedik = new Ext.form.ComboBox
    (
        {
                    id:'cboJenisTr_viKasir_RehabMedik',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viKasir_RehabMedik,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                combovalues=b.data.displayText;
                                if(b.data.Id==1){
                                    /* Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').setValue(null);
                                    Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').disable(); */
                                    //Ext.getCmp('cboKeteranganHasilRehab_MedikFilterDepan').disable();
                                    tmppasienbarulama = 'Baru';
                                }else{
                                    /* Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').enable();
                                    Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').setValue(combovaluesunittujuan); */
                                    //Ext.getCmp('cboKeteranganHasilRehab_MedikFilterDepan').enable();
                                    tmppasienbarulama = 'Lama';
                                }
                                
                                validasiJenisTrKasirRM();

                            }
                    }
        }
    );
    return cboJenisTr_viKasir_RehabMedik;
};

var tmppasienbarulama = 'Baru';


function validasiJenisTrKasirRM(){
    var kriteria = "";
    var tmpkriteriaunittujuancrwi='';
    var strkriteria=getCriteriaFilter_viDaftar();
    var tmpKodeNomorRehabMedik="";
    var tmpKodeCustomer="";
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viKasir_RehabMedik').enable();
    
        if(radiovaluesKasirRM === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaKasirRM = "";
                }else
                {
                    tmpkreteriaKasirRM = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirRehab_Medik').getValue() + "'";
                }
            }else {
                tmpkreteriaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianamaKasirRM = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else
            {
                tmpkriterianamaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrecKasirRM = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else
            {
                tmpkriteriamedrecKasirRM = "";
            }
           /*  if(Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != null && Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            } */
            /* if(selectCountStatusByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusByr_viKasirKasirRM === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirKasirRM === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusLunasByr_viKasirKasirRM === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirKasirRM === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorRM_PA=='Semua'){
                tmpKodeNomorRehabMedik="";
            }else if(KodeNomorRM_PA=='MF'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MF' ";
            }else if(KodeNomorRM_PA=='MT'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MT' ";
            }else if(KodeNomorRM_PA=='MI'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MI' ";
            }else if(KodeNomorRM_PA=='MS'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MS' ";
            }else if(KodeNomorRM_PA=='K'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            
            kriteria = " kd_bagian = 74 and kd_unit in ('"+tmpkd_unit+"') and left(kd_unit_asal, 1) in('3') "+ tmpkriteriaunittujuancrwi +""+ tmpkreteriaKasirRM +" "+tmpKodeCustomer+" "+ tmpkriterianamaKasirRM +" "+ tmpkriteriamedrecKasirRM +"  "+tmplunas+" "+strkriteria+" "+tmpKodeNomorRehabMedik+" and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjasKasirRM(kriteria, tmpunit);
            
        }else if (radiovaluesKasirRM === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            /* if(Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != null && Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            } */
            /* 
            if(selectCountStatusByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusByr_viKasirKasirRM === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirKasirRM === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusLunasByr_viKasirKasirRM === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirKasirRM === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorRM_PA=='Semua'){
                tmpKodeNomorRehabMedik="";
            }else if(KodeNomorRM_PA=='MF'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MF' ";
            }else if(KodeNomorRM_PA=='MT'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MT' ";
            }else if(KodeNomorRM_PA=='MI'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MI' ";
            }else if(KodeNomorRM_PA=='MS'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MS' ";
            }else if(KodeNomorRM_PA=='K'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 74  and kd_unit in ('"+tmpkd_unit+"') "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" "+tmpKodeCustomer+" "+tmplunas+" "+strkriteria+"  "+tmpKodeNomorRehabMedik+"  and left(kd_pasien, 2) not in ('LB')  and left(kd_unit_asal, 1) = '1' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjasKasirRM(kriteria, tmpunit);
        }else if (radiovaluesKasirRM === '3')
        {
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            /* if(Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != null && Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            } */
            /* if(selectCountStatusByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusByr_viKasirKasirRM === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirKasirRM === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusLunasByr_viKasirKasirRM === "Lunas")
                {
                   tmplunas = " and lunas='t' ";
                }else if(selectCountStatusLunasByr_viKasirKasirRM === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorRM_PA=='Semua'){
                tmpKodeNomorRehabMedik="";
            }else if(KodeNomorRM_PA=='MF'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MF' ";
            }else if(KodeNomorRM_PA=='MT'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MT' ";
            }else if(KodeNomorRM_PA=='MI'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MI' ";
            }else if(KodeNomorRM_PA=='MS'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MS' ";
            }else if(KodeNomorRM_PA=='K'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 74  and kd_unit in ('"+tmpkd_unit+"') "+ tmpkriterianama +""+ tmpkriteriamedrec +" "+tmpKodeCustomer+" "+ tmpkriteriaunittujuancrwi +"  "+tmpKodeNomorRehabMedik+"    and left(kd_pasien, 2) = 'LB' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+tmplunas+" ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLabKunjunganLangsung';
            loadpenjasKasirRM(kriteria, tmpunit);
        
        }else if (radiovaluesKasirRM === '4')
        {
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
            /* if(Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != null && Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() != ''){
                tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitKasirRMs_viKasir_RehabMedik').getValue() + "'";
            }else{
                tmpkriteriaunittujuancrwi="";
            } */
            /* if(selectCountStatusByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusByr_viKasirKasirRM === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirKasirRM === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
            if(selectCountStatusLunasByr_viKasirKasirRM !== "")
            {
                if(selectCountStatusLunasByr_viKasirKasirRM === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirKasirRM === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            if(KodeNomorRM_PA=='Semua'){
                tmpKodeNomorRehabMedik="";
            }else if(KodeNomorRM_PA=='MF'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MF' ";
            }else if(KodeNomorRM_PA=='MT'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MT' ";
            }else if(KodeNomorRM_PA=='MI'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MI' ";
            }else if(KodeNomorRM_PA=='MS'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='MS' ";
            }else if(KodeNomorRM_PA=='K'){
                tmpKodeNomorRehabMedik=" and left(no_register,"+KodeNomorRM_PA.length+")='K' ";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=0";
                }else{
                    tmpKodeCustomer="and jenis_cust=0 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=1";
                }else{
                tmpKodeCustomer="and jenis_cust=1 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and jenis_cust=2";
                }else{
                    tmpKodeCustomer="and jenis_cust=2 and kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            kriteria = " kd_bagian = 74  and kd_unit in ('"+tmpkd_unit+"') and left(kd_unit_asal, 1) in('2') "+tmpkriteriamedrec+" and left(kd_pasien, 2) not in ('LB')  "+tmpkriteriaunittujuancrwi+" "+tmplunas+"  "+tmpKodeNomorRehabMedik+"  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+tmpKodeCustomer+" ORDER BY no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjasKasirRM(kriteria, tmpunit);
        }else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 2  and kd_unit in ('"+tmpkd_unit+"') and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasLab';
            loadpenjasKasirRM(kriteria, tmpunit);
        }
        
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viKasir_RehabMedik').disable();
        if(radiovaluesKasirRM === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaKasirRM = "";
                }else
                {
                tmpkreteriaKasirRM = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRehab_Medik').getValue() + "'";
                }
            }else {
                tmpkreteriaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianamaKasirRM = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else{
                tmpkriterianamaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrecKasirRM = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else{
                tmpkriteriamedrecKasirRM = "";
            }
        if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
            tmpKodeCustomer="";
        }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
            if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=0";
            }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
            if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=1";
            }else{
            tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
            if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                tmpKodeCustomer="and kontraktor.jenis_cust=2";
            }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
            }
            
        }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+tmpKodeCustomer+" "+ tmpkreteriaKasirRM +" "+ tmpkriterianamaKasirRM +" "+ tmpkriteriamedrecKasirRM +" and left(u.kd_unit,1) IN ('3') ORDER BY   tr.no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            //loadpenjasKasirRM(tmpparams, tmpunit);
            refeshpenjasKasirRM(tmpparams);
        }
        else if (radiovaluesKasirRM === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=0";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=1";
                }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=2";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            if (tmpkd_unit==='44' || tmpkd_unit===44){
                 tmpparams = " nginap.tgl_inap >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and nginap.tgl_inap <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and u.kd_unit IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047') ORDER BY   tr.no_transaksi desc limit 10";
            }else if (tmpkd_unit==='45' || tmpkd_unit===45){
                 tmpparams = " nginap.tgl_inap >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and nginap.tgl_inap <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and (left(u.kd_unit,1) ='1' and u.kd_unit NOT IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047')) ORDER BY   tr.no_transaksi desc limit 10";
                 // tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and ( u.kd_unit NOT IN ('10030','10031','10032','10033','10034','10035','10036','10037','10038','10039','10040','10041','10042','10043','10044','10045','10046','10047')) ORDER BY   tr.no_transaksi desc limit 10";
            }else {
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkriteriakamar +" "+tmpKodeCustomer+" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1)='1' ORDER BY   tr.no_transaksi desc limit 10";
            }
            
             tmpunit = 'ViewPenJasLab';
             //loadpenjasKasirRM(tmpparams, tmpunit);
             refeshpenjasKasirRM(tmpparams);
             
        }else if (radiovaluesKasirRM === '3')
        {
            Kasir_RehabMedikLookUp();
            Ext.getCmp('txtnotlpKasirRM').setReadOnly(false);
        }else if (radiovaluesKasirRM === '4')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaKasirRM = "";
                }else
                {
                tmpkreteriaKasirRM = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirRehab_Medik').getValue() + "'";
                }
            }else {
                tmpkreteriaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNamaPasienKasir_RehabMedik').getValue())
            {
               tmpkriterianamaKasirRM = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienKasir_RehabMedik').dom.value + "%')";
            }else{
                tmpkriterianamaKasirRM = "";
            }
            
            if (Ext.getCmp('txtNoMedrecKasir_RehabMedik').getValue())
            {
               tmpkriteriamedrecKasirRM = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecKasir_RehabMedik').dom.value + "'";
            }else{
                tmpkriteriamedrecKasirRM = "";
            }
            if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Semua' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 0){
                tmpKodeCustomer="";
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perseorangan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 1){
                if (Ext.getCmp('cboPerseoranganRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=0";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=0 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Perusahaan' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 2){
                if (Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=1";
                }else{
                tmpKodeCustomer="and kontraktor.jenis_cust=1 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }else if (Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()==='Asuransi' || Ext.getCmp('cboKelPasienDepanRehab_Medik').getValue()=== 3){
                if (Ext.getCmp('cboAsuransiRehabMedikDepan').getValue()==='Semua'){
                    tmpKodeCustomer="and kontraktor.jenis_cust=2";
                }else{
                    tmpKodeCustomer="and kontraktor.jenis_cust=2 and kunjungan.kd_customer='"+kodeCustomerFilterDepan+"' ";
                }
                
            }
            //alert(tmpparams);
            if (tmpkd_unit==='44' || tmpkd_unit===44){
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkreteriaKasirRM +" "+tmpKodeCustomer+" "+ tmpkriterianamaKasirRM +" "+ tmpkriteriamedrecKasirRM +" and u.kd_unit IN ('231','246','249','251','259','260','261','262','263','264','265','266','267','268','269','270','271','272','273','274','275','276') ORDER BY  tr.no_transaksi desc limit 10";
            }else if (tmpkd_unit==='45' || tmpkd_unit===45){
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkreteriaKasirRM +" "+tmpKodeCustomer+" "+ tmpkriterianamaKasirRM +" "+ tmpkriteriamedrecKasirRM +" and (left(u.kd_unit,1) ='2' and u.kd_unit NOT IN ('231','246','249','251','254','259','260','261','262','263','264','265','266','267','268','269','270','271','272','273','274','275','276')) ORDER BY  tr.no_transaksi desc limit 10";
            }else{
                tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() + "' "+ tmpkreteriaKasirRM +" "+tmpKodeCustomer+" "+ tmpkriterianamaKasirRM +" "+ tmpkriteriamedrecKasirRM +" and left(u.kd_unit,1) ='2' ORDER BY  tr.no_transaksi desc limit 10";
            }
            
            tmpunit = 'ViewPenJasLab';
            //loadpenjasKasirRM(tmpparams, tmpunit);
            refeshpenjasKasirRM(tmpparams);
        }
        
    }
}

//VALIDASI COMBO UNIT/POLI
function getDataCariUnitKasir_RehabMedik(kriteria)
{
    if (kriteria===undefined)
    {
        kriteria="kd_bagian=3 and parent<>'0'";
    }
    dsunit_viKasirRehab_Medik.load
    (
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
    );
    return dsunit_viKasirRehab_Medik;
}
function mComboUnit_viKasirKasirRM() 
{
    
    var Field = ['KD_UNIT','NAMA_UNIT'];
    
    dsunit_viKasirRehab_Medik = new WebApp.DataStore({ fields: Field });
    getDataCariUnitKasir_RehabMedik();
    var cboUNIT_viKasirRehab_Medik = new Ext.form.ComboBox
    (
            {
                id: 'cboUNIT_viKasirRehab_Medik',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
                emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 100,
                store: dsunit_viKasirRehab_Medik,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
                listeners:
                    {

                        'select': function(a, b, c) 
                            {                          
                                //RefreshDataFilterPenJasRad();
                                combovaluesunit=b.data.valueField;
                                validasiJenisTrKasirRM();
                            }

                    }
            }
    );
    
    return cboUNIT_viKasirRehab_Medik;
};

function mcomboKamarSpesialKasirRM()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

    ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialJasRadRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialJasRadRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'150',
            listeners:
                {
                    'select': function(a, b, c)
                    {
                        ComboValuesKamar=b.data.valueField;
                        validasiJenisTrKasirRM();    
                    },
                    'render': function(c)
                    {
                        
                    }


        }
        }
    )

    return cboRujukanKamarSpesialJasRadRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI KasirRMORATORIUM
function Kasir_RehabMedikLookUp(rowdata) 
{
    var lebar = 900;
    FormLookUpsdetailTRKasirRM = new Ext.Window
    (
        {
            id: 'gridKasir_RehabMedik',
            title: ' Kasir Rehab Medik',
            closeAction: 'destroy',
            width: lebar,
            height: 580,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasir_RehabMedik(lebar,rowdata),
            listeners:
            {
                 activate: function()
                {
                    if (rowdata === undefined){
                        Ext.getCmp('txtNamaPasienKasirRM').focus(false,100);
                        Ext.getCmp('txtNoMedrecKasirRM').setReadOnly(true);
                        Ext.getCmp('txtNamaUnitKasirRM').setReadOnly(true);
                        Ext.getCmp('txtNamaPasienKasirRM').setReadOnly(false);
                        Ext.getCmp('txtAlamatKasirRM').setReadOnly(false);
                        Ext.getCmp('dtpTtlKasirRM').setReadOnly(false);
                        Ext.getCmp('txtCustomerLamaHide').hide();
                    }else{
                        Ext.getCmp('cboDOKTER_viKasir_RehabMedik').focus(false,1000);
                    }
                    
                    
                }
            }
        }
    );

    FormLookUpsdetailTRKasirRM.show();
    
    if (rowdata === undefined) 
    {
        KasirRMAddNew();
    }
    else 
    {
        TRKasirRMInit(rowdata);
    }

};
function load_data_printer_kasirKasirRM(param)
{

    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionRM/getPrinter",
        params:{
            command: param
        } ,
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            //cbopasienorder_mng_apotek.store.removeAll();
                var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsprinter_kasirKasirRM.recordType;
                var o=cst['listData'][i];
                
                recs.push(new recType(o));
                dsprinter_kasirKasirRM.add(recs);
            }
        }
    });
}
/* function mCombo_printer_kasirKasirRM(){ 
    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];
    dsprinter_kasirKasirRM = new WebApp.DataStore({ fields: Field });
    load_data_printer_kasirKasirRM();
    var cbo_printer_kasirKasirRM= new Ext.form.ComboBox
    (
        {
            id: 'cbopasienorder_printer_kasirKasirRM',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            hidden :true,
            mode            : 'local',
            emptyText: 'Pilih Printer',
            fieldLabel:  '',
            align: 'Right',
            width: 200,
            store: dsprinter_kasirKasirRM,
            valueField: 'name',
            displayField: 'name',
            //hideTrigger       : true,
            listeners:
            {
                                
            }
        }
    );return cbo_printer_kasirKasirRM;
}; */
function PenjasLookUpKasirRM(rowdata)
{
    var lebar = 700;
    FormLookUpsdetailTRPenjasKasirRM = new Ext.Window
            (
                    {
                        id: 'gridPenjasKasirRM',
                        title: ' Kasir Rehab Medik',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        
                        items: getFormEntryPenjasBayarKasirRM(lebar),
                        enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRPenjasKasirRM.show();
    if (rowdata == undefined)
    {
        TRPenjasBayarKasirRMInit(rowdata);
    } else
    {
        TRPenjasBayarKasirRMInit(rowdata)
    }

}

function getFormEntryCatatanHasilRehabMedik(lebar,catatannya)
{
    var pnlTRCatatanHasilRehabMedik = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRCatatanHasilRehabMedik',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 400,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                },
                        //items: [getItemPanelInputCatatanHasilKasirRM(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    




    var FormDepan2CatatanHasilRM = new Ext.Panel
            (
                    {
                        id: 'FormDepan2CatatanHasilRM',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: false,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 100,
                        shadhow: true,
                        items: [pnlTRCatatanHasilRehabMedik,
                        ],tbar:[
							{
								xtype: 'textarea',
								fieldLabel:'Catatan Hasil  ',
								name: 'txtCatatanHasilKasirRMKasir',
								id: 'txtCatatanHasilKasirRMKasir',
								width:680,
								height:380,
								value: catatannya,
								readOnly:false,
								anchor: '99%'
							}
						]
                    }
            );

    return FormDepan2CatatanHasilRM
}
function FormCatatanHasilRehabMedik(catatannya)
{
    var lebar = 700;
    FormLookUpCatatanHasilRehabMedik = new Ext.Window
            (
                    {
                        id: 'CatatanHasilRehabMedik',
                        title: ' Catatan Hasil',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 440,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        
                        items: getFormEntryCatatanHasilRehabMedik(lebar,catatannya),
                        enableKeyEvents:true,
						tbar:
						[
							{
									text: 'Simpan',
									id: 'btnSimpanCatatanHasilKasirRM',
									tooltip: nmSimpan,
									iconCls: 'save',
									handler: function()
									{
										    Ext.Ajax.request({
											url: baseURL + "index.php/rehab_medik/functionKasirRM/simpanCatatanHasil",
											params: {
												kd_pasien: vKd_Pasien,
												kd_unit:vKdUnit,
												tgl_masuk:vTanggal,
												urut_masuk:vUrutMasuk,
												catatan: Ext.getCmp('txtCatatanHasilKasirRMKasir').getValue()
											},
											failure: function (o)
											{
												var cst = Ext.decode(o.responseText);
												ShowPesanWarningKasir_RehabMedik('Proses penyimpanan catatan GAGAL', "Simpan Catatan");
											},
											success: function (o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true){
													ShowPesanInfoKasir_RehabMedik('Proses penyimpanan catatan BERHASIL', "Simpan Catatan");
												}else{
													ShowPesanWarningKasir_RehabMedik('Proses penyimpanan catatan GAGAL', "Simpan Catatan");
												}
												
											}

										});             
									}
							},  
							  
						],
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                }
                    }
            );

    FormLookUpCatatanHasilRehabMedik.show();
    /* if (rowdata == undefined)
    {
        TRPenjasBayarKasirRMInit(rowdata);
    } else
    {
        TRPenjasBayarKasirRMInit(rowdata)
    } */

}
;
function mEnabledKasirKasirRMCM(mBol)
{
   /*  Ext.get('btnLookupKasirKasirRM').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirKasirRM').dom.disabled = mBol; */
}
;
function PenjasBayarKasirRMAddNew()
{
    AddNewKasirKasirRMKasir = true;
    Ext.get('txtNoTransaksiKasirKasirRMKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirKasirRMKasir').dom.value = ''
    rowSelectedKasir_RehabMedik = undefined;
    dsTRDetailKasir_RehabMedikList.removeAll();
    mEnabledKasirKasirRMCM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
    console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarKasirRMInit(rowdata)
{
    AddNewKasirKasirRMKasir = false;
    
    
    console.log(rowdata);
    
    
    
    if (rowdata===undefined)
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganKasirRM').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecKasirRM').getValue();
        Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienKasirRM').getValue();
        tanggaltransaksitampung = Ext.getCmp('dtpKunjunganKasirRM').getValue();
    }
    else
    {
        Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganKasirRM').getValue().format('d/M/Y');
        Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
        Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
        tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
    }
    
    // take the displayField value 
    var notransnya;
    if (Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue()==='' || Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue()===undefined){
        notransnya=rowdata.NO_TRANSAKSI;
    }
    else
    {
        notransnya=Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue();
    }
    var modul='';
    if(radiovaluesKasirRM == 1){
        modul='igd';
    } else if(radiovaluesKasirRM == 2){
        modul='rwi';
    } else if(radiovaluesKasirRM == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.get('txtNoTransaksiKasirKasirRMKasir').dom.value = notransnya;
    RefreshDataKasirKasirRMKasirDetail(notransnya,kd_kasir_KasirRM);
    Ext.Ajax.request({
        url: baseURL + "index.php/rehab_medik/functionKasirRM/cekPembayaran",
        params: {
            notrans: notransnya,
            Modul:modul,
            kdkasir: kd_kasir_KasirRM
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
            notransaksi=cst.ListDataObj[0].no_transaksi;
            kodeunit=cst.ListDataObj[0].kd_unit;
            vkd_unit = cst.ListDataObj[0].kd_unit;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            //alert(kdkasir);
            notransaksiasal_KasirRM = cst.ListDataObj[0].no_transaksi_asal;
            kdkasirasal_KasirRM = cst.ListDataObj[0].kd_kasir_asal;
            Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
            Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
            //Ext.get('txtNomorRehab_Medik').dom.value =cst.ListDataObj[0].no_register; 
            vflag = cst.ListDataObj[0].flag;
            tapungkd_pay = cst.ListDataObj[0].kd_pay;
            vkode_customer =  cst.ListDataObj[0].kd_customer;
            
        }

    });
    
    
    tampungtypedata = 0;
    
    jenispay = 1;
    
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });
    


}
;
function getItemPanelNoTransksiKasirRMKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirKasirRMKasir',
                                                id: 'txtNoTransaksiKasirKasirRMKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadRehab_Medik();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadRehab_Medik();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                //readOnly: true,
                                                value: now,
                                                anchor: '100%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadRehab_Medik();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadRehab_Medik();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
                                        hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProdukKasirRM();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                    keydown:function(text,e)
                                    {
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            printbillRadRehab_Medik();
                                        }else if(e.keyCode == 123){
                                            e.preventDefault();
                                            printkwitansiRadRehab_Medik();
                                        }
                                    }
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProdukKasirRM()
{
    
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirKasirRMKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirKasirRMKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirKasirRM').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirKasirRMKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirKasirRM').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirKasirRMKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirKasirRM').dom.value = formatCurrency(recordterakhir);
        }
    }
    
    if (Ext.getCmp('txtJumlah2EditData_viKasirKasirRM').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirKasirRM').getValue()===0 )
    {
        Ext.getCmp('btnSimpanKasirKasirRM').disable();
		tombol_bayar='disable';
        Ext.getCmp('btnTransferKasirKasirRM').disable();
    }
    else
    {
        Ext.getCmp('btnSimpanKasirKasirRM').enable();
		tombol_bayar='enable';
        Ext.getCmp('btnTransferKasirKasirRM').enable();
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirKasirRM').getValue();
    return bayar;
}
;


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
                                    keydown:function(text,e)
                                    {
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            printbillRadRehab_Medik();
                                        }else if(e.keyCode == 123){
                                            e.preventDefault();
                                            printkwitansiRadRehab_Medik();
                                        }
                                    }
                                }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasirKasirRM(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadRehab_Medik();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadRehab_Medik();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                enableKeyEvents:true,
                                                listeners:
                                                        {
                                                            keydown:function(text,e){
                                                                if(e.keyCode == 122){
                                                                    e.preventDefault();
                                                                    printbillRadRehab_Medik();
                                                                }else if(e.keyCode == 123){
                                                                    e.preventDefault();
                                                                    printkwitansiRadRehab_Medik();
                                                                }
                                                            }
                                                        },
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasirKasirRM(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 110,
                enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                },
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiKasirRMKasir(lebar),
                                            getItemPanelmedreckasirKasirRM(lebar), getItemPanelUnitKasirKasirRM(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasirKasirRM(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function RefreshDataKasirKasirRMKasirDetail(no_transaksi,kd_kasir)
{
    var strKriteriaKasirKasirRM = '';
     if (kd_kasir !== undefined) {
        strKriteriaKasirKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaKasirKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
    //strKriteriaKasirKasirRM = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirKasirRMKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayarLAB',
                                    param: strKriteriaKasirKasirRM
                                }
                    }
            );
    return dsTRDetailKasirKasirRMKasirList;
}
;
function GetDTLTRKasirKasirRMGrid()
{
    var fldDetailKasirRM = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG','TAG'];

    dsTRDetailKasirKasirRMKasirList = new WebApp.DataStore({fields: fldDetailKasirRM})
    //RefreshDataKasirKasirRMKasirDetail();
    gridDTLTRKasirKasirRM = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Pembayaran',
                        stripeRows: true,
                        id: 'gridDTLTRKasirKasirRM',
                        store: dsTRDetailKasirKasirRMKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRehab_MedikColumModel()
                    }
            );

    return gridDTLTRKasirKasirRM;
}
;

function TRKasirRehab_MedikColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirKasirRM',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },{
                            id: 'coltagKasirRehab_Medik',
                            header: 'Tag',
                            dataIndex: 'TAG',
                            width:30,
                            xtype:'checkcolumn',
							checked:true
                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirKasirRM',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirKasirRM',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirKasirRM',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirKasirRM',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirKasirRM',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangKasirRM',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukKasirRM();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirKasirRM',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiKasirRM',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukKasirRM();
                                
                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirKasirRM',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountKasirRM',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirKasirRM()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirKasirRMKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProdukKasirRM(),
                List: getArrDetailTrKasirKasirRM(),
                JmlField: mRecordKasirKasirRM.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksiKasirRM(),
                Hapus: 1,
                Ubah: 0,
                TglBayar : Ext.get('dtpTanggalDetransaksi').dom.value,
            };
    return params
}
;

function getArrDetailTrKasirKasirRM()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirKasirRMKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirKasirRMKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirKasirRMKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirKasirRMKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirKasirRMKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirKasirRMKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;

function ValidasiEntryCMKasirKasirRM(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirKasirRMKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirKasirRMKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirKasirRMKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasir_RehabMedik(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasir_RehabMedik(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasir_RehabMedik('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasir_RehabMedik(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirKasirRMKasirList.getCount() === 0)
        {
            ShowPesanWarningKasir_RehabMedik('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirKasirRMKasir(mBol)
{
    if (ValidasiEntryCMKasirKasirRM(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/rehab_medik/functionKasirRM/savepembayaran",
                            params: getParamDetailTransaksiKasirKasirRM(),
                            failure: function (o)
                            {
                                ShowPesanWarningKasir_RehabMedik('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                //RefreshDataFilterKasirKasirRMKasir();
                            },
                            success: function (o)
                            {
                                /*  RefreshDatahistoribayar_KasirRM('0');
                                RefreshDataKasir_RehabMedikDetail('0'); */
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
									Ext.getCmp('btnSimpanKasirKasirRM').disable();
									 tombol_bayar='disable';
									 Ext.getCmp('btnTransferKasirKasirRM').disable();
                                    ShowPesanInfoKasir_RehabMedik("Pembayaran berhasil", "Pembayaran");
                                    Ext.getCmp('txtNoTransaksiKasirKasirRMKasir').focus();
                                    RefreshDatahistoribayar_KasirRM(Ext.get('txtNoTransaksiKasirKasirRMKasir').dom.value);
                                    RefreshDataKasirKasirRMKasirDetail(Ext.get('txtNoTransaksiKasirKasirRMKasir').dom.value,kd_kasir_KasirRM);
                                    if (radiovaluesKasirRM !=='3' && combovalues !== 'Transaksi Baru'){
                                        validasiJenisTrKasirRM();
                                    }
                                    
                                    //FormLookUpsdetailTRPenjasKasirRM.close();
                                    //ViewGridBawahLookupKasir_RehabMedik(Ext.getCmp('txtNoTransaksiKasirKasirRMKasir').getValue(),kd_kasir_KasirRM);
                                    //RefreshDataKasirKasirRMKasir();
                                    /* if (mBol === false)
                                    {
                                        ViewGridBawahLookupKasir_RehabMedik();
                                    }
                                    ; */
                                } else
                                {
                                    ShowPesanWarningKasir_RehabMedik('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_KasirRM(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Rehab_Medik = new Ext.Window
            (
                    {
                        id: 'gridTransfer_rehab_medik',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_KasirRM(lebar),
                        listeners:
                                {
                                     activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transferKasirRM').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Rehab_Medik.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_KasirRM()
{

/*
       KDkasirIGD:'01',
        TrKodeTranskasi: notransaksi,
        KdUnit: kodeunit,
        Kdpay: kdpaytransfer,
        Jumlahtotal: Ext.get(txtjumlahbiayasalKasirRM).dom.value,
        Tglasal:  ShowDate(tgltrans),
        Shift: tampungshiftsekarang,
        TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
        KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
        TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
        KDunittujuan : Trkdunit2,
        KDalasan :Ext.get(cboalasan_transferKasirRM).dom.value,
        KasirRWI:'05',
        Kdcustomer:kdcustomeraa,
        kodeunitkamar:tmp_kodeunitkamar,
        kdspesial:tmp_kdspesial,
        nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customer,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasalKasirRM).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiKasirRM).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecKasirRM).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanKasirRM).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transferKasirRM).dom.value,
                KasirRWI: kdkasirasal_KasirRM,
                kodeunitkamar:tmp_kodeunitkamar_KasirRM,
                kdspesial:tmp_kdspesial_KasirRM,
                nokamar:tmp_nokamar_KasirRM,
                TglTransfer:Ext.get('dtpTanggalDetransaksi').dom.value,

            };
    return params
}
;
function TransferData_KasirRM(mBol)
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
	var UrlTransferKasirRM="";
	/* if (radiovaluesKasirRM === 2 || radiovaluesKasirRM ==='2'){
		UrlTransferKasirRM="index.php/rehab_medik/functionKasirRM/saveTransferRWI";
	}else{ */
		UrlTransferKasirRM="index.php/rehab_medik/functionKasirRM/saveTransfer";
	//}
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + UrlTransferKasirRM,//"index.php/rehab_medik/functionKasirRM/saveTransfer",
                        params: getParamTransferRwi_dari_KasirRM(),
                        failure: function (o)
                        {

                            ShowPesanWarningKasir_RehabMedik('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            ViewGridBawahLookupKasir_RehabMedik();
                        },
                        success: function (o)
                        {   
                        
                            
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {   Ext.getCmp('btnSimpanKasirKasirRM').disable();
								tombol_bayar='disable';
                                Ext.getCmp('btnTransferKasirKasirRM').disable();
                                ShowPesanInfoKasir_RehabMedik('Transfer Berhasil', 'transfer ');
                                Ext.getCmp('txtNoTransaksiKasirKasirRMKasir').focus();
                                RefreshDatahistoribayar_KasirRM(Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue());
                                FormLookUpsdetailTRTransfer_Rehab_Medik.close();
                                FormLookUpsdetailTRPenjasKasirRM.close();
                                
                            } else
                            {
                                if(cst.message != undefined){
                                    ShowPesanWarningKasir_RehabMedik(cst.message, 'Gagal');
                                }else{
                                    ShowPesanWarningKasir_RehabMedik('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                                }
                                
                            }
                            ;
                        }
                    }
            )

}
;

function getItemPanelButtonTransfer_KasirRM(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_KasirRM',
                                                handler: function ()
                                                {
                                                    loadMask.show();
                                                    TransferData_KasirRM(false);
                                                    loadMask.hide();
                                                     Ext.getCmp('btnSimpanKasirKasirRM').disable();
													 tombol_bayar='disable';
                                                     Ext.getCmp('btnTransferKasirKasirRM').disable();
                                                    
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_KasirRM',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Rehab_Medik.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_KasirRM(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_KasirRM(lebar), getItemPanelButtonTransfer_KasirRM(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: false,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupKasir_RehabMedik();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_KasirRM(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiKasirRM',
                                id: 'txtTranfernoTransaksiKasirRM',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanKasirRM',
                                name: 'dtpTanggaltransaksiRujuanKasirRM',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecKasirRM',
                                id: 'txtTranfernomedrecKasirRM',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienKasirRM',
                                id: 'txtTranfernamapasienKasirRM',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitKasirRM',
                                id: 'txtTranferunitKasirRM',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_KasirRM',
                                        id: 'txtTranferkelaskamar_KasirRM',
                                        readOnly : true,
                                        labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransferKasirRM(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasalKasirRM',
                                                id: 'txtjumlahbiayasalKasirRM',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaidKasirRM',
                                                id: 'txtpaidKasirRM',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranferKasirRM',
                                                id: 'txtjumlahtranferKasirRM',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihanKasirRM',
                                                id: 'txtsaldotagihanKasirRM',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transferKasirRM()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transferKasirRM = new WebApp.DataStore({fields: Field});
    dsalasan_transferKasirRM.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transferKasirRM = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transferKasirRM',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transferKasirRM,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transferKasirRM;
};

function getItemPanelInputTransfer_KasirRM(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_KasirRM(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransferKasirRM(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transferKasirRM()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
    if(kode==='05')
    {
    Ext.getCmp('txtTranferunitKasirRM').hide();
    Ext.getCmp('txtTranferkelaskamar_KasirRM').show();

    }else{
    Ext.getCmp('txtTranferunitKasirRM').show();
    Ext.getCmp('txtTranferkelaskamar_KasirRM').hide();
    }
}
function getFormEntryPenjasBayarKasirRM(lebar)
{
    var pnlTRKasirKasirRM = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirKasirRM',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        enableKeyEvents:true,
                        listeners:
                                {
                                    keydown:function(text,e){
                                        if(e.keyCode == 122){
                                            e.preventDefault();
                                            alert('hai');
                                        }
                                    }
                                },
                        items: [getItemPanelInputKasirKasirRM(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotalKasirRM = new Ext.Panel
            (
                    {
                        id: 'paneltotalKasirRM',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: false,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirKasirRM',
                                                        name: 'txtJumlah2EditData_viKasirKasirRM',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: false,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirKasirRM',
                                                        name: 'txtJumlahEditData_viKasirKasirRM',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirKasirRM = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirKasirRM',
                        region: 'center',
                        activeTab: 0,
                        height: 380,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirKasirRMGrid(), paneltotalKasirRM],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirKasirRM',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirKasirRMKasir(false);
                                           // FormDepan2KasirKasirRM.close();
											tombol_bayar='disable';
                                            //Ext.getCmp('btnHpsBrsKasirKasirRM').disable();
                                        }
                                    },
                                    
                                    {
                                        id: 'btnTransferKasirKasirRM',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            //Disini
                                            TransferLookUp_KasirRM();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/rehab_medik/functionKasirRM/GetPasienTranferRehabMedik",
                                                params: {
                                                    notr: notransaksiasal_KasirRM,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirKasirRMKasir').getValue(),//tmp_NoTransaksi,
                                                    kdkasir: kdkasirasal_KasirRM
                                                },
                                                success: function (o)
                                                {   
                                                showhide_unit(kdkasirasal_KasirRM);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningKasir_RehabMedik('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningKasir_RehabMedik('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Rehab_Medik.close();
                                                        } else {
                                                            console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiKasirRM).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanKasirRM).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecKasirRM).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienKasirRM).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitKasirRM).dom.value = tmp[1];
                                                            Trkdunit2 = tmp[6];
                                                            Ext.get(txtTranferkelaskamar_KasirRM).dom.value=tmp[9];
                                                            Ext.get(txtjumlahbiayasalKasirRM).dom.value=tmp[12];
                                                            Ext.get(txtjumlahtranferKasirRM).dom.value=tmp[12];
                                                            
                                                            tmp_kodeunitkamar_KasirRM=tmp[8];
                                                            tmp_kdspesial_KasirRM=tmp[10];
                                                            tmp_nokamar_KasirRM=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/rehab_medik/functionKasirRM/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
                                                                    kd_kasir:kdkasir,
																	tgl_transaksi: Ext.getCmp('dtpTanggalDetransaksi').getValue()
                                                                },
                                                                success: function (o)
                                                                {
                                                                    console.log(o);
                                                                    Ext.get(txtjumlahbiayasalKasirRM).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranferKasirRM).dom.value = formatCurrency(o.responseText);
                                                                    /* Ext.get(txtjumlahbiayasalKasirRM).dom.value=tmp[12];
                                                                    Ext.get(txtjumlahtranferKasirRM).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadrehab_medik',
                                                            handler: function ()
                                                            {
                                                                /* if (Ext.getCmp('cbopasienorder_printer_kasirKasirRM').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirKasirRM').getValue()===undefined){
                                                                    ShowPesanWarningKasir_RehabMedik('Pilih printer terlebih dahulu !', 'Cetak Data');
                                                                }else{ */
                                                                    printbillRadRehab_Medik();
                                                                //}
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadRehab_Medik',
                                                            handler: function ()
                                                            {
                                                                /* if (Ext.getCmp('cbopasienorder_printer_kasirKasirRM').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirKasirRM').getValue()===undefined){
                                                                    ShowPesanWarningKasir_RehabMedik('Pilih printer terlebih dahulu !', 'Cetak Data');
                                                                }else{ */
                                                                    printkwitansiRadRehab_Medik();
                                                                //}
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirKasirRM2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirKasirRM2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirKasirRM,
                        ]
                    }
            );




    FormDepan2KasirKasirRM = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirKasirRM',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: false,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirKasirRM, pnlTRKasirKasirRM2,
                        ]
                    }
            );

    return FormDepan2KasirKasirRM
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,
                KDkasir: kdkasir
            };
    return params
}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanInfoKasir_RehabMedik('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTrKasirRM();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar_KasirRM(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasir_RehabMedik('Tutup Transaksi Berhasil', nmHeaderSimpanData);

                                //RefreshDataKasirKasirRMKasir();
                                if (mBol === false)
                                {
                                    validasiJenisTrKasirRM();
                                }
                                ;
                                cellSelecteddeskripsi = '';
                            } else
                            {
                                ShowPesanInfoKasir_RehabMedik('Tutup Transaksi gagal segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
            )
}
;
function RecordBaruKasirRM()
{
    var TanggalTransaksiGrid;
    //alert(TglTransaksi);
    /* if (TglTransaksi === undefined)
    {
        TanggalTransaksiGrid=tglGridBawah;
    }else{
        TanggalTransaksiGrid=TglTransaksi;
    } */
    var TanggalTransaksiGrid = Ext.getCmp('dtpKunjunganKasirRM').getValue();
    var p = new mRecordKasirRM
    (
        {
            'cito':'',
            'kd_tarif':'',
            'kd_produk':'',
            'kp_produk':'',
            'deskripsi':'',
            'tgl_transaksi':TanggalTransaksiGrid,
            'tgl_berlaku':null,
            'harga':'',
            'qty':'',
            'jumlah':''
            
        }
    );
    
    return p;
};
function TambahBarisKasirRM()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirRM();
        dsTRDetailKasir_RehabMedikList.insert(dsTRDetailKasir_RehabMedikList.getCount(), p);
    };
};
function getFormEntryKasir_RehabMedik(lebar,data) 
{
    console.log(lebar);
    var pnlTRKasirRM = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasir_RehabMedik',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding: 5px 5px 5px 5px',
            height:285,
            width: lebar,
            border: false,
            items: [getItemPanelKasir_RehabMedik(lebar)],
            tbar:
            [
                {
                        text: 'Tambah Baru',
                        id: 'btnTambahBaruKasirRM',
                        tooltip: nmSimpan,
                        iconCls: 'add',
                        handler: function()
                        {
                            KasirRMAddNew();
                            ViewGridBawahLookupKasir_RehabMedik();
                            Ext.getCmp('txtNamaPasienKasirRM').focus();                 
                            Ext.getCmp('txtnotlpKasirRM').setReadOnly(false);                 
                        }
                },  
                //'-',
                {
                        text: 'Simpan',
                        id: 'btnSimpanKasirRM',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        hidden:true,
                        handler: function()
                        {
                            /* var tmp_nomer_lab = Ext.getCmp('txtNomorRehab_Medik').getValue();
                            if (tmp_nomer_lab.length > 0) { */
                                loadMask.show();
                                Datasave_Kasir_RehabMedik(false); 
                                loadMask.hide();
                            /* }else{
                                Ext.Msg.confirm('Warning', 'Nomer LAB masih kosong, apakah anda yakin untuk melanjutkan pembayaran?', function(button){
                                    if (button == 'yes'){
                                        loadMask.show();
                                        Datasave_Kasir_RehabMedik(false); 
                                        loadMask.hide();
                                    }else{
                                        Ext.getCmp('txtNomorRehab_Medik').focus();
                                    }
                                });
                            } */                       
                            // Datasave_Kasir_RehabMedik_SQL();                                  
                        }
                },  
                
                '-',
                {
                    id: 'btnPembayaranPenjasKasirRM',
                    text: 'Pembayaran',
                    tooltip: nmEditData,
                    iconCls: 'Edit_Tr',
                    handler: function (sm, row, rec)
                    {
                                    if (rowSelectedKasir_RehabMedik != undefined) {
                                        PenjasLookUpKasirRM(rowSelectedKasir_RehabMedik.data);
                                     } else {
                                        PenjasLookUpKasirRM();
                                        //ShowPesanWarningKasir_RehabMedik('Pilih data tabel  ', 'Pembayaran');
                                    } 
                    }//, disabled: true
                },'-',
                {
                    id: 'btnCatatanHasilPenjasKasirRM',
                    text: 'Catatan Hasil',
                    tooltip: nmEditData,
                    iconCls: 'Edit_Tr',
                    handler: function (sm, row, rec)
                    {
							Ext.Ajax.request({
								url: baseURL + "index.php/rehab_medik/functionKasirRM/getCatatanHasil",
								params: {
									kd_pasien: vKd_Pasien,
									kd_unit:vKdUnit,
									tgl_masuk:vTanggal,
									urut_masuk:vUrutMasuk
								},
								failure: function (o)
								{
									var cst = Ext.decode(o.responseText);
									ShowPesanWarningKasir_RehabMedik('Proses mendapatkan catatan GAGAL', "Baca Data");
								},
								success: function (o) {
									var cst = Ext.decode(o.responseText);
									if (cst.success === true){
										FormCatatanHasilRehabMedik(cst.catatan);
									}else{
										ShowPesanWarningKasir_RehabMedik('Proses mendapatkan catatan GAGAL', "Simpan Catatan");
									}
									
								}

							});
                          
                    }//, disabled: true
                }, '-',
                {
                    text: 'Tutup Transaksi',
                    id: 'btnTutupTransaksiPenjasKasirRM',
                    tooltip: nmHapus,
                    iconCls: 'remove',
					//hidden : true,
                    handler: function ()
                    {
                        if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
                            ShowPesanWarningKasir_RehabMedik('Pilih data tabel  ', 'Pembayaran');
                        } else {
                            UpdateTutuptransaksi(false);
                            Ext.getCmp('btnPembayaranPenjasKasirRM').disable();
                            Ext.getCmp('btnCatatanHasilPenjasKasirRM').disable();
                            Ext.getCmp('btnTutupTransaksiPenjasKasirRM').disable();
                            Ext.getCmp('btnHpsBrsItemRehab_Medik').disable();
                        }
                    }//, disabled: true
                }   
            ]
        }
    );
 var x;
 var GDtabDetailKasirRM = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailKasirRM',
        region: 'center',
        activeTab: 0,
        height:250,
        width:855,
        //anchor: '100%',
        border:true,
        plain: true,
        defaults:
        {
            autoScroll: true
        },
        items: [GridDetailItemPemeriksaan(),GetDTLTRHistoryGrid()],
        
            listeners:
            {   
                'tabchange' : function (panel, tab) {
                    if (x ==1)
                    {
                        Ext.getCmp('btnLookupKasir_RehabMedik').hide()
                        //Ext.getCmp('btnSimpanKasirRM').hide()
                        Ext.getCmp('btnHpsBrsItemRehab_Medik').hide()
                        x=2;
                        return x;
                    }else 
                    {   
                        Ext.getCmp('btnLookupKasir_RehabMedik').show()
                        //Ext.getCmp('btnSimpanKasirRM').show()
                        Ext.getCmp('btnHpsBrsItemRehab_Medik').show()
                        x=1;    
                        return x;
                    }

                }
            }
        }
        
    );
    
   
   
   var pnlTRKasirRM2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasir_RehabMedik2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:460,
            //anchor: '100%',
            width: lebar,
            border: false,
            items: [    GDtabDetailKasirRM
            
            ]
        }
    );

    
   
   
    var FormDepanKasir_RehabMedik = new Ext.Panel
    (
        {
            id: 'FormDepanKasir_RehabMedik',
            region: 'center',
            width: '100%',
            anchor: '100%',
            layout: 'form',
            title: '',
            bodyStyle: 'padding:15px',
            border: false,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
            items: 
            [
                pnlTRKasirRM,pnlTRKasirRM2  
            ]

        }
    );
    return FormDepanKasir_RehabMedik
};

function DataDeletePenJasRadDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeletePenJasRadDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasir_RehabMedik(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasir_RehabMedikList.removeAt(CurrentKasir_RehabMedik.row);
                    cellSelecteddeskripsi=undefined;
					Ext.getCmp('cboDataJadwal_KASIRRM').setValue('ALL');
                    ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                    AddNewPenJasRad = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasir_RehabMedik(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasir_RehabMedik(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeletePenJasRadDetail()
{
    var params =
    {//^^^
        Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentKasir_RehabMedik.data.data.NO_TRANSAKSI,
        TrTglTransaksi:  CurrentKasir_RehabMedik.data.data.TGL_TRANSAKSI,
        TrKdPasien :     CurrentKasir_RehabMedik.data.data.KD_PASIEN,
        TrKdNamaPasien : Ext.get('txtNamaPasienKasirRM').getValue(),
        //TrAlamatPasien : Ext.get('txtAlamatKasirRM').getValue(),    
        TrKdUnit :       Ext.get('txtKdUnitRehab_Medik').getValue(),
        TrNamaUnit :     Ext.get('txtNamaUnitKasirRM').getValue(),
        Uraian :         CurrentKasir_RehabMedik.data.data.DESKRIPSI2,
        AlasanHapus :    variablehistori,
        TrHarga :        CurrentKasir_RehabMedik.data.data.HARGA,
        
        TrKdProduk :     CurrentKasir_RehabMedik.data.data.KD_PRODUK,
        RowReq: CurrentKasir_RehabMedik.data.data.URUT,
        Hapus:2
    };
    
    return params
};

function getParamDataupdatePenJasRadDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentKasir_RehabMedik.data.data.NO_TRANSAKSI,
        RowReq: CurrentKasir_RehabMedik.data.data.URUT,

        Qty: CurrentKasir_RehabMedik.data.data.QTY,
        Ubah:1
    };
    
    return params
};
function msg_box_buatjadwalbaru_RM(data)
{
    var lebar = 200;
    var form_msg_box_buatjadwalbaru_RM = new Ext.Window
            (
                    {
                        id: 'alasan_buatjadwalbaru',
                        title: 'Buat Jadwal Baru',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
												{
													xtype: 'datefield',
													//fieldLabel: 'Tanggal',
													id: 'TglBuatJadwalBaru_getTindakanKasir_RehabMedik',
													name: 'TglBuatJadwalBaru_getTindakanKasir_RehabMedik',
													width: 100,
													format: 'd/m/Y',
													value: now,
												},
												/* {
													xtype: 'textfield',
													//fieldLabel: 'No. Transaksi ',
													name: 'txtbuatjadwalbaruRM',
													id: 'txtbuatjadwalbaruRM',
													emptyText: 'Alasan Buka Transaksi',
													anchor: '99%',
												}, */
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Buat',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkBuatJadwalBaru_RM',
                                                                    handler: function ()
                                                                    {
																		kirimanTanggalDariFormLookUpBuatJadwal=Ext.getCmp('TglBuatJadwalBaru_getTindakanKasir_RehabMedik').getValue()
																		getTindakanRehab_Medik();
																		setLookUp_getTindakanKasir_RehabMedik();
																		form_msg_box_buatjadwalbaru_RM.close();
																		/* if (Ext.getCmp('txtAlasanbukatransRM').getValue().trim()==='' || Ext.getCmp('txtAlasanbukatransRM').getValue().trim==='Alasan Buka Transaksi')
																		{
																			ShowPesanWarningKasir_RehabMedik("Isi alasan buka transaksi !", "Perhatian");
																		}else{
																			Ext.Ajax.request
																			(
																					{
																						url: baseURL + "index.php/main/functionKasirPenunjang/bukatransaksi",
																						params: {
																							no_transaksi : data.notrans_bukatrans,
																							kd_kasir	 : data.kd_kasir_bukatrans,
																							tgl_transaksi: data.tgl_transaksi_bukatrans,
																							kd_pasien	 : data.kd_pasien_bukatrans,
																							urut_masuk	 : data.urut_masuk_bukatrans,
																							kd_unit 	 : data.kd_unit_bukatrans,
																							keterangan	 : Ext.getCmp('txtAlasanbukatransRM').getValue(),
																							kd_bagian_shift : 4
																						},
																						success: function (o)
																						{
																							//	RefreshDatahistoribayar_RM(Kdtransaksi);
																						   // RefreshDataFilterKasirRMKasir();
																							//RefreshDatahistoribayar_RM('0');
																							var cst = Ext.decode(o.responseText);
																							if (cst.success === true)
																							{
																								ShowPesanInfoKasir_RehabMedik("Proses Buka Transaksi Berhasil", nmHeaderHapusData);
																								Ext.getCmp('btnBukaTransaksiRM').disable();
																								validasiJenisTrKasirRM();
																								
																							} else {
																								ShowPesanWarningKasir_RehabMedik(nmPesanHapusError, nmHeaderHapusData);
																							}
																							;
																						}
																					}
																			)
																			form_msg_box_buatjadwalbaru_RM.close();
																		} */
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelBuatJadwalBaru_RM',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_buatjadwalbaru_RM.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_buatjadwalbaru_RM.show();
}
;
function GridDetailItemPemeriksaan() 
{
    var fldDetailKasirRM = ['kp_produk','kd_produk','deskripsi','deskripsi2','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','kd_unit','kd_kasir'];
    
    dsTRDetailKasir_RehabMedikList = new WebApp.DataStore({ fields: fldDetailKasirRM })
    //ViewGridBawahLookupKasir_RehabMedik() ;
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
            id: 'gridDTItemTest',
            stripeRows: true,
            store: dsTRDetailKasir_RehabMedikList,
            border: false,
            columnLines: true,
            frame: false,
            width:50,
            height:100,
            //anchor: '100%',
            autoScroll:true,
            viewConfig: {forceFit: true},
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasir_RehabMedikList.getAt(row);
                            CurrentKasir_RehabMedik.row = row;
                            CurrentKasir_RehabMedik.data = cellSelecteddeskripsi;
                            // console.log(cellSelecteddeskripsi);
                        }
                    }
                }
            ),
            
            cm: TRKasir_RehabMedikColumModel(),
            tbar:
            [
                {
                    text: 'Tambah Baris',
                    id: 'btnTambahBarisKasir_RehabMedik',
                    tooltip: nmLookup,
                    iconCls: 'Edit_Tr',
                    handler: function()
                    {
                        TambahBarisKasirRM()
                        
                    }
                },{
                    text: 'Tambah Item Pemeriksaan (Lookup)',
                    id: 'btnLookupKasir_RehabMedik',
                    tooltip: nmLookup,
                    iconCls: 'find',
                    handler: function()
                    {
						kirimanTanggalDariFormLookUpBuatJadwal=now;
                        getTindakanRehab_Medik();
                        setLookUp_getTindakanKasir_RehabMedik();
                        
                    }
                },
                {
                        id:'btnHpsBrsItemRehab_Medik',
                        text: 'Hapus Baris',
                        tooltip: 'Hapus Baris',
                        disabled:false,
                        iconCls: 'RemoveRow',
                        handler: function()
                        {
                            if (dsTRDetailKasir_RehabMedikList.getCount() > 0 ) {
                                if (cellSelecteddeskripsi != undefined)
                                {
                                    Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
                                        if (button == 'yes'){
                                            if(CurrentKasir_RehabMedik != undefined) {
                                                loadMask.show();
                                                var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                                Ext.Ajax.request
                                                 (
                                                    {
                                                        url: baseURL + "index.php/rehab_medik/functionKasirRM/deletedetaillab",
                                                        params: getParamHapusDetailTransaksiKasirRM(),
                                                        failure: function(o)
                                                        {
                                                            ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                                                            loadMask.hide();
                                                        },  
                                                        success: function(o) 
                                                        {
                                                            loadMask.hide();
                                                            var cst = Ext.decode(o.responseText);
                                                            if (cst.success === true && cst.cari===false) 
                                                            {
                                                                
                                                                ShowPesanInfoKasir_RehabMedik('Data Berhasil Di hapus', 'Sukses');
                                                                dsTRDetailKasir_RehabMedikList.removeAt(line);
																Ext.getCmp('cboDataJadwal_KASIRRM').setValue('ALL');
                                                                ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                                                                gridDTItemTest.getView().refresh();
                                                            }else if (cst.success === true && cst.cari===true)
                                                            {
                                                                dsTRDetailKasir_RehabMedikList.removeAt(line);
                                                            }
                                                            else 
                                                            {
																if (cst.cari !== false || cst.cari !== 'false'){
																	ShowPesanWarningKasir_RehabMedik(cst.cari, 'Gagal');
																	  
																}else
																{
																	ShowPesanWarningKasir_RehabMedik('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
																}
                                                            };
                                                        }
                                                    }
                                                )
                                                
                                            }
                                        }
                                    })
                                } else {
                                    ShowPesanWarningKasir_RehabMedik('Pilih record ','Hapus data');
                                }
                            }
                        }
                },
				
                 mCombo_Jadwal_KASIRRM() ,
				{
                    text: 'Buat Jadwal Baru',
                    id: 'btnBuatJadwalBaruKasir_RehabMedik',
                    tooltip: nmLookup,
                    iconCls: 'Edit_Tr',
                    handler: function()
                    {
                        msg_box_buatjadwalbaru_RM()
                    }
                },				 
            ],
        }
        
        
    );
    
    

    return gridDTItemTest;
};
function loaddatastoredaftar_jadwal(no_transaksi, kode_kasir){
    Ext.Ajax.request({
        url: baseURL +  "index.php/rehab_medik/functionKasirRM/getCmbDaftarJadwal",
        params: {
            kd_kasir        : kode_kasir,
            no_transaksi    : no_transaksi,
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            var cst  = Ext.decode(response.responseText);
            console.log(cst);
			dsDataDaftarJadwal_KASIR_RM.removeAll();
            //dsDataDaftarJadwal_KASIR_RM.load(myData);
            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                var recs    = [],recType = dsDataDaftarJadwal_KASIR_RM.recordType;
                var o       = cst['data'][i];
                recs.push(new recType(o));
                dsDataDaftarJadwal_KASIR_RM.add(recs);
            }
        },
        //jsonData: Ext.JSON.encode(queryform.getValues())
    });
    /*dsDataDaftarJadwal_KASIR_RM.load({
        }
    });*/
}
function mCombo_Jadwal_KASIRRM()
{ 
    var Field = ['TGL_JADWAL','TGL_JADWAL'];

    dsDataDaftarJadwal_KASIR_RM = new WebApp.DataStore({ fields: Field });
    
    var cboDataJadwal_KASIRRM = new Ext.form.ComboBox
    (
        {
            id: 'cboDataJadwal_KASIRRM',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Daftar Jadwal',
            fieldLabel:  '',
            align: 'Right',
            width: 150,
            store: dsDataDaftarJadwal_KASIR_RM,
            valueField: 'TGL_JADWAL_FORMAT',
            displayField: 'TGL_JADWAL',
			listeners:{
				'select': function(a,b,c){
					ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,b.data.TGL_JADWAL_FORMAT);
				}
			}
           
        }
    );return cboDataJadwal_KASIRRM;
};
function TRKasir_RehabMedikColumModel()
{

    checkColumn_penata__KasirRM = new Ext.grid.CheckColumn({
       header: 'Cito',
       dataIndex: 'cito',
       id: 'checkid',
       width: 55,
       renderer: function(value)
       {
        console.log("Grid");
        },
    });
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
            {
                header: 'Cito',
                dataIndex: 'cito',
                width:65,
                menuDisabled:true,
                listeners:{
                    itemclick: function(dv, record, items, index, e)
                    {
                                    Ext.getCmp('btnHpsBrsItemRehab_MedikL').enable();
                                    console.log("test");
                    },
                },
                renderer:function (v, metaData, record)
                {
                    if ( record.data.cito=='0')
                    {
                    record.data.cito='Tidak'
                    }else if (record.data.cito=='1')
                    {
                    metaData.style  ='background:#FF0000;  "font-weight":"bold";';
                    record.data.cito='Ya'
                    }else if (record.data.cito=='Ya')
                    {
                    metaData.style  ='background:#FF0000;  "font-weight":"bold";';
                    }
                    
                    return record.data.cito; 
                },
                editor:new Ext.form.ComboBox
                ({
                    id: 'cboKasus',
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    selectOnFocus: true,
                    forceSelection: true,
                    emptyText: 'Silahkan Pilih...',
                    width: 50,
                    anchor: '95%',
                    value: 1,
                    store: new Ext.data.ArrayStore({
                        id: 0,
                        fields: ['Id', 'displayText'],
                        data: [[1, 'Ya'], [2, 'Tidak']]
                    }),
                    valueField: 'displayText',
                    displayField: 'displayText',
                    value       : '',
                       
                })
            },
            {
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
                menuDisabled:true,
                hidden :true

            },
            {
                header: 'KD Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },{
                header: 'Kode Produk',
                dataIndex: 'kp_produk',
                width:100,
                menuDisabled:true,
                //hidden:true,
                editor      : new Ext.form.TextField({
                    id                  : 'fieldcolKdProduk',
                    allowBlank          : true,
                    enableKeyEvents     : true,
                    width               : 30,
                    listeners           :
                    { 
                        'specialkey' : function(a, b)
                        {
                            if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                var kd_cus_gettarif=vkode_customer;
                                /* if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perseorangan'){
                                    kd_cus_gettarif=Ext.getCmp('cboPerseoranganRehab_Medik').getValue();
                                }else if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perusahaan'){
                                    kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').getValue();
                                }else {
                                    kd_cus_gettarif=Ext.getCmp('cboAsuransiRehab_Medik').getValue();
                                  } */
                                var modul='';
                                if(radiovaluesKasirRM == 1){
                                    modul='igd';
                                } else if(radiovaluesKasirRM == 2){
                                    modul='rwi';
                                } else if(radiovaluesKasirRM == 4){
                                    modul='rwj';
                                } else{
                                    modul='langsung';
                                }
                                Ext.Ajax.request
                                ({
                                    store   : KasirKasirRMDataStoreProduk,
                                    url     : baseURL + "index.php/rehab_medik/functionKasirRM/getGridProdukKey",
                                    params  :  {
                                        kd_unit:kodeUnitRehab_Medik,
                                     kd_customer:kd_cus_gettarif,
                                     notrans:Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue(),
                                     penjas:modul,
                                     kdunittujuan:tmpkd_unit,
                                        text            : Ext.getCmp('fieldcolKdProduk').getValue(),
                                    },
                                    failure : function(o)
                                    {
                                        ShowPesanWarningKasir_RehabMedik("Data produk tidak ada!",'WARNING');
                                    },
                                    success : function(o)
                                    {
                                        var cst = Ext.decode(o.responseText);
                                        if (cst.processResult == 'SUCCESS'){
                                            kd_produk           = cst.listData.kd_produk;
                                            kp_produk           = cst.listData.kp_produk;
                                            kd_tarif            = cst.listData.kd_tarif;
                                            deskripsi           = cst.listData.deskripsi;
                                            harga               = cst.listData.harga;
                                            tgl_berlaku         = cst.listData.tgl_berlaku;
                                            jumlah              = cst.listData.jumlah;
                                            var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                            
                                            /* dsTRDetailKasir_RehabMedikList.getRange()[CurrentKasir_RehabMedik.row].set('deskripsi', cst.listData.deskripsi);
                                            dsTRDetailKasir_RehabMedikList.getRange()[CurrentKasir_RehabMedik.row].set('qty', 1); */
                                            //alert(dsTRDetailKasir_RehabMedikList.data.items[line].data.deskripsi);
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_produk         = cst.listData.kd_produk;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.kp_produk         = cst.listData.kp_produk;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_klas           = cst.listData.kd_klas;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_unit           = cst.listData.kd_unit;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.harga             = cst.listData.harga;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.deskripsi         = cst.listData.deskripsi;
                                            console.log(dsTRDetailKasir_RehabMedikList.data.items[line]);
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.tgl_berlaku       = cst.listData.tgl_berlaku;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_tarif          = cst.listData.kd_tarif;
                                            //dsTRDetailKasir_RehabMedikList.data.items[line].data.tgl_transaksi     = Tgltransaksi;
                                            dsTRDetailKasir_RehabMedikList.data.items[line].data.qty               = '1';
                                            
                                            gridDTItemTest.getView().refresh();
                                            gridDTItemTest.startEditing(line, 9);
                                            
                                        }else{
                                            ShowPesanWarningKasir_RehabMedik('Data produk tidak ada!','WARNING');
                                        };
                                    }
                                })
                            }
                        },
                    }
                }), 
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320

            }, */
            {   id:'nama_pemereksaaan_KasirRM',
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                sortable: true,
                menuDisabled:true,
                width: 320,
                /* editor:new Nci.form.Combobox.autoComplete({
                    store   : TrKasir_RehabMedik.form.ArrayStore.produk,
                    select  : function(a,b,c){
                    //  console.log(b);
                    //Disini
                        Ext.Ajax.request
                        (
                            {
                                url: baseURL + "index.php/main/functionLAB/cekProduk",
                                params:{kd_KasirRM:b.data.kd_produk} ,
                                failure: function(o)
                                {
                                    ShowPesanErrorKasir_RehabMedik('Hubungi Admin', 'Error');
                                },
                                success: function(o)
                                {
                                    Ext.getCmp('btnHpsBrsItemRehab_MedikL').enable();
                                    console.log("test");
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.deskripsi=b.data.deskripsi;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.uraian=b.data.uraian;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_tarif=b.data.kd_tarif;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_produk=b.data.kd_produk;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.harga=b.data.harga;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.qty=b.data.qty;
                                        dsTRDetailKasir_RehabMedikList.data.items[line].data.jumlah=b.data.jumlah;

                                        gridDTItemTest.getView().refresh();
                                    }
                                    else
                                    {
                                        ShowPesanInfoKasir_RehabMedik('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
                                    };
                                }
                            }

                        )
                        
                    },
                    insert  : function(o){
                        return {
                            uraian          : o.uraian,
                            kd_tarif        : o.kd_tarif,
                            kd_produk       : o.kd_produk,
                            tgl_transaksi   : o.tgl_transaksi,
                            tgl_berlaku     : o.tgl_berlaku,
                            harga           : o.harga,
                            qty             : o.qty,
                            deskripsi       : o.deskripsi,
                            jumlah          : o.jumlah,
                            text            :  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
                        }
                    },
                    param   : function(){
                    var kd_cus_gettarif;
                    if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perseorangan'){
                        kd_cus_gettarif=Ext.getCmp('cboPerseoranganRehab_Medik').getValue();
                    }else if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perusahaan'){
                        kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').getValue();
                    }else {
                        kd_cus_gettarif=Ext.getCmp('cboAsuransiRehab_Medik').getValue();
                          }
                    var params={};
                    params['kd_unit']=kodeUnitRehab_Medik;
                    params['kd_customer']=kd_cus_gettarif;
                    return params;
                    },
                    url     : baseURL + "index.php/main/functionLAB/getProduk",
                    valueField: 'deskripsi',
                    displayField: 'text',
                    listWidth: 210
                }) */
            },
            {
                header: 'Tanggal Transaksi',
                dataIndex: 'tgl_transaksi',
                width: 130,
                menuDisabled:true,
                renderer: function(v, params, record)
                {
                    if(record.data.tgl_transaksi == undefined){
                        record.data.tgl_transaksi=tglGridBawah;
                        return record.data.tgl_transaksi;
                    } else{
                        if(record.data.tgl_transaksi.substring(5, 4) == '-'){
                            return ShowDate(record.data.tgl_transaksi);
                        } else{
                            var tgl=record.data.tgl_transaksi.split("/");

                            if(tgl[2].length == 4 && isNaN(tgl[1])){
                                return record.data.tgl_transaksi;
                            } else{
                                return ShowDate(record.data.tgl_transaksi);
                            }
                        }

                    }
                }
            },
            {
                header: 'Tanggal Berlaku',
                dataIndex: 'tgl_berlaku',
                width: 130,
                menuDisabled:true,
                //hidden: true,
                renderer: function(v, params, record)
                {
                    if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
                        record.data.tgl_berlaku=tglGridBawah;
                        return record.data.tgl_berlaku;
                    } else{
                        if(record.data.tgl_berlaku.substring(5, 4) == '-'){
                            return ShowDate(record.data.tgl_berlaku);
                        } else{
                            var tglb=record.data.tgl_berlaku.split("-");

                            if(tglb[2].length == 4 && isNaN(tglb[1])){
                                return record.data.tgl_berlaku;
                            } else{
                                return ShowDate(record.data.tgl_berlaku);
                            }
                        }

                    } 
                }
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
                renderer: function(v, params, record)
                {
                    return formatCurrency(record.data.harga);
                }
            },
            {   id:'qty_KasirRM',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'qty',
                editor      : new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemKasirRM',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                        { 
                            'specialkey' : function(a, b)
                            {
                                if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
                                    var line = gridDTItemTest.getSelectionModel().selection.cell[0];
                                    
                                    TambahBarisKasirRM();
                                    gridDTItemTest.startEditing(line+1, 4);
                                    loadMask.show();
                                    Datasave_Kasir_RehabMedik_LangsungTambahBaris(false); 
                                    loadMask.hide();    
                                }
                            },
                        }
                    }
                )
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                Dataupdate_PenJasRad(false);
                            }
                        }
                    } */
              //  ),



            },
             {
                header: 'Dokter',
                width:91,
                menuDisabled:true,
                dataIndex: 'jumlah',
                hidden: true,

            },

        ]
    )
};

/* function TRKasir_RehabMedikColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
                menuDisabled:true,
                hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
            ,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
                renderer: function(v, params, record) 
                            {
                            return formatCurrency(record.data.HARGA);
                            
                            }   
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
                align: 'right',
                menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
                        listeners:
                            { 
                                'specialkey' : function()
                                {
                                    
                                            Dataupdate_PenJasRad(false);

                                }
                            }
                    }
                ),
                
                
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
                hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
                
            }

        ]
    )
}; */


function RecordBaruKasirRM()
{
    var tgltranstoday = Ext.getCmp('dtpKunjunganKasirRM').getValue();
    var p = new mRecordKasirRM
    (
        {
            'DESKRIPSI2':'',
            'KD_PRODUK':'',
            'DESKRIPSI':'', 
            'KD_TARIF':'', 
            'HARGA':'',
            'QTY':'',
            'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
            'DESC_REQ':'',
            'KD_TARIF':'',
            'URUT':''
        }
    );
    
    return p;
};
var kd_kasir_KasirRM;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRKasirRMInit(rowdata)
{
    Ext.getCmp('btnTambahBaruKasirRM').hide();
    tmpurut = rowdata.URUT_MASUK;
    AddNewPenJasRad = false;
    
    console.log(rowdata);
    TmpNotransaksi=rowdata.NO_TRANSAKSI;
    KdKasirAsal=rowdata.KD_KASIR;
    TglTransaksi=rowdata.TGL_TRANSAKSI;
    Kd_Spesial=rowdata.KD_SPESIAL;
    No_Kamar=rowdata.KAMAR;
    kodeunit=rowdata.KD_UNIT;
	 
    if (combovalues==='Transaksi Lama' )
    {
		loaddatastoredaftar_jadwal(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
		Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value = rowdata.NO_TRANSAKSI;
        // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').disable();
        Ext.getCmp('cboDataJadwal_KASIRRM').setValue('ALL');
        ViewGridBawahLookupKasir_RehabMedik(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR,'ALL');
        kdkasir=rowdata.KD_KASIR;
        Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
        Ext.getCmp('btnCatatanHasilPenjasKasirRM').enable();
        Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
        Ext.getCmp('btnHpsBrsKasirKasirRM').enable();
        RefreshDatahistoribayar_KasirRM(rowdata.NO_TRANSAKSI);
        kodeUnitRehab_Medik=rowdata.KD_UNIT_ASAL;
        if (rowdata.LUNAS==='t')
        {
            //Ext.getCmp('btnTutupTransaksiPenjasKasirRM').disable();
        }
		/* if (rowdata.CO_STATUS==='t'){
			   
			Ext.getCmp('btnTambahBarisKasir_RehabMedik').disable();
			Ext.getCmp('btnLookupKasir_RehabMedik').disable();
			Ext.getCmp('btnHpsBrsItemRehab_Medik').disable();
		}else{
			Ext.getCmp('btnTambahBarisKasir_RehabMedik').enable();
			Ext.getCmp('btnLookupKasir_RehabMedik').enable();
			Ext.getCmp('btnHpsBrsItemRehab_Medik').enable();
		} */
		Ext.getCmp('cboDokterPengirimKasirRM').setValue(rowdata.DOKTER_ASAL)
        Ext.getCmp('cboDOKTER_viKasir_RehabMedik').setValue(rowdata.DOKTER);
        // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').setValue(rowdata.NAMA_UNIT_ASLI);
        tmpkd_unit = rowdata.KD_UNIT;
        tmpkddoktertujuan = rowdata.KD_DOKTER;
        tmpkddokterpengirim = rowdata.KD_DOKTER;
        kd_kasir_KasirRM = rowdata.KD_KASIR;
        if (radiovaluesKasirRM !=='3'){
            Ext.get('txtNamaUnitKasirRM').dom.value = rowdata.NAMA_UNIT_ASAL;
            Ext.get('txtKdUnitRehab_Medik').dom.value   = rowdata.KD_UNIT_ASAL;
        }else{
            Ext.get('txtNamaUnitKasirRM').dom.value = rowdata.NAMA_UNIT;
            Ext.get('txtKdUnitRehab_Medik').dom.value   = rowdata.KD_UNIT;
        }
        
        tmpkd_unit_asal=rowdata.KD_UNIT_ASAL;
    }
    else
    {
		// Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').enable();
		Ext.getCmp('cboDokterPengirimKasirRM').setValue(rowdata.DOKTER);
        Ext.getCmp('cboDOKTER_viKasir_RehabMedik').setValue('');
        // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').setValue('');
        //tmpkd_unit = '';
        Ext.get('txtNamaUnitKasirRM').dom.value = rowdata.NAMA_UNIT_ASLI;
        tmpkddoktertujuan = '';
        tmpkddokterpengirim = rowdata.KD_DOKTER;
        kd_kasir_KasirRM = '';
        tmpkd_unit_asal= rowdata.KD_UNIT;
        Ext.get('txtKdUnitRehab_Medik').dom.value   = rowdata.KD_UNIT;
        Ext.getCmp('btnPembayaranPenjasKasirRM').disable();
        Ext.getCmp('btnCatatanHasilPenjasKasirRM').disable();
        Ext.getCmp('btnHpsBrsKasirKasirRM').disable();
        Ext.getCmp('btnTutupTransaksiPenjasKasirRM').disable();
        kodeUnitRehab_Medik=rowdata.KD_UNIT;
    }
    var modul='';
    if(radiovaluesKasirRM == 1){
        modul='igd';
    } else if(radiovaluesKasirRM == 2){
        modul='rwi';
    } else if(radiovaluesKasirRM == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
	kodepasien = rowdata.KD_PASIEN;
    namapasien = rowdata.NAMA;
    namaunit = rowdata.NAMA_UNIT;
    kdcustomeraa = rowdata.KD_CUSTOMER;
    Ext.Ajax.request({
        url: baseURL + "index.php/rehab_medik/functionKasirRM/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul,
            kdkasir: rowdata.KD_KASIR
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            console.log(cst.ListDataObj);
            
            kodepay = cst.ListDataObj[0].kd_pay;
            uraianpay = cst.ListDataObj[0].cara_bayar;
            kdkasir = cst.ListDataObj[0].kd_kasir;
            kdcustomeraa = rowdata.KD_CUSTOMER;
            //Ext.get('txtNomorRehab_Medik').dom.value =cst.ListDataObj[0].no_register;
            
        }

    });
    
    Ext.get('dtpTtlKasirRM').dom.value=ShowDate(rowdata.TGL_LAHIR);
    var tahunLahir= ShowDate(rowdata.TGL_LAHIR);
    var tahunLahirSplit= tahunLahir.split('/');
    var tahunSekarang=nowTglTransaksiGrid.format("Y");
    var selisihTahunLahir=tahunSekarang-tahunLahirSplit[2];
    console.log(rowdata);
    Ext.get('txtUmurRehab_Medik').dom.value=selisihTahunLahir;
    Ext.get('txtNoMedrecKasirRM').dom.value= rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienKasirRM').dom.value = rowdata.NAMA;
    Ext.get('txtAlamatKasirRM').dom.value = rowdata.ALAMAT;
    Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
    
   
    Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
    
    jenisKelaminPasien=rowdata.JENIS_KELAMIN;       
    Ext.get('cboJKKasirRM').dom.value = rowdata.JENIS_KELAMIN;
    if (rowdata.JENIS_KELAMIN === 't')
    {
        Ext.get('cboJKKasirRM').dom.value= 'Laki-laki';
        
    }else
    {
        Ext.get('cboJKKasirRM').dom.value= 'Perempuan'
        
    }
    Ext.getCmp('cboJKKasirRM').disable();
    
    Ext.get('cboGDRKasirRM').dom.value = rowdata.GOL_DARAH;
    if (rowdata.GOL_DARAH === '0')
    {
        Ext.get('cboGDRKasirRM').dom.value= '-';
    }else if (rowdata.GOL_DARAH === '1')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'A+'
    }else if (rowdata.GOL_DARAH === '2')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'B+'
    }else if (rowdata.GOL_DARAH === '3')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'AB+'
    }else if (rowdata.GOL_DARAH === '4')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'O+'
    }else if (rowdata.GOL_DARAH === '5')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'A-'
    }else if (rowdata.GOL_DARAH === '6')
    {
        Ext.get('cboGDRKasirRM').dom.value= 'B-'
    }
    Ext.getCmp('cboGDRKasirRM').disable();
    
    Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
    vkode_customer = rowdata.KD_CUSTOMER;
    Ext.getCmp('cboKelPasienRehab_Medik').disable();
    
    Ext.get('cboKelPasienRehab_Medik').dom.value= rowdata.KELPASIEN;
    Combo_Select(rowdata.KELPASIEN);
    
    kelompokPasien= rowdata.KELPASIEN;
    Ext.getCmp('cboKelPasienRehab_Medik').enable();
    Ext.get('txtCustomerLamaHide').hide();
    if(rowdata.KELPASIEN == 'Perseorangan'){
        Ext.getCmp('cboPerseoranganRehab_Medik').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboPerseoranganRehab_Medik').show();
    } else if(rowdata.KELPASIEN == 'Perusahaan'){
        Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').show();
    } else{
        Ext.getCmp('cboAsuransiRehab_Medik').setValue(rowdata.CUSTOMER);
        Ext.getCmp('cboAsuransiRehab_Medik').show();
    }

    
    Ext.getCmp('txtnotlpKasirRM').setValue(rowdata.TELEPON);
    Ext.getCmp('txtNoSJPRehab_Medik').setValue(rowdata.SJP); 
        
    
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/getcurrentshift",
         params: {
            command: '0',
        },
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            tampungshiftsekarang=o.responseText
            
        }
        
    
    });
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
            Modul:modul
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            //Ext.getCmp('cboUnitLab_PA_viPenJasLab_PA').setValue(cst.kd_unit);
            getComboDokterRehab_Medik(cst.kd_unit);
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            //tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/rehab_medik/functionKasirRM/getTarifMir",
                params: {
                    kd_unit:tmpkd_unit_asal,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customer,
                     penjas:modul,
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    });
    namaCustomer_LIS=rowdata.CUSTOMER;
    namaKelompokPasien_LIS=rowdata.KELPASIEN;
    
    
};


function KasirRMAddNew() 
{
    Ext.getCmp('btnTambahBaruKasirRM').show();
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/rehab_medik/functionKasirRM/getCurrentShiftLab",
        params: {
            command: '0',
            // parameter untuk url yang dituju (fungsi didalam controller)
        },
        failure: function(o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function(o) {
            tampungshiftsekarang=o.responseText;
        }
    });
    AddNewPenJasRad = true;
    Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value = '';
    Ext.get('txtNoMedrecKasirRM').dom.value='';
    Ext.get('txtNamaPasienKasirRM').dom.value = '';
    //Ext.get('cboDOKTER_viKasir_RehabMedik').dom.value = '';
    // Ext.get('cboUnitRehab_Medik_viKasir_RehabMedik').dom.value = '';
    Ext.get('cboJKKasirRM').dom.value = '';
    Ext.get('cboGDRKasirRM').dom.value = '';
    Ext.get('txtUmurRehab_Medik').dom.value = '';
    Ext.getCmp('dtpTtlKasirRM').setValue(now)
    Ext.get('txtnotlpKasirRM').dom.value = '';
    Ext.get('txtKdDokter').dom.value   = undefined;
    Ext.get('txtNamaUnitKasirRM').dom.value   = 'Rehab Medik';
    //Ext.getCmp('txtDokterPengirimKasirRM').setValue('DOKTER LUAR');
    Ext.getCmp('cboDokterPengirimKasirRM').setValue('DOKTER LUAR');
    tmpkddokterpengirim = '';
    Ext.get('txtKdUrutMasuk').dom.value = '';
    rowSelectedKasir_RehabMedik=undefined;
    dsTRDetailKasir_RehabMedikList.removeAll();
    
    databaru = 1;
    Ext.get('txtNamaUnitKasirRM').readOnly;
    Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/getUnitDefault",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').setValue(cst.kd_unit);
            getComboDokterRehab_Medik(cst.kd_unit);
            //Ext.getCmp('cbounitrads_viPenJasRad').setValue(cst.kd_unit);
            //combovaluesunittujuan=cst.kd_unit;
            tmpkd_unit = cst.kd_unit;
            
            Ext.Ajax.request({
                url: baseURL + "index.php/rehab_medik/functionKasirRM/getTarifMir",
                params: {
                    kd_unit:cst.kd_unit,
                     //kd_unit_asal:tmpkd_unit_asal,
                     kd_customer:vkode_customer,
                     penjas:'langsung',
                     kdunittujuan:cst.kd_unit
                },
                failure: function (o)
                {
                    var cst = Ext.decode(o.responseText);
                },
                success: function (o) {
                    var cst = Ext.decode(o.responseText);
                    console.log(cst.ListDataObj);
                    tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                }

            });
        }

    }); 

};

function RefreshDataKasir_RehabMedikDetail(no_transaksi) 
{
    var strKriteriaKasirRM='';
    //strKriteriaKasirRM = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='4'";
    //strKriteriaKasirRM = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasir_RehabMedikList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailRWJGridBawah',
                param: strKriteriaKasirRM
            }
        }
    );
    return dsTRDetailKasir_RehabMedikList;
};

//tampil grid bawah penjas KasirRM
function ViewGridBawahLookupKasir_RehabMedik(no_transaksi,kd_kasir,tgl_transaksis) 
{
    var strKriteriaKasirRM='';
     if (kd_kasir !== undefined) {
		if (tgl_transaksis == 'ALL'){
			strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
		}else{
			strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~ and tgl_transaksi = " + "~" + tgl_transaksis + "~";
		}
        
    }else{
        //strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
		if (tgl_transaksis == 'ALL'){
			strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + "";
		}else{
			strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and tgl_transaksi = " + "~" + tgl_transaksis + "~";
		}
    }
   // strKriteriaKasirRM = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
 
   
    dsTRDetailKasir_RehabMedikList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target: 'ViewDetailGridBawahPenJasLab',
                param: strKriteriaKasirRM
            }
        }
    );
    return dsTRDetailKasir_RehabMedikList;
};

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirRM(tgl_jadwal_flex) 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovaluesKasirRM == 1){
        modul='igd';
    } else if(radiovaluesKasirRM == 2){
        modul='rwi';
    } else if(radiovaluesKasirRM == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    /* if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perseorangan'){
        KdCust=Ext.getCmp('cboPerseoranganRehab_Medik').getValue();
    }else if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perusahaan'){
        KdCust=Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').getValue();
    }else {
        KdCust=Ext.getCmp('cboAsuransiRehab_Medik').getValue();
    } */
    
    if(Ext.getCmp('txtNoMedrecKasirRM').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    //alert(tmpkd_unit);
    var params =
    {
        //Table:'ViewDetailTransaksiKasir_RehabMedik',
        
        KdTransaksi: Ext.get('txtNoTransaksiKasir_RehabMedik').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecKasirRM').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienKasirRM').getValue(),
        Ttl:Ext.getCmp('dtpTtlKasirRM').getValue(),
        Alamat:Ext.getCmp('txtAlamatKasirRM').getValue(),
        JK:Ext.getCmp('cboJKKasirRM').getValue(),
        GolDarah:Ext.getCmp('cboGDRKasirRM').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRehab_Medik').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdDokterPengirim:tmpkddokterpengirim,
        KdUnitTujuan:tmpkd_unit,
        //Tgl: Ext.getCmp('dtpKunjunganKasirRM').getValue(),
        Tgl: tgl_jadwal_flex,
        TglTransaksiAsal:TglTransaksi,
        Tlp: Ext.getCmp('txtnotlpKasirRM').getValue(),
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRehab_Medik').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRehab_Medik').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrKasir_RehabMedik(),
        JmlField: mRecordKasirRM.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiKasirRM(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:setting_kd_unit,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter    : '',
        Modul:modul,
        KdProduk:'',//sTRDetailKasir_RehabMedikList.data.items[CurrentKasir_RehabMedik.row].data.KD_PRODUK
        URUT :tmpurut,
        //ketHasilRehabMedik : Ext.getCmp('cboKeteranganHasilRehab_Medik').getValue(),
        //nomorKasirRM : Ext.getCmp('txtNomorRehab_Medik').getValue()
    };
    return params
};

function getParamHapusDetailTransaksiKasirRM() 
{   
    var tmpparam = cellSelecteddeskripsi.data;
    var line = gridDTItemTest.getSelectionModel().selection.cell[0];
    var params =
    {       
        no_tr: tmpparam.no_transaksi,
        urut:tmpparam.urut,
        tgl_transaksi:tmpparam.tgl_transaksi,
        //ABULHASSAN--------------20_01_2017
        no_trans_cadangan:Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue(),
        kd_produk: dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_produk,
        kd_kasir: dsTRDetailKasir_RehabMedikList.data.items[line].data.kd_kasir,
    };
    return params
};

function getParamKonsultasi() 
{

    var params =
    {
        
        Table:'ViewDetailTransaksiPenJasLab', //data access listnya belum dibuat
        
        //TrKodeTranskasi: Ext.get('txtNoTransaksiKasir_RehabMedik').getValue(),
        KdUnitAsal : Ext.get('txtKdUnitRehab_Medik').getValue(),
        KdDokterAsal : Ext.get('txtKdDokter').getValue(),
        KdUnit: selectKlinikPoli,
        KdDokter:selectDokter,
        KdPasien:Ext.get('txtNoMedrecKasirRM').getValue(),
        TglTransaksi : Ext.get('dtpKunjunganKasirRM').dom.value,
        KDCustomer :vkode_customer,
    };
    return params
};

function GetListCountDetailTransaksiKasirRM()
{
    
    var x=0;
    for(var i = 0 ; i < dsTRDetailKasir_RehabMedikList.getCount();i++)
    {
        if (dsTRDetailKasir_RehabMedikList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasir_RehabMedikList.data.items[i].data.DESKRIPSI  != '')
        {
            x += 1;
        };
    }
    return x;
    
};


function getArrKasir_RehabMedik()
{
    //console.log(dsTRDetailKasir_RehabMedikList.data);
    var abc=dsTRDetailKasir_RehabMedikList.data;
    var x='';
    var arr=[];
    for(var i = 0 ; i < dsTRDetailKasir_RehabMedikList.getCount();i++)
    {
        if (dsTRDetailKasir_RehabMedikList.data.items[i].data.kd_produk != undefined && dsTRDetailKasir_RehabMedikList.data.items[i].data.deskripsi != undefined && dsTRDetailKasir_RehabMedikList.data.items[i].data.kd_produk != '' && dsTRDetailKasir_RehabMedikList.data.items[i].data.deskripsi != '')
        {
            /* var y='';
            var z='@@##$$@@';
            
            y =  dsTRDetailKasir_RehabMedikList.data.items[i].data.URUT
            y += z + dsTRDetailKasir_RehabMedikList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasir_RehabMedikList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailKasir_RehabMedikList.data.items[i].data.TGL_BERLAKU)
            y += z +dsTRDetailKasir_RehabMedikList.data.items[i].data.HARGA
            y += z +dsTRDetailKasir_RehabMedikList.data.items[i].data.KD_TARIF
            y += z +dsTRDetailKasir_RehabMedikList.data.items[i].data.URUT
            
            
            if (i === (dsTRDetailKasir_RehabMedikList.getCount()-1))
            {
                x += y 
            }
            else
            {
                x += y + '##[[]]##'
            }; */
            var o={};
            var y='';
            var z='@@##$$@@';
            o['URUT']= dsTRDetailKasir_RehabMedikList.data.items[i].data.urut;
            o['KD_PRODUK']= dsTRDetailKasir_RehabMedikList.data.items[i].data.kd_produk;
            o['QTY']= dsTRDetailKasir_RehabMedikList.data.items[i].data.qty;
            o['TGL_TRANSAKSI']= dsTRDetailKasir_RehabMedikList.data.items[i].data.tgl_transaksi;
            o['TGL_BERLAKU']= dsTRDetailKasir_RehabMedikList.data.items[i].data.tgl_berlaku;
            o['HARGA']= dsTRDetailKasir_RehabMedikList.data.items[i].data.harga;
            o['KD_TARIF']= dsTRDetailKasir_RehabMedikList.data.items[i].data.kd_tarif;
            o['cito']= dsTRDetailKasir_RehabMedikList.data.items[i].data.cito;
            o['kd_unit']= dsTRDetailKasir_RehabMedikList.data.items[i].data.kd_unit;
            //--------------ABULHASSAN--------------20_01_2017
            o['DESKRIPSI']= dsTRDetailKasir_RehabMedikList.data.items[i].data.deskripsi;
            arr.push(o);
            //console.log(abc.items[i].data);
        };
    }   
    
    return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelKasir_RehabMedik(lebar) 
{
    //pengaturan panel data pasien
    var items =
    {
        layout: 'fit',
        anchor: '100%',
        width: lebar-35,
        labelAlign: 'right',
        bodyStyle: 'padding: 5px 5px 5px 5px',
        border:false,
        height:255,
        items:
        [
                    { //awal panel biodata pasien
                        columnWidth:.99,
                        layout: 'absolute',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        width: 100,
                        height: 120,
                        anchor: '100% 100%',
                        items:
                        [
                            //bagian pinggir kiri                            
                            {
                                x: 10,
                                y: 10,
                                xtype: 'label',
                                text: 'No. Transaksi  '
                            },
                            {
                                x: 100,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x : 110,
                                y : 10,
                                xtype: 'textfield',
                                name: 'txtNoTransaksiKasir_RehabMedik',
                                id: 'txtNoTransaksiKasir_RehabMedik',
                                width: 100,
                                emptyText:nmNomorOtomatis,
                                readOnly:true

                            },
                            {
                                x: 10,
                                y: 40,
                                xtype: 'label',
                                text: 'No. Medrec  '
                            },
                            {
                                x: 100,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: 'No. Medrec',
                                name: 'txtNoMedrecKasirRM',
                                id: 'txtNoMedrecKasirRM',
                                readOnly:true,
                                width: 120,
                                emptyText:'Otomatis',
                                listeners: 
                                { 
                                    
                                }
                            },
                            {
                                x: 10,
                                y: 70,
                                xtype: 'label',
                                text: 'Unit  '
                            },
                            {
                                x: 100,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 110,
                                y: 70,
                                xtype: 'textfield',
                                name: 'txtNamaUnitKasirRM',
                                id: 'txtNamaUnitKasirRM',
                                readOnly:true,
                                width: 120
                            },
                            {
                                x: 10,
                                y: 100,
                                xtype: 'label',
                                text: 'Dokter Pengirim'
                            },
                            {
                                x: 100,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },mComboDokterPengirim()
                            /* {
                                x: 110,
                                y: 100,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtDokterPengirimKasirRM',
                                id: 'txtDokterPengirimKasirRM',
                                width: 200
                            } */,
                            // {
                            //     x: 10,
                            //     y: 130,
                            //     xtype: 'label',
                            //     text: 'Unit Tujuan'
                            // },
                            // {
                            //     x: 100,
                            //     y: 130,
                            //     xtype: 'label',
                            //     text: ':'
                            // },
                            // mComboUnitRehab_Medik(),
                            {
                                x: 360,
                                y: 130,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 400,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 410,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlpKasirRM',
                                id: 'txtnotlpKasirRM',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('cboDOKTER_viKasir_RehabMedik').focus();
                                        }
                                    }
                                }
                            },
                            //bagian tengah
                            {
                                x: 260,
                                y: 10,
                                xtype: 'label',
                                text: 'Tanggal Kunjung '
                            },
                            {
                                x: 350,
                                y: 10,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 10,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal hari ini ',
                                id: 'dtpKunjunganKasirRM',
                                name: 'dtpKunjunganKasirRM',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110
                            },
                            {
                                x: 260,
                                y: 40,
                                xtype: 'label',
                                text: 'Nama Pasien '
                            },
                            {
                                x: 350,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtNamaPasienKasirRM',
                                id: 'txtNamaPasienKasirRM',
                                width: 200,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            Ext.getCmp('dtpTtlKasirRM').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 260,
                                y: 70,
                                xtype: 'label',
                                text: 'Alamat '
                            },
                            {
                                x: 350,
                                y: 70,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 360,
                                y: 70,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtAlamatKasirRM',
                                id: 'txtAlamatKasirRM',
                                width: 395,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            if (combovalues === 'Transaksi Lama')
                                            {
                                                Ext.getCmp('txtnotlpKasirRM').focus();
                                            }
                                            else
                                            {
                                                Ext.getCmp('cboJKKasirRM').focus();
                                            }
                                            
                                        }
                                    }
                                }
                            },
                            {
                                x: 360,
                                y: 100,
                                xtype: 'label',
                                text: 'Jenis Kelamin '
                            },
                            {
                                x: 445,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboJK(),
                            {
                                x: 570,
                                y: 100,
                                xtype: 'label',
                                text: 'Gol. Darah '
                            },
                            {
                                x: 640,
                                y: 100,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboGDarah(),
                            {
                                x: 570,
                                y: 40,
                                xtype: 'label',
                                text: 'Tanggal lahir '
                            },
                            {
                                x: 640,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 645,
                                y: 40,
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                selectOnFocus: true,
                                forceSelection: true,
                                id: 'dtpTtlKasirRM',
                                name: 'dtpTtlKasirRM',
                                format: 'd/M/Y',
                                //readOnly : true,
                                value: now,
                                width: 110,
                                enableKeyEvents: true,
                                listeners:{
                                    'specialkey': function (){
                                        if (Ext.EventObject.getKey() === 13){
                                            var tmptanggal = Ext.get('dtpTtlKasirRM').getValue();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetUmur_LAB",
                                                params: {
                                                    TanggalLahir: tmptanggal
                                                },
                                                success: function (o){
                                                    var tmphasil = o.responseText;
                                                    var tmp = tmphasil.split(' ');
                                                    if (tmp.length == 6){
                                                        Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 4){
                                                        if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }else{
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 2 ){
                                                        if (tmp[1] == 'year' ){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'years' ){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue(tmp[0]);
                                                        }else if (tmp[1] == 'mon'  ){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue('0');
                                                        }else if (tmp[1] == 'mons'  ){
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue('0');
                                                        }else{
                                                            Ext.getCmp('txtUmurRehab_Medik').setValue('0');
                                                        }
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else if(tmp.length == 1){
                                                        Ext.getCmp('txtUmurRehab_Medik').setValue('0');
                                                        getParamBalikanHitungUmur(tmptanggal);
                                                    }else{
                                                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                    }   
                                                }
                                            });
                                            Ext.getCmp('txtAlamatKasirRM').focus();
                                        }
                                    }
                                }
                            },
                            {
                                x: 765,
                                y: 40,
                                xtype: 'label',
                                text: 'Umur '
                            },
                            {
                                x: 795,
                                y: 40,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 805,
                                y: 40,
                                xtype: 'textfield',
                                fieldLabel: '',
                                name: 'txtUmurRehab_Medik',
                                id: 'txtUmurRehab_Medik',
                                width: 30,
                                //anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {
                                                    Ext.Ajax.request
                                                    (
                                                            {
                                                                url: baseURL + "index.php/main/functionLAB/cekUsia",
                                                                params: {umur: Ext.get('txtUmurRehab_Medik').getValue()},
                                                                failure: function (o)
                                                                {
                                                                    ShowPesanErrorKasir_RehabMedik('Proses Error ! ', 'Kasir Rehab Medik');
                                                                },
                                                                success: function (o)
                                                                {
                                                                    var cst = Ext.decode(o.responseText);
                                                                    if (cst.success === true)
                                                                    {
                                                                        Ext.getCmp('dtpTtlKasirRM').setValue(cst.tahunumur);
                                                                    }
                                                                    else
                                                                    {
                                                                        ShowPesanErrorKasir_RehabMedik('Proses Error ! ', 'Kasir Rehab Medik');
                                                                    }
                                                                }
                                                            }
                                                    )
                                                }
                                            }
                                        }
                            },
                            {
                                x: 10,
                                y: 160,
                                xtype: 'label',
                                text: 'Dokter  '
                            },
                            {
                                x: 100,
                                y: 160,
                                xtype: 'label',
                                text: ':'
                            },
                            mComboDOKTER(),
                            {
                                x: 10,
                                y: 185,
                                xtype: 'label',
                                text: 'Kelompok Pasien'
                            },
                            {
                                x: 100,
                                y: 185,
                                xtype: 'label',
                                text: ':'
                            },
                            
                            /* {
                                x: 360,
                                y: 185,
                                xtype: 'label',
                                text: 'No. SEP'
                            },
                            {
                                x: 400,
                                y: 185,
                                xtype: 'label',
                                text: ':'
                            }, */
                            mCboKelompokpasien(),
                            mComboPerseorangan(),
                            mComboPerusahaan(),
                            mComboAsuransi(),
                            {
                                x: 415,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'Nama Peserta',
                                maxLength: 200,
                                name: 'txtNamaPesertaAsuransiRehab_Medik',
                                id: 'txtNamaPesertaAsuransiRehab_Medik',
                                emptyText:'Nama Peserta Asuransi',
                                width: 125
                            },
                            {
                                x: 545,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'No. Askes',
                                maxLength: 200,
                                name: 'txtNoAskesRehab_Medik',
                                id: 'txtNoAskesRehab_Medik',
                                emptyText:'No Askes',
                                width: 125
                            },
                            {
                                x: 675,
                                y: 185,
                                xtype: 'textfield',
                                fieldLabel: 'No. SEP',
                                maxLength: 200,
                                name: 'txtNoSJPRehab_Medik',
                                id: 'txtNoSJPRehab_Medik',
                                emptyText:'No SEP',
                                width: 125
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtCustomerLamaHide',
                                id: 'txtCustomerLamaHide',
                                readOnly:true,
								hidden:true,
                                width: 280
                            },
                            {
                                x: 110,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel:'Kode customer  ',
                                name: 'txtKdCustomerLamaHide',
                                id: 'txtKdCustomerLamaHide',
                                readOnly:true,
                                hidden:true,
                                width: 280
                            },
                            /* {
                                x: 10,
                                y: 210,
                                xtype: 'label',
                                text: 'Kode Nomor'
                            },
                            {
                                x: 100,
                                y: 210,
                                xtype: 'label',
                                text: ':'
                            }, */
                            /* mCboKeteranganHasilRehabMedik(),
                            {
                                x: 230,
                                y: 210,
                                xtype: 'label',
                                text: 'Nomor RehabMedik'
                            },
                            {
                                x: 300,
                                y: 210,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 310,
                                y: 210,
                                xtype: 'textfield',
                                maxLength: 200,
                                name: 'txtNomorRehab_Medik',
                                id: 'txtNomorRehab_Medik',
                                emptyText:'Nomor RehabMedik',
                                width: 125
                            },{
								x: 450,
                                y: 210,
								xtype: 'button',
								id: 'btnSimpanNoRehabMedikPenjasKasirRM',
								text: 'Simpan Nomor RehabMedik',
								tooltip: nmEditData,
								//iconCls: 'Edit_Tr',
								//hidden: true,
								handler: function (sm, row, rec)
								{
									Ext.Ajax.request
										(
											{
												url: baseURL +  "index.php/rehab_medik/functionKasirRM/UpdateNoLab",   
												params: {
															KDPASIEN  		: Ext.getCmp('txtNoMedrecKasirRM').getValue(),
															KDUNIT     		: tmpkd_unit,
															TGLTRANSAKSI   	: Ext.getCmp('dtpKunjunganKasirRM').getValue(),
															NOLAB    		: Ext.getCmp('txtNomorRehab_Medik').getValue(),
												},
												failure: function(o)
												{
													ShowPesanWarningKasir_RehabMedik('Simpan Nomor LAB gagal', 'Gagal');
												},  
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														 
														ShowPesanInfoKasir_RehabMedik("Mengganti Nomor LAB berhasil", "Success");

													}else 
													{
														ShowPesanWarningKasir_RehabMedik('Simpan Nomor LAB gagal', 'Gagal');
													};
												}
											}
										);
								}//, disabled: true
							}, */
                            //---------------------hidden----------------------------------
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Dokter  ',
                                name: 'txtKdDokter',
                                id: 'txtKdDokter',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            
                            {
                                xtype: 'textfield',
                                fieldLabel:'Unit  ',
                                name: 'txtKdUnitRehab_Medik',
                                id: 'txtKdUnitRehab_Medik',
                                readOnly:true,
                                hidden:true,
                                anchor: '99%'
                            },
                            {
                                xtype: 'textfield',
                                name: 'txtKdUrutMasuk',
                                id: 'txtKdUrutMasuk',
                                readOnly:true,
                                hidden:true,
                                anchor: '100%'
                            }
                                
                            //-------------------------------------------------------------
                            
                        ]
                    },  //akhir panel biodata pasien
            
        ]
    };
    return items;
};


function getParamBalikanHitungUmur(tgl){
    Ext.getCmp('dtpTtlKasirRM').setValue(tgl);
}
function getItemPanelUnit(lebar) 
{
    var items =
    {
        layout: 'column',
        border: false,
        items:
        [
            {
                columnWidth: .40,
                layout: 'form',
                labelWidth:100,
                border: false,
                items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel:'Unit  ',
                        name: 'txtKdUnitRehab_Medik',
                        id: 'txtKdUnitRehab_Medik',
                        readOnly:true,
                        anchor: '99%'
                    }
                ]
            },
            {
                columnWidth: .60,
                layout: 'form',
                border: false,
                labelWidth:1,
                items:
                [
                    {
                        xtype: 'textfield',
                        name: 'txtNamaUnitKasirRM',
                        id: 'txtNamaUnitKasirRM',
                        readOnly:true,
                        anchor: '100%'
                    }
                ]
            }
        ]
    }
    return items;
};

function refeshpenjasKasirRM(kriteria)
{
    /* dsTRKasir_RehabMedikList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasLab',
                    param : kriteria
                }           
            }
        );   
    return dsTRKasir_RehabMedikList; */
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/lab/viewpenjaslab/getPasien",
            params: getParamRefreshRehab_Medik(kriteria),
            failure: function(o)
            {
                ShowPesanWarningKasir_RehabMedik('Hubungi Admin', 'Error');
            },
            success: function(o)
            {   
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    dsTRKasir_RehabMedikList.removeAll();
                    var recs=[],
                    recType=dsTRKasir_RehabMedikList.recordType;
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dsTRKasir_RehabMedikList.add(recs);
                    // grListKasir_RehabMedik.getView().refresh();

                }
                else
                {
                    ShowPesanWarningKasir_RehabMedik('Gagal membaca Data ini', 'Error');
                };
            }
        }

    );
}


function getParamRefreshRehab_Medik(krite){
    var unit;
    if(radiovaluesKasirRM == 1){
        unit = 'IGD';
    } else if(radiovaluesKasirRM == 2){
        unit = 'RWI';
    } else if(radiovaluesKasirRM == 3){
        unit = 'Langsung';
    } else{
        unit = 'RWJ';
    }
    var params =
    {
        unit:unit,
        kriteria:krite
        /* tglAwal:
        tglAkhir:dtpTglAwalFilterKasir_RehabMedik */
        
    }
    return params;
}

function loadpenjasKasirRM(tmpparams, tmpunit)
{
    dsTRKasir_RehabMedikList.load
        (
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: tmpunit,
                            param : tmpparams
                        }           
                    }
        );   
    return dsTRKasir_RehabMedikList;
}

function Datasave_Kasir_RehabMedik(mBol,tgl_jadwalnya) 
{   
	//alert(tgl_jadwalnya);
     if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/rehab_medik/functionKasirRM/save_detail",
                    params: getParamDetailTransaksiKasirRM(tgl_jadwalnya),
                    failure: function(o)
                    {
                        ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                    },  
                    success: function(o) 
                    {
                        
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            loadMask.hide();
                            ShowPesanInfoKasir_RehabMedik("Berhasil menyimpan data ini","Information");
                            kd_kasir_KasirRM = cst.kdkasir;
                            
                            Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value=cst.notrans;
                            Ext.getCmp('cboDataJadwal_KASIRRM').setValue('ALL');
                            Ext.get('txtNoMedrecKasirRM').dom.value=cst.kdPasien;
                            ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,cst.kdkasir,'ALL');
                            Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
                            Ext.getCmp('btnCatatanHasilPenjasKasirRM').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,cst.kdkasir,'ALL');
                            };
                            loaddatastoredaftar_jadwal(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value, cst.kdkasir)
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
							
							vKd_Pasien= cst.kdPasien;
							vKdUnit= cst.kd_unit_tujuan;
							vTanggal= cst.tgl;
							vUrutMasuk= cst.urut;
                            if (Ext.get('cboJenisTr_viKasir_RehabMedik').getValue()!='Transaksi Baru' && radiovaluesKasirRM != '3')
                            {
                                validasiJenisTrKasirRM();
                            }
                            //
                            //coba disatukan di function php yang sama
                            //Datasave_Kasir_RehabMedik_SQL();
                            //Datasave_KasirRM_LIS();
                        }
                        else 
                        {
                                ShowPesanWarningKasir_RehabMedik('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_Kasir_RehabMedik_LangsungTambahBaris(mBol) 
{   
	var tgl_untuk_jadwal= Ext.getCmp('dtpKunjunganKasirRM').getValue().format("Y-m-d");
     if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
     {
         loadMask.show();
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/rehab_medik/functionKasirRM/save_detail",
                    params: getParamDetailTransaksiKasirRM(tgl_untuk_jadwal),
                    failure: function(o)
                    {
                        ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                    },  
                    success: function(o) 
                    {
                        
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            loadMask.hide();
                            //ShowPesanInfoKasir_RehabMedik("Berhasil menyimpan data ini","Information");
                            kd_kasir_KasirRM = cst.kdkasir;
                            loaddatastoredaftar_jadwal(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value, cst.kdkasir)
                            Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value=cst.notrans;
                            Ext.get('txtNoMedrecKasirRM').dom.value=cst.kdPasien;
                            //ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,cst.kdkasir);
                            Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
                            Ext.getCmp('btnCatatanHasilPenjasKasirRM').enable();
                            Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
                            /* if(mBol === false)
                            {
                                ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,cst.kdkasir);
                            }; */
                            
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
                            if (Ext.get('cboJenisTr_viKasir_RehabMedik').getValue()!='Transaksi Baru' && radiovaluesKasirRM != '3')
                            {
                                validasiJenisTrKasirRM();
                            }
                            //
                            //coba disatukan di function php yang sama
                            //Datasave_Kasir_RehabMedik_SQL();
                            //Datasave_KasirRM_LIS();
                        }
                        else 
                        {
                                ShowPesanWarningKasir_RehabMedik('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';

function DataHapus_Kasir_RehabMedik(mBol) 
{   
     if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/rehab_medik/functionKasirRM/deletedetaillab",
                    params: getParamHapusDetailTransaksiKasirRM(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoKasir_RehabMedik('Data Berhasil Di hapus', 'Sukses');
                            
                        }
                        else 
                        {
							if (cst.cari !== false || cst.cari !== 'false'){
								ShowPesanWarningKasir_RehabMedik(cst.cari, 'Gagal');
								  
							}else
							{
								ShowPesanWarningKasir_RehabMedik('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
							}
                              
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};


function Dataupdate_PenJasRad(mBol) 
{   
    if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
    {
        
            Ext.Ajax.request
             (
                {
                    //url: "./Datapool.mvc/CreateDataObj",
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDataupdatePenJasRadDetail(),
                    success: function(o) 
                    {
                        ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            ShowPesanInfoKasir_RehabMedik(nmPesanSimpanSukses,nmHeaderSimpanData);
                            //RefreshDataPenJasRad();
                            if(mBol === false)
                            {
                                ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM,'ALL');
                            };
                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                            ShowPesanWarningKasir_RehabMedik(nmPesanSimpanGagal,nmHeaderSimpanData);
                        }
                        else if (cst.success === false && cst.pesan===1)
                        {
                            ShowPesanWarningKasir_RehabMedik(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
                        }
                        else 
                        {
                            ShowPesanErrorKasir_RehabMedik(nmPesanSimpanError,nmHeaderSimpanData);
                        };
                    }
                }
            )
        
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
};
function mComboUnitTujuans_viKasir_RehabMedik(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitKasirRMs_viKasir_RehabMedik = new WebApp.DataStore({ fields: Field });
    dsunitKasirRMs_viKasir_RehabMedik.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '7' and kd_unit in('74')"
        }
    });
    var cbounitKasirRMs_viKasir_RehabMedik = new Ext.form.ComboBox({
        id: 'cbounitKasirRMs_viKasir_RehabMedik',
        x: 455,
        y: 10,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        disabled:true,
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Unit',
        store: dsunitKasirRMs_viKasir_RehabMedik,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
        listeners:{
            'select': function(a,b,c){
                combovaluesunittujuan=b.data.KD_UNIT;
                validasiJenisTrKasirRM();
            }
        }
    });
    return cbounitKasirRMs_viKasir_RehabMedik;
    
};
function ValidasiEntryKasir_RehabMedik(modul,mBolHapus)
{
    var x = 1;
    // (Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').getValue() == '') ||
    if((Ext.get('txtNamaPasienKasirRM').getValue() == '') || (Ext.get('cboDOKTER_viKasir_RehabMedik').getValue() == 'Pilih DOkter') || (Ext.get('dtpKunjunganKasirRM').getValue() == '') || (dsTRDetailKasir_RehabMedikList.getCount() === 0 ))
    {
        if (Ext.get('txtNoTransaksiKasir_RehabMedik').getValue() == '' && mBolHapus === true) 
        {
            x = 0;
        }
         else if (Ext.get('txtNoMedrecKasirRM').getValue() == '') 
        {
            ShowPesanWarningKasir_RehabMedik('No. Medrec tidak boleh kosong!','Warning');
            x = 0;
        }/* else if (Ext.get('cboUnitRehab_Medik_viKasir_RehabMedik').getValue() == '') 
        {
            ShowPesanWarningKasir_RehabMedik('Rehab Medik tujuan Wajib diisi!','Warning');
            x = 0;
        }*/
        else if (Ext.get('txtNamaPasienKasirRM').getValue() == '') 
        {
            ShowPesanWarningKasir_RehabMedik('Nama tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('dtpKunjunganKasirRM').getValue() == '') 
        {
            ShowPesanWarningKasir_RehabMedik('Tanggal kunjungan tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (Ext.get('cboDOKTER_viKasir_RehabMedik').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viKasir_RehabMedik').dom.value  === undefined) 
        {
            ShowPesanWarningKasir_RehabMedik('Dokter Rehab Medik tidak boleh kosong!','Warning');
            x = 0;
        }
        else if (dsTRDetailKasir_RehabMedikList.getCount() === 0) 
        {
            ShowPesanWarningKasir_RehabMedik('Daftar item test tidak boleh kosong!','Warning');
            x = 0;
        };
    };
    return x;
};

function ShowPesanWarningKasir_RehabMedik(str, modul) 
{
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width:250
        }
    );
};

function ShowPesanErrorKasir_RehabMedik(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.ERROR,
            width:250
        }
    );
};

function ShowPesanInfoKasir_RehabMedik(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width:250
        }
    );
};


function DataDeletePenJasRad() 
{
	var tgl_untuk_jadwal= Ext.getCmp('dtpKunjunganKasirRM').getValue().format("Y-m-d");
   if (ValidasiEntryKasir_RehabMedik(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiKasirRM(tgl_untuk_jadwal),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoKasir_RehabMedik(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataPenJasRad();
                                        KasirRMAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningKasir_RehabMedik(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningKasir_RehabMedik(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorKasir_RehabMedik(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
    
    // rowSelectedKasir_RehabMedik = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
    
    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
    (
        {
            id:'cboKelompokpasien',
            typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            listeners:
            {
                'select': function(a,b,c)
                {
                    selectSetKelompokpasien=b.data.displayText ;
                    selectKdCustomer=b.data.KD_CUSTOMER;
                    selectNamaCustomer=b.data.CUSTOMER;
                
                }
            }
        }
    );
    return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
    {
        Width:lebar,
        height:40,
        layout: 'column',
        border: false,
    //  title: 'Kelompok Pasien Lama',
        
        items:
        [
            {
                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: false,
                //title: 'Kelompok Pasien Lama',
                items:
                [
                    {    
                        xtype: 'tbspacer',
                        height: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kelompok Pasien Asal',
                        //maxLength: 200,
                        name: 'txtCustomer_KasirRMLama',
                        id: 'txtCustomer_KasirRMLama',
                        labelWidth:130,
                        width: 100,
                        anchor: '95%'
                    },
                    
                    
                ]
            }
            
        ]
    }
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
    {
        Width:lebar,
        height:120,
        layout: 'column',
        border: false,
        
        
        items:
        [
            {

                columnWidth: .990,
                layout: 'form',
                Width:lebar-10,
                labelWidth:130,
                border: false,
                items:
                [
                    
                    
                    {    
                        xtype: 'tbspacer',
                        
                        height:3
                    },
                        {  

                        xtype: 'combo',
                        fieldLabel: 'Kelompok Pasien Baru',
                        id: 'kelPasien',
                         editable: false,
                        //value: 'Perseorangan',
                        store: new Ext.data.ArrayStore
                            (
                                {
                                id: 0,
                                fields:
                                [
                                'Id',
                                'displayText'
                                ],
                                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                }
                            ),
                              displayField: 'displayText',
                              mode: 'local',
                              width: 100,
                              forceSelection: true,
                              triggerAction: 'all',
                              emptyText: 'Pilih Salah Satu...',
                              selectOnFocus: true,
                              anchor: '95%',
                              listeners:
                                {
                                        'select': function(a, b, c)
                                    {
                                        if(b.data.displayText =='Perseorangan')
                                        {
                                            jeniscus='0'
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Perusahaan')
                                        {
                                            jeniscus='1';
                                            Ext.getCmp('txtNoSJP').disable();
                                            Ext.getCmp('txtNoAskes').disable();}
                                        else if(b.data.displayText =='Asuransi')
                                        {
                                            jeniscus='2';
                                            Ext.getCmp('txtNoSJP').enable();
                                            Ext.getCmp('txtNoAskes').enable();
                                        }
                                    
                                        RefreshDatacombo(jeniscus);
                                    }

                                }
                        }, 
                        {
                            columnWidth: .990,
                            layout: 'form',
                            border: false,
                            labelWidth:130,
                            items:
                            [
                                mComboKelompokpasien()
                            ]
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. SJP',
                            maxLength: 200,
                            name: 'txtNoSJP',
                            id: 'txtNoSJP',
                            width: 100,
                            anchor: '95%'
                        },
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Askes',
                            maxLength: 200,
                            name: 'txtNoAskes',
                            id: 'txtNoAskes',
                            width: 100,
                            anchor: '95%'
                         }
                        
                        //mComboKelompokpasien
        
        
                ]
            }
            
        ]
    }
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJKKasirRM = new Ext.form.ComboBox
    (
        {
            id:'cboJKKasirRM',
            x: 455,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Jenis Kelamin',
            fieldLabel: 'Jenis Kelamin ',
            editable: false,
            width:95,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetJK,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('cboGDRKasirRM').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetJK=b.data.Id ;
                    jenisKelaminPasien=b.data.Id ;
                    console.log(jenisKelaminPasien);
                },
/*              'render': function(c) {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir').focus();
                                    }, c);
                                }
 */         }
        }
    );
    return cboJKKasirRM;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRKasirRM = new Ext.form.ComboBox
    (
        {
            id:'cboGDRKasirRM',
            x: 645,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            tabIndex:3,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Gol. Darah',
            fieldLabel: 'Gol. Darah ',
            width:50,
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                        'Id',
                        'displayText'
                    ],
                data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetGDarah,
            enableKeyEvents: true,
            listeners:
            {
                'specialkey': function (){
                    if (Ext.EventObject.getKey() === 13){
                        Ext.getCmp('txtnotlpKasirRM').focus();
                    }
                },
                'select': function(a,b,c)
                {
                    selectSetGDarah=b.data.displayText ;
                },
            }
        }
    );
    return cboGDRKasirRM;
};

//Combo Dokter Lookup
function getComboDokterRehab_Medik(kdUnit)
{
    dsdokter_viKasir_RehabMedik.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param: "kd_unit = '"+kdUnit+"'"
        }
    });
    return dsdokter_viKasir_RehabMedik;
}
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];
    dsdokter_viKasir_RehabMedik = new WebApp.DataStore({ fields: Field });
    dsdokter_viKasir_RehabMedik.load
    ({
        params:
        {
            Skip: 0,
            Take: 1000,
            Sort: 'kd_dokter',
            Sortdir: 'ASC',
            target: 'ViewDokterPenunjang',
            param:"kd_unit in ("+setting_kd_unit+")",
        }
    });
    
    var cboDOKTER_viKasir_RehabMedik = new Ext.form.ComboBox
    (
            {
                id: 'cboDOKTER_viKasir_RehabMedik',
                x: 110,
                y: 160,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 230,
                emptyText:'Pilih Dokter',
                store: dsdokter_viKasir_RehabMedik,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
               // value:'All',
                editable: false,
                enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            Ext.getCmp('cboKelPasienRehab_Medik').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    
                    tmpkddoktertujuan=b.data.KD_DOKTER ;
                    Ext.getCmp('cboKelPasienRehab_Medik').focus();
                },
            }
            }
    );
    
    return cboDOKTER_viKasir_RehabMedik;
};
var tmpkddoktertujuan;

function mCboKelompokpasien(){  
 var cboKelPasienRehab_Medik = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienRehab_Medik',
            fieldLabel: 'Kelompok Pasien',
            x: 110,
            y: 185,
            mode: 'local',
            width: 130,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:selectSetKelPasien,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (a, b, c){
                        if (Ext.EventObject.getKey() === 13){
                            if(kelompokPasien == 'Perseorangan' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 1)
                           {
                              // cboPerseorangan
                                //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                                //Ext.getCmp('cboRujukanRequestEntry').hide();
                                Ext.getCmp('cboPerseoranganRehab_Medik').focus(false,100);
                           }
                           else if(kelompokPasien == 'Perusahaan' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 2)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show(); 
                                Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').focus(false,100);
                           }
                           else if(kelompokPasien == 'Asuransi' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 3)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show();  
                                 Ext.getCmp('cboAsuransiRehab_Medik').focus(false,100);
                           }
                        }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganRehab_Medik').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiRehab_Medik').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    
                    Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').hide();
                    Ext.getCmp('txtNoAskesRehab_Medik').hide();
                    Ext.getCmp('txtNoSJPRehab_Medik').hide();
                    
                    Ext.getCmp('cboPerseoranganRehab_Medik').hide();
                    Ext.getCmp('cboAsuransiRehab_Medik').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').hide();
                    
                }
            }

        }
    );
    return cboKelPasienRehab_Medik;
};

function mCboKelompokpasien_Depan(){  
 var cboKelPasienDepanRehab_Medik = new Ext.form.ComboBox
    (
        {
            
            id:'cboKelPasienDepanRehab_Medik',
            fieldLabel: 'Kelompok Pasien',
            x: 155,
            y: 100,
            mode: 'local',
            width: 130,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[0, 'Semua'],[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:'Semua',
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (a, b, c){
                        if (Ext.EventObject.getKey() === 13){
                            if(kelompokPasien == 'Perseorangan' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 1)
                           {
                              // cboPerseorangan
                                //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                                //Ext.getCmp('cboRujukanRequestEntry').hide();
                                Ext.getCmp('cboPerseoranganRehabMedikDepan').focus(false,100);
                           }
                           else if(kelompokPasien == 'Perusahaan' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 2)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show(); 
                                Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').focus(false,100);
                           }
                           else if(kelompokPasien == 'Asuransi' || Ext.getCmp('cboKelPasienRehab_Medik').getValue() == 3)
                           {
                                //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                                //Ext.getCmp('cboRujukanRequestEntry').show();  
                                 Ext.getCmp('cboAsuransiRehabMedikDepan').focus(false,100);
                           }
                        }
                    },
                'select': function(a, b, c)
                    {
                       Combo_Select_Depan(b.data.displayText);
                       if(b.data.displayText == 'Perseorangan')
                       {
                          // cboPerseorangan
                            //Ext.getCmp('cboRujukanDariRequestEntry').hide();
                            //Ext.getCmp('cboRujukanRequestEntry').hide();
                            Ext.getCmp('cboPerseoranganRehabMedikDepan').focus(false,100);
                       }
                       else if(b.data.displayText == 'Perusahaan')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show(); 
                            Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').focus(false,100);
                       }
                       else if(b.data.displayText == 'Asuransi')
                       {
                            //Ext.getCmp('cboRujukanDariRequestEntryRehab_Medik').show();
                            //Ext.getCmp('cboRujukanRequestEntry').show();  
                             Ext.getCmp('cboAsuransiRehabMedikDepan').focus(false,100);
                       }
                      
                    },
                'render': function(a,b,c)
                {
                    Ext.getCmp('cboPerseoranganRehabMedikDepan').hide();
                    Ext.getCmp('cboAsuransiRehabMedikDepan').hide();
                    Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').hide();
                    
                } 
            }

        }
    );
    return cboKelPasienDepanRehab_Medik;
};
function mCboKeteranganHasilRehabMedik(){  
 var cboKeteranganHasilRehab_Medik = new Ext.form.ComboBox
    (
        {
            
            id:'cboKeteranganHasilRehab_Medik',
            fieldLabel: 'Ket. Hasil RehabMedik',
            x: 110,
            y: 210,
            mode: 'local',
            width: 100,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[1, 'MF'], [2, 'MT'], [3, 'MI'], [4, 'MS'], [5, 'K'], [6, 'VC'], [7, 'SVC']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:1,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                
            }

        }
    );
    return cboKeteranganHasilRehab_Medik;
};

function mCboKeteranganHasilRehabMedikFilterDepan(){  
 var cboKeteranganHasilRehab_MedikFilterDepan = new Ext.form.ComboBox
    (
        {
            
            id:'cboKeteranganHasilRehab_MedikFilterDepan',
            fieldLabel: 'Ket. Hasil Rehab Medik',
            x: 155,
            y: 100,
            mode: 'local',
            disabled: true,
            width: 100,
            forceSelection: true,
            lazyRender:true,
            triggerAction: 'all',
            editable: false,
            store: new Ext.data.ArrayStore
            (
                {
                id: 0,
                fields:
                [
                    'Id',
                    'displayText'
                ],
                   data: [[0,'Semua'],[1, 'MF'], [2, 'MT'], [3, 'MI'], [4, 'MS'], [5, 'K']]
                }
            ),          
            valueField: 'Id',
            displayField: 'displayText',
            value:0,
            selectOnFocus: true,
            tabIndex:22,
            enableKeyEvents: true,
                
            listeners:
            {
                'select': function(a,b,c)
                {
                    KodeNomorRM_PA=b.data.displayText;
                }
            }

        }
    );
    return cboKeteranganHasilRehab_MedikFilterDepan;
};

function mComboPerseorangan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRehab_MedikRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganRehab_MedikRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganRehab_Medik = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganRehab_Medik',
            x: 260,
            y: 185,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganRehab_MedikRequestEntry,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ;
                }
            }
        }
    );
    return cboPerseoranganRehab_Medik;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryRehab_Medik = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryRehab_Medik',
            x: 260,
            y: 185,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntry,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            listeners:
            {
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerusahaan=b.data.valueField ;
                }
            }
        }
    );

    return cboPerusahaanRequestEntryRehab_Medik;
};

function mComboPerseoranganDepan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganRehab_MedikRequestEntryDepan = new WebApp.DataStore({fields: Field});
    dsPeroranganRehab_MedikRequestEntryDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=0'
            }
        }
    )
    var cboPerseoranganRehabMedikDepan = new Ext.form.ComboBox
    (
        {
            id:'cboPerseoranganRehabMedikDepan',
            x: 305,
            y: 100,
            editable: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Jenis',
            tabIndex:23,
            width:150,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            store:dsPeroranganRehab_MedikRequestEntryDepan,
            value:selectSetPerseorangan,
            listeners:
            {
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerseorangan=b.data.displayText ; */
                }
            }
        }
    );
    return cboPerseoranganRehabMedikDepan;
};

function mComboPerusahaanDepan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntryDepan = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntryDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=1'
            }
        }
    )
    var cboPerusahaanRequestEntryRehabMedikDepan = new Ext.form.ComboBox
    (
        {
            id: 'cboPerusahaanRequestEntryRehabMedikDepan',
            x: 305,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Perusahaan...',
            fieldLabel: 'Perusahaan',
            align: 'Right',
            width:150,
            store: dsPerusahaanRequestEntryDepan,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            //anchor: '95%',
            value:'Semua',
            listeners:
            {
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetPerusahaan=b.data.valueField ; */
                }
            }
        }
    );

    return cboPerusahaanRequestEntryRehabMedikDepan;
};

function mComboAsuransiDepan()
{
var Field_poli_viDaftarDepan = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftarDepan = new WebApp.DataStore({fields: Field_poli_viDaftarDepan});

    ds_customer_viDaftarDepan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
           /*  params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            } */
        }
    )
    var cboAsuransiRehabMedikDepan = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiRehabMedikDepan',
            x: 305,
            y: 100,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            //anchor: '95%',
            store: ds_customer_viDaftarDepan,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            enableKeyEvents: true,
            value:'Semua',  
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            //Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    kodeCustomerFilterDepan=b.data.KD_CUSTOMER;
                    /* vkode_customer = b.data.KD_CUSTOMER;
                    selectSetAsuransi=b.data.valueField ; */
                }
            }
        }
    );
    return cboAsuransiRehabMedikDepan;
};

function mComboUnitRehab_Medik(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitKasirRM_viKasir_RehabMedik= new WebApp.DataStore({ fields: Field });
    dsunitKasirRM_viKasir_RehabMedik.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama_unit',
            Sortdir: 'ASC',
            target: 'ViewSetupUnit',
            param: "parent = '7' and kd_unit in('74')"
        }
    });
    var cbounitKasirRM_viKasir_RehabMedik = new Ext.form.ComboBox({
        id: 'cboUnitRehab_Medik_viKasir_RehabMedik',
        x: 110,
        y: 130,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Unit',
        store: dsunitKasirRM_viKasir_RehabMedik,
        valueField: 'KD_UNIT',
        displayField: 'NAMA_UNIT',
        //value:'All',
        editable: false,
            listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkd_unit = b.data.KD_UNIT;
                    getComboDokterRehab_Medik(b.data.KD_UNIT);
                    if (Ext.getCmp('cboDokterPengirimKasirRM').getValue()=='DOKTER LUAR')
                    {
                        Ext.Ajax.request({
                            url: baseURL + "index.php/rehab_medik/functionKasirRM/getTarifMir",
                            params: {
                                kd_unit:b.data.KD_UNIT,
                                 //kd_unit_asal:tmpkd_unit_asal,
                                 kd_customer:vkode_customer,
                                 penjas:'langsung',
                                 kdunittujuan:b.data.KD_UNIT
                            },
                            failure: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                            },
                            success: function (o) {
                                var cst = Ext.decode(o.responseText);
                                console.log(cst.ListDataObj);
                                tmpkd_unit_tarif_mir=cst.kd_unit_tarif_mir;
                            }

                        });
                    }
                }
            }
    });
    return cbounitKasirRM_viKasir_RehabMedik;
}
function mComboDokterPengirim(){ 
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry= new WebApp.DataStore({ fields: Field });
    dsDokterRequestEntry.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'nama',
            Sortdir: 'ASC',
            target: 'ViewComboDokterPengirim',
            param: ''
        }
    })
    
    var cboDokterPengirimKasirRM = new Ext.form.ComboBox({
        id: 'cboDokterPengirimKasirRM',
        x: 110,
        y: 100,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        emptyText: '',
        fieldLabel:  ' ',
        align: 'Right',
        width: 230,
        emptyText:'Pilih Dokter Pengirim',
        store: dsDokterRequestEntry,
        valueField: 'KD_DOKTER',
        displayField: 'NAMA',
        //value:'All',
        //editable: false,
            listeners:
            {
                'select': function(a,b,c)
                {
                    tmpkddokterpengirim=b.data.KD_DOKTER;
                    /* tmpkd_unit = b.data.KD_UNIT;
                    getComboDokterRehab_Medik(b.data.KD_UNIT); */
                }
            }
    });
    return cboDokterPengirimKasirRM;
}

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target: 'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
           /*  params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            } */
        }
    )
    var cboAsuransiRehab_Medik = new Ext.form.ComboBox
    (
        {
            id:'cboAsuransiRehab_Medik',
            x: 260,
            y: 185,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Asuransi...',
            fieldLabel: 'Asuransi',
            align: 'Right',
            width:150,
            //anchor: '95%',
            store: ds_customer_viDaftar,
            valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
            enableKeyEvents: true,
                
            listeners:
            {
                'specialkey': function (){
                        if (Ext.EventObject.getKey() === 13){
                            // Ext.getCmp('cboUnitRehab_Medik_viKasir_RehabMedik').focus();
                        }
                    },
                'select': function(a,b,c)
                {
                    vkode_customer = b.data.KD_CUSTOMER;
                    selectSetAsuransi=b.data.valueField ;
                }
            }
        }
    );
    return cboAsuransiRehab_Medik;
};


function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').hide()
        Ext.getCmp('txtNoAskesRehab_Medik').hide()
        Ext.getCmp('txtNoSJPRehab_Medik').hide()
        Ext.getCmp('cboPerseoranganRehab_Medik').show()
        Ext.getCmp('cboAsuransiRehab_Medik').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').hide()
        Ext.getCmp('txtNoAskesRehab_Medik').hide()
        Ext.getCmp('txtNoSJPRehab_Medik').hide()
        Ext.getCmp('cboPerseoranganRehab_Medik').hide()
        Ext.getCmp('cboAsuransiRehab_Medik').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').show()
         Ext.getCmp('txtNoAskesRehab_Medik').show()
         Ext.getCmp('txtNoSJPRehab_Medik').show()
         Ext.getCmp('cboPerseoranganRehab_Medik').hide()
         Ext.getCmp('cboAsuransiRehab_Medik').show()
         Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').hide()
         
       }
}

function Combo_Select_Depan(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik') ;

   if(value == "Perseorangan")
   {    

        Ext.getCmp('cboPerseoranganRehabMedikDepan').show()
        Ext.getCmp('cboAsuransiRehabMedikDepan').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('cboPerseoranganRehabMedikDepan').hide()
        Ext.getCmp('cboAsuransiRehabMedikDepan').hide()
        Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').show()
        
   }
   else if(value == "Asuransi")
       {
         Ext.getCmp('cboPerseoranganRehabMedikDepan').hide()
         Ext.getCmp('cboAsuransiRehabMedikDepan').show()
         Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').hide()
         
       }
    else if(value == "Semua")
       {
         Ext.getCmp('cboPerseoranganRehabMedikDepan').hide()
         Ext.getCmp('cboAsuransiRehabMedikDepan').hide()
         Ext.getCmp('cboPerusahaanRequestEntryRehabMedikDepan').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirKasirRM').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirKasirRM').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable(); 

Ext.getCmp('btnLookupKasir_RehabMedik').disable()
Ext.getCmp('btnSimpanKasirRM').disable()
Ext.getCmp('btnHpsBrsKasirRM').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirKasirRM').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirKasirRM').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();    

Ext.getCmp('btnLookupKasir_RehabMedik').enable()
Ext.getCmp('btnSimpanKasirRM').enable()
//Ext.getCmp('btnHpsBrsKasirRM').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()  
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{
         var strKriteria = "";

           /* if (Ext.get('txtNoMedrecKasir_RehabMedik').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrecKasir_RehabMedik').getValue() +"'";
            }
            
            if (Ext.get('txtNamaPasienKasir_RehabMedik').getValue() != "")//^^^
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienKasir_RehabMedik').getValue() +"%')" ;
                    }
                    else
                        {
                            strKriteria += " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienKasir_RehabMedik').getValue() +"%')";
                        }
                
            }
            if (Ext.get('cboStatusLunas_viKasir_RehabMedik').getValue() != "")
            {
                if (Ext.get('cboStatusLunas_viKasir_RehabMedik').getValue()==='Belum Lunas')
                {
                    if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "= 'f'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "= 'f'";
                       }
                }
                if (Ext.get('cboStatusLunas_viKasir_RehabMedik').getValue()==='Posting')
                {
                    if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "='t'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "='t'";
                       }
                }
                if (Ext.get('cboStatusLunas_viKasir_RehabMedik').getValue()==='Semua')
                {
                    
                }
                
                
            } */
            if (Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue()+"'";
                        }
                
            }
            if (Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterKasir_RehabMedik').getValue()+"'";
                        }
                
            }
            /* if (Ext.get('cboUNIT_viKasirRehab_Medik').getValue() != "")
            {
                    if (strKriteria == "")
                    {
                        strKriteria = " unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirRehab_Medik').getValue() + "'"  ;
                    }
                    else
                       {
                            strKriteria += " and unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirRehab_Medik').getValue() + "'";
                       }
                
            } */
    
     return strKriteria;
}

function getItemPanelPenJasRehabMedikDepan()
{
    //var tmptruefalse = true;
    var items =
    {
        layout:'column',
        border:true,
		width: 1300,
        items:
        [
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: '100%',
                height: 100,
                anchor: '50% 50%',
                items:
                [
                    //-----------COMBO JENIS TRANSAKSI-----------------------------
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Jenis Transaksi '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboJenisTrans_viKasir_RehabMedik(),
                    /* {
                        x: 310,
                        y: 10,
                        xtype: 'label',
                        text: 'Unit Lab '
                    },
                    {
                        x: 445,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    mComboUnitTujuans_viKasir_RehabMedik(), */
                    //--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 40,
                        xtype: 'radiogroup',
                        id: 'rbrujukanKasirRM',
                        fieldLabel: 'Asal Pasien ',
                        items: [
                                    {
                                        boxLabel: 'IGD',
                                        name: 'rb_autoKasirRM',
                                        id: 'rb_pilihanKasirRM1',
                                        checked: true,
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
												Ext.getCmp('dtpTglAwalFilterKasir_RehabMedik').setValue(now);
												Ext.getCmp('dtpTglAkhirFilterKasir_RehabMedik').setValue(now);
                                                getDataCariUnitKasir_RehabMedik("kd_bagian=3 and parent<>'0'");
                                                Ext.getCmp('cboUNIT_viKasirRehab_Medik').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												//--------------TAMBAH BARU 27-SEPTEMBER-2017
												Ext.getCmp('btnEditDataPasienKasirRM').disable();
                                                radiovaluesKasirRM='1';
                                                validasiJenisTrKasirRM();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasLab';
                                                loadpenjasKasirRM(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWJ',
                                        name: 'rb_autoKasirRM',
                                        id: 'rb_pilihanKasirRM4',
                                        inputValue: '1',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
												Ext.getCmp('dtpTglAwalFilterKasir_RehabMedik').setValue(now);
												Ext.getCmp('dtpTglAkhirFilterKasir_RehabMedik').setValue(now);
                                                getDataCariUnitKasir_RehabMedik("kd_bagian=2 and type_unit=false");
                                                Ext.getCmp('cboUNIT_viKasirRehab_Medik').show();
                                                Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
                                                radiovaluesKasirRM='4';
												//--------------TAMBAH BARU 27-SEPTEMBER-2017
												Ext.getCmp('btnEditDataPasienKasirRM').disable();
                                                validasiJenisTrKasirRM();
                                                /* tmpparams = "'" + Ext.get('dtpTglAwalFilterKasir_RehabMedik').getValue() + "'";//tanya
                                                tmpunit = 'ViewRwjPenjasLab';
                                                loadpenjasKasirRM(tmpparams, tmpunit); */
                                        }
                                        }
                                    },
                                    {
                                        boxLabel: 'RWI',
                                        name: 'rb_autoKasirRM',
                                        id: 'rb_pilihanKasirRM2',
                                        inputValue: '2',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
											Ext.getCmp('dtpTglAwalFilterKasir_RehabMedik').setValue(now);
											Ext.getCmp('dtpTglAkhirFilterKasir_RehabMedik').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirRehab_Medik').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            radiovaluesKasirRM='2';
											//--------------TAMBAH BARU 27-SEPTEMBER-2017
											Ext.getCmp('btnEditDataPasienKasirRM').disable();
                                            validasiJenisTrKasirRM();
                                            /* tmpparams = " transaksi.co_Status='0' ORDER BY transaksi.no_transaksi desc limit 100";
                                            tmpunit = 'ViewRwiPenjasLab';
                                            loadpenjasKasirRM(tmpparams, tmpunit); */
                                        }
                                     }
                                    },
                                    {
                                        boxLabel: 'Kunj. Langsung',
                                        name: 'rb_autoKasirRM',
                                        id: 'rb_pilihanKasirRM3',
										hidden : true,
                                        inputValue: 'K.Kd_Pasien',
                                        handler: function (field, value) {
                                        if (value === true)
                                        {
											Ext.getCmp('dtpTglAwalFilterKasir_RehabMedik').setValue(now);
											Ext.getCmp('dtpTglAkhirFilterKasir_RehabMedik').setValue(now);
                                            Ext.getCmp('cboUNIT_viKasirRehab_Medik').hide();
                                            Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
                                            radiovaluesKasirRM='3';
											//--------------TAMBAH BARU 27-SEPTEMBER-2017
											if (combovalues==='Transaksi Lama'){
												Ext.getCmp('btnEditDataPasienKasirRM').enable();
											}else{
												Ext.getCmp('btnEditDataPasienKasirRM').disable();
											}
                                            validasiJenisTrKasirRM();
                                            //Kasir_RehabMedikLookUp();
                                            //belum
                                            Ext.getCmp('txtNamaUnitKasirRM').setValue="Rehab Medik";
                                        }
                                    }   
                                    },
                                ]
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Detail Asal Pasien '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    //COMBO POLI dan KAMAR
                    mComboUnit_viKasirKasirRM(),
                    mcomboKamarSpesialKasirRM(),
                    
                ]
            },
            //---------------JARAK 1----------------------------
             {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 100,
                height: 5,
                anchor: '50% 50%',
                items:
                [
                        
                ]
             },
            //--------------------------------------------------
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: '100%',
                height: 190,
                anchor: '100% 100%',
                items:
                [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
                    {
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecKasir_RehabMedik',
                        id: 'txtNoMedrecKasir_RehabMedik',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecKasir_RehabMedik').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecKasir_RehabMedik').getValue())
                                             Ext.getCmp('txtNoMedrecKasir_RehabMedik').setValue(tmpgetNoMedrec);
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshpenjasKasirRM(tmpkriteria); */
                                             validasiJenisTrKasirRM();
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshpenjasKasirRM(tmpkriteria); */
                                                        validasiJenisTrKasirRM();
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecKasir_RehabMedik').setValue('')
                                                    validasiJenisTrKasirRM();
                                            }
                                }
                            }

                        }

                    },  
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
                    {
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienKasir_RehabMedik',
                        id: 'txtNamaPasienKasir_RehabMedik',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
                                            //getCriteriaFilter_viDaftar();
                                            validasiJenisTrKasirRM();
                                            /* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshpenjasKasirRM(tmpkriteria); */

                                            //RefreshDataFilterPenJasRad();

                                        }                       
                                }
                        }
                    },
                    /* {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mComboStatusBayar_viKasir_RehabMedik(), */
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
                    {
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mComboStatusLunas_viKasir_RehabMedik(),
                    /* {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kode Nomor '
                    },
                    {
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },                  
                    mCboKeteranganHasilRehabMedikFilterDepan(), */
                    
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelompok Pasien '
                    },
                    {
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },
                    mCboKelompokpasien_Depan(),
                    mComboPerusahaanDepan(),
                    mComboPerseoranganDepan(),
                    mComboAsuransiDepan(),
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
                    {
                        x: 145,
                        y: 130,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 130,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterKasir_RehabMedik',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
                    {
                        x: 270,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 305,
                        y: 130,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterKasir_RehabMedik',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
                    {
                        x: 415,
                        y: 130,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            validasiJenisTrKasirRM();
                            /* tmpparams = getCriteriaFilter_viDaftar();
                            refeshpenjasKasirRM(tmpparams); */
                        }
                    }
                    
                ]
            },
            
        //----------------------------------------------------------------  
            
            {
                columnWidth:.10,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 10,
                height: 135,
                anchor: '100% 100%',
                items:
                [
                    
                ]
            },
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanRehab_Medik(){
    var kd_cus_gettarif=vkode_customer;
    /* if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perseorangan'){
        kd_cus_gettarif=Ext.getCmp('cboPerseoranganRehab_Medik').getValue();
    }else if(Ext.get('cboKelPasienRehab_Medik').getValue()=='Perusahaan'){
        kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryRehab_Medik').getValue();
    }else {
        kd_cus_gettarif=Ext.getCmp('cboAsuransiRehab_Medik').getValue();
      } */

    var modul='';
    if(radiovaluesKasirRM == 1){
        modul='igd';
    } else if(radiovaluesKasirRM == 2){
        modul='rwi';
    } else if(radiovaluesKasirRM == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/rehab_medik/functionKasirRM/getGridProduk",
            params: {
                    kd_unit        :rowSelectedKasir_RehabMedik.data.KD_UNIT,
                    kd_customer    :rowSelectedKasir_RehabMedik.data.KD_CUSTOMER,
                    notrans        :Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue(),
                    penjas         :modul,
                    kdunittujuan   :tmpkd_unit
                    },
            failure: function(o)
            {
                ShowPesanErrorKasir_RehabMedik('Error, pasien! Hubungi Admin', 'Error');
            },  
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                
                if (cst.success === true) 
                {
                    
                    var recs=[],
                        recType=dsDataGrd_getTindakanKasir_RehabMedik.recordType;
                        
                    for(var i=0; i<cst.ListDataObj.length; i++){
                
                        recs.push(new recType(cst.ListDataObj[i]));
                        
                    }
                    dsDataGrd_getTindakanKasir_RehabMedik.add(recs);
                    
                    GridGetTindakanKasir_RehabMedik.getView().refresh();
                    
                    for(var i = 0 ; i < dsDataGrd_getTindakanKasir_RehabMedik.getCount();i++)
                    {
                        var o= dsDataGrd_getTindakanKasir_RehabMedik.getRange()[i].data;
                        for(var je = 0 ; je < dsTRDetailKasir_RehabMedikList.getCount();je++)
                        {
                            
                            var p= dsTRDetailKasir_RehabMedikList.getRange()[je].data;
                            if (o.kd_produk === p.kd_produk)
                            {
                                o.pilihchkprodukKasirRM = true;
                                
                            }
                            
                        }
                    }
                    GridGetTindakanKasir_RehabMedik.getView().refresh();
                }
                else 
                {
                    ShowPesanErrorKasir_RehabMedik('Gagal membaca data pasien ini', 'Error');
                };
            }
        }
        
    )
    
}

function setLookUp_getTindakanKasir_RehabMedik(rowdata){
    var lebar = 985;
    setLookUps_getTindakanKasir_RehabMedik = new Ext.Window({
        id: 'FormLookUpGetTindakan',
        title: 'Daftar Pemeriksaan', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
        autoScroll: false,
        border: false,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,        
        items: getFormItemEntry_getTindakanKasir_RehabMedik(lebar,rowdata),
        listeners:{
            activate: function(){
                
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanKasir_RehabMedik=undefined;
            }
        }
    });

    setLookUps_getTindakanKasir_RehabMedik.show();
   
}


function getFormItemEntry_getTindakanKasir_RehabMedik(lebar,rowdata){
    var pnlFormDataBasic_getTindakanKasir_RehabMedik = new Ext.FormPanel({
        title: '',
        region: 'north',
        layout: 'form',
        bodyStyle: 'padding:10px 10px 10px 10px',
        anchor: '100%',
        labelWidth: 1,
        autoWidth: true,
        width: lebar,
        border: false,
        items:[
                gridDataViewEdit_getTindakanKasir_RehabMedik()
            ],
            fileUpload: true,
            tbar: {
                xtype: 'toolbar',
                items: 
                [
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal',
						id: 'LabelTglJadwal_getTindakanKasir_RehabMedik',
						name: 'LabelTglJadwal_getTindakanKasir_RehabMedik',
						width: 100,
						format: 'd/m/Y',
						readOnly:true,
						value: kirimanTanggalDariFormLookUpBuatJadwal,
					}
					
					,
                    {
                        xtype: 'button',
                        text: 'Tambahkan',
                        id:'btnTambahKanPermintaan_getTindakanKasir_RehabMedik',
                        iconCls: 'add',
                        //disabled:true,
                        handler:function()
                        {
                            /* var jumlah=0;
                            var items=[];
                            for(var i = 0 ; i < dsDataGrd_getTindakanKasir_RehabMedik.getCount();i++){
                                var o=dsDataGrd_getTindakanKasir_RehabMedik.getRange()[i].data;
                                    if(o.pilihchkprodukKasirRM == true){
                                        jumlah += 1;
                                        
                                        // for(var x = 0; x < 100; x++){
                                            // arr[x] = [];    
                                            // for(var y = 0; y < 100; y++){ 
                                                // arr[x][y] = x*y;    
                                            // }    
                                        // }
                                        var recs=[],
                                            recType=dsTRDetailKasir_RehabMedikList.recordType;

                                        for(var i=0; i<dsDataGrd_getTindakanKasir_RehabMedik.getCount(); i++){

                                            recs.push(new recType(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data));

                                        }
                                        dsTRDetailKasir_RehabMedikList.add(recs);

                                        gridDTItemTest.getView().refresh();
                                    }
                            }
                            console.log(items[0][0]) */
                            
                            
                            
                            /* var tmp_nomer_lab = Ext.getCmp('txtNomorRehab_Medik').getValue();
                            if (tmp_nomer_lab.length > 0) { */
								//alert(Ext.getCmp('LabelTglJadwal_getTindakanKasir_RehabMedik').getValue().format("Y-m-d"));
								var tgl_jadwal_lookup = Ext.getCmp('LabelTglJadwal_getTindakanKasir_RehabMedik').getValue().format("Y-m-d");
                                console.log(dsDataGrd_getTindakanKasir_RehabMedik.getCount());
                                dsTRDetailKasir_RehabMedikList.removeAll();
                                var recs=[],
                                recType=dsTRDetailKasir_RehabMedikList.recordType;
                                var adadata=false;
                                for(var i = 0 ; i < dsDataGrd_getTindakanKasir_RehabMedik.getCount();i++)
                                {
                                    console.log(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data.pilihchkprodukKasirRM);
                                    if (dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data.pilihchkprodukKasirRM === true)
                                    {
                                        console.log(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data);
                                        recs.push(new recType(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data));
                                        adadata=true;
                                    }
                                }
                                dsTRDetailKasir_RehabMedikList.add(recs);
                                gridDTItemTest.getView().refresh(); 
                                console.log(recs);
                                if (adadata===true)
                                {
                                    setLookUps_getTindakanKasir_RehabMedik.close(); 
                                    loadMask.show();
                                    Datasave_Kasir_RehabMedik(false,tgl_jadwal_lookup); 
                                    loadMask.hide();    
                                }
                                else
                                {
                                    ShowPesanWarningKasir_RehabMedik('Ceklis data item pemeriksaan  ','Rehab_Medikoratorium');
                                }
								
								
								
								
                            /* }else{
                                Ext.Msg.confirm('Warning', 'Nomer RM masih kosong, apakah anda yakin untuk melanjutkan pembayaran?', function(button){
                                    if (button == 'yes'){
                                        console.log(dsDataGrd_getTindakanKasir_RehabMedik.getCount());
                                        dsTRDetailKasir_RehabMedikList.removeAll();
                                        var recs=[],
                                        recType=dsTRDetailKasir_RehabMedikList.recordType;
                                        var adadata=false;
                                        for(var i = 0 ; i < dsDataGrd_getTindakanKasir_RehabMedik.getCount();i++)
                                        {
                                            console.log(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data.pilihchkprodukKasirRM);
                                            if (dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data.pilihchkprodukKasirRM === true)
                                            {
                                                console.log(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data);
                                                recs.push(new recType(dsDataGrd_getTindakanKasir_RehabMedik.data.items[i].data));
                                                adadata=true;
                                            }
                                        }
                                        dsTRDetailKasir_RehabMedikList.add(recs);
                                        gridDTItemTest.getView().refresh(); 
                                        console.log(recs);
                                        if (adadata===true)
                                        {
                                            setLookUps_getTindakanKasir_RehabMedik.close(); 
                                            loadMask.show();
                                            Datasave_Kasir_RehabMedik(false); 
                                            loadMask.hide();    
                                        }
                                        else
                                        {
                                            ShowPesanWarningKasir_RehabMedik('Ceklis data item pemeriksaan  ','Rehab_Medikoratorium');
                                        }
                                    }else{
                                        Ext.getCmp('txtNomorRehab_Medik').focus();
                                    }
                                });
                            } */                           
                        }
                          
                    },
                    
                    
                ]
            }//,items:
        }
    )

    return pnlFormDataBasic_getTindakanKasir_RehabMedik;
}

function RefreshDatahistoribayar_KasirRM(no_transaksi)
{
    var strKriteriaKasirKasirRM = '';

    strKriteriaKasirKasirRM = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir= ~'+kdkasir+'~';

    dsTRDetailHistoryList_KasirRM.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirKasirRM
                                }
                    }
            );
    return dsTRDetailHistoryList_KasirRM;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetailKasirRM = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_KasirRM = new WebApp.DataStore({fields: fldDetailKasirRM})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_KasirRM,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_KasirRM.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                            Ext.getCmp('btnHpsBrsKasirKasirRM').enable();
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirKasirRM',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_KasirRM.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningPenJasRehabMedik('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirKasirRM').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirKasirRM').disable();
                                            Ext.getCmp('btnHpsBrsKasirKasirRM').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirKasirRMKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirKasirRM('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }  
             });  */
            msg_box_alasanhapus_KasirRM();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_KasirRM()
{
    var lebar = 250;
    form_msg_box_alasanhapus_KasirRM = new Ext.Window
            (
                    {
                        id: 'alasan_hapusLAb',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanHapusKasirRM',
                                                    id: 'txtAlasanHapusKasirRM',
                                                    emptyText: 'Alasan Hapus',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirKasirRMKasirDetail();
                                                                        form_msg_box_alasanhapus_KasirRM.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_KasirRM.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_KasirRM.show();
}
;

function msg_box_alasanbataltrans_KasirRM()
{
    var lebar = 250;
    form_msg_box_alasanbataltrans_KasirRM = new Ext.Window
            (
                    {
                        id: 'alasan_bataltransLAb',
                        title: 'Alasan Batal Transaksi',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    //fieldLabel: 'No. Transaksi ',
                                                    name: 'txtAlasanBatalTransKasirRM',
                                                    id: 'txtAlasanBatalTransKasirRM',
                                                    emptyText: 'Alasan Batal',
                                                    anchor: '99%',
                                                },
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Ok',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        var params = {
                                                                            kd_unit: rowSelectedKasir_RehabMedik.data.KD_UNIT,
                                                                            Tglkunjungan: rowSelectedKasir_RehabMedik.data.MASUK,
                                                                            Kodepasein: rowSelectedKasir_RehabMedik.data.KD_PASIEN,
                                                                            urut: rowSelectedKasir_RehabMedik.data.URUT_MASUK,
                                                                            keterangan : Ext.getCmp('txtAlasanBatalTransKasirRM').getValue()
                                                                        };
                                                                        Ext.Ajax.request({
                                                                            url: baseURL + "index.php/rehab_medik/functionKasirRM/deletekunjungan",
                                                                            params: params,
                                                                            failure: function (o){
                                                                                loadMask.hide();
                                                                                ShowPesanInfoKasir_RehabMedik('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                validasiJenisTrKasirRM();
                                                                            },
                                                                            success: function (o){
                                                                                var cst = Ext.decode(o.responseText);
                                                                                if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false){
                                                                                    ShowPesanWarningKasir_RehabMedik('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true){
                                                                                    ShowPesanWarningKasir_RehabMedik('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === true){
                                                                                    validasiJenisTrKasirRM();
                                                                                    ShowPesanInfoKasir_RehabMedik('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                } else if (cst.success === false && cst.pesan === 0){
                                                                                    ShowPesanWarningKasir_RehabMedik('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                } else{
                                                                                    ShowPesanErrorKasir_RehabMedik('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                                                }
                                                                            }
                                                                        }); 
                                                                        form_msg_box_alasanbataltrans_KasirRM.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanbataltrans_KasirRM.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanbataltrans_KasirRM.show();
}
;

function mComboalasan_hapusLAb()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusLAb = new WebApp.DataStore({fields: Field});

    dsalasan_hapusLAb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusLAb = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusLAb',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusLAb,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusLAb = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusLAb;
}
;
function DataDeleteKasirKasirRMKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirKasirRMKasirDetail(),
                        success: function (o)
                        {
                            //  RefreshDatahistoribayar_KasirRM(Kdtransaksi);
                           // RefreshDataFilterKasirKasirRMKasir();
                            //RefreshDatahistoribayar_KasirRM('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasir_RehabMedik("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
                                Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
                                Ext.getCmp('btnCatatanHasilPenjasKasirRM').enable();
                                Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
                                RefreshDatahistoribayar_KasirRM(Ext.getCmp('txtNoTransaksiKasir_RehabMedik').getValue());
                                if (radiovaluesKasirRM !=='3' && combovalues !== 'Transaksi Baru'){
                                    validasiJenisTrKasirRM();
                                }
                                //alert(Kdtransaksi);                    

                                //refeshKasirKasirRMKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasir_RehabMedik(nmPesanHapusGagal, nmHeaderHapusData);
                            }	else  if (cst.success === false && cst.type === 6)
                            {
                                ShowPesanWarningKasir_RehabMedik("Transaksi sudah ditutup.", nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasir_RehabMedik(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirKasirRMKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('txtAlasanHapusKasirRM').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;


function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanKasir_RehabMedik(){
    var FieldGrdKasir_getTindakanKasir_RehabMedik = [];
    dsDataGrd_getTindakanKasir_RehabMedik= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanKasir_RehabMedik
    });
    chkgetTindakanKasir_RehabMedik = new Ext.grid.CheckColumn
        (
            {
                
                id: 'chkgetTindakanKasir_RehabMedik',
                header: 'Pilih',
                align: 'center',
                //disabled:false,
                sortable: true,
                dataIndex: 'pilihchkprodukKasirRM',
                anchor: '10% 100%',
                width: 30,
                listeners: 
                {
                    checkchange: function()
                    {
                        alert('hai');
                    }
                }
                /* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            alert('hai');
                            return true;
                            
                            break;
                        case 'f':
                            alert('fei');
                            return false;
                            break;
                    }
                    return false;
                } */  
        
            }
        ); 
    GridGetTindakanKasir_RehabMedik =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        //title: 'Dafrar Pemeriksaan',
        store: dsDataGrd_getTindakanKasir_RehabMedik,
        autoScroll: true,
        columnLines: true,
        border: false,
        height: 250,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
                (
                        {
                            singleSelect: true,
                            listeners:
                                    {
                                    }
                        }
                ),
        listeners:
                {
                    // Function saat ada event double klik maka akan muncul form view
                    rowclick: function (sm, ridx, cidx)
                    {
                        console.log(GridGetTindakanKasir_RehabMedik);
                        //Ext.getCmp('chkgetTindakanKasir_RehabMedik').setValue(true);
                    },
                    rowdblclick: function (sm, ridx, cidx)
                    {
                        
                    }

                },
        /* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanKasir_RehabMedik,
        height: 250,
        autoScroll: true,
        columnLines: true,
        border: false,
        plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanKasir_RehabMedik ],
        selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
                    /* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
                    rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
                    CurrentDataPasien_viGzPermintaanDiet
                    CurrentDataPasien_viGzPermintaanDiet.row = row;
                    CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
                    
                    dsTRDetailDietGzPermintaanDiet.removeAll();
                    getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                    
                    if(GzPermintaanDiet.vars.status_order == 'false'){
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
                        Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
                    } else{
                        Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
                        Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
                    }
                    Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
                     
                },
            }
        }), */
        //stripeRows: true,
        
        colModel:new Ext.grid.ColumnModel([
            //new Ext.grid.RowNumberer(),   
            {
                dataIndex: 'kd_produk',
                header: 'KD Produk',
                sortable: true,
                width: 70,
                hidden : true,
            },{
                dataIndex: 'kp_produk',
                header: 'Kode Produk',
                sortable: true,
                width: 70,
                 filter: {}
            },
            {
                dataIndex: 'deskripsi',
                header: 'Nama Pemeriksaan',
                width: 70,
                align:'left',
                 filter: {}
            },{
                dataIndex: 'uraian',
                header: 'Uraian',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'kd_tarif',
                header: 'Kd Tarif',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'tgl_transaksi',
                header: 'Tgl Transaksi',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'tgl_berlaku',
                header: 'Tgl Berlaku',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'harga',
                header: 'harga',
                hidden : true,
                sortable: true,
                width: 70
            },
            {
                dataIndex: 'qty',
                header: 'qty',
                width: 70,
                hidden : true,
                align:'center'
            },{
                dataIndex: 'jumlah',
                header: 'jumlah',
                width: 70,
                hidden : true,
                align:'center'
            },
            chkgetTindakanKasir_RehabMedik
        ]),
        viewConfig:{
            forceFit: true
        } 
    });
    return GridGetTindakanKasir_RehabMedik;
}

function getDokterPengirim(no_transaksi,kd_kasir){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
            params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
            failure: function(o)
            {
                ShowPesanErrorKasir_RehabMedik('Hubungi Admin', 'Error');
            },
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    //Ext.getCmp('txtDokterPengirimKasirRM').setValue(cst.nama);
                    Ext.getCmp('cboDokterPengirimKasirRM').setValue(cst.nama);
                }
                else
                {
                    ShowPesanErrorKasir_RehabMedik('Gagal membaca dokter pengirim', 'Error');
                };
            }
        }

    )
}

function printbillRadRehab_Medik()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadRehab_Medik(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningKasir_RehabMedik('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

var tmpkdkasirRadRehab_Medik = '1';
function dataparamcetakbill_vikasirDaftarRadRehab_Medik()
{
    var paramscetakbill_vikasirDaftarRadRehab_Medik =
            {
                Table: 'DirectPrintingRehabMedik',
                notrans_asal:notransaksiasal_KasirRM,
                No_TRans: Ext.get('txtNoTransaksiKasirKasirRMKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirKasirRM').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirKasirRM').getValue(),
                printer: ''
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirKasirRM').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadRehab_Medik['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadRehab_Medik['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadRehab_Medik;
    return paramscetakbill_vikasirDaftarRadRehab_Medik;
}
;

function printkwitansiRadRehab_Medik()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadRehab_Medik(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirKasirRM('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirKasirRM('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningKasir_RehabMedik('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

function dataparamcetakkwitansi_vikasirDaftarRadRehab_Medik()
{
    var paramscetakbill_vikasirDaftarRadRehab_Medik =
            {
                Table: 'DirectKwitansiRehabMedik',
                No_TRans: Ext.get('txtNoTransaksiKasirKasirRMKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirKasirRM').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirKasirRM').getValue(),
                printer: ''
            };
    var urut=[];
    var kd_produk=[];
    var items=Ext.getCmp('gridDTLTRKasirKasirRM').getStore().data.items;
    for(var i=0,iLen=items.length; i<iLen; i++){
        if(items[i].data.TAG==true){
            urut.push(items[i].data.URUT);
            kd_produk.push(items[i].data.KD_PRODUK);
        }
    }
    paramscetakbill_vikasirDaftarRadRehab_Medik['no_urut[]']=urut;
    paramscetakbill_vikasirDaftarRadRehab_Medik['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadRehab_Medik;
}
;

/* function mCombo_printer_kasirKasirRM()
{ 
 
    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirKasirRM = new WebApp.DataStore({ fields: Field });
    
    load_data_printer_kasirKasirRM();
    var cbo_printer_kasirKasirRM= new Ext.form.ComboBox
    (
        {
            id: 'cbopasienorder_printer_kasirKasirRM',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Pilih Printer',
            fieldLabel:  '',
            align: 'Right',
            width: 100,
            store: dsprinter_kasirKasirRM,
            valueField: 'name',
            displayField: 'name',
            //hideTrigger       : true,
            listeners:
            {
                                
            }
        }
    );return cbo_printer_kasirKasirRM;
}; */

function load_data_printer_kasirKasirRM(param)
{

    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getPrinter",
        params:{
            command: param
        } ,
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            //cbopasienorder_mng_apotek.store.removeAll();
                var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsprinter_kasirKasirRM.recordType;
                var o=cst['listData'][i];
                
                recs.push(new recType(o));
                dsprinter_kasirKasirRM.add(recs);
                console.log(o);
            }
        }
    });
}


function Datasave_Kasir_RehabMedik_SQL(mBol) 
{   
     if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDetailTransaksiKasirRM_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoKasir_RehabMedik("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecKasirRM').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningKasir_RehabMedik('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_KasirRM_LIS(mBol) 
{   
     /* if (ValidasiEntryKasir_RehabMedik(nmHeaderSimpanData,false) == 1 )
     { */
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionLAB/savedblis",
                    params: getParamDetailTransaksiKasirRM_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningKasir_RehabMedik('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value,kd_kasir_KasirRM);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoKasir_RehabMedik("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecKasirRM').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasKasirRM').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasKasirRM').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupKasir_RehabMedik(Ext.get('txtNoTransaksiKasir_RehabMedik').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningKasir_RehabMedik('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     /* }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    };  */
    
};

function getParamDetailTransaksiKasirRM_SQL() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovaluesKasirRM == 1){
        modul='igd';
    } else if(radiovaluesKasirRM == 2){
        modul='rwi';
    } else if(radiovaluesKasirRM == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    
    if(Ext.getCmp('txtNoMedrecKasirRM').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    var params =
    {
        Table: 'SQL_LAB',
        KdTransaksi: Ext.get('txtNoTransaksiKasir_RehabMedik').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecKasirRM').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienKasirRM').getValue(),
        Ttl:Ext.getCmp('dtpTtlKasirRM').getValue(),
        Alamat:Ext.getCmp('txtAlamatKasirRM').getValue(),
        JK:jenisKelaminPasien,
        GolDarah:Ext.getCmp('cboGDRKasirRM').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitRehab_Medik').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganKasirRM').getValue(),
        TglTransaksiAsal:TglTransaksi,
        Tlp: Ext.getCmp('txtnotlpKasirRM').getValue(),
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiRehab_Medik').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesRehab_Medik').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPRehab_Medik').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrKasir_RehabMedik(),
        JmlField: mRecordKasirRM.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiKasirRM(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter : '',
        Modul:modul,
        KdProduk:'',
        URUT :tmp_urut_sql,
        kunjungan_KasirRM:tmppasienbarulama,
        //-----------ABULHASSAN-----------20_01_2017
        kodeKasir_LIS:tmpkd_kasir_sql,
        namaCusto_LIS: namaCustomer_LIS,
        guarantorNM_LIS: namaKelompokPasien_LIS,
        kodeDokterPengirim_LIS:tmpkddokterpengirim
        
    };
    return params
};
//---- baru
function fnDlgKasirRMPasswordDulu()
{
    winKasirRMPasswordDulu = new Ext.Window
    (
        {
            id: 'winKasirRMPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgKasirRMPasswordDulu()]

        }
    );

    winKasirRMPasswordDulu.show();
};
function ItemDlgKasirRMPasswordDulu()
{
    var PnlLapKasirRMSPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapKasirRMSPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: false,
            items:
            [
                getItemLapKasirRMPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapKasirRMPasswordDulu',
                            handler: function()
                            {
								Ext.Ajax.request
								(
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/rehab_medik/functionKasirRM/cekpassword_KasirRM",
										params: {passDulu:Ext.getCmp('TxPasswordDuluKasirRM').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasir_RehabMedik('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winKasirRMPasswordDulu.close();
												GantiDokterPasienLookUp_KasirRM();
														
											}
											else
											{
												ShowPesanWarningKasir_RehabMedik('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapKasirRMPasswordDulu',
                            handler: function()
                            {
                                    winKasirRMPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapKasirRMSPasswordDulu;
};
function getItemLapKasirRMPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluKasirRM',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};

//------------------TAMBAH BARU 27-September-2017
function setLookUpGridDataView_viKasirRM(rowdata){
	var lebar = 819; // Lebar Form saat PopUp
	setLookUpsGridDataView_viKasirRM = new Ext.Window({
		id: 'setLookUpsGridDataView_viKasirRM',
		title: 'Edit Data Pasien',
		closeAction: 'destroy',
		autoScroll: true,
		width: 640,
		height: 225,
		resizable: false,
		border: false,
		plain: true,
		layout: 'fit',
		modal: true,
		items: getFormItemEntryGridDataView_viKasirRM(lebar, rowdata),
		listeners:{
			activate: function (){
			},
			afterShow: function (){
				this.activate();
			},
			deactivate: function (){
				
			}
		}
	});
	setLookUpsGridDataView_viKasirRM.show();
	datainit_viKasirRM(rowdata)
	
}
function getFormItemEntryGridDataView_viKasirRM(lebar, rowdata)
{
	var pnlFormDataWindowPopup_viKasirRM = new Ext.FormPanel
			(
					{
						title: '',
						// region: 'center',
						fileUpload: true,
						margin:true,
						// layout: 'anchor',
						// padding: '8px',
						bodyStyle: 'padding-top:5px;',
						// Tombol pada tollbar Edit Data Pasien
						tbar: {
							xtype: 'toolbar',
							items:
									[
										{
											xtype: 'button',
											text: 'Save',
											id: 'btnSimpanWindowPopup_viKasirRM',
											iconCls: 'save',
											handler: function (){
												datasave_EditDataPasienKasirRM(false); //1.1
												
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
										{
											xtype: 'button',
											text: 'Save & Close',
											id: 'btnSimpanExitWindowPopup_viKasirRM',
											iconCls: 'saveexit',
											handler: function ()
											{
												var x = datasave_EditDataPasienKasirRM(false); //1.2
												
												if (x === undefined)
												{
													setLookUpsGridDataView_viKasirRM.close();
												}
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
									]
						},
						//-------------- #items# --------------
						items:
								[
									// Isian Pada Edit Data Pasien
									{
										xtype: 'panel',
										title: '',
										layout:'form',
										margin:true,
										border:false,
										labelAlign: 'right',
										width: 610,
										items:
												[//---------------# Penampung data untuk fungsi update # ---------------
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpAgama_viKasirRM',
														name: 'TxtTmpAgama_viKasirRM',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPekerjaan_viKasirRM',
														name: 'TxtTmpPekerjaan_viKasirRM',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPendidikan_viKasirRM',
														name: 'TxtTmpPendidikan_viKasirRM',
														readOnly: true,
														hidden: true
													},
													//------------------------ end ---------------------------------------------								
													//-------------- # Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_CUSTOMER',
														id: 'TxtWindowPopup_KD_CUSTOMER_viKasirRM',
														name: 'TxtWindowPopup_KD_CUSTOMER_viKasirRM',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'URUT_MASUK',
														id: 'TxtWindowPopup_Urut_Masuk_viKasirRM',
														name: 'TxtWindowPopup_Urut_Masuk_viKasirRM',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_PASIEN',
														id: 'TxtWindowPopup_KD_PASIEN_viKasirRM',
														name: 'TxtWindowPopup_KD_PASIEN_viKasirRM',
														readOnly: true,
														hidden: true
													},
													//-------------- # End Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'No. MedRec',
														id: 'TxtWindowPopup_NO_RM_viKasirRM',
														name: 'TxtWindowPopup_NO_RM_viKasirRM',
														emptyText: 'No. MedRec',
														labelSeparator: '',
														readOnly: true,
														flex: 1,
														width: 195
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'Nama',
														id: 'TxtWindowPopup_NAMA_viKasirRM',
														name: 'TxtWindowPopup_NAMA_viKasirRM',
														emptyText: 'Nama',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},{
														xtype: 'textfield',
														fieldLabel: 'Alamat',
														id: 'TxtWindowPopup_ALAMAT_viKasirRM',
														name: 'TxtWindowPopup_ALAMAT_viKasirRM',
														emptyText: 'Alamat',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'No Tlp',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_No_Tlp_viKasirRM',
																		name: 'TxtWindowPopup_No_Tlp_viKasirRM',
																		emptyText: 'No Tlp',
																		flex: 1,
																		width: 195
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'tbspacer',
																		width: 17,
																		height: 23
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Tgl. Lahir',
																		style: {'text-align': 'right'},
																		id: '',
																		name: '',
																		flex: 1,
																		width: 58
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'datefield',
																		fieldLabel: 'Tanggal',
																		id: 'DateWindowPopup_TGL_LAHIR_viKasirRM',
																		name: 'DateWindowPopup_TGL_LAHIR_viKasirRM',
																		width: 198,
																		format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey': function (){
																				if (Ext.EventObject.getKey() === 13){
																					var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viKasirRM').getValue();
																					Ext.Ajax.request({
																						url: baseURL + "index.php/main/GetUmur_LAB",
																						params: {
																							TanggalLahir: tmptanggal
																						},
																						success: function (o){
																							var tmphasil = o.responseText;
																							var tmp = tmphasil.split(' ');
																							if (tmp.length == 6){
																								Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 4){
																								if(tmp[1]== 'years' && tmp[3] == 'day'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}else if(tmp[1]== 'years' && tmp[3] == 'days'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}else if(tmp[1]== 'year' && tmp[3] == 'days'){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}else{
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 2 ){
																								if (tmp[1] == 'year' ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}else if (tmp[1] == 'years' ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
																								}else if (tmp[1] == 'mon'  ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
																								}else if (tmp[1] == 'mons'  ){
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
																								}else{
																									Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
																								}
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else if(tmp.length == 1){
																								Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
																								getParamBalikanHitungUmurPopUp(tmptanggal);
																							}else{
																								alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
																							}	
																						}
																					});
																				}
																			},
                                                                        }
																	}
																	//-------------- ## --------------				
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Umur',
														labelSeparator: '',
														anchor: '100%',
														width: 50,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TAHUN_viKasirRM',
																		name: 'TxtWindowPopup_TAHUN_viKasirRM',
																		emptyText: 'Thn',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Thn',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	/* {
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_BULAN_viRAD',
																		name: 'TxtWindowPopup_BULAN_viRAD',
																		emptyText: 'Bln',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Bln',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_HARI_viRAD',
																		name: 'TxtWindowPopup_HARI_viRAD',
																		emptyText: 'Hari',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Hari',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	} */
																	//-------------- ## --------------			
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Jenis Kelamin',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	mComboJKKasirRM(),
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Golongan Darah',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 90
																	},
																	mComboGolDarahKasirRM()
																			//-------------- ## --------------				
																]
													},
													//-------------- ## --------------						

													//-------------- ## --------------
												]
									},
									//-------------- ## --------------
									/*{
									 xtype: 'spacer',
									 width: 10,
									 height: 1
									 },*/
									//-------------- ## --------------
									
											//-------------- ## --------------
								]
								//-------------- #items# --------------
					}
			)
	return pnlFormDataWindowPopup_viKasirRM;
}

function datainit_viKasirRM(rowdata){
	var tmpjk;
	var tmp_data;
    //addNew_viDataPasien = false;
	if (rowdata.jk === "t" || rowdata.jk === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
		Ext.get('cboJKKasirRM').dom.value= 'Laki-laki';
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
		Ext.get('cboJKKasirRM').dom.value= 'Perempuan'
    }
    ID_JENIS_KELAMIN = rowdata.jk;
    Ext.getCmp('cboGolDarahKasirRM').setValue(rowdata.goldarah);
    Ext.getCmp('TxtWindowPopup_ALAMAT_viKasirRM').setValue(rowdata.alamat);
    Ext.getCmp('TxtWindowPopup_NAMA_viKasirRM').setValue(rowdata.nama);
    Ext.getCmp('TxtWindowPopup_NO_RM_viKasirRM').setValue(rowdata.medrec);
    Ext.getCmp('TxtWindowPopup_No_Tlp_viKasirRM').setValue(rowdata.hp);
	Ext.get('DateWindowPopup_TGL_LAHIR_viKasirRM').dom.value=ShowDate(rowdata.tgl_lahir);
	setUsiaPopUp(ShowDate(rowdata.tgl_lahir));
}
function mComboGolDarahKasirRM()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarahKasirRM',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJKKasirRM()
        {
			
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJKKasirRM',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                /* c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c); */
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;
function setUsiaPopUp(Tanggal)
{
    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: Tanggal
                },
                success: function (o)
                {
                    var tmphasil = o.responseText;
                    var tmp = tmphasil.split(' ');

                    if (tmp.length === 6)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
                    } else if (tmp.length === 4) {
                        if (tmp[1] === 'years') {
                            if (tmp[3] === 'day') {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                            } else {
                                Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
                                // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
                                // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                            }
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length === 2) {
                        if (tmp[1] === 'year') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'years') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mon') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else if (tmp[1] === 'mons') {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
                            // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                            // Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length === 1) {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viKasirRM').setValue('0');
                        // Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
                        // Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
                    } else {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }
            });
}
function datasave_EditDataPasienKasirRM(mBol){  
    
	if (ValidasiEntry_viDataPasienKasirRM('Simpan Data', true) == 1){
		Ext.Ajax.request({
			url: baseURL + "index.php/main/functionKasirPenunjang/simpanEditDataPasien",
			params: dataparam_datapasviDataPasienKasirRM(),
			failure: function (o){
				ShowPesanErrorKasir_RehabMedik('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
			},
			success: function (o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true){
					ShowPesanInfoKasir_RehabMedik('Data berhasil disimpan', 'Edit Data');
					validasiJenisTrKasirRM();
					//DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
				} else if (cst.success === false && cst.pesan === 0){
					ShowPesanErrorKasir_RehabMedik('Data tidak berhasil diupdate ', 'Edit Data');
				} else{
					ShowPesanErrorKasir_RehabMedik('Data tidak berhasil diupdate. ', 'Edit Data');
				}
			}
		});
	} else{
		if (mBol === true){
			return false;
		}
	}
    
}
function ValidasiEntry_viDataPasienKasirRM(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viKasirRM').getValue() === 'Nama' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viKasirRM').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viKasirRM').getValue() === '' ||
                     (Ext.get('TxtWindowPopup_ALAMAT_viKasirRM').getValue() === ''))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viKasirRM').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viKasirRM').getValue() === ''))
        {
            ShowPesanWarningKasir_RehabMedik('Nama belum terisi', modul);
            x = 0;
        }  
    }

    return x;
}

function dataparam_datapasviDataPasienKasirRM(){
    var params_viDataPasienKasirRM ={
                //-------------- # modelist Net. # --------------
		//-------------- # textfield # --------------
		NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viKasirRM').getValue(),
		NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viKasirRM').getValue(),
		ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viKasirRM').getValue(),
		NO_TELP: Ext.getCmp('TxtWindowPopup_No_Tlp_viKasirRM').getValue(),
		//NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

		//-------------- # datefield # --------------
		TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viKasirRM').getValue(),
		//-------------- # combobox # --------------
		ID_JENIS_KELAMIN: Ext.getCmp('cboJKKasirRM').getValue(),
		ID_GOL_DARAH: Ext.getCmp('cboGolDarahKasirRM').getValue(),
		TRUESQL: false
	}
    return params_viDataPasienKasirRM
}
function getParamBalikanHitungUmurPopUp(tgl){
	Ext.getCmp('DateWindowPopup_TGL_LAHIR_viKasirRM').setValue(tgl);
}