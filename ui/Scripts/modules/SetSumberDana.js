﻿
var dsSumberDanaList;
var AddNewSumberDana=false;
var selectCountSumberDana=50;
var SumberDanaLookUps;
var rowSelectedSetupSumberDana;

CurrentPage.page=getPanelSumberDana(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSumberDana(mod_id) 
{
	
    var Field = ['Dana_Code','Description'];
    dsSumberDanaList = new WebApp.DataStore({ fields: Field });
	RefreshDataSumberDana();

    var grListSumberDana = new Ext.grid.EditorGridPanel
    (
		{ 
			id:'grListSumberDana',
			stripeRows: true,
			store: dsSumberDanaList,
			autoScroll: true,
		    columnLines: true,
			border:false,
			anchor: '100% 100%',
			sm: new Ext.grid.RowSelectionModel
            (
				{ 
					singleSelect: true,
                    listeners: 
					{ 
						rowselect: function(sm, row, rec)
						{
							rowSelectedSetupSumberDana = dsSumberDanaList.getAt(row);
						}
                    }
                }
			),
            cm: new Ext.grid.ColumnModel
            (
				[	
					new Ext.grid.RowNumberer(),
                    { 	
						id: 'KODE_SumberDana',
						header: "Code",
						dataIndex: 'Dana_Code',
						sortable: true,
						width: 30
                    },
					{ 	
						id: 'DESCRIPTION',
						header: "Description",
						dataIndex: 'Description',
						width: 200,
						sortable: true                           
					}
                ]
			),
            tbar: 
			[
				{
				    id: 'btnEditSumberDana',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetupSumberDana != undefined)
						{
							SumberDanaLookUp(rowSelectedSetupSumberDana.data);
						}
						else
						{
							SumberDanaLookUp();
						}
				    }
				},' ','-'
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsSumberDanaList,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: { forceFit: true }
        }
	); 

	
    var FormSumberDana = new Ext.Panel
    (
		{ 
			id: mod_id,
			closable:true,
            region: 'center',
            layout: 'form',
            title: 'Sumber Dana Setup',
            itemCls: 'blacklabel',
            bodyStyle: 'padding:15px',
            border: false,
            bodyStyle: 'background:#FFFFFF;',
            shadhow: true,
			margins:'0 5 5 0',
			anchor:'100%',
            iconCls: 'SetupJenisSumberDana',
            items: [grListSumberDana],
			tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSumberDanaFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Description : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Description : ',
					id: 'txtSumberDanaFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataSumberDana(),
				' ','-',
				{
				    id: 'btnRefreshSumberDana',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSumberDanaFilter();
					}
				}
			],
            listeners:
			{ 'afterrender': function()
                {   
					//Ext.getCmp('cboDESKRIPSI').store = getSumberDana();
                }
            }
        }
	); 
	
	RefreshDataSumberDana();
	return FormSumberDana
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///

function RefreshDataSumberDana()
{	
	dsSumberDanaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSumberDana, 
				Sort: 'Dana_Code', 
				Sortdir: 'DESC', 
				target:'JenisSumberDana',
				param: ''
			} 
		}
	);
	rowSelectedSetupSumberDana = undefined;
	return dsSumberDanaList;
};

function SumberDanaLookUp(rowdata)
{   
	var lebar=600;
    SumberDanaLookUps = new Ext.Window   	
    (
		{ 	
			id: 'SumberDanaLookUps',
			title: 'Sumber Dana Setup',
			closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 152,
			resizable:false,
			border: false,
			plain: true,
			layout: 'fit',
			iconCls: 'SetupJenisSumberDana',
			modal: true,
			items: getFormEntrySumberDana(lebar),
			listeners:
            { 
				activate: function() 
				{ 
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetupSumberDana=undefined;
					RefreshDataSumberDana();
				}
            }
        }
	);
		
    SumberDanaLookUps.show();
	if (rowdata == undefined)
	{
		ACC_DanaAddNew();
	}
	else
	{
		SumberDanaInit(rowdata)
	}
};

///-------------------------------------------------------------------------------------///


function getFormEntrySumberDana(lebar)   
{	
	
	var pnlSumberDana = new Ext.FormPanel  
    (
		{ 	
			id: 'PanelSumberDana',
			fileUpload: true,
			region: 'north',
			layout: 'column',
			height: 120,
			anchor: '100%' ,
			bodyStyle: 'padding:10px 0px 10px 10px',	
			iconCls: 'SetupJenisSumberDana',
			border: true,
			items: 
			[
				{
					layout: 'column',
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
					width:lebar-36.5,
					labelAlign: 'right',
					anchor: '100%',
					items: 
					[
						{ 	
							columnWidth:.989,
							layout: 'form',
							id: 'PnlKiriSumberDana',
							labelWidth:70,//65,
							border: false,
							items: 
							[	
								{
									xtype: 'textfield',
									fieldLabel: 'Code ',
									name: 'txtDana_Code',
									id: 'txtDana_Code',
									anchor: '40%'
								}, 
								{
									xtype: 'textfield',
									fieldLabel: 'Description ',
									name: 'txtDescription',
									id: 'txtDescription',
									anchor: '100%'
								}
							]
						}
					]
				}
			],
			tbar: 
			[	
				{ 	
					id:'btnAddSumberDana',
					text: 'Tambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() { ACC_DanaAddNew() }
				}, '-',
				{ 	
					id:'btnSimpanSumberDana',
					text: 'Simpan',
					tooltip: 'Simpan Data ',
					iconCls: 'save',
					handler: function()
					{ 
						ACC_DanaSave(false);
						RefreshDataSumberDana();		
					}
				}, '-',
				{
					id:'btnSimpanCloseSumberDana',
					text: 'Simpan & Keluar',
					tooltip: 'Simpan dan Keluar',
					iconCls: 'saveexit',
					handler: function() 
					{  
						var x = ACC_DanaSave(true);
						RefreshDataSumberDana();
						if (x===undefined)
						{
							SumberDanaLookUps.close();
						};
					}
				},
				'-',
				{ 	
					id:'btnHapusSumberDana',
					text: 'Hapus',
					tooltip: 'Hapus Data',
					iconCls: 'remove',
					handler: function() 
					{
						ACC_DanaDelete();
						RefreshDataSumberDana();
					}
				},'-','->','-',
				{
					id:'btnPrintSumberDana',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() 
					{
						var criteria = "";
						if(Ext.getCmp('txtDana_Code').getValue() != "")
						{
							criteria += " WHERE Dana_Code='" + Ext.getCmp('txtDana_Code').getValue() + "'";
						}
						ShowReport("",'011106',criteria);
					}
				}
			]
		}
	);
    // END VAR pnlSumberDana --------------------------------------------------------------------- 
    

	return pnlSumberDana;
}; 
//END FUNCTION getFormEntrySumberDana
///------------------------------------------------------------------------------------------------------------///





function ACC_DanaSave(mBol) 
{
	if (ValidasiEntrySumberDana('Simpan Data') == 1 )
	{
		if (AddNewSumberDana == true)
		 {
			Ext.Ajax.request
			(
				{
					url: "./Datapool.mvc/CreateDataObj",
					params:getACC_DanaParam(),
					success: function(o) 
					{
						var cst = o.responseText;
						if (cst == '{"success":true}') 
						{
							ShowPesanInfoSumberDana('Data berhasil di simpan','Simpan Data');
							RefreshDataSumberDana();
							if(mBol === false)
							{
								Ext.get('txtDana_Code').dom.readOnly=true;
							};
							AddNewSumberDana = false;
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarningSumberDana('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSumberDana('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		 }
		else 
		{
			 Ext.Ajax.request
			 (
				{
					url: "./Datapool.mvc/UpdateDataObj",
					params:  getACC_DanaParam(), 
					success: function(o) 
					{
						var cst = o.responseText;
						if (cst == '{"success":true}') 
						{
							ShowPesanInfoSumberDana('Data berhasil di edit','Edit Data');
							RefreshDataSumberDana();
							if(mBol === false)
							{
								Ext.get('txtDana_Code').dom.readOnly=true;
							};
						}
						else if (cst == '{"pesan":0,"success":false}' )
						{
							ShowPesanWarningSumberDana('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else 
						{
							ShowPesanErrorSumberDana('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ACC_DanaDelete() 
{
	if (ValidasiEntrySumberDana('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params:  getACC_DanaParam(), 
				success: function(o) 
				{
					var cst = o.responseText;
					if (cst == '{"success":true}') 
					{
						ShowPesanInfoSumberDana('Data berhasil di hapus','Hapus Data');
						RefreshDataSumberDana();
						ACC_DanaAddNew();
					}
					else if (cst == '{"pesan":0,"success":false}' )
					{
						ShowPesanWarningSumberDana('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanErrorSumberDana('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		) 
	}
};

function ValidasiEntrySumberDana(modul)
{
	var x = 1;
	if (Ext.get('txtDana_Code').getValue() == '' || Ext.get('txtDescription').getValue() == '')
	{
		if (Ext.get('txtDana_Code').getValue() == '')
		{
			ShowPesanWarningSumberDana('Code SumberDana belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningSumberDana('Description belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningSumberDana(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSumberDana(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSumberDana(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function SumberDanaInit(rowdata)
{
	AddNewSumberDana = false;
	Ext.get('txtDana_Code').dom.value=rowdata.Dana_Code;
	Ext.get('txtDana_Code').dom.readOnly=true;
	Ext.get('txtDescription').dom.value=rowdata.Description;
};

function ACC_DanaAddNew() 
{
    AddNewSumberDana = true;
	Ext.get('txtDana_Code').dom.value='';
	Ext.get('txtDana_Code').dom.readOnly=false;
	Ext.get('txtDescription').dom.value='';
};

function  getACC_DanaParam()
{
	var params = 
	{
		Table: 'JenisSumberDana',
		Dana_Code: Ext.get('txtDana_Code').getValue(),
		Description: Ext.get('txtDescription').getValue()
    };
    return params 
};


function RefreshDataSumberDanaFilter() 
{   
	var strSumberDana;
    if (Ext.get('txtKDSumberDanaFilter').getValue() != '')
    { 
		strSumberDana = 'kode@ Dana_Code =' + Ext.get('txtKDSumberDanaFilter').getValue(); 
	}
    if (Ext.get('txtSumberDanaFilter').getValue() != '')
    { 
		if (strSumberDana == undefined)
		{
			strSumberDana = 'nama@ Description =' + Ext.get('txtSumberDanaFilter').getValue();
		}
		else
		{
			strSumberDana += '##@@##nama@ Description =' + Ext.get('txtSumberDanaFilter').getValue();
		}  
	}
        
    if (strSumberDana != undefined) 
    {  
		dsSumberDanaList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSumberDana, 
					Sort: 'Dana_Code', 
					Sortdir: 'DESC', 
					target:'JenisSumberDana',
					param: strSumberDana
				}			
			}
		);        
    }
	else
	{
		RefreshDataSumberDana();
	}
};



function mComboMaksDataSumberDana()
{
  var cboMaksDataSumberDana = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSumberDana',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'10',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSumberDana,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSumberDana=b.data.displayText ;
					RefreshDataSumberDana();
				} 
			}
		}
	);
	return cboMaksDataSumberDana;
};
 





