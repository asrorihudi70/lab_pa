
var dsSetCategorySchPMList;
var AddNewSetCategorySchPM;
var selectCountSetCategorySchPM=50;
var rowSelectedSetCategorySchPM;
var SetCategorySchPMLookUps;
var selectTrackByDateSetCategorySchPMEntry;
var selectTrackByMeterSetCategorySchPMEntry;
CurrentPage.page = getPanelSetCategorySchPM(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetCategorySchPM(mod_id) 
{
	
    var Field = ['KD_CAT_SCH_PM','CAT_SCH_PM_NAME','IS_TRACK_BY_DATE','DESC_DATE','IS_TRACK_BY_METER','DESC_METER',];
    dsSetCategorySchPMList = new WebApp.DataStore({ fields: Field });
	
	RefreshDataSetCategorySchPM();
	
	 var chkTrackByDateSetCategorySchPM = new Ext.grid.CheckColumn
	(
		{
			id: 'chkTrackByDateSetCategorySchPM',
			header: "Track By Date",
			align: 'center',
			disabled:true,
			dataIndex: 'IS_TRACK_BY_DATE',
			width: 100
		}
	);
	
	 var chkTrackByMeterSetCategorySchPM = new Ext.grid.CheckColumn
	(
		{
			id: 'chkTrackByMeterSetCategorySchPM',
			header: "Track By Meter",
			align: 'center',
			disabled:true,
			dataIndex: 'IS_TRACK_BY_METER',
			width: 100
		}
	);
	
    var grListSetCategorySchPM = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetCategorySchPM',
		    stripeRows: true,
		    store: dsSetCategorySchPMList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetCategorySchPM=undefined;
							rowSelectedSetCategorySchPM = dsSetCategorySchPMList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetCategorySchPM',
                        header: "Code",                      
                        dataIndex: 'KD_CAT_SCH_PM',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetCategorySchPM',
					    header: "Schedule Category PM Name",					   
					    dataIndex: 'CAT_SCH_PM_NAME',
					    width: 200,
					    sortable: true
					},chkTrackByDateSetCategorySchPM,
					{
					    id: 'colSetDescDateCategorySchPM',
					    header: "Date Description",					   
					    dataIndex: 'DESC_DATE',
					    width: 100,
					    sortable: true
					},chkTrackByMeterSetCategorySchPM,
					{
					    id: 'colSetDescMeterCategorySchPM',
					    header: "Meter Description",					   
					    dataIndex: 'DESC_METER',
					    width: 100,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetCategorySchPM',
				    text: 'Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetCategorySchPM != undefined)
						{
						    SetCategorySchPMLookUp(rowSelectedSetCategorySchPM.data);
						}
						else
						{
						    SetCategorySchPMLookUp();
						}
				    }
				},' ','-'
			],viewConfig: { forceFit: true }
		}
	);


    var FormSetCategorySchPM = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Schedule Category PM',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupCategorySchPM',
		    items: [grListSetCategorySchPM],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetCategorySchPMFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Schedule Category PM Name : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'CategorySchPM : ',
					id: 'txtSetCategorySchPMFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Max.Data : ', ' ',mComboMaksDataSetCategorySchPM(),
				' ','-',
				{
				    id: 'btnRefreshSetCategorySchPM',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetCategorySchPMFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					
				}
			}
		}
	);
    
	RefreshDataSetCategorySchPM();
    return FormSetCategorySchPM ;
};
///------------------------------------------------------------------------------------------------------------///




function SetCategorySchPMLookUp(rowdata) 
{
	var lebar=600;
    SetCategorySchPMLookUps = new Ext.Window   	
    (
		{
		    id: 'SetCategorySchPMLookUps',
		    title: 'Schedule Category PM',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 333,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupCategorySchPM',
		    modal: true,
		    items: getFormEntrySetCategorySchPM(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetCategorySchPM=undefined;
					RefreshDataSetCategorySchPM();
				}
            }
		}
	);


    SetCategorySchPMLookUps.show();
	if (rowdata == undefined)
	{
		SetCategorySchPMAddNew();
	}
	else
	{
		SetCategorySchPMInit(rowdata)
	}	
};


function getFormEntrySetCategorySchPM(lebar) 
{
    var pnlSetCategorySchPM = new Ext.FormPanel
    (
		{
		    id: 'PanelSetCategorySchPM',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 375,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupCategorySchPM',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 253,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetCategorySchPM',
							labelWidth:60,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Code ',
								    name: 'txtKode_SetCategorySchPM',
								    id: 'txtKode_SetCategorySchPM',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Name ',
								    name: 'txtNameSetCategorySchPM',
								    id: 'txtNameSetCategorySchPM',
								    anchor: '100%'
								},getItemPanelTrackDateSetCategorySchPM(lebar),getItemPanelTrackMeterSetCategorySchPM(lebar)
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetCategorySchPM',
				    text: 'New',
				    tooltip: 'New',
				    iconCls: 'add',				   
				    handler: function() { SetCategorySchPMAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetCategorySchPM',
				    text: 'Save',
				    tooltip: 'Save',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetCategorySchPMSave(false);
						RefreshDataSetCategorySchPM();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetCategorySchPM',
				    text: 'Save & Close',
				    tooltip: 'Save & Close',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetCategorySchPMSave(true);
						RefreshDataSetCategorySchPM();
						if (x===undefined)
						{
							SetCategorySchPMLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetCategorySchPM',
				    text: 'Remove',
				    tooltip: 'Remove',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetCategorySchPMDelete() ;
							RefreshDataSetCategorySchPM();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetCategorySchPM',
					text: ' Print',
					tooltip: 'Print',
					iconCls: 'print',					
					handler: function() {LoadReport(231032);}
				}
			]
		}
	); 

    return pnlSetCategorySchPM
};

function getItemPanelTrackDateSetCategorySchPM(lebar)
{
	var PanelTrackDate2SetCategorySchPM = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 400,
			border:false,
			labelAlign:'right',
			labelWidth:120,
			style: 
			{
				'margin-top': '6px',
				'margin-left': '83px'
			},
			items: 
			[
				mComboTrackByDateSetCatSchPMEntry()
			]
		}
	);

	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				xtype: 'fieldset',
				id:'fieldsetTrackByDateSetCatSchPM',
				title: 'Track By Date',
				width:lebar-42,
				items: 
				[
					{
						xtype: 'checkbox',
						id: 'chkTrackByDateSetCatSchPMEntry',
						name:'chkTrackByDateSetCatSchPMEntry',
						style: 
						{
							'margin-top': '4px'
						},
						fieldLabel: '',
						boxLabel: 'Track By Date',
						anchor: '100%'
					},PanelTrackDate2SetCategorySchPM
				]
			}
		]
	}
	
	return items;
};


function getItemPanelTrackMeterSetCategorySchPM(lebar)
{
	var PanelTrackMeter2SetCategorySchPM = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 400,
			border:false,
			labelAlign:'right',
			labelWidth:120,
			style: 
			{
				'margin-top': '6px',
				'margin-left': '83px'
			},
			items: 
			[
				mComboTrackByMeterSetCatSchPMEntry()
			]
		}
	);

	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				xtype: 'fieldset',
				id:'fieldsetTrackByMeterSetCatSchPM',
				title: 'Track By Meter',
				width:lebar-42,
				style: 
				{
					'margin-top': '4px'
				},
				items: 
				[
					{
						xtype: 'checkbox',
						id: 'chkTrackByMeterSetCatSchPMEntry',
						name:'chkTrackByMeterSetCatSchPMEntry',
						style: 
						{
							'margin-top': '4px'
						},
						fieldLabel: '',
						boxLabel: 'Track By Meter',
						anchor: '100%'
					},PanelTrackMeter2SetCategorySchPM
				]
			}
		]
	}
	
	return items;
};

function mComboTrackByDateSetCatSchPMEntry()
{
  var cboTrackByDateSetCatSchPMEntry = new Ext.form.ComboBox
	(
		{
			id:'cboTrackByDateSetCatSchPMEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: '',
			fieldLabel: 'Default Tracking By ',			
			anchor:'70%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Kd_Default_Tracking',
						'Default_Tracking'
					],
				data: [[1, 'Days'], [2, 'Weeks'],[3, 'Months'],[4, 'Years']]
				}
			),
			valueField: 'Kd_Default_Tracking',
			displayField: 'Default_Tracking',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectTrackByDateSetCategorySchPMEntry=b.data.Kd_Default_Tracking;
				} 
			}
		}
	);
	return cboTrackByDateSetCatSchPMEntry;
};

function mComboTrackByMeterSetCatSchPMEntry()
{
  var cboTrackByMeterSetCatSchPMEntry = new Ext.form.ComboBox
	(
		{
			id:'cboTrackByMeterSetCatSchPMEntry',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: '',
			fieldLabel: 'Unit Type ',			
			anchor:'70%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Kd_Unit_Type',
						'Unit_Type'
					],
				data: [[1, 'Mileage'], [2, 'Hours'],[3, 'Kilometers']]
				}
			),
			valueField: 'Kd_Unit_Type',
			displayField: 'Unit_Type',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectTrackByMeterSetCategorySchPMEntry=b.data.Kd_Unit_Type;
				} 
			}
		}
	);
	return cboTrackByMeterSetCatSchPMEntry;
};

function SetCategorySchPMSave(mBol) 
{
	if (ValidasiEntrySetCategorySchPM('Simpan Data') == 1 )
	{
		if (AddNewSetCategorySchPM == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetCategorySchPM(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetCategorySchPM('Data berhasil di simpan','Simpan Data');
							RefreshDataSetCategorySchPM();
							if(mBol === false)
							{
								Ext.get('txtKode_SetCategorySchPM').dom.readOnly=true;
							};
							AddNewSetCategorySchPM = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetCategorySchPM('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSetCategorySchPM('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetCategorySchPM(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetCategorySchPM('Data berhasil di edit','Edit Data');
							RefreshDataSetCategorySchPM();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetCategorySchPM('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else {
							ShowPesanErrorSetCategorySchPM('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetCategorySchPMSave
///---------------------------------------------------------------------------------------///

function SetCategorySchPMDelete() 
{
	if (ValidasiEntrySetCategorySchPM('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlDeleteData,
				params: getParamSetCategorySchPM(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetCategorySchPM('Data berhasil di hapus','Hapus Data');
						RefreshDataSetCategorySchPM();
						SetCategorySchPMAddNew();
					}
					else if (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningSetCategorySchPM('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorSetCategorySchPM('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetCategorySchPM(modul)
{
	var x = 1;
	if (Ext.get('txtKode_SetCategorySchPM').getValue() == '' || Ext.get('txtNameSetCategorySchPM').getValue() == '')
	{
		if (Ext.get('txtKode_SetCategorySchPM').getValue() == '')
		{
			ShowPesanWarningSetCategorySchPM('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningSetCategorySchPM('Sumber dana belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningSetCategorySchPM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSetCategorySchPM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSetCategorySchPM(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetCategorySchPMInit(rowdata) 
{
    AddNewSetCategorySchPM = false;
    // Ext.get('txtKode_SetCategorySchPM').dom.value = rowdata.KD_CategorySchPM;
    // Ext.get('txtNameSetCategorySchPM').dom.value = rowdata.CategorySchPM;
	// Ext.get('txtContact1SetCategorySchPM').dom.value = rowdata.CONTACT1;
	// Ext.get('txtContact2SetCategorySchPM').dom.value = rowdata.CONTACT2;
	// Ext.get('txtAddressSetCategorySchPM').dom.value = rowdata.ADDRESS;
	// Ext.get('txtCitySetCategorySchPM').dom.value = rowdata.CITY;
	// Ext.get('txtPosCodeSetCategorySchPM').dom.value = rowdata.POS_CODE;
	// Ext.get('txtPhone1SetCategorySchPM').dom.value = rowdata.PHONE1;
	// Ext.get('txtPhone2SetCategorySchPM').dom.value = rowdata.PHONE2;
	// Ext.get('txtCountrySetCategorySchPM').dom.value = rowdata.COUNTRY;
	// Ext.get('txtKode_SetCategorySchPM').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetCategorySchPMAddNew() 
{
    AddNewSetCategorySchPM = true;   
	// Ext.get('txtKode_SetCategorySchPM').dom.value = '';
    // Ext.get('txtNameSetCategorySchPM').dom.value = '';
	// Ext.get('txtContact1SetCategorySchPM').dom.value = '';
	// Ext.get('txtContact2SetCategorySchPM').dom.value = '';
	// Ext.get('txtAddressSetCategorySchPM').dom.value = '';
	// Ext.get('txtCitySetCategorySchPM').dom.value = '';
	// Ext.get('txtPosCodeSetCategorySchPM').dom.value = '';
	// Ext.get('txtPhone1SetCategorySchPM').dom.value = '';
	// Ext.get('txtPhone2SetCategorySchPM').dom.value = '';
	// Ext.get('txtCountrySetCategorySchPM').dom.value = '';
	rowSelectedSetCategorySchPM   = undefined;
	Ext.get('txtKode_SetCategorySchPM').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetCategorySchPM() 
{
    var params =
	{	
		Table: ''//,   
	    //Kd_Sumber_Dana: Ext.get('txtKode_SetCategorySchPM').getValue(),
	    //Sumber_Dana: Ext.get('txtSetCategorySchPM').getValue()	
	};
    return params
};


function RefreshDataSetCategorySchPM()
{	
	dsSetCategorySchPMList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetCategorySchPM, 
				Sort: 'KD_CategorySchPM', 
				Sortdir: 'ASC', 
				target:'SETUP_CategorySchPM',
				param: ''
			} 
		}
	);
	rowSelectedSetCategorySchPM = undefined;
	return dsSetCategorySchPMList;
};

function RefreshDataSetCategorySchPMFilter() 
{   
	// var KataKunci;
    // if (Ext.get('txtKDSetCategorySchPMFilter').getValue() != '')
    // { 
		// KataKunci = 'kode@ Kd_Sumber_Dana =' + Ext.get('txtKDSetCategorySchPMFilter').getValue(); 
	// }
    // if (Ext.get('txtSetCategorySchPMFilter').getValue() != '')
    // { 
		// if (KataKunci == undefined)
		// {
			// KataKunci = 'nama@ Sumber_Dana =' + Ext.get('txtSetCategorySchPMFilter').getValue();
		// }
		// else
		// {
			// KataKunci += '##@@##nama@ Sumber_Dana =' + Ext.get('txtSetCategorySchPMFilter').getValue();
		// }  
	// }
        
    if (KataKunci != undefined) 
    {  
		// dsSetCategorySchPMList.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCountSetCategorySchPM, 
					// Sort: 'Kd_Sumber_Dana', 
					// Sortdir: 'ASC', 
					// target:'acc_sumber_dana',
					// param: KataKunci
				// }			
			// }
		// );        
    }
	else
	{
		RefreshDataSetCategorySchPM();
	}
};



function mComboMaksDataSetCategorySchPM()
{
  var cboMaksDataSetCategorySchPM = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetCategorySchPM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetCategorySchPM,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetCategorySchPM=b.data.displayText ;
					RefreshDataSetCategorySchPM();
				} 
			}
		}
	);
	return cboMaksDataSetCategorySchPM;
};
 
