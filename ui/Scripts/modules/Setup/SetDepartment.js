
var dsSetDepartmentList;
var AddNewSetDepartment;
var selectCountSetDepartment=50;
var rowSelectedSetDepartment;
var SetDepartmentLookUps;
CurrentPage.page = getPanelSetDepartment(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetDepartment(mod_id) 
{
	
    var Field = ['DEPT_ID', 'DEPT_NAME'];
    dsSetDepartmentList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetDepartment();

    var grListSetDepartment = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetDepartment',
		    stripeRows: true,
		    store: dsSetDepartmentList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetDepartment=undefined;
							rowSelectedSetDepartment = dsSetDepartmentList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetDepartment = dsSetDepartmentList.getAt(ridx);
					if (rowSelectedSetDepartment != undefined)
					{
						SetDepartmentLookUp(rowSelectedSetDepartment.data);
					}
					else
					{
						SetDepartmentLookUp();
					}
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetDepartment',
                        header: nmKdDepartement2,                      
                        dataIndex: 'DEPT_ID',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'colSetDepartment',
					    header: nmDepartement,					   
					    dataIndex: 'DEPT_NAME',
					    width: 200,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetDepartmentList,
            pageSize: selectCountSetDepartment,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetDepartment',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetDepartment != undefined)
						{
						    SetDepartmentLookUp(rowSelectedSetDepartment.data);
						}
						else
						{
						    SetDepartmentLookUp();
						}
				    }
				},' ','-'
			],
		    viewConfig: { forceFit: true }
		}
	);


    var FormSetDepartment = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormDepartement,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupDepartment',
		    items: [grListSetDepartment],
		    tbar:
			[
				nmKdDepartement2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetDepartmentFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmDepartement + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Department : ',
					id: 'txtSetDepartmentFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, 
				{
				    id: 'btnRefreshSetDepartment',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetDepartmentFilter();
					}
				}
			]
		}
	);
    //END var FormSetDepartment--------------------------------------------------

	RefreshDataSetDepartment();
    return FormSetDepartment
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetDepartmentLookUp(rowdata) 
{
	var lebar=600;
    SetDepartmentLookUps = new Ext.Window   	
    (
		{
		    id: 'SetDepartmentLookUps',
		    title: nmFormDepartement,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupDepartment',
		    modal: true,
		    items: getFormEntrySetDepartment(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetDepartment=undefined;
					RefreshDataSetDepartment();
				}
            }
		}
	);


    SetDepartmentLookUps.show();
	if (rowdata == undefined)
	{
		SetDepartmentAddNew();
	}
	else
	{
		SetDepartmentInit(rowdata)
	}	
};


function getFormEntrySetDepartment(lebar) 
{
    var pnlSetDepartment = new Ext.FormPanel
    (
		{
		    id: 'PanelSetDepartment',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupDepartment',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetDepartment',
							labelWidth:80,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdDepartement2 + ' ',
								    name: 'txtKode_SetDepartment',
								    id: 'txtKode_SetDepartment',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmDepartement + ' ',
								    name: 'txtSetDepartment',
								    id: 'txtSetDepartment',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetDepartment',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetDepartmentAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetDepartment',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetDepartmentSave(false);
						RefreshDataSetDepartment();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetDepartment',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetDepartmentSave(true);
						RefreshDataSetDepartment();
						if (x===undefined)
						{
							SetDepartmentLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetDepartment',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetDepartmentDelete() ;
							RefreshDataSetDepartment();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetDepartment',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls:  'print',								
					handler: function() {LoadReport(950006);}
				}
			]
		}
	); 

    return pnlSetDepartment
};



function SetDepartmentSave(mBol) 
{
	if (ValidasiEntrySetDepartment(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetDepartment == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetDepartment(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetDepartment(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetDepartment();
							if(mBol === false)
							{
								Ext.get('txtKode_SetDepartment').dom.value=cst.DeptId;
							};
							AddNewSetDepartment = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetDepartment(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetDepartment(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetDepartment(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetDepartment(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetDepartment();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetDepartment(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetDepartment(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetDepartmentSave
///---------------------------------------------------------------------------------------///

function SetDepartmentDelete() 
{
	if (ValidasiEntrySetDepartment(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmDepartement) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:300,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetDepartment(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetDepartment(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetDepartment();
										SetDepartmentAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetDepartment(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetDepartment(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
			   }
			}
		)
	};
};


function ValidasiEntrySetDepartment(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetDepartment').getValue() == '' || (Ext.get('txtSetDepartment').getValue() == ''))
	{
		if (Ext.get('txtKode_SetDepartment').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetDepartment(nmGetValidasiKosong(nmKdDepartement),modul);
			x=0;
		}
		else if (Ext.get('txtSetDepartment').getValue() === '')
		{
			x=0;
			if (mBolHapus === false)
			{
				ShowPesanWarningSetDepartment(nmGetValidasiKosong(nmDepartement),modul);
			};
		};
	};

	return x;
};

function ShowPesanWarningSetDepartment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetDepartment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		  width :250
		}
	);
};

function ShowPesanInfoSetDepartment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
			width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetDepartmentInit(rowdata) 
{
    AddNewSetDepartment = false;
    Ext.get('txtKode_SetDepartment').dom.value = rowdata.DEPT_ID;
    Ext.get('txtSetDepartment').dom.value = rowdata.DEPT_NAME;
	Ext.get('txtKode_SetDepartment').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetDepartmentAddNew() 
{
    AddNewSetDepartment = true;   
	Ext.get('txtKode_SetDepartment').dom.value = '';
	Ext.get('txtSetDepartment').dom.value = '';
	rowSelectedSetDepartment   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetDepartment() 
{
    var params =
	{	
		Table: 'ViewSetupDept',   
	    DeptId: Ext.get('txtKode_SetDepartment').getValue(),
	    Dept: Ext.get('txtSetDepartment').getValue()	
	};
    return params
};


function RefreshDataSetDepartment()
{	
	dsSetDepartmentList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetDepartment, 
				Sort: 'dept_id', 
				Sortdir: 'ASC', 
				target:'ViewSetupDept',
				param: ''
			} 
		}
	);
	rowSelectedSetDepartment = undefined;
	return dsSetDepartmentList;
};

function RefreshDataSetDepartmentFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetDepartmentFilter').getValue() != '')
    { 
		KataKunci = '  dept_id like ~%' + Ext.get('txtKDSetDepartmentFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetDepartmentFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  dept_name like  ~%' + Ext.get('txtSetDepartmentFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  dept_name like  ~%' + Ext.get('txtSetDepartmentFilter').getValue() + '%~';
		};  
	};
	    
    if (KataKunci != undefined) 
    {  
		dsSetDepartmentList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetDepartment, 
					Sort: 'dept_id', 
					Sortdir: 'ASC', 
					target:'ViewSetupDept',
					param: KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetDepartment();
	};
};



function mComboMaksDataSetDepartment()
{
  var cboMaksDataSetDepartment = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetDepartment',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData,			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetDepartment,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetDepartment=b.data.displayText ;
					RefreshDataSetDepartment();
				} 
			}
		}
	);
	return cboMaksDataSetDepartment;
};
 
