
var dsSetEquipmentTypeList;
var AddNewSetEquipmentType;
var selectCountTypeEqp;
var rowSelectedSetEquipmentType;
var selectCategorySetEquipmentType;
var SetEquipmentTypeLookUps;
CurrentPage.page = getPanelSetEquipmentType(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEquipmentType(mod_id) 
{
	
    var Field = ['KD_TYPE_ASSET_MAIN','TYPE_ASSET_MAIN','KD_CATEGORY_ASSET_MAIN','CATEGORY_ASSET_MAIN'];
    dsSetEquipmentTypeList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetEquipmentType();

    var grListSetEquipmentType = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEquipmentType',
		    stripeRows: true,
		    store: dsSetEquipmentTypeList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEquipmentType=undefined;
							rowSelectedSetEquipmentType = dsSetEquipmentTypeList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetEquipmentType',
                        header: "Code",                      
                        dataIndex: 'KD_TYPE_ASSET_MAIN',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'colSetCategoryEquipmentType',
					    header: "Category",					   
					    dataIndex: 'CATEGORY_ASSET_MAIN',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetEquipmentType',
					    header: "Type",					   
					    dataIndex: 'TYPE_ASSET_MAIN',
					    width: 200,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetEquipmentType',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetEquipmentType != undefined)
						{
						    SetEquipmentTypeLookUp(rowSelectedSetEquipmentType.data);
						}
						else
						{
						    SetEquipmentTypeLookUp();
						}
				    }
				},' ','-'
			],
		    viewConfig: { forceFit: true }
		}
	);


    var FormSetEquipmentType = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Asset Maintenance Type',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupSetEquipmentType',
		    items: [grListSetEquipmentType],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetEquipmentTypeFilter',                   
					width:40,
					onInit: function() { }
				}, ' ','-',
				'Category : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Category : ',
					id: 'txtSetCategoryEquipmentTypeFilter',                   
					anchor: '40%',
					onInit: function() { }
				}, ' ','-',
				'Type : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Type : ',
					id: 'txtSetEquipmentTypeFilter',                   
					anchor: '40%',
					onInit: function() { }
				}, ' ','-',
				'Max.Data : ', ' ',mComboMaksDataTypeEqp(),
				' ','-',
				{
				    id: 'btnRefreshSetEquipmentType',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEquipmentTypeFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetEquipmentType();
				}
			}
		}
	);
    //END var FormSetEquipmentType--------------------------------------------------

	RefreshDataSetEquipmentType();
    return FormSetEquipmentType
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetEquipmentTypeLookUp(rowdata) 
{
	var lebar=600;
    SetEquipmentTypeLookUps = new Ext.Window   	
    (
		{
		    id: 'SetEquipmentTypeLookUps',
		    title: 'Asset Maintenance Type',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 177,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupSetEquipmentType',
		    modal: true,
		    items: getFormEntrySetEquipmentType(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetEquipmentType=undefined;
					RefreshDataSetEquipmentType();
				}
            }
		}
	);
    //END var SetEquipmentTypeLookUps------------------------------------------------------------------

    SetEquipmentTypeLookUps.show();
	if (rowdata == undefined)
	{
		SetEquipmentTypeAddNew();
	}
	else
	{
		SetEquipmentTypeInit(rowdata)
	}	
};

//  END FUNCTION SetEquipmentTypeLookUp
///------------------------------------------------------------------------------------------------------------///




function getFormEntrySetEquipmentType(lebar) 
{
    var pnlSetEquipmentType = new Ext.FormPanel
    (
		{
		    id: 'PanelSetEquipmentType',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 147,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupSetEquipmentType',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 97,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetEquipmentType',
							labelWidth:70,
						    border: false,
						    items:
							[
								mComboCategoryEqpSetTypeEqp(),
								{
								    xtype: 'textfield',
								    fieldLabel: 'Code ',
								    name: 'txtKode_SetEquipmentType',
								    id: 'txtKode_SetEquipmentType',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Type ',
								    name: 'txtSetEquipmentType',
								    id: 'txtSetEquipmentType',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetEquipmentType',
				    text: 'New',
				    tooltip: 'New',
				    iconCls: 'add',				   
				    handler: function() { SetEquipmentTypeAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetEquipmentType',
				    text: 'Save',
				    tooltip: 'Save',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetEquipmentTypeSave(false);
						RefreshDataSetEquipmentType();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetEquipmentType',
				    text: 'Save & Close',
				    tooltip: 'Save & Close',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetEquipmentTypeSave(true);
						RefreshDataSetEquipmentType();
						if (x===undefined)
						{
							SetEquipmentTypeLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetEquipmentType',
				    text: 'Remove',
				    tooltip: 'Remove',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetEquipmentTypeDelete() ;
							RefreshDataSetEquipmentType();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetEquipmentType',
					text: ' Print',
					tooltip: 'Print',
					iconCls: 'print',					
					handler: function() {LoadReport(231032);}
				}
			]
		}
	); 

    return pnlSetEquipmentType
};
//END FUNCTION getFormEntrySetEquipmentType
///------------------------------------------------------------------------------------------------------------///


function mComboCategoryEqpSetTypeEqp()
{
  var cboCategoryEqpTypeEqp = new Ext.form.ComboBox
	(
		{
			id:'cboCategoryEqpTypeEqp',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			 emptyText: 'Choose Category...',
			fieldLabel: 'Category ',			
			anchor:'60%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Kd_Category',
						'Category'
					],
				data: [[1, 'Electrical'], [2, 'Furniture'],[3, 'Air Conditioners']]
				}
			),
			valueField: 'Kd_Category',
			displayField: 'Category',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCategorySetEquipmentType=b.data.Category ;
				} 
			}
		}
	);
	return cboCategoryEqpTypeEqp;
};

function SetEquipmentTypeSave(mBol) 
{
	if (ValidasiEntrySetEquipmentType('Simpan Data') == 1 )
	{
		if (AddNewSetEquipmentType == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEquipmentType(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoTypeEqp('Data berhasil di simpan','Simpan Data');
							RefreshDataSetEquipmentType();
							if(mBol === false)
							{
								Ext.get('txtKode_SetEquipmentType').dom.readOnly=true;
							};
							AddNewSetEquipmentType = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningTypeEqp('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorTypeEqp('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetEquipmentType(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoTypeEqp('Data berhasil di edit','Edit Data');
							RefreshDataSetEquipmentType();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningTypeEqp('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else {
							ShowPesanErrorTypeEqp('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetEquipmentTypeSave
///---------------------------------------------------------------------------------------///

function SetEquipmentTypeDelete() 
{
	if (ValidasiEntrySetEquipmentType('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlDeleteData,
				params: getParamSetEquipmentType(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoTypeEqp('Data berhasil di hapus','Hapus Data');
						RefreshDataSetEquipmentType();
						SetEquipmentTypeAddNew();
					}
					else if (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningTypeEqp('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorTypeEqp('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetEquipmentType(modul)
{
	var x = 1;
	if (Ext.get('txtKode_SetEquipmentType').getValue() == '' || Ext.get('txtSetEquipmentType').getValue() == '')
	{
		if (Ext.get('txtKode_SetEquipmentType').getValue() == '')
		{
			ShowPesanWarningTypeEqp('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningTypeEqp('Sumber dana belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningTypeEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorTypeEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoTypeEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetEquipmentTypeInit(rowdata) 
{
    AddNewSetEquipmentType = false;
    Ext.get('txtKode_SetEquipmentType').dom.value = rowdata.KD_TYPE_ASSET_MAIN;
	Ext.get('cboCategoryEqpTypeEqp').dom.value = rowdata.CATEGORY_ASSET_MAIN;
    Ext.get('txtSetEquipmentType').dom.value = rowdata.TYPE_ASSET_MAIN;
	selectCategorySetEquipmentType= rowdata.KD_CATEGORY_ASSET_MAIN;
	Ext.get('txtKode_SetEquipmentType').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetEquipmentTypeAddNew() 
{
    AddNewSetEquipmentType = true;   
	Ext.get('txtKode_SetEquipmentType').dom.value = '';
	Ext.get('txtSetEquipmentType').dom.value = '';
	rowSelectedSetEquipmentType   = undefined;
	selectCategorySetEquipmentType = '';
	Ext.get('txtKode_SetEquipmentType').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetEquipmentType() 
{
    var params =
	{	
		Table: 'acc_sumber_dana',   
	    Kd_Sumber_Dana: Ext.get('txtKode_SetEquipmentType').getValue(),
	    Sumber_Dana: Ext.get('txtSetEquipmentType').getValue()	
	};
    return params
};


function RefreshDataSetEquipmentType()
{	
	// dsSetEquipmentTypeList.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: selectCountTypeEqp, 
				// Sort: 'Kd_Sumber_Dana', 
				// Sortdir: 'ASC', 
				// target:'acc_sumber_dana',
				// param: ''
			// } 
		// }
	// );
	// rowSelectedSetEquipmentType = undefined;
	// return dsSetEquipmentTypeList;
};

function RefreshDataSetEquipmentTypeFilter() 
{   
	// var KataKunci;
    // if (Ext.get('txtKDSetEquipmentTypeFilter').getValue() != '')
    // { 
		// KataKunci = 'kode@ Kd_Sumber_Dana =' + Ext.get('txtKDSetEquipmentTypeFilter').getValue(); 
	// }
    // if (Ext.get('txtSetEquipmentTypeFilter').getValue() != '')
    // { 
		// if (KataKunci == undefined)
		// {
			// KataKunci = 'nama@ Sumber_Dana =' + Ext.get('txtSetEquipmentTypeFilter').getValue();
		// }
		// else
		// {
			// KataKunci += '##@@##nama@ Sumber_Dana =' + Ext.get('txtSetEquipmentTypeFilter').getValue();
		// }  
	// }
        
    if (KataKunci != undefined) 
    {  
		// dsSetEquipmentTypeList.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCountTypeEqp, 
					// Sort: 'Kd_Sumber_Dana', 
					// Sortdir: 'ASC', 
					// target:'acc_sumber_dana',
					// param: KataKunci
				// }			
			// }
		// );        
    }
	else
	{
		RefreshDataSetEquipmentType();
	}
};



function mComboMaksDataTypeEqp()
{
  var cboMaksDataTypeEqp = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataTypeEqp',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountTypeEqp,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountTypeEqp=b.data.displayText ;
					RefreshDataSetEquipmentType();
				} 
			}
		}
	);
	return cboMaksDataTypeEqp;
};
 
