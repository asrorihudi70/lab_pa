
var dsSetLocationList;
var AddNewSetLocation;
var selectCountSetLocation=50;
var rowSelectedSetLocation;
var SetLocationLookUps;
CurrentPage.page = getPanelSetLocation(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetLocation(mod_id) 
{
	
    var Field =['LOCATION_ID','LOCATION'];
    dsSetLocationList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetLocation();

    var grListSetLocation = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetLocation',
		    stripeRows: true,
		    store: dsSetLocationList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetLocation = undefined;
							rowSelectedSetLocation = dsSetLocationList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetLocation = dsSetLocationList.getAt(ridx);
					if (rowSelectedSetLocation != undefined)
					{
						SetLocationLookUp(rowSelectedSetLocation.data);
					}
					else
					{
						SetLocationLookUp();
					}
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetLocation',
                        header: nmKdLokasi2,                      
                        dataIndex: 'LOCATION_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetLocation',
					    header: nmLokasi,					   
					    dataIndex: 'LOCATION',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetLocationList,
            pageSize: selectCountSetLocation,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetLocation',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetLocation != undefined)
						{
						    SetLocationLookUp(rowSelectedSetLocation.data);
						}
						else
						{
						    SetLocationLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetLocation = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormLokasi,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupLocation',
		    items: [grListSetLocation],
		    tbar:
			[
				nmKdLokasi2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetLocationFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmLokasi + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Location : ',
					id: 'txtSetLocationFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, 
				{
				    id: 'btnRefreshSetLocation',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetLocationFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetLocation();
				}
			}
		}
	);
    //END var FormSetLocation--------------------------------------------------

	RefreshDataSetLocation();
    return FormSetLocation ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetLocationLookUp(rowdata) 
{
	var lebar=600;
    SetLocationLookUps = new Ext.Window   	
    (
		{
		    id: 'SetLocationLookUps',
		    title: nmFormLokasi,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupLocation',
		    modal: true,
		    items: getFormEntrySetLocation(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetLocation=undefined;
					RefreshDataSetLocation();
				}
            }
		}
	);


    SetLocationLookUps.show();
	if (rowdata == undefined)
	{
		SetLocationAddNew();
	}
	else
	{
		SetLocationInit(rowdata)
	}	
};


function getFormEntrySetLocation(lebar) 
{
    var pnlSetLocation = new Ext.FormPanel
    (
		{
		    id: 'PanelSetLocation',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupLocation',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetLocation',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdLokasi2 + ' ',
								    name: 'txtKode_SetLocation',
								    id: 'txtKode_SetLocation',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmLokasi + ' ',
								    name: 'txtNameSetLocation',
								    id: 'txtNameSetLocation',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetLocation',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetLocationAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetLocation',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetLocationSave(false);
						RefreshDataSetLocation();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetLocation',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetLocationSave(true);
						RefreshDataSetLocation();
						if (x===undefined)
						{
							SetLocationLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetLocation',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetLocationDelete() ;
							RefreshDataSetLocation();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetLocation',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950002);}
				}
			]
		}
	); 

    return pnlSetLocation
};


function SetLocationSave(mBol) 
{
	if (ValidasiEntrySetLocation(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetLocation == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetLocation(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetLocation(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetLocation();
							if(mBol === false)
							{
								Ext.get('txtKode_SetLocation').dom.value=cst.LocationId;
							};
							AddNewSetLocation = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetLocation(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetLocation(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetLocation(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetLocation(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetLocation();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetLocation(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetLocation(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetLocationSave
///---------------------------------------------------------------------------------------///

function SetLocationDelete() 
{
	if (ValidasiEntrySetLocation(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmLokasi) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetLocation(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetLocation(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetLocation();
										SetLocationAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetLocation(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetLocation(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetLocation(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetLocation').getValue() == '' || (Ext.get('txtNameSetLocation').getValue() == ''))
	{
		if (Ext.get('txtKode_SetLocation').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetLocation(nmGetValidasiKosong(nmKdLokasi),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetLocation').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetLocation(nmGetValidasiKosong(nmLokasi),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetLocation(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetLocation(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetLocation(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetLocationInit(rowdata) 
{
    AddNewSetLocation = false;
    Ext.get('txtKode_SetLocation').dom.value = rowdata.LOCATION_ID;
    Ext.get('txtNameSetLocation').dom.value = rowdata.LOCATION;
};
///---------------------------------------------------------------------------------------///



function SetLocationAddNew() 
{
    AddNewSetLocation = true;   
	Ext.get('txtKode_SetLocation').dom.value = '';
    Ext.get('txtNameSetLocation').dom.value = '';
	rowSelectedSetLocation   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetLocation() 
{
    var params =
	{	
		Table: 'ViewSetupLocation',   
	    LocationId: Ext.get('txtKode_SetLocation').getValue(),
	    Location: Ext.get('txtNameSetLocation').getValue()	
	};
    return params
};


function RefreshDataSetLocation()
{	
	dsSetLocationList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetLocation, 
					Sort: 'location_id', 
					Sortdir: 'ASC', 
					target:'ViewSetupLocation',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetLocation = undefined;
	return dsSetLocationList;
};

function RefreshDataSetLocationFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetLocationFilter').getValue() != '')
    { 
		KataKunci = ' location_id like ~%' + Ext.get('txtKDSetLocationFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetLocationFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  location like  ~%' + Ext.get('txtSetLocationFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  location like  ~%' + Ext.get('txtSetLocationFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetLocationList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetLocation, 
					Sort: 'location_id', 
					Sortdir: 'ASC', 
					target:'ViewSetupLocation',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetLocation();
	};
};



function mComboMaksDataSetLocation()
{
  var cboMaksDataSetLocation = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetLocation',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetLocation,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetLocation=b.data.displayText ;
					RefreshDataSetLocation();
				} 
			}
		}
	);
	return cboMaksDataSetLocation;
};
 
