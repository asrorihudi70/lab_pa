
var dsSetMeteringList;
var AddNewSetMetering;
var SetMeteringLookUps;
var selectLogTypeSetMetering;
var selectSatuanSetMetering;
var selectFlagSetMetering=1;
var valueSelectFlagSetMetering='Technic';
var IdServiceSetMetering;

var dsUnitServiceCat;
var SelectUnitServiceCat;

var dsTypeServiceCat;
var SelectTypeServiceCat;

var selectActionCatEqpBefore=1;
var selectActionCatEqpTarget=1;
var selectActionCatEqpAfter=1;

var PathLegendBefore;
var PathLegendTarget;
var PathLegendAfter;
var valueComboActionSetMet='Send Email';

function SetMeteringLookUp(rowdata) 
{

	IdServiceSetMetering='';
	var lebar=600;
    SetMeteringLookUps = new Ext.Window   	
    (
		{
		    id: 'SetMeteringLookUps',
		    title: 'Metering',
		    closeAction: 'destroy',
		    width: lebar,
		    height: 400,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupMetering',
		    modal: true,
		    items: getFormEntrySetMetering(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					SetMeteringLookUps=undefined;
				}
            }
		}
	);


    SetMeteringLookUps.show();
	if (rowdata == undefined)
	{
		SetMeteringAddNew();
	}
	else
	{
		SetMeteringInit(rowdata)
	}	
};


function getFormEntrySetMetering(lebar) 
{
    var pnlSetMetering = new Ext.FormPanel
    (
		{
		    id: 'PanelSetMetering',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 135,//350,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupMetering',
		    border: false,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 97,//226,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetMetering',
							labelWidth:65,
						    border: false,
						    items:
							[	getItemPanelServiceSetMetering(lebar),
								getItemPanelLogSetMetering(lebar),
								getItemPanelAsumsiSetMetering(lebar)
								
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetMetering',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetMeteringAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetMetering',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetMeteringSave(false);
						//RefreshDataSetMetering();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetMetering',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetMeteringSave(true);
						//RefreshDataSetMetering();
						if (x===undefined)
						{
							SetMeteringLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetMetering',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetMeteringDelete() ;
							RefreshDataAddFieldMetering(Ext.get('txtKode_SetEquipmentCat').dom.value);			
					}
				}
				// ,'-','->','-',
				// {
					// id:'btnPrintSetMetering',
					// text: nmCetak,
					// tooltip: nmCetak,
					// iconCls: 'print',					
					// handler: function() {LoadReport(950012);}
				// }
			]
		}
	); 
	
	var vTabPanelSetMetering = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelSetMetering',
			name:'vTabPanelSetMetering',
		    region: 'center',
		    margins: '7 7 7 7',
			style:
			{
			   'margin-top': '5px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
			labelAlign: 'right',
		    frame: false,
		    border: true,
		    height:212,
			width:lebar-34,
		    anchor: '100%',
		    items:
			[
			    {
					 title: 'Action',
					 id: 'vTabActionPanelSetMetering',
					 name: 'vTabActionPanelSetMetering',
					 items:
					 [
						getItemPanelCardLayoutActionSetMetering(lebar)
						// {
							// layout: 'hBox',
							// width:lebar-34,
							// style: 
							// {
								// 'margin-top': '-3px'
							// },
							// height:'36px',
							// border: false,
							// bodyStyle: 'padding:0px 0px 0px 0px',
							// defaults: { margins: '0 0 0 0' },
							// anchor: '100%',
							// layoutConfig: 
							// {
								// padding:'5',
								// align:'middle'
							// },
							// defaults:{margins:'0 5 0 0'},
							// items:
							// [
								// {
									// xtype:'button',
									// text:'Send Email',
									// width:80,
									// id: 'btnSendEmailSetMetering',
									// handler:function()
									// {
										
									// }
								// }
							// ]			
						// }
					 ],
			        listeners:
					{
						  activate: function() 
						  {
						  }
					}
				}
			]
		}
	);
	
	var FormTRSetMetering = new Ext.Panel
	(
		{
		    id: 'FormTRSetMetering',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlSetMetering,
				{
					xtype: 'panel',
					layout: 'form',
					bodyStyle: 'padding:10px 10px 10px 10px',
					labelAlign: 'right',
					border:false,
					id: 'pnlTabSetMetering',
					items:
					[	 
						vTabPanelSetMetering
					]
				}
			]
		}
	);

    return FormTRSetMetering;
};

function getItemPanelLogSetMetering(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.31,
				layout: 'form',
				border:false,
				items: 
				[ 	
					mComboLogTypeSetMetering()
				]
			},
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:60,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: 'Interval' + ' ',
						name: 'txtIntervalSetMetering',
						id: 'txtIntervalSetMetering',
						anchor: '99%'
					}
				]
			},
			{
				columnWidth:.31,
				layout: 'form',
				labelWidth:50,
				border:false,
				items: 
				[ 	
					mComboSatuanSetMetering()
				]
			}
		]
	}
	
	return items;
};

function getItemPanelServiceSetMetering(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.25,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: 'Service' + ' ',
						name: 'txtKdServiceSetMetering',
						id: 'txtKdServiceSetMetering',
						anchor: '98%',
						readOnly:true
					}
				]
			},
			{
				columnWidth:.65,
				layout: 'form',
				labelWidth:0,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						hideLabel:true,
						fieldLabel: '',
						name: 'txtServiceSetMetering',
						id: 'txtServiceSetMetering',
						anchor: '100%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtServiceSetMetering').dom.value != '')
									{
										criteria = ' where SERVICE_NAME like ~%' + Ext.get('txtServiceSetMetering').dom.value + '%~';
									};
									FormLookupService('txtKdServiceSetMetering','txtServiceSetMetering',criteria);
								};
							}
						}
					}
				]
			},
			{
				columnWidth:.01,
				layout: 'form',
				labelWidth:0,
				height:25,
				border:false,
				items: 
				[ 
				]
			},
			{
				xtype:'button',
				text:'Add Service',				
				width:30,
				hideLabel:true,
				name: 'btnAddService',
				id: 'btnAddService',
				handler:function() 
				{
					SetServiceCatLookUp();
				}
			}
		]
	}
	
	return items;
};

function getItemPanelAsumsiSetMetering(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.31,
				layout: 'form',
				labelWidth:65,
				border:false,
				items: 
				[ 	
					mComboFlagSetMetering()
				]
			},
			{
				columnWidth:.32,
				layout: 'form',
				labelWidth:60,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: 'Equivalen' + ' ',
						name: 'txtAsumsi1SetMetering',
						id: 'txtAsumsi1SetMetering',
						anchor: '100%'
					}
				]
			},
			{
				columnWidth:.17,
				layout: 'form',
				labelWidth:10,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: ' = ',
						labelSeparator:'',
						name: 'txtAsumsi2SetMetering',
						id: 'txtAsumsi2SetMetering',
						anchor: '80%'
					}
					// ,
					// {
						// xtype: 'label',
						// fieldLabel: ' Day',
						// labelSeparator:'',
						// style: 
						// {
							// 'margin-left': '1px'
						// },
						// name: 'lblSatuanSetMetering',
						// id: 'lblSatuanSetMetering'//,
						// //anchor: '90%'
					// }
				]
			},
			{
				columnWidth:.1,
				layout: 'form',
				labelWidth:10,
				border:false,
				items: 
				[ 						
					{
						xtype: 'label',
						fieldLabel: ' Day',
						labelSeparator:'',
						style: 
						{
							'margin-left': '1px'
						},
						name: 'lblSatuanSetMetering',
						id: 'lblSatuanSetMetering',
						anchor: '90%'
					}
				]
			}
			
		]
	}
	
	return items;
};

function getItemPanelCardLayoutActionSetMetering(lebar)
{
	 var FormPanelCardLayoutActionSetMetering = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutActionSetMetering',
		    trackResetOnLoad: true,
		    width: 742,
			height: 80,
			labelAlign: 'right',
			layout: 'card',
			border:false,
			activeItem: 0,
			items: 
			[
				{
					xtype: 'panel',
					layout: 'form',
					labelAlign: 'right',
					border:false,
					id: 'CardActionSetMetering1',
					items:
					[	 
						getItemPanelCardActionSetMetering1(lebar),
						getItemPanelCardActionSetMetering2(lebar),
						getItemPanelCardActionSetMetering3(lebar)
					]
				},
				{
					xtype: 'panel',
					layout: 'form',
					align:'right',
					border:false,
					id: 'CardActionSetMetering2',
					items:
					[
						// getItemPanelCardActionSetMetering1(lebar,'Before','Km'),
						// getItemPanelCardActionSetMetering1(lebar,'Target','Km'),
						// getItemPanelCardActionSetMetering1(lebar,'After','Km')
					]
				}
			]
		}
	);
	
	return FormPanelCardLayoutActionSetMetering;
};

function getItemPanelCardActionSetMetering1(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:65,
				border:false,
				items: 
				[ 	
					{
							xtype: 'textfield',
							fieldLabel: 'Before' + ' ',
							name: 'txtBeforeSetMetering',
							id: 'txtBeforeSetMetering',
							anchor: '100%'
					}
				]
			},
			{
				columnWidth:.15,
				layout: 'form',
				labelWidth:50,
				height:23,
				border:false,
				items: 
				[ 	
					{
						xtype: 'label',
						fieldLabel: '( Days )',
						labelSeparator:'',
						style: 
						{
							'margin-left': '7px'
						},
						name: 'lblBeforeSetMetering',
						id: 'lblBeforeSetMetering',
						anchor: '100%'
					}
				]
			},
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:0,
				border:false,
				items: 
				[ 	
					mComboActionSetMetering1()
				]
			},
			{
				columnWidth:.1,
				layout: 'form',
				labelWidth:30,
				height:23,
				border:false,
				items: 
				[ 
					{
						xtype:'label',
						fieldLabel:'Legend ',
						id:'lblLegendSetMeteringBefore',
						name:'lblLegendSetMeteringBefore'
					}
				]
			},
			{
				columnWidth:.05,
				layout: 'form',
				labelWidth:0,
				height:23,
				border:true,
				items: 
				[ 	
					{
						xtype:'label', 
						id:'gbrBeforeSetMetering',
						name:'gbrBeforeSetMetering',
						border:true,
						frame:true,
						html:'<div id="images" style="margin-top:-2px;width:16px;height:16px;" align="center"> </div>'
					}
				]
			}
			,
			{
				columnWidth:.01,
				layout: 'form',
				labelWidth:0,
				height:25,
				border:false,
				items: 
				[ 
				]
			 }
			 //,				
			// {
				// columnWidth:.2,
				// layout: 'form',
				// labelWidth:0,
				// border:false,
				// items: 
				// [ 	
					// {
						// xtype:'button',
						// text:'Browse',
						// width:50,
						// style:{'margin-left':'0px','margin-top':'0px'},
						// hideLabel:true,
						// id: 'btnBrowseSetMetering',
						// handler:function()
						// {
							// Lookup_vupload(2,'Img Asset');
						// }
					// }
				// ]
			// }
			,
			{
				columnWidth:.28,
				layout: 'form',
				labelWidth:20,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						//fieldLabel: 'Before' + ' ',
						hideLabel:true,
						name: 'txtTagActionBefore',
						id: 'txtTagActionBefore',
						anchor: '93%'
					}
				]
			}
		]
	}
	
	return items;
};

function Dataview_fotoAssetSetMetering(StrPath,asal) 
{
	if (StrPath=='' || StrPath==null)
	{
		StrPath=baseURL+'ui/Img Asset/blank.png'
	}
	var pgbr=''
	if (asal === 2 )
	{
		StrPath=baseURL+'ui/Img Asset/hijau.png'
		pgbr=Ext.getCmp('gbrBeforeSetMetering');
		PathLegendBefore=StrPath;
	}
	else if (asal === 3)
	{
		StrPath=baseURL+'ui/Img Asset/kuning.png'
		pgbr=Ext.getCmp('gbrTargetSetMetering');
		PathLegendTarget=StrPath;		
	}
	else
	{
		StrPath=baseURL+'ui/Img Asset/merah.png'
		pgbr=Ext.getCmp('gbrAfterSetMetering');
		PathLegendAfter=StrPath;
	};
    
    Ext.DomHelper.overwrite(pgbr.getEl(),
	{                                	
	  tag: 'img', src: StrPath ,style:'margin:3px;width:16px;height:16px;'  
    }, true).show(true).frame();		
};

function getItemPanelCardActionSetMetering2(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:65,
				border:false,
				items: 
				[ 	
					{
							xtype: 'textfield',
							fieldLabel: 'Target' + ' ',
							name: 'txtTargetSetMetering',
							id: 'txtTargetSetMetering',
							anchor: '100%'
					}
				]
			},
			{
				columnWidth:.15,
				layout: 'form',
				labelWidth:50,
				height:23,
				border:false,
				items: 
				[ 	
					{
						xtype: 'label',
						fieldLabel: '( Days )',
						labelSeparator:'',
						style: 
						{
							'margin-left': '7px'
						},
						name: 'lblTargetSetMetering',
						id: 'lblTargetSetMetering',
						anchor: '100%'
					}
				]
			},
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:0,
				border:false,
				items: 
				[ 	
					mComboActionSetMetering2()
				]
			},
			{
				columnWidth:.1,
				layout: 'form',
				labelWidth:30,
				height:23,
				border:false,
				items: 
				[ 
					{
						xtype:'label',
						id:'lblLegendSetMeteringTarget',
						name:'lblLegendSetMeteringTarget',
						fieldLabel:'Legend '
					}
				]
			},
			{
				columnWidth:.05,
				layout: 'form',
				labelWidth:0,
				height:23,
				border:true,
				items: 
				[ 	
					{
						xtype:'label', 
						id:'gbrTargetSetMetering',
						name:'gbrTargetSetMetering',
						border:true,
						frame:true,
						html:'<div id="images" style="margin:0px;width:16px;height:16px;" align="center"> </div>'
					}
				]
			},
			{
				columnWidth:.01,
				layout: 'form',
				labelWidth:0,
				height:25,
				border:false,
				items: 
				[ 
				]
			 }
			 //,				
			// {
				// columnWidth:.2,
				// layout: 'form',
				// labelWidth:0,
				// border:false,
				// items: 
				// [ 	
					// {
						// xtype:'button',
						// text:'Browse',
						// width:50,
						// style:{'margin-left':'0px','margin-top':'0px'},
						// hideLabel:true,
						// id: 'btnBrowseSetMeteringTarget',
						// handler:function()
						// {
							// Lookup_vupload(3,'Img Asset');
						// }
					// }
				// ]
			// }
			,
			{
				columnWidth:.28,
				layout: 'form',
				labelWidth:20,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						//fieldLabel: 'Before' + ' ',
						hideLabel:true,
						name: 'txtTagActionTarget',
						id: 'txtTagActionTarget',
						anchor: '93%'
					}
				]
			}
		]
	}
	
	return items;
};

function getItemPanelCardActionSetMetering3(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-34,
		items:
		[
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:65,
				border:false,
				items: 
				[ 	
					{
							xtype: 'textfield',
							fieldLabel: 'After' + ' ',
							name: 'txtAfterSetMetering',
							id: 'txtAfterSetMetering',
							anchor: '100%'
					}
				]
			},
			{
				columnWidth:.15,
				layout: 'form',
				labelWidth:50,
				height:23,
				border:false,
				items: 
				[ 	
					{
						xtype: 'label',
						fieldLabel: '( Days )',
						labelSeparator:'',
						style: 
						{
							'margin-left': '7px'
						},
						name: 'lblAfterSetMetering',
						id: 'lblAfterSetMetering',
						anchor: '100%'
					}
				]
			},
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:0,
				border:false,
				items: 
				[ 	
					mComboActionSetMetering3()
				]
			},
			{
				columnWidth:.1,
				layout: 'form',
				labelWidth:30,
				height:23,
				border:false,
				items: 
				[ 
					{
						xtype:'label',
						id:'lblLegendSetMeteringAfter',
						name:'lblLegendSetMeteringAfter',
						fieldLabel:'Legend '
					}
				]
			},
			{
				columnWidth:.05,
				layout: 'form',
				labelWidth:0,
				height:23,
				border:true,
				items: 
				[ 	
					{
						xtype:'label', 
						id:'gbrAfterSetMetering',
						name:'gbrAfterSetMetering',
						border:true,
						frame:true,
						html:'<div id="images" style="margin:0px;width:16px;height:16px;" align="center"> </div>'
					}
				]
			},
			{
				columnWidth:.01,
				layout: 'form',
				labelWidth:0,
				height:25,
				border:false,
				items: 
				[ 
				]
			 }
			//,				
			// {
				// columnWidth:.2,
				// layout: 'form',
				// labelWidth:0,
				// border:false,
				// items: 
				// [ 	
					// {
						// xtype:'button',
						// text:'Browse',
						// width:50,
						// style:{'margin-left':'0px','margin-top':'0px'},
						// hideLabel:true,
						// id: 'btnBrowseSetMeteringAfter',
						// handler:function()
						// {
							// Lookup_vupload(4,'Img Asset');
						// }
					// }
				// ]
			// }
			,
			{
				columnWidth:.28,
				layout: 'form',
				labelWidth:20,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						//fieldLabel: 'Before' + ' ',
						hideLabel:true,
						name: 'txtTagActionAfter',
						id: 'txtTagActionAfter',
						anchor: '93%'
					}
				]
			}
		]
	}
	
	return items;
};


function mComboActionSetMetering1()
{
  var cboActionCatEqp1 = new Ext.form.ComboBox
	(
		{
			id:'cboActionCatEqp1',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			hideLabel:true,
			emptyText:'',
			fieldLabel: '',	
			width:100,
			value:valueComboActionSetMet,
			store: new Ext.data.ArrayStore
			(
				{
					id:'strcboActionCatEqp1',
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Send Email'],[2, 'Send SMS']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',			
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectActionCatEqpBefore = b.data.Id ;					
				} 
			}
		}
	);
	return cboActionCatEqp1;
};

function mComboActionSetMetering2()
{
  var cboActionCatEqp2 = new Ext.form.ComboBox
	(
		{
			id:'cboActionCatEqp2',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			hideLabel:true,
			mode: 'local',
			emptyText:'',
			hideLabel:true,
			fieldLabel: '',	
			width:100,
			value:valueComboActionSetMet,
			store: new Ext.data.ArrayStore
			(
				{
					id:'strcboActionCatEqp2',
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Send Email'],[2, 'Send SMS']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',			
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectActionCatEqpTarget =b.data.Id ;					
				} 
			}
		}
	);
	return cboActionCatEqp2;
};


function mComboActionSetMetering3()
{
  var cboActionCatEqp3 = new Ext.form.ComboBox
	(
		{
			id:'cboActionCatEqp3',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			hideLabel:true,
			fieldLabel: '',	
			width:100,
			value:valueComboActionSetMet,
			store: new Ext.data.ArrayStore
			(
				{
					id:'strcboActionCatEqp3',
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Send Email'],[2, 'Send SMS']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',			
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectActionCatEqpAfter =b.data.Id ;					
				} 
			}
		}
	);
	return cboActionCatEqp3;
};

function SetMeteringSave(mBol) 
{
	if (ValidasiEntrySetMetering(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetMetering == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetMetering(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetMetering(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataAddFieldMetering(Ext.get('txtKode_SetEquipmentCat').dom.value);
							
							AddNewSetMetering = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetMetering(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetMetering(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetMetering(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetMetering(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataAddFieldMetering(Ext.get('txtKode_SetEquipmentCat').dom.value);
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetMetering(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetMetering(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetMeteringSave
///---------------------------------------------------------------------------------------///


function SetMeteringDelete() 
{
	if (ValidasiEntrySetMetering(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus('Metering') ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetMetering(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetMetering(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataAddFieldMetering(Ext.get('txtKode_SetEquipmentCat').dom.value);	
										SetMeteringAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetMetering(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetMetering(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetMetering(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtServiceSetMetering').getValue() == '' || Ext.get('txtKdServiceSetMetering').getValue() == '')
	{
		if (Ext.get('txtKode_SetMetering').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetMetering('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else if (Ext.get('txtKdServiceSetMetering').getValue() == '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetMetering(nmGetValidasiKosong(nmMetering),modul);
			};
		};
	};
	return x;
};

function ShowPesanWarningSetMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorSetMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoSetMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetMeteringInit(rowdata) 
{
    AddNewSetMetering = false;	
    Ext.get('txtKdServiceSetMetering').dom.value = rowdata.data.SERVICE_ID;
    Ext.get('txtIntervalSetMetering').dom.value = rowdata.data.INTERVAL;
    Ext.get('txtAsumsi1SetMetering').dom.value = rowdata.data.METERING_ASSUMPTIONS;
    Ext.get('txtAsumsi2SetMetering').dom.value = rowdata.data.DAYS_ASSUMPTIONS;
    Ext.get('txtServiceSetMetering').dom.value = rowdata.data.SERVICE_NAME;
    SelectTypeServiceCat = rowdata.data.TYPE_SERVICE_ID;
    SelectUnitServiceCat = rowdata.data.UNIT_ID;
    selectFlagSetMetering = rowdata.data.FLAG;
	Ext.get('cboSatuanSetMetering').dom.value = rowdata.data.UNIT;
	Ext.get('cboLogTypeSetMetering').dom.value = rowdata.data.SERVICE_TYPE;
	Ext.get('txtBeforeSetMetering').dom.value= rowdata.data.ACT_DAY_BEFORE;
	Ext.get('txtTargetSetMetering').dom.value= rowdata.data.ACT_DAY_TARGET;
	Ext.get('txtAfterSetMetering').dom.value= rowdata.data.ACT_DAY_AFTER;
	selectActionCatEqpBefore= rowdata.data.ACT_BEFORE;
	selectActionCatEqpTarget= rowdata.data.ACT_TARGET;
	selectActionCatEqpAfter= rowdata.data.ACT_AFTER;
	PathLegendBefore= rowdata.data.LEGEND_BEFORE;
	PathLegendTarget= rowdata.data.LEGEND_TARGET;
	PathLegendAfter= rowdata.data.LEGEND_AFTER;
	Ext.get('cboActionCatEqp1').dom.value= rowdata.data.ACTION_BEFORE;
	Ext.get('cboActionCatEqp2').dom.value= rowdata.data.ACTION_TARGET;
    Ext.get('cboActionCatEqp3').dom.value= rowdata.data.ACTION_AFTER;
	Dataview_fotoAssetSetMetering(PathLegendBefore,2)
	Dataview_fotoAssetSetMetering(PathLegendTarget,3)
	Dataview_fotoAssetSetMetering(PathLegendAfter,4)
	Ext.get('txtTagActionBefore').dom.value= rowdata.data.TAG_ACT_BEFORE;
	Ext.get('txtTagActionTarget').dom.value= rowdata.data.TAG_ACT_TARGET;
	Ext.get('txtTagActionAfter').dom.value= rowdata.data.TAG_ACT_AFTER;
	
};
///---------------------------------------------------------------------------------------///



function SetMeteringAddNew() 
{
    AddNewSetMetering = true;   
	Ext.get('txtKdServiceSetMetering').dom.value = '';
    Ext.get('txtIntervalSetMetering').dom.value = '';
	Ext.get('txtAsumsi1SetMetering').dom.value = '';
	Ext.get('txtAsumsi2SetMetering').dom.value = '';
	Ext.get('txtServiceSetMetering').dom.value = '';
	Ext.get('cboSatuanSetMetering').dom.value = '';
	Ext.get('txtBeforeSetMetering').dom.value= '';
	Ext.get('txtTargetSetMetering').dom.value= '';
	Ext.get('txtAfterSetMetering').dom.value= '';
	selectActionCatEqpBefore=selectActionCatEqpBefore;
	selectActionCatEqpTarget=selectActionCatEqpTarget;
	selectActionCatEqpAfter=selectActionCatEqpAfter;
	PathLegendBefore='';
	PathLegendTarget='';
	PathLegendAfter='';
	Ext.get('cboActionCatEqp1').dom.value=valueComboActionSetMet;
	Ext.get('cboActionCatEqp2').dom.value=valueComboActionSetMet;
    Ext.get('cboActionCatEqp3').dom.value=valueComboActionSetMet;
	rowSelectedSetMetering   = undefined;
	
	Ext.get('txtTagActionBefore').dom.value= '';
	Ext.get('txtTagActionTarget').dom.value= '';
	Ext.get('txtTagActionAfter').dom.value= '';
	
	
	Dataview_fotoAssetSetMetering(PathLegendBefore,2);
	Dataview_fotoAssetSetMetering(PathLegendTarget,3);
	Dataview_fotoAssetSetMetering(PathLegendAfter,4);
};
///---------------------------------------------------------------------------------------///


function getParamSetMetering() 
{
    var params =
	{	
		Table: 'ViewSetupServiceCategory',   
	    ServiceId: Ext.get('txtKdServiceSetMetering').getValue(),
	    CategoryId: Ext.get('txtKode_SetEquipmentCat').dom.value,
		TypeId: SelectTypeServiceCat,
		UnitId: SelectUnitServiceCat,
		IsAktif: '1',
		Interval: Ext.get('txtIntervalSetMetering').getValue(),
		MeteringAsumsi: Ext.get('txtAsumsi1SetMetering').getValue(),
		DaysAsumsi: Ext.get('txtAsumsi2SetMetering').getValue(),
		Flag: selectFlagSetMetering,
		Act_Day_Before: Ext.get('txtBeforeSetMetering').dom.value,
		Act_Day_After: Ext.get('txtAfterSetMetering').dom.value,
		Act_Day_Target: Ext.get('txtTargetSetMetering').dom.value,
		Act_Before:selectActionCatEqpBefore,
		Act_Target:selectActionCatEqpTarget,
		Act_After:selectActionCatEqpAfter,
		PathLegendBefore:PathLegendBefore,
		PathLegendTarget:PathLegendTarget,
		PathLegendAfter:PathLegendAfter,
		TagActBefore: Ext.get('txtTagActionBefore').dom.value,
		TagActTarget: Ext.get('txtTagActionTarget').dom.value,
		TagActAfter: Ext.get('txtTagActionAfter').dom.value
	};
    return params
};


//function RefreshDataSetMetering()
//{
//	dsSetMeteringList.load
//	(
//		{
//			params:
//			{
//				Skip: 0,
//				Take: selectCountSetMetering,
//				Sort: 'Metering_ID',
//				Sortdir: 'ASC',
//				target:'ViewSetupMetering',
//				param: ''
//			}
//		}
//	);
//	rowSelectedSetMetering = undefined;
//	return dsSetMeteringList;
//};
//
//function RefreshDataSetMeteringFilter()
//{
//	var KataKunci='';
//    if (Ext.get('txtSetMeteringFilter').getValue() != '')
//    {
//		KataKunci = ' where Metering like ~%' + Ext.get('txtSetMeteringFilter').getValue() + '%~';
//	};
//
//    if (Ext.get('txtSetMeteringAddressFilter').getValue() != '')
//    {
//		if (KataKunci === '')
//		{
//			KataKunci = ' where vend_address like  ~%' + Ext.get('txtSetMeteringAddressFilter').getValue() + '%~';
//		}
//		else
//		{
//			KataKunci += ' and  vend_address like  ~%' + Ext.get('txtSetMeteringAddressFilter').getValue() + '%~';
//		};
//	};
//
//    if (KataKunci != undefined)
//    {
//		dsSetMeteringList.load
//		(
//			{
//				params:
//				{
//					Skip: 0,
//					Take: selectCountSetMetering,
//					//Sort: 'Metering_ID',
//                                        Sort: 'metering_id',
//					Sortdir: 'ASC',
//					target:'ViewSetupMetering',
//					param: KataKunci
//				}
//			}
//		);
//    }
//	else
//	{
//		RefreshDataSetMetering();
//	};
//};



function mComboLogTypeSetMetering()
{
	var Field = ['TYPE_SERVICE_ID', 'SERVICE_TYPE'];
	dsTypeServiceCat = new WebApp.DataStore({ fields: Field });

	dsTypeServiceCat.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'service_type',
                            //Sort: 'SERVICE_TYPE',
			    Sortdir: 'ASC',
			    target: 'ViewSetupServicePMType',
			    param: ''
			}
		}
	);
	
  var cboLogTypeSetMetering = new Ext.form.ComboBox
	(
		{
			id:'cboLogTypeSetMetering',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Log Type' + ' ',			
			width:100,
			store: dsTypeServiceCat,
			valueField: 'TYPE_SERVICE_ID',
			displayField: 'SERVICE_TYPE',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					SelectTypeServiceCat=b.data.TYPE_SERVICE_ID ;					
				} 
			}
		}
	);
	
	dsTypeServiceCat.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_TYPE',
                            Sort: 'service_type',
			    Sortdir: 'ASC',
			    target: 'ViewSetupServicePMType',
			    param: ''
			}
		}
	);
	return cboLogTypeSetMetering;
};

function mComboSatuanSetMetering()
{
	var Field = ['UNIT_ID', 'UNIT'];
	dsUnitServiceCat = new WebApp.DataStore({ fields: Field });

	dsUnitServiceCat.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'UNIT',
                            Sort: 'unit',
			    Sortdir: 'ASC',
			    target: 'ViewSetupUnit',
			    param: ''
			}
		}
	);
  var cboSatuanSetMetering = new Ext.form.ComboBox
	(
		{
			id:'cboSatuanSetMetering',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Unit' + ' ',			
			width:100,
			store: dsUnitServiceCat,
			valueField: 'UNIT_ID',
			displayField: 'UNIT',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					SelectUnitServiceCat=b.data.UNIT_ID ;					
				} 
			}
		}
	);
	
	dsUnitServiceCat.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'UNIT',
                            Sort: 'unit',
			    Sortdir: 'ASC',
			    //target: 'viUnit', ---> cek ini kok beda dgn load diatas
                            target: 'ViewSetupUnit',
			    param: ''
			}
		}
	);
	return cboSatuanSetMetering;
};

function mComboFlagSetMetering()
{
  var cboFlagSetMetering = new Ext.form.ComboBox
    (
        {
            id:'cboFlagSetMetering',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'',
            fieldLabel: 'Log Type' + ' ',
            width:100,
            store: new Ext.data.ArrayStore
            (
                {
                        id: 0,
                        fields:
                        [
                                'Id',
                                'displayText'
                        ],
                data: [[1, 'Technic'], [0, 'Adm']]
                }
            ),
            valueField: 'Id',
            displayField: 'displayText',
            value:valueSelectFlagSetMetering,
            listeners:
            {
                'select': function(a,b,c)
                {
                        selectFlagSetMetering=b.data.Id ;
                }
            }
        }
    );
    return cboFlagSetMetering;
};