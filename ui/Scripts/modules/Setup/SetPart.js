
var dsSetPartList;
var AddNewSetPart;
var selectCountSetPart=50;
var rowSelectedSetPart;
var SetPartLookUps;
CurrentPage.page = getPanelSetPart(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetPart(mod_id) 
{
	
    var Field = ['PART_ID','PART_NAME','DESC_PART','PRICE'];
    dsSetPartList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetPart();

    var grListSetPart = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetPart',
		    stripeRows: true,
		    store: dsSetPartList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetPart=undefined;
							rowSelectedSetPart = dsSetPartList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetPart = dsSetPartList.getAt(ridx);
					if (rowSelectedSetPart != undefined)
					{
						SetPartLookUp(rowSelectedSetPart.data);
					}
					else
					{
						SetPartLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetPart',
                        header: nmKdPart2,            
                        dataIndex: 'PART_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colNameSetPart',
					    header: nmPart,					   
					    dataIndex: 'PART_NAME',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colDescSetPart',
					    header: nmDescSetPart,					   
					    dataIndex: 'DESC_PART',
					    width: 400,
					    sortable: true
					},
					{
					    id: 'colPriceSetPart',
					    header: nmPriceSetPart,					   
					    dataIndex: 'PRICE',
					    width: 100,
					    sortable: true,
						renderer: function(v, params, record) 
						{
							return formatCurrency(record.data.PRICE);
						}	
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetPartList,
            pageSize: selectCountSetPart,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),	
		    tbar:
			[
				{
				    id: 'btnEditSetPart',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetPart != undefined)
						{
						    SetPartLookUp(rowSelectedSetPart.data);
						}
						else
						{
						    SetPartLookUp();
						};
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetPart = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormPart,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupPart',
		    items: [grListSetPart],
		    tbar:
			[
				nmKdPart2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmKdPart2 + ' : ',
					id: 'txtKDSetPartFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmPart + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmPart + ' : ',
					id: 'txtSetPartFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, 
				{
				    id: 'btnRefreshSetPart',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetPartFilter();
					}
				}
			]
		}
	);
    //END var FormSetPart--------------------------------------------------

	RefreshDataSetPart();
    return FormSetPart ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetPartLookUp(rowdata) 
{
	var lebar=600;
    SetPartLookUps = new Ext.Window   	
    (
		{
		    id: 'SetPartLookUps',
		    title: nmFormPart,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 247,//220,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupPart',
		    modal: true,
		    items: getFormEntrySetPart(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetPart=undefined;
					RefreshDataSetPart();
				}
            }
		}
	);


    SetPartLookUps.show();
	if (rowdata == undefined)
	{
		SetPartAddNew();
	}
	else
	{
		SetPartInit(rowdata)
	};	
};


function getFormEntrySetPart(lebar) 
{
    var pnlSetPart = new Ext.FormPanel
    (
		{
		    id: 'PanelSetPart',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 317,//290,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupPart',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 167,//140,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetPart',
							labelWidth:80,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdPart2 + ' ',
								    name: 'txtKode_SetPart',
								    id: 'txtKode_SetPart',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmPart + ' ',
								    name: 'txtNameSetPart',
								    id: 'txtNameSetPart',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmPriceSetPart + ' ',
								    name: 'txtPriceSetPart',
								    id: 'txtPriceSetPart',
								    anchor: '40%'
								},
								{
								    xtype: 'textarea',
								    fieldLabel: nmDescSetPart + ' ',
								    name: 'txtDescSetPart',
								    id: 'txtDescSetPart',
									scroll:true,
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetPart',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetPartAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetPart',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetPartSave(false);
						RefreshDataSetPart();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetPart',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetPartSave(true);
						RefreshDataSetPart();
						if (x===undefined)
						{
							SetPartLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetPart',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetPartDelete() ;
							RefreshDataSetPart();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetPart',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950010);}
				}
			]
		}
	); 

    return pnlSetPart
};


function SetPartSave(mBol) 
{
	if (ValidasiEntrySetPart(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetPart == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetPart(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetPart(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetPart();
							if(mBol === false)
							{
								Ext.get('txtKode_SetPart').dom.readOnly=true;
							};
							AddNewSetPart = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetPart(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetPart(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetPart(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetPart(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetPart();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetPart(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetPart(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetPartSave
///---------------------------------------------------------------------------------------///

function SetPartDelete() 
{
	if (ValidasiEntrySetPart(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmPart) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetPart(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetPart(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetPart();
										SetPartAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetPart(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetPart(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetPart(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetPart').getValue() === '' || Ext.get('txtNameSetPart').getValue() === '')
	{
		if (Ext.get('txtKode_SetPart').getValue() === '')
		{
			x=0;
			if(mBolHapus === false)
			{
				ShowPesanWarningSetPart(nmGetValidasiKosong(nmKdPart),modul);
			};
			
		}
		else if (Ext.get('txtNameSetPart').getValue() === '')
		{	
			x=0;
			if(mBolHapus === false)
			{
				ShowPesanWarningSetPart(nmGetValidasiKosong(nmPart),modul);
			};
			
		};
	};
	return x;
};

function ShowPesanWarningSetPart(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorSetPart(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoSetPart(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetPartInit(rowdata) 
{
    AddNewSetPart = false;
    Ext.get('txtKode_SetPart').dom.value = rowdata.PART_ID;
    Ext.get('txtNameSetPart').dom.value = rowdata.PART_NAME;
	Ext.get('txtPriceSetPart').dom.value = rowdata.PRICE;
	Ext.get('txtDescSetPart').dom.value = rowdata.DESC_PART;
	Ext.get('txtKode_SetPart').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetPartAddNew() 
{
    AddNewSetPart = true;   
	Ext.get('txtKode_SetPart').dom.value = '';
    Ext.get('txtNameSetPart').dom.value = '';
	Ext.get('txtPriceSetPart').dom.value='';
	Ext.get('txtDescSetPart').dom.value = '';
	rowSelectedSetPart   = undefined;
	Ext.get('txtKode_SetPart').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetPart() 
{
    var params =
	{	
		Table: 'ViewSetupPart',   
	    PartNumber: Ext.get('txtKode_SetPart').getValue(),
	    Part: Ext.get('txtNameSetPart').getValue(),
		Price:Ext.get('txtPriceSetPart').getValue(),
		Desc:Ext.get('txtDescSetPart').getValue()
	};
    return params
};


function RefreshDataSetPart()
{	
	dsSetPartList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetPart, 
				//Sort: 'PART_ID',
                                Sort: 'part_id',
				Sortdir: 'ASC', 
				target:'ViewSetupPart',
				param: ''
			} 
		}
	);
	rowSelectedSetPart = undefined;
	return dsSetPartList;
};

function RefreshDataSetPartFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetPartFilter').getValue() != '')
    { 
		KataKunci = ' part_id like ~%' + Ext.get('txtKDSetPartFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetPartFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  part_name like  ~%' + Ext.get('txtSetPartFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  part_name like  ~%' + Ext.get('txtSetPartFilter').getValue() + '%~';
		};  
	};
	    
    if (KataKunci != undefined) 
    {  
		dsSetPartList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountSetPart, 
					//Sort: 'PART_ID',
                                        Sort: 'part_id',
					Sortdir: 'ASC', 
					target:'ViewSetupPart',
					param: KataKunci
				} 
			}
		);    
    }
	else
	{
		RefreshDataSetPart();
	};
};



function mComboMaksDataSetPart()
{
  var cboMaksDataSetPart = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetPart',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetPart,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetPart=b.data.displayText ;
					RefreshDataSetPart();
				} 
			}
		}
	);
	return cboMaksDataSetPart;
};
 
