
var dsSetRegionList;
var AddNewSetRegion;
var selectCountSetRegion=50;
var rowSelectedSetRegion;
var SetRegionLookUps;
CurrentPage.page = getPanelSetRegion(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetRegion(mod_id) 
{
	
    var Field = ['KD_REGION', 'REGION_NAME'];
    dsSetRegionList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetRegion();

    var grListSetRegion = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetRegion',
		    stripeRows: true,
		    store: dsSetRegionList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetRegion=undefined;
							rowSelectedSetRegion = dsSetRegionList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetRegion',
                        header: "Code",                      
                        dataIndex: 'KD_REGION',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'colSetRegion',
					    header: "Region",					   
					    dataIndex: 'REGION_NAME',
					    width: 200,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetRegion',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetRegion != undefined)
						{
						    SetRegionLookUp(rowSelectedSetRegion.data);
						}
						else
						{
						    SetRegionLookUp();
						}
				    }
				},' ','-'
			],
		    viewConfig: { forceFit: true }
		}
	);


    var FormSetRegion = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Region',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupRegion',
		    items: [grListSetRegion],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetRegionFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Region : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Region : ',
					id: 'txtSetRegionFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataSetRegion(),
				' ','-',
				{
				    id: 'btnRefreshSetRegion',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetRegionFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetRegion();
				}
			}
		}
	);
    //END var FormSetRegion--------------------------------------------------

	RefreshDataSetRegion();
    return FormSetRegion
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetRegionLookUp(rowdata) 
{
	var lebar=600;
    SetRegionLookUps = new Ext.Window   	
    (
		{
		    id: 'SetRegionLookUps',
		    title: 'Region',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupRegion',
		    modal: true,
		    items: getFormEntrySetRegion(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetRegion=undefined;
					RefreshDataSetRegion();
				}
            }
		}
	);


    SetRegionLookUps.show();
	if (rowdata == undefined)
	{
		SetRegionAddNew();
	}
	else
	{
		SetRegionInit(rowdata)
	}	
};


function getFormEntrySetRegion(lebar) 
{
    var pnlSetRegion = new Ext.FormPanel
    (
		{
		    id: 'PanelSetRegion',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupRegion',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetRegion',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Code ',
								    name: 'txtKode_SetRegion',
								    id: 'txtKode_SetRegion',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Region ',
								    name: 'txtSetRegion',
								    id: 'txtSetRegion',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetRegion',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',				   
				    handler: function() { SetRegionAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetRegion',
				    text: 'Simpan',
				    tooltip: 'Rekam Data ',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetRegionSave(false);
						RefreshDataSetRegion();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetRegion',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetRegionSave(true);
						RefreshDataSetRegion();
						if (x===undefined)
						{
							SetRegionLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetRegion',
				    text: 'Hapus',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetRegionDelete() ;
							RefreshDataSetRegion();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetRegion',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() {LoadReport(231032);}
				}
			]
		}
	); 

    return pnlSetRegion
};



function SetRegionSave(mBol) 
{
	if (ValidasiEntrySetRegion('Simpan Data') == 1 )
	{
		if (AddNewSetRegion == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetRegion(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetRegion('Data berhasil di simpan','Simpan Data');
							RefreshDataSetRegion();
							if(mBol === false)
							{
								Ext.get('txtKode_SetRegion').dom.readOnly=true;
							};
							AddNewSetRegion = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetRegion('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSetRegion('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetRegion(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetRegion('Data berhasil di edit','Edit Data');
							RefreshDataSetRegion();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetRegion('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else {
							ShowPesanErrorSetRegion('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetRegionSave
///---------------------------------------------------------------------------------------///

function SetRegionDelete() 
{
	if (ValidasiEntrySetRegion('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlDeleteData,
				params: getParamSetRegion(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetRegion('Data berhasil di hapus','Hapus Data');
						RefreshDataSetRegion();
						SetRegionAddNew();
					}
					else if (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningSetRegion('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorSetRegion('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetRegion(modul)
{
	var x = 1;
	if (Ext.get('txtKode_SetRegion').getValue() == '' || Ext.get('txtSetRegion').getValue() == '')
	{
		if (Ext.get('txtKode_SetRegion').getValue() == '')
		{
			ShowPesanWarningSetRegion('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningSetRegion('Sumber dana belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningSetRegion(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSetRegion(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSetRegion(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetRegionInit(rowdata) 
{
    AddNewSetRegion = false;
    Ext.get('txtKode_SetRegion').dom.value = rowdata.KD_REGION;
    Ext.get('txtSetRegion').dom.value = rowdata.REGION_NAME;
	Ext.get('txtKode_SetRegion').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetRegionAddNew() 
{
    AddNewSetRegion = true;   
	Ext.get('txtKode_SetRegion').dom.value = '';
	Ext.get('txtSetRegion').dom.value = '';
	rowSelectedSetRegion   =undefined;
	Ext.get('txtKode_SetRegion').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetRegion() 
{
    var params =
	{	
		Table: 'acc_sumber_dana',   
	    Kd_Sumber_Dana: Ext.get('txtKode_SetRegion').getValue(),
	    Sumber_Dana: Ext.get('txtSetRegion').getValue()	
	};
    return params
};


function RefreshDataSetRegion()
{	
	// dsSetRegionList.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: selectCountSetRegion, 
				// Sort: 'Kd_Sumber_Dana', 
				// Sortdir: 'ASC', 
				// target:'acc_sumber_dana',
				// param: ''
			// } 
		// }
	// );
	// rowSelectedSetRegion = undefined;
	// return dsSetRegionList;
};

function RefreshDataSetRegionFilter() 
{   
	// var KataKunci;
    // if (Ext.get('txtKDSetRegionFilter').getValue() != '')
    // { 
		// KataKunci = 'kode@ Kd_Sumber_Dana =' + Ext.get('txtKDSetRegionFilter').getValue(); 
	// }
    // if (Ext.get('txtSetRegionFilter').getValue() != '')
    // { 
		// if (KataKunci == undefined)
		// {
			// KataKunci = 'nama@ Sumber_Dana =' + Ext.get('txtSetRegionFilter').getValue();
		// }
		// else
		// {
			// KataKunci += '##@@##nama@ Sumber_Dana =' + Ext.get('txtSetRegionFilter').getValue();
		// }  
	// }
        
    if (KataKunci != undefined) 
    {  
		// dsSetRegionList.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCountSetRegion, 
					// Sort: 'Kd_Sumber_Dana', 
					// Sortdir: 'ASC', 
					// target:'acc_sumber_dana',
					// param: KataKunci
				// }			
			// }
		// );        
    }
	else
	{
		RefreshDataSetRegion();
	}
};



function mComboMaksDataSetRegion()
{
  var cboMaksDataSetRegion = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetRegion',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetRegion,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetRegion=b.data.displayText ;
					RefreshDataSetRegion();
				} 
			}
		}
	);
	return cboMaksDataSetRegion;
};
 
