
var dsSetServiceCatList;
var AddNewSetServiceCat;

var rowSelectedSetServiceCat;
var SetServiceCatLookUps;

function SetServiceCatLookUp(rowdata) 
{
	var lebar=600;
    SetServiceCatLookUps = new Ext.Window   	
    (
		{
		    id: 'SetServiceCatLookUps',
		    title: nmFormService,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupService',
		    modal: true,
		    items: getFormEntrySetServiceCat(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetServiceCat=undefined;
					
				}
            }
		}
	);


    SetServiceCatLookUps.show();
	if (rowdata == undefined)
	{
		SetServiceCatAddNew();
	}
	else
	{
		
	}	
};


function getFormEntrySetServiceCat(lebar) 
{
    var pnlSetServiceCat = new Ext.FormPanel
    (
		{
		    id: 'PanelSetServiceCat',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupService',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetServiceCat',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdService2 + ' ',
								    name: 'txtKode_SetServiceCat',
								    id: 'txtKode_SetServiceCat',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmService + ' ',
								    name: 'txtNameSetServiceCat',
								    id: 'txtNameSetServiceCat',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetServiceCat',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetServiceCatAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetServiceCat',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetServiceCatSave(false);
						
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetServiceCat',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetServiceCatSave(true);
						
						if (x===undefined)
						{
							SetServiceCatLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetServiceCat',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetServiceCatDelete() ;
										
					}
				},'-','->','-',
				{
					id:'btnPrintSetServiceCat',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() 
					{
						LoadReport(950016);
					}
				}
			]
		}
	); 

    return pnlSetServiceCat
};


function SetServiceCatSave(mBol) 
{
	if (ValidasiEntrySetServiceCat(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetServiceCat == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetServiceCat(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetServiceCat(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							if(mBol === false)
							{
								Ext.get('txtKode_SetServiceCat').dom.value=cst.ServiceId;
							};
							AddNewSetServiceCat = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetServiceCat(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetServiceCat(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetServiceCat(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetServiceCat(nmPesanEditSukses,nmHeaderEditData);
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetServiceCat(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetServiceCat(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetServiceCatSave
///---------------------------------------------------------------------------------------///

function SetServiceCatDelete() 
{
	if (ValidasiEntrySetServiceCat(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmService) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetServiceCat(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetServiceCat(nmPesanHapusSukses,nmHeaderHapusData);
										
										SetServiceCatAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetServiceCat(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetServiceCat(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetServiceCat(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetServiceCat').getValue() == '' || (Ext.get('txtNameSetServiceCat').getValue() == ''))
	{
		if (Ext.get('txtKode_SetServiceCat').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetServiceCat(nmGetValidasiKosong(nmKdService),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetServiceCat').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetServiceCat(nmGetValidasiKosong(nmService),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetServiceCat(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetServiceCat(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetServiceCat(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};



function SetServiceCatAddNew() 
{
    AddNewSetServiceCat = true;   
	Ext.get('txtKode_SetServiceCat').dom.value = '';
    Ext.get('txtNameSetServiceCat').dom.value = '';
	rowSelectedSetServiceCat   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetServiceCat() 
{
    var params =
	{	
		Table: 'ViewSetupService',   
	    ServiceId: Ext.get('txtKode_SetServiceCat').getValue(),
	    Service: Ext.get('txtNameSetServiceCat').getValue()	
	};
    return params
};



 
