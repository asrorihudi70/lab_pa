var CurrentPartIntServicePM =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordServPMParts = Ext.data.Record.create
(
        [
                {name: 'SERVICE_ID', mapping:'SERVICE_ID'},
           {name: 'PART_NAME', mapping:'PART_NAME'},
           {name: 'PART_ID', mapping:'PART_ID'},
           {name: 'INSTRUCTION', mapping:'INSTRUCTION'},
           {name: 'QTY', mapping:'QTY'},
           {name: 'UNIT_COST', mapping:'UNIT_COST'},
           {name: 'TOT_COST', mapping:'TOT_COST'}
        ]
);
	
	
var dsServicePM;
var selectServicePM;
var dsCategorySetServPart;
var selectCategorySetServPart;


var IdService;
var cellSelectedServicePMParts;
var dsServiceServPMPartsView;



var selectStatusServPMPartsEntry;
var dsTRServPMPartsList;
var dsDTLTRServicePMPartsList;
var AddNewServPMParts = true;
var selectCountServPMParts = 50;
var now = new Date();
var rowSelectedServPMParts;
var TRServPMPartsLookUps;

var vTabPanelRepairExternalServPMParts;
var valueServiceServPMPartsView = ' All';
var selectServiceServPMPartsView = 'xxx';
var rootTreeServPMPartsEntry;
var treeSetServPMPartsEntry;
var StrTreeComboServPMPartsEntry;


CurrentPage.page = getPanelServPMParts(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelServPMParts(mod_id) {
    var Field = ['SERVICE_ID','SERVICE_NAME','CATEGORY_NAME','CATEGORY_ID'];
    dsTRServPMPartsList = new WebApp.DataStore({ fields: Field });

   GetStrTreeComboSetServicePartEntry();
    var grListTRServPMParts = new Ext.grid.EditorGridPanel
	(
		{
		    stripeRows: true,
		    store: dsTRServPMPartsList,
		    anchor: '100% 100%',
		    columnLines: true,
		    border: false,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{
					    rowselect: function(sm, row, rec) 
						{
					        rowSelectedServPMParts = dsTRServPMPartsList.getAt(row);
					    }
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedServPMParts = dsTRServPMPartsList.getAt(ridx);
					 if (rowSelectedServPMParts != undefined) 
					{
						ServPMPartsLookUp(rowSelectedServPMParts.data);
					}
					else 
					{
						ServPMPartsLookUp();
					}
					
				}
			},
		    cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					
					{
					    id: 'colIdViewServPMParts', //+ Code
					    header: nmIdServSetServPart,
					    dataIndex: 'SERVICE_ID',
					    sortable: true,
					    width: 120
					},
					{
					    id: 'colNameViewServPMParts', //+ Code
					    header: nmNamaServSetServPart,
					    dataIndex: 'SERVICE_NAME',
					    sortable: true,
					    width: 500
					}
				]
			),
			
			viewConfig: 
			{
				forceFit: true
			},
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsTRServPMPartsList,
            pageSize: selectCountServPMParts,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditServPMParts',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
				        if (rowSelectedServPMParts != undefined) 
						{
				            ServPMPartsLookUp(rowSelectedServPMParts.data);
				        }
				        else 
						{
				            ServPMPartsLookUp();
				        }
				    }
				},' ', '-'
			]
		}
	);

    var FormTRServPMParts = new Ext.Panel
	(
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormSetServPart,
		    border: false,
		    shadhow: true,
		    iconCls: 'ServPMParts',
		    margins: '0 5 5 0',
		    items: [grListTRServPMParts],
		    tbar:
			[
				nmNamaServSetServPart + ' : ', ' ', mComboServiceServPMPartsView()
				, ' ',
				{
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    anchor: '25%',
				    handler: function(sm, row, rec) {
				        RefreshDataServPMPartsFilter();
				    }
				}
			],
		    listeners:
			{
			    'afterrender': function() {
			    }
			}
		}
	);
	
    RefreshDataServPMParts();
    RefreshDataServPMParts();

    return FormTRServPMParts

};

function RefreshDataServPMParts() {
    dsTRServPMPartsList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountServPMParts,
			    //Sort: 'SERVICE_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    //target: 'ViewServicePMParts',
                            //target: 'viServicePMParts',
                            target: 'viewSetServicePMParts',
			    param: ''
			}
		}
	);
    return dsTRServPMPartsList;
};

function ServPMPartsLookUp(rowdata) {
    var lebar = 700; //735;
	GetStrTreeComboSetServicePartEntry();
    TRServPMPartsLookUps = new Ext.Window
	(
		{
		    id: 'gridServPMParts',
		    title: nmTitleFormSetServPart,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 430,
		    border: false,
		    resizable: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'ServPMParts',
		    modal: true,
		    items: getFormEntryTRServPMParts(lebar),
		    listeners:
			{
			    activate: function() 
				{
			    },
			    afterShow: function() 
				{
			        this.activate();
			    },
			    deactivate: function() 
				{
			        rowSelectedServPMParts = undefined;
			        RefreshDataServPMPartsFilter(true);
			    }
			}
		}
	);

    TRServPMPartsLookUps.show();
    if (rowdata == undefined) 
	{
        ServPMPartsAddNew();
    }
    else {
        TRServPMPartsInit(rowdata)
    }
};

function getFormEntryTRServPMParts(lebar) {
    var pnlTRServPMParts = new Ext.FormPanel
	(
		{
		    id: 'PanelTRServPMParts',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    bodyStyle: 'padding:10px 10px 0px 10px',
		    height: 518,
		    anchor: '100%',
		    width: lebar,
		    border: false,
		    //autoScroll:true,
		    items: [getItemPanelInputServPMParts(lebar)],
		    tbar:
			[
				{
				    text: nmTambah,
				    id: 'btnTambahServPMParts',
				    tooltip: nmTambah,
				    iconCls: 'add',
				    handler: function() {
				        ServPMPartsAddNew();			        

				    }
				}, '-',
				{
				    text: nmSimpan,
				    id: 'btnSimpanServPMParts',
				    tooltip: nmSimpan,
				    iconCls: 'save',
				    handler: function() {
				        ServPMPartsSave(false);
				        RefreshDataServPMPartsFilter(false);
				        Ext.getCmp('FormPanelCardLayoutServPMParts').getLayout().setActiveItem(0);
				    }
				}, '-',
				{
				    text: nmSimpanKeluar,
				    id: 'btnSimpanKeluarServPMParts',
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() {
				        var x = ServPMPartsSave(true);
				        RefreshDataServPMPartsFilter(false);
				        if (x === undefined) {
				            TRServPMPartsLookUps.close();
				        };
				    }
				}, '-',
				{
				    text: nmHapus,
				    id: 'btnHapusServPMParts',
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() {
				        ServPMPartsDelete();
				        RefreshDataServPMPartsFilter(false);
				    }
				}, '-',
				{
				    text: nmLookup,
				    id: 'btnLookupServPMParts',
				    tooltip: nmLookup,
				    iconCls: 'find',
				    handler: function() {
				        // var criteria;
				        // criteria = ' WHERE Kd_Unit_Kerja =~' + selectServPMParts + '~';
				        // FormLookupCashIn(criteria);
				    }
				},
				'-', '->', '-',
				{
				    text: nmCetak,
				    id: 'btnCetakServPMParts',
				    tooltip: nmCetak,
				    iconCls: 'print',
				    handler: function()
				    { CetakServPMParts() }
				}
			]
		}
	);




    var FormTRServPMParts = new Ext.Panel
	(
		{
		    id: 'FormTRServPMParts',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRServPMParts]
		}
	);

    return FormTRServPMParts
};
///---------------------------------------------------------------------------------------///

function CetakServPMParts() {

};

 		   
function RecordBaruServPMParts() 
{
    var p = new mRecordServPMParts
	(
		{
			'SERVICE_ID':'',
		    'PART_NAME': '',
		    'PART_ID': '',
		    'INSTRUCTION': '',
		    'QTY': 1,
		    'UNIT_COST': '',
		    'TOT_COST': ''		   
		}
	);

    return p;
};

function TRServPMPartsInit(rowdata) {
    AddNewServPMParts = false;

	selectCategorySetServPart = rowdata.CATEGORY_ID;    
	selectServicePM = rowdata.SERVICE_ID;    
	Ext.get('cboServicePM').dom.value = rowdata.SERVICE_NAME;
	Ext.get('cboCategoryServicePart').dom.value = rowdata.CATEGORY_NAME;
	RefreshDataServPMPartsDetail(rowdata.SERVICE_ID,rowdata.CATEGORY_ID) 

};

function RefreshDataServPMPartsDetail(serv_id,cat_id) 
{
    var strKriteriaServPMParts='';
    //strKriteriaServPMParts = 'where SERVICE_ID = ~' + serv_id + '~ AND CATEGORY_ID=~' + cat_id + '~'
    strKriteriaServPMParts = 'service_id = ~' + serv_id + '~ AND category_id = ~' + cat_id + '~'
   
    dsDTLTRServicePMPartsList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_PM_ID',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    //target: 'viServicePMParts',
                            target: 'viewSetServicePMParts',
			    param: strKriteriaServPMParts
			}
		}
	);
    return dsDTLTRServicePMPartsList;
};


///---------------------------------------------------------------------------------------///
function ServPMPartsAddNew() 
{
    AddNewServPMParts = true;
    rowSelectedServPMParts = undefined;
	dsDTLTRServicePMPartsList.removeAll();
	selectServicePM  = undefined;
	selectCategorySetServPart = undefined;
	Ext.get('cboServicePM').dom.value = '';
	Ext.get('cboCategoryServicePart').dom.value='';
};
///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamServPMParts() {
    var params =
	{
	    Table:'viewSetServicePMParts',
		List:getArrDetailServicePM(),
		JmlField: '7',
		JmlList:GetListCountServicePMParts(),
		CatId:selectCategorySetServPart,
		ServId:selectServicePM,
		Hapus:1
	};

    return params
};

function GetListCountServicePMParts()
{
	
	var x=0;
	for(var i = 0 ; i < dsDTLTRServicePMPartsList.getCount();i++)
	{
		if (dsDTLTRServicePMPartsList.data.items[i].data.PART_ID != '' || dsDTLTRServicePMPartsList.data.items[i].data.PART_NAME  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrDetailServicePM()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRServicePMPartsList.getCount();i++)
	{
		if (dsDTLTRServicePMPartsList.data.items[i].data.PART_ID != '' && dsDTLTRServicePMPartsList.data.items[i].data.PART_NAME != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'PART_ID=' + dsDTLTRServicePMPartsList.data.items[i].data.PART_ID
			y += z + 'SERVICE_PM_ID='+ selectServicePM
			y += z + 'CATEGORY_ID='+ selectCategorySetServPart
			y += z + 'QTY='+dsDTLTRServicePMPartsList.data.items[i].data.QTY
			y += z + 'UNIT_COST='+ dsDTLTRServicePMPartsList.data.items[i].data.UNIT_COST
			y += z + 'TOT_COST='+dsDTLTRServicePMPartsList.data.items[i].data.TOT_COST
			y += z + 'INSTRUCTION='+dsDTLTRServicePMPartsList.data.items[i].data.INSTRUCTION
			
			if (i === (dsDTLTRServicePMPartsList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function getItemPanelInputServPMParts(lebar) {
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar - 35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border: false,
	    height: 734,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar - 35,
			    labelWidth: 100,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'fieldset',
					    title: nmTitleFieldServSetServPart,
					    height: '80px',
					    items:
						[
							mComboCategoryEqpSetServPart(),
							mComboServicePM()							
						]
					},getItemPanelCardLayoutServPMParts()
				]
			}
		]
	};
    return items;
};

function RefreshDataComboService()
{
	
	selectServicePM='';
	
	dsServicePM.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_NAME',
                            Sort: 'service_name',
			    Sortdir: 'ASC',
			    target: 'viComboServicePM',
			    //param: ' WHERE Service_Id<>~xxx~ AND CATEGORY_ID=~' + selectCategorySetServPart + '~'
                            param: ' service_id <> ~xxx~ AND category_id = ~' + selectCategorySetServPart + '~'
			}
		}
	);
	return dsServicePM;
};

function mComboServicePM()
{
	var Field = ['SERVICE_ID', 'SERVICE_NAME'];
	dsServicePM = new WebApp.DataStore({ fields: Field });

	RefreshDataComboService();
	
  var cboServicePM = new Ext.form.ComboBox
	(
		{
			id:'cboServicePM',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihService,
			fieldLabel: nmNamaServSetServPart + ' ',			
			anchor:'60%',
			store: dsServicePM,
			valueField: 'SERVICE_ID',
			displayField: 'SERVICE_NAME',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectServicePM=b.data.SERVICE_ID ;
				} 
			}
		}
	);
	
	RefreshDataComboService();
	
	return cboServicePM;
};

function getItemPanelCardLayoutServPMParts() 
{
    var FormPanelCardLayoutServPMParts = new Ext.Panel
	(
		{
		    id: 'FormPanelCardLayoutServPMParts',
		    trackResetOnLoad: true,
		    width: 670,
		    height: 260,
		    layout: 'card',
		    border: false,
		    activeItem: 0,
		    items:
			[
				{
				    xtype: 'panel',
				    layout: 'form',
				    border: false,
				    id: 'CardServPMParts2',
				    items:
					[
						getItemTabPanelRepairExternalServPMParts()
					]
				}
			]
		}
	);

    return FormPanelCardLayoutServPMParts;
};


function getItemTabPanelRepairExternalServPMParts() {
    vTabPanelRepairExternalServPMParts = new Ext.TabPanel
	(
		{
		    id: 'vTabPanelRepairExternalServPMParts',
		    region: 'center',
		    margins: '7 7 7 7',
		    style:
			{
			    'margin-top': '1px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 255,
		    width: 665,
		    //anchor: '100%',
		    items:
			[
			    
				{
				    title: nmTitleFormSetServPart,
				    id: 'tabWOPartExternalServPMParts',
				    items:
					 [
						GetDTLPartExternalServPMPartsGrid()
					 ],
				    listeners:
					{
					    activate: function() {
					    }
					}
				}
			]
		}
	)

    return vTabPanelRepairExternalServPMParts;
};


function GetDTLPartExternalServPMPartsGrid() {
    var fldDetail = ['PART_NAME', 'PART_ID', 'INSTRUCTION', 'QTY', 'UNIT_COST','TOT_COST'];

    dsDTLTRServicePMPartsList = new WebApp.DataStore({ fields: fldDetail })

    var grIdServiceExternalServPMParts = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDTLTRServicePMPartsList,
		    border: true,
		    columnLines: true,
		    frame: false,
		    //anchor: '100%',
		    width: 648,
		    height: 215,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
							cellSelectedServicePMParts = dsDTLTRServicePMPartsList.getAt(row);
					        CurrentPartIntServicePM.row = row;
					        CurrentPartIntServicePM.data = cellSelectedServicePMParts;
					    }
					}
				}
			),
		    cm: PartExternalServPMPartsDetailColumModel(),
		    tbar:
			         [
				        {
				            id: 'btnTambahBrsPartExternalServPMParts',
				            text: nmTambahBaris,
				            tooltip: nmTambahBaris,
				            iconCls: 'AddRow',
				            handler: function() {
								TambahBarisServicePMParts();
				            }
				        }, '-',
				        {
				            id: 'btnHpsBrsPartExternalServPMParts',
				            text: nmHapusBaris,
				            tooltip: nmHapusBaris,
				            iconCls: 'RemoveRow',
				            handler: function() 
							{
								if (dsDTLTRServicePMPartsList.getCount() > 0 )
									{
										if (cellSelectedServicePMParts != undefined)
										{
											if(CurrentPartIntServicePM != undefined)
											{
												HapusBarisServicePMParts();
											}
										}
										else
										{
											ShowPesanWarningServPMParts(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
										}
									}
				            }
				        }, ' ', '-'
			            ]
		    , viewConfig: { forceFit: true }
		}
	);

    return grIdServiceExternalServPMParts;
};

function HapusBarisServicePMParts()
{
	if ( cellSelectedServicePMParts != undefined )
	{
		if (cellSelectedServicePMParts.data.PART_NAME != '' && cellSelectedServicePMParts.data.PART_ID != '')
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentPartIntServicePM.row + 1) + ' ' + nmOperatorDengan + ' ' + nmPartNumSetServPart + ' : ' + cellSelectedServicePMParts.data.PART_ID + ' ' + nmOperatorAnd + ' '+nmPartNamaSetServPart + ' : ' + cellSelectedServicePMParts.data.PART_NAME ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsDTLTRServicePMPartsList.data.items[CurrentPartIntServicePM.row].data.PART_ID === '')
							{
								
								dsDTLTRServicePMPartsList.removeAt(CurrentPartIntServicePM.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												ServicePMPartsDeleteDetail();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDTLTRServicePMPartsList.removeAt(CurrentPartIntServicePM.row);
		};
	}
};

function ServicePMPartsDeleteDetail()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData,
			params:  getParamServPMPartsDeleteDetail(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoServPMParts(nmPesanHapusSukses,nmHeaderHapusData);
					dsDTLTRServicePMPartsList.removeAt(CurrentPartIntServicePM.row);
					cellSelectedServicePMParts=undefined;
					RefreshDataServPMPartsDetail(selectServicePM,selectCategorySetServPart);
					AddNewServPMParts = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningServPMParts(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanWarningServPMParts(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamServPMPartsDeleteDetail()
{
	var params =
	{	
		Table: 'viewSetServicePMParts',   
	    PART_ID: CurrentPartIntServicePM.data.data.PART_ID,
		CatId:selectCategorySetServPart,
		ServId:selectServicePM,
		Hapus:2
	};
	
    return params
};


function TambahBarisServicePMParts()
{
	var x=true;
	
		if (x === true)
		{
			var p = RecordBaruServPMParts();
			dsDTLTRServicePMPartsList.insert(dsDTLTRServicePMPartsList.getCount(), p);
		};
};

function PartExternalServPMPartsDetailColumModel() {
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
			    id: 'colKdPartExternalServPMParts',
			    header: nmPartNumSetServPart,
			    dataIndex: 'PART_ID',
			    width: 100,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolKdPartExternalServPMParts',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() {
						        if (Ext.EventObject.getKey() === 13) {
						            var str = '';
									if(Ext.get('fieldcolKdPartExternalServPMParts').dom.value != '')
									{
										str=' where PART_ID like ~%' + Ext.get('fieldcolKdPartExternalServPMParts').dom.value + '%~'; 
									};
									GetLookupPartServicePM(str,CurrentPartIntServicePM.row,dsDTLTRServicePMPartsList);

						        };
						    }
						}
					}
				)
			},
			{
			    id: 'colPartExternalServPMParts',
			    header: nmPartNamaSetServPart,
			    dataIndex: 'PART_NAME',
			    sortable: false,
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartExternalServPMParts',
					    allowBlank: false,
					    enableKeyEvents: true,
					    listeners:
						{
						    'specialkey': function() 
							{
						        if (Ext.EventObject.getKey() === 13) 
								{						            
									var str='';
									if(Ext.get('fieldcolPartExternalServPMParts').dom.value != '')
									{
										str=' where PART_NAME like ~%' + Ext.get('fieldcolPartExternalServPMParts').dom.value + '%~'; 
									};
									GetLookupPartServicePM(str,CurrentPartIntServicePM.row,dsDTLTRServicePMPartsList);
						        };
						    }
						}
					}
				),
			    width: 180,
			    renderer: function(value, cell) {
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			},
			{
			    id: 'colDescPartExternalServPMParts',
			    header: nmInsSetServPart,
			    width: 180,
			    dataIndex: 'INSTRUCTION',
			    editor: new Ext.form.TextField
				(
					{
					    id: 'fieldcolPartExternalServPMParts',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 30
					}
				),
			    renderer: function(value, cell) {
			        var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
			        return str;
			    }
			},
			{
			    id: 'colQtyPartExternalServPMParts',
			    header: nmQtySetServPart,
			    width: 90,
				align:'center',
			    dataIndex: 'QTY',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolQtyPartExternalServPMParts',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				)
			},
			{
			    id: 'colCostPartExternalServPMParts',
			    header: nmCostSetServPart,
			    width: 150,
				align:'right',
			    dataIndex: 'UNIT_COST',
			    editor: new Ext.form.NumberField
				(
					{
					    id: 'fieldcolCostPartExternalServPMParts',
					    allowBlank: true,
					    enableKeyEvents: true,
					    width: 90
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST);
				}
			},
			{
			    id: 'colTotCostPartExternalServPMParts',
			    header: nmTotCostSetServPart,
			    width: 150,
				align:'right',
			    dataIndex: 'TOT_COST',
			    renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST * record.data.QTY);
				}	
			}
		]
	)
};



function mComboMaksDataServPMParts() {
    var cboMaksDataServPMParts = new Ext.form.ComboBox
	(
		{
		    id: 'cboMaksDataServPMParts',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel: nmMaksData + ' ',
		    width: 50,
		    store: new Ext.data.ArrayStore
			(
				{
				    id: 0,
				    fields:
					[
						'Id',
						'displayText'
					],
				    data: [[1, 50], [2, 100], [3, 200], [4, 500], [5, 1000]]
				}
			),
		    valueField: 'Id',
		    displayField: 'displayText',
		    value: selectCountServPMParts,
		    listeners:
			{
			    'select': function(a, b, c) {
			        selectCountServPMParts = b.data.displayText;
			        RefreshDataServPMPartsFilter(false);
			    }
			}
		}
	);
    return cboMaksDataServPMParts;
};

function RefreshDataServPMPartsFilter(mBol) 
{
    var strServPMParts = '';
	if (selectServiceServPMPartsView != 'xxx')
	{
		//strServPMParts=' WHERE SERVICE_PM_ID=~'+ selectServiceServPMPartsView + '~'
                //strServPMParts=' service_pm_id = ~'+ selectServiceServPMPartsView + '~'
                strServPMParts=' service_id = ~'+ selectServiceServPMPartsView + '~'
	};
	dsTRServPMPartsList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: selectCountServPMParts,
			    //Sort: 'SERVICE_PM_ID',
                            //Sort: 'service_pm_id',
                            Sort: 'service_id',
			    Sortdir: 'ASC',
			    //target: 'ViewServicePMParts',
                            //target: 'viServicePMParts',
                            target: 'viewSetServicePMParts',
			    param: strServPMParts
			}
		}
	);
    return dsTRServPMPartsList;
};


function ServPMPartsSave(mBol) 
{
	if (ValidasiEntryServPMParts(nmHeaderSimpanData,false) == 1 )
	{
		if (IdService == '' || IdService === undefined) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamServPMParts(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoServPMParts(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataServPMParts();
							if(mBol === false)
							{
								IdService=cst.WOId;
							};
							AddNewServPMParts = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningServPMParts(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorServPMParts(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamServPMParts(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoServPMParts(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataServPMParts();
							if(mBol === false)
							{
								//RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningServPMParts(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorServPMParts(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ShowPesanWarningServPMParts(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
			width:250,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorServPMParts(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
			width:250,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoServPMParts(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
			width:250,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO
		}
	);
};


function ServPMPartsDelete() 
{
	if (ValidasiEntryServPMParts(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus('Service Part') ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamServPMParts(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoServPMParts(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataServPMParts();
										ServPMPartsAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningServPMParts(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorServPMParts(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};

function ValidasiEntryServPMParts(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('cboCategoryServicePart').getValue() == '' || (Ext.get('cboServicePM').getValue() == '') || selectServicePM === '' || selectCategorySetServPart === '' || dsDTLTRServicePMPartsList.getCount() === 0 )
	{
		if (Ext.get('cboCategoryServicePart').getValue() == '' || selectCategorySetServPart === '' || selectCategorySetServPart === undefined )
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningServPMParts(nmGetValidasiKosong(nmCatSetServPart),modul);
			};
		}
		else if (Ext.get('cboServicePM').getValue() === '' || selectServicePM === '' || selectServicePM === undefined )
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningServPMParts(nmGetValidasiKosong(nmNamaServSetServPart),modul);
			};
			
		}
		else if (dsDTLTRServicePMPartsList.getCount() === 0)
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningServPMParts(nmGetValidasiKosong(nmPartDtlSetServPart),modul);
			};
		};
	};
	
	
	return x;
};



function mComboServiceServPMPartsView() {

var Field = ['SERVICE_ID', 'SERVICE_NAME'];
	dsServiceServPMPartsView = new WebApp.DataStore({ fields: Field });

	dsServiceServPMPartsView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_NAME',
                            Sort: 'service_name',
			    Sortdir: 'ASC',
			    target: 'viComboServicePM',
			    param: ' '
			}
		}
	);
	
  var cboServiceServPMPartsView = new Ext.form.ComboBox
	(
		{
			id:'cboServicePMView',
			name:'cboServicePMView',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihService,
			fieldLabel: nmNamaServSetServPart + ' ',			
			anchor:'60%',
			store: dsServiceServPMPartsView,
			valueField: 'SERVICE_ID',
			displayField: 'SERVICE_NAME',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectServiceServPMPartsView=b.data.SERVICE_ID ;
				} 
			}
		}
	);
	
	dsServiceServPMPartsView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_NAME',
                            Sort: 'service_name',
			    Sortdir: 'ASC',
			    target: 'viComboServicePM',
			     param: ' '
			}
		}
	);
	
	return cboServiceServPMPartsView;
	
    
};



function GetLookupPartServicePM(str,idx,ds)
{
	if (IdService === '' || IdService === undefined)
	{
		var p = new mRecordServPMParts
		(
			{
				'SERVICE_ID':'',				
				'PART_NAME':'', 
				'PART_ID':'',
				'INSTRUCTION':ds.data.items[idx].data.INSTRUCTION,
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.TOT_COST				
			}
		);
		FormLookupPart(str,ds,p,true,'',false);
	}
	else
	{	
		var p = new mRecordServPMParts
		(
			{
				'SERVICE_ID':IdService,				
				'PART_NAME':'', 
				'PART_ID':'',
				'INSTRUCTION':ds.data.items[idx].data.INSTRUCTION,
				'QTY':ds.data.items[idx].data.QTY,
				'UNIT_COST':ds.data.items[idx].data.UNIT_COST,
				'TOT_COST':ds.data.items[idx].data.TOT_COST				
			}
		);
		FormLookupPart(str,ds,p,false,idx,false);
	};
};

function mComboCategoryEqpSetServPart()
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySetServPart = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboSetupSetServPartEntry = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSetServPMPartsEntry = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeServPMPartsEntry  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						children: StrTreeComboServPMPartsEntry,
						autoScroll: true
					}
				) 
				treeSetServPMPartsEntry.setRootNode(rootTreeServPMPartsEntry); 
				
				this.list = treeSetServPMPartsEntry
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSetupSetServPartEntry.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSetServPMPartsEntry
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.leaf === true)
				{
					this.setRawValue(node.attributes.text);
					selectCategorySetServPart = node.attributes.id;
					Ext.get('cboServicePM').dom.value='';
					RefreshDataComboService();		
				}
				else
				{
					selectCategorySetServPart=undefined;
					this.setRawValue('');
					ShowPesanWarningServPMParts(nmWarningDtlCat,nmTitleFormSetServPart)
				};
			
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySetServPart
		}
	);
	
	
	var cboCategoryServicePart = new Ext.TreeComboSetupSetServPartEntry
	(
		{
			id:'cboCategoryServicePart',
			fieldLabel: nmCatSetServPart + ' ',
			emptyText: nmPilihCategory,
			anchor:'60%'
		}
	);
	
	return cboCategoryServicePart;

};

function GetStrTreeComboSetServicePartEntry()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboServPMPartsEntry= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};