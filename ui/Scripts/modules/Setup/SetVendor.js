
var dsSetVendorList;
var AddNewSetVendor;
var selectCountSetVendor=50;
var rowSelectedSetVendor;
var SetVendorLookUps;
CurrentPage.page = getPanelSetVendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetVendor(mod_id) 
{
	
    var Field = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
    dsSetVendorList = new WebApp.DataStore({ fields: Field });
	
	RefreshDataSetVendor();
	
    var grListSetVendor = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetVendor',
		    stripeRows: true,
		    store: dsSetVendorList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetVendor=undefined;
							rowSelectedSetVendor = dsSetVendorList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetVendor = dsSetVendorList.getAt(ridx);
					if (rowSelectedSetVendor != undefined)
					{
						SetVendorLookUp(rowSelectedSetVendor.data);
					}
					else
					{
						SetVendorLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetVendor',
                        header: nmKdVendor2,                      
                        dataIndex: 'VENDOR_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetVendor',
					    header: nmVendor,					   
					    dataIndex: 'VENDOR',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colContact1SetVendor',
					    header: nmVendorContact1,					   
					    dataIndex: 'CONTACT1',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colContact2SetVendor',
					    header: nmVendorContact2,					   
					    dataIndex: 'CONTACT2',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetAdressVendor',
					    header: nmVendorAddress,					   
					    dataIndex: 'VEND_ADDRESS',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetCityVendor',
					    header: nmVendorCity,					   
					    dataIndex: 'VEND_CITY',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetPosCodeVendor',
					    header: nmVendorPosCode,					   
					    dataIndex: 'VEND_POS_CODE',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'colSetCountryVendor',
					    header: nmVendorCountry,					   
					    dataIndex: 'COUNTRY',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetPhone1Vendor',
					    header: nmVendorPhone1,					   
					    dataIndex: 'VEND_PHONE1',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colSetPhone2Vendor',
					    header: nmVendorPhone2,					   
					    dataIndex: 'VEND_PHONE2',
					    width: 150,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetVendorList,
            pageSize: selectCountSetVendor,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetVendor',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetVendor != undefined)
						{
						    SetVendorLookUp(rowSelectedSetVendor.data);
						}
						else
						{
						    SetVendorLookUp();
						}
				    }
				},' ','-'
			]
		}
	);


    var FormSetVendor = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormVendor,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupVendor',
		    items: [grListSetVendor],
		    tbar:
			[
				nmVendor + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmVendor + ' : ',
					id: 'txtSetVendorFilter',                   
					width:100,
					onInit: function() { }
				}, ' ','-',
				nmVendorAddress + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmVendorAddress + ' : ',
					id: 'txtSetVendorAddressFilter',                 
					anchor: '95%',
					onInit: function() { }
				}, '-',
				{
				    id: 'btnRefreshSetVendor',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetVendorFilter();
					}
				}
			]
		}
	);
    
	RefreshDataSetVendor();
    return FormSetVendor ;
};
///------------------------------------------------------------------------------------------------------------///




function SetVendorLookUp(rowdata) 
{
	var lebar=600;
    SetVendorLookUps = new Ext.Window   	
    (
		{
		    id: 'SetVendorLookUps',
		    title: nmFormVendor,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 306,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupVendor',
		    modal: true,
		    items: getFormEntrySetVendor(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetVendor=undefined;
					RefreshDataSetVendor();
				}
            }
		}
	);


    SetVendorLookUps.show();
	if (rowdata == undefined)
	{
		SetVendorAddNew();
	}
	else
	{
		SetVendorInit(rowdata)
	}	
};


function getFormEntrySetVendor(lebar) 
{
    var pnlSetVendor = new Ext.FormPanel
    (
		{
		    id: 'PanelSetVendor',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 350,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupVendor',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 226,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetVendor',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdVendor2 + ' ',
								    name: 'txtKode_SetVendor',
								    id: 'txtKode_SetVendor',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendor + ' ',
								    name: 'txtNameSetVendor',
								    id: 'txtNameSetVendor',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorContact1 + ' ',
								    name: 'txtContact1SetVendor',
								    id: 'txtContact1SetVendor',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorContact2 + ' ',
								    name: 'txtContact2SetVendor',
								    id: 'txtContact2SetVendor',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorAddress + ' ',
								    name: 'txtAddressSetVendor',
								    id: 'txtAddressSetVendor',
								    anchor: '100%'
								},getItemPanelCitySetVendor(),
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorCountry + ' ',
								    name: 'txtCountrySetVendor',
								    id: 'txtCountrySetVendor',
								    anchor: '69.3%'
								},getItemPanelPhoneSetVendor()
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetVendor',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetVendorAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetVendor',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetVendorSave(false);
						RefreshDataSetVendor();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetVendor',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetVendorSave(true);
						RefreshDataSetVendor();
						if (x===undefined)
						{
							SetVendorLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetVendor',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetVendorDelete() ;
							RefreshDataSetVendor();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetVendor',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950012);}
				}
			]
		}
	); 

    return pnlSetVendor
};

function getItemPanelCitySetVendor()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.672,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorCity + ' ',
						name: 'txtCitySetVendor',
						id: 'txtCitySetVendor',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:75,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPosCode + ' ',
						name: 'txtPosCodeSetVendor',
						id: 'txtPosCodeSetVendor',						
						anchor: '100%'
					}	
				]
			}
		]
	}
	
	return items;
};



function getItemPanelPhoneSetVendor()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPhone1 + ' ',
						name: 'txtPhone1SetVendor',
						id: 'txtPhone1SetVendor',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				labelWidth:75,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPhone2 + ' ',
						name: 'txtPhone2SetVendor',
						id: 'txtPhone2SetVendor',						
						anchor: '100%'
					}	
				]
			}
		]
	}
	
	return items;
};

function SetVendorSave(mBol) 
{
	if (ValidasiEntrySetVendor(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetVendor == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetVendor(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetVendor(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetVendor();
							if(mBol === false)
							{
								Ext.get('txtKode_SetVendor').dom.value=cst.VendorId;
							};
							AddNewSetVendor = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetVendor(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetVendor(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetVendor(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetVendor(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetVendor();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetVendor(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetVendor(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetVendorSave
///---------------------------------------------------------------------------------------///


function SetVendorDelete() 
{
	if (ValidasiEntrySetVendor(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmVendor2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetVendor(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetVendor(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetVendor();
										SetVendorAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetVendor(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetVendor(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetVendor(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetVendor').getValue() == '' || Ext.get('txtNameSetVendor').getValue() == '')
	{
		if (Ext.get('txtKode_SetVendor').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetVendor('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else if (Ext.get('txtNameSetVendor').getValue() == '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetVendor(nmGetValidasiKosong(nmVendor),modul);
			};
		};
	};
	return x;
};

function ShowPesanWarningSetVendor(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorSetVendor(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoSetVendor(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetVendorInit(rowdata) 
{
    AddNewSetVendor = false;
    Ext.get('txtKode_SetVendor').dom.value = rowdata.VENDOR_ID;
    Ext.get('txtNameSetVendor').dom.value = rowdata.VENDOR;
	
	if (rowdata.CONTACT1 === null)
	{
		Ext.get('txtContact1SetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtContact1SetVendor').dom.value = rowdata.CONTACT1
	};
	
	if (rowdata.CONTACT2 === null)
	{
		Ext.get('txtContact2SetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtContact2SetVendor').dom.value = rowdata.CONTACT2
	};
	
	if (rowdata.VEND_ADDRESS === null)
	{
		Ext.get('txtAddressSetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtAddressSetVendor').dom.value = rowdata.VEND_ADDRESS;
	};
	
	if (rowdata.VEND_CITY === null)
	{
		Ext.get('txtCitySetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtCitySetVendor').dom.value = rowdata.VEND_CITY;
	};
	
	if (rowdata.VEND_POS_CODE === null)
	{
		Ext.get('txtPosCodeSetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPosCodeSetVendor').dom.value = rowdata.VEND_POS_CODE;
	};
	
	if (rowdata.COUNTRY === null)
	{
		Ext.get('txtCountrySetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtCountrySetVendor').dom.value = rowdata.COUNTRY;
	};
	
	if (rowdata.VEND_PHONE1 === null)
	{
		Ext.get('txtPhone1SetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPhone1SetVendor').dom.value = rowdata.VEND_PHONE1;
	};
	
	if (rowdata.VEND_PHONE2 === null)
	{
		Ext.get('txtPhone2SetVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPhone2SetVendor').dom.value = rowdata.VEND_PHONE2;
	};
};
///---------------------------------------------------------------------------------------///



function SetVendorAddNew() 
{
    AddNewSetVendor = true;   
	Ext.get('txtKode_SetVendor').dom.value = '';
    Ext.get('txtNameSetVendor').dom.value = '';
	Ext.get('txtContact1SetVendor').dom.value = '';
	Ext.get('txtContact2SetVendor').dom.value = '';
	Ext.get('txtAddressSetVendor').dom.value = '';
	Ext.get('txtCitySetVendor').dom.value = '';
	Ext.get('txtPosCodeSetVendor').dom.value = '';
	Ext.get('txtCountrySetVendor').dom.value = '';
	Ext.get('txtPhone1SetVendor').dom.value = '';
	Ext.get('txtPhone2SetVendor').dom.value = '';
	rowSelectedSetVendor   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetVendor() 
{
    var params =
	{	
		Table: 'ViewSetupVendor',   
	    VendorId: Ext.get('txtKode_SetVendor').getValue(),
	    Vendor: Ext.get('txtNameSetVendor').getValue(),
		Contact1: Ext.get('txtContact1SetVendor').getValue(),
		Contact2: Ext.get('txtContact2SetVendor').getValue(),
		Address: Ext.get('txtAddressSetVendor').getValue(),
		City: Ext.get('txtCitySetVendor').getValue(),
		PosCode: Ext.get('txtPosCodeSetVendor').getValue(),
		Country: Ext.get('txtCountrySetVendor').getValue(),
		Phone1: Ext.get('txtPhone1SetVendor').getValue(),
		Phone2: Ext.get('txtPhone2SetVendor').getValue()
	};
    return params
};


function RefreshDataSetVendor()
{	
	dsSetVendorList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetVendor, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC', 
				target:'ViewSetupVendor',
				param: ''
			} 
		}
	);
	rowSelectedSetVendor = undefined;
	return dsSetVendorList;
};

function RefreshDataSetVendorFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtSetVendorFilter').getValue() != '')
    { 
		KataKunci = '  vendor like ~%' + Ext.get('txtSetVendorFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetVendorAddressFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  vend_address like  ~%' + Ext.get('txtSetVendorAddressFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  vend_address like  ~%' + Ext.get('txtSetVendorAddressFilter').getValue() + '%~';
		};  
	};
	    
    if (KataKunci != undefined) 
    {  
		dsSetVendorList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountSetVendor, 
					//Sort: 'VENDOR_ID',
                                        Sort: 'vendor_id',
					Sortdir: 'ASC', 
					target:'ViewSetupVendor',
					param: KataKunci
				} 
			}
		);     
    }
	else
	{
		RefreshDataSetVendor();
	};
};



function mComboMaksDataSetVendor()
{
  var cboMaksDataSetVendor = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetVendor',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetVendor,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetVendor=b.data.displayText ;
					RefreshDataSetVendor();
				} 
			}
		}
	);
	return cboMaksDataSetVendor;
};
 
