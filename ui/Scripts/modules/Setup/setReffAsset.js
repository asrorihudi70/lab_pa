
var dsSetReferenceList;
var AddNewSetReference;
var selectCountSetReference=50;
var rowSelectedSetReference;
var SetReferenceLookUps;
CurrentPage.page = getPanelSetReference(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetReference(mod_id) 
{
	
    var Field =['REFF_ID','DESCRIPTION','PATH_IMAGE'];
    dsSetReferenceList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetReference();

    var grListSetReference = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetReference',
		    stripeRows: true,
		    store: dsSetReferenceList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetReference=undefined;
							rowSelectedSetReference = dsSetReferenceList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetReference = dsSetReferenceList.getAt(ridx);
					if (rowSelectedSetReference != undefined)
					{
						SetReferenceLookUp(rowSelectedSetReference.data);
					}
					else
					{
						SetReferenceLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colPathImageReff',
					    header: '',					
						dataIndex: 'PATH_IMAGE',
					    sortable: true,
					    width: 20,
						align:'center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store) 
						{						   
							return '<img src="' + value + '" class="text-desc-legend"/>'	
											
						}
					},
                    {
                        id: 'colKodeSetReference',
                        header: nmKdReference2,                      
                        dataIndex: 'REFF_ID',
                        sortable: true,
                        width: 60
                    },
					{
					    id: 'colSetReference',
					    header: nmReference,					   
					    dataIndex: 'DESCRIPTION',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetReferenceList,
            pageSize: selectCountSetReference,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetReference',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetReference != undefined)
						{
						    SetReferenceLookUp(rowSelectedSetReference.data);
						}
						else
						{
						    SetReferenceLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetReference = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Reference',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupReference',
		    items: [grListSetReference],
		    tbar:
			[
				nmKdReference2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetReferenceFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmReference + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Reference : ',
					id: 'txtSetReferenceFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetReference(),
				' ','-',
				{
				    id: 'btnRefreshSetReference',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetReferenceFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetReference();
				}
			}
		}
	);
    //END var FormSetReference--------------------------------------------------

	RefreshDataSetReference();
    return FormSetReference ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetReferenceLookUp(rowdata) 
{
	var lebar=600;
    SetReferenceLookUps = new Ext.Window   	
    (
		{
		    id: 'SetReferenceLookUps',
		    title: 'Reference',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 200,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupReference',
		    modal: true,
		    items: getFormEntrySetReference(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetReference=undefined;
					RefreshDataSetReference();
				}
            }
		}
	);


    SetReferenceLookUps.show();
	if (rowdata == undefined)
	{
		SetReferenceAddNew();
	}
	else
	{
		SetReferenceInit(rowdata)
	}	
};


function getFormEntrySetReference(lebar) 
{
    var pnlSetReference = new Ext.FormPanel
    (
		{
		    id: 'PanelSetReference',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 170,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupReference',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 120,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetReference',
							labelWidth:80,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdReference2 + ' ',
								    name: 'txtKode_SetReference',
								    id: 'txtKode_SetReference',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmReference + ' ',
								    name: 'txtNameSetReference',
								    id: 'txtNameSetReference',
								    anchor: '100%'
								},getItemPanelAttachSetReff(lebar)
								
							]
						}//,getItemPanelAttachSetReff()
					]
				}//,getItemPanelAttachSetReff()
			],
		    tbar:
			[
				{
				    id: 'btnAddSetReference',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetReferenceAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetReference',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetReferenceSave(false);
						RefreshDataSetReference();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetReference',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetReferenceSave(true);
						RefreshDataSetReference();
						if (x===undefined)
						{
							SetReferenceLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetReference',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetReferenceDelete() ;
							RefreshDataSetReference();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetReference',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() 
					{
						//LoadReport(950002);
					}
				}
			]
		}
	); 

    return pnlSetReference
};


function SetReferenceSave(mBol) 
{
	if (ValidasiEntrySetReference(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetReference == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetReference(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetReference(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetReference();
							if(mBol === false)
							{
								Ext.get('txtKode_SetReference').dom.value=cst.ReffId;
							};
							AddNewSetReference = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetReference(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetReference(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetReference(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetReference(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetReference();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetReference(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetReference(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetReferenceSave
///---------------------------------------------------------------------------------------///

function SetReferenceDelete() 
{
	if (ValidasiEntrySetReference(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmReference) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetReference(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetReference(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetReference();
										SetReferenceAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetReference(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetReference(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetReference(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetReference').getValue() == '' || (Ext.get('txtNameSetReference').getValue() == ''))
	{
		if (Ext.get('txtKode_SetReference').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetReference(nmGetValidasiKosong(nmKdReference),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetReference').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetReference(nmGetValidasiKosong(nmReference),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetReference(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetReference(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetReference(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetReferenceInit(rowdata) 
{
    AddNewSetReference = false;
    Ext.get('txtKode_SetReference').dom.value = rowdata.REFF_ID;
    Ext.get('txtNameSetReference').dom.value = rowdata.DESCRIPTION;
	Ext.get('txtAttach_SetReff').dom.value = rowdata.PATH_IMAGE;	
    Dataview_fotoReff(rowdata.PATH_IMAGE);
};
///---------------------------------------------------------------------------------------///



function SetReferenceAddNew() 
{
    AddNewSetReference = true;   
	Ext.get('txtKode_SetReference').dom.value = '';
    Ext.get('txtNameSetReference').dom.value = '';
	Ext.get('txtAttach_SetReff').dom.value='';
	rowSelectedSetReference   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetReference() 
{
    var params =
	{	
            Table: 'ViewSetupReference',
	    ReffId: Ext.get('txtKode_SetReference').getValue(),
	    deskription: Ext.get('txtNameSetReference').getValue(),
            Path: Ext.get('txtAttach_SetReff').getValue()
	};
    return params
};


function RefreshDataSetReference()
{	
	dsSetReferenceList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetReference, 
					//Sort: 'Reff_ID',
                                        Sort: 'reff_id',
					Sortdir: 'ASC', 
					target:'ViewSetupReference',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetReference = undefined;
	return dsSetReferenceList;
};

function RefreshDataSetReferenceFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetReferenceFilter').getValue() != '')
    { 
		//KataKunci = ' where Reff_ID like ~%' + Ext.get('txtKDSetReferenceFilter').getValue() + '%~';
                KataKunci = ' reff_id like ~%' + Ext.get('txtKDSetReferenceFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetReferenceFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where Reference like  ~%' + Ext.get('txtSetReferenceFilter').getValue() + '%~';
                        KataKunci = ' reference like  ~%' + Ext.get('txtSetReferenceFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  Reference like  ~%' + Ext.get('txtSetReferenceFilter').getValue() + '%~';
                        KataKunci += ' and  reference like  ~%' + Ext.get('txtSetReferenceFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetReferenceList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetReference, 
					//Sort: 'Reff_ID',
                                        Sort: 'reff_id',
					Sortdir: 'ASC', 
					target:'ViewSetupReference',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetReference();
	};
};



function mComboMaksDataSetReference()
{
  var cboMaksDataSetReference = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetReference',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetReference,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetReference=b.data.displayText ;
					RefreshDataSetReference();
				} 
			}
		}
	);
	return cboMaksDataSetReference;
};
 

function getItemPanelAttachSetReff(lebar)
{	
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-70,
		items:
		[
			{
				columnWidth:.72,
				layout: 'form',
				border:false,
				labelWidth:80,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmPathImageSetRef + ' ',
						name: 'txtAttach_SetReff',
						id: 'txtAttach_SetReff',
						anchor: '99%'
					}
				]
			},
			{
				columnWidth:.05,
				layout: 'form',
				labelWidth:0,
				height:23,
				border:true,
				items: 
				[ 	
					{
						xtype:'label', 
						id:'gbrSetReff',
						name:'gbrSetReff',
						border:true,
						frame:true,
						html:'<div id="images" style="margin-top:-2px;width:16px;height:16px;" align="center"> </div>'
					}
				]
			},
			{
				columnWidth:.009,
				layout: 'form',
				labelWidth:0,
				height:23,
				border:false,
				items: 
				[ 
				]
			},
			{
				columnWidth:.2,
				layout: 'form',
				labelWidth:0,
				border:false,
				items: 
				[ 	
					{
						xtype:'button',
						text:nmBtnBrowse,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnBrowseSetReff',
						handler:function()
						{
							Lookup_vupload(5,'Img Asset');
						}
					}					
				]
			}
		]
	}
	
	return items;
};

function Dataview_fotoReff(StrPath) 
{
	if (StrPath=='' || StrPath==null)
	{
		StrPath='./Img Asset/blank.png'
	}
	var pgbr=Ext.getCmp('gbrSetReff');    
    Ext.DomHelper.overwrite(pgbr.getEl(),
	{                                	
	  tag: 'img', src: StrPath ,style:'margin:3px;width:16px;height:16px;'  
    }, true).show(true).frame();		
};ss