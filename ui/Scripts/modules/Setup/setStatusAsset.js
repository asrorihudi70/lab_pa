
var dsSetStatusAssetList;
var AddNewSetStatusAsset;
var selectCountSetStatusAsset=50;
var rowSelectedSetStatusAsset;
var SetStatusAssetLookUps;
CurrentPage.page = getPanelSetStatusAsset(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetStatusAsset(mod_id) 
{
	
    var Field =['STATUS_ASSET_ID','STATUS_ASSET'];
    dsSetStatusAssetList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetStatusAsset();

    var grListSetStatusAsset = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetStatusAsset',
		    stripeRows: true,
		    store: dsSetStatusAssetList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetStatusAsset = undefined;
							rowSelectedSetStatusAsset = dsSetStatusAssetList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetStatusAsset = dsSetStatusAssetList.getAt(ridx);
					if (rowSelectedSetStatusAsset != undefined)
					{
						SetStatusAssetLookUp(rowSelectedSetStatusAsset.data);
					}
					else
					{
						SetStatusAssetLookUp();
					}
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetStatusAsset',
                        header: nmKdStatusAsset2,                      
                        dataIndex: 'STATUS_ASSET_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetStatusAsset',
					    header: nmStatusAsset,					   
					    dataIndex: 'STATUS_ASSET',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetStatusAssetList,
            pageSize: selectCountSetStatusAsset,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetStatusAsset',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetStatusAsset != undefined)
						{
						    SetStatusAssetLookUp(rowSelectedSetStatusAsset.data);
						}
						else
						{
						    SetStatusAssetLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetStatusAsset = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormStatusAsset,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupStatusAsset',
		    items: [grListSetStatusAsset],
		    tbar:
			[
				nmKdStatusAsset2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetStatusAssetFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmStatusAsset + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'StatusAsset : ',
					id: 'txtSetStatusAssetFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetStatusAsset(),
				' ','-',
				{
				    id: 'btnRefreshSetStatusAsset',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetStatusAssetFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetStatusAsset();
				}
			}
		}
	);
    //END var FormSetStatusAsset--------------------------------------------------

	RefreshDataSetStatusAsset();
    return FormSetStatusAsset ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetStatusAssetLookUp(rowdata) 
{
	var lebar=600;
    SetStatusAssetLookUps = new Ext.Window   	
    (
		{
		    id: 'SetStatusAssetLookUps',
		    title: nmFormStatusAsset,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupStatusAsset',
		    modal: true,
		    items: getFormEntrySetStatusAsset(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetStatusAsset=undefined;
					RefreshDataSetStatusAsset();
				}
            }
		}
	);


    SetStatusAssetLookUps.show();
	if (rowdata == undefined)
	{
		SetStatusAssetAddNew();
	}
	else
	{
		SetStatusAssetInit(rowdata)
	}	
};


function getFormEntrySetStatusAsset(lebar) 
{
    var pnlSetStatusAsset = new Ext.FormPanel
    (
		{
		    id: 'PanelSetStatusAsset',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupStatusAsset',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetStatusAsset',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdStatusAsset2 + ' ',
								    name: 'txtKode_SetStatusAsset',
								    id: 'txtKode_SetStatusAsset',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmStatusAsset + ' ',
								    name: 'txtNameSetStatusAsset',
								    id: 'txtNameSetStatusAsset',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetStatusAsset',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetStatusAssetAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetStatusAsset',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetStatusAssetSave(false);
						RefreshDataSetStatusAsset();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetStatusAsset',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetStatusAssetSave(true);
						RefreshDataSetStatusAsset();
						if (x===undefined)
						{
							SetStatusAssetLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetStatusAsset',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetStatusAssetDelete() ;
							RefreshDataSetStatusAsset();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetStatusAsset',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950002);}
				}
			]
		}
	); 

    return pnlSetStatusAsset
};


function SetStatusAssetSave(mBol) 
{
	if (ValidasiEntrySetStatusAsset(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetStatusAsset == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetStatusAsset(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetStatusAsset(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetStatusAsset();
							if(mBol === false)
							{
								Ext.get('txtKode_SetStatusAsset').dom.value=cst.StatusAssetId;
							};
							AddNewSetStatusAsset = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetStatusAsset(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetStatusAsset(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetStatusAsset(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetStatusAsset(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetStatusAsset();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetStatusAsset(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetStatusAsset(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetStatusAssetSave
///---------------------------------------------------------------------------------------///

function SetStatusAssetDelete() 
{
	if (ValidasiEntrySetStatusAsset(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmStatusAsset) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetStatusAsset(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetStatusAsset(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetStatusAsset();
										SetStatusAssetAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetStatusAsset(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetStatusAsset(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetStatusAsset(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetStatusAsset').getValue() == '' || (Ext.get('txtNameSetStatusAsset').getValue() == ''))
	{
		if (Ext.get('txtKode_SetStatusAsset').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetStatusAsset(nmGetValidasiKosong(nmKdStatusAsset),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetStatusAsset').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetStatusAsset(nmGetValidasiKosong(nmStatusAsset),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetStatusAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetStatusAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetStatusAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetStatusAssetInit(rowdata) 
{
    AddNewSetStatusAsset = false;
    Ext.get('txtKode_SetStatusAsset').dom.value = rowdata.STATUS_ASSET_ID;
    Ext.get('txtNameSetStatusAsset').dom.value = rowdata.STATUS_ASSET;
};
///---------------------------------------------------------------------------------------///



function SetStatusAssetAddNew() 
{
    AddNewSetStatusAsset = true;   
	Ext.get('txtKode_SetStatusAsset').dom.value = '';
    Ext.get('txtNameSetStatusAsset').dom.value = '';
	rowSelectedSetStatusAsset   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetStatusAsset() 
{
    var params =
	{	
		Table: 'ViewSetupStatusAsset',   
	    StatusAssetId: Ext.get('txtKode_SetStatusAsset').getValue(),
	    StatusAsset: Ext.get('txtNameSetStatusAsset').getValue()	
	};
    return params
};


function RefreshDataSetStatusAsset()
{	
	dsSetStatusAssetList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetStatusAsset, 
					//Sort: 'STATUS_ASSET_ID',
                                        Sort: 'status_asset_id',
					Sortdir: 'ASC', 
					target:'ViewSetupStatusAsset',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetStatusAsset = undefined;
	return dsSetStatusAssetList;
};

function RefreshDataSetStatusAssetFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetStatusAssetFilter').getValue() != '')
    { 
		//KataKunci = ' where STATUS_ID like ~%' + Ext.get('txtKDSetStatusAssetFilter').getValue() + '%~';
                KataKunci = ' status_id like ~%' + Ext.get('txtKDSetStatusAssetFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetStatusAssetFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where STATUS_ASSET like  ~%' + Ext.get('txtSetStatusAssetFilter').getValue() + '%~';
                        KataKunci = ' status_asset like  ~%' + Ext.get('txtSetStatusAssetFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  STATUS_ASSET like  ~%' + Ext.get('txtSetStatusAssetFilter').getValue() + '%~';
                        KataKunci += ' and  status_asset like  ~%' + Ext.get('txtSetStatusAssetFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetStatusAssetList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetStatusAsset, 
					//Sort: 'STATUS_ASSET_ID',
                                        Sort: 'status_asset_id',
					Sortdir: 'ASC', 
					target:'ViewSetupStatusAsset',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetStatusAsset();
	};
};



function mComboMaksDataSetStatusAsset()
{
  var cboMaksDataSetStatusAsset = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetStatusAsset',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetStatusAsset,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetStatusAsset=b.data.displayText ;
					RefreshDataSetStatusAsset();
				} 
			}
		}
	);
	return cboMaksDataSetStatusAsset;
};
 
