
var dsSetBahasaList;
var AddNewSetBahasa;
var selectCountSetBahasa=50;
var rowSelectedSetBahasa;
var SetBahasaLookUps;
CurrentPage.page = getPanelSetBahasa(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetBahasa(mod_id) 
{
	
    var Field =['LANGUAGE_ID','LANGUAGE'];
    dsSetBahasaList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetBahasa();

    var grListSetBahasa = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetBahasa',
		    stripeRows: true,
		    store: dsSetBahasaList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetBahasa=undefined;
							rowSelectedSetBahasa = dsSetBahasaList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetBahasa=undefined;
					rowSelectedSetBahasa = dsSetBahasaList.getAt(ridx);
					if (rowSelectedSetBahasa != undefined)
					{
						SetBahasaLookUp(rowSelectedSetBahasa.data);
					}
					else
					{
						SetBahasaLookUp();
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetBahasa',
                        header: nmIDSetupBhs,                      
                        dataIndex: 'LANGUAGE_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetBahasa',
					    header: nmNamaSetupBhs,					   
					    dataIndex: 'LANGUAGE',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetBahasaList,
            pageSize: selectCountSetBahasa,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetBahasa',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetBahasa != undefined)
						{
						    SetBahasaLookUp(rowSelectedSetBahasa.data);
						}
						else
						{
						    SetBahasaLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetBahasa = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormSetupBhs,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupLocation',
		    items: [grListSetBahasa],
		    tbar:
			[
				nmIDSetupBhs + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmIDSetupBhs + ' ',
					id: 'txtKDSetBahasaFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmNamaSetupBhs + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmNamaSetupBhs + ' : ',
					id: 'txtSetBahasaFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetBahasa(),
				' ','-',
				{
				    id: 'btnRefreshSetBahasa',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetBahasaFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetBahasa();
				}
			}
		}
	);
    //END var FormSetBahasa--------------------------------------------------

	RefreshDataSetBahasa();
    return FormSetBahasa ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetBahasaLookUp(rowdata) 
{
	var lebar=600;
    SetBahasaLookUps = new Ext.Window   	
    (
		{
		    id: 'SetBahasaLookUps',
		    title: nmTitleFormSetupBhs,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupLocation',
		    modal: true,
		    items: getFormEntrySetBahasa(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetBahasa=undefined;
					RefreshDataSetBahasa();
				}
            }
		}
	);


    SetBahasaLookUps.show();
	if (rowdata == undefined)
	{
		SetBahasaAddNew();
	}
	else
	{
		SetBahasaInit(rowdata)
	}	
};


function getFormEntrySetBahasa(lebar) 
{
    var pnlSetBahasa = new Ext.FormPanel
    (
		{
		    id: 'PanelSetBahasa',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupLocation',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetBahasa',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmIDSetupBhs + ' ',
								    name: 'txtKode_SetBahasa',
								    id: 'txtKode_SetBahasa',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmNamaSetupBhs + ' ',
								    name: 'txtNameSetBahasa',
								    id: 'txtNameSetBahasa',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetBahasa',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetBahasaAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetBahasa',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetBahasaSave(false);
						RefreshDataSetBahasa();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetBahasa',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetBahasaSave(true);
						RefreshDataSetBahasa();
						if (x===undefined)
						{
							SetBahasaLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetBahasa',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetBahasaDelete() ;
							RefreshDataSetBahasa();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetBahasa',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',
					disabled:true,
					handler: function() 
					{
						//LoadReport(950002);
					}
				}
			]
		}
	); 

    return pnlSetBahasa
};


function SetBahasaSave(mBol) 
{
	if (ValidasiEntrySetBahasa(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetBahasa == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetBahasa(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetBahasa(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetBahasa();
							if(mBol === false)
							{
								Ext.get('txtKode_SetBahasa').dom.value=cst.LanguageId;
							};
							AddNewSetBahasa = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetBahasa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetBahasa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetBahasa(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetBahasa(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetBahasa();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetBahasa(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetBahasa(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetBahasaSave
///---------------------------------------------------------------------------------------///

function SetBahasaDelete() 
{
	if (ValidasiEntrySetBahasa(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmNamaSetupBhs) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetBahasa(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetBahasa(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetBahasa();
										SetBahasaAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetBahasa(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetBahasa(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetBahasa(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetBahasa').getValue() == '' || (Ext.get('txtNameSetBahasa').getValue() == ''))
	{
		if (Ext.get('txtKode_SetBahasa').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetBahasa(nmGetValidasiKosong(nmIDSetupBhs),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetBahasa').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetBahasa(nmGetValidasiKosong(nmNamaSetupBhs),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetBahasa(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetBahasaInit(rowdata) 
{
    AddNewSetBahasa = false;
    Ext.get('txtKode_SetBahasa').dom.value = rowdata.LANGUAGE_ID;
    Ext.get('txtNameSetBahasa').dom.value = rowdata.LANGUAGE;
};
///---------------------------------------------------------------------------------------///



function SetBahasaAddNew() 
{
    AddNewSetBahasa = true;   
	Ext.get('txtKode_SetBahasa').dom.value = '';
    Ext.get('txtNameSetBahasa').dom.value = '';
	rowSelectedSetBahasa   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetBahasa() 
{
    var params =
	{	
		Table: 'ViewSetupBahasa',   
	    LanguageId: Ext.get('txtKode_SetBahasa').getValue(),
	    Language: Ext.get('txtNameSetBahasa').getValue()	
	};
    return params
};


function RefreshDataSetBahasa()
{	
	dsSetBahasaList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetBahasa, 
					Sort: 'language_id', 
					Sortdir: 'ASC', 
					target:'ViewSetupBahasa',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetBahasa = undefined;
	return dsSetBahasaList;
};

function RefreshDataSetBahasaFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetBahasaFilter').getValue() != '')
    { 
		//KataKunci = ' where LANGUAGE_id like ~%' + Ext.get('txtKDSetBahasaFilter').getValue() + '%~';
                KataKunci = ' language_id like ~%' + Ext.get('txtKDSetBahasaFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetBahasaFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where LANGUAGE like  ~%' + Ext.get('txtSetBahasaFilter').getValue() + '%~';
                        KataKunci = ' language like  ~%' + Ext.get('txtSetBahasaFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  LANGUAGE like  ~%' + Ext.get('txtSetBahasaFilter').getValue() + '%~';
                        KataKunci += ' and  language like  ~%' + Ext.get('txtSetBahasaFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetBahasaList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetBahasa, 
					//Sort: 'LANGUAGE_id',
                                        Sort: 'language_id',
					Sortdir: 'ASC', 
					target:'ViewSetupBahasa',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetBahasa();
	};
};



function mComboMaksDataSetBahasa()
{
  var cboMaksDataSetBahasa = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetBahasa',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetBahasa,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetBahasa=b.data.displayText ;
					RefreshDataSetBahasa();
				} 
			}
		}
	);
	return cboMaksDataSetBahasa;
};
 
