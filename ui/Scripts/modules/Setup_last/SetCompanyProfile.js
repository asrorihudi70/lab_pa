
var dsSetCompanyList;
var AddNewSetCompany;
var selectCountSetCompany=50;
var rowSelectedSetCompany;
var SetCompanyLookUps;
CurrentPage.page = getPanelSetCompany(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetCompany(mod_id) 
{
	
    var Field =['COMP_ID', 'COMP_NAME', 'COMP_ADDRESS', 'COMP_TELP', 'COMP_FAX', 'COMP_CITY', 'COMP_POS_CODE', 'COMP_EMAIL', 'LOGO'];
    dsSetCompanyList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetCompany();

    var grListSetCompany = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetCompany',
		    stripeRows: true,
		    store: dsSetCompanyList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetCompany=undefined;
							rowSelectedSetCompany = dsSetCompanyList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetCompany',
                        header: 'Id',                      
                        dataIndex: 'COMP_ID',
                        sortable: true,
                        width: 60
                    },
					{
					    id: 'colSetCompanyName',
					    header: 'Comp Name',					   
					    dataIndex: 'COMP_NAME',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanyAdd',
					    header: 'Address',					   
					    dataIndex: 'COMP_ADDRESS',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanyTlp',
					    header: 'Telp',					   
					    dataIndex: 'COMP_TELP',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanyfax',
					    header: 'Fax',					   
					    dataIndex: 'COMP_FAX',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanyCity',
					    header: 'City',					   
					    dataIndex: 'COMP_CITY',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanypos',
					    header: 'Pos Code',					   
					    dataIndex: 'COMP_POS_CODE',
					    width: 130,
					    sortable: true
					},
					{
					    id: 'colSetCompanyposem',
					    header: 'Email',					   
					    dataIndex: 'COMP_EMAIL',
					    width: 130,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetCompany',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetCompany != undefined)
						{
						    SetCompanyLookUp(rowSelectedSetCompany.data);
						}
						else
						{
						    SetCompanyLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetCompany = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Company',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupCompany',
		    items: [grListSetCompany],		   
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetCompany();
				}
			}
		}
	);
    //END var FormSetCompany--------------------------------------------------

	RefreshDataSetCompany();
    return FormSetCompany ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetCompanyLookUp(rowdata) 
{
	var lebar=600;
    SetCompanyLookUps = new Ext.Window   	
    (
		{
		    id: 'SetCompanyLookUps',
		    title: 'Company',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 310,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupCompany',
		    modal: true,
		    items: getFormEntrySetCompany(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetCompany=undefined;
					RefreshDataSetCompany();
				}
            }
		}
	);


    SetCompanyLookUps.show();
	if (rowdata == undefined)
	{
		SetCompanyAddNew();
	}
	else
	{
		SetCompanyInit(rowdata)
	}	
};


function getFormEntrySetCompany(lebar) 
{
    var pnlSetCompany = new Ext.FormPanel
    (
		{
		    id: 'PanelSetCompany',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 280,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupCompany',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 230,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetCompany',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Id ',
								    name: 'txtKode_SetCompany',
								    id: 'txtKode_SetCompany',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Name ',
								    name: 'txtNameSetCompany',
								    id: 'txtNameSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Address ',
								    name: 'txtAddressSetCompany',
								    id: 'txtAddressSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Phone ',
								    name: 'txtTlpSetCompany',
								    id: 'txtTlpSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Fax ',
								    name: 'txtFaxSetCompany',
								    id: 'txtFaxSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'City ',
								    name: 'txtCitySetCompany',
								    id: 'txtCitySetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Pos Code ',
								    name: 'txtPosSetCompany',
								    id: 'txtPosSetCompany',
								    anchor: '100%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Email ',
								    name: 'txtEmailSetCompany',
								    id: 'txtEmailSetCompany',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetCompany',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetCompanyAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetCompany',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetCompanySave(false);
						RefreshDataSetCompany();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetCompany',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetCompanySave(true);
						RefreshDataSetCompany();
						if (x===undefined)
						{
							SetCompanyLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetCompany',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetCompanyDelete() ;
							RefreshDataSetCompany();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetCompany',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950002);}
				}
			]
		}
	); 

    return pnlSetCompany
};


function SetCompanySave(mBol) 
{
	if (ValidasiEntrySetCompany(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetCompany == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetCompany(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetCompany(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetCompany();
							if(mBol === false)
							{
								Ext.get('txtKode_SetCompany').dom.value=cst.CompanyId;
							};
							AddNewSetCompany = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetCompany(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetCompany(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetCompany(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetCompany(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetCompany();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetCompany(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetCompany(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetCompanySave
///---------------------------------------------------------------------------------------///

function SetCompanyDelete() 
{
	if (ValidasiEntrySetCompany(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSatuan) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetCompany(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetCompany(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetCompany();
										SetCompanyAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetCompany(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetCompany(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetCompany(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetCompany').getValue() == '' || (Ext.get('txtNameSetCompany').getValue() == ''))
	{
		if (Ext.get('txtKode_SetCompany').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetCompany(nmGetValidasiKosong(nmKdSatuan),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetCompany').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetCompany(nmGetValidasiKosong(nmSatuan),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetCompany(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetCompany(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetCompany(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetCompanyInit(rowdata) 
{
    AddNewSetCompany = false;
    Ext.get('txtKode_SetCompany').dom.value = rowdata.COMP_ID;
    Ext.get('txtNameSetCompany').dom.value = rowdata.COMP_NAME;
	Ext.get('txtAddressSetCompany').dom.value = rowdata.COMP_ADDRESS;
	Ext.get('txtTlpSetCompany').dom.value = rowdata.COMP_TELP;
	Ext.get('txtFaxSetCompany').dom.value = rowdata.COMP_FAX;
	Ext.get('txtCitySetCompany').dom.value = rowdata.COMP_CITY;
	Ext.get('txtPosSetCompany').dom.value = rowdata.COMP_POS_CODE;
	Ext.get('txtEmailSetCompany').dom.value = rowdata.COMP_EMAIL;
};
///---------------------------------------------------------------------------------------///



function SetCompanyAddNew() 
{
    AddNewSetCompany = true;   
	Ext.get('txtKode_SetCompany').dom.value = '';
    Ext.get('txtNameSetCompany').dom.value = '';
	Ext.get('txtAddressSetCompany').dom.value = '';
	Ext.get('txtTlpSetCompany').dom.value = '';
	Ext.get('txtFaxSetCompany').dom.value = '';
	Ext.get('txtCitySetCompany').dom.value = '';
	Ext.get('txtPosSetCompany').dom.value = '';
	Ext.get('txtEmailSetCompany').dom.value = '';
	rowSelectedSetCompany   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetCompany() 
{
    var params =
	{	
		Table: 'ViewSetupCompany',   
	    CompId: Ext.get('txtKode_SetCompany').getValue(),
	    CompName: Ext.get('txtNameSetCompany').getValue(),
		CompAddress:Ext.get('txtAddressSetCompany').getValue(),
		CompTelp:Ext.get('txtTlpSetCompany').getValue(),
		CompFax:Ext.get('txtFaxSetCompany').getValue(),
		CompCity:Ext.get('txtCitySetCompany').getValue(),
		CompPos:Ext.get('txtPosSetCompany').getValue(),
		CompEmail:Ext.get('txtEmailSetCompany').getValue()
	};
    return params
};


function RefreshDataSetCompany()
{	
	dsSetCompanyList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetCompany, 
					Sort: 'Company_ID', 
					Sortdir: 'ASC', 
					target:'ViewSetupCompany',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetCompany = undefined;
	return dsSetCompanyList;
};

function RefreshDataSetCompanyFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetCompanyFilter').getValue() != '')
    { 
		KataKunci = ' where Company_ID like ~%' + Ext.get('txtKDSetCompanyFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetCompanyFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = ' where Company like  ~%' + Ext.get('txtSetCompanyFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  Company like  ~%' + Ext.get('txtSetCompanyFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetCompanyList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetCompany, 
					Sort: 'Company_ID', 
					Sortdir: 'ASC', 
					target:'ViewSetupCompany',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetCompany();
	};
};



function mComboMaksDataSetCompany()
{
  var cboMaksDataSetCompany = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetCompany',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetCompany,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetCompany=b.data.displayText ;
					RefreshDataSetCompany();
				} 
			}
		}
	);
	return cboMaksDataSetCompany;
};
 
