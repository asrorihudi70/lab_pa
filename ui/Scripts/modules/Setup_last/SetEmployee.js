var selectDeptSetEmployee;
var dsDeptSetupEmployee;

var dsSetEmployeeList;
var AddNewSetEmployee;
var selectCountSetEmployee=50;
var rowSelectedSetEmployee;
var SetEmployeeLookUps;
CurrentPage.page = getPanelSetEmployee(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEmployee(mod_id) 
{
	
    var Field = ['EMP_ID','EMP_NAME','EMP_STATE','EMP_PHONE1','EMP_PHONE2','IS_AKTIF','EMP_ADDRESS','EMP_CITY','EMP_POS_CODE','EMP_EMAIL','DEPT_NAME','DEPT_ID'];
    dsSetEmployeeList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetEmployee();
	
	 var chkAktifSetEmployeeView = new Ext.grid.CheckColumn
	(
		{
			id: 'chkAktifSetEmployeeView',
			header: nmEmpAktif,
			align: 'center',
			disabled:true,
			dataIndex: 'IS_AKTIF',
			width: 70
		}
	);

    var grListSetEmployee = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEmployee',
		    stripeRows: true,
		    store: dsSetEmployeeList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEmployee=undefined;
							rowSelectedSetEmployee = dsSetEmployeeList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetEmployee = dsSetEmployeeList.getAt(ridx);
					if (rowSelectedSetEmployee != undefined)
					{
						SetEmployeeLookUp(rowSelectedSetEmployee.data);
					}
					else
					{
						SetEmployeeLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetEmployee',
                        header: nmKdEmp2,                      
                        dataIndex: 'EMP_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetEmployee',
					    header: nmEmp,					   
					    dataIndex: 'EMP_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colSetEmployeeDept',
					    header: 'Department',					   
					    dataIndex: 'DEPT_NAME',
					    width: 150,
					    sortable: true
					},
					{
					    id: 'colSetAdressEmployee',
					    header: nmEmpAddress,					   
					    dataIndex: 'EMP_ADDRESS',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetCityEmployee',
					    header: nmEmpCity,					   
					    dataIndex: 'EMP_CITY',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'colSetPosCodeEmployee',
					    header: nmEmpPosCode,					   
					    dataIndex: 'EMP_POS_CODE',
					    width: 80,
					    sortable: true
					},
					{
					    id: 'colSetStateEmployee',
					    header: nmEmpState,					   
					    dataIndex: 'EMP_STATE',
					    width: 120,
					    sortable: true
					},
					{
					    id: 'colSetPhone1Employee',
					    header: nmEmpPhone1,					   
					    dataIndex: 'EMP_PHONE1',
					    width: 100,
					    sortable: true
					},
					{
					    id: 'colSetPhone2Employee',
					    header: nmEmpPhone2,					   
					    dataIndex: 'EMP_PHONE2',
					    width: 90,
					    sortable: true
					},
					{
					    id: 'colSetEmailEmployee',
					    header: nmEmpEmail,					   
					    dataIndex: 'EMP_EMAIL',
					    width: 100,
					    sortable: true
					},
					chkAktifSetEmployeeView
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetEmployeeList,
            pageSize: selectCountSetEmployee,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetEmployee',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetEmployee != undefined)
						{
						    SetEmployeeLookUp(rowSelectedSetEmployee.data);
						}
						else
						{
						    SetEmployeeLookUp();
						}
				    }
				},' ','-'
			]
		    //,viewConfig: { forceFit: true }
		}
	);


    var FormSetEmployee = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormEmp,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupEmployee',
		    items: [grListSetEmployee],
		    tbar:
			[
				nmKdEmp2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmKdEmp2 + ' : ',
					id: 'txtKDSetEmployeeFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmEmp + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmEmp + ' : ',
					id: 'txtSetEmployeeFilter',                   
					width:90,
					onInit: function() { }
				}, ' ','-',
				nmEmpAddress + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmEmpAddress + ' : ',
					id: 'txtSetEmployeeAddressFilter',                   
					width:90,
					onInit: function() { }
				}, 
				{
				    id: 'btnRefreshSetEmployee',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEmployeeFilter();
					}
				}
			]
		}
	);
    //END var FormSetEmployee--------------------------------------------------

	RefreshDataSetEmployee();
    return FormSetEmployee ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetEmployeeLookUp(rowdata) 
{
	var lebar=600;
    SetEmployeeLookUps = new Ext.Window   	
    (
		{
		    id: 'SetEmployeeLookUps',
		    title: nmFormEmp,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 320,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupEmployee',
		    modal: true,
		    items: getFormEntrySetEmployee(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetEmployee=undefined;
					RefreshDataSetEmployee();
				}
            }
		}
	);


    SetEmployeeLookUps.show();
	if (rowdata == undefined)
	{
		SetEmployeeAddNew();
	}
	else
	{
		SetEmployeeInit(rowdata)
	};
};


function getFormEntrySetEmployee(lebar) 
{
    var pnlSetEmployee = new Ext.FormPanel
    (
		{
		    id: 'PanelSetEmployee',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 390,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupEmployee',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 240,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetEmployee',
							labelWidth:75,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdEmp2 + ' ',
								    name: 'txtKode_SetEmployee',
								    id: 'txtKode_SetEmployee',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmEmp + ' ',
								    name: 'txtNameSetEmployee',
								    id: 'txtNameSetEmployee',
								    anchor: '100%'
								},
								mComboDeptSetEmployee(),
								{
								    xtype: 'textfield',
								    fieldLabel: nmEmpAddress + ' ',
								    name: 'txtAddressSetEmployee',
								    id: 'txtAddressSetEmployee',
								    anchor: '100%'
								},getItemPanelCitySetEmployee(),
								{
								    xtype: 'textfield',
								    fieldLabel: nmEmpState + ' ',
								    name: 'txtStateSetEmployee',
								    id: 'txtStateSetEmployee',
								    anchor: '69.3%'
								},getItemPanelPhoneSetEmployee(),getItemPanelEmailSetEmployee()
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetEmployee',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetEmployeeAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetEmployee',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetEmployeeSave(false);
						RefreshDataSetEmployee();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetEmployee',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetEmployeeSave(true);
						RefreshDataSetEmployee();
						if (x===undefined)
						{
							SetEmployeeLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetEmployee',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetEmployeeDelete() ;
							RefreshDataSetEmployee();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetEmployee',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950011);}
				}
			]
		}
	); 

    return pnlSetEmployee
};

function getItemPanelCitySetEmployee()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.672,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmEmpCity + ' ',
						name: 'txtCitySetEmployee',
						id: 'txtCitySetEmployee',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:75,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmEmpPosCode + ' ',
						name: 'txtPosCodeSetEmployee',
						id: 'txtPosCodeSetEmployee',						
						anchor: '100%'
					}	
				]
			}
		]
	}
	
	return items;
};



function getItemPanelPhoneSetEmployee()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmEmpPhone1 + ' ',
						name: 'txtPhone1SetEmployee',
						id: 'txtPhone1SetEmployee',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				labelWidth:75,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmEmpPhone2 + ' ',
						name: 'txtPhone2SetEmployee',
						id: 'txtPhone2SetEmployee',						
						anchor: '100%'
					}	
				]
			}
		]
	}
	
	return items;
};

function getItemPanelEmailSetEmployee()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.672,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmEmpEmail + ' ',
						name: 'txtEmailSetEmployee',
						id: 'txtEmailSetEmployee',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:75,
				border:false,
				items: 
				[ 	
					{
						xtype: 'checkbox',
						id: 'chkAktifSetEmployee',
						name:'chkAktifSetEmployee',
						style: 
						{
							'margin-top': '3.5px'
						},
						fieldLabel: nmEmpAktif + ' ',
						anchor: '100%'
					}
				]
			}
		]
	}
	
	return items;
};

function SetEmployeeSave(mBol) 
{
	if (ValidasiEntrySetEmployee(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetEmployee == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEmployee(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetEmployee(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetEmployee();
							if(mBol === false)
							{
								Ext.get('txtKode_SetEmployee').dom.readOnly=true;
							};
							AddNewSetEmployee = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetEmployee(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetEmployee(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetEmployee(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetEmployee(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetEmployee();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetEmployee(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetEmployee(nmPesanEditHapus,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetEmployeeSave
///---------------------------------------------------------------------------------------///

function SetEmployeeDelete() 
{
	if (ValidasiEntrySetEmployee(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetEmployee(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetEmployee(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetEmployee();
										SetEmployeeAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetEmployee(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetEmployee(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetEmployee(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetEmployee').getValue() == '' || Ext.get('txtNameSetEmployee').getValue() == '')
	{
		if (Ext.get('txtKode_SetEmployee').getValue() === '')
		{
			x=0;
			if (mBolHapus === false)
			{
				ShowPesanWarningSetEmployee(nmGetValidasiKosong(nmKdEmp2),modul);
			};
			
		}
		else if (Ext.get('txtNameSetEmployee').getValue() === '')
		{
			x=0;
			if (mBolHapus === false)
			{
				ShowPesanWarningSetEmployee(nmGetValidasiKosong(nmEmp),modul);
			};
		};
	};
	
	return x;
};

function ShowPesanWarningSetEmployee(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorSetEmployee(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoSetEmployee(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetEmployeeInit(rowdata) 
{
    AddNewSetEmployee = false;
    Ext.get('txtKode_SetEmployee').dom.value = rowdata.EMP_ID;
    Ext.get('txtNameSetEmployee').dom.value = rowdata.EMP_NAME;
	Ext.get('cboDeptSetEmployee').dom.value = rowdata.DEPT_NAME;
	selectDeptSetEmployee= rowdata.DEPT_ID;
	if (rowdata.EMP_ADDRESS === null)
	{
		Ext.get('txtAddressSetEmployee').dom.value = '';
	}
	else
	{
		Ext.get('txtAddressSetEmployee').dom.value = rowdata.EMP_ADDRESS;
	};
	
	if (rowdata.EMP_CITY === null)
	{
			Ext.get('txtCitySetEmployee').dom.value = '';
	}
	else
	{
			Ext.get('txtCitySetEmployee').dom.value = rowdata.EMP_CITY;
	};
	
	if (rowdata.EMP_POS_CODE === null)
	{
		Ext.get('txtPosCodeSetEmployee').dom.value = '';
	}
	else
	{
		Ext.get('txtPosCodeSetEmployee').dom.value = rowdata.EMP_POS_CODE;
	};
	
	if (rowdata.EMP_STATE === null)
	{
		Ext.get('txtStateSetEmployee').dom.value = '';
	}
	else
	{
		Ext.get('txtStateSetEmployee').dom.value = rowdata.EMP_STATE;
	};
	
	if (rowdata.EMP_PHONE1 === null)
	{
		Ext.get('txtPhone1SetEmployee').dom.value ='';
	}
	else
	{
		Ext.get('txtPhone1SetEmployee').dom.value = rowdata.EMP_PHONE1;
	};
	
	if (rowdata.EMP_PHONE2 === null)
	{
		Ext.get('txtPhone2SetEmployee').dom.value = '';
	}
	else
	{
		Ext.get('txtPhone2SetEmployee').dom.value = rowdata.EMP_PHONE2;
	};
	
	if (rowdata.EMP_EMAIL === null)
	{
		Ext.get('txtEmailSetEmployee').dom.value = '';
	}
	else
	{
		Ext.get('txtEmailSetEmployee').dom.value = rowdata.EMP_EMAIL;
	};

	Ext.get('chkAktifSetEmployee').dom.checked=rowdata.IS_AKTIF;
	
	Ext.get('txtKode_SetEmployee').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetEmployeeAddNew() 
{
    AddNewSetEmployee = true;   
	Ext.get('txtKode_SetEmployee').dom.value = '';
    Ext.get('txtNameSetEmployee').dom.value = '';
	Ext.get('txtAddressSetEmployee').dom.value = '';
	Ext.get('txtCitySetEmployee').dom.value = '';
	Ext.get('txtPosCodeSetEmployee').dom.value = '';
	Ext.get('txtStateSetEmployee').dom.value = '';
	Ext.get('txtPhone1SetEmployee').dom.value = '';
	Ext.get('txtPhone2SetEmployee').dom.value = '';
	Ext.get('txtEmailSetEmployee').dom.value = '';
	Ext.get('chkAktifSetEmployee').dom.checked=true;
	rowSelectedSetEmployee   = undefined;
	Ext.get('txtKode_SetEmployee').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetEmployee() 
{
    var params =
	{	
		Table: 'ViewSetupEmployee',   
	    EmpId: Ext.get('txtKode_SetEmployee').getValue(),
	    Emp: Ext.get('txtNameSetEmployee').getValue(),
		Address: Ext.get('txtAddressSetEmployee').getValue(),
		City: Ext.get('txtCitySetEmployee').getValue(),
		PosCode: Ext.get('txtPosCodeSetEmployee').getValue(),
		State: Ext.get('txtStateSetEmployee').getValue(),
		Phone1: Ext.get('txtPhone1SetEmployee').getValue(),
		Phone2: Ext.get('txtPhone2SetEmployee').getValue(),
		Email: Ext.get('txtEmailSetEmployee').getValue(),
		IsAktif: Ext.get('chkAktifSetEmployee').dom.checked,
		Dept_Id:selectDeptSetEmployee
	};
    return params
};


function RefreshDataSetEmployee()
{	
	dsSetEmployeeList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetEmployee, 
				//Sort: 'EMP_ID',
                                Sort: 'emp_id',
				Sortdir: 'ASC', 
				target:'ViewSetupEmployee',
				param: ''
			} 
		}
	);
	rowSelectedSetEmployee = undefined;
	return dsSetEmployeeList;
};

function RefreshDataSetEmployeeFilter() 
{   
	var KataKunci='';
	
	 if (Ext.get('txtKDSetEmployeeFilter').getValue() != '')
    { 
		KataKunci = '  emp_id like ~%' + Ext.get('txtKDSetEmployeeFilter').getValue() + '%~'; 
	};
	
    if (Ext.get('txtSetEmployeeFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  emp_name like  ~%' + Ext.get('txtSetEmployeeFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  emp_name like  ~%' + Ext.get('txtSetEmployeeFilter').getValue() + '%~';
		};  
	};
	
	 if (Ext.get('txtSetEmployeeAddressFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			KataKunci = '  emp_address like  ~%' + Ext.get('txtSetEmployeeAddressFilter').getValue() + '%~';
		}
		else
		{
			KataKunci += ' and  emp_address like  ~%' + Ext.get('txtSetEmployeeAddressFilter').getValue() + '%~';
		};  
	};
	
	    
    if (KataKunci != undefined) 
    {  
		dsSetEmployeeList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountSetEmployee, 
					//Sort: 'EMP_ID',
                                        Sort: 'emp_id',
					Sortdir: 'ASC', 
					target:'ViewSetupEmployee',
					param: KataKunci
				} 
			}
		);    
    }
	else
	{
		RefreshDataSetEmployee();
	};
};



function mComboMaksDataSetEmployee()
{
  var cboMaksDataSetEmployee = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetEmployee',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetEmployee,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetEmployee=b.data.displayText ;
					RefreshDataSetEmployee();
				} 
			}
		}
	);
	return cboMaksDataSetEmployee;
};
 

function mComboDeptSetEmployee()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDeptSetupEmployee = new WebApp.DataStore({ fields: Field });

	dsDeptSetupEmployee.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    //param: ' WHERE DEPT_ID <> ~xxx~'
                            param: ' dept_id <> ~xxx~'
			}
		}
	);
	
  var cboDeptSetEmployee = new Ext.form.ComboBox
	(
		{
			id:'cboDeptSetEmployee',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: 'Choose Department...',
			fieldLabel: 'Department ',			
			anchor:'100%',
			store: dsDeptSetupEmployee,
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptSetEmployee=b.data.DEPT_ID ;
				} 
			}
		}
	);
	
	dsDeptSetupEmployee.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    //param: ' WHERE DEPT_ID <> ~xxx~'
                            param: ' dept_id <> ~xxx~'
			}
		}
	);
	
	return cboDeptSetEmployee;
};