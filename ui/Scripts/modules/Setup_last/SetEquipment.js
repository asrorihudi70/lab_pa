var CurrentListAdditonalFieldSetEquipment =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentAttachSetEquipment =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordAttcahSetEquipment = Ext.data.Record.create
	(
		[
		   {name: 'REFF_ID', mapping:'REFF_ID'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'PATH_IMAGE', mapping:'PATH_IMAGE'},
		   {name: 'ASSET_MAINT_ID', mapping:'ASSET_MAINT_ID'},
		   {name: 'TITLE', mapping:'TITLE'},
		   {name: 'EXT', mapping:'EXT'},
		   {name: 'ROW_REFF', mapping:'ROW_REFF'},
		   {name: 'PATH', mapping:'PATH'}
		]
	);

var selectStatusAsset;
var dsStatusAsset;
var dsSetEquipmentList;
var AddNewSetEquipment;
var selectCountSetEquipment=50;
var rowSelectedSetEquipment;
var SetEquipmentLookUps;
var selectCategorySetEquipment;
var selectCompanySetEquipment;
var selectCatSchPMSetEquipment;

var selectLocationSetEquipment;
var selectDeptSetEquipment;
var cellSelectAdditonalFieldSetEquipment;
var cellSelectedAttachSetEquipment;

var dsCategorySetupAsset;
var dsCompanySetupAsset;
var dsDeptSetupAsset;
var dsLocationSetupAsset;
var dsAdditonalFieldSetEquipment;

var selectCategorySetEquipmentView=' 9999';
var IdCategorySetEquipmentView=' 9999';
var valueCategorySetEquipmentView=' All';

var dsCategorySetupAssetView;
var dsAttachSetEquipment;
var EditorAttachSetEquipment;
var strPathFileImageSetEquipment;
var strRecordRefDocSetAsset;
var StrTreeComboSetEquipmentLocal;
var rootTreeSetEquipmentView;
var treeSetEquipmentView;
var rootTreeSetEquipmentEntry;
var treeSetEquipmentEntry;

CurrentPage.page = getPanelSetEquipment(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEquipment(mod_id) 
{
	
    var Field = ['ASSET_MAINT_ID','CATEGORY_ID','COMP_ID','LOCATION_ID','DEPT_ID','ASSET_MAINT_NAME','YEARS','DESCRIPTION_ASSET','ASSET_ID','CATEGORY_NAME','DEPT_NAME','LOCATION','COMP_NAME','PATH_IMAGE','STATUS_ASSET_ID','STATUS_ASSET'];
    dsSetEquipmentList = new WebApp.DataStore({ fields: Field });
	
	GetStrTreeComboSetEquipmentLocal();
	RefreshDataSetEquipment();
    var grListSetEquipment = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEquipment',
		    stripeRows: true,
		    store: dsSetEquipmentList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEquipment=undefined;
							rowSelectedSetEquipment = dsSetEquipmentList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetEquipment = dsSetEquipmentList.getAt(ridx);
					if (rowSelectedSetEquipment != undefined)
					{
						SetEquipmentLookUp(rowSelectedSetEquipment.data);
					}
					else
					{
						SetEquipmentLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
					{
					    id: 'colSetEquipmentCode',
					    header: nmKdAsetSetupAset,
					    dataIndex: 'ASSET_MAINT_ID',
					    width: 200,
					    sortable: true
					},
					{
					    id: 'colSetEquipmentName',
					    header: nmNamaAsetSetupAset,
					    dataIndex: 'ASSET_MAINT_NAME',
					    width: 200,
					    sortable: true
					},
                    {
                        id: 'colKodeSetEquipmentCatFK',
                        header: nmCatSetupAset,                      
                        dataIndex: 'CATEGORY_NAME',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetEquipmentzDept',
					    header: nmDeptSetupAset,					   
					    dataIndex: 'DEPT_NAME',
					    width: 200,
					    sortable: true
					},
					
					{
					    id: 'colSetYearsEquipment',
					    header: nmYearSetupAset,					   
					    dataIndex: 'YEARS',
					    width: 200,
					    sortable: true
					},					
					{
					    id: 'colSetDescriptionEquipment',
					    header: nmDescSetupAset,					   
					    dataIndex: 'DESCRIPTION_ASSET',
					    width: 300,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetEquipment',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetEquipment != undefined)
						{
						    SetEquipmentLookUp(rowSelectedSetEquipment.data);
						}
						else
						{
						    SetEquipmentLookUp();
						}
				    }
				},' ','-'
			]
		}
	);


    var FormSetEquipment = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormSetupAset,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupEquipment',
		    items: [grListSetEquipment],
		    tbar:
			[
				nmCatSetupAset + ' : ', ' ',
				mComboCategoryEqpSetEquipmentView()
				, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetEquipment(),
				' ','-',
				{
				    id: 'btnRefreshSetEquipment',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetEquipmentFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					
				}
			}
		}
	);
    
	RefreshDataSetEquipment();
    return FormSetEquipment ;
};
///------------------------------------------------------------------------------------------------------------///




function SetEquipmentLookUp(rowdata) 
{
	var lebar=900;
	GetStrTreeComboSetEquipmentLocal();
    SetEquipmentLookUps = new Ext.Window   	
    (
		{
		    id: 'SetEquipmentLookUps',
		    title: nmTitleFormSetupAset,
		    closeAction: 'destroy',
		    width: lebar,
		    height:530,//510,//375,//324,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupEquipment',
		    modal: true,
		    items: getFormEntrySetEquipment(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetEquipment=undefined;
					var strKriteria='';
					if (selectCategorySetEquipmentView.id != undefined)
					{
						if(selectCategorySetEquipmentView.id != ' 9999')
						{
							if (selectCategorySetEquipmentView.leaf === false)
							{
								//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id +'~'
							}
							else
							{
								//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
							};
						};
					};
					
					RefreshDataSetEquipment(strKriteria);
				}
            }
		}
	);


    SetEquipmentLookUps.show();
	if (rowdata == undefined)
	{
		SetEquipmentAddNew();
	}
	else
	{
		SetEquipmentInit(rowdata)
	}	
};


function getFormEntrySetEquipment(lebar) 
{
    var pnlSetEquipment = new Ext.FormPanel
    (
		{
		    id: 'PanelSetEquipment',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 500, //374,
			width:lebar,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 10px 0px 10px',
		    iconCls: 'SetupEquipment',
		    border: true,
		    items:
			[getItemPanelInputSetEquipment(lebar)],
		    tbar:
			[
				{
				    id: 'btnAddSetEquipment',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetEquipmentAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetEquipment',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetEquipmentSave(false);
						var strKriteria='';
						if (selectCategorySetEquipmentView.id != undefined)
						{
							if(selectCategorySetEquipmentView.id != ' 9999')
							{
								if (selectCategorySetEquipmentView.leaf === false)
								{
									//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                        strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ' +  selectCategorySetEquipmentView.id + '`'
								}
								else
								{
									//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                        strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
								};
							};
						};
						
						RefreshDataSetEquipment(strKriteria);
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetEquipment',
				    text: nmSimpanKeluar,
				    tooltip:  nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetEquipmentSave(true);
						var strKriteria='';
						if (selectCategorySetEquipmentView.id != undefined)
						{
							if(selectCategorySetEquipmentView.id != ' 9999')
							{
								if (selectCategorySetEquipmentView.leaf === false)
								{
									//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                        strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id +'~'
								}
								else
								{
									//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                        strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
								};
							};
						};
							
							RefreshDataSetEquipment(strKriteria);
						if (x===undefined)
						{
							SetEquipmentLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetEquipment',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
						SetEquipmentDelete() ;
						var strKriteria='';
						if (selectCategorySetEquipmentView.id != undefined)
						{
							if(selectCategorySetEquipmentView.id != ' 9999')
							{
								if (selectCategorySetEquipmentView.leaf === false)
								{
									//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                        strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id +'~'
								}
								else
								{
									//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                        strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
								};
							};
						};
						
						RefreshDataSetEquipment(strKriteria);			
					}
				},'-','->','-',
				{
					id:'btnPrintSetEquipment',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950009);}
				}
			]
		}
	); 

    return pnlSetEquipment
};
function getItemPanelInputSetEquipment(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
		border:false,
		height:734,
	    items:
		[
			{
			    columnWidth: 1,
			    width: lebar-35,
				labelWidth:120,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
						xtype:'fieldset',
						title: nmTitleGenInfoSetupAset,
						layout:'column',
						anchor:  '99.99%',
						height: '200px',
						items :
						[
							{
								columnWidth: .77,
								labelWidth:120,
								layout: 'form',
								height:'183px',
								border: false,
								items:
								[	
									
									{
										xtype: 'textfield',
										fieldLabel: nmKdAsetSetupAset + ' ',
										name: 'txtKode_SetEquipment',
										id: 'txtKode_SetEquipment',
										anchor: '50%'
									},
									{
										xtype: 'textfield',
										fieldLabel: nmNamaAsetSetupAset + ' ',
										name: 'txtNameSetEquipment',
										id: 'txtNameSetEquipment',
										anchor: '97.2%'
									}, getItemPanelCategorySetEquipment(lebar),
									getItemPanelDeptLocSetEquipment(lebar),
									getItemPanelYearsSetEquipment(lebar),
									{
										xtype: 'textarea',
										fieldLabel: nmDescSetupAset + ' ',
										name: 'txtDescriptionSetEquipment',
										id: 'txtDescriptionSetEquipment',
										scroll:true,
										//anchor: '97.2% 32%'
										anchor: '97.2% 18%'
									}
									,mComboStatusAsset()
								]
							},
							{
								columnWidth:.23,
								layout: 'form',
								border: false,
								height:201,//193,
								bodyStyle: 'padding:0px 0px 0px 0px',
								items: 
								[
									{
										layout:'form',
										border: true,
										bodyStyle: 'padding:5px 5px 5px 5px',
										height:168,
										items:
										{
											xtype:'label', 
											id:'gbr',
											border:true,
											frame:true,
											html:'<div id="images" style="margin-left:3px;margin-top:3px;width:174px;height:150px;" align="center"> </div>'						
										}
									},getItemPanelAttachSetEquipment()
								]
							}
						]
					},getTabSpecAttachSetEquipment(),
					{
						layout: 'hBox',
						width:lebar-34,
						style: 
						{
							'margin-top': '0px' //'-3px'
						},
						height:'36px',
						border: false,
						bodyStyle: 'padding:0px 0px 0px 0px',
						defaults: { margins: '0 0 0 0' },
						anchor: '100%',
						layoutConfig: 
						{
							padding:'5',
							align:'middle'
						},
						defaults:{margins:'0 5 0 0'},
						items:
						[
							{
								xtype:'button',
								text:nmBtnMetering,
								width:80,
								id: 'btnMeteringSetupEquipment',
								handler:function()
								{
									SetMeteringAssetLookUp(selectCategorySetEquipment,Ext.get('txtKode_SetEquipment').getValue(),Ext.get('txtNameSetEquipment').getValue());
								}
							},
							{
								xtype:'button',
								text:nmBtnLog,
								width:80,
								id: 'btnLogMetering',
								handler:function()
								{
									InputLogMeteringLookUp(Ext.get('txtKode_SetEquipment').getValue(),Ext.get('txtNameSetEquipment').getValue());
								}
							},
							{
								xtype:'button',
								text:nmBtnHistoryLog,
								width:80,
								id: 'btnHistInputLogMetering',
								handler:function()
								{
									var criteria;
									//criteria='WHERE ASSET_MAINT_ID=~' + Ext.get('txtKode_SetEquipment').getValue() + '~'
                                                                        criteria='asset_maint_id = ~' + Ext.get('txtKode_SetEquipment').getValue() + '~'
									FormLookupHistoryInputLog(criteria,Ext.get('txtNameSetEquipment').dom.value);
								}
							},
							{
								xtype:'spacer',
								flex:1
							},
							{
								xtype:'button',
								text:nmBtnHistory,
								width:80,
								id: 'btnHistorySetupEquipment',
								margins:'0',
								handler:function()
								{
									var criteria;
									//criteria='WHERE ASSET_MAINT_ID=~' + Ext.get('txtKode_SetEquipment').getValue() + '~'
                                                                        criteria='asset_maint_id = ~' + Ext.get('txtKode_SetEquipment').getValue() + '~'
									FormLookupHistoryAsset(criteria,Ext.get('txtNameSetEquipment').dom.value);
								}
							}
						]			
					}
				]
			}
		]
	};
    return items;
};


function getTabSpecAttachSetEquipment()
{
	var vTabTabSpecAttachSetEquipment = new Ext.TabPanel
	(
		{
		    id: 'vTabTabSpecAttachSetEquipment',
		    region: 'center',
		    margins: '7 7 7 7',
			style:
			{
			   'margin-top': '5px'
			},
		    bodyStyle: 'padding:7px 7px 7px 7px',
		    activeTab: 0,
		    plain: true,
		    defferedRender: false,
		    frame: false,
		    border: true,
		    height: 180,//170,
			width:742,
		    anchor: '100%',
		    items:
			[
			    {
					 title: nmTitleTabSpecSetupAset,
					 id: 'tabSpesificSetEquipment',
					 height: 180,
					 items:
					 [
						GetDTLAdditonalFieldSetEquipment() 
					 ],
			        listeners:
					{
						  activate: function() 
						  {
							
						  }
					}
				},
				{
					 title: nmTitleTabRefSetupAset,
					 id: 'tabAttachmentSetEquipment',
					 height: 180,
					 border:true,
					 items:
					 [
						GetGridAttachSetEquipment()
					 ],
					  tbar: 
					 [
						{
							id:'btnTambahBrsAttachSetEquipment',
							text: nmTambahBaris,
							tooltip: nmTambahBaris,
							iconCls: 'AddRow',
							handler: function() 
							{ 
								//TambahBarisAttcahSetEquipment()
								var p = GetRecordBaruAttcahSetEquipment();
								Lookup_vuploadAset(1,'Doc Asset',dsAttachSetEquipment,p);
							}
						},'-',
						{
							id:'btnHpsBrsAttachSetEquipment',
							text: nmHapusBaris,
							tooltip: nmHapusBaris,
							iconCls: 'RemoveRow',
							handler: function()
							{
								if (dsAttachSetEquipment.getCount() > 0 )
								{
									if (cellSelectedAttachSetEquipment != undefined)
									{
										if(CurrentAttachSetEquipment != undefined)
										{
											HapusBarisAttachSetEquipment();
										}
									}
									else
									{
										ShowPesanWarningSetEquipment(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									}
								}
							}
						},' ','-'
					 ],
			        listeners:
					{
						  activate: function() 
						  {
								
						  }
					}
				}
			]
		}
	);
	
	return vTabTabSpecAttachSetEquipment;
};

function LoadModuleDocSetAsset(strPath,strTitle)
{
	if(strPath != undefined && strPath != '' && strPath != null && strPath != 'null')
	{
		var str = strPath
		window.open(str + '?' + strTitle, '_blank', 'location=0,resizable=1', false);
	}
	else
	{
		ShowPesanWarningSetEquipment(nmKonfirmasiPathUploadFile,nmTitleFormSetupAset);
	};
};

function GetGridAttachSetEquipment()
{

	  var Field = ['REFF_ID','DESCRIPTION','PATH_IMAGE','ASSET_MAINT_ID','TITLE','EXT','ROW_REFF','PATH','strTitle','strDesc','strDir']
    dsAttachSetEquipment = new WebApp.DataStore({ fields: Field });
	
	var grdAttachSetEquipment = new Ext.grid.EditorGridPanel
	(
		{
			region:'center',
			store: dsAttachSetEquipment,
			columnLines: true,
			stripRows:true,
			//plugins:[EditorAttachSetEquipment],
			height:111,
			autoScroll:true,
			border:false,
			cm: new Ext.grid.ColumnModel
			(
				[
					{
					   id: 'colImageRef',
					   header: "",
					   dataIndex: 'PATH_IMAGE',
					   width: 96,
					   renderer: function(value, p, record) 
					    {
							var str = '';
							if (value != undefined) 
							{
								str = '<img src="' + value + '" style="margin-left:35px;margin-top:15px;width:16px;height:16px;" align="center"/>'	
							};
					    return str;
						
						}
					},
					{
					   id: 'colDescRef',
					   header: nmDescSetupAset,
					   dataIndex: 'DESCRIPTION',
					   width: 700,
					   // editor: new Ext.form.TextField
					   // (
							// {
								// id:'fieldcolDescRef',
								// allowBlank: false,
								// enableKeyEvents : true,
								// listeners: 
								// { 
									// 'specialkey' : function()
									// {
										// if (Ext.EventObject.getKey() === 13) 
										// {
											// Lookup_vuploadAset(1,'Doc Asset')
										// }
									// }
								// }
							// }
					   // )//,
					   renderer:function(value, p, record)
						{
							var str='';
							if (record != undefined)
							{
								//str = "<p><div style='white-space:normal;font:bold'>" + record.data.TITLE + "</div></p>";
								// str = "<p><a href='#' onclick=" + window.open('viewPdf.aspx?title=' + record.data.TITLE + '&pdf=./Doc Asset/' + record.data.TITLE , '_blank', 'location=0,resizable=1', false) + "><div style='white-space:normal;font:bold'>" + record.data.TITLE + "</a></div></p>";
									str = '<p><a href="#" onclick="LoadModuleDocSetAsset(\'' + record.data.PATH + '\',\'' + record.data.TITLE + '\')"><div style="white-space:normal;font:bold;text-decoration:underline">Title : ' + record.data.TITLE + '</a></div></p>';
					str += "<p><div style='white-space:normal;padding:2px 2px 2px 2px'> Description : " + record.data.DESCRIPTION + "</div></p>";
							};
							return str;
						}
					}
				]
			),
			listeners:
			{
				celldblclick: function (sm, ridx, cidx, e)
				{
					var p = GetRecordBaruAttcahSetEquipment();
					p.data.ASSET_MAINT_ID= sm.store.data.items[ridx].data.ASSET_MAINT_ID
					p.data.DESCRIPTION=sm.store.data.items[ridx].data.DESCRIPTION
					p.data.TITLE=sm.store.data.items[ridx].data.TITLE
					p.data.PATH=sm.store.data.items[ridx].data.PATH
					p.data.PATH_IMAGE=sm.store.data.items[ridx].data.PATH_IMAGE
					p.data.REFF_ID=sm.store.data.items[ridx].data.REFF_ID
					p.data.EXT=sm.store.data.items[ridx].data.EXT
					p.data.ROW_REFF=sm.store.data.items[ridx].data.ROW_REFF
					Lookup_vuploadAset(1,'Doc Asset',dsAttachSetEquipment,p,ridx);
				}
			},
			sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
					listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectedAttachSetEquipment = dsAttachSetEquipment.getAt(row);
					        CurrentAttachSetEquipment.row = row;
					        CurrentAttachSetEquipment.data = cellSelectedAttachSetEquipment;
					    }
					}
				}
			), viewConfig: { forceFit: true }
        }
	)
	
	return grdAttachSetEquipment;
};

function HapusBarisAttachSetEquipment()
{
	if ( cellSelectedAttachSetEquipment != undefined )
	{
		if (cellSelectedAttachSetEquipment.data.REFF_ID != '' )
		{
			Ext.Msg.show
			(
				{
				   title:nmHapusBaris,
				   msg: nmKonfirmasiHapusBaris + ' ' + nmBaris + ' : ' + (CurrentAttachSetEquipment.row + 1) + ' ' + nmOperatorDengan + ' ' + nmTitleDocSetupAset + ' : ' + cellSelectedAttachSetEquipment.data.TITLE,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {			
					   if (btn =='yes') 
						{
							if(dsAttachSetEquipment.data.items[CurrentAttachSetEquipment.row].data.ROW_REFF === '')
							{
								dsAttachSetEquipment.removeAt(CurrentAttachSetEquipment.row);
							}
							else
							{
								Ext.Msg.show
								(
									{
									   title:nmHapusBaris,
									   msg: nmGetValidasiHapusBarisDatabase() ,
									   buttons: Ext.MessageBox.YESNO,
									   fn: function (btn) 
									   {			
											if (btn =='yes') 
											{
												AttachSetEquipmentDeleteDetail();
											};
										}
									}
								)
							};	
						}; 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsAttachSetEquipment.removeAt(CurrentAttachSetEquipment.row);
		};
	}
};

function AttachSetEquipmentDeleteDetail()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData,
			params:  getParamAttachSetEquipmentDeleteDetail(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoSetEquipment(nmPesanHapusSukses,nmHeaderHapusData);
					dsAttachSetEquipment.removeAt(CurrentAttachSetEquipment.row);
					cellSelectedAttachSetEquipment=undefined;
					RefreshDataAttachSetEquipmentDetail(Ext.get('txtKode_SetEquipment').dom.value);
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningSetEquipment(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanWarningSetEquipment(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function getParamAttachSetEquipmentDeleteDetail()
{
	var params =
	{	
		Table: 'viewSetupAsset',   
	    AssetId: CurrentAttachSetEquipment.data.data.ASSET_MAINT_ID,
		ReffId: CurrentAttachSetEquipment.data.data.REFF_ID,
		RowReff: CurrentAttachSetEquipment.data.data.ROW_REFF,
		Hapus:2
	};
	
    return params
};

function RefreshDataAttachSetEquipmentDetail(str)
{	
	dsAttachSetEquipment.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'TITLE',
                                Sort: 'title',
				Sortdir: 'ASC', 
				target:'ViewSetupAssetRef',
				//param: 'where asset_maint_id=~' + str + '~'
                                param: 'asset_maint_id = ~' + str + '~'
			} 
		}
	);
	cellSelectedAttachSetEquipment = undefined;
	return dsAttachSetEquipment;
};

function TambahBarisAttcahSetEquipment()
{
	var p = GetRecordBaruAttcahSetEquipment();
	 //EditorAttachSetEquipment.stopEditing();
      dsAttachSetEquipment.insert(dsAttachSetEquipment.getCount(), p);
       //EditorAttachSetEquipment.startEditing(dsAttachSetEquipment.getCount()-1);

	
};

function GetRecordBaruAttcahSetEquipment()
{
	var p = new mRecordAttcahSetEquipment
	(
		{
			REFF_ID:'',
			DESCRIPTION :'',
		    PATH_IMAGE :'',
			ASSET_MAINT_ID : Ext.get('txtKode_SetEquipment').dom.value,
			TITLE :'',
			EXT:'',
			ROW_REFF:'',
			PATH:''
		}
	);
	return p;
};

function getItemPanelAttachSetEquipment()
{
	var items= 			
	{
		layout: 'hBox',
		width:100,
		border: false,
		bodyStyle: 'padding:0px 0px 5px 0px',
		defaults: { margins: '3 3 3 0' },
		anchor: '90%',
		layoutConfig: 
		{
			align: 'middle',
			pack:'start'
		},
		items:
		[
			{
				xtype:'button',
				text:nmBtnBrowse,
				width:70,
				style:{'margin-left':'0px','margin-top':'0px'},
				hideLabel:true,
				id: 'btnBrowseSetEquipment',
				handler:function()
				{
					Lookup_vupload(1,'Img Asset');
				}
			},
			{
				xtype:'button',
				text:nmBtnRemoveImage,
				width:70,
				style:{'margin-left':'0px','margin-top':'0px'},
				hideLabel:true,
				id: 'btnClearSetEquipment',
				handler:function()
				{
					strPathFileImageSetEquipment='';
					Dataview_fotoAsset('');
				}
			}
		]		
	}
	
	return items;
};


function getItemPanelCategorySetEquipment(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-200,
		items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:120,
				items: 
				[ 	
					mComboCategoryEqpSetEquipment()
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				labelWidth:80,
				border:false,
				items: 
				[ 	
					mComboCompanyEqpSetEquipment()
				]
			}
		]
	}
	
	return items;
};

function getItemPanelDeptLocSetEquipment(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-200,
		items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:120,
				items: 
				[ 	
					mComboDeptEqpSetEquipment()
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				labelWidth:80,
				border:false,
				items: 
				[ 	
					mComboLocationEqpSetEquipment()
				]
			}
		]
	}
	
	return items;
};

function GetDTLAdditonalFieldSetEquipment() 
{
    var fldDetail = ['CATEGORY_ID', 'ROW_ADD', 'ADDFIELD', 'TYPE_FIELD', 'VALUE', 'ASSET_MAINT_ID'];
	
    dsAdditonalFieldSetEquipment = new WebApp.DataStore({ fields: fldDetail })

    var gridAdditonalFieldSetEquipment = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsAdditonalFieldSetEquipment,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100%',
			height:140,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectAdditonalFieldSetEquipment = dsAdditonalFieldSetEquipment.getAt(row);
					        CurrentListAdditonalFieldSetEquipment.row = row;
					        CurrentListAdditonalFieldSetEquipment.data = cellSelectAdditonalFieldSetEquipment;
					    }
					}
				}
			),
		    cm: AdditionalFieldSetEquipmentColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridAdditonalFieldSetEquipment;
};

function AdditionalFieldSetEquipmentColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colAdditionalFieldSetEquipment',
				header: nmAttributSetupAset,
				dataIndex: 'ADDFIELD',
				width:200
			},
			{
				id: 'colFieldValueSetEquipment',
				header: nmValueSetupAset,
				dataIndex: 'VALUE',
				width:200	,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolValue',
						allowBlank: true,
						enableKeyEvents : true,
						width:30
					} 
				),
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
                }
			}
		]
	)
};

function mComboCategoryEqpSetEquipment()
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySetupAsset = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboSetupAsetEntry = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSetEquipmentEntry = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeSetEquipmentEntry  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						children: StrTreeComboSetEquipmentLocal,
						autoScroll: true
					}
				) 
				treeSetEquipmentEntry.setRootNode(rootTreeSetEquipmentEntry); 
				
				this.list = treeSetEquipmentEntry
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSetupAsetEntry.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSetEquipmentEntry
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.leaf === true)
				{
					this.setRawValue(node.attributes.text);
					selectCategorySetEquipment = node.attributes.id;
					var strID='';
					if (Ext.get('txtKode_SetEquipment').getValue() !='' && Ext.get('txtKode_SetEquipment').getValue() !=undefined)
					{
						strID=Ext.get('txtKode_SetEquipment').getValue();
					};
					RefreshDataAdditionalFieldBlank(selectCategorySetEquipment,'');
				}
				else
				{
					selectCategorySetEquipment=undefined;
					this.setRawValue('');
					ShowPesanWarningSetEquipment(nmWarningDtlCat,nmTitleFormSetupAset)
				};
			
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySetupAsset
		}
	);
	
	
	var cboCategoryEqpSetEquipment = new Ext.TreeComboSetupAsetEntry
	(
		{
			id:'cboCategoryEqpSetEquipment',
			fieldLabel: nmCatSetupAset + ' ',
			emptyText: nmPilihCategory,
			anchor:'100%'
		}
	);
	
	return cboCategoryEqpSetEquipment;
	
};

function mComboCompanyEqpSetEquipment()
{
	var Field = ['COMP_ID', 'COMP_NAME'];
	dsCompanySetupAsset = new WebApp.DataStore({ fields: Field });

	dsCompanySetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'COMP_NAME',
                            Sort: 'comp_name',
			    Sortdir: 'ASC',
			    target: 'viComboCompany',
			    param: ''
			}
		}
	);
	
  var cboCompanyEqpSetEquipment = new Ext.form.ComboBox
	(
		{
			id:'cboCompanyEqpSetEquipment',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihCompany,
			fieldLabel: nmCompanySetupAset + ' ',			
			anchor:'80%',
			store: dsCompanySetupAsset,
			valueField: 'COMP_ID',
			displayField: 'COMP_NAME',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCompanySetEquipment=b.data.COMP_ID ;
				} 
			}
		}
	);
	
	dsCompanySetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'COMP_NAME',
                            Sort: 'comp_name',
			    Sortdir: 'ASC',
			    target: 'viComboCompany',
			    param: ''
			}
		}
	);
	
	return cboCompanyEqpSetEquipment;
};



function mComboDeptEqpSetEquipment()
{
	var Field = ['DEPT_ID', 'DEPT_NAME'];
	dsDeptSetupAsset = new WebApp.DataStore({ fields: Field });

	dsDeptSetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    //param: ' WHERE DEPT_ID <> ~xxx~'
                            param: 'dept_id <> ~xxx~'
			}
		}
	);
	
  var cboDeptEqpSetEquipment = new Ext.form.ComboBox
	(
		{
			id:'cboDeptEqpSetEquipment',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihDept,
			fieldLabel: nmDeptSetupAset + ' ',			
			anchor:'100%',
			store: dsDeptSetupAsset,
			valueField: 'DEPT_ID',
			displayField: 'DEPT_NAME',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectDeptSetEquipment=b.data.DEPT_ID ;
				} 
			}
		}
	);
	
	dsDeptSetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_NAME',
                            Sort: 'dept_name',
			    Sortdir: 'ASC',
			    target: 'ComboDepartment',
			    //param: ' WHERE DEPT_ID <> ~xxx~'
                            param: 'dept_id <> ~xxx~'
			}
		}
	);
	
	return cboDeptEqpSetEquipment;
};

function mComboLocationEqpSetEquipment()
{
	var Field = ['LOCATION_ID', 'LOCATION'];
	dsLocationSetupAsset = new WebApp.DataStore({ fields: Field });

	dsLocationSetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'LOCATION',
                            Sort: 'location',
			    Sortdir: 'ASC',
			    target: 'viComboLocation',
			    //param: ' WHERE LOCATION_ID<>~xxx~'
                            param: 'location_id <> ~xxx~'
			}
		}
	);
	
  var cboLocationEqpSetEquipment = new Ext.form.ComboBox
	(
		{
			id:'cboLocationEqpSetEquipment',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihLocation,
			fieldLabel: nmLocationSetupAset + ' ',			
			anchor:'80%',
			store: dsLocationSetupAsset,
			valueField: 'LOCATION_ID',
			displayField: 'LOCATION',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectLocationSetEquipment=b.data.LOCATION_ID ;
				} 
			}
		}
	);
	
	dsLocationSetupAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'LOCATION',
                            Sort: 'location',
			    Sortdir: 'ASC',
			    target: 'viComboLocation',
			     //param: ' WHERE LOCATION_ID<>~xxx~'
                             param: 'location_id <> ~xxx~'
			}
		}
	);
	
	return cboLocationEqpSetEquipment;
};

function getItemPanelYearsSetEquipment(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-200,
		items:
		[
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:120,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmYearSetupAset + ' ',
						name: 'txtYearsSetEquipment',
						id: 'txtYearsSetEquipment',						
						anchor: '100%'
					}	
				]
			},
			{
				columnWidth:.6,
				layout: 'form',
				labelWidth:100,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmAsetCodeSetupAset + ' ',
						name: 'txtAssetCodeSetEquipment',
						id: 'txtAssetCodeSetEquipment',						
						anchor: '100%'
					}	
				]
			}
		]
	}
	
	return items;
};


function SetEquipmentSave(mBol) 
{
	if (ValidasiEntrySetEquipment(nmHeaderSimpanData) == 1 )
	{
		if (AddNewSetEquipment == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEquipment(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetEquipment(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							var strKriteria='';
							if (selectCategorySetEquipmentView.id != undefined)
							{
								if(selectCategorySetEquipmentView.id != ' 9999')
								{
									if (selectCategorySetEquipmentView.leaf === false)
									{
										//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                                strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id + '~'
									}
									else
									{
										//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                                strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
									};
								};
							};
							
							RefreshDataSetEquipment(strKriteria);

							
							if(mBol === false)
							{
								Ext.get('txtKode_SetEquipment').dom.readOnly=true;
								RefreshDataAttachSetEquipmentDetail(Ext.get('txtKode_SetEquipment').dom.value);
							};
							AddNewSetEquipment = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetEquipment(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetEquipment(nmPesanSimpanError,nmHeaderSimpanData);
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEquipment(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetEquipment(nmPesanEditSukses,nmHeaderEditData);
							var strKriteria='';
							if (selectCategorySetEquipmentView.id != undefined)
							{
								if(selectCategorySetEquipmentView.id != ' 9999')
								{
									if (selectCategorySetEquipmentView.leaf === false)
									{
										//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                                strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id  + '~'
									}
									else
									{
										//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                                strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
									};
								};
							};
							
							RefreshDataSetEquipment(strKriteria);
							
							if(mBol === false)
							{
								RefreshDataAttachSetEquipmentDetail(Ext.get('txtKode_SetEquipment').dom.value);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetEquipment(nmPesanEditGagal,nmHeaderEditData);
						}
						else {
							ShowPesanErrorSetEquipment(nmPesanEditError,nmHeaderEditData);
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetEquipmentSave
///---------------------------------------------------------------------------------------///

function SetEquipmentDelete() 
{
	if (ValidasiEntrySetEquipment(nmHeaderHapusData) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlDeleteData,
				params: getParamSetEquipment(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetEquipment(nmPesanHapusSukses,nmHeaderHapusData);
						var strKriteria='';
							if (selectCategorySetEquipmentView.id != undefined)
							{
								if(selectCategorySetEquipmentView.id != ' 9999')
								{
									if (selectCategorySetEquipmentView.leaf === false)
									{
										//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                                strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id +'~'
									}
									else
									{
										//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                                strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
									};
								};
							};
							
						RefreshDataSetEquipment(strKriteria);
						
						SetEquipmentAddNew();
					}
					else if (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningSetEquipment(nmPesanHapusGagal,nmHeaderHapusData);
					}
					else {
						ShowPesanErrorSetEquipment(nmPesanHapusError,nmHeaderHapusData);
					}
				}
			}
		)
	}
};


function ValidasiEntrySetEquipment(modul)
{
	var x = 1;
	if (Ext.get('txtKode_SetEquipment').getValue() == '' || Ext.get('txtNameSetEquipment').getValue() == '')
	{
		if (Ext.get('txtKode_SetEquipment').getValue() == '')
		{
			ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmKdAsetSetupAset),modul);
			x=0;			
		}
		else
		{
			ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmNamaAsetSetupAset),modul);
			x=0;			
		};
	};
	
	if (selectCategorySetEquipment == '' || selectCategorySetEquipment ===  undefined)
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmCatSetupAset),modul);
		x=0;
	}
	else if (selectCompanySetEquipment == '' || selectCompanySetEquipment ===  undefined)
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmCompanySetupAset),modul);
		x=0;
	} 
	else if (selectLocationSetEquipment == '' || selectLocationSetEquipment ===  undefined)
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmLocationSetupAset),modul);
		x=0;
	} 
	else if (selectDeptSetEquipment == '' || selectDeptSetEquipment ===  undefined)
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmDeptSetupAset),modul);
		x=0;
	} 
	else if (Ext.get('txtYearsSetEquipment').getValue() == '')
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmYearSetupAset),modul);
		x=0;
	}
	else if (selectStatusAsset === '' || selectStatusAsset === undefined)
	{
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmStatusSetupAsset),modul);
		x=0;
	};
	
	return x;
};

function ShowPesanWarningSetEquipment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   width:250,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSetEquipment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   width:250,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSetEquipment(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   width:250,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetEquipmentInit(rowdata) 
{
    AddNewSetEquipment = false;
    Ext.get('txtKode_SetEquipment').dom.value = rowdata.ASSET_MAINT_ID;
    Ext.get('txtNameSetEquipment').dom.value = rowdata.ASSET_MAINT_NAME;
	Ext.get('cboCategoryEqpSetEquipment').dom.value = rowdata.CATEGORY_NAME;
	Ext.get('cboCompanyEqpSetEquipment').dom.value = rowdata.COMP_NAME;
	Ext.get('cboDeptEqpSetEquipment').dom.value = rowdata.DEPT_NAME;
	Ext.get('cboLocationEqpSetEquipment').dom.value = rowdata.LOCATION;
	Ext.get('txtYearsSetEquipment').dom.value = rowdata.YEARS;
	Ext.get('txtAssetCodeSetEquipment').dom.value = rowdata.ASSET_ID;
	Ext.get('txtDescriptionSetEquipment').dom.value = rowdata.DESCRIPTION_ASSET;
	Dataview_fotoAsset(rowdata.PATH_IMAGE);
	Ext.get('txtKode_SetEquipment').dom.readOnly=true;
	Ext.get('cboStatusAsset').dom.value=rowdata.STATUS_ASSET;
	strPathFileImageSetEquipment = rowdata.PATH_IMAGE;
//	Ext.get('cboCategoryEqpSetEquipment').dom.disabled=true;	
	
	selectStatusAsset=rowdata.STATUS_ASSET_ID;
	selectCategorySetEquipment= rowdata.CATEGORY_ID;
	selectCompanySetEquipment= rowdata.COMP_ID;
	selectLocationSetEquipment= rowdata.LOCATION_ID;
	selectDeptSetEquipment= rowdata.DEPT_ID;
	
	RefreshDataAdditionalField(selectCategorySetEquipment,rowdata.ASSET_MAINT_ID);
	RefreshDataAttachSetEquipmentDetail(rowdata.ASSET_MAINT_ID);
	
};
///---------------------------------------------------------------------------------------///

function SetEquipmentAddNew() 
{
    AddNewSetEquipment = true;   
	Ext.get('txtKode_SetEquipment').dom.value = '';
    Ext.get('txtNameSetEquipment').dom.value = '';
	Ext.get('cboCategoryEqpSetEquipment').dom.value = '';
	Ext.get('cboCompanyEqpSetEquipment').dom.value = '';
	Ext.get('cboDeptEqpSetEquipment').dom.value = '';
	Ext.get('cboLocationEqpSetEquipment').dom.value = '';
	Ext.get('txtYearsSetEquipment').dom.value = '';
	Ext.get('txtAssetCodeSetEquipment').dom.value = '';
	Ext.get('txtDescriptionSetEquipment').dom.value = '';
	Ext.get('cboStatusAsset').dom.value='';
	selectStatusAsset='';	
	strPathFileImageSetEquipment = '';	
	rowSelectedSetEquipment   = undefined;
	Ext.get('txtKode_SetEquipment').dom.readOnly=false;
	dsAdditonalFieldSetEquipment.removeAll();
	dsAttachSetEquipment.removeAll();
	Dataview_fotoAsset('');
};
///---------------------------------------------------------------------------------------///


function getParamSetEquipment() 
{
    var params =
	{
	    Table: 'viewSetupAsset',   
	    asset_maint_id: Ext.get('txtKode_SetEquipment').getValue(),
	    category_id: selectCategorySetEquipment,
		comp_id:selectCompanySetEquipment,
		location_id:selectLocationSetEquipment,
		dept_id:selectDeptSetEquipment,
		asset_maint_name:Ext.get('txtNameSetEquipment').getValue(),
		years:Ext.get('txtYearsSetEquipment').getValue(),
		description_asset:Ext.get('txtDescriptionSetEquipment').getValue(),
		asset_id:Ext.get('txtAssetCodeSetEquipment').getValue(),
		PATH_IMAGE:strPathFileImageSetEquipment,
		List:getArrAdditional(),
		ListAttach:getArrAttachSetEquipment(),
		JmlList:GetListCountAssetDetail(),
		JmlListAttach:GetListCountAttachSetEquipment(),
		JmlFieldAttach: mRecordAttcahSetEquipment.prototype.fields.length-2,
		status_id:selectStatusAsset,
		Hapus:1
	};
    return params
};

function GetListCountAssetDetail()
{
	
	var x=0;
	for(var i = 0 ; i < dsAdditonalFieldSetEquipment.getCount();i++)
	{
		if (dsAdditonalFieldSetEquipment.data.items[i].data.CATEGORY_ID != '' )
		{
			x += 1;
		};
	}
	return x;
	
};

function GetListCountAttachSetEquipment()
{
	
	var x=0;
	for(var i = 0 ; i < dsAttachSetEquipment.getCount();i++)
	{
		if (dsAttachSetEquipment.data.items[i].data.REFF_ID != '' )
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrAdditional()
{
	var x='';
	for(var i = 0 ; i < dsAdditonalFieldSetEquipment.getCount();i++)
	{
		if (dsAdditonalFieldSetEquipment.data.items[i].data.CATEGORY_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'CATEGORY_ID=' + dsAdditonalFieldSetEquipment.data.items[i].data.CATEGORY_ID
			y += z + 'ROW_ADD='+dsAdditonalFieldSetEquipment.data.items[i].data.ROW_ADD
			y += z + 'VALUE='+dsAdditonalFieldSetEquipment.data.items[i].data.VALUE		
			
			if (i === (dsAdditonalFieldSetEquipment.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function getArrAttachSetEquipment()
{
	var x='';
	for(var i = 0 ; i < dsAttachSetEquipment.getCount();i++)
	{
		if (dsAttachSetEquipment.data.items[i].data.REFF_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'REFF_ID=' + dsAttachSetEquipment.data.items[i].data.REFF_ID
			y += z + 'ASSET_MAINT_ID='+dsAttachSetEquipment.data.items[i].data.ASSET_MAINT_ID
			y += z + 'TITLE='+dsAttachSetEquipment.data.items[i].data.TITLE
			y += z + 'DESCRIPTION='+dsAttachSetEquipment.data.items[i].data.DESCRIPTION		
			y += z + 'ROW_REFF='+dsAttachSetEquipment.data.items[i].data.ROW_REFF	
			y += z + 'PATH='+dsAttachSetEquipment.data.items[i].data.PATH
			if (i === (dsAttachSetEquipment.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function RefreshDataSetEquipment(str)
{	
	 if (str === undefined)
	  {
		str='';
	  };
	  
	dsSetEquipmentList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetEquipment, 
				//Sort: 'ASSET_MAINT_ID',
                                Sort: 'asset_maint_id',
				Sortdir: 'ASC', 
				target:'ViewAsset',
				param: str
			} 
		}
	);
	
	
	rowSelectedSetEquipment = undefined;
	return dsSetEquipmentList;
};



function RefreshDataSetEquipmentFilter() 
{   
	var strKriteria = '';
	
	if (Ext.get('cboCategoryEqpSetEquipmentView').getValue() != '' && selectCategorySetEquipmentView != undefined)
    { 
		if (selectCategorySetEquipmentView.id != undefined)
		{
			if(selectCategorySetEquipmentView.id != ' 9999')
			{
				if (selectCategorySetEquipmentView.leaf === false)
				{
					//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                        strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id + '~'
				}
				else
				{
					//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                        strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
				};
			};
		};
	};
	
	dsSetEquipmentList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetEquipment, 
				//Sort: 'ASSET_MAINT_ID',
                                Sort: 'asset_maint_id',
				Sortdir: 'ASC', 
				target:'ViewAsset',
				param: strKriteria
			} 
		}
	);
    return dsSetEquipmentList;
};



function mComboMaksDataSetEquipment()
{
  var cboMaksDataSetEquipment = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetEquipment',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetEquipment,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetEquipment=b.data.displayText ;
					var strKriteria='';
					if (selectCategorySetEquipmentView.id != undefined)
					{
						if(selectCategorySetEquipmentView.id != ' 9999')
						{
							if (selectCategorySetEquipmentView.leaf === false)
							{
								//strKriteria =' where left(category_id,' + selectCategorySetEquipmentView.id.length + ')=' +  selectCategorySetEquipmentView.id
                                                                strKriteria ='substring(category_id,1,' + selectCategorySetEquipmentView.id.length + ') = ~' +  selectCategorySetEquipmentView.id +'~';
							}
							else
							{
								//strKriteria =' WHERE Category_id=~'+ selectCategorySetEquipmentView.id + '~'
                                                                strKriteria ='category_id = ~'+ selectCategorySetEquipmentView.id + '~'
							};
						};
					};
					
					RefreshDataSetEquipment(strKriteria);
				} 
			}
		}
	);
	return cboMaksDataSetEquipment;
};
 

function mComboCategoryEqpSetEquipmentView()
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySetupAssetView = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboSetupAsetView = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSetEquipmentView = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeSetEquipmentView  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						id:IdCategorySetEquipmentView,
						children: StrTreeComboSetEquipmentLocal,
						autoScroll: true
					}
				) 
				treeSetEquipmentView.setRootNode(rootTreeSetEquipmentView); 
				
				this.list = treeSetEquipmentView
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSetupAsetView.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSetEquipmentView
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				if (node.attributes.id === IdCategorySetEquipmentView)
				{
					this.setRawValue(valueCategorySetEquipmentView);
				}
				else
				{
					this.setRawValue(node.attributes.text);
				};
				
				selectCategorySetEquipmentView = node.attributes;
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySetupAssetView
		}
	);
	
	
	
	var cboCategoryEqpSetEquipmentView = new Ext.TreeComboSetupAsetView
	(
		{
			id:'cboCategoryEqpSetEquipmentView',
			fieldLabel: nmCatSetupAset + ' ',
			width:250,
			value:valueCategorySetEquipmentView
		}
	);
	
	return cboCategoryEqpSetEquipmentView;

};


function RefreshDataAdditionalField(strCat,StrIDAsset)
{	
	var strKriteria;

	//strKriteria=' WHERE CATEGORY_ID=~' + selectCategorySetEquipment + '~'
        strKriteria='category_id = ~' + selectCategorySetEquipment + '~'

	if (StrIDAsset != '')
	{
		//strKriteria +=' AND (ASSET_MAINT_ID=~' + StrIDAsset + '~ or ASSET_MAINT_ID is null) '
                strKriteria +=' AND (asset_maint_id = ~' + StrIDAsset + '~ or asset_maint_id is null) '
	}
	dsAdditonalFieldSetEquipment.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'ROW_ADD',
                                Sort: 'row_add',
				Sortdir: 'ASC', 
				target:'viAdditionalField',
				param: strKriteria
			} 
		}
	);	
	return dsAdditonalFieldSetEquipment;
};


function RefreshDataAdditionalFieldBlank(strCat,StrIDAsset)
{	
	var strKriteria;
	//strKriteria=' WHERE CATEGORY_ID=~' + selectCategorySetEquipment + '~'
        strKriteria='category_id = ~' + selectCategorySetEquipment + '~'
	if (StrIDAsset != '')
	{
		//strKriteria +=' AND (ASSET_MAINT_ID=~' + StrIDAsset + '~ or ASSET_MAINT_ID is null) '
                strKriteria +=' AND (asset_maint_id = ~' + StrIDAsset + '~ or asset_maint_id is null) '
	}
	dsAdditonalFieldSetEquipment.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'ROW_ADD',
                                Sort: 'row_add',
				Sortdir: 'ASC', 
				target:'viAdditionalFieldBlank',
				param: strKriteria
			} 
		}
	);	
	return dsAdditonalFieldSetEquipment;
};

function Dataview_fotoAsset(StrPath) 
{
	if (StrPath=='' || StrPath==null)
	{
		//StrPath= './Img Asset/blank.png'
                StrPath= baseURL + 'Img Asset/blank.png'
	}
	var pgbr=Ext.getCmp('gbr');    
    Ext.DomHelper.overwrite(pgbr.getEl(),
	{                                	
	  tag: 'img', src: StrPath ,style:'margin-top:3px;margin-left:3px;margin-bottom:3px;width:175px;height:150px;'   
    }, true).show(true).frame();		
};

function GetStrTreeComboSetEquipmentLocal()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboSetEquipmentLocal= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};


function mComboStatusAsset()
{
	var Field = ['STATUS_ASSET_ID', 'STATUS_ASSET'];
	dsStatusAsset = new WebApp.DataStore({ fields: Field });

	dsStatusAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ASSET',
                            Sort: 'status_asset',
			    Sortdir: 'ASC',
			    target: 'viComboStatusAsset',
			    //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                            param:' status_asset_id <> ~xxx~'
			}
		}
	);
	
  var cboStatusAsset = new Ext.form.ComboBox
	(
		{
			id:'cboStatusAsset',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText: nmPilihStatusSetupAset,
			fieldLabel: nmStatusSetupAsset + ' ',			
			anchor:'54%',
			store: dsStatusAsset,
			valueField: 'STATUS_ASSET_ID',
			displayField: 'STATUS_ASSET',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectStatusAsset=b.data.STATUS_ASSET_ID ;
				} 
			}
		}
	);
	
	dsStatusAsset.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'STATUS_ASSET',
                            Sort: 'status_asset',
			    Sortdir: 'ASC',
			    target: 'viComboStatusAsset',
			     //param:' WHERE STATUS_ASSET_ID <> ~xxx~'
                             param:' status_asset_id <> ~xxx~'
			}
		}
	);
	
	return cboStatusAsset;
};