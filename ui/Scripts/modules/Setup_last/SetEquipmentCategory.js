
var CurrentListPartCat =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentListFieldSetCat =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentListFieldSetCatMetering =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordPartCategory = Ext.data.Record.create
	(
		[
		   {name: 'PART_ID', mapping:'PART_ID'},
		   {name: 'PART_NAME', mapping:'PART_NAME'},
		   {name: 'TAG', mapping:'TAG'}		  
		]
	);

var mRecordFieldSetCat = Ext.data.Record.create
	(
		[
		   {name: 'CATEGORY_ID', mapping:'CATEGORY_ID'},
		   {name: 'ROW_ADD', mapping:'ROW_ADD'},
		   {name: 'ADDFIELD', mapping:'ADDFIELD'},
		   {name: 'TYPE_FIELD', mapping:'TYPE_FIELD'},
		   {name: 'TYPEFIELD', mapping:'TYPEFIELD'},
		   {name: 'LENGHT', mapping:'LENGHT'}		   
		]
	);

var selectTypeCatEqp;
var selectCategorySetCat;
var dsCategorySetupCat;

var dsSetEquipmentCatList;

var cellSelectListPartCat;
var dsDTLAddFieldPartSetEquipmentCatList;
var AddNewSetEquipmentCat;
var selectCountCatEqp=50;
var rowSelectedSetEquipmentCat;
var rowSelectedTreeSetEquipmentCat;
var SetEquipmentCatLookUps;
var dsDTLTRSetEquipmentCatList;
var dsDTLMeteringSetEquipmentCatList;
var cellSelectListFieldSetEquipmentCat;
var cellSelectListFieldSetEquipmentCatMetering;
var strTreeCriteriaSetEquipment;
var StrTreeComboSetEquipmentCat;
var rootTreeSetEquipmentCatEntry;
var treeSetEquipmentCatEntry;
var rootTreeSetEquipmentCatView;
var IdCategorySetEquipmentCatView=' 9999';
	
CurrentPage.page = getPanelSetEquipmentCat(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetEquipmentCat(mod_id) 
{
    var Field = ['CATEGORY_ID','CATEGORY_NAME','PARENT','TYPE','PARENT_NAME','DESCTYPE'];
    dsSetEquipmentCatList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetEquipmentCat();
	

    var grListSetEquipmentCat = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetEquipmentCat',
		    stripeRows: true,
		    store: dsSetEquipmentCatList,
			autoScroll: true,
		    columnLines: true,
			border:true,
		    anchor: '100% 100%',
			height:410,
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetEquipmentCat=undefined;
							rowSelectedSetEquipmentCat = dsSetEquipmentCatList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetEquipmentCat = dsSetEquipmentCatList.getAt(ridx);
					if (rowSelectedSetEquipmentCat != undefined)
					{
						SetEquipmentCatLookUp(rowSelectedSetEquipmentCat.data);
					}
					else if(rowSelectedTreeSetEquipmentCat != undefined)
					{
						SetEquipmentCatLookUp(rowSelectedTreeSetEquipmentCat,true);
					}
					else
					{
						SetEquipmentCatLookUp();
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetEquipmentCat',
                        header: nmKdCategoryAset2,                      
                        dataIndex: 'CATEGORY_ID',
                        sortable: true,
                        width: 50
                    },
					{
					    id: 'colSetEquipmentCat',
					    header: nmCategoryAset,					   
					    dataIndex: 'CATEGORY_NAME',
					    width: 150,
					    sortable: true
					},					
					{
					    id: 'colSetParentName',
					    header: 'Parent',					   
					    dataIndex: 'PARENT_NAME',
					    width: 150,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colSetType',
					    header: 'Type',					   
					    dataIndex: 'TYPE',
						hidden:true,
					    width: 80,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				// nmKdCategoryAset2 + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: nmKdCategoryAset2 + ' : ',
					// id: 'txtKDSetEquipmentCatFilter',                   
					// width:30,
					// onInit: function() { }
				// }, ' ','-',
				// nmCategoryAset + ' : ', ' ',
				// {
					// xtype: 'textfield',
					// fieldLabel: nmCategoryAset + ' : ',
					// id: 'txtSetEquipmentCatFilter',                   
					// anchor: '70%',
					// onInit: function() { }
				// }, ' ','-',
				// nmMaksData + ' : ', ' ',mComboMaksDataCatEqp(),
				// ' ','-',
				// {
				    // id: 'btnRefreshSetEquipmentCat',
				    // text: nmRefresh,
				    // tooltip: nmRefresh,
				    // iconCls: 'refresh',
				    // handler: function(sm, row, rec) 
					// {  
						// RefreshDataSetEquipmentCatFilter();
					// }
				// },
				{
				    id: 'btnEditSetEquipmentCat',
				    text: nmEditData,
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetEquipmentCat != undefined)
						{
						    SetEquipmentCatLookUp(rowSelectedSetEquipmentCat.data);
						}
						else if(rowSelectedTreeSetEquipmentCat != undefined)
						{
							SetEquipmentCatLookUp(rowSelectedTreeSetEquipmentCat,true);
						}
						else
						{
						    SetEquipmentCatLookUp();
						};
				    }
				},' ','-'
			],
		    viewConfig: { forceFit: true }
		}
	);
	
	
	treeCatAset= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:410,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriaSetEquipment=n.attributes
					rowSelectedTreeSetEquipmentCat=n.attributes;
					if (strTreeCriteriaSetEquipment.id != ' 9999')
					{
						if (strTreeCriteriaSetEquipment.leaf === false)
						{
							var str='';
							//str=' and left(parent,' + n.attributes.id.length + ')=' + n.attributes.id
                                                        str=' and substring(parent,1, ' + n.attributes.id.length + ') = ~' + n.attributes.id + '~';
							RefreshDataSetEquipmentCat(str);
						}
						else
						{
							RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
						};
					}
					else
					{
						RefreshDataSetEquipmentCat('');
					};
				}
			}
		}
	);
	
	rootTreeSetEquipmentCatView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:nmTreeComboParent,
			id:IdCategorySetEquipmentCatView,
			children: StrTreeSetEquipment,
			autoScroll: true
		}
	)  
  
  treeCatAset.setRootNode(rootTreeSetEquipmentCatView);  
	

    var FormSetEquipmentCat = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title: nmFormCategoryAset,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupSetEquipmentCat',
		    items: 
			[
				{
					columnWidth: .34,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[
						treeCatAset
					]
				},
				{
					columnWidth: .66,
					layout: 'form',
					bodyStyle: 'padding:6px 6px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[
						grListSetEquipmentCat
					]
				}
			]
		}
	);
    //END var FormSetEquipmentCat--------------------------------------------------
	RefreshDataSetEquipmentCat();
	GetStrTreeComboSetEquipmentCat();
        
    return FormSetEquipmentCat
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///



function SetEquipmentCatLookUp(rowdata,mBolTree) 
{
	var lebar=600;
    SetEquipmentCatLookUps = new Ext.Window   	
    (
		{
		    id: 'SetEquipmentCatLookUps',
		    title: nmFormCategoryAset,
		    closeAction: 'destroy',
			//y:90,
		    width: lebar,
		    height: 520,//450,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupSetEquipmentCat',
		    modal: true,
		    items: getFormEntrySetEquipmentCat(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetEquipmentCat=undefined;
					rootTreeSetEquipmentCat = new Ext.tree.AsyncTreeNode
					(
						{
							expanded: true,
							text:nmTreeComboParent,
							id:IdCategorySetEquipmentCatView,
							children: StrTreeSetEquipment,
							autoScroll: true
						}
					)  
					treeCatAset.setRootNode(rootTreeSetEquipmentCat);  
					//RefreshDataSetEquipmentCat();
					if(strTreeCriteriaSetEquipment != undefined)
					{
						if (strTreeCriteriaSetEquipment.id != ' 9999')
						{
							if (strTreeCriteriaSetEquipment.leaf === false)
							{
								var str='';
								//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                str=' and substring(parent,1,' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
								RefreshDataSetEquipmentCat(str);
							}
							else
							{
								RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
							};
						}
						else
						{
							RefreshDataSetEquipmentCat('');
						};
					};
				}
            }
		}
	);
    //END var SetEquipmentCatLookUps------------------------------------------------------------------

    SetEquipmentCatLookUps.show();
	if (rowdata == undefined)
	{
		SetEquipmentCatAddNew();
	}
	else
	{
		SetEquipmentCatInit(rowdata,mBolTree)
	}	
};

//  END FUNCTION SetEquipmentCatLookUp
///------------------------------------------------------------------------------------------------------------///




function getFormEntrySetEquipmentCat(lebar) 
{
    var pnlSetEquipmentCat = new Ext.FormPanel
    (
		{
		    id: 'pnlSetEquipmentCat',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 145,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupSetEquipmentCat',
		    border: false,
		    items:
			[
				{
				    layout: 'form',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 90,
				    labelAlign: 'right',
					labelWidth:65,
				    //anchor: '100%',
				    items:
					[
						getItemPanelKodeNameSetEquipmentCat(lebar),
						mComboCategorySetCat(lebar),
						mComboTypeCatEqp()
					]
				}
				
			],
		    tbar:
			[
				{
				    id: 'btnAddSetEquipmentCat',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetEquipmentCatAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetEquipmentCat',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetEquipmentCatSave(false);
						//RefreshDataSetEquipmentCat();
						if(strTreeCriteriaSetEquipment != undefined)
							{
								if (strTreeCriteriaSetEquipment.id != ' 9999')
								{
									if (strTreeCriteriaSetEquipment.leaf === false)
									{
										var str='';
										//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                                str=' and substring( parent, 1,' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
										RefreshDataSetEquipmentCat(str);
									}
									else
									{
										RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
									};
								}
								else
								{
									RefreshDataSetEquipmentCat('');
								};
							};
						loadCategory();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetEquipmentCat',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetEquipmentCatSave(true);
						RefreshDataSetEquipmentCat();
						if (x===undefined)
						{
							SetEquipmentCatLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetEquipmentCat',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
						SetEquipmentCatDelete() ;
						if(strTreeCriteriaSetEquipment != undefined)
						{
							if (strTreeCriteriaSetEquipment.id != ' 9999')
							{
								if (strTreeCriteriaSetEquipment.leaf === false)
								{
									var str='';
									//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                        str=' and substring( parent, 1, ' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
									RefreshDataSetEquipmentCat(str);
								}
								else
								{
									//RefreshDataSetEquipmentCat('and parent =~' + strTreeCriteriaSetEquipment.id + '~');
                                                                        RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
								};
							}
							else
							{
								RefreshDataSetEquipmentCat('');
							};
						};
						//RefreshDataSetEquipmentCat();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetEquipmentCat',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950007);}
				}
			]
		}
	); 
	
	var GDtabDetailSetEquipmentCat = new Ext.TabPanel   
	(
		{
			id:'GDtabDetailSetEquipmentCat',
	        region: 'center',
	        activeTab: 0,
	        anchor: '100% 40%',
			border:false,
	        plain: true,
	        defaults: 
			{
		        autoScroll: false
	        },
			items: 
			[
			    {
					 title: nmTitleGridAddField,
					 id: 'tabAddFieldSetEquipmentCat',
					 height: 150,
					 items:
					 [
						GetDTLTRSetEquipmentCatGrid()
					 ],
					 tbar: 
					[
						{
							id:'btnTambahBrsSetEquipmentCat',
							text: nmTambahBaris,
							tooltip: nmTambahBaris,
							iconCls: 'AddRow',
							handler: function() 
							{ 
								TambahBarisAddFieldSetCat();
							}
						},
						'-',
						{
							id:'btnHpsBrsSetEquipmentCat',
							text: nmHapusBaris,
							tooltip: nmHapusBaris,
							iconCls: 'RemoveRow',
							handler: function()
							{
								if (dsDTLTRSetEquipmentCatList.getCount() > 0 )
								{
									if (cellSelectListFieldSetEquipmentCat != undefined)
									{
										if(CurrentListFieldSetCat != undefined)
										{
											HapusBarisAddFieldSetCat();
										};
									}
									else
									{
										ShowPesanWarningCatEqp(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
							}
						},' ','-'
					],
			        listeners:
					{
						  activate: function() 
						  {
							
						  }
					}
				},
				{
					 title: nmTitleGridAddPart,
					 id: 'tabAddFieldPartSetEquipmentCat',
					 height: 180,
					 items:
					 [
						GetAddFieldPartSetEquipmentCatGrid()
					 ],
					  tbar: 
					[
						{
							id:'btnTambahBrsAddFieldPartSetEquipmentCat',
							text: nmTambahBaris,
							tooltip: nmTambahBaris,
							iconCls: 'AddRow',
							handler: function() 
							{ 
								var str='';
								var str = GetStrListPartCat();
								if (str !='')
								{
									//str = ' WHERE PART_ID not in ( ' + str + ') '
                                                                        str = ' part_id not in ( ' + str + ') '
								};
								
								var p = GetRecordBaruPartCategory()
								
								FormLookupPartCategory(str,p,dsDTLAddFieldPartSetEquipmentCatList)
							}
						},
						'-',
						{
							id:'btnHpsBrsAddFieldPartSetEquipmentCat',
							text: nmHapusBaris,
							tooltip: nmHapusBaris,
							iconCls: 'RemoveRow',
							handler: function()
							{
								if (dsDTLAddFieldPartSetEquipmentCatList.getCount() > 0 )
								{
									if (cellSelectListPartCat != undefined)
									{
										if(CurrentListFieldSetCat != undefined)
										{
											HapusBarisPartCat();
										};
									}
									else
									{
										ShowPesanWarningCatEqp(nmGetKonfirmasiHapusBaris(),nmHapusBaris);
									};
								};
							}
						},' ','-'
					],
			        listeners:
					{
						  activate: function() 
						  {
								
						  }
					}
				}
			]
		}
	);
	
   
    var FormTRSetEquipmentCat = new Ext.Panel
	(
		{
		    id: 'FormTRSetEquipmentCat',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlSetEquipmentCat,GDtabDetailSetEquipmentCat,
				{
					layout: 'hBox',
					width:lebar-34,
					height:36,
					border: false,
					bodyStyle: 'padding:5px 0px 5px 5px',
					defaults: { margins: '3 3 3 3' },
					anchor: '90%',
					items:
					[
						{
							xtype:'button',
							text:nmBtnMetering,
							width:80,
							style:{'margin-left':'0px','margin-top':'0px'},
							hideLabel:true,
							id: 'btnMeteringSetupCategory',
							handler:function()
							{
								SetMeteringLookUp(cellSelectListFieldSetEquipmentCatMetering);
							}
						}
					]
				},GetDTLMeteringSetEquipmentCatGrid()
			]

		}
	);

    return FormTRSetEquipmentCat

};


function getItemPanelKodeNameSetEquipmentCat(lebar)
{
	var items= 			
	{
		layout:'column',
		border:false,
		width:lebar-70,
		items:
		[
			{
				columnWidth:.3,
				layout: 'form',
				border:false,
				labelWidth:65,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmKdCategoryAset2 + ' ',
						name: 'txtKode_SetEquipmentCat',
						id: 'txtKode_SetEquipmentCat',
						anchor: '98%',
						readOnly:true
					}
				]
			},
			{
				columnWidth:.7,
				layout: 'form',
				labelWidth:65,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmCategoryAset + ' ',
						name: 'txtSetEquipmentCat',
						id: 'txtSetEquipmentCat',
						anchor: '100%'
					}
				]
			}
			
		]
	}	
	
	return items;
};



function TambahBarisAddFieldSetCat()
{
	var x = true;
	
	if (Ext.get('txtSetEquipmentCat').dom.value === '')
	{
		ShowPesanWarningCatEqp(nmGetValidasiKosong(nmCategoryAset2), nmTambahBaris);
		x=false;
	};
	
	if (x === true)
	{
		var p = RecordBaruAddFieldSetCat();
		dsDTLTRSetEquipmentCatList.insert(dsDTLTRSetEquipmentCatList.getCount(), p);
	};
	
};

function HapusBarisPartCat()
{
	if (cellSelectListPartCat.data.PART_ID != '')
	{
		Ext.Msg.show
		(
			{
			   title:nmHapusBaris,
			   msg: nmGetValidasiHapus(nmTitleGridAddField) + ' ' + nmBaris + ' :' + ' ' + (CurrentListPartCat.row + 1) ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						if(dsDTLAddFieldPartSetEquipmentCatList.data.items[CurrentListPartCat.row].data.TAG === '1')
						{
							dsDTLAddFieldPartSetEquipmentCatList.removeAt(CurrentListPartCat.row);
						}
						else
						{
							Ext.Msg.show
							(
								{
								   title:nmHapusBaris,
								   msg: nmGetValidasiHapusBarisDatabase() ,
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
										if (btn =='yes') 
										{
											DeleteListPartCat();
										};
									}
								}
							)
						};	
					}; 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
		dsDTLAddFieldPartSetEquipmentCatList.removeAt(CurrentListPartCat.row);
	};
};

function HapusBarisAddFieldSetCat()
{
	if (cellSelectListFieldSetEquipmentCat.data.ADDFIELD != '')
	{
		Ext.Msg.show
		(
			{
			   title:nmHapusBaris,
			   msg: nmGetValidasiHapus(nmTitleGridAddField) + ' ' + nmBaris + ' :' + ' ' + (CurrentListFieldSetCat.row + 1) + ', ' + nmHeaderColAddFieldLabel + ' : '+ ' ' + cellSelectListFieldSetEquipmentCat.data.ADDFIELD + ' , ' + nmHeaderColAddFieldType + ' : ' + cellSelectListFieldSetEquipmentCat.data.TYPEFIELD ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						if(dsDTLTRSetEquipmentCatList.data.items[CurrentListFieldSetCat.row].data.ROW_ADD === '')
						{
							dsDTLTRSetEquipmentCatList.removeAt(CurrentListFieldSetCat.row);
						}
						else
						{
							Ext.Msg.show
							(
								{
								   title:nmHapusBaris,
								   msg: nmGetValidasiHapusBarisDatabase() ,
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
										if (btn =='yes') 
										{
											DeleteListFieldSetEquipmentCat();
										};
									}
								}
							)
						};	
					}; 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
		dsDTLTRSetEquipmentCatList.removeAt(CurrentListFieldSetCat.row);
	};
};

function DeleteListFieldSetEquipmentCat()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData,
			params:  getParamDeleteSetEquipmentCat(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCatEqp(nmPesanHapusSukses,nmHeaderHapusData);
					dsDTLTRSetEquipmentCatList.removeAt(CurrentListFieldSetCat.row);
					cellSelectListFieldSetEquipmentCat=undefined;
					RefreshDataAddFieldSetEquipmentCat(Ext.get('txtKode_SetEquipmentCat').dom.value);
					AddNewSetEquipmentCat = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCatEqp(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanWarningCatEqp(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function DeleteListPartCat()
{
	Ext.Ajax.request
	(
		{
			url: WebAppUrl.UrlDeleteData,
			params:  getParamDeletePartCat(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoCatEqp(nmPesanHapusSukses,nmHeaderHapusData);
					dsDTLAddFieldPartSetEquipmentCatList.removeAt(CurrentListPartCat.row);
					cellSelectListPartCat=undefined;
					RefreshDataPartCat(Ext.get('txtKode_SetEquipmentCat').dom.value);
					AddNewSetEquipmentCat = false;
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarningCatEqp(nmPesanHapusGagal, nmHeaderHapusData);
				}
				else 
				{
					ShowPesanWarningCatEqp(nmPesanHapusError,nmHeaderHapusData);
				};
			}
		}
	)
};

function GetDTLTRSetEquipmentCatGrid() 
{
    var fldDetail = ['ADDFIELD','CATEGORY_ID','ROW_ADD','TYPE_FIELD','TYPE_FIELD_NAME','TYPEFIELD','LENGHT'];
	
    dsDTLTRSetEquipmentCatList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRSetEquipmentCat = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDTLTRSetEquipmentCatList,
		    border: false,
		    columnLines: true,
		    frame: true,
		    //anchor: '100%',
			height: 143,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectListFieldSetEquipmentCat = dsDTLTRSetEquipmentCatList.getAt(row);
					        CurrentListFieldSetCat.row = row;
					        CurrentListFieldSetCat.data = cellSelectListFieldSetEquipmentCat;
					    }
					}
				}
			),
		    cm: TRSetEquipmentCatDetailColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridDTLTRSetEquipmentCat;
};



function TRSetEquipmentCatDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colLabelAddFieldSetEquipmentCat',
				header: nmHeaderColAddFieldLabel,
				dataIndex: 'ADDFIELD',
				width:300,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolLabelAddFieldSetEquipmentCat',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{
						}
					}
				)
			},
			{
				id: 'colTypeAddFieldSetEquipmentCat',
				header: nmHeaderColAddFieldType,
				dataIndex: 'TYPEFIELD',
				width:250,
				editor: new Ext.form.ComboBox
				(
					{
						id:'cboTypeAddFieldSetEquipmentCat',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						mode: 'local',
						allowBlank: true,
						enableKeyEvents : true,
						store: new Ext.data.ArrayStore
						(
							{
								id: 0,
								fields: 
								[
									'Id',
									'displayText'
								],
							data: [[1, 'Text'], [2, 'Numeric'],[3, 'Date / Time']]
							}
						),
						valueField: 'displayText',
						displayField: 'displayText',
						listeners:  
						{
							'select': function(a,b,c)
							{   
								//dsDTLTRSetEquipmentCatList.data.items[CurrentListFieldSetCat.row].data.TYPE_FIELD_NAME = b.data.displayText;
								//dsDTLTRSetEquipmentCatList.data.items[CurrentListFieldSetCat.row].data.TYPE_FIELD = b.data.Id;
							} 
						}
					}
				),
				renderer: function(v, params, record) 
				{
				    return record.data.TYPEFIELD;
				}	
			},
			{
				id: 'colLengthAddFieldSetEquipmentCat',
				header: nmColLengthAddFieldSetEqpCat,//nmHeaderColAddFieldLabel,
				dataIndex:'LENGHT',
				width:300,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolLengthAddFieldSetEquipmentCat',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{
						}
					}
				)
			}
		]
	)
};

function GetAddFieldPartSetEquipmentCatGrid() 
{
    var fldDetail = ['CATEGORY_ID','PART_ID','PART_NAME','TAG'];
	
    dsDTLAddFieldPartSetEquipmentCatList = new WebApp.DataStore({ fields: fldDetail })

    var gridAddFieldPartSetEquipmentCat = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDTLAddFieldPartSetEquipmentCatList,
		    border: false,
		    columnLines: true,
		    frame: true,
		    //anchor: '100%',
			height: 143,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectListPartCat = dsDTLAddFieldPartSetEquipmentCatList.getAt(row);
					        CurrentListPartCat.row = row;
					        CurrentListPartCat.data = cellSelectListPartCat;
					    }
					}
				}
			),
		    cm: TRAddFieldPartSetEquipmentCatDetailColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridAddFieldPartSetEquipmentCat;
};

function TRAddFieldPartSetEquipmentCatDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colKdPartAddFieldSetEquipmentCat',
				header: nmColNumPartSetEqpCat,
				dataIndex: 'PART_ID',
				width:90,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolKdPartAddFieldSetEquipmentCat',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{
							
						}
					}
				)
			},
			{
				id: 'colPartNameAddFieldSetEquipmentCat',
				header: nmColNamePartSetEqpCat,
				dataIndex: 'PART_NAME',
				width:150,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolPartNameAddFieldSetEquipmentCat',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{
						}
					}
				)
			}
		]
	)
};

function GetDTLMeteringSetEquipmentCatGrid() 
{
    var fldDetail = ['SERVICE_NAME','SERVICE_TYPE','INTERVAL','UNIT','SERVICE_ID','TYPE_SERVICE_ID','UNIT_ID','IS_AKTIF','METERING_ASSUMPTIONS','DAYS_ASSUMPTIONS','FLAG','ACT_DAY_BEFORE', 'ACT_DAY_TARGET', 'ACT_DAY_AFTER', 'ACT_BEFORE', 'ACT_TARGET', 'ACT_AFTER', 'LEGEND_BEFORE', 'LEGEND_TARGET', 'LEGEND_AFTER', 'ACTION_BEFORE', 'ACTION_TARGET', 'ACTION_AFTER','TAG_ACT_BEFORE','TAG_ACT_TARGET','TAG_ACT_AFTER'];
	
    dsDTLMeteringSetEquipmentCatList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLMeteringSetEquipmentCat = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsDTLMeteringSetEquipmentCatList,
		    border: true,
		    columnLines: true,
		    frame: false,
		    anchor: '100% 23%',
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectListFieldSetEquipmentCatMetering = dsDTLMeteringSetEquipmentCatList.getAt(row);
					        CurrentListFieldSetCatMetering.row = row;
					        CurrentListFieldSetCatMetering.data = cellSelectListFieldSetEquipmentCatMetering;
					    }
					}
				}
			),
		    cm: MeteringSetEquipmentCatDetailColumModel(),
			 viewConfig: { forceFit: true }
		}
	);

    return gridDTLMeteringSetEquipmentCat;
};


function MeteringSetEquipmentCatDetailColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colServMeteringSetEquipmentCat',
				header: nmColServMetSetEqpCat,
				dataIndex: 'SERVICE_NAME',
				width:200
			},
			{
				id: 'colLogMeteringSetEquipmentCat',
				header: nmColLogMetSetEqpCat,
				dataIndex: 'SERVICE_TYPE',
				width:100	
			},
			{
				id: 'colIIntervalMeteringSetEquipmentCat',
				header: nmColIntMetSetEqpCat,
				dataIndex: 'INTERVAL',
				width:150
			},
			{
				id: 'colISatuanMeteringSetEquipmentCat',
				header: nmColUnitMetSetEqpCat,
				dataIndex: 'UNIT',
				width:150
			}
		]
	)
};

function SetEquipmentCatSave(mBol) 
{
	if (ValidasiEntrySetEquipmentCat(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetEquipmentCat == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEquipmentCat(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoCatEqp(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataSetEquipmentCat();
							if(strTreeCriteriaSetEquipment != undefined)
							{
								if (strTreeCriteriaSetEquipment.id != ' 9999')
								{
									if (strTreeCriteriaSetEquipment.leaf === false)
									{
										var str='';
										//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                                str=' and substring(parent, 1,' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
										RefreshDataSetEquipmentCat(str);
									}
									else
									{
										RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
									};
								}
								else
								{
									RefreshDataSetEquipmentCat('');
								};
							};
							
							if(mBol === false)
							{
								Ext.get('txtKode_SetEquipmentCat').dom.value=cst.CatId;
								RefreshDataAddFieldSetEquipmentCat(Ext.get('txtKode_SetEquipmentCat').dom.value);
								RefreshDataPartCat(Ext.get('txtKode_SetEquipmentCat').dom.value);
								loadCategory();								
							};
							AddNewSetEquipmentCat = false;
							GetStrTreeSetEquipment();
							GetStrTreeComboSetEquipmentCat();
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCatEqp(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorCatEqp(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetEquipmentCat(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoCatEqp(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataSetEquipmentCat();
							if(strTreeCriteriaSetEquipment != undefined)
							{
								if (strTreeCriteriaSetEquipment.id != ' 9999')
								{
									if (strTreeCriteriaSetEquipment.leaf === false)
									{
										var str='';
										//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                                str=' and substring(parent, 1, ' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
										RefreshDataSetEquipmentCat(str);
									}
									else
									{
										RefreshDataSetEquipmentCat('and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
									};
								}
								else
								{
									RefreshDataSetEquipmentCat('');
								};
							};
							
							if(mBol === false)
							{
								RefreshDataAddFieldSetEquipmentCat(Ext.get('txtKode_SetEquipmentCat').dom.value);
								loadCategory();								
							};
							GetStrTreeSetEquipment();
							GetStrTreeComboSetEquipmentCat();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningCatEqp(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorCatEqp(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetEquipmentCatSave
//---------------------------------------------------------------------------------------//

function SetEquipmentCatDelete() 
{
	if (ValidasiEntrySetEquipmentCat(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmCategoryAset2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetEquipmentCat(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoCatEqp(nmPesanHapusSukses,nmHeaderHapusData);
										//RefreshDataSetEquipmentCat();
										if(strTreeCriteriaSetEquipment != undefined)
										{
											if (strTreeCriteriaSetEquipment.id != ' 9999')
											{
												if (strTreeCriteriaSetEquipment.leaf === false)
												{
													var str='';
													//str=' and left(parent,' + strTreeCriteriaSetEquipment.id.length + ')=' + strTreeCriteriaSetEquipment.id
                                                                                                        str=' and substring(parent,1,' + strTreeCriteriaSetEquipment.id.length + ') = ~' + strTreeCriteriaSetEquipment.id + '~';
													RefreshDataSetEquipmentCat(str);
												}
												else
												{
													//RefreshDataSetEquipmentCat('and parent =~' + strTreeCriteriaSetEquipment.id + '~');
                                                                                                        RefreshDataSetEquipmentCat(' and parent = ~' + strTreeCriteriaSetEquipment.id + '~');
												};
											}
											else
											{
												RefreshDataSetEquipmentCat('');
											};
										};
										RefreshDataAddFieldSetEquipmentCat();
										GetStrTreeSetEquipment();
										GetStrTreeComboSetEquipmentCat();
										SetEquipmentCatAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningCatEqp(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorCatEqp(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetEquipmentCat(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetEquipmentCat').getValue() == '' || (Ext.get('txtSetEquipmentCat').getValue() == '') || dsDTLTRSetEquipmentCatList.getCount() > 0)
	{
		if (Ext.get('txtKode_SetEquipmentCat').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtSetEquipmentCat').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningCatEqp(nmGetValidasiKosong(nmCategoryAset2),modul);
			};
		}
		else if (dsDTLTRSetEquipmentCatList.getCount() > 0 )
		{
			var mBolKetemu = false;
			var colKetemu=0;
			for ( var i = 0 ; i < dsDTLTRSetEquipmentCatList.getCount() ; i++)
			{
				if (dsDTLTRSetEquipmentCatList.data.items[i].data.ADDFIELD === '')
				{
					mBolKetemu = true;
					colKetemu=1;
					break;
				}
				else if(dsDTLTRSetEquipmentCatList.data.items[i].data.TYPEFIELD === '')
				{
					mBolKetemu = true;
					colKetemu=2;
					break;
				};
			}
			
			if (mBolKetemu === true)
			{
				x=0;
				if(colKetemu === 1)
				{
					ShowPesanWarningCatEqp(nmGetValidasiKosong(nmHeaderColAddFieldLabel),modul);
				}
				else if (colKetemu === 2)
				{
					ShowPesanWarningCatEqp(nmGetValidasiKosong(nmHeaderColAddFieldType),modul);
				};
			};
		};
	};
	
	return x;
};

function ShowPesanWarningCatEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorCatEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoCatEqp(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetEquipmentCatInit(rowdata,mBolTree) 
{
    AddNewSetEquipmentCat = false;
	if (mBolTree === true)
	{
		Ext.get('txtKode_SetEquipmentCat').dom.value = rowdata.id;
		Ext.get('txtSetEquipmentCat').dom.value = rowdata.text;
		RefreshDataAddFieldSetEquipmentCat(rowdata.id);
		RefreshDataPartCat(rowdata.id);
		RefreshDataAddFieldMetering(rowdata.id);
		Ext.get('cboTypeCatEqp').dom.value = 'GROUP';
		Ext.get('cboCategorySetCat').dom.value = rowdata.parents_name;
		selectTypeCatEqp= 'G';
		selectCategorySetCat= rowdata.parents;
	}
	else
	{
		Ext.get('txtKode_SetEquipmentCat').dom.value = rowdata.CATEGORY_ID;
		Ext.get('txtSetEquipmentCat').dom.value = rowdata.CATEGORY_NAME;
		RefreshDataAddFieldSetEquipmentCat(rowdata.CATEGORY_ID);
		RefreshDataPartCat(rowdata.CATEGORY_ID);
		RefreshDataAddFieldMetering(rowdata.CATEGORY_ID);
		Ext.get('cboTypeCatEqp').dom.value = rowdata.DESCTYPE;
		Ext.get('cboCategorySetCat').dom.value = rowdata.PARENT_NAME;
		selectTypeCatEqp= rowdata.TYPE;
		selectCategorySetCat= rowdata.PARENT;
	};
   
};
///---------------------------------------------------------------------------------------///



function SetEquipmentCatAddNew() 
{
    AddNewSetEquipmentCat = true;   
	Ext.get('txtKode_SetEquipmentCat').dom.value = '';
	Ext.get('txtSetEquipmentCat').dom.value = '';
	rowSelectedSetEquipmentCat   =undefined;
	dsDTLTRSetEquipmentCatList.removeAll();
	dsDTLAddFieldPartSetEquipmentCatList.removeAll();
	selectTypeCatEqp='';
	selectCategorySetCat='';
	Ext.get('cboTypeCatEqp').dom.value = '';
	Ext.get('cboCategorySetCat').dom.value = '';
};
///---------------------------------------------------------------------------------------///


function getParamSetEquipmentCat() 
{
    var params =
	{	
		Table: 'ViewSetupCategory',   
	    Id: Ext.get('txtKode_SetEquipmentCat').getValue(),
	    Category: Ext.get('txtSetEquipmentCat').getValue(),
		Type:selectTypeCatEqp,
		Parent:selectCategorySetCat,
		ParentName:Ext.get('cboCategorySetCat').getValue(),
		ListPart:getArrPartCatList(),
		JmlListPart:dsDTLAddFieldPartSetEquipmentCatList.getCount(),
		List:getArrDetailFieldSetEquipmentCatList(),		
		JmlField: mRecordFieldSetCat.prototype.fields.length,
		JmlList:dsDTLTRSetEquipmentCatList.getCount(),
		Hapus:1
	};
    return params
};

function getArrPartCatList()
{
	var x='';
	for(var i = 0 ; i < dsDTLAddFieldPartSetEquipmentCatList.getCount();i++)
	{
		if (dsDTLAddFieldPartSetEquipmentCatList.data.items[i].data.PART_ID != '' )
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'PART_ID=' + dsDTLAddFieldPartSetEquipmentCatList.data.items[i].data.PART_ID
			y += z + 'CATEGORY_ID='+ Ext.get('txtKode_SetEquipmentCat').getValue()		
			
			if (i === (dsDTLAddFieldPartSetEquipmentCatList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getArrDetailFieldSetEquipmentCatList()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRSetEquipmentCatList.getCount();i++)
	{
		if (dsDTLTRSetEquipmentCatList.data.items[i].data.ADDFIELD != '' && dsDTLTRSetEquipmentCatList.data.items[i].data.TYPE_FIELD != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'ROW_ADD=' + dsDTLTRSetEquipmentCatList.data.items[i].data.ROW_ADD

			y += z + 'CATEGORY_ID='+dsDTLTRSetEquipmentCatList.data.items[i].data.CATEGORY_ID
			y += z + 'ADDFIELD='+dsDTLTRSetEquipmentCatList.data.items[i].data.ADDFIELD
			y += z + 'TYPE_FIELD='+dsDTLTRSetEquipmentCatList.data.items[i].data.TYPE_FIELD
			y += z + 'TYPE_FIELD_NAME='+dsDTLTRSetEquipmentCatList.data.items[i].data.TYPEFIELD
			y += z + 'LENGHT='+dsDTLTRSetEquipmentCatList.data.items[i].data.LENGHT
			
			if (i === (dsDTLTRSetEquipmentCatList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function getParamDeleteSetEquipmentCat() 
{
    var params =
	{	
		Table: 'ViewSetupCategory',   
	    Id: Ext.get('txtKode_SetEquipmentCat').getValue(),
		RowAdd: CurrentListFieldSetCat.data.data.ROW_ADD,
		Hapus:2
	};
    return params
};

function getParamDeletePartCat() 
{
    var params =
	{	
		Table: 'ViewSetupCategory',   
	    Id: Ext.get('txtKode_SetEquipmentCat').getValue(),
		Part_Id: CurrentListPartCat.data.data.PART_ID,
		Hapus:3
	};
    return params
};

function RefreshDataSetEquipmentCat(str)
{	
  if (str === undefined)
  {
	str='';
  };
	dsSetEquipmentCatList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountCatEqp, 
				Sort: 'category_id', 
				Sortdir: 'ASC', 
				target:'ViewSetupCategory',
				//param: 'where type = ~D~ ' + str
                                param: 'type = ~D~ ' + str
			} 
		}
	);
	rowSelectedSetEquipmentCat = undefined;
	return dsSetEquipmentCatList;
};

function RefreshDataAddFieldSetEquipmentCat(x)
{	
	dsDTLTRSetEquipmentCatList.load
	(
            {
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'row_add',
                    Sortdir: 'ASC',
                    target:'ViewSetupCategoryAddField',
                    //param: 'where category_id = ~' + x + '~'
                    param: 'category_id = ~' + x + '~'
                }
            }
	);
	cellSelectListFieldSetEquipmentCat = undefined;
	return dsDTLTRSetEquipmentCatList;
};

function RefreshDataSetEquipmentCatFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetEquipmentCatFilter').getValue() != '')
    { 
		//KataKunci = ' where category_id like ~%' + Ext.get('txtKDSetEquipmentCatFilter').getValue() + '%~';
                KataKunci = ' category_id like ~%' + Ext.get('txtKDSetEquipmentCatFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetEquipmentCatFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where category_name like  ~%' + Ext.get('txtSetEquipmentCatFilter').getValue() + '%~';
                        KataKunci = ' category_name like  ~%' + Ext.get('txtSetEquipmentCatFilter').getValue() + '%~';
		}
		else
		{
                        KataKunci += ' and  category_name  like ~%' + Ext.get('txtSetEquipmentCatFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetEquipmentCatList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountCatEqp, 
					Sort: 'category_id', 
					Sortdir: 'ASC', 
					target:'ViewSetupCategory',
					param: KataKunci
				} 
			}
		);    
    }
	else
	{
		RefreshDataSetEquipmentCat();
	}
};



function mComboMaksDataCatEqp()
{
  var cboMaksDataCatEqp = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataCatEqp',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' : ',			
			width:40,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountCatEqp,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountCatEqp=b.data.displayText ;
					RefreshDataSetEquipmentCat();
				} 
			}
		}
	);
	return cboMaksDataCatEqp;
};
 
 
function RecordBaruAddFieldSetCat()
{
	var p = new mRecordFieldSetCat
	(
		{
			'CATEGORY_ID':'',
			'ROW_ADD':'',
			'ADDFIELD':'',
			'TYPE_FIELD':1,
			'TYPEFIELD':'Text',
			'LENGHT':0
		}
	);
	
	return p;
};


function RefreshDataAddFieldMetering(cat_id)
{	
	dsDTLMeteringSetEquipmentCatList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'category_id', 
				Sortdir: 'ASC', 
				target:'viServiceCategory',
				//param: 'WHERE CATEGORY_ID=~' + cat_id + '~'
                                param: 'category_id = ~' + cat_id + '~'
			} 
		}
	);
	
	return dsDTLMeteringSetEquipmentCatList;
};


function mComboCategorySetCat(lebar)
{
	var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	dsCategorySetupCat = new WebApp.DataStore({ fields: Field });
	
	Ext.TreeComboSetEqpCat = Ext.extend
	(
		Ext.form.ComboBox, 
		{
			initList: function() 
			{
				treeSetEquipmentCatEntry = new Ext.tree.TreePanel
				(
					{
						loader: new Ext.tree.TreeLoader(),
						floating: true,
						autoHeight: true,
						listeners: 
						{
							click: this.onNodeClick,
							scope: this
						},
						alignTo: function(el, pos) 
						{
							this.setPagePosition(this.el.getAlignToXY(el, pos));
						}
					}
				);
			},
			expand: function() 
			{
				rootTreeSetEquipmentCatEntry  = new Ext.tree.AsyncTreeNode
				(
					{
						expanded: true,
						text:nmTreeComboParent,
						children: StrTreeComboSetEquipmentCat,
						autoScroll: true
					}
				) 
				treeSetEquipmentCatEntry.setRootNode(rootTreeSetEquipmentCatEntry); 
				
				this.list = treeSetEquipmentCatEntry
				if (!this.list.rendered) 
				{
					this.list.render(document.body);
					this.list.setWidth(this.el.getWidth() + 150);
					this.innerList = this.list.body;
					this.list.hide();
				}
				this.el.focus();
				Ext.TreeComboSetEqpCat.superclass.expand.apply(this, arguments);
			},
			doQuery: function(q, forceAll)
			{
				this.expand();
			},
			collapseIf : function(e)
			{
				this.list = treeSetEquipmentCatEntry
				if(!e.within(this.wrap) && !e.within(this.list.el))
				{
					this.collapse();
				}
			},
			onNodeClick: function(node, e) 
			{
				// if (node.attributes.type === 'D')
				// {
					this.setRawValue(node.attributes.text);
					selectCategorySetCat = node.attributes.id
				//};//
				// else
				// {
					// this.setRawValue('');
				// };
				
				if (this.hiddenField) 
				{
					this.hiddenField.value = node.id;
				}
				this.collapse();
			},
			store: dsCategorySetupCat
		}
	);
	
	
	
	var cboCategorySetCat = new Ext.TreeComboSetEqpCat
	(
		{
			id:'cboCategorySetCat',
			fieldLabel: nmParentSetEqpCat + ' ',
			anchor:'70%'
		}
	);
	
	return cboCategorySetCat;
	
	// var Field = ['CATEGORY_ID', 'CATEGORY_NAME'];
	// dsCategorySetupCat = new WebApp.DataStore({ fields: Field });

	// loadCategory();
	
  // var cboCategorySetCat = new Ext.form.ComboBox
	// (
		// {
			// id:'cboCategorySetCat',
			// typeAhead: true,
			// triggerAction: 'all',
			// lazyRender:true,
			// mode: 'local',
			// emptyText: 'Choose Parent...',
			// fieldLabel: 'Parent ',			
			// anchor:'70%',
			// store: dsCategorySetupCat,
			// valueField: 'CATEGORY_ID',
			// displayField: 'CATEGORY_NAME',
			// listeners:  
			// {
				// 'select': function(a,b,c)
				// {   
					// selectCategorySetCat=b.data.CATEGORY_ID ;					
				// } 
			// }
		// }
	// );
	
	// loadCategory();
	
	// return cboCategorySetCat;
};

function loadCategory()
{
	// dsCategorySetupCat.load
	// (
		// {
		    // params:
			// {
			    // Skip: 0,
			    // Take: 1000,
			    // Sort: 'CATEGORY_NAME',
			    // Sortdir: 'ASC',
			    // target: 'ViewComboCategoryVw',
			    // param: ' WHERE CATEGORY_ID<> ~ 9999~'
			// }
		// }
	// );
	// return dsCategorySetupCat;
};

function mComboTypeCatEqp()
{
  var cboTypeCatEqp = new Ext.form.ComboBox
	(
		{
			id:'cboTypeCatEqp',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmTypeSetEqpCat + ' ',	
			width:80,
			store: new Ext.data.ArrayStore
			(
				{
					id:'cboTypeCatEqp2',
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [['G', 'GROUP'],['D', 'DETAIL']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',			
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectTypeCatEqp=b.data.Id ;					
				} 
			}
		}
	);
	return cboTypeCatEqp;
};

function GetStrTreeComboSetEquipmentCat()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetDataTreeComboSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeComboSetEquipmentCat= cst.ListDataObj;
			},
			failure:function()
			{
			    
			}
		}
	);
};

function GetRecordBaruPartCategory()
{
	var p = new mRecordPartCategory
	(
		{			
			PART_ID:'',
			PART_NAME :'',
			TAG:''
		}
	);
	return p;
};

function RefreshDataPartCat(x)
{	
	dsDTLAddFieldPartSetEquipmentCatList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PART_ID',
                                Sort: 'part_id',
				Sortdir: 'ASC', 
				target:'viPartCategory',
				//param: 'where category_id = ~' + x + '~'
                                param: 'category_id = ~' + x + '~'
			} 
		}
	);
	cellSelectListPartCat = undefined;
	return dsDTLAddFieldPartSetEquipmentCatList;
};

function GetStrListPartCat()
{
	var str='';
	
	for (var i = 0; i < dsDTLAddFieldPartSetEquipmentCatList.getCount() ; i++) 
	{
		if (dsDTLAddFieldPartSetEquipmentCatList.data.items[i].data.PART_ID != '')
		{
			if (i === dsDTLAddFieldPartSetEquipmentCatList.getCount() -1 || dsDTLAddFieldPartSetEquipmentCatList.getCount() === 1)
			{
				
				str +=  '\'' + dsDTLAddFieldPartSetEquipmentCatList.data.items[i].data.PART_ID + '\'' ;
				
			}
			else
			{
				
				str +=  '\'' + dsDTLAddFieldPartSetEquipmentCatList.data.items[i].data.PART_ID + '\'' + ',' ;
				
			};
		};
	};

	return str;
};