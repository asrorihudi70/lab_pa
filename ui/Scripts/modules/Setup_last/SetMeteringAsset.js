var CurrentListMeteringSetMeteringAsset =
{
    data: Object,
    details: Array,
    row: 0
};

var strCatId;
var strAssetID;
var dsSetMeteringAssetList;
var AddNewSetMeteringAsset;
var SetMeteringAssetLookUps;
var selectSatuanSetMeteringAsset;
var IdServiceSetMeteringAsset;
var cellSelectMeteringSetMeteringAsset;

function SetMeteringAssetLookUp(cat_id,Asset_Id,Asset_Name) 
{
	var strNamaAsetSetMeteringAsset = 'Metering'
	if(Asset_Name != '')
	{
		strNamaAsetSetMeteringAsset= 'Metering ( ' + Asset_Name + ' )';
	};
	
	IdServiceSetMeteringAsset='';
	var lebar=800;
    SetMeteringAssetLookUps = new Ext.Window   	
    (
		{
		    id: 'SetMeteringAssetLookUps',
		    title: strNamaAsetSetMeteringAsset,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 400,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupMeteringAsset',
		    modal: true,
		    items: getFormEntrySetMeteringAsset(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					SetMeteringAssetLookUps=undefined;
				}
            }
		}
	);


    SetMeteringAssetLookUps.show();
	// if (rowdata == undefined)
	// {
		// SetMeteringAssetAddNew();
	// }
	// else
	// {
		// SetMeteringAssetInit(rowdata)
    // }	;
	strAssetID=Asset_Id;
    strCatId = cat_id;
	RefreshDataSetMeteringAsset(cat_id);
};


function getFormEntrySetMeteringAsset(lebar) 
{
    var pnlSetMeteringAsset = new Ext.FormPanel
    (
		{
		    id: 'PanelSetMeteringAsset',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 365,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 10px 10px 10px',
		    iconCls: 'SetupMeteringAsset',
		    border: false,
		    items:
			[GetDTLMeteringSetMateringAsset()],
		    tbar:
			[
				{
				    id: 'btnAddSetMeteringAsset',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetMeteringAssetAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetMeteringAsset',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetMeteringAssetSave(false);
						RefreshDataSetMeteringAsset(strCatId);
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetMeteringAsset',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetMeteringAssetSave(true);
						RefreshDataSetMeteringAsset(strCatId);
						if (x===undefined)
						{
							SetMeteringAssetLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetMeteringAsset',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetMeteringAssetDelete() ;
							RefreshDataSetMeteringAsset(strCatId);					
					}
				},'-','->','-',
				{
					id:'btnPrintSetMeteringAsset',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {}
				}
			]
		}
	); 
	
	var FormTRSetMeteringAsset = new Ext.Panel
	(
		{
		    id: 'FormTRSetMeteringAsset',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlSetMeteringAsset
			]
		}
	);

    return FormTRSetMeteringAsset;
};

function GetDTLMeteringSetMateringAsset() 
{
    var fldDetail = ['SERVICE_ID','SERVICE_NAME','CURRENT','INTERVAL','UNIT','TARGET_METERING','STATUS','HISTORY','SCHEDULE','CURRENT_METERING','START_DATE','TGL_PENDEKATAN','PATH_LEGEND','LAST_METERING_SERVICE','TYPE_SERVICE_ID'];
	
    dsSetMeteringAssetList = new WebApp.DataStore({ fields: fldDetail })

    var gridMeteringSetMateringAsset = new Ext.grid.EditorGridPanel
	(
		{
		    title: '',
		    stripeRows: true,
		    store: dsSetMeteringAssetList,
		    border: true,
		    columnLines: true,
		    frame: false,
		    //anchor: '100%',
			width:766,
			height:320,
			autoScroll:true,
		    sm: new Ext.grid.CellSelectionModel
			(
				{
				    singleSelect: true,
				    listeners:
					{
					    cellselect: function(sm, row, rec) 
						{
					        cellSelectMeteringSetMeteringAsset = dsSetMeteringAssetList.getAt(row);
					        CurrentListMeteringSetMeteringAsset.row = row;
					        CurrentListMeteringSetMeteringAsset.data = cellSelectMeteringSetMeteringAsset;
					    }
					}
				}
			),
		    cm: MeteringSetMeteringAssetColumModel(),
			// tbar: 
			// [
				// // {
					// // id:'btnTambahBrsSetMeteringAsset',
					// // // text: nmTambahBaris,
					// // // tooltip: nmTambahBaris,
					// // text: 'Edit Row',
					// // tooltip: 'Edit Row',
					// // iconCls: 'AddRow',
					// // handler: function() 
					// // { 
						// // //TambahBarisAddFieldSetCat();
					// // }
				// // }
				// // ,
				// // '-',
				// // {
					// // id:'btnHpsBrsSetMeteringAsset',
					// // text: nmHapusBaris,
					// // tooltip: nmHapusBaris,
					// // iconCls: 'RemoveRow',
					// // handler: function()
					// // {
						
					// // }
				// // },' ','-'
			// ] ,
			 viewConfig: { forceFit: true }
		}
	);

    return gridMeteringSetMateringAsset;
};

function MeteringSetMeteringAssetColumModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colServiceSetMeteringAsset',
				header: 'Service',
				dataIndex: 'SERVICE_NAME',
				width:150,
				editor: new Ext.form.TextField
				(
					{
						id:'fieldcolServiceSetMeteringAsset',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{
						}
					}
				)
			},
			{
			   header: 'Start Date',
			   dataIndex: 'START_DATE',
			   width: 95,
			   renderer: function(v, params, record) 
				{
					return ShowDate(record.data.START_DATE);
				},
			   editor: new Ext.form.DateField({
					format: 'm/d/y',
					minValue: '01/01/10'
					// disabledDays: [0, 6],
					// disabledDaysText: 'Plants are not available on the weekends'
				})
			},
			// {
				// id: 'colStartDateSetMeteringAsset',
				// header: 'Start Date',
				// dataIndex: 'START_DATE',
				// width:100,
				// renderer: function(v, params, record) 
						// {
					        // return ShowDate(record.data.START_DATE);
					    // },
				// editor: new Ext.form.DateField
				// (
					// {
						// format: 'm/d/y',
						// //minValue: '01/01/10',						
						// listener: 
						// {
							// change: function(field, vnew, vold)
							// {
								// field=vnew.format('ymd');							
							// }
						// }					
					// }
				// )
			// },
			{
				id: 'colLastServiceMetering',
				header: 'Last Metering Service',
				dataIndex: 'LAST_METERING_SERVICE',
				width:100
				,
				editor: new Ext.form.TextField
				(
					{						
						allowBlank: false						
					}
				)
			},{
				id: 'colCurrentSetMeteringAsset',
				header: 'Current Log',
				dataIndex: 'CURRENT_METERING',
				width:100
				// ,
				// editor: new Ext.form.TextField
				// (
					// {						
						// allowBlank: false						
					// }
				// )
			},
			{
				id: 'colIntervalSetMeteringAsset',
				header: 'Interval',
				dataIndex: 'INTERVAL',
				width:100				
			},
			{
				id: 'colSatuanSetMeteringAsset',
				header: 'Unit',
				dataIndex: 'UNIT',
				width:100				
			},
			{
				id: 'colTargetSetMeteringAsset',
				header: 'Target',
				dataIndex: 'TARGET_METERING',
				width:100,
				editor: new Ext.form.TextField
				(
					{						
						allowBlank: false						
					}
				)
			},
			{
				id: 'colTglPendekatanMeteringAsset',
				header: 'Next Service',
				dataIndex: 'TGL_PENDEKATAN',
				width:100,
				renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.TGL_PENDEKATAN);
					    }
			},
			{
				id: 'colStatusSetMeteringAsset',
				header: 'Status',
				dataIndex: 'PATH_LEGEND',
				sortable: true,
				width: 50,
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store) 
				{
					if (value != null)
					{
						return '<img src="' + value + '" class="text-desc-legend"/>'
					}else
					{
						return '';  
					}
					 // switch (value) 
					 // {
						 // case 1:  
							 // metaData.css = 'StatusHijau';
							 // break;
						 // case 2:   
							 // metaData.css = 'StatusKuning';
							 // break;
						 // case 3:  
							// metaData.css = 'StatusMerah';
							
							 // break;
					 // }
						// return '';     
				}
			}
			// ,
			// {
				// id: 'colHistorySetMeteringAsset',
				// header: 'History',
				// dataIndex: 'HISTORY',
				// width:100,
				// editor: new Ext.form.TextField
				// (
					// {
						// id:'fieldcolHistorySetMeteringAsset',
						// allowBlank: false,
						// enableKeyEvents : true,
						// listeners: 
						// {
						// }
					// }
				// )
			// },
			// {
				// id: 'colScheduleSetMeteringAsset',
				// header: 'Schedule',
				// dataIndex: 'SCHEDULE',
				// width:100,
				// editor: new Ext.form.TextField
				// (
					// {
						// id:'fieldcolScheduleSetMeteringAsset',
						// allowBlank: false,
						// enableKeyEvents : true,
						// listeners: 
						// {
						// }
					// }
				// )
			// }
		]
	)
};

function SetMeteringAssetSave(mBol) 
{
	if (ValidasiEntrySetMeteringAsset(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetMeteringAsset == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetMeteringAsset(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetMeteringAsset(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetMeteringAsset(strCatId);
							
							AddNewSetMeteringAsset = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetMeteringAsset(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetMeteringAsset(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetMeteringAsset(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetMeteringAsset(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetMeteringAsset(strCatId);
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetMeteringAsset(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetMeteringAsset(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};



function SetMeteringAssetDelete() 
{
	if (ValidasiEntrySetMeteringAsset(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmMeteringAsset2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetMeteringAsset(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetMeteringAsset(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetMeteringAsset(strCatId);
										SetMeteringAssetAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetMeteringAsset(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetMeteringAsset(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntrySetMeteringAsset(modul,mBolHapus)
{
	var x = 1;
	// if (Ext.get('txtKode_SetMeteringAsset').getValue() == '' || Ext.get('txtNameSetMeteringAsset').getValue() == '')
	// {
		// if (Ext.get('txtKode_SetMeteringAsset').getValue() == '' && mBolHapus === true)
		// {
			// //ShowPesanWarningSetMeteringAsset('Kode sumber dana belum di isi',modul);
			// x=0;
		// }
		// else if (Ext.get('txtNameSetMeteringAsset').getValue() == '')
		// {
			// x=0;
			// if ( mBolHapus === false )
			// {
				// ShowPesanWarningSetMeteringAsset(nmGetValidasiKosong(nmMeteringAsset),modul);
			// };
		// };
	// };
	return x;
};

function ShowPesanWarningSetMeteringAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorSetMeteringAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoSetMeteringAsset(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetMeteringAssetInit(rowdata) 
{
    AddNewSetMeteringAsset = false;
    
};
///---------------------------------------------------------------------------------------///



function SetMeteringAssetAddNew() 
{
    AddNewSetMeteringAsset = true;   
	
	rowSelectedSetMeteringAsset   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetMeteringAsset() 
{
    var params =
	{	
		Table: 'viAssetService',  
		List:getArrAssetService(),
		JmlList:GetListCountAssetService()
	  
	};
    return params
};

function GetListCountAssetService()
{
	
	var x=0;
	for(var i = 0 ; i < dsSetMeteringAssetList.getCount();i++)
	{
		if (dsSetMeteringAssetList.data.items[i].data.SERVICE_ID != '' )
		{
			x += 1;
		};
	}
	return x;
	
};

function getArrAssetService()
{
	var x='';
	for(var i = 0 ; i < dsSetMeteringAssetList.getCount();i++)
	{
		if (dsSetMeteringAssetList.data.items[i].data.SERVICE_ID != '')
		{
			var y='';
			var z='@@##$$@@';
			y = 'ASSET_MAINT_ID=' + strAssetID
			y += z +'CATEGORY_ID=' + strCatId
			y += z + 'SERVICE_ID='+dsSetMeteringAssetList.data.items[i].data.SERVICE_ID
			y += z + 'CURRENT_METERING='+dsSetMeteringAssetList.data.items[i].data.CURRENT_METERING		
			y += z + 'TARGET_METERING='+dsSetMeteringAssetList.data.items[i].data.TARGET_METERING
			y += z + 'START_DATE='+ShowDate(dsSetMeteringAssetList.data.items[i].data.START_DATE)
			y += z + 'LAST_METERING_SERVICE='+dsSetMeteringAssetList.data.items[i].data.LAST_METERING_SERVICE
			y += z + 'TYPE_SERVICE_ID='+dsSetMeteringAssetList.data.items[i].data.TYPE_SERVICE_ID
			if (i === (dsSetMeteringAssetList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function RefreshDataSetMeteringAsset(cat_id)
{	
	var cKriteria;
    cKriteria = cat_id + '###1###';
	cKriteria +=strAssetID;
	
	dsSetMeteringAssetList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'meteringasset_id',
				Sortdir: 'ASC', 
				target:'viAssetService',
				param: cKriteria
			} 
		}
	);
	rowSelectedSetMeteringAsset = undefined;
	return dsSetMeteringAssetList;
};


 
