
var dsSetPriorityList;
var AddNewSetPriority;
var selectCountSetPriority=50;
var rowSelectedSetPriority;
var SetPriorityLookUps;
CurrentPage.page = getPanelSetPriority(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetPriority(mod_id) 
{
	
    var Field = ['KD_PRIORITY', 'PRIORITY'];
    dsSetPriorityList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetPriority();

    var grListSetPriority = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetPriority',
		    stripeRows: true,
		    store: dsSetPriorityList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetPriority=undefined;
							rowSelectedSetPriority = dsSetPriorityList.getAt(row);
						}
					}
				}
			),
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetPriority',
                        header: "Code",                      
                        dataIndex: 'KD_PRIORITY',
                        sortable: true,
                        width: 30
                    },
					{
					    id: 'colSetPriority',
					    header: "Priority",					   
					    dataIndex: 'PRIORITY',
					    width: 200,
					    sortable: true
					}
                ]
			),
		    tbar:
			[
				{
				    id: 'btnEditSetPriority',
				    text: '  Edit Data',
				    tooltip: 'Edit Data',
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetPriority != undefined)
						{
						    SetPriorityLookUp(rowSelectedSetPriority.data);
						}
						else
						{
						    SetPriorityLookUp();
						}
				    }
				},' ','-'
			],
		    viewConfig: { forceFit: true }
		}
	);


    var FormSetPriority = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    Priority: 'center',
		    layout: 'form',
		    title: 'Priority',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupPriority',
		    items: [grListSetPriority],
		    tbar:
			[
				'Code : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Code : ',
					id: 'txtKDSetPriorityFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				'Priority : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Priority : ',
					id: 'txtSetPriorityFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				'Maks.Data : ', ' ',mComboMaksDataSetPriority(),
				' ','-',
				{
				    id: 'btnRefreshSetPriority',
				    text: 'Refresh',
				    tooltip: 'Refresh',
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetPriorityFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetPriority();
				}
			}
		}
	);
    //END var FormSetPriority--------------------------------------------------

	RefreshDataSetPriority();
    return FormSetPriority
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetPriorityLookUp(rowdata) 
{
	var lebar=600;
    SetPriorityLookUps = new Ext.Window   	
    (
		{
		    id: 'SetPriorityLookUps',
		    title: 'Priority',
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupPriority',
		    modal: true,
		    items: getFormEntrySetPriority(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetPriority=undefined;
					RefreshDataSetPriority();
				}
            }
		}
	);


    SetPriorityLookUps.show();
	if (rowdata == undefined)
	{
		SetPriorityAddNew();
	}
	else
	{
		SetPriorityInit(rowdata)
	}	
};


function getFormEntrySetPriority(lebar) 
{
    var pnlSetPriority = new Ext.FormPanel
    (
		{
		    id: 'PanelSetPriority',
		    fileUpload: true,
		    Priority: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupPriority',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetPriority',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: 'Code ',
								    name: 'txtKode_SetPriority',
								    id: 'txtKode_SetPriority',
								    anchor: '40%'
								},
								{
								    xtype: 'textfield',
								    fieldLabel: 'Priority ',
								    name: 'txtSetPriority',
								    id: 'txtSetPriority',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetPriority',
				    text: 'Tambah',
				    tooltip: 'Tambah Record Baru ',
				    iconCls: 'add',				   
				    handler: function() { SetPriorityAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetPriority',
				    text: 'Simpan',
				    tooltip: 'Rekam Data ',
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetPrioritySave(false);
						RefreshDataSetPriority();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetPriority',
				    text: 'Simpan & Keluar',
				    tooltip: 'Simpan dan Keluar',
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetPrioritySave(true);
						RefreshDataSetPriority();
						if (x===undefined)
						{
							SetPriorityLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetPriority',
				    text: 'Hapus',
				    tooltip: 'Remove the selected item',
				    iconCls: 'remove',
				    handler: function() 
					{
							SetPriorityDelete() ;
							RefreshDataSetPriority();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetPriority',
					text: ' Cetak',
					tooltip: 'Cetak',
					iconCls: 'print',					
					handler: function() {LoadReport(231032);}
				}
			]
		}
	); 

    return pnlSetPriority
};



function SetPrioritySave(mBol) 
{
	if (ValidasiEntrySetPriority('Simpan Data') == 1 )
	{
		if (AddNewSetPriority == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetPriority(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetPriority('Data berhasil di simpan','Simpan Data');
							RefreshDataSetPriority();
							if(mBol === false)
							{
								Ext.get('txtKode_SetPriority').dom.readOnly=true;
							};
							AddNewSetPriority = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetPriority('Data tidak berhasil di simpan, data tersebut sudah ada','Simpan Data');
						}
						else 
						{
							ShowPesanErrorSetPriority('Data tidak berhasil di simpan','Simpan Data');
						}
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetPriority(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetPriority('Data berhasil di edit','Edit Data');
							RefreshDataSetPriority();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetPriority('Data tidak berhasil di edit, data tersebut belum ada','Edit Data');
						}
						else {
							ShowPesanErrorSetPriority('Data tidak berhasil di edit','Edit Data');
						}
					}
				}
			)
		}
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetPrioritySave
///---------------------------------------------------------------------------------------///

function SetPriorityDelete() 
{
	if (ValidasiEntrySetPriority('Hapus Data') == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: WebAppUrl.UrlDeleteData,
				params: getParamSetPriority(),
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetPriority('Data berhasil di hapus','Hapus Data');
						RefreshDataSetPriority();
						SetPriorityAddNew();
					}
					else if (cst.success === false && cst.pesan===0)
					{
						ShowPesanWarningSetPriority('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else {
						ShowPesanErrorSetPriority('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function ValidasiEntrySetPriority(modul)
{
	var x = 1;
	if (Ext.get('txtKode_SetPriority').getValue() == '' || Ext.get('txtSetPriority').getValue() == '')
	{
		if (Ext.get('txtKode_SetPriority').getValue() == '')
		{
			ShowPesanWarningSetPriority('Kode sumber dana belum di isi',modul);
			x=0;
		}
		else
		{
			ShowPesanWarningSetPriority('Sumber dana belum di isi',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarningSetPriority(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanErrorSetPriority(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfoSetPriority(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

//------------------------------------------------------------------------------------
function SetPriorityInit(rowdata) 
{
    AddNewSetPriority = false;
    Ext.get('txtKode_SetPriority').dom.value = rowdata.PRIORITY;
    Ext.get('txtSetPriority').dom.value = rowdata.PRIORITY;
	Ext.get('txtKode_SetPriority').dom.readOnly=true;

};
///---------------------------------------------------------------------------------------///



function SetPriorityAddNew() 
{
    AddNewSetPriority = true;   
	Ext.get('txtKode_SetPriority').dom.value = '';
	Ext.get('txtSetPriority').dom.value = '';
	rowSelectedSetPriority   =undefined;
	Ext.get('txtKode_SetPriority').dom.readOnly=false;
};
///---------------------------------------------------------------------------------------///


function getParamSetPriority() 
{
    var params =
	{	
		Table: 'acc_sumber_dana',   
	    Kd_Sumber_Dana: Ext.get('txtKode_SetPriority').getValue(),
	    Sumber_Dana: Ext.get('txtSetPriority').getValue()	
	};
    return params
};


function RefreshDataSetPriority()
{	
	// dsSetPriorityList.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: selectCountSetPriority, 
				// Sort: 'Kd_Sumber_Dana', 
				// Sortdir: 'ASC', 
				// target:'acc_sumber_dana',
				// param: ''
			// } 
		// }
	// );
	// rowSelectedSetPriority = undefined;
	// return dsSetPriorityList;
};

function RefreshDataSetPriorityFilter() 
{   
	// var KataKunci;
    // if (Ext.get('txtKDSetPriorityFilter').getValue() != '')
    // { 
		// KataKunci = 'kode@ Kd_Sumber_Dana =' + Ext.get('txtKDSetPriorityFilter').getValue(); 
	// }
    // if (Ext.get('txtSetPriorityFilter').getValue() != '')
    // { 
		// if (KataKunci == undefined)
		// {
			// KataKunci = 'nama@ Sumber_Dana =' + Ext.get('txtSetPriorityFilter').getValue();
		// }
		// else
		// {
			// KataKunci += '##@@##nama@ Sumber_Dana =' + Ext.get('txtSetPriorityFilter').getValue();
		// }  
	// }
        
    if (KataKunci != undefined) 
    {  
		// dsSetPriorityList.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCountSetPriority, 
					// Sort: 'Kd_Sumber_Dana', 
					// Sortdir: 'ASC', 
					// target:'acc_sumber_dana',
					// param: KataKunci
				// }			
			// }
		// );        
    }
	else
	{
		RefreshDataSetPriority();
	}
};



function mComboMaksDataSetPriority()
{
  var cboMaksDataSetPriority = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetPriority',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetPriority,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetPriority=b.data.displayText ;
					RefreshDataSetPriority();
				} 
			}
		}
	);
	return cboMaksDataSetPriority;
};
 
