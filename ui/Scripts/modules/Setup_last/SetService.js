
var dsSetServiceList;
var AddNewSetService;
var selectCountSetService=50;
var rowSelectedSetService;
var SetServiceLookUps;
CurrentPage.page = getPanelSetService(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetService(mod_id) 
{
	
    var Field =['SERVICE_ID','SERVICE_NAME'];
    dsSetServiceList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetService();

    var grListSetService = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetService',
		    stripeRows: true,
		    store: dsSetServiceList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetService=undefined;
							rowSelectedSetService = dsSetServiceList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetService=undefined;
					rowSelectedSetService = dsSetServiceList.getAt(ridx);
					if (rowSelectedSetService != undefined)
					{
						SetServiceLookUp(rowSelectedSetService.data);
					}
					else
					{
						SetServiceLookUp();
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetService',
                        header: nmKdService2,                      
                        dataIndex: 'SERVICE_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetService',
					    header: nmService,					   
					    dataIndex: 'SERVICE_NAME',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetServiceList,
            pageSize: selectCountSetService,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetService',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetService != undefined)
						{
						    SetServiceLookUp(rowSelectedSetService.data);
						}
						else
						{
						    SetServiceLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetService = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormService,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupService',
		    items: [grListSetService],
		    tbar:
			[
				nmKdService2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'ID : ',
					id: 'txtKDSetServiceFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmService + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Service : ',
					id: 'txtSetServiceFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetService(),
				' ','-',
				{
				    id: 'btnRefreshSetService',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetServiceFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetService();
				}
			}
		}
	);
    //END var FormSetService--------------------------------------------------

	RefreshDataSetService();
    return FormSetService ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetServiceLookUp(rowdata) 
{
	var lebar=600;
    SetServiceLookUps = new Ext.Window   	
    (
		{
		    id: 'SetServiceLookUps',
		    title: nmFormService,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupService',
		    modal: true,
		    items: getFormEntrySetService(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetService=undefined;
					RefreshDataSetService();
				}
            }
		}
	);


    SetServiceLookUps.show();
	if (rowdata == undefined)
	{
		SetServiceAddNew();
	}
	else
	{
		SetServiceInit(rowdata)
	}	
};


function getFormEntrySetService(lebar) 
{
    var pnlSetService = new Ext.FormPanel
    (
		{
		    id: 'PanelSetService',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupService',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetService',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdService2 + ' ',
								    name: 'txtKode_SetService',
								    id: 'txtKode_SetService',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmService + ' ',
								    name: 'txtNameSetService',
								    id: 'txtNameSetService',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetService',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetServiceAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetService',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetServiceSave(false);
						RefreshDataSetService();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetService',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetServiceSave(true);
						RefreshDataSetService();
						if (x===undefined)
						{
							SetServiceLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetService',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetServiceDelete() ;
							RefreshDataSetService();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetService',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() 
					{
						LoadReport(950016);
					}
				}
			]
		}
	); 

    return pnlSetService
};


function SetServiceSave(mBol) 
{
	if (ValidasiEntrySetService(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetService == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetService(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetService(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetService();
							if(mBol === false)
							{
								Ext.get('txtKode_SetService').dom.value=cst.ServiceId;
							};
							AddNewSetService = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetService(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetService(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetService(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetService(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetService();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetService(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetService(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};//END FUNCTION SetServiceSave
///---------------------------------------------------------------------------------------///

function SetServiceDelete() 
{
	if (ValidasiEntrySetService(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmService) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetService(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetService(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetService();
										SetServiceAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetService(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetService(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetService(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetService').getValue() == '' || (Ext.get('txtNameSetService').getValue() == ''))
	{
		if (Ext.get('txtKode_SetService').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetService(nmGetValidasiKosong(nmKdService),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetService').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetService(nmGetValidasiKosong(nmService),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetService(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetService(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetService(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetServiceInit(rowdata) 
{
    AddNewSetService = false;
    Ext.get('txtKode_SetService').dom.value = rowdata.SERVICE_ID;
    Ext.get('txtNameSetService').dom.value = rowdata.SERVICE_NAME;
};
///---------------------------------------------------------------------------------------///



function SetServiceAddNew() 
{
    AddNewSetService = true;   
	Ext.get('txtKode_SetService').dom.value = '';
    Ext.get('txtNameSetService').dom.value = '';
	rowSelectedSetService   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetService() 
{
    var params =
	{	
		Table: 'ViewSetupService',   
	    ServiceId: Ext.get('txtKode_SetService').getValue(),
	    Service: Ext.get('txtNameSetService').getValue()	
	};
    return params
};


function RefreshDataSetService()
{	
	dsSetServiceList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetService, 
					//Sort: 'Service_ID',
                                        Sort: 'service_id',
					Sortdir: 'ASC', 
					target:'ViewSetupService',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetService = undefined;
	return dsSetServiceList;
};

function RefreshDataSetServiceFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetServiceFilter').getValue() != '')
    { 
		//KataKunci = ' where Service_ID like ~%' + Ext.get('txtKDSetServiceFilter').getValue() + '%~';
                KataKunci = ' service_id like ~%' + Ext.get('txtKDSetServiceFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetServiceFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where Service like  ~%' + Ext.get('txtSetServiceFilter').getValue() + '%~';
                        KataKunci = ' service like  ~%' + Ext.get('txtSetServiceFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  Service like  ~%' + Ext.get('txtSetServiceFilter').getValue() + '%~';
                        KataKunci += ' and  service like  ~%' + Ext.get('txtSetServiceFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetServiceList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetService, 
					//Sort: 'Service_ID',
                                        Sort: 'service_id',
					Sortdir: 'ASC', 
					target:'ViewSetupService',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetService();
	};
};



function mComboMaksDataSetService()
{
  var cboMaksDataSetService = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetService',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetService,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetService=b.data.displayText ;
					RefreshDataSetService();
				} 
			}
		}
	);
	return cboMaksDataSetService;
};
 
