
var dsSetServicePMTypeList;
var AddNewSetServicePMType;
var selectCountSetServicePMType=50;
var rowSelectedSetServicePMType;
var SetServicePMTypeLookUps;
CurrentPage.page = getPanelSetServicePMType(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetServicePMType(mod_id) 
{
	
    var Field = ['TYPE_SERVICE_ID','SERVICE_TYPE'];
    dsSetServicePMTypeList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetServicePMType();

    var grListSetServicePMType = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetServicePMType',
		    stripeRows: true,
		    store: dsSetServicePMTypeList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetServicePMType=undefined;
							rowSelectedSetServicePMType = dsSetServicePMTypeList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetServicePMType = dsSetServicePMTypeList.getAt(ridx);
					if (rowSelectedSetServicePMType != undefined)
					{
						SetServicePMTypeLookUp(rowSelectedSetServicePMType.data);
					}
					else
					{
						SetServicePMTypeLookUp();
					};
					
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetServicePMType',
                        header: nmKdTypeServicePM2,                      
                        dataIndex: 'TYPE_SERVICE_ID',
                        sortable: true,
                        width: 100
                    },
					{
					    id: 'colSetServicePMType',
					    header: nmTypeServicePM,					   
					    dataIndex: 'SERVICE_TYPE',
					    width: 300,
					    sortable: true
					}
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetServicePMTypeList,
            pageSize: selectCountSetServicePMType,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetServicePMType',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetServicePMType != undefined)
						{
						    SetServicePMTypeLookUp(rowSelectedSetServicePMType.data);
						}
						else
						{
						    SetServicePMTypeLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetServicePMType = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormTypeServicePM,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupServicePMType',
		    items: [grListSetServicePMType],
		    tbar:
			[
				nmKdTypeServicePM2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmKdTypeServicePM2 + ' : ',
					id: 'txtKDSetServicePMTypeFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmTypeServicePM + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmTypeServicePM + ' : ',
					id: 'txtSetServicePMTypeFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ','-',
				nmMaksData + ' : ', ' ',mComboMaksDataSetServicePMType(),
				' ','-',
				{
				    id: 'btnRefreshSetServicePMType',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetServicePMTypeFilter();
					}
				}
			]
		}
	);
    //END var FormSetServicePMType--------------------------------------------------

	RefreshDataSetServicePMType();
    return FormSetServicePMType ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetServicePMTypeLookUp(rowdata) 
{
	var lebar=600;
    SetServicePMTypeLookUps = new Ext.Window   	
    (
		{
		    id: 'SetServicePMTypeLookUps',
		    title: nmFormTypeServicePM,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupServicePMType',
		    modal: true,
		    items: getFormEntrySetServicePMType(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetServicePMType=undefined;
					RefreshDataSetServicePMType();
				}
            }
		}
	);


    SetServicePMTypeLookUps.show();
	if (rowdata == undefined)
	{
		SetServicePMTypeAddNew();
	}
	else
	{
		SetServicePMTypeInit(rowdata)
	};	
};


function getFormEntrySetServicePMType(lebar) 
{
    var pnlSetServicePMType = new Ext.FormPanel
    (
		{
		    id: 'PanelSetServicePMType',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupServicePMType',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetServicePMType',
							labelWidth:105,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdTypeServicePM2 + ' ',
								    name: 'txtKode_SetServicePMType',
								    id: 'txtKode_SetServicePMType',
								    anchor: '50%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmTypeServicePM + ' ',
								    name: 'txtNameSetServicePMType',
								    id: 'txtNameSetServicePMType',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetServicePMType',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() { SetServicePMTypeAddNew() }
				}, '-',
				{
				    id: 'btnSimpanSetServicePMType',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetServicePMTypeSave(false);
						RefreshDataSetServicePMType();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetServicePMType',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetServicePMTypeSave(true);
						RefreshDataSetServicePMType();
						if (x===undefined)
						{
							SetServicePMTypeLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetServicePMType',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetServicePMTypeDelete() ;
							RefreshDataSetServicePMType();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetServicePMType',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() {LoadReport(950013);}
				}
			]
		}
	); 

    return pnlSetServicePMType
};


function SetServicePMTypeSave(mBol) 
{
	if (ValidasiEntrySetServicePMType(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewSetServicePMType == true) 
		{
			Ext.Ajax.request
			(
				{
					url: WebAppUrl.UrlSaveData,
					params: getParamSetServicePMType(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetServicePMType(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetServicePMType();
							if(mBol === false)
							{
								Ext.get('txtKode_SetServicePMType').dom.value=cst.ServicePMTypeId;
							};
							AddNewSetServicePMType = false;

						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetServicePMType(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorSetServicePMType(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: WebAppUrl.UrlUpdateData,
					params: getParamSetServicePMType(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoSetServicePMType(nmPesanEditSukses,nmHeaderEditData);
							RefreshDataSetServicePMType();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningSetServicePMType(nmPesanEditGagal,nmHeaderEditData);
						}
						else 
						{
							ShowPesanErrorSetServicePMType(nmPesanEditError,nmHeaderEditData);
						};
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};//END FUNCTION SetServicePMTypeSave
///---------------------------------------------------------------------------------------///

function SetServicePMTypeDelete() 
{
	if (ValidasiEntrySetServicePMType(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmTypeServicePM2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetServicePMType(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetServicePMType(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetServicePMType();
										SetServicePMTypeAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetServicePMType(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorSetServicePMType(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
			   }
			}
		)
	};
};


function ValidasiEntrySetServicePMType(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetServicePMType').getValue() == '' || (Ext.get('txtNameSetServicePMType').getValue() == ''))
	{
		if (Ext.get('txtKode_SetServicePMType').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNameSetServicePMType').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetServicePMType(nmGetValidasiKosong(nmTypeServicePM),modul);
			};
		
		};
	};
	
	return x;
};

function ShowPesanWarningSetServicePMType(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetServicePMType(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetServicePMType(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetServicePMTypeInit(rowdata) 
{
    AddNewSetServicePMType = false;
    Ext.get('txtKode_SetServicePMType').dom.value = rowdata.TYPE_SERVICE_ID;
    Ext.get('txtNameSetServicePMType').dom.value = rowdata.SERVICE_TYPE;

};
///---------------------------------------------------------------------------------------///



function SetServicePMTypeAddNew() 
{
    AddNewSetServicePMType = true;   
	Ext.get('txtKode_SetServicePMType').dom.value = '';
    Ext.get('txtNameSetServicePMType').dom.value = '';
	rowSelectedSetServicePMType   = undefined;

};
///---------------------------------------------------------------------------------------///


function getParamSetServicePMType() 
{
    var params =
	{	
		Table: 'ViewSetupServicePMType',   
	    Id: Ext.get('txtKode_SetServicePMType').getValue(),
	    ServicePMType : Ext.get('txtNameSetServicePMType').getValue()	
	};
    return params
};


function RefreshDataSetServicePMType()
{	
	dsSetServicePMTypeList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountSetServicePMType, 
				//Sort: 'TYPE_SERVICE_ID',
                                Sort: 'type_service_id',
				Sortdir: 'ASC', 
				target:'ViewSetupServicePMType',
				param: ''
			} 
		}
	);
	rowSelectedSetServicePMType = undefined;
	return dsSetServicePMTypeList;
};

function RefreshDataSetServicePMTypeFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetServicePMTypeFilter').getValue() != '')
    { 
		//KataKunci = ' where TYPE_SERVICE_ID like ~%' + Ext.get('txtKDSetServicePMTypeFilter').getValue() + '%~';
                KataKunci = ' type_service_id like ~%' + Ext.get('txtKDSetServicePMTypeFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetServicePMTypeFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where SERVICE_TYPE like  ~%' + Ext.get('txtSetServicePMTypeFilter').getValue() + '%~';
                        KataKunci = ' service_type like  ~%' + Ext.get('txtSetServicePMTypeFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  SERVICE_TYPE like  ~%' + Ext.get('txtSetServicePMTypeFilter').getValue() + '%~';
                        KataKunci += ' and  service_type like  ~%' + Ext.get('txtSetServicePMTypeFilter').getValue() + '%~';
		};  
	};
	
        
    if (KataKunci != undefined) 
    {  
		dsSetServicePMTypeList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountSetServicePMType, 
					//Sort: 'TYPE_SERVICE_ID',
                                        Sort: 'type_service_id',
					Sortdir: 'ASC', 
					target:'ViewSetupServicePMType',
					param: KataKunci
				} 
			}
		);    
    }
	else
	{
		RefreshDataSetServicePMType();
	}
};



function mComboMaksDataSetServicePMType()
{
  var cboMaksDataSetServicePMType = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetServicePMType',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaksData + ' ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetServicePMType,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetServicePMType=b.data.displayText ;
					RefreshDataSetServicePMType();
				} 
			}
		}
	);
	return cboMaksDataSetServicePMType;
};
 
