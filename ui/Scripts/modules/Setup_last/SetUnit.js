
var dsSetUnitList;
var AddNewSetUnit;
var selectCountSetUnit=50;
var rowSelectedSetUnit;
var SetUnitLookUps;
CurrentPage.page = getPanelSetUnit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetUnit(mod_id) 
{
	
    var Field =['UNIT_ID','UNIT'];
    dsSetUnitList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetUnit();

    var grListSetUnit = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetUnit',
		    stripeRows: true,
		    store: dsSetUnitList,
			autoScroll: true,
		    columnLines: true,
			border:false,
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetUnit=undefined;
							rowSelectedSetUnit = dsSetUnitList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedSetUnit = dsSetUnitList.getAt(ridx);
					if (rowSelectedSetUnit != undefined)
					{
						SetUnitLookUp(rowSelectedSetUnit.data);
					}
					else
					{
						SetUnitLookUp();
					};
				}
			},
		    cm: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colKodeSetUnit',
                        header: nmKdSatuan2,                      
                        dataIndex: 'UNIT_ID',
                        sortable: true,
                        width: 100
                        
                    },
					{
					    id: 'colSetUnit',
					    header: nmSatuan,					   
					    dataIndex: 'UNIT',
					    width: 300,
					    sortable: true
                                        }
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetUnitList,
            pageSize: selectCountSetUnit,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetUnit',
				    text: nmEditData,
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetUnit != undefined)
						{
						    SetUnitLookUp(rowSelectedSetUnit.data);
						}
						else
						{
						    SetUnitLookUp();
						}
				    }
				},' ','-'
			]
		    ,viewConfig: { forceFit: true }
		}
	);


    var FormSetUnit = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmFormSatuan,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupUnit',
		    items: [],
		    tbar:
			[
				nmKdSatuan2 + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Id : ',
					id: 'txtKDSetUnitFilter',                   
					width:80,
					onInit: function() { }
				}, ' ','-',
				nmSatuan + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Unit : ',
					id: 'txtSetUnitFilter',                   
					anchor: '95%',
					onInit: function() { }
				}, ' ',
				{
				    id: 'btnRefreshSetUnit',
				    text: nmRefresh,
				    tooltip: nmRefresh,
				    iconCls: 'refresh',
				    handler: function(sm, row, rec) 
					{  
						RefreshDataSetUnitFilter();
					}
				}
			],
		    listeners:
			{ 'afterrender': function() 
				{   
					//Ext.getCmp('cboDESKRIPSI').store = getSetUnit();
				}
			}
		}
	);
    //END var FormSetUnit--------------------------------------------------

	RefreshDataSetUnit();
    return FormSetUnit ;
};
// end function get panel main data
///------------------------------------------------------------------------------------------------------------///




function SetUnitLookUp(rowdata) 
{
	var lebar=600;
    SetUnitLookUps = new Ext.Window   	
    (
		{
		    id: 'SetUnitLookUps',
		    title: nmFormSatuan,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 150,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'SetupUnit',
		    modal: true,
		    items: getFormEntrySetUnit(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedSetUnit=undefined;
					RefreshDataSetUnit();
				}
            }
		}
	);


    SetUnitLookUps.show();
	if (rowdata == undefined)
	{
		SetUnitAddNew();
	}
	else
	{
		SetUnitInit(rowdata)
	}	
};


function getFormEntrySetUnit(lebar) 
{
    var pnlSetUnit = new Ext.FormPanel
    (
		{
		    id: 'PanelSetUnit',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 120,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupUnit',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 70,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetUnit',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdSatuan2 + ' ',
								    name: 'txtKode_SetUnit',
								    id: 'txtKode_SetUnit',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmSatuan + ' ',
								    name: 'txtNameSetUnit',
								    id: 'txtNameSetUnit',
								    anchor: '100%'
								}
							]
						}
					]
				}
			],
		    tbar:
			[
				{
				    id: 'btnAddSetUnit',
				    text: nmTambah,
				    tooltip: nmTambah,
				    iconCls: 'add',				   
				    handler: function() 
					{ 
						SetUnitAddNew() 
					}
				}, '-',
				{
				    id: 'btnSimpanSetUnit',
				    text: nmSimpan,
				    tooltip: nmSimpan,
				    iconCls: 'save',				   
				    handler: function() 
					{ 
						SetUnitSave(false);
						RefreshDataSetUnit();
					}
				}, '-',
				{
				    id: 'btnSimpanCloseSetUnit',
				    text: nmSimpanKeluar,
				    tooltip: nmSimpanKeluar,
				    iconCls: 'saveexit',
				    handler: function() 
					{					
						var x = SetUnitSave(true);
						RefreshDataSetUnit();
						if (x===undefined)
						{
							SetUnitLookUps.close();
						};
					}
				},'-',
				{
				    id: 'btnHapusSetUnit',
				    text: nmHapus,
				    tooltip: nmHapus,
				    iconCls: 'remove',
				    handler: function() 
					{
							SetUnitDelete() ;
							RefreshDataSetUnit();					
					}
				},'-','->','-',
				{
					id:'btnPrintSetUnit',
					text: nmCetak,
					tooltip: nmCetak,
					iconCls: 'print',					
					handler: function() 
					{
						//LoadReport(950002);
					}
				}
			]
		}
	); 

    return pnlSetUnit
};


function SetUnitSave(mBol) 
{
    if (ValidasiEntrySetUnit(nmHeaderSimpanData,false) == 1 )
    {
            if (AddNewSetUnit == true)
            {
                    Ext.Ajax.request
                    (
                            {
                                    url: WebAppUrl.UrlSaveData,
                                    params: getParamSetUnit(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetUnit(nmPesanSimpanSukses,nmHeaderSimpanData);
                                                    RefreshDataSetUnit();
                                                    if(mBol === false)
                                                    {
                                                            Ext.get('txtKode_SetUnit').dom.value=cst.UnitId;
                                                    };
                                                    AddNewSetUnit = false;

                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetUnit(nmPesanSimpanGagal,nmHeaderSimpanData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetUnit(nmPesanSimpanError,nmHeaderSimpanData);
                                            };
                                    }
                            }
                    )
            }
            else
            {
                    Ext.Ajax.request
                     (
                            {
                                    url: WebAppUrl.UrlUpdateData,
                                    params: getParamSetUnit(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoSetUnit(nmPesanEditSukses,nmHeaderEditData);
                                                    RefreshDataSetUnit();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanWarningSetUnit(nmPesanEditGagal,nmHeaderEditData);
                                            }
                                            else
                                            {
                                                    ShowPesanErrorSetUnit(nmPesanEditError,nmHeaderEditData);
                                            };
                                    }
                            }
                    )
            };
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };
	
};//END FUNCTION SetUnitSave
///---------------------------------------------------------------------------------------///

function SetUnitDelete() 
{
	if (ValidasiEntrySetUnit(nmHeaderHapusData,true) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmSatuan) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:275,
			   fn: function (btn) 
			   {			
					if (btn =='yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamSetUnit(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoSetUnit(nmPesanHapusSukses,nmHeaderHapusData);
										RefreshDataSetUnit();
										SetUnitAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningSetUnit(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else {
										ShowPesanErrorSetUnit(nmPesanHapusError,nmHeaderHapusData);
									}
								}
							}
						)
					};
				}
			}
		)
	}
};


function ValidasiEntrySetUnit(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtKode_SetUnit').getValue() == '' || (Ext.get('txtNameSetUnit').getValue() == ''))
	{
		if (Ext.get('txtKode_SetUnit').getValue() == '' && mBolHapus === true)
		{
			//ShowPesanWarningSetUnit(nmGetValidasiKosong(nmKdSatuan),modul);
			x=0;
		}
		else if (Ext.get('txtNameSetUnit').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningSetUnit(nmGetValidasiKosong(nmSatuan),modul);
			};
			
		};
	};
	
	
	return x;
};

function ShowPesanWarningSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width :250
		}
	);
};

function ShowPesanErrorSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		   width :250
		}
	);
};

function ShowPesanInfoSetUnit(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		   width :250
		}
	);
};

//------------------------------------------------------------------------------------
function SetUnitInit(rowdata) 
{
    AddNewSetUnit = false;
    Ext.get('txtKode_SetUnit').dom.value = rowdata.UNIT_ID;
    Ext.get('txtNameSetUnit').dom.value = rowdata.UNIT;
};
///---------------------------------------------------------------------------------------///



function SetUnitAddNew() 
{
    AddNewSetUnit = true;   
	Ext.get('txtKode_SetUnit').dom.value = '';
    Ext.get('txtNameSetUnit').dom.value = '';
	rowSelectedSetUnit   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamSetUnit() 
{
    var params =
	{	
		Table: 'ViewSetupUnit',   
	    UnitId: Ext.get('txtKode_SetUnit').getValue(),
	    Unit: Ext.get('txtNameSetUnit').getValue()	
	};
    return params
};


function RefreshDataSetUnit()
{	
	dsSetUnitList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetUnit, 
					//Sort: 'UNIT_ID',
                                        Sort: 'unit_id',
					Sortdir: 'ASC', 
					target:'ViewSetupUnit',
					param : ''
				}			
			}
		);       
	
	rowSelectedSetUnit = undefined;
	return dsSetUnitList;
};

function RefreshDataSetUnitFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtKDSetUnitFilter').getValue() != '')
    { 
		//KataKunci = ' where UNIT_ID like ~%' + Ext.get('txtKDSetUnitFilter').getValue() + '%~';
                KataKunci = ' unit_id like ~%' + Ext.get('txtKDSetUnitFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtSetUnitFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where Unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
                        KataKunci = ' unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  Unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
                        KataKunci += ' and unit like  ~%' + Ext.get('txtSetUnitFilter').getValue() + '%~';
		};  
	};
        
    if (KataKunci != undefined) 
    {  
		dsSetUnitList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountSetUnit, 
					//Sort: 'UNIT_ID',
                                        Sort: 'unit_id',
					Sortdir: 'ASC', 
					target:'ViewSetupUnit',
					param : KataKunci
				}			
			}
		);        
    }
	else
	{
		RefreshDataSetUnit();
	};
};



function mComboMaksDataSetUnit()
{
  var cboMaksDataSetUnit = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataSetUnit',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5,1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountSetUnit,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCountSetUnit=b.data.displayText ;
					RefreshDataSetUnit();
				} 
			}
		}
	);
	return cboMaksDataSetUnit;
};
 
