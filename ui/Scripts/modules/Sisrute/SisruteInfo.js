var SisruteInfo = {};
SisruteInfo.data_store                  = {};
SisruteInfo.obj                         = {};
SisruteInfo.parameter                   = {};
SisruteInfo.data_store.informasi        = new WebApp.DataStore({fields:['kd_pasien','nama','tgl_lahir','umur','telepon','nama_unit','rujukan','tgl_rujukan','kd_penyakit','penyakit']});

CurrentPage.page = getPanel(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel(mod_id) {
    get_store_informasi();
    SisruteInfo.grid = new Ext.grid.EditorGridPanel({
        // renderTo: 'column-group-grid',
        store   : SisruteInfo.data_store.informasi,
        title   : 'Informasi rujukan',
        flex    : 1,
        plugins : [new Ext.ux.grid.FilterRow()],
        columns : [
            // ['kd_pasien','nama','tgl_lahir','umur','telepon','nama_unit','rujukan','tgl_rujukan','kd_penyakit','penyakit']
            {header: 'Medrec', width: 100, sortable: true, dataIndex: 'kd_pasien', filter : {}},
            {header: 'Nama', flex : 1, sortable: true,  dataIndex: 'nama', filter : {}},
            {header: 'Tgl lahir', flex : 1, sortable: true, dataIndex: 'tgl_lahir', filter : {}},
            {header: 'Umur', width: 100, sortable: true, dataIndex: 'umur', filter : {}},
            {header: 'Telepon', width: 100, sortable: true, dataIndex: 'telepon', filter : {}},
            {header: 'Nama Unit', width: 100, sortable: true, dataIndex: 'nama_unit', filter : {}},
            {header: 'Rujukan', width: 100, sortable: true, dataIndex: 'rujukan', filter : {}},
            {header: 'Tgl Rujukan', width: 100, sortable: true, dataIndex: 'tgl_rujukan', filter : {}},
            {header: 'Kode penyakit', width: 100, sortable: true, dataIndex: 'kd_penyakit', filter : {}},
            {header: 'Penyakit', width: 100, sortable: true, dataIndex: 'penyakit', filter : {}},
        ],
        tbar    : [
            {
                xtype   : 'button',
                iconCls : 'add',
                text    : 'Tambah',
                hidden  : true,
            }
        ],
        viewConfig: {
            forceFit: true
        },
    });

    SisruteInfo.filter = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        frame:true,
        title: 'Pencarian',
        bodyStyle:'padding:5px 5px 0',
        defaults: {width: '100%'},
        flex    : 1,
        defaultType: 'textfield',
        items: [
            SisruteInfo.obj.txt_medrec = {
                xtype       : 'textfield',
                name        : 'txt_medrec',
                fieldLabel  : 'Medrec',
                width       : '40%',
            },
            SisruteInfo.obj.cmb_spesialisasi = {
                xtype           : 'combo',
                typeAhead       : true,
                triggerAction   : 'all',
                lazyRender      : true,
                mode            : 'local',
                selectOnFocus   : true,
                fieldLabel      : 'Spesialisasi',
                store           : SisruteInfo.data_store.cmb_spesialisasi,
                valueField      : 'kd_spesial',
                displayField    : 'spesialisasi',
                listeners:{
                    afterrender : function(){
                        get_store_cmb_spesialisasi();
                    },
                    select      : function(a,b){
                        SisruteInfo.parameter.kd_spesial = b.data.kd_spesial;
                    }
                }
            },
        ],
        buttons: [
            {
                iconCls : 'search',
                text    : 'Cari',
                handler : function(){
                    var criteria = "";

                    if (SisruteInfo.obj.txt_medrec.getValue() != "" && typeof(SisruteInfo.obj.txt_medrec) !=='undefined') {
                        if (criteria !== "") {
                            criteria += " AND ";
                        }
                        criteria += " p.kd_pasien = ~"+SisruteInfo.obj.txt_medrec.getValue()+"~ ";
                    }

                    if (SisruteInfo.parameter.kd_spesial != "" && typeof(SisruteInfo.parameter.kd_spesial) !=='undefined'  && SisruteInfo.parameter.kd_spesial !=='Semua' ) {
                        if (criteria !== "") {
                            criteria += " AND ";
                        }                        
                        criteria += " spc.kd_spesial = ~"+SisruteInfo.parameter.kd_spesial+"~ ";
                    }
                    get_store_informasi(criteria);
                }
            },
        ]
    });


    SisruteInfo.top_panel = new Ext.Panel({
        height  : 150,
        layout: {
            type    : 'hbox',
            align   : 'stretch'
        },
        items: [
            SisruteInfo.filter,
        ]
    });

    var Panel = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        title: 'Monitoring Rujukan',
        border: false,
        flex    : 1,
        layout  : {
            type    : 'vbox',
            align   : 'stretch'
        },
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 0 0 0',
        items   : [
            // SisruteInfo.top_panel,
            SisruteInfo.grid,
        ]
    });
    return Panel;
};

function get_store_informasi(criteria = null){
    SisruteInfo.data_store.informasi.load({ 
        params: { 
            Skip    : 0, 
            Take    : 0, 
            Sort    : 'ASC',
            Sortdir : 'ASC', 
            target  : 'SIRS_get_info',
            param   : criteria
        } 
    });
}
