
var now = new Date();
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = yyyy+'/'+mm+'/'+dd;
var dsSpesialisasiDistribusiRWJ;
var ListHasilDistribusiRWJ;
var rowSelectedHasilDistribusiRWJ;
var dsKelasDistribusiRWJ;
var dsKamarDistribusiRWJ;
var dsDistribusiRWJ;
var dataSource_DistribusiRWJ;
var dsDetailUnitUtillity;
var TMPTANGGAL = now.format("d/M/Y");
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};

var panelActivate;
var tmpNoMedrec;
var tmpNamaPasien;
var tmpAlamat;
var tmpLokasi;
var tmpPoli;
var tmpStatus;
var tmpKdUnit;
var tmpKdUnitKembali;
var tmpParentUnit;
var tmpTanggalMasuk;
var tmpTanggalKembali;
var tmpTanggalKeluar;
var tmpKdDokter;
var winFormCrudDistribusiTracer;

var rowSelectedDistribusi_viDaftar;


CurrentPage.page = getPanelDistribusiRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelDistribusiRWJ(mod_id) {
    var Field = ['KD_PASIEN', 'ALAMAT', 'KD_UNIT', 'NAMA_UNIT', 'NAMA_PASIEN', 'TGL_MASUK', 'TGL_KELUAR', 'TGL_KEMBALI', 'LOKASI', 'PARENT_UNIT', 'STATUS', 'KD_UNIT_KEMBALI'];

    dataSource_DistribusiRWJ = new WebApp.DataStore({
        fields: Field
    });
	
    panelActivate = 0;
	var gridListHasilDistribusiRWJ = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_DistribusiRWJ,
        id: 'gridDistribusiRWJ',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					Ext.getCmp('btnPengeluaranPasien').enable();
					Ext.getCmp('btnPengembalianPasien').enable();
					tmpNoMedrec  	= rec.data.KD_PASIEN;
					tmpNamaPasien 	= rec.data.NAMA_PASIEN;
					tmpPoli 	 	= rec.data.NAMA_UNIT;
					tmpKdUnit 	 	= rec.data.KD_UNIT;
					tmpParentUnit 	= rec.data.PARENT_UNIT;
					tmpLokasi 		= rec.data.LOKASI;
					tmpStatus 		= rec.data.STATUS;
					tmpTanggalMasuk = rec.data.TGL_MASUK;
					//console.log(rec);
					tmpParentUnit = rec.data.PARENT_UNIT;
					mComboDetailUnit(false);
					Ext.getCmp('IDDetailUnitUtillity').setValue(rec.data.NAMA_UNIT);
					if(rec.data.KD_UNIT_KEMBALI != '' || rec.data.KD_UNIT_KEMBALI != 0){
						tmpKdUnitKembali= rec.data.KD_UNIT_KEMBALI;
					}else{
						tmpKdUnitKembali= rec.data.KD_UNIT;
					}
					
					if(rec.data.TGL_KELUAR == '' || rec.data.TGL_KELUAR == null){
						tmpTanggalKeluar= new Date(now);
					}else{
						tmpTanggalKeluar= new Date(rec.data.TGL_KELUAR);
					}
					
					if(rec.data.TGL_KEMBALI == '' || rec.data.TGL_KEMBALI == null){
						tmpTanggalKembali= new Date(now);
					}else{
						tmpTanggalKembali= new Date(rec.data.TGL_KEMBALI);
					}
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
				getFormCrudDistribusiTracer();
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    {
                        id: 'colKodePasien',
                        header: 'Kode Pasien',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colNamaPasien',
                        header: 'Nama Pasien',
                        dataIndex: 'NAMA_PASIEN',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colPoliklinik',
                        header: 'Poliklinik',
                        dataIndex: 'NAMA_UNIT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colAlamat',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colLokasi',
                        header: 'Lokasi',
                        dataIndex: 'LOKASI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnPengeluaranPasien',
                text: 'Pengeluaran Pasien',
                tooltip: 'Pengeluaran',
                iconCls: 'Edit_Tr',
                disabled : true,
                handler: function (sm, row, rec) {
					getFormCrudDistribusiTracer();
                }
            },
            {
                id: 'btnPengembalianPasien',
                text: 'Pengembalian Pasien',
                tooltip: 'Pengembalian',
                iconCls: 'Edit_Tr',
                disabled : true,
                handler: function (sm, row, rec) {
					getFormCrudDistribusiTracer();
                }
            }
        ]
    });
    var FormDepanDistribusiRWJ = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Distribusi RWJ',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
			getPanelPencarianDistribusiRWJ(),
            GetPanelTabDistribusiTracer(),
            gridListHasilDistribusiRWJ
        ],
    });
    return FormDepanDistribusiRWJ;
};

/*  ================================= PANEL TAB =============================================*/

function GetPanelTabDistribusiTracer() 
{
    var vTabPanelDistribusiTracer = new Ext.TabPanel
    (
        {
            id: 'TabPanelDistribusiTracer',
            region: 'center',
            //margins: '5 5 5 5',
            //bodyStyle: 'padding:10px 10px 10px 10px',
            activeTab: 0,
            plain: true,
            defferedRender: false,
            frame: false,
            border: true,
            anchor: '100%',
            items:
            [
                {
                    title: 'Pengeluaran Pasien',
                    id: 'vTabPanelPengeluaaranPasien',
                    listeners:
                    {
                        activate: function()
                        {
							panelActivate = 0;
							load_DistribusiRWJ(" k.tgl_masuk = '"+today+"' AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
							Ext.getCmp('btnPengeluaranPasien').show();
							Ext.getCmp('btnPengembalianPasien').hide();
							Ext.getCmp('btnPengembalianPasien').disable();
							Ext.getCmp('btnPengeluaranPasien').disable();
                        }
                    }
                },{
					title: 'Pengembalian Pasien',
					id: 'vTabPanelPengembalianPasien',
                    listeners:
                    {
                        activate: function()
                        {
							panelActivate = 1;
							load_DistribusiRWJ(" s.status = '0' AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC ");
							Ext.getCmp('btnPengeluaranPasien').hide();
							Ext.getCmp('btnPengembalianPasien').show();
							Ext.getCmp('btnPengembalianPasien').disable();
							Ext.getCmp('btnPengeluaranPasien').disable();
							//tmpTanggalKeluar = Ext.getCmp('txtTanggalKembali_kartu').getValue();
                        }
                    }
				}
			]
        }
	)
	
	return vTabPanelDistribusiTracer;
};

/* ================================================================================ FORM CRUD FOR GANTI STATUS KARTU ==================================== */
function getFormCrudDistribusiTracer(){
    winFormCrudDistribusiTracer = new Ext.Window
    (
        {
            id: 'winRWJPasswordDulu',
            title: 'Formulir',
            closeAction: 'destroy',
            width:600,
            height: 240,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getFormStatusKartuDistribusiRWJ(panelActivate),
			],
			fbar:[
                {
					text: 'Simpan',
					handler: function(){
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/rawat_jalan/viewdistribusitracer/Crud",
								params: {
									NoMedrec 	: tmpNoMedrec,
									NamaPasien 	: tmpNamaPasien,
									Poliklinik 	: tmpPoli,
									Tgl_keluar 	: tmpTanggalKeluar,
									Tgl_kembali	: Ext.getCmp('txtTanggalKembali_kartu').getValue(),
									Tgl_masuk	: tmpTanggalMasuk,
									ParentUnit 	: tmpParentUnit,
									Lokasi 		: Ext.getCmp('txtLokasi_kartu').getValue(),
									Status 		: tmpStatus,
									panel 		: panelActivate,
									KdUnit 		: tmpKdUnit,
									KdUnitKembali: tmpKdUnitKembali,
								},
								failure: function (o)
								{
									ShowPesanWarningDistribusiRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
								},
								success: function (o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === 1)
									{
										ShowPesanInfoDistribusiRWJ('Proses Saving Berhasil', 'Save');
										if(cst.status==1){
											load_DistribusiRWJ(" s.status = '0' AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC ");
										}else{
											ShowPesanInfoDistribusiRWJ('Berkas pasien tersebut sedang berada di Poli '+tmpPoli, 'Information');
										}
									}
									else
									{
										ShowPesanWarningDistribusiRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
									};
								} 
							}
						);
					}
                },{
					text: 'Keluar',
					handler: function(){
						winFormCrudDistribusiTracer.close();
					}
				}
            ]
        }
    );

    winFormCrudDistribusiTracer.show();
};

/* ==================================================== FORM PENCARIAN DATA PASIEN ================================================ */
function getPanelPencarianDistribusiRWJ() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                border: true,
                width: 1000,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x 		: 10,
                        y 		: 10,
                        xtype 	: 'label',
                        text 	: 'No. medrec '
                    },
                    {
                        x 		: 110,
                        y 		: 10,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 10,
                        width 	: 150,
                        xtype 	: 'textfield',
                        id 		: 'txtNoMedrec',
                        name	: 'txtNoMedrec',
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                        {
											tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
											tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
											tmpAlamat  		= Ext.get('txtAlamat').getValue();
											if(Ext.EventObject.getKey() != 8){
												if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
												{
													tmpNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
													Ext.getCmp('txtNoMedrec').setValue(tmpNoMedrec);
													var tmpkriteria = getCriteriaFilter_Distribusi();
													load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
												
												} else
												{
													if (tmpNoMedrec.length === 10)
													{
														var tmpkriteria = getCriteriaFilter_Distribusi();
														load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
														
													} else{
														Ext.getCmp('txtNoMedrec').setValue('');
													}
												}
											}
                                        }
										if(Ext.EventObject.getKey() === 8){
											tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
											tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
											tmpAlamat  		= Ext.get('txtAlamat').getValue();
											if (tmpNoMedrec.length == 0 && panelActivate == 0){
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}else{
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}
										}
                                    }
                                }
                    },
                    {
                        x 		: 10,
                        y 		: 40,
                        xtype 	: 'label',
                        text 	: 'Alamat'
                    },
                    {
                        x 		: 110,
                        y 		: 40,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 40,
                        width 	: 150,
                        xtype 	: 'textfield',
                        id 		: 'txtAlamat',
                        name	: 'txtAlamat',
						listeners: {
							'specialkey' 	: function()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
											tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
											tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
											tmpAlamat  		= Ext.get('txtAlamat').getValue();
											if(Ext.EventObject.getKey() != 8){
												if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
												{
													tmpNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
													Ext.getCmp('txtNoMedrec').setValue(tmpNoMedrec);
												
												}
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}
                                        }
										if(Ext.EventObject.getKey() === 8){
											tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
											tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
											tmpAlamat  		= Ext.get('txtAlamat').getValue();
											if (tmpNoMedrec.length == 0 && panelActivate == 0){
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}else{
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}
										}
                                    }
						}
                    },
                    {
                        x 		: 10,
                        y 		: 70,
                        xtype 	: 'label',
                        text 	: 'Tanggal'
                    },
                    {
                        x 		: 110,
                        y 		: 70,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 70,
                        width 	: 150,
                        xtype 	: 'datefield',
                        id 		: 'txtTanggal',
                        name 	: 'txtTanggal',
                        format 	: "d/M/Y",
						value 	: TMPTANGGAL,
						listeners: {
							'specialkey' 	: function()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
											tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
											tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
											tmpAlamat  		= Ext.get('txtAlamat').getValue();
											if(Ext.EventObject.getKey() != 8){
												if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
												{
													tmpNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
													Ext.getCmp('txtNoMedrec').setValue(tmpNoMedrec);
												
												}
												var tmpkriteria = getCriteriaFilter_Distribusi();
												load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
											}
                                        }
                                    }
						}
                    },                    
                    {
                        x: 10,
                        y: 100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataDistribusiRWJ',
                        handler: function () {
							tmpNoMedrec  	= Ext.get('txtNoMedrec').getValue();
							tmpTanggalMasuk = Ext.get('txtTanggal').getValue();
							tmpAlamat  		= Ext.get('txtAlamat').getValue();
								if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
								{
									tmpNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue());
									Ext.getCmp('txtNoMedrec').setValue(tmpNoMedrec);
								
								}
							var tmpkriteria = getCriteriaFilter_Distribusi();
							load_DistribusiRWJ(tmpkriteria+" AND u.kd_bagian = '2' ORDER BY p.nama, u.nama_unit ASC");
                        }
                    }
                ]
            }
        ]
    };
    return items;
};

/* ==================================================== FORM GANTI STATUS KARTU DATA PASIEN ================================================ */
function getFormStatusKartuDistribusiRWJ(panelActivate) {
	var activate;
	var width_kartu;
	if(panelActivate==0){
		width_kartu 	= 450;
		activate  		= true;
	}else{
		width_kartu 	= 150;
		activate  		= false;
	}
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px;',
                border: true,
                width: 1000,
                height: 400,
                anchor: '100% 100%',
                items: [
                    {
                        x 		: 10,
                        y 		: 10,
                        xtype 	: 'label',
                        text 	: 'No. medrec '
                    },
                    {
                        x 		: 110,
                        y 		: 10,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 10,
                        width 	: 450,
                        xtype 	: 'textfield',
						value 	: tmpNoMedrec,
                        editable: false,
						disabled: true,
                        id 		: 'txtNoMedrec_kartu',
                        name	: 'txtNoMedrec_kartu',
                    },
                    {
                        x 		: 10,
                        y 		: 40,
                        xtype 	: 'label',
                        text 	: 'Nama Pasien'
                    },
                    {
                        x 		: 110,
                        y 		: 40,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 40,
                        width 	: 450,
                        xtype 	: 'textfield',
                        id 		: 'txtNama_kartu',
                        name	: 'txtNama_kartu',
						editable: false,
						disabled: true,
						value 	: tmpNamaPasien,
                    }, 
                    {
                        x 		: 10,
                        y 		: 70,
                        xtype 	: 'label',
                        text 	: 'Poliklinik',
                    },
                    {
                        x 		: 110,
                        y 		: 70,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 70,
                        width 	: width_kartu,
                        xtype 	: 'textfield',
                        id 		: 'txtPoli_kartu',
                        name	: 'txtPoli_kartu',
						value 	: tmpPoli,
						disabled: true,
						editable: false,
                    },   
                    {
                        x 		: 300,
                        y 		: 70,
                        xtype 	: 'label',
                        id 		: 'txtLabelPoli',
                        text 	: 'Poliklinik Masuk',
						hidden 	: activate,
                    },
                    {
                        x 		: 400,
                        y 		: 70,
                        xtype 	: 'label',
                        id 		: 'txtDotPoliMasuk',
						hidden 	: activate,
                        text 	: ' : ',
                    },/* 
                    {
                        x 		: 410,
                        y 		: 70,
                        width 	: width_kartu,
                        xtype 	: 'textfield',
						hidden 	: activate,
                        id 		: 'txtPoliMasuk_kartu',
                        name	: 'txtPoliMasuk_kartu', 
                    },   */
					mComboDetailUnit(activate),
                    {
                        x 		: 10,
                        y 		: 100,
                        xtype 	: 'label',
                        text 	: 'Tanggal Keluar',
                    },
                    {
                        x 		: 110,
                        y 		: 100,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 100,
						disabled: true,
                        width 	: width_kartu,
                        xtype 	: 'datefield',
                        value	: now,
                        id 		: 'txtTanggalKeluar_kartu',
                        name	: 'txtTanggalKeluar_kartu',
                    },  
                    {
                        x 		: 300,
                        y 		: 100,
                        xtype 	: 'label',
                        id 		: 'txtLabelTanggalKembali',
                        text 	: 'Tanggal Kembali',
						hidden 	: activate,
                    },
                    {
                        x 		: 400,
                        y 		: 100,
                        xtype 	: 'label',
                        id 		: 'txtDotTanggalKembali',
                        text 	: ' : ',
						hidden 	: activate,
                    },
                    {
                        x 		: 410,
                        y 		: 100,
                        width 	: width_kartu,
                        xtype 	: 'datefield',
                        value	: tmpTanggalKembali,
						hidden 	: activate,
                        id 		: 'txtTanggalKembali_kartu',
                        name	: 'txtTanggalKembali_kartu',
                    },       
                    {
                        x 		: 10,
                        y 		: 130,
                        xtype 	: 'label',
                        text 	: 'Lokasi'
                    },
                    {
                        x 		: 110,
                        y 		: 130,
                        xtype 	: 'label',
                        text 	: ' : '
                    },
                    {
                        x 		: 120,
                        y 		: 130,
                        width 	: 450,
						disabled: activate,
                        xtype 	: 'textfield',
                        id 		: 'txtLokasi_kartu',
                        name	: 'txtLokasi_kartu',
						value 	: tmpLokasi,
                    },   
                ]
            }
        ]
    };
    return items;
};

function load_DistribusiRWJ(criteria)
{    
    dataSource_DistribusiRWJ.load
    (
		{
			params:
				{
					Skip 	: 0,
					Take 	: 100,
					Sort 	: 'kd_DistribusiRWJ',
					Sortdir : 'ASC',
					target 	: 'ViewDistribusiTracer',
					param 	: criteria
				}
		}
    );
    return dataSource_DistribusiRWJ;
}

/* ================================================================================== GET VARIABLE SEARCH ============================================== */
function getCriteriaFilter_Distribusi()
{
    var tmpkode 	= tmpNoMedrec;
    var tmpalamat 	= tmpAlamat;
    var tmptanggal  = tmpTanggalMasuk;
    var tmpkriteria = " ";
	
	if(tmptanggal != ''){
		tmpkriteria += " k.tgl_masuk = '"+tmptanggal+"' ";
	}
	
    if (tmpkode !== '') {
        tmpkriteria += " and p.kd_pasien = '"+tmpkode+"' ";
    }
	
	if(tmpAlamat != ''){
		tmpkriteria += " and lower(p.alamat) like lower('%"+tmpalamat+"%') ";
	}

    return tmpkriteria;
};


function SavingData(tmpkdpasien,tmpkdunit,tmpantrian)
{

    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWJ/updatestatusantrian",
                        params: {
							kd_pasien:tmpkdpasien,
                        },
                        failure: function (o)
                        {
                            ShowPesanWarningDistribusiRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_DistribusiRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 0  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoDistribusiRWJ('Proses Saving Berhasil', 'Save');
                                SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian);
                                load_DistribusiRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 0  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                            }
                            else
                            {
                                ShowPesanWarningDistribusiRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                load_DistribusiRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = 0  and LEFT(k.kd_unit,1) = '2' and ap.sts_ditemukan = 0  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");

                            }
                            ;
                        }
                    }
            );
}
;

function SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian){

    var params =
        {
            Table: 'ViewDistribusiRWJ',
            KDPASIEN: tmpkdpasien,
            KDUNIT: tmpkdunit,
            ANTRIAN:tmpantrian
        };

    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/CreateDataObj",
            params: params,
            failure: function (o)
            {
                ShowPesanWarningDistribusiRWJ('Error Database SQL !', '');
                // load_DistribusiRWJ("");
            },
            success: function (o)
            {
                // var cst = Ext.decode(o.responseText);
                // if (cst.success === true)
                // {
                //     ShowPesanInfoDistribusiRWJ('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
                //     load_DistribusiRWJ("");
                // } else if (cst.success === false && cst.pesan === 0)
                // {
                //     ShowPesanWarningDistribusiRWJ('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');
                //     load_DistribusiRWJ("");
                // }
            }
        }
    )
}

function ShowPesanWarningDistribusiRWJ(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoDistribusiRWJ(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningDistribusiRWJ('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoDistribusiRWJ('Data berhasil di hapus', 'Information');
                                dataSource_DistribusiRWJ.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusDistribusiRWJ').disable();
                                Ext.getCmp('btnBatalDistribusiRWJ').disable();
                                Ext.getCmp('btnSimpanDistribusiRWJ').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_DistribusiRWJ.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusDistribusiRWJ').disable();
                                    Ext.getCmp('btnBatalDistribusiRWJ').disable();
                                    Ext.getCmp('btnSimpanDistribusiRWJ').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningDistribusiRWJ('Gagal menghapus data', 'Error');
                                }
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepDistribusiRWJ',
                query: "kd_DistribusiRWJ = '" + dataSource_DistribusiRWJ.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}


function mComboJumlahDataDistribusi()
{
    var cboJumlahData = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboJumlahData',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [
                                           ['100', '100'], ['200', '200'],['300', '300'], ['400', '400'],
                                           ['500', '500'], ['600', '600'],['700', '700'], ['800', '800'],
                                           ['900', '900'],['1000', '1000']
                                          ]
                                }
                        ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        // Ext.getCmp('cboKamarPengkajian').setValue('');
                                        // loaddatastoreKamarPengkajian(b.data.kd_unit);
                                        // var tmpkriteriaPengkajian = getCriteriaFilter_Distribusi();
                                        // load_pengkajian(tmpkriteriaPengkajian);
                                    },

                                }
                    }
            );
    return cboJumlahData;
}
;

function printDistribusiDistribusi()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/rawat_jalan/Distribusi/save",
                        params: dataDistribusi(),
                        failure: function (o)
                        {
                            ShowPesanWarningDistribusiRWJ('Distribusi segera di cetak ', 'Cetak Data');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoDistribusiRWJ('Distribusi segera  di cetak', 'Cetak Data');
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningDistribusiRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningDistribusiRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            )
}
/* =================================================================================== GET PARAMETER CRUD ============================================================= */
function dataDistribusi()
{
    var paramstracependaftaran =
            {
                // Table: 'Distribusi',
                NoMedrec 	: tmpNoMedrec,
                NamaPasien 	: tmpNamaPasien,
				Poliklinik 	: tmpPoli,
				ParentUnit 	: tmpParentUnit,
				Lokasi 		: tmpLokasi,
				Status 		: tmpStatus,
            }
    return paramstracependaftaran
}

/* ==================================================================================== DETAIL UNIT ======================================================================== */

function mComboDetailUnit(activate)
{
	var Field           = ['KD_UNIT', 'NAMA_UNIT'];
	var dsDetailUnitDisribusi= new WebApp.DataStore({fields: Field});
	dsDetailUnitDisribusi.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViewSetupUnit',
					param: "parent='"+tmpParentUnit+"' and type_unit=false"
				}
			}
		)
	var cboDetailUnitUtillity = new Ext.form.ComboBox
	(
		{
			id: 'IDDetailUnitUtillity',
			width:150,
            x 		: 410,
            y 		: 70,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih Unit...',
			fieldLabel: ' ',
			align: 'Right',
			hidden: activate,
			store: dsDetailUnitDisribusi,
			valueField: 'KD_UNIT',
			displayField: 'NAMA_UNIT',
			value:tmpKdUnitKembali,
			listeners:
			{
				'select': function (a, b, c)
				{
					//console.log(b.data.KD_UNIT);
					tmpKdUnitKembali = b.data.KD_UNIT;
				},
				'render': function (c)
				{
				}
			}
		}
	)
	return cboDetailUnitUtillity;
};

function loaddatastoreDetailUnit(kd_unit)
{
	dsDetailUnitUtillity.load
	(
		{
			params:
			{
				Skip: 0,
				Take: 1000,
				Sort: '',
				Sortdir: 'ASC',
				target: 'ViewSetupUnit',
				param: "parent='"+kd_unit+"' and type_unit=false"
			}
		}
	)
};

