var SinarapInfo = {};
SinarapInfo.data_store                  = {};
SinarapInfo.obj                         = {};
SinarapInfo.parameter                   = {};
SinarapInfo.data_store.informasi        = new WebApp.DataStore({fields:['no_kamar','nama_kamar','spesialisasi','kosong','digunakan','kls_bpjs','kelas']});
SinarapInfo.data_store.count            = new WebApp.DataStore({fields:['informasi','nilai','status']});
SinarapInfo.data_store.cmb_kelas        = new WebApp.DataStore({fields:['kls_bpjs','kelas']});
SinarapInfo.data_store.cmb_spesialisasi = new WebApp.DataStore({fields:['kd_spesial','spesialisasi']});

CurrentPage.page = getPanel(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel(mod_id) {

    get_store_informasi();
    get_store_count_informasi();
    SinarapInfo.grid = new Ext.grid.EditorGridPanel({
        // renderTo: 'column-group-grid',
        store   : SinarapInfo.data_store.informasi,
        title   : 'informasi ketersediaan tempat tidur',
        flex    : 1,
        columns: [
            {header: 'No kamar', width: 50, sortable: true, dataIndex: 'no_kamar'},
            {header: 'Nama kamar', flex : 1, sortable: true,  dataIndex: 'nama_kamar'},
            {header: 'Spesialisasi', flex : 1, sortable: true, dataIndex: 'spesialisasi'},
            {header: 'Kelas BPJS', width: 100, sortable: true, dataIndex: 'kelas'},
            {header: 'Kosong', width: 100, sortable: true, dataIndex: 'kosong'},
            {header: 'Digunakan', width: 100, sortable: true, dataIndex: 'digunakan'},
        ],
        tbar    : [
            {
                xtype   : 'button',
                iconCls : 'add',
                text    : 'Tambah',
                hidden  : true,
            }
        ],
        viewConfig: {
            forceFit: true
        },
    });

    SinarapInfo.grid_info = new Ext.grid.EditorGridPanel({
        // renderTo: 'column-group-grid',
        store   : SinarapInfo.data_store.count,
        flex    : 1,
        columns: [
            {header: 'Name',  flex : 1, sortable: true, dataIndex: 'informasi'},
            {header: 'Jumlah', width : 100, sortable: true, dataIndex: 'nilai'},
            // {
            //     header: 'Status', 
            //     width: 25, 
            //     dataIndex: 'status',
            //     renderer: function(value, metaData, record, rowIndex, colIndex, store)
            //     {
            //          switch (value)
            //          { 
            //             case 'success':
            //                 metaData.css = 'StatusHijau';
            //             break;
            //             case 'danger':
            //                 metaData.css = 'StatusMerah';
            //             break;
            //             case 'warning':
            //                 metaData.css = 'StatusKuning';
            //             break;
            //          }
            //          return '';
            //     }
            // },
        ],
        viewConfig: {
            forceFit: true
        },

        buttons: [
            {
                iconCls : 'refresh',
                text    : 'Refresh',
                handler : function(){
                    get_store_count_informasi();
                }
            },
        ]
    });

    SinarapInfo.filter = new Ext.FormPanel({
        labelWidth: 75, // label settings here cascade unless overridden
        frame:true,
        title: 'Pencarian',
        bodyStyle:'padding:5px 5px 0',
        defaults: {width: '100%'},
        flex    : 1,
        defaultType: 'textfield',
        items: [
            SinarapInfo.obj.cmb_kelas = {
                xtype           : 'combo',
                typeAhead       : true,
                triggerAction   : 'all',
                lazyRender      : true,
                mode            : 'local',
                selectOnFocus   : true,
                fieldLabel      : 'Kelas',
                store           : SinarapInfo.data_store.cmb_kelas,
                valueField      : 'kls_bpjs',
                displayField    : 'kelas',
                listeners:{
                    afterrender : function(){
                        get_store_cmb_kelas();
                    },
                    select      : function(a,b){
                        SinarapInfo.parameter.kelas = b.data.kls_bpjs;
                    }
                }
            },
            SinarapInfo.obj.cmb_spesialisasi = {
                xtype           : 'combo',
                typeAhead       : true,
                triggerAction   : 'all',
                lazyRender      : true,
                mode            : 'local',
                selectOnFocus   : true,
                fieldLabel      : 'Spesialisasi',
                store           : SinarapInfo.data_store.cmb_spesialisasi,
                valueField      : 'kd_spesial',
                displayField    : 'spesialisasi',
                listeners:{
                    afterrender : function(){
                        get_store_cmb_spesialisasi();
                    },
                    select      : function(a,b){
                        SinarapInfo.parameter.kd_spesial = b.data.kd_spesial;
                    }
                }
            },
        ],
        buttons: [
            {
                iconCls : 'search',
                text    : 'Cari',
                handler : function(){
                    var criteria = "";

                    if (SinarapInfo.parameter.kelas != "" && typeof(SinarapInfo.parameter.kelas) !=='undefined'  && SinarapInfo.parameter.kelas !=='Semua' ) {
                        if (criteria !== "") {
                            criteria += " AND ";
                        }
                        criteria += " k.kls_bpjs = ~"+SinarapInfo.parameter.kelas+"~ ";
                    }

                    if (SinarapInfo.parameter.kd_spesial != "" && typeof(SinarapInfo.parameter.kd_spesial) !=='undefined'  && SinarapInfo.parameter.kd_spesial !=='Semua' ) {
                        if (criteria !== "") {
                            criteria += " AND ";
                        }                        
                        criteria += " spc.kd_spesial = ~"+SinarapInfo.parameter.kd_spesial+"~ ";
                    }
                    get_store_informasi(criteria);
                }
            },
        ]
    });

    SinarapInfo.informasi = new Ext.Panel({
        title   : 'informasi',
        flex    : 1,
        layout  : {
            type    : 'hbox',
            align   : 'stretch'
        },
        items   : [
            SinarapInfo.grid_info
        ]
    });

    SinarapInfo.top_panel = new Ext.Panel({
        height  : 150,
        layout: {
            type    : 'hbox',
            align   : 'stretch'
        },
        items: [
            SinarapInfo.filter,
            SinarapInfo.informasi,
        ]
    });

    var Panel = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        title: 'Bridging Siranap',
        border: false,
        flex    : 1,
        layout  : {
            type    : 'vbox',
            align   : 'stretch'
        },
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 0 0 0',
        items   : [
            SinarapInfo.top_panel,
            SinarapInfo.grid,
        ]
    });
    return Panel;
};

function get_store_informasi(criteria = null){
    SinarapInfo.data_store.informasi.load({ 
        params: { 
            Skip    : 0, 
            Take    : 0, 
            Sort    : 'ASC',
            Sortdir : 'ASC', 
            target  : 'SNAP_get_info',
            param   : criteria
        } 
    });
}

function get_store_count_informasi(criteria = null){
    SinarapInfo.data_store.count.load({ 
        params: { 
            Skip    : 0, 
            Take    : 0, 
            Sort    : 'ASC',
            Sortdir : 'ASC', 
            target  : 'SNAP_get_count_info',
            param   : criteria
        } 
    });
}

function get_store_cmb_kelas(criteria = null){
    SinarapInfo.data_store.cmb_kelas.load({ 
        params: { 
            Skip    : 0, 
            Take    : 0, 
            Sort    : 'ASC',
            Sortdir : 'ASC', 
            target  : 'SNAP_cmb_kelas',
            param   : criteria
        } 
    });
}

function get_store_cmb_spesialisasi(criteria = null){
    SinarapInfo.data_store.cmb_spesialisasi.load({ 
        params: { 
            Skip    : 0, 
            Take    : 0, 
            Sort    : 'ASC',
            Sortdir : 'ASC', 
            target  : 'SNAP_cmb_spesialisasi',
            param   : criteria
        } 
    });
}