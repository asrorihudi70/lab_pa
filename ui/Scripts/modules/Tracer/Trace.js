var now = new Date();
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1;
var yyyy = today.getFullYear();
if(dd<10) {
    dd='0'+dd
} 
if(mm<10) {
    mm='0'+mm
} 
today = yyyy+'/'+mm+'/'+dd;
var dsSpesialisasiTracerRWJ;
var ListHasilTracerRWJ;
var rowSelectedHasilTracerRWJ;
var dsKelasTracerRWJ;
var dsKamarTracerRWJ;
var dsTracerRWJ;
var dataSource_TracerRWJ;
var TMPTANGGAL = now;
var rowSelected_viDaftar;
var selectrow = {
    data: Object,
    details: Array,
    row: 0
};
var tmpNoMedrec;
var tmpNamaPasien;
var tmpAlamat;
var tmpPoli;
var tmpTanggalMasuk;
var tmpKdDokter;
var rowSelectedTracer_viDaftar;
var tmpdatarowditemukan = '';
var fldDetail = ['STS_PRINT', 'URUT', 'MEDREC', 'PASIEN',
             'UNIT', 'TANGGAL', 'CUSTOMER', 'UMUR',
             'NO_ANTRI','DITEMUKAN','KD_UNIT','POLI','DOKTER','ALAMAT','URUTAN'];
dsTRDetailKasirrwjKasirList = new WebApp.DataStore({ fields: fldDetail })
var dataSource_Detail_TracerRWJ = Ext.data.Record.create([
   {name: 'ALAMAT', mapping:'ALAMAT'},
   {name: 'CUSTOMER', mapping:'CUSTOMER'},
   {name: 'DITEMUKAN', mapping:'DITEMUKAN'},
   {name: 'DOKTER', mapping:'DOKTER'},
   {name: 'KD_UNIT', mapping:'KD_UNIT'},
   {name: 'MEDREC', mapping:'MEDREC'},
   {name: 'NO_ANTRI', mapping:'NO_ANTRI'},
   {name: 'PASIEN', mapping:'PASIEN'},
   {name: 'POLI', mapping:'POLI'},
   {name: 'STS_PRINT', mapping:'STS_PRINT'},
   {name: 'TANGGAL', mapping:'TANGGAL'},
   {name: 'UMUR', mapping:'UMUR'},
   {name: 'UNIT', mapping:'UNIT'},
   {name: 'URUT', mapping:'URUT'},
   {name: 'URUTAN', mapping:'URUTAN'}
]);
var records = '';
CurrentPage.page = getPanelTracerRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelTracerRWJ(mod_id) {
    var i = setInterval(function(){
        loadMask.hide();
        var tmpkriteria = getCriteriaFilter_Tracer();
        load_TracerRWJ(tmpkriteria);
        loadMask.hide();
    }, 60000);
    var Field = ['STS_PRINT', 'URUT', 'MEDREC', 'PASIEN',
		'UNIT', 'TANGGAL', 'CUSTOMER', 'UMUR',
		'NO_ANTRI','DITEMUKAN','KD_UNIT','POLI','DOKTER','NAMA_DOKTER','ALAMAT','URUTAN','BARU','JAM_MASUK'];

    dataSource_TracerRWJ = new WebApp.DataStore({
        fields: Field
    });
    load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = '0'  and LEFT(k.kd_unit,1) = '2' and AP.STS_DITEMUKAN = 'false'");  
    var checkColumn = new Ext.grid.CheckColumn({
        header: 'Print',
        dataIndex: 'STS_PRINT',
        id: 'colprinttracertracerRWJ',
        sortable: false,
        hideable: false,
        value:false,
        menuDisabled: true,
        width: 30
    });
    var gridListHasilTracerRWJ = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_TracerRWJ,
        id: 'gridTracerRWJ',
        anchor: '100% 75%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        clicksToEdit: 1,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        plugins: [checkColumn],
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelectedTracer_viDaftar = undefined;
                    rowSelectedTracer_viDaftar = dataSource_TracerRWJ.getAt(row);
                    tmpNoMedrec = rowSelectedTracer_viDaftar.data.MEDREC;
                    tmpNamaPasien = rowSelectedTracer_viDaftar.data.PASIEN;
                    tmpAlamat = rowSelectedTracer_viDaftar.data.ALAMAT;
                    tmpPoli = rowSelectedTracer_viDaftar.data.KD_UNIT;
                    tmpTanggalMasuk = rowSelectedTracer_viDaftar.data.TANGGAL;
                    tmpKdDokter = rowSelectedTracer_viDaftar.data.DOKTER;
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelectedTracer_viDaftar = dataSource_TracerRWJ.getAt(ridx);
                console.log(rowSelectedTracer_viDaftar)
                if(rowSelectedTracer_viDaftar.data.DITEMUKAN === 1){
                }else{
                    var tmpkdpasien = rowSelectedTracer_viDaftar.data.MEDREC;
					var tmpkdunit = rowSelectedTracer_viDaftar.data.KD_UNIT;
					var tmpantrian = rowSelectedTracer_viDaftar.data.NO_ANTRI;
					var tmpkd_dokter = rowSelectedTracer_viDaftar.data.DOKTER;
					Ext.Msg.confirm("Konfirmasi", "Data Telah Ditemukan", function(btnText){
						if(btnText === "no"){
						}else if(btnText === "yes"){
							SavingData(tmpkdpasien,tmpkdunit,tmpantrian,tmpkd_dokter);
						}
					}, this);
                }
            }
        },
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			checkColumn,{
				id: 'colUrutTracerRWJ',
				header: 'Urut',
				dataIndex: 'URUTAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colmedrecTracerRWJ',
				header: 'medrec',
				dataIndex: 'MEDREC',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colPasienTracerRWJ',
				header: 'Pasien',
				dataIndex: 'PASIEN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colUnitTracerRWJ',
				header: 'Unit',
				dataIndex: 'UNIT',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},
            {
				id: 'colDokterTracerRWJ',
				header: 'Dokter',
				dataIndex: 'NAMA_DOKTER',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colnTanggalTracerRWJ',
				header: 'Tanggal',
				dataIndex: 'TANGGAL',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},
            {
				id: 'colJamTracerRWJ',
				header: 'Jam',
				dataIndex: 'JAM_MASUK',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colCustomerTracerRWJ',
				header: 'Customer',
				dataIndex: 'CUSTOMER',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colUmurTracerRWJ',
				header: 'Umur',
				dataIndex: 'UMUR',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colAntrianTracerRWJ',
				header: 'No. Antrian',
				dataIndex: 'NO_ANTRI',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50
			},{
				id: 'colDitemukanTracerRWJ',
				header: 'Ditemukan',
				dataIndex: 'DITEMUKAN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50,
				renderer: function(val, meta, record, rowIndex) {
					if (val === true){
						return 'Ya';
					}else{
						return 'Tidak';
					}
				}
			},{
				id: 'colDBaruTracerRWJ',
				header: 'Baru',
				dataIndex: 'BARU',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 50,
				renderer: function(val, meta, record, rowIndex) {
					if (val === 't'){
						return '<div style="color: red;"><b>Ya</b></div>';
					}else{
						return 'Tidak';
					}
				}
			},{
                id: 'colHistory_TracerRWJ',
                header: 'History',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 50,
                renderer: function(val, meta, record, rowIndex) {
                    return '<button id="'+record.data.MEDREC+'" onclick="javascript:dlg_OpenListKunjungan(\''+record.data.MEDREC+'\');" type="button" style="font-size:10px;">History</button>';
                }
            }, 
		]),
        viewConfig: {
            forceFit: true
        },
        tbar: [
            {
                id: 'btnprinttracertracerRWJ',
                text: 'Print',
                tooltip: 'Cetak',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    HasilTracerLookUp(dataSource_TracerRWJ.data.items);
                }
            },{
                id: 'btnprintstatustracerRWJ',
                text: 'Print Status',
                tooltip: 'Cetak',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
					var dat = gridListHasilTracerRWJ.getSelectionModel().selections.items[0];
					if(dat!= undefined){
						if(dat.data.BARU=='t'){
							var win = window.open(baseURL + "index.php//rawat_jalan/statuspasienprinting/printTracerBaru/"+dat.data.MEDREC, '_blank');
							win.focus();
						}else{ShowPesanInfoTracerRWJ('Harus Pasien Baru.', 'Info');}
					}else{ShowPesanInfoTracerRWJ('Data Belum dipilih', 'Info');}
                }
            },{
                id: 'btnSimpanTracerRWJ',
                text: 'Report',
                tooltip: 'Report',
                iconCls: 'save'
            }
        ]
    });
    var FormDepanTracerRWJ = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Tracer RWJ',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianTracerRWJ(),
            gridListHasilTracerRWJ
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboJenisPencarianStatusTracer').show();
                Ext.getCmp('cboJenisPencarianStatusTracer').setValue('Blm Ditemukan');
                Ext.getCmp('cboUnitTracer').hide();
                Ext.getCmp('cboJenisPencarian').setValue('Status');
                Ext.getCmp('cboJumlahData').setValue('100');
                Ext.getCmp('txtAllData').hide();
                // Ext.getCmp('txtAllData').setValue('Semua');
                // Ext.getCmp('txtAllData').setDisabled(true);
            }
        }
    });
    return FormDepanTracerRWJ;
}
function getPanelPencarianTracerRWJ() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. medrec '
                    },{
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },{
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        id: 'txtkodeTracerRWJ',
                        width: 100,
                        listeners:{
							'specialkey': function (){
								var tmpNoIIMedrec = Ext.get('txtkodeTracerRWJ').getValue()
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoIIMedrec.length === 10){
									if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10){
										var tmpgetNoIIMedrec = formatnomedrec(Ext.get('txtkodeTracerRWJ').getValue())
										Ext.getCmp('txtkodeTracerRWJ').setValue(tmpgetNoIIMedrec);
										var tmpkriteria = getCriteriaFilter_Tracer();
										load_TracerRWJ(tmpkriteria);
									} else{
										if(tmpNoIIMedrec.length === 10){
											var tmpkriteria = getCriteriaFilter_Tracer();
											load_TracerRWJ(tmpkriteria);
										}else
											Ext.getCmp('txtNoMedrec').setValue('')
									}
								}
							}
						}
                    },{
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Jenis Pencarian '
                    },{
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboJenisPencarianTracer(),
                    mComboJenisPencarianStatusTracer(),
                    mComboUnitTracer(),
                    {
                        x: 240,
                        y: 40,
                        xtype: 'textfield',
                        id: 'txtAllData',
                        value:'Semua',
                        width:300
                    },{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Banyak Data '
                    },{
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboJumlahDataTracer(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandataTracerRWJ',
                        handler: function () {
                            var tmpkriteria = getCriteriaFilter_Tracer();
                            load_TracerRWJ(tmpkriteria);
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
function datainit_TracerRWJ(rowdata){
    load_detsmp(rowdata.TANGGAL, rowdata.KD_UNIT, rowdata.NO_KAMAR, rowdata.KD_TracerRWJ, rowdata.KD_WAKTU);
    TMPTANGGAL = rowdata.TANGGAL;
    TMPKD_UNIT = rowdata.KD_UNIT;
    TMPNO_KAMAR = rowdata.NO_KAMAR;
    TMPKD_TracerRWJ = rowdata.KD_TracerRWJ;
    TMPKD_WAKTU = rowdata.KD_WAKTU;
    Ext.getCmp('dtpTanggalPelaksanaan').setValue(rowdata.TANGGAL);
    Ext.getCmp('cboSpesialisasiTracerRWJpopup').setValue(rowdata.SPESIALISASI);
    Ext.getCmp('cbokelasTracerRWJpopup').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('cboKamarTracerRWJpopup').setValue(rowdata.NAMA_KAMAR);
    Ext.getCmp('cboTracerRWJTracerRWJPopUp').setValue(rowdata.TracerRWJ);
    Ext.getCmp('cboWaktuTracerRWJPopUp').setValue(rowdata.WAKTU);
}
function load_TracerRWJ(criteria){    
    dataSource_TracerRWJ.load({
		params:{
			Skip: 0,
			Take: 100,
			Sort: 'kd_TracerRWJ',
			Sortdir: 'ASC',
			target: 'ViewTracerRWJ',
			param: criteria
		}
	});
    return dataSource_TracerRWJ;
}
function getCriteriaFilter_Tracer()
{
    var tmpkode = Ext.getCmp('txtkodeTracerRWJ').getValue();
    var tmpJenisPencarian = Ext.getCmp('cboJenisPencarian').getValue();
    var tmpbanyakdata = Ext.getCmp('cboJumlahData').getValue();
    var tmpkriteria = "k.tgl_masuk = '"+today+"' "
    if (tmpkode !== '') {
        tmpkriteria += " And ps.Kd_Pasien = '"+tmpkode+"'"
    }
    console.log(tmpJenisPencarian)
    if (tmpJenisPencarian !== 'Semua'){
        if (tmpJenisPencarian === 'Unit'){
            
            if(Ext.getCmp('cboUnitTracer').getValue() === 'All'){
                tmpkriteria += '';
            }else{
              tmpkriteria += " and k.kd_unit = '"+Ext.getCmp('cboUnitTracer').getValue()+"'";            
            }
        } else if (tmpJenisPencarian === 'Status')// {}
        {
            if(Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Semua'){
                tmpkriteria += '';
            }else{
                if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Semua') {
                    tmpkriteria +="";
                }else if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Ditemukan'){
                    tmpkriteria += " and AP.STS_DITEMUKAN = 'true'";    
                }else if (Ext.getCmp('cboJenisPencarianStatusTracer').getValue() === 'Blm Ditemukan'){
                    tmpkriteria += " and AP.STS_DITEMUKAN = 'false'";
                }else{
                    tmpkriteria += " ";
                }
                
            }
            
        }
    }else{
        tmpkriteria += " ";
    }
    

    return tmpkriteria;
}
function SavingData(tmpkdpasien,tmpkdunit,tmpantrian,tmpkd_dokter)
{

    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionRWJ/updatestatusantrian",
                        params: {
                                    kd_pasien:tmpkdpasien,
                                    kd_unit:tmpkdunit,
                                    tgl_masuk:today,
                                    kd_dokter:tmpkd_dokter
                                },
                        failure: function (o)
                        {
                            ShowPesanWarningTracerRWJ('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = '0'  and LEFT(k.kd_unit,1) = '2'  Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoTracerRWJ('Proses Saving Berhasil', 'Save');
                                //SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian);
                                load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = '0'  and LEFT(k.kd_unit,1) = '2' Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");
                            }
                            else
                            {
                                ShowPesanWarningTracerRWJ(cst.message, 'Gagal');
                                load_TracerRWJ("k.Tgl_Masuk = '"+today+"' and k.baru = '0'  and LEFT(k.kd_unit,1) = '2' Order By k.jam_masuk,u.NAMA_UNIT, ps.nama asc");

                            }
                            ;
                        }
                    }
            );
}
;

function SavingDataSQL(tmpkdpasien,tmpkdunit,tmpantrian){

    var params =
        {
            Table: 'ViewTracerRWJ',
            KDPASIEN: tmpkdpasien,
            KDUNIT: tmpkdunit,
            ANTRIAN:tmpantrian
        };

    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/CreateDataObj",
            params: params,
            failure: function (o)
            {
                ShowPesanWarningTracerRWJ('Error Database SQL !', '');
                // load_TracerRWJ("");
            },
            success: function (o)
            {
                // var cst = Ext.decode(o.responseText);
                // if (cst.success === true)
                // {
                //     ShowPesanInfoTracerRWJ('Kunjungan berhasil di simpan', 'Simpan Kunjungan');
                //     load_TracerRWJ("");
                // } else if (cst.success === false && cst.pesan === 0)
                // {
                //     ShowPesanWarningTracerRWJ('Kunjungan tidak berhasil di simpan ' + cst.pesan, 'Simpan Kunjungan');
                //     load_TracerRWJ("");
                // }
            }
        }
    )
}

function ShowPesanWarningTracerRWJ(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoTracerRWJ(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}


function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningTracerRWJ('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                loadMask.hide();
                                ShowPesanInfoTracerRWJ('Data berhasil di hapus', 'Information');
                                dataSource_TracerRWJ.removeAt(selectrow.row);
                                Ext.getCmp('btnHapusTracerRWJ').disable();
                                Ext.getCmp('btnBatalTracerRWJ').disable();
                                Ext.getCmp('btnSimpanTracerRWJ').disable();
                            }
                            else
                            {
                                if (cst.pesan === 0)
                                {
                                    loadMask.hide();
                                    dataSource_TracerRWJ.removeAt(selectrow.row);
                                    Ext.getCmp('btnHapusTracerRWJ').disable();
                                    Ext.getCmp('btnBatalTracerRWJ').disable();
                                    Ext.getCmp('btnSimpanTracerRWJ').disable();
                                } else
                                {
                                    loadMask.hide();
                                    ShowPesanWarningTracerRWJ('Gagal menghapus data', 'Error');
                                }
                            }
                            ;
                        }
                    }

            );
}
;

function ParamDelete()
{
    var params =
            {
                Table: 'ViewAskepTracerRWJ',
                query: "kd_TracerRWJ = '" + dataSource_TracerRWJ.data.items[selectrow.row].data.KODE + "'"
            };
    return params;
}

function mComboUnitTracer()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    dsUnitTracer = new WebApp.DataStore({fields: Field});
    dsUnitTracer.load
    (
        {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'NAMA_UNIT',
                        Sortdir: 'ASC',
                        target: 'ComboUnit',
                        param: "kd_bagian=2 and type='0'"
                    }
        }
    )
    var cboUnitTracer = new Ext.form.ComboBox
            (
                    {
                        x: 240,
                        y: 40,
                        id: 'cboUnitTracer',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        emptyText: 'Pilih Unit',
                        fieldLabel: '',
                        align: 'Right',
                        store: dsUnitTracer,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        // anchor: '20%',
                        width:300,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteria = getCriteriaFilter_Tracer();
                                        load_TracerRWJ(tmpkriteria);
                                    },
                                    // 'render': function (c) {
                                    //     c.getEl().on('keypress', function (e) {
                                    //         if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                    //             Ext.getCmp('cboKamarTracer').focus();
                                    //     }, c);
                                    // }

                                }
                    }
            );
    return cboUnitTracer;
}
;

function mComboJenisPencarianTracer()
{
    var cboJenisPencarian = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboJenisPencarian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, 
                        mode: 'local', 
                        forceSelection: true,
                        // emptyText: 'Semua',
                        fieldLabel: '',
                        align: 'Right',
                        store: new Ext.data.ArrayStore
                        (
                                {
                                    id: 0,
                                    fields:
                                            [
                                                'Id',
                                                'displayText'
                                            ],
                                    data: [
                                            ['Semua', 'Semua'], ['Unit', 'Unit'],['Status', 'Status Berkas']
                                          ]
                                }
                        ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        width:100,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpdata = Ext.getCmp('cboJenisPencarian').getValue();
                                        if (tmpdata == 'Unit') {
                                          Ext.getCmp('cboJenisPencarianStatusTracer').hide();                                          
                                          Ext.getCmp('cboUnitTracer').show();
                                          Ext.getCmp('txtAllData').hide();
                                        } else if (tmpdata == 'Status')
                                        {
                                          Ext.getCmp('cboUnitTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').show();
                                          Ext.getCmp('txtAllData').hide();
                                        }else
                                        {
                                          Ext.getCmp('txtAllData').show();
                                          Ext.getCmp('cboUnitTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').hide();
                                          Ext.getCmp('cboJenisPencarianStatusTracer').setValue('Semua');

                                        }
                                    }

                                }
                    }
            );
    return cboJenisPencarian;
}
function mComboJenisPencarianStatusTracer(){
    var cboJenisPencarianStatusTracer = new Ext.form.ComboBox({
		x: 240,
		y: 40,
		id: 'cboJenisPencarianStatusTracer',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true, 
		mode: 'local', 
		forceSelection: true,
		fieldLabel: '',
		align: 'Right',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields:[
				'Id',
				'displayText'
			],
			data: [
				['Semua', 'Semua'], ['Ditemukan', 'Ditemukan'],['Blm Ditemukan', 'Belum Ditemukan']
			]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		width:200,
		listeners:{
			'select': function (a, b, c){
				var tmpkriteria = getCriteriaFilter_Tracer();
				load_TracerRWJ(tmpkriteria);
			}
		}
	});
    return cboJenisPencarianStatusTracer;
}
function mComboPencarianMedrecTracer(){
    var Field = ['KODE', 'NAMA'];
    dsPencarianMedrecTracer = new WebApp.DataStore({fields: Field});
    var cboPencarianMedrec = new Ext.form.ComboBox({
		x: 320,
		y: 10,
		id: 'cboPencarianMedrec',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true, 
		mode: 'local', 
		forceSelection: true,
		emptyText: 'Semua',
		fieldLabel: '',
		align: 'Right',
		store: dsPencarianMedrecTracer,
		valueField: 'KODE',
		displayField: 'NAMA',
		width:100
	});
    return cboPencarianMedrec;
}
function mComboJumlahDataTracer(){
    var cboJumlahData = new Ext.form.ComboBox({
		x: 120,
		y: 70,
		id: 'cboJumlahData',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true, 
		mode: 'local', 
		forceSelection: true,
		emptyText: 'Semua',
		fieldLabel: '',
		align: 'Right',
		store: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [
				['100', '100'], ['200', '200'],['300', '300'], ['400', '400'],
				['500', '500'], ['600', '600'],['700', '700'], ['800', '800'],
				['900', '900'],['1000', '1000']
			]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		width:100
	});
    return cboJumlahData;
}
function printtracertracer(MEDREC,PASIEN,ALAMAT,UNIT,TANGGAL,DOKTER){
    Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/tracer/save",
		params: {
			NoMedrec: MEDREC,
			NamaPasien: PASIEN,
			Alamat: ALAMAT,
			Poli: UNIT,
			TanggalMasuk: TANGGAL,
			KdDokter: DOKTER,
		},
		failure: function (o){
			ShowPesanWarningTracerRWJ('Tracer segera di cetak ', 'Cetak Data');
		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoTracerRWJ('Tracer segera  di cetak', 'Cetak Data');
			} else if (cst.success === false && cst.pesan === 0){
				ShowPesanWarningTracerRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
			} else{
				ShowPesanWarningTracerRWJ('Data tidak berhasil di cetak ' + cst.pesan, 'Cetak Data');
			}
		}
	});
}
function datatracer(){
    var paramstracependaftaran ={
		NoMedrec: tmpNoMedrec,
		NamaPasien: tmpNamaPasien,
		Alamat: tmpAlamat,
		Poli: tmpPoli,
		TanggalMasuk: tmpTanggalMasuk,
		KdDokter: tmpKdDokter,
	};
    return paramstracependaftaran
}
var FormLookUpdetailTracer;
var tmpdataarray = '';
var tmpdataseparoh = '';
function HasilTracerLookUp(rowdata) {
    FormLookUpdetailTracer = new Ext.Window({
        id: 'wHasilTracer',
        title: 'Tracer',
        closeAction: 'destroy',
        closable: true,
        width: 600,
        height: 280,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopupTracer()]
    });
    FormLookUpdetailTracer.show();
    if (rowdata != undefined) {
		dsTRDetailKasirrwjKasirList.removeAll();
        for (var i = 0; i < rowdata.length; i++) {
            if(rowdata[i].data.STS_PRINT == true){
                tmpdataseparoh = new dataSource_Detail_TracerRWJ({
					'ALAMAT': rowdata[i].data.ALAMAT,
					'CUSTOMER': rowdata[i].data.CUSTOMER,
					'DITEMUKAN': rowdata[i].data.DITEMUKAN,
					'DOKTER': rowdata[i].data.DOKTER,
					'KD_UNIT': rowdata[i].data.KD_UNIT,
					'MEDREC': rowdata[i].data.MEDREC,
					'NO_ANTRI': rowdata[i].data.NO_ANTRI,
					'PASIEN': rowdata[i].data.PASIEN,
					'POLI': rowdata[i].data.POLI,
					'STS_PRINT': rowdata[i].data.STS_PRINT,
					'TANGGAL': rowdata[i].data.TANGGAL,
					'UMUR': rowdata[i].data.UMUR,
					'UNIT': rowdata[i].data.UNIT,
					'URUT': rowdata[i].data.URUT,
					'URUTAN': rowdata[i].data.URUTAN
				});
                dsTRDetailKasirrwjKasirList.insert(dsTRDetailKasirrwjKasirList.getCount(), tmpdataseparoh);
            }
        }
    }
}
function formpopupTracer() {
    var gridListHasilTracer = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsTRDetailKasirrwjKasirList,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viDaftar = dsTRDetailKasirrwjKasirList.getAt(row);
                }
            }
        }),
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				id: 'colMedrecViewHasilTracer',
				header: 'No. Medrec',
				dataIndex: 'MEDREC',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 30
			},{
				id: 'colNamaViewHasilTracer',
				header: 'Nama Pasien',
				dataIndex: 'PASIEN',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 30
			},{
				id: 'colPoliklinikViewHasilTracer',
				header: 'Poliklinik',
				dataIndex: 'UNIT',
				sortable: false,
				hideable: false,
				menuDisabled: true,
				width: 30
			}
		]),
        viewConfig: {
            forceFit: true
        }
    });
    var FrmTabs_popupdatahasilrad = new Ext.Panel({
		id: 'formpopupTracer',
		closable: true,
		region: 'center',
		layout: 'column',
		itemCls: 'blacklabel',
		bodyStyle: 'padding: 5px 5px 5px 5px',
		border: false,
		shadhow: true,
		margins: '5 5 5 5',
		anchor: '99%',
		iconCls: '',
		items:[
			gridListHasilTracer,
			PanelTracer()
		]
	});
    return FrmTabs_popupdatahasilrad;
}
function PanelTracer() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 600,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 360,
                        y: 10,
                        xtype: 'button',
                        text: 'Print',
                        width: 100,
                        // iconCls: 'search',
                        id: 'btnPrintdata',
                        handler: function () {
                            var tmpdatasimpan = dsTRDetailKasirrwjKasirList.data.items;
                            for (var i = 0; i < tmpdatasimpan.length; i++) {
                                printtracertracer(tmpdatasimpan[i].data.MEDREC,tmpdatasimpan[i].data.PASIEN,tmpdatasimpan[i].data.ALAMAT,tmpdatasimpan[i].data.KD_UNIT,tmpdatasimpan[i].data.TANGGAL,tmpdatasimpan[i].data.DOKTER);
                                // dsTRDetailKasirrwjKasirList.removeAll();
                                // FormLookUpdetailTracer.destroy();
                            }
                        }
                    },
                    {
                        x: 470,
                        y: 10,
                        xtype: 'button',
                        text: 'Batal',
                        width: 100,
                        id: 'btnBatalPrintdata',
                        handler: function () {
                            dsTRDetailKasirrwjKasirList.removeAll();
                            tmpdataseparoh = '';
                            FormLookUpdetailTracer.destroy();

                        }
                    },
                ]
            }
        ]
    };
    return items;
}


function getDS_listKunjungan(kd_pasien){
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_jalan/viewdatakunjunganpasien/getHistoryKunjungan",
        params: {
            kd_pasien:kd_pasien
        },
        failure: function(o){
            var cst = Ext.decode(o.responseText);
        },      
        success: function(o) {
            DS_HistoryKunjungan_Tracer.removeAll();
            var cst = Ext.decode(o.responseText);

            if (cst.success === true) 
            {
                var recs=[],
                    recType=DS_HistoryKunjungan_Tracer.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                DS_HistoryKunjungan_Tracer.add(recs);
            } 
            else 
            {
                ShowPesanWarning_viDaftar('Gagal membaca history kunjungan pasien!', 'WARNING');
            };
        }
    });
}

function dlg_OpenListKunjungan(kd_pasien)
{
    var field_tracer =[
        'KD_PASIEN','URUT','KD_UNIT','POLI','TGL','DOKTER','TGL_KELUAR'
    ];
    DS_HistoryKunjungan_Tracer = new WebApp.DataStore({fields: field_tracer});
    getDS_listKunjungan(kd_pasien);
    var gridPanel = new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
        title: '',
        store: DS_HistoryKunjungan_Tracer,
        id:'gridiostorykunjunganpasien',
        autoScroll: true,
        columnLines: true,
        border: true,
        trackMouseOver: true,
        width : 500,
        plugins: [new Ext.ux.grid.FilterRow()],
        colModel: new Ext.grid.ColumnModel([
            new Ext.grid.RowNumberer(),
            {
                id: 'col_KunjunganPasien_kd_pasien',
                header: 'Kd Pasien',
                dataIndex: 'KD_PASIEN',
                sortable: true,
                hideable: false,
                menuDisabled: true,
                hidden:true,
                width: 50
            },
            {
                id: 'col_KunjunganPasien_Unit',
                header: 'Unit',
                dataIndex: 'POLI',
                sortable: true,
                hideable: false,
                menuDisabled: true,
                hidden:false,
                width: 50
            },
            {
                id: 'col_KunjunganPasien_Tgl',
                header: 'Tgl. Kunjungan',
                dataIndex: 'TGL',
                sortable: true,
                hideable: false,
                menuDisabled: true,
                hidden:false,
                width: 50
            },
            {
                id: 'col_KunjunganPasien_Dokter',
                header: 'Dokter',
                dataIndex: 'DOKTER',
                sortable: true,
                hideable: false,
                menuDisabled: true,
                hidden:false,
                width: 50
            },
        ]),
        viewConfig: {
            forceFit: true
        },  
    });
    windowListKunjungan = new Ext.Window
    (
        {
            id: 'windowListKunjungan',
            title: 'List Kunjungan',
            closeAction: 'destroy',
            width:600,
            height: 300,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
                gridPanel,
            ],
            fbar : [
                {
                    xtype   : 'button',
                    text    : 'Keluar',
                    handler : function(){
                        windowListKunjungan.close();
                    }
                }
            ]
        }
    );

    windowListKunjungan.show();
};
