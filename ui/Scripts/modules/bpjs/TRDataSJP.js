// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRCreateSEP.js
 *	Menu 			: Pendaftaran
 *	Model id 		: 030101
 *	Keterangan 		: Data Pasien adalah proses untuk melihat list/ daftar pasien, selain itu data pasien ini digunakan untuk___
 *					  ___melengkapi atau memperbaiki biodata dari pasien
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 *	Edit                    : HDHT
 */

// Deklarasi Variabel pada Data Pasien # --------------
var CurrentHistoryCreateSEP =
        {
            data: Object,
            details: Array,
            row: 0
        };
var CreateSEPvariable = {};
CreateSEPvariable.kd_pasien;
CreateSEPvariable.urut;
CreateSEPvariable.tglkunjungan;
CreateSEPvariable.kd_unit;
var dsDataStoreGridDiagnosa = new Ext.data.JsonStore();
var mod_name_viCreateSEP = "viCreateSEP";
var NamaForm_viCreateSEP = "Create SEP";
var selectSetJK;
var selectSetGolDarah;
var selectSetSatusMarital;
var icons_viCreateSEP = "Data_pegawai";
var DfltFilterBtn_viCreateSEP = 0;
var slctCount_viCreateSEP = 10;
var now_viCreateSEP = new Date();
var rowSelectedGridDataView_viCreateSEP;
var setLookUpsGridDataView_viCreateSEP;
var addNew_viCreateSEP;
var tmphasil;
var tmpjk;
var response_bpjs;
/* 
	PERBARUAN VARIALBLE GANTI KELOMPOK PASIEN
	LEH 	: HADAD
	TANGGAL : 2016 - 12 -31
 */
var cellSelecteddeskripsi_CreateSEP;
var jeniscus_RWJ;
var labelisi_RWJ;
var FormLookUpsdetailTRKelompokPasien_rwj;
var vkode_customer_RWJ;
var KelompokPasienAddNew_RWJ=true;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNoAsuransi;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var panelActiveCreateSEP;
var statusCekDetail;
var dsDokterRequestEntry;
var dataPasienCell_Select;
var dataKunjunganCell_Select;

var lakalantas_checkbox_1_BPJS;
var lakalantas_checkbox_2_BPJS;
var lakalantas_checkbox_3_BPJS;
var lakalantas_checkbox_4_BPJS;
var txtLokasi_Lakalantas;
var tmp_diagnosa;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
CurrentPage.page = dataGrid_viCreateSEP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function loaddatastoredokter(){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ vKdUnit+ '~'
		}
    });
}

// Start Project Data Pasien # --------------

// --------------------------------------- # Start Function # ---------------------------------------

/**
 *	Function : ShowPesanWarning_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanWarning_viCreateSEP(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}
// End Function ShowPesanWarning_viCreateSEP # --------------

/**
 *	Function : ShowPesanError_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanError_viCreateSEP(str, modul){
    Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 250
	});
}
// End Function ShowPesanError_viCreateSEP # --------------

/**
 *	Function : ShowPesanInfo_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat data berhasil
 */

function ShowPesanInfo_viCreateSEP(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}
// End Function ShowPesanInfo_viCreateSEP # --------------

/**
 *	Function : getCriteriaFilterGridDataView_viCreateSEP
 *	
 *	Sebuah fungsi untuk memfilter data yang diambil dari Net.
 */
function getCriteriaFilterGridDataView_viCreateSEP(){
    var strKriteriaGridDataView_viCreateSEPGridDataView_viCreateSEP = "";

    //strKriteriaGridDataView_viCreateSEP = " WHERE PS.KD_CUSTOMER = '" + DfltFilter_KD_CUSTOMER_viCreateSEP + "' ";

    if (DfltFilterBtn_viCreateSEP == 1)
    {
        if (Ext.getCmp('TxtFilterGridDataView_NO_RM_viCreateSEP').getValue() != "")
        {
            strKriteriaGridDataView_viCreateSEP += " AND PS.KD_PASIEN LIKE '" + DfltFilter_KD_CUSTOMER_viCreateSEP + ".%" + Ext.getCmp('TxtFilterGridDataView_NO_RM_viCreateSEP').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NAMA_viCreateSEP').getValue() != "")
        {
            strKriteriaGridDataView_viCreateSEP += " AND PS.NAMA LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_viCreateSEP').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_TELP_viCreateSEP').getValue() != "")
        {
            strKriteriaGridDataView_viCreateSEP += " AND PS.NO_TELP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_TELP_viCreateSEP').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_HP_viCreateSEP').getValue() != "")
        {
            strKriteriaGridDataView_viCreateSEP += " AND PS.NO_HP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_HP_viCreateSEP').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_ALAMAT_viCreateSEP').getValue() != "")
        {
            strKriteriaGridDataView_viCreateSEP += " AND PS.ALAMAT LIKE '%" + Ext.getCmp('TxtFilterGridDataView_ALAMAT_viCreateSEP').getValue() + "%' ";
        }
    }

    //strKriteriaGridDataView_viCreateSEP += " ORDER BY PS.KD_PASIEN ASC ";

    // DfltFilterBtn_viCreateSEP = 0; // Tidak diaktifkan agar saat tutup windows popup tetap di filter
    //return strKriteriaGridDataView_viCreateSEP;
}
// End Function getCriteriaFilterGridDataView_viCreateSEP # --------------

/**
 *	Function : ValidasiEntry_viCreateSEP
 *	
 *	Sebuah fungsi untuk mengecek isian
 */
function ValidasiEntry_viCreateSEP(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').getValue() === 'Tempat Lahir' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viCreateSEP').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === '' ||
                    (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').getValue() === '' || (Ext.get('TxtWindowPopup_ALAMAT_viCreateSEP').getValue() === ''))))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === ''))
        {
            ShowPesanWarning_viCreateSEP('Nama belum terisi', modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').getValue() === 'Tempat Lahir' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Tempat Lahir belum terisi", modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_ALAMAT_viCreateSEP').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_ALAMAT_viCreateSEP').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Alamat belum terisi", modul);
            x = 0;
        }
    }

    return x;
}


function ValidasiEntryHistori_viCreateSEP(modul)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viCreateSEP').getValue() === ''))
    {
        ShowPesanWarning_viCreateSEP('Nama belum terisi', modul);
        x = 0;
    }

    return x;
}
// End Function ValidasiEntry_viCreateSEP # --------------

/**
 *	Function : DataRefresh_viCreateSEP
 *	
 *	Sebuah fungsi untuk mengambil data dari Net.
 *	Digunakan pada View Grid Pertama, Filter Grid 
 */

function DataRefresh_viCreateSEP(criteria){
    dataSourceGrid_viCreateSEP.load({
		params:{
			Skip: 0,
			Take: slctCount_viCreateSEP,
			Sort: 'kunjungan.tgl_masuk',
			Sortdir: 'ASC',
			target: 'Vi_ViewDataPasien',
			param: criteria
		}
	});
    dataKunjunganCell_Select = dataSourceGrid_viCreateSEP;
    return dataSourceGrid_viCreateSEP;
}
// End Function DataRefresh_viCreateSEP # --------------

/**
 *	Function : datainit_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
 */

function datainit_viCreateSEP(rowdata){
	var tmp_data;
    addNew_viCreateSEP = false;
    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_CUSTOMER_viCreateSEP').dom.value = rowdata.TGL_MASUK;
    Ext.get('TxtWindowPopup_KD_PASIEN_viCreateSEP').dom.value = rowdata.KD_UNIT;
    Ext.get('TxtWindowPopup_NO_RM_viCreateSEP').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtWindowPopup_Urut_Masuk_viCreateSEP').dom.value = rowdata.URUT_MASUK;
    Ext.get('TxtWindowPopup_NAMA_viCreateSEP').dom.value = rowdata.NAMA;
    Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').dom.value = rowdata.TEMPAT_LAHIR;
    Ext.get('TxtWindowPopup_TAHUN_viCreateSEP').dom.value = rowdata.TAHUN;
    Ext.get('TxtWindowPopup_BULAN_viCreateSEP').dom.value = rowdata.BULAN;
    Ext.get('TxtWindowPopup_HARI_viCreateSEP').dom.value = rowdata.HARI;
    Ext.get('TxtWindowPopup_ALAMAT_viCreateSEP').dom.value = rowdata.ALAMAT;
    Ext.get('TxtTmpAgama_viCreateSEP').dom.value = rowdata.KD_AGAMA;
    Ext.get('TxtTmpPekerjaan_viCreateSEP').dom.value = rowdata.KD_PEKERJAAN;
    Ext.get('TxtTmpPendidikan_viCreateSEP').dom.value = rowdata.KD_PENDIDIKAN;
    //Ext.get('TxtWindowPopup_NO_HP_viCreateSEP').dom.value=TmpNoHP;
    Ext.get('TxtWindowPopup_TAHUN_viCreateSEP').dom.value = '';
    Ext.get('TxtWindowPopup_BULAN_viCreateSEP').dom.value = '';
    Ext.get('TxtWindowPopup_HARI_viCreateSEP').dom.value = '';
    setUsia(rowdata.TGL_LAHIR_PASIEN);
    //-------------- # datefield # --------------
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viCreateSEP').setValue(rowdata.TGL_LAHIR_PASIEN);

    //-------------- # combobox # --------------
    if (rowdata.JENIS_KELAMIN === "t" || rowdata.JENIS_KELAMIN === 1){
        tmpjk = "1";
        tmp_data = "Laki- laki";
    }else{
        tmpjk = "2";
        tmp_data = "Perempuan";
    }
    ID_JENIS_KELAMIN = rowdata.JENIS_KELAMIN;
    Ext.getCmp('cboJK').setValue(tmpjk);
    Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
    Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
    Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
    Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
    Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
    Ext.getCmp('TxtWindowPopup_NO_TELP_viCreateSEP').setValue(rowdata.TELEPON);
    Ext.getCmp('cbokelurahan_editCreateSEP').setValue(rowdata.KELURAHAN);
    Ext.getCmp('cboKecamatan_editCreateSEP').setValue(rowdata.KECAMATAN);
    Ext.getCmp('cboKabupaten_editCreateSEP').setValue(rowdata.KABUPATEN);
    Ext.getCmp('cboPropinsi_EditCreateSEP').setValue(rowdata.PROPINSI);
    loaddatastorekelurahan(rowdata.KD_KECAMATAN);
    loaddatastorekecamatan(rowdata.KD_KABUPATEN);
    loaddatastorekabupaten(rowdata.KD_PROPINSI);
    selectPropinsiRequestEntry = rowdata.KD_PROPINSI;
    selectKabupatenRequestEntry = rowdata.KD_KABUPATEN;
    selectKecamatanpasien = rowdata.KD_KECAMATAN;
    kelurahanpasien = rowdata.KD_KELURAHAN;
    Ext.getCmp('TxtWindowPopup_Email_viCreateSEP').setValue(rowdata.EMAIL);
    Ext.getCmp('TxtWindowPopup_HP_viCreateSEP').setValue(rowdata.HP);
    Ext.getCmp('TxtWindowPopup_Nama_ayah_viCreateSEP').setValue(rowdata.AYAH);
    Ext.getCmp('TxtWindowPopup_NamaIbu_viCreateSEP').setValue(rowdata.IBU);
    Ext.getCmp('TxtWindowPopup_Nama_keluarga_viCreateSEP').setValue(rowdata.NAMA_KELUARGA);
}
// End Function datainit_viCreateSEP # --------------

/**
 *	Function : dataparam_datapasviCreateSEP
 *	
 *	Sebuah fungsi untuk mengirim balik isian ke Net.
 *	Digunakan saat save, edit, delete
 */

function setUsia(Tanggal){
    Ext.Ajax.request({
		url: baseURL + "index.php/main/GetUmur",
		params: {
			TanggalLahir: Tanggal
		},
		success: function (o){
			var tmphasil = o.responseText;
			var tmp = tmphasil.split(' ');
			if (tmp.length == 6){
				Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
				Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[2]);
				Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[4]);
			} else if (tmp.length == 4)
			{
				if (tmp[1] === 'years' && tmp[3] === 'day')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);
				} else if (tmp[1] === 'year' && tmp[3] === 'days')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);
				} else
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);
				}
			} else if (tmp.length == 2)
			{

				if (tmp[1] == 'year')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
				} else if (tmp[1] == 'years')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
				} else if (tmp[1] == 'mon')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
				} else if (tmp[1] == 'mons')
				{
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[0]);
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
				} else {
					Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
					Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[0]);
				}
			} else if (tmp.length == 1)
			{
				Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
				Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
				Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('1');
			} else
			{
				alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
			}
		}


            });



}
/*
    PERBARUAN UPDATE DATA PASIEN
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2016 - 01 - 30
    ALASAN  : DATA TIDAK MENGUPDATE KE SQL SERVER 
 */

function dataparam_datapasviCreateSEP(){
    var params_viCreateSEP ={
                //-------------- # modelist Net. # --------------
		Table: 'Vi_ViewDataPasien',
		//-------------- # textfield # --------------
		NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viCreateSEP').getValue(),
		NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viCreateSEP').getValue(),
		TEMPAT_LAHIR: Ext.getCmp('TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP').getValue(),
		ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viCreateSEP').getValue(),
		NO_TELP: Ext.getCmp('TxtWindowPopup_NO_TELP_viCreateSEP').getValue(),
		//NO_HP: Ext.get('TxtWindowPopup_NO_HP_viCreateSEP').getValue(),

		//-------------- # datefield # --------------
		TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viCreateSEP').getValue(),
		//-------------- # combobox # --------------
		ID_JENIS_KELAMIN: Ext.getCmp('cboJK').getValue(),
		ID_GOL_DARAH: Ext.getCmp('cboGolDarah').getValue(),
		KD_STS_MARITAL: Ext.getCmp('cboStatusMarital').getValue(),
		NAMA_PEKERJAAN: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		NAMA_AGAMA: Ext.getCmp('cboAgamaRequestEntry').getValue(),
		NAMA_PENDIDIKAN: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
		//KD_PEKERJAAN:Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
		//KD_AGAMA:Ext.getCmp('cboAgamaRequestEntry').getValue(),

		KD_AGAMA: Ext.getCmp('TxtTmpAgama_viCreateSEP').getValue(),
		KD_PEKERJAAN: Ext.getCmp('TxtTmpPekerjaan_viCreateSEP').getValue(),
		KD_PENDIDIKAN: Ext.getCmp('TxtTmpPendidikan_viCreateSEP').getValue(),
        TMPPARAM: '0',
		KD_KELURAHAN: kelurahanpasien,
		AYAHPASIEN: Ext.getCmp('TxtWindowPopup_Nama_ayah_viCreateSEP').getValue(),
		IBUPASIEN: Ext.getCmp('TxtWindowPopup_NamaIbu_viCreateSEP').getValue(),
		NAMA_KELUARGA: Ext.getCmp('TxtWindowPopup_Nama_keluarga_viCreateSEP').getValue(),
		Emailpasien: Ext.getCmp('TxtWindowPopup_Email_viCreateSEP').getValue(),
		HPpasien: Ext.getCmp('TxtWindowPopup_HP_viCreateSEP').getValue()
	}
    return params_viCreateSEP
}
/**
 *	Function : datasave_dataPasiCreateSEP
 *	
 *	Sebuah fungsi untuk menyimpan dan mengedit data entry 
 */

function getParamRequest()
{
    var params =
            {
                Table: 'Vi_ViewDataPasien',
                NOMEDREC: Ext.get('TxtWindowPopup_NO_RM_viCreateSEP').getValue(),
                TGLMASUK: Ext.get('TxtWindowPopup_KD_CUSTOMER_viCreateSEP').getValue(),
                URUTMASUK: Ext.get('TxtWindowPopup_Urut_Masuk_viCreateSEP').getValue(),
                KDUNIT: Ext.get('TxtWindowPopup_KD_PASIEN_viCreateSEP').getValue(),
                KET: Ext.get('TxtHistoriDeleteCreateSEP').getValue(),
                TMPPARAM: '1'
            };
    return params
}
;

function datasavehistori_viCreateSEP()
{
    if (ValidasiEntryHistori_viCreateSEP('Simpan Data', true) === 1)
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: getParamRequest(),
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfo_viCreateSEP('Data transaksi pasien berhasil di hapus', 'Edit Data');
                                    DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
                                    //tmphasil = true;
                                } else
                                {
                                    ShowPesanError_viCreateSEP('Data transaksi pasien tidak berhasil di hapus' + cst.pesan, 'Edit Data');
                                    //tmphasil = false;
                                }
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function datasave_dataPasiCreateSEP(mBol){
    var cbo_kabupaten = Ext.getCmp('cboKabupaten_editCreateSEP').getValue();
    var cbo_kecamatan = Ext.getCmp('cboKecamatan_editCreateSEP').getValue();    
    var cbo_kelurahan = Ext.getCmp('cbokelurahan_editCreateSEP').getValue();    
    if (cbo_kabupaten.length == 0 || cbo_kecamatan.length == 0 || cbo_kelurahan.length == 0) {
        ShowPesanWarning_viCreateSEP('Cek kembali alamat lengkap pasien', 'Peringatan');
    }else{
        if (ValidasiEntry_viCreateSEP('Simpan Data', true) == 1){
            Ext.Ajax.request({
    			url: baseURL + "index.php/main/CreateDataObj",
    			params: dataparam_datapasviCreateSEP(),
    			failure: function (o){
    				ShowPesanError_viCreateSEP('Data tidak berhasil diupdate. Hubungi admin! ', 'Edit Data');
    			},
    			success: function (o){
    				var cst = Ext.decode(o.responseText);
    				if (cst.success === true){
    					ShowPesanInfo_viCreateSEP('Data berhasil disimpan', 'Edit Data');
    					DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
    				} else if (cst.success === false && cst.pesan === 0){
    					ShowPesanError_viCreateSEP('Data tidak berhasil diupdate ', 'Edit Data');
    				} else{
    					ShowPesanError_viCreateSEP('Data tidak berhasil diupdate. ', 'Edit Data');
    				}
    			}
    		});
        } else{
            if (mBol === true){
                return false;
            }
        }
    }
}
// End Function datasave_dataPasiCreateSEP # --------------

/**
 *	Function : datadelete_viCreateSEP
 *	
 *	Sebuah fungsi untuk menghapus data entry 
 */

function datadelete_viCreateSEP()
{
    //}
}
// End Function datadelete_viCreateSEP # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viCreateSEP(mod_id_viCreateSEP)
{/*
 
 
 Public $;
 public $;
 public $;
 public $; */

    var FieldGrid_viCreateSEP =
            [
                'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'DESC_JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'TGL_LAHIR_PASIEN', 'AGAMA',
                'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                'KD_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KABUPATEN', 'KABUPATEN', 'KECAMATAN', 'KD_KECAMATAN', 'PROPINSI',
                'KD_PROPINSI', 'KELURAHAN', 'EMAIL', 'HP', 'AYAH', 'IBU',
                'KD_AGAMA', 'KD_PEKERJAAN', 'KD_PENDIDIKAN', 'TELEPON', 'NAMA_UNIT', 'NO_ASURANSI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceGrid_viCreateSEP = new WebApp.DataStore({
        fields: FieldGrid_viCreateSEP

    });



    // Pemangilan Function untuk memangil data yang akan ditampilkan # --------------

    // Grid Data Pasien # --------------
    var GridDataView_viCreateSEP = new Ext.grid.EditorGridPanel({
        xtype       : 'editorgrid',
        store       : dataSourceGrid_viCreateSEP,
        title       : '',
        // anchor      : '100% 40%',
        columnLines : true,
        border      : false,
		flex:1,
        plugins     : [new Ext.ux.grid.FilterRow()],
        selModel    : new Ext.grid.RowSelectionModel
                        // Tanda aktif saat salah satu baris dipilih # --------------
                        ({
                            singleSelect: true,
                            listeners:
                                {
                                    rowselect: function (sm, row, rec)
                                    {
                                        dataPasienCell_Select    = rec.data;
                                        dataKunjunganCell_Select = null;
                                        Ext.getCmp('btnCreateSEP').disable();
                                        RefreshDatahistoribayar(dataPasienCell_Select.KD_PASIEN);
                                        //rowSelectedGridDataView_viCreateSEP = undefined;
    									// Ext.getCmp('btnGantiKekompokPasien').disable();
    									// Ext.getCmp('btnGantiDokter').disable();
             //                            Ext.getCmp('btnUnitPasien').disable();
    									// Ext.getCmp('btnHpsCreateSEP').disable();
             //                            rowSelectedGridDataView_viCreateSEP = dataSourceGrid_viCreateSEP.getAt(row);
                                    }
                                }
                        }),
                        /**
                         *	Mengatur tampilan pada Grid Data Pasien
                         *	Terdiri dari : Judul, Isi dan Event
                         *	Isi pada Grid di dapat dari pemangilan dari Net.
                         *	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viCreateSEP
                         *	didapat dari Function DataRefresh_viCreateSEP()
                         */
                        colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNRM_viDaftar',
                                                        header: 'No.Medrec',
                                                        dataIndex: 'KD_PASIEN',
                                                        sortable: true,
                                                        width: 20
//						filter:
//						{
//							type: 'int'
//						}
                                                    },
                                                    {
                                                        id: 'colNMPASIEN_viDaftar',
                                                        header: 'Nama',
                                                        dataIndex: 'NAMA',
                                                        sortable: true,
                                                        width: 40
//						filter:
//						{}
                                                    },
                                                    {
                                                        id: 'colALAMAT_viDaftar',
                                                        header: 'Alamat',
                                                        dataIndex: 'ALAMAT',
                                                        width: 60,
                                                        sortable: true
//						filter: {}
                                                    },
                                                    {
                                                        id: 'colTglKunj_viDaftar',
                                                        header: 'Tgl Lahir Pasien',
                                                        dataIndex: 'TGL_LAHIR_PASIEN',
                                                        width: 20,
                                                        sortable: true,
                                                        //format: 'd/M/Y',
//						filter: {},
														/* 
                                                        renderer: function (v, params, record)
                                                        {
                                                            return ShowDate(record.data.TGL_LAHIR_PASIEN);
                                                        }  */
                                                    }

                                                    //-------------- ## --------------
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'ToolbarGridDataView_viCreateSEP',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            hidden : true,
                                                            tooltip: 'Edit Data',
                                                            width: 100,
                                                            id: 'BtnEditGridDataView_viCreateSEP',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelectedGridDataView_viCreateSEP != undefined)
                                                                {
                                                                    setLookUpGridDataView_viCreateSEP(rowSelectedGridDataView_viCreateSEP.data);
                                                                } else
                                                                {
                                                                    ShowPesanWarning_viCreateSEP('Silahkan pilih Data Pasien ', 'Edit Data');
                                                                }
                                                            }
                                                        }
                                                    ]
                                        },
                                bbar: bbar_paging(mod_name_viCreateSEP, slctCount_viCreateSEP, dataSourceGrid_viCreateSEP),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                {
                                    forceFit: true
                                }
    })
                    DataRefresh_viCreateSEP();

                    var top = new Ext.FormPanel(
                            {
                                labelAlign: 'top',
                                frame: true,
                                title: '',
                                bodyStyle: 'padding:5px 5px 0',
                                //width: 600,
                                items: [{
                                        layout: 'column',
                                        items:
                                                [
                                                    {
                                                        columnWidth: .3,
                                                        layout: 'form',
                                                        items:
                                                                [
                                                                    {
                                                                        columnWidth: .5,
                                                                        layout: 'form',
                                                                        items: [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                fieldLabel: 'Alamat Pasien ',
                                                                                name: 'txtAlamatPasien_CreateSEP',
                                                                                enableKeyEvents: true,
                                                                                id: 'txtAlamatPasien_CreateSEP',
                                                                                anchor: '95%',
                                                                                listeners:
                                                                                        {
                                                                                            'keyup': function ()
                                                                                            {
                                                                                                var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                                DataRefresh_viCreateSEP(tmpkriteria);
																								
                                                                                            }
                                                                                        }
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                    }
                                                ]
                                    }]
                            });

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viCreateSEP = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viCreateSEP,
                                        iconCls: icons_viCreateSEP,
                                        id: mod_id_viCreateSEP,
                                        // region: 'center',
                                        layout: {
											type:'vbox',
											align:'stretch'
										},
                                        closable: true,
										// flex:1,
                                        border: false,
                                        // margins: '0 5 5 0',
                                        items: [
                                            {
                                                layout: 'form',
												// title:'Pencarian',
                                                bodyStyle: 'padding-top:2px',
                                                border: false,
												labelAlign:'right',
                                                items:
                                                        [
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'No. Medrec / NIK',
																// labelWidth:200,
                                                                id: 'txtNoMedrec_CreateSEP',
                                                                anchor: '40%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtNoMedrec_CreateSEP').getValue()
                                                                                // if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                    {

                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_CreateSEP').getValue())
                                                                                        Ext.getCmp('txtNoMedrec_CreateSEP').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        DataRefresh_viCreateSEP(tmpkriteria);
																						RefreshDatahistoribayar(tmpgetNoMedrec);


                                                                                    } else
                                                                                    {
                                                                                        if (tmpNoMedrec.length === 10)
                                                                                        {
                                                                                            tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                            DataRefresh_viCreateSEP(tmpkriteria);
																							RefreshDatahistoribayar(Ext.get('txtNoMedrec_CreateSEP').getValue());
																							Ext.Ajax.request
																							(
																									{
																										url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien2",
																										params: {part_number_nik: Ext.get('txtNoMedrec_CreateSEP').getValue()},
																										failure: function (o)
																										{
																											ShowPesanError_viCreateSEP('Pencarian error ! ', 'Data Pasien');
																										},
																										success: function (o)
																										{
																											var cst = Ext.decode(o.responseText);
																											if (cst.success === true)
																											{
																												Ext.getCmp('txtNoMedrec_CreateSEP').setValue(cst.nomedrec);
																											}
																											else
																											{
																												//Ext.getCmp('txtNoMedrec_CreateSEP').setValue('');
																											}
																										}
																									}
																							) 
                                                                                        } else
                                                                                        {
                                                                                            Ext.getCmp('txtNoMedrec_CreateSEP').setValue('')
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Pasien',
                                                                name: 'txtNamaPasien_CreateSEP',
                                                                id: 'txtNamaPasien_CreateSEP',
                                                                enableKeyEvents: true,
                                                                anchor: '40%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viCreateSEP(tmpkriteria);
																					console.log(dataSourceGrid_viCreateSEP.getCount());
																					var x=dataSourceGrid_viCreateSEP.getCount();
																					if (x===1)
																					{
																						RefreshDatahistoribayar(Ext.get('txtNoMedrec_CreateSEP').getValue());
																					}
																					else
																					{
																						RefreshDatahistoribayar();
																					}
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Alamat',
                                                                name: 'txtAlamatPasien_CreateSEP',
                                                                id: 'txtAlamatPasien_CreateSEP',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viCreateSEP(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viCreateSEP.getCount());
                                                                                    var x=dataSourceGrid_viCreateSEP.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoribayar(Ext.get('txtNoMedrec_CreateSEP').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoribayar();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Keluarga',
                                                                name: 'txtKeluargaPasien_CreateSEP',
                                                                id: 'txtKeluargaPasien_CreateSEP',
                                                                enableKeyEvents: true,
                                                                anchor: '60%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viCreateSEP(tmpkriteria);
                                                                                    // console.log(dataSourceGrid_viCreateSEP.getCount());
                                                                                    var x=dataSourceGrid_viCreateSEP.getCount();
                                                                                    if (x===1)
                                                                                    {
                                                                                        RefreshDatahistoribayar(Ext.get('txtNoMedrec_CreateSEP').getValue());
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        RefreshDatahistoribayar();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            // {
                                                                // xtype: 'tbspacer',
                                                                // height: 3
                                                            // },
                                                            //getItemPanelcombofilter(),
                                                            //	getItemPaneltgl_filter()
                                                        ]},
                                            GridDataView_viCreateSEP,
                                            // {
                                                // xtype: 'tbspacer',
                                                // height: 7
                                            // }, 
											gridKunjunganpasien_editdata()
                                            ]

                                    }
                            )
                    return FrmFilterGridDataView_viCreateSEP;

                    //-------------- # End form pencarian # --------------
                }
// End Function dataGrid_viCreateSEP # --------------

        function gridKunjunganpasien_editdata()
        {
            var fldDetail = ['KD_PASIEN', 'TGL_MASUK', 'NAMA_UNIT', 'URUT', 'DOKTER', 'KD_UNIT', 'JAM', 'TGL_KELUAR', 'CUST', 'NO_TRANSAKSI', 'CO_STATUS', 'NO_SEP', 'KD_DOKTER', 'KD_KASIR', 
            'KD_CUSTOMER', 'KD_PENYAKIT', 'PENYAKIT', 'PPK_PELAYANAN', 'PPK_RUJUKAN', 'UNIT_BPJS', 'TGL_MASUK_BPJS'];
            dskunjunganpasien = new WebApp.DataStore({fields: fldDetail})
            var gridCreateSEPrwj = new Ext.grid.EditorGridPanel({
                title: 'Kunjungan',
                stripeRows: true,
				height:200,
                store: dskunjunganpasien,
                border: false,
                autoScroll: true,
                columnLines: true,
                frame: false,
                // anchor: '100% 35%',
                sm: new Ext.grid.RowSelectionModel
                ({
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function (sm, row, rec) {
                        	Ext.getCmp('btnCreateSEP').enable();
                            // cellSelecteddeskripsi_CreateSEP = dskunjunganpasien.getAt(row);
                            dataKunjunganCell_Select = rec.data;
                            // console.log(rec);
							
                        }
                    }
				}),
                        tbar: [
                            {								
                                // DISINI
								id: 'btnCreateSEP', text: 'Create SEP', disabled:true, tooltip: 'Pembuatan SEP Pasien', iconCls: 'Edit_Tr',hidden:false,
                                handler: function () {
                                    console.log(dataPasienCell_Select);
                                    console.log(dataKunjunganCell_Select);
                                    formLookup_Create_SEP();
                                }
							}
                        ],
                        cm: kunjunganpasiencolummodel(),
                        viewConfig: {forceFit: true}
                    });
            return gridCreateSEPrwj;
        }
        ;


        function RefreshDatahistoribayar(kd_pasien) {
            var strKriteriaKasirrwj = '';
            strKriteriaKasirrwj = ' k.kd_pasien= ~' + kd_pasien + '~';
            dskunjunganpasien.load
            ({
                params: {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'tgl_transaksi',
                    //Sort: 'tgl_transaksi',
                    Sortdir: 'ASC',
                    target: 'ViewkunjunganPasien_bpjs',
                    param: strKriteriaKasirrwj
                }
            });
            return dskunjunganpasien;
        };


        function kunjunganpasiencolummodel()
        {
            return new Ext.grid.ColumnModel(
                    [new Ext.grid.RowNumberer(),
                        {id: 'colKdTransaksi', header: 'No. Transaksi', dataIndex: 'NO_TRANSAKSI', width: 100, menuDisabled: true, hidden: false},
                        {id: 'colenamunit', header: 'Poli', dataIndex: 'NAMA_UNIT', width: 80, hidden: false},
                        {id: 'coldok', header: 'Dokter', dataIndex: 'DOKTER', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colkelpas', header: 'Kelompok Pasien', dataIndex: 'CUST', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colTGlMasuk', header: 'Tgl Masuk', dataIndex: 'TGL_MASUK', menuDisabled: true, width: 100,/* renderer:
                                    function (v, params, record) {
                                        return ShowDate(record.data.TGL_MASUK);
                                    }*/},
                        {
                            id: 'coletglkeluar',
                            header: 'tgl Keluar',
                            menuDisabled: true,
                            dataIndex: 'TGL_KELUAR'

                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'Jam Masuk',
                            menuDisabled: true,
                        dataIndex: 'JAM',
                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'NO SEP',
                            menuDisabled: true,
							dataIndex: 'NO_SEP',
                        }
                        ,
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Masuk',
                            menuDisabled: true,
                            dataIndex: 'URUT',
                            hidden:true

                        },{
                            id: 'colStatusTransaksi',
                            header: 'Status Transaksi',
                            menuDisabled: true,
                            dataIndex: 'CO_STATUS',
                            //hidden:true

                        }



                    ]
                    )
        }
        ;
        /**
         *	Function : setLookUpGridDataView_viCreateSEP
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */
function setLookUpGridDataView_viCreateSEP(rowdata){
	var lebar = 819; // Lebar Form saat PopUp
	setLookUpsGridDataView_viCreateSEP = new Ext.Window({
		id: 'setLookUpsGridDataView_viCreateSEP',
		title: NamaForm_viCreateSEP,
		closeAction: 'destroy',
		autoScroll: true,
		width: 640,
		height: 425,
		resizable: false,
		border: false,
		plain: true,
		layout: 'fit',
		iconCls: icons_viCreateSEP,
		modal: true,
		items: getFormItemEntryGridDataView_viCreateSEP(lebar, rowdata),
		listeners:{
			activate: function (){
			},
			afterShow: function (){
				this.activate();
			},
			deactivate: function (){
				rowSelectedGridDataView_viCreateSEP = undefined;
				DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
			}
		}
	});
	setLookUpsGridDataView_viCreateSEP.show();
	if (rowdata == undefined){
		dataaddnew_viCreateSEP();
	} else{
		datainit_viCreateSEP(rowdata)
	}
}
// End Function setLookUpGridDataView_viCreateSEP # --------------

/**
 *	Function : getFormItemEntryGridDataView_viCreateSEP
 *	
 *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
 *	Di pangil dari Function setLookUpGridDataView_viCreateSEP
 */
function getFormItemEntryGridDataView_viCreateSEP(lebar, rowdata)
{
	var pnlFormDataWindowPopup_viCreateSEP = new Ext.FormPanel
			(
					{
						title: '',
						// region: 'center',
						fileUpload: true,
						margin:true,
						// layout: 'anchor',
						// padding: '8px',
						bodyStyle: 'padding-top:5px;',
						// Tombol pada tollbar Edit Data Pasien
						tbar: {
							xtype: 'toolbar',
							items:
									[
										{
											xtype: 'button',
											text: 'Save',
											id: 'btnSimpanWindowPopup_viCreateSEP',
											iconCls: 'save',
											handler: function (){
												datasave_dataPasiCreateSEP(false); //1.1
												DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
										{
											xtype: 'button',
											text: 'Save & Close',
											id: 'btnSimpanExitWindowPopup_viCreateSEP',
											iconCls: 'saveexit',
											handler: function ()
											{
												var x = datasave_dataPasiCreateSEP(false); //1.2
												DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
												if (x === undefined)
												{
													setLookUpsGridDataView_viCreateSEP.close();
												}
											}
										},
										//-------------- ## --------------
										{
											xtype: 'tbseparator'
										},
										//-------------- ## --------------
									]
						},
						//-------------- #items# --------------
						items:
								[
									// Isian Pada Edit Data Pasien
									{
										xtype: 'panel',
										title: '',
										layout:'form',
										margin:true,
										border:false,
										labelAlign: 'right',
										width: 610,
										items:
												[//---------------# Penampung data untuk fungsi update # ---------------
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpAgama_viCreateSEP',
														name: 'TxtTmpAgama_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPekerjaan_viCreateSEP',
														name: 'TxtTmpPekerjaan_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													{
														xtype: 'textfield',
														fieldLabel: '',
														id: 'TxtTmpPendidikan_viCreateSEP',
														name: 'TxtTmpPendidikan_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													//------------------------ end ---------------------------------------------								
													//-------------- # Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_CUSTOMER',
														id: 'TxtWindowPopup_KD_CUSTOMER_viCreateSEP',
														name: 'TxtWindowPopup_KD_CUSTOMER_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'URUT_MASUK',
														id: 'TxtWindowPopup_Urut_Masuk_viCreateSEP',
														name: 'TxtWindowPopup_Urut_Masuk_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'KD_PASIEN',
														id: 'TxtWindowPopup_KD_PASIEN_viCreateSEP',
														name: 'TxtWindowPopup_KD_PASIEN_viCreateSEP',
														readOnly: true,
														hidden: true
													},
													//-------------- # End Pelengkap untuk kriteria ke Net. # --------------
													{
														xtype: 'textfield',
														fieldLabel: 'No. MedRec',
														id: 'TxtWindowPopup_NO_RM_viCreateSEP',
														name: 'TxtWindowPopup_NO_RM_viCreateSEP',
														emptyText: 'No. MedRec',
														labelSeparator: '',
														readOnly: true,
														flex: 1,
														width: 195
													},
													//-------------- ## --------------
													{
														xtype: 'textfield',
														fieldLabel: 'Nama',
														id: 'TxtWindowPopup_NAMA_viCreateSEP',
														name: 'TxtWindowPopup_NAMA_viCreateSEP',
														emptyText: 'Nama',
														labelSeparator: '',
														flex: 1,
														anchor: '100%'
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Tempat Lahir',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP',
																		name: 'TxtWindowPopup_TEMPAT_LAHIR_viCreateSEP',
																		emptyText: 'Tempat Lahir',
																		flex: 1,
																		width: 195
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'tbspacer',
																		width: 17,
																		height: 23
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Tgl. Lahir',
																		style: {'text-align': 'right'},
																		id: '',
																		name: '',
																		flex: 1,
																		width: 58
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'datefield',
																		fieldLabel: 'Tanggal',
																		id: 'DateWindowPopup_TGL_LAHIR_viCreateSEP',
																		name: 'DateWindowPopup_TGL_LAHIR_viCreateSEP',
																		width: 198,
																		format: 'd/m/Y',
                                                                        listeners:{
                                                                            'specialkey' : function(){                      
                                                                                if (Ext.EventObject.getKey() == 9 ||  Ext.EventObject.getKey() == 13){                 
                                                                                    var tmptanggal = Ext.get('DateWindowPopup_TGL_LAHIR_viCreateSEP').getValue();
                                                                                    Ext.Ajax.request({
                                                                                        url: baseURL + "index.php/main/GetUmur",
                                                                                        params: {
                                                                                            TanggalLahir: Ext.getCmp('DateWindowPopup_TGL_LAHIR_viCreateSEP').getValue()
                                                                                        },
                                                                                        success: function(o){
                                                                                            var tmphasil = o.responseText;
                                                                                            var tmp = tmphasil.split(' ');
                                                                                            if (tmp.length == 6){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[2]);
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[4]);
                                                                                                getParamBalikanHitungUmurCreateSEP(tmptanggal);
                                                                                            }else if(tmp.length == 4){
                                                                                                if(tmp[1]== 'years' && tmp[3] == 'day'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'years' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);  
                                                                                                }else if(tmp[1]== 'year' && tmp[3] == 'days'){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[2]);  
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[2]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
                                                                                                }
                                                                                                getParamBalikanHitungUmurCreateSEP(tmptanggal);
                                                                                            }else if(tmp.length == 2 ){
                                                                                                if (tmp[1] == 'year' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
                                                                                                }else if (tmp[1] == 'years' ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
                                                                                                }else if (tmp[1] == 'mon'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
                                                                                                }else if (tmp[1] == 'mons'  ){
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue(tmp[0]);
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('0');
                                                                                                }else{
                                                                                                    Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                    Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue(tmp[0]);
                                                                                                }
                                                                                                getParamBalikanHitungUmurCreateSEP(tmptanggal);
                                                                                            }else if(tmp.length == 1){
                                                                                                Ext.getCmp('TxtWindowPopup_TAHUN_viCreateSEP').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_BULAN_viCreateSEP').setValue('0');
                                                                                                Ext.getCmp('TxtWindowPopup_HARI_viCreateSEP').setValue('1');
                                                                                                getParamBalikanHitungUmurCreateSEP(tmptanggal);
                                                                                            }else{
                                                                                                alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                                                                                            }                                                               
                                                                                        }
                                                                                    });
                                                                                }
                                                                            },
                                                                        }
																	}
																	//-------------- ## --------------				
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Umur',
														labelSeparator: '',
														anchor: '100%',
														width: 50,
														items:
																[
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_TAHUN_viCreateSEP',
																		name: 'TxtWindowPopup_TAHUN_viCreateSEP',
																		emptyText: 'Thn',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Thn',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_BULAN_viCreateSEP',
																		name: 'TxtWindowPopup_BULAN_viCreateSEP',
																		emptyText: 'Bln',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Bln',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'textfield',
																		id: 'TxtWindowPopup_HARI_viCreateSEP',
																		name: 'TxtWindowPopup_HARI_viCreateSEP',
																		emptyText: 'Hari',
																		style: {'text-align': 'right'},
																		readOnly: true,
																		flex: 1,
																		width: 35
																	},
																	//-------------- ## --------------
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Hari',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 20
																	}
																	//-------------- ## --------------			
																]
													},
													//-------------- ## --------------
													{
														xtype: 'compositefield',
														fieldLabel: 'Jenis Kelamin',
														labelSeparator: '',
														anchor: '100%',
														width: 250,
														items:
																[
																	mComboJK(),
																	{
																		xtype: 'displayfield',
																		fieldLabel: 'Label',
																		value: 'Golongan Darah',
																		id: '',
																		name: '',
																		flex: 1,
																		width: 90
																	},
																	mComboGolDarah()
																			//-------------- ## --------------				
																]
													},
													//-------------- ## --------------						

													//-------------- ## --------------
												]
									},
									//-------------- ## --------------
									/*{
									 xtype: 'spacer',
									 width: 10,
									 height: 1
									 },*/
									//-------------- ## --------------
									tabsWindowPopupDataLainnya_viCreateSEP(rowdata)
											//-------------- ## --------------
								]
								//-------------- #items# --------------
					}
			)
	return pnlFormDataWindowPopup_viCreateSEP;
}
// End Function getFormItemEntryGridDataView_viCreateSEP # --------------

        /**
         *	Function : tabsWindowPopupDataLainnya_viCreateSEP
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function getFormItemEntryGridDataView_viCreateSEP
         */
        function tabsWindowPopupDataLainnya_viCreateSEP(rowdata)
        {
            var TabsDataLainnya_viCreateSEP = new Ext.TabPanel
                    (
                            {
                                id: 'GDtabDetailIGD',
                                //region: 'center',
                                activeTab: 2,
                                // anchor: '100% 400%',
                                border: false,
                                // plain: true,
                                defaults:
                                        {
                                            autoScroll: true
                                        },
                                items: [
                                    tabsDetailWindowPopupDataLainnyaAktif_viCreateSEP(rowdata),
                                    tabsDetailWindowPopupkontak_viCreateSEP(rowdata),
                                    tabsDetailWindowPopupalamatpasien_viCreateSEP(rowdata),
                                    tabsDetailWindowPopupkeluarga_viCreateSEP(rowdata)


                                ]
                            }
                    );

            return TabsDataLainnya_viCreateSEP;
        }
// End Function tabsWindowPopupDataLainnya_viCreateSEP # --------------

        /**
         *	Function : tabsDetailWindowPopupDataLainnyaAktif_viCreateSEP
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function tabsWindowPopupDataLainnya_viCreateSEP
         */
        function tabsDetailWindowPopupDataLainnyaAktif_viCreateSEP(rowdata)
        {
            var itemstabsDetailWindowPopupDataLainnyaAktif_viCreateSEP =
                    {
                        xtype: 'panel',
                        title: 'Data Pasien',
                        id: 'tabsDetailWindowPopupDataLainnyaAktifItem_viCreateSEP',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 110,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboAgamaRequestEntry(),
                                                    mComboSatusMarital()
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPekerjaanRequestEntry(),
                                                    mComboPendidikanRequestEntry()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupDataLainnyaAktif_viCreateSEP;
        }
// End Function tabsDetailWindowPopupDataLainnyaAktif_viCreateSEP # --------------



        function tabsDetailWindowPopupalamatpasien_viCreateSEP(rowdata)
        {
            var itemstabsDetailWindowPopupalamatpasien_viCreateSEP =
                    {
                        xtype: 'panel',
                        title: 'Tempat Tinggal',
                        id: 'tabsDetailWindowPopupAlamatpasienItem_viCreateSEP',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 150,
                        border: false,
                        items:
                                [
                                    //-------------- ## --------------
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Alamat',
                                        id: 'TxtWindowPopup_ALAMAT_viCreateSEP',
                                        name: 'TxtWindowPopup_ALAMAT_viCreateSEP',
                                        emptyText: 'Alamat',
                                        labelSeparator: '',
                                        anchor: '100%',
                                        flex: 1
                                    },
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboPropinsi_EditCreateSEP(),
                                                    mComboKabupaten_CreateSEP(),
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    mComboKecamatan_editCreateSEP(),
                                                    mCombokelurahan_editCreateSEP()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupalamatpasien_viCreateSEP;
        }


        function loaddatastorekelurahan(kd_kecamatan)
        {
            dsKelurahan.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kelurahan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKelurahan',
                                            param: 'kd_kecamatan=' + kd_kecamatan
                                        }
                            }
                    )
        }


        function mCombokelurahan_editCreateSEP()
        {
            var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
            dsKelurahan = new WebApp.DataStore({fields: Field});

            var cbokelurahan_editCreateSEP = new Ext.form.ComboBox
                    (
                            {
                                id: 'cbokelurahan_editCreateSEP',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kelurahan...',
                                fieldLabel: 'Kelurahan',
                                align: 'Right',
                                tabIndex: 21,
//		    anchor:'60%',
                                store: dsKelurahan,
                                valueField: 'KD_KELURAHAN',
                                displayField: 'KELURAHAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                kelurahanpasien = b.data.KD_KELURAHAN;
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('txtpos').focus();
                                                    }
                                                    //atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {

                                                        kelurahanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cbokelurahan_editCreateSEP;
        }
        ;


function mComboPropinsi_EditCreateSEP(){
	var Field = ['KD_PROPINSI', 'PROPINSI'];
	dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
	dsPropinsiRequestEntry.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'propinsi',
			Sortdir: 'ASC',
			target: 'ViewComboPropinsi',
			param: ''
		}
	});
	var cboPropinsi_EditCreateSEP = new Ext.form.ComboBox({
		id: 'cboPropinsi_EditCreateSEP',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		forceSelection: true,
		emptyText: 'Pilih a Propinsi...',
		selectOnFocus: true,
		fieldLabel: 'Propinsi',
		align: 'Right',
		store: dsPropinsiRequestEntry,
		valueField: 'KD_PROPINSI',
		displayField: 'PROPINSI',
		anchor: '99%',
		tabIndex: 18,
		listeners:{
			'select': function (a, b, c){
                Ext.getCmp('cboKabupaten_editCreateSEP').setValue('');
                Ext.getCmp('cboKecamatan_editCreateSEP').setValue('');
                Ext.getCmp('cbokelurahan_editCreateSEP').setValue('');
				selectPropinsiRequestEntry = b.data.KD_PROPINSI;
				loaddatastorekabupaten(b.data.KD_PROPINSI);
			},
			'render': function (c) {
				c.getEl().on('keypress', function (e) {
					if (e.getKey() == 13) //atau Ext.EventObject.ENTER
					{
						Ext.getCmp('cboKabupaten_editCreateSEP').focus();
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					} else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
					{
						selectPropinsiRequestEntry = c.value;
						loaddatastorekabupaten(selectPropinsiRequestEntry);
					}
				}, c);
			}
		}
	});
	return cboPropinsi_EditCreateSEP;
}
        function mComboKabupaten_CreateSEP()
        {
            var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
            dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKabupaten_editCreateSEP = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKabupaten_editCreateSEP',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kabupaten...',
                                fieldLabel: 'Kab/Kod',
                                align: 'Right',
                                store: dsKabupatenRequestEntry,
                                valueField: 'KD_KABUPATEN',
                                displayField: 'KABUPATEN',
                                tabIndex: 19,
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cboKecamatan_editCreateSEP').setValue('');
                                                Ext.getCmp('cbokelurahan_editCreateSEP').setValue('');
                                                selectKabupatenRequestEntry = b.data.KD_KABUPATEN;
                                                loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('cboKecamatan_editCreateSEP').focus();
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    } else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    }
                                                }, c);
                                            }

                                        }
                            }
                    );
            return cboKabupaten_editCreateSEP;
        }
        ;


        function loaddatastorekabupaten(kd_propinsi)
        {
            dsKabupatenRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'kabupaten',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKabupaten',
                                            param: 'kd_propinsi=' + kd_propinsi
                                        }
                            }
                    )
        }

        function loaddatastorekecamatan(kd_kabupaten)
        {
            dsKecamatanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kecamatan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKecamatan',
                                            param: 'kd_kabupaten=' + kd_kabupaten
                                        }
                            }
                    )
        }


        function mComboKecamatan_editCreateSEP()
        {
            var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
            dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKecamatan_editCreateSEP = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKecamatan_editCreateSEP',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'pilih Kecamatan...',
                                fieldLabel: 'Kecamatan',
                                align: 'Right',
                                tabIndex: 20,
                                store: dsKecamatanRequestEntry,
                                valueField: 'KD_KECAMATAN',
                                displayField: 'KECAMATAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                Ext.getCmp('cbokelurahan_editCreateSEP').setValue();
                                                selectKecamatanpasien = b.data.KD_KECAMATAN;
                                                loaddatastorekelurahan(b.data.KD_KECAMATAN)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13)
                                                    {
                                                        Ext.getCmp('cbokelurahan_editCreateSEP').focus();
                                                        selectKecamatanpasien = c.value;
                                                    }																//atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        loaddatastorekelurahan(c.value);
                                                        selectKecamatanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboKecamatan_editCreateSEP;
        }
        ;


        function tabsDetailWindowPopupkontak_viCreateSEP(rowdata)
        {
            var itemstabsDetailWindowPopupkontak_viCreateSEP =
                    {
                        xtype: 'panel',
                        title: 'Kontak',
                        id: 'tabsDetailWindowPopupDatakontakItem_viCreateSEP',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' No. Tlpn' + ' ',
                                                        id: 'TxtWindowPopup_NO_TELP_viCreateSEP',
                                                        name: 'TxtWindowPopup_NO_TELP_viCreateSEP',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' Email' + ' ',
                                                        id: 'TxtWindowPopup_Email_viCreateSEP',
                                                        name: 'TxtWindowPopup_Email_viCreateSEP',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. Hp',
                                                        id: 'TxtWindowPopup_HP_viCreateSEP',
                                                        name: 'TxtWindowPopup_HP_viCreateSEP',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupkontak_viCreateSEP;
        }




        function tabsDetailWindowPopupkeluarga_viCreateSEP(rowdata)
        {
            var itemstabsDetailWindowPopupkeluarga_viCreateSEP =
                    {
                        xtype: 'panel',
                        title: 'Keluarga',
                        id: 'tabsDetailWindowPopupDataKeluargaItem_viCreateSEP',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: false,
                        items:
                                [
                                    {
                                        layout: 'column',
										border:false,
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ayah' + ' ',
                                                        id: 'TxtWindowPopup_Nama_ayah_viCreateSEP',
                                                        name: 'TxtWindowPopup_Nama_ayah_viCreateSEP',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
												border:false,
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ibu',
                                                        id: 'TxtWindowPopup_NamaIbu_viCreateSEP',
                                                        name: 'TxtWindowPopup_NamaIbu_viCreateSEP',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    },{
										xtype: 'textfield',
										fieldLabel: 'Nama Keluarga',
										id: 'TxtWindowPopup_Nama_keluarga_viCreateSEP'
									}
                                ]
                    }
            return itemstabsDetailWindowPopupkeluarga_viCreateSEP;
        }


        function getCriteriaFilter_viDaftar()
        {
            var strKriteria = "";

            if (Ext.get('txtNoMedrec_CreateSEP').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec_CreateSEP').getValue() + "'"+ " or pasien.part_number_nik= "+ "'" + Ext.get('txtNoMedrec_CreateSEP').dom.value + "'" ;
				
            }

            if (Ext.get('txtNamaPasien_CreateSEP').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_CreateSEP').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasien_CreateSEP').getValue() + "%')";
                }

            }
            
            if (Ext.get('txtAlamatPasien_CreateSEP').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_CreateSEP').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_CreateSEP').getValue() + "%')";
                }

            }

            if (Ext.get('txtKeluargaPasien_CreateSEP').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_CreateSEP').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama_keluarga) " + "LIKE lower('" + Ext.get('txtKeluargaPasien_CreateSEP').getValue() + "%')";
                }

            }
            return strKriteria;
        }

// --------------------------------------- # End Form # ---------------------------------------


// End Project Data Pasien # --------------

        function mComboPekerjaanRequestEntry()
        {
            var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];

            dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPekerjaanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pekerjaan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPekerjaan',
                                            param: ''
                                        }
                            }
                    )

            var cboPekerjaanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPekerjaanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pekerjaan...',
                                fieldLabel: 'Pekerjaan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPekerjaanRequestEntry,
                                valueField: 'KD_PEKERJAAN',
                                displayField: 'PEKERJAAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                                Ext.getCmp('cboPekerjaanRequestEntry').setValue(b.data.KD_PEKERJAAN);
                                                Ext.getCmp('TxtTmpPekerjaan_viCreateSEP').setValue(b.data.KD_PEKERJAAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPekerjaanRequestEntry;
        }
        ;

        function mComboPendidikanRequestEntry()
        {
            var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];

            dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPendidikanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pendidikan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPendidikan',
                                            param: ''
                                        }
                            }
                    )

            var cboPendidikanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPendidikanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pendidikan...',
                                fieldLabel: 'Pendidikan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPendidikanRequestEntry,
                                valueField: 'KD_PENDIDIKAN',
                                displayField: 'PENDIDIKAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPendidikanRequestEntry = b.data.KD_PENDIDIKAN;
                                                Ext.getCmp('cboPendidikanRequestEntry').setValue(b.data.KD_PENDIDIKAN);
                                                Ext.getCmp('TxtTmpPendidikan_viCreateSEP').setValue(b.data.KD_PENDIDIKAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPendidikanRequestEntry;
        }
        ;

        function mComboAgamaRequestEntry()
        {
            var Field = ['KD_AGAMA', 'AGAMA'];

            dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
            dsAgamaRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'agama',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboAgama',
                                            param: ''
                                        }
                            }
                    )

            var cboAgamaRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboAgamaRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Agama...',
                                fieldLabel: 'Agama',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsAgamaRequestEntry,
                                valueField: 'KD_AGAMA',
                                displayField: 'AGAMA',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectAgamaRequestEntry = b.data.KD_AGAMA;
                                                Ext.getCmp('cboAgamaRequestEntry').setValue(b.data.KD_AGAMA);
                                                Ext.getCmp('TxtTmpAgama_viCreateSEP').setValue(b.data.KD_AGAMA);

                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboGolDarah').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboAgamaRequestEntry;
        }
        ;

        function mComboSatusMarital()
        {
            var cboStatusMarital = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboStatusMarital',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Status...',
                                fieldLabel: 'Status Marital ',
                                width: 110,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, 'Blm Kawin'], [1, 'Kawin'], [2, 'Janda'], [3, 'Duda']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetSatusMarital,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetSatusMarital = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtAlamat').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboStatusMarital;
        }
        ;

        function mComboGolDarah()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarah',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJK()
        {
			
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJK',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;


        function form_histori() {
            var form = new Ext.form.FormPanel({
                baseCls: 'x-plain',
                labelWidth: 55,
                url: 'save-form.php',
                defaultType: 'textfield',
                items:
                        [
                            {
                                x: 0,
                                y: 60,
                                xtype: 'textarea',
                                id: 'TxtHistoriDeleteCreateSEP',
                                hideLabel: true,
                                name: 'msg',
                                anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                            }

                        ]
            });

            var window = new Ext.Window({
                title: 'Pesan',
                width: 400,
                height: 300,
                minWidth: 300,
                minHeight: 200,
                layout: 'fit',
                plain: true,
                bodyStyle: 'padding:5px;',
                buttonAlign: 'center',
                items: form,
                buttons: [{
                        xtype: 'button',
                        text: 'Save',
                        //height: 20,
                        id: 'btnSimpanHistori_viCreateSEP',
                        iconCls: 'save',
                        handler: function ()
                        {
                            datasavehistori_viCreateSEP(false); //1.1
                            window.close();
                            setLookUpsGridDataView_viCreateSEP.close();
                            //DataRefresh_viCreateSEP(getCriteriaFilterGridDataView_viCreateSEP());
                        }
                    }]
            });

            window.show();
        }
        ;

        function item()
        {
            var pnlFormDataHistori_viCreateSEP = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                fileUpload: true,
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                items:
                                        [
                                        ]
                            }
                    );
            return pnlFormDataHistori_viCreateSEP;
        }
		
/* 
	PERBARUAN GANTI KELOMPOK PASIEN 
	OLEH 	: HADAD
	TANGGAL : 2016 - 12 - 30
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_rwj = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_rwj.show();
    KelompokPasienbaru_rwj();

};

function KelompokPasienbaru_rwj() 
{
	jeniscus_RWJ=0;
    KelompokPasienAddNew_RWJ = true;
    Ext.getCmp('cboKelompokpasien_RWJ').show()
	Ext.getCmp('txtCustomer_rwjLama').disable();
	Ext.get('txtCustomer_rwjLama').dom.value=vCustomer;
	Ext.get('txtRWJNoSEP').dom.value = vNoSEP;
	
	RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRKelompokPasien_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_igd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputKelompokPasien_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_rwj(lebar),	
					getItemPanelNoTransksiKelompokPasien_rwj(lebar)	,
					
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI DOKTER  
	OLEH 	: HADAD
	TANGGAL : 2017 - 01 - 05
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_rwj(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_rwj = new Ext.Window
    (
        {
            id: 'idGantiDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_rwj(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_rwj.show();
    GantiPasien_rwj();

};

function GantiPasien_rwj() 
{
	//RefreshDatacombo_rwj(jeniscus_RWJ);
};

function getFormEntryTRGantiDokter_rwj(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGanti',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputGantiDokter_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanGantiDokter
};

function getItemPanelInputGantiDokter_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'txtUnitAsal_CreateSEP',
					    id: 'txtUnitAsal_CreateSEP',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'txtDokterAsal_CreateSEP',
					    id: 'txtDokterAsal_CreateSEP',
						value:vNamaDokter,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};


/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI UNIT
	HADAD
	2017 - 01 -05
 */
 
/* =============================================================================================================================================== */
function KonsultasiCreateSEPLookUp_rwj(lebar){
	
    var lebar = 440;
    FormLookUpsdetailTRKonsultasi_CreateSEP = new Ext.Window
    (
        {
            id: 'idKonsultasiCreateSEP',
            title: 'Ganti Unit',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiUnit_rwj(lebar),
            listeners:
            {
                
            }
        }
    );
    FormLookUpsdetailTRKonsultasi_CreateSEP.show();
    //KonsultasiAddNew();
};



function getFormEntryTRGantiUnit_rwj(lebar) 
{
    var pnlTRUnitPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiUnit',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar-35,
            border: false,
            items: [
					getItemPanelInputGantiUnit_rwj(lebar),
					getItemPanelButtonKelompokPasien_rwj(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiUnit = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiUnit',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRUnitPasien_igd]

		}
	);

    return FormDepanGantiUnit
};


function getItemPanelInputGantiUnit_rwj(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Poli Asal ',
					    name: 'txtUnitAsal_CreateSEP',
					    id: 'txtUnitAsal_CreateSEP',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboPoliklinik(),
					mComboDokterGantiEntry()
				]
			}
		]
	};
    return items;
};
/* =============================================================================================================================================== */

function getItemPanelButtonKelompokPasien_rwj(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getId(),
						handler:function()
						{
							if(panelActiveCreateSEP == 0){
								Datasave_Kelompokpasien_rwj();
							}
							if(panelActiveCreateSEP == 1){
								Datasave_GantiDokter_rwj();
							}
							if(panelActiveCreateSEP == 2){
								Datasave_GantiUnit_rwj();
							}
						}
					},
					{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id:Nci.getId(),
						handler:function() 
						{
							if(panelActiveCreateSEP == 0){
								FormLookUpsdetailTRKelompokPasien_rwj.close();
							}
							
							if(panelActiveCreateSEP == 1){
								FormLookUpsdetailTRGantiDokter_rwj.close();
							}
							
							if(panelActiveCreateSEP == 2){
								FormLookUpsdetailTRKonsultasi_CreateSEP.close();
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function getKelompokpasienlama_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_rwjLama',
							id: 'txtCustomer_rwjLama',
							labelWidth:130,
							editable: false,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};
function getItemPanelNoTransksiKelompokPasien_rwj(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_RWJ',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{jeniscus_RWJ='0'
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Perusahaan')
										{jeniscus_RWJ='1';
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Asuransi')
										{jeniscus_RWJ='2';
											//Ext.getCmp('txtRWJNoSEP').enable();
											//Ext.getCmp('txtRWJNoAskes').enable();
										}
										
										RefreshDatacombo_rwj(jeniscus_RWJ);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_rwj()
							]
						},{
							xtype: 'textfield',
							fieldLabel:'No SEP  ',
							name: 'txtRWJNoSEP',
							id: 'txtRWJNoSEP',
							width: 100,
							anchor: '99%'
						 }, {
                            xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtRWJNoAskes',
                            id: 'txtRWJNoAskes',
                            width: 100,
                            anchor: '99%',
                            value : vNoAsuransi,
						 }
									
				]
			}
			
		]
	}
    return items;
};

function mComboKelompokpasien_rwj()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_IGD = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	if (jeniscus_RWJ===undefined || jeniscus_RWJ==='')
	{
		jeniscus_RWJ=0;
	}
	ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~'
            }
        }
    )
    var cboKelompokpasien_RWJ = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_RWJ',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi_RWJ,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_IGD,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_RWJ;
};


function RefreshDatacombo_rwj(jeniscus_RWJ) 
{

    ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_RWJ +'~ and kontraktor.kd_customer not in(~'+ vkode_customer_RWJ+'~)'
            }
        }
    )
	
    return ds_customer_viPJ_IGD;
};

/* 
	============================= AJAX 
 */
function Datasave_Kelompokpasien_rwj(mBol) 
{	
    if (ValidasiEntryUpdateKelompokPasien_RWJ(nmHeaderSimpanData,false) == 1 )
    {           

        var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
                if (combo!='') {
                    Ext.Ajax.request
                    (
                        {
                            url: baseURL +  "index.php/main/functionRWJ/UpdateGantiKelompok",   
                            params: {
                                        KDCustomer  : selectKdCustomer,
                                        KDNoSJP     : Ext.get('txtRWJNoSEP').getValue(),
                                        KDNoAskes   : Ext.get('txtRWJNoAskes').getValue(),
                                        KdPasien    : vKd_Pasien,
                                        TglMasuk    : vTanggal,
                                        KdUnit      : vKdUnit,
                                        UrutMasuk   : vUrutMasuk,
                                        KdDokter    : vKdDokter,
                                        alasan      : combo,
                            },
                            failure: function(o)
                            {
                                ShowPesanWarning_viCreateSEP('Simpan kelompok pasien gagal', 'Gagal');
                            },  
                            success: function(o) 
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true) 
                                {
                                    panelActiveCreateSEP = 1;
                                    FormLookUpsdetailTRKelompokPasien_rwj.close();
                                    RefreshDatahistoribayar(vKd_Pasien);
                                    Ext.getCmp('btnGantiKekompokPasien').disable(); 
                                    Ext.getCmp('btnGantiDokter').disable(); 
                                    Ext.getCmp('btnUnitPasien').disable();  
                                    Ext.getCmp('btnHpsCreateSEP').disable();   
                                    ShowPesanInfo_viCreateSEP("Mengganti kelompok pasien berhasil", "Success");

                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                    DataRefresh_viCreateSEP(tmpkriteria);
                                }else 
                                {
                                    panelActiveCreateSEP = 1;
                                    ShowPesanWarning_viCreateSEP('Simpan kelompok pasien gagal', 'Gagal');
                                };
                            }
                        }
                    );
                }else{
                    ShowPesanWarning_viCreateSEP('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
            }
        });
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
	
};

function Datasave_GantiUnit_rwj(mBol) 
{	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viCreateSEP('Dokter baru harap diisi', "Informasi");
	}else{	
			Ext.Ajax.request
			(
				{
					url: baseURL +  "index.php/main/functionRWJ/UpdateGantiUnit",	
					params: getParamKelompokpasien_RWJ(),
					failure: function(o)
					{
						ShowPesanWarning_viCreateSEP('Simpan unit pasien gagal', 'Gagal');
						loadMask.hide();
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.totalrecords === 1) 
						{
                            panelActiveCreateSEP = 1;
							vKdUnitDulu = "";
							FormLookUpsdetailTRKonsultasi_CreateSEP.close();
							RefreshDatahistoribayar(vKd_Pasien);
							Ext.getCmp('btnGantiKekompokPasien').disable();	
							Ext.getCmp('btnGantiDokter').disable();	
                            Ext.getCmp('btnUnitPasien').disable();  
							Ext.getCmp('btnHpsCreateSEP').disable();	
							ShowPesanInfo_viCreateSEP("Mengganti unit pasien berhasil", "Success");
						}else 
						{
							ShowPesanWarning_viCreateSEP('Simpan   pasien gagal', 'Gagal');
						};
					}
				}
			 )
	}
	
};

function Datasave_GantiDokter_rwj(mBol) 
{	
	//console.log(Ext.get('cboDokterRequestEntry').getValue());
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarning_viCreateSEP('Dokter baru harap diisi', "Informasi");
	}else{
		Ext.Ajax.request
		(
			{
				url: baseURL +  "index.php/main/functionRWJ/UpdateGantiDokter",	
				params: getParamKelompokpasien_RWJ(),
				failure: function(o)
				{
					ShowPesanWarning_viCreateSEP('Simpan dokter pasien gagal', 'Gagal');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
                        panelActiveCreateSEP = 1;
						FormLookUpsdetailTRGantiDokter_rwj.close();
						RefreshDatahistoribayar(vKd_Pasien);
						Ext.getCmp('btnGantiKekompokPasien').disable();	
						Ext.getCmp('btnGantiDokter').disable();	
                        Ext.getCmp('btnUnitPasien').disable();  
						Ext.getCmp('btnHpsCreateSEP').disable();	
						ShowPesanInfo_viCreateSEP("Mengganti dokter pasien berhasil", "Success");
					}else 
					{
						ShowPesanWarning_viCreateSEP('Simpan dokter pasien gagal', 'Gagal');
					};
				}
			}
		) 
	}
	
};

function cekDetailTransaksi() 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL +  "index.php/main/functionRWJ/cekDetailTransaksi",	
			params: getParamKelompokpasien_RWJ(),
			failure: function(o)
			{
				ShowPesanWarning_viCreateSEP('Data gagal di periksa', 'Gagal');
				loadMask.hide();
			},	
			success: function(o) 
			{
				//console.log(o.responseText);
				if(o.responseText > 0){
					statusCekDetail = true;
				}else{
					statusCekDetail = false;
				}
			}
		}
	)
};

function ValidasiEntryUpdateKelompokPasien_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_RWJ').getValue() == '') || (Ext.get('kelPasien_RWJ').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_RWJ').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarning_viCreateSEP(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};

function ValidasiEntryUpdateGantiDokter_RWJ(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ))
	{
		ShowPesanWarning_viCreateSEP(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
		x = 0;
	};
	return x;
};

function getParamKelompokpasien_RWJ() 
{
	var params;
	if(panelActiveCreateSEP == 0){
		params = {
			KDCustomer 	: selectKdCustomer,
			KDNoSJP  	: Ext.get('txtRWJNoSEP').getValue(),
			KDNoAskes  	: Ext.get('txtRWJNoAskes').getValue(),
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
		}
	}else if(panelActiveCreateSEP == 1 || panelActiveCreateSEP == 2){
		params = {
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
			NoTransaksi	: vNoTransaksi,
			KdKasir  	: vKdKasir,
			KdUnitDulu 	: vKdUnitDulu,
		}
	}else if(panelActiveCreateSEP == 'undefined'){
		params = { data : "null", }
	}else{
		params = { data : "null", }
	}
    return params
};

function mComboDokterGantiEntry(){ 
	/* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntry = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterRequestEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				vKdDokter = b.data.KD_DOKTER;
            },
		}
    });
    return cboDokterGantiEntry;
};

function mComboPoliklinik(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnitB',
            param	: " parent<>'0'"
        }
    });
    var cboPoliklinikRequestEntry = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntry',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '100%',
        listeners:{
            select: function(a, b, c){
				vKdUnitDulu 	= vKdUnit;
				vKdUnit  		= b.data.KD_UNIT;
				loaddatastoredokter();
            }
		}
    });
    return cboPoliklinikRequestEntry;
};

/*
    
    PERBARUAN FORMAT TANGGAL LAHIR 
    OLEH    : HADAD AL GOJALI 
    TANGGAL : 2017 - 01 - 21 

 */
function getParamBalikanHitungUmurCreateSEP(tgl){
    var param;
    var tglPisah=tgl.split("/");
    var tglAsli;
    if (tglPisah[1]==="01"){
        tglAsli=tglPisah[0]+"/"+"Jan"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="02"){
        tglAsli=tglPisah[0]+"/"+"Feb"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="03"){
        tglAsli=tglPisah[0]+"/"+"Mar"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="04"){
        tglAsli=tglPisah[0]+"/"+"Apr"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="05"){
        tglAsli=tglPisah[0]+"/"+"May"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="06"){
        tglAsli=tglPisah[0]+"/"+"Jun"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="07"){
        tglAsli=tglPisah[0]+"/"+"Jul"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="08"){
        tglAsli=tglPisah[0]+"/"+"Aug"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="09"){
        tglAsli=tglPisah[0]+"/"+"Sep"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="10"){
        tglAsli=tglPisah[0]+"/"+"Oct"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="11"){
        tglAsli=tglPisah[0]+"/"+"Nov"+"/"+tglPisah[2];
    }else if (tglPisah[1]==="12"){
        tglAsli=tglPisah[0]+"/"+"Dec"+"/"+tglPisah[2];
    }else{
        tglAsli=tgl;
    }
    Ext.getCmp('DateWindowPopup_TGL_LAHIR_viCreateSEP').setValue(tgl);
}



/* 
    ===============================================================================================================
    ======================================================== Form Create SEP
    ===============================================================================================================
*/

function formLookup_Create_SEP(){
    var lebar = 600;
    formLookups_CreateSEP = new Ext.Window({
        id: 'formLookup_Create_SEP',
        name: 'formLookup_Create_SEP',
        title: 'Formulir Create SEP',
        closeAction: 'destroy',
        width: lebar,
        height: 500, //575,
        resizable: false,
        constrain: true,
        autoScroll: false,
        // iconCls: 'Studi_Lanjut',
        modal: true,
        items: panel_form_lookup_Create_SEP(), //1
        listeners:{
            afterShow: function ()
            {
                this.activate();
            }
        }
    });
    formLookups_CreateSEP.show();
}
/*
  {
           "request": {
              "t_sep": {
                 "noKartu": "0001112230666",
                 "tglSep": "2017-10-18",
                 "ppkPelayanan": "0301R001",
                 "jnsPelayanan": "2",
                 "klsRawat": "3",
                 "noMR": "123456",
                 "rujukan": {
                    "asalRujukan": "1",
                    "tglRujukan": "2017-10-17",
                    "noRujukan": "1234567",
                    "ppkRujukan": "00010001"
                 },
                 "catatan": "test",
                 "diagAwal": "A00.1",
                 "poli": {
                    "tujuan": "INT",
                    "eksekutif": "0"
                 },
                 "cob": {
                    "cob": "0"
                 },
                 "jaminan": {
                    "lakaLantas": "1",
                    "penjamin": "1",
                    "lokasiLaka": "Jakarta"
                 },
                 "noTelp": "081919999",
                 "user": "Coba Ws"
              }
           }
        }                    

 */

function panel_form_lookup_Create_SEP(){
    var tmp_kunjungan_unit      = "";
    var tmp_kunjungan_customer  = "";
    var tmp_kunjungan_diagnosa  = "";
    var tmp_kunjungan_tgl_masuk = "";
    var tmp_kunjungan_tgl_masuk = "";

    if (dataKunjunganCell_Select != null) {
        tmp_kunjungan_unit      = dataKunjunganCell_Select.NAMA_UNIT;
        tmp_kunjungan_customer  = dataKunjunganCell_Select.KD_CUSTOMER;
        if (dataKunjunganCell_Select.KD_PENYAKIT != null || dataKunjunganCell_Select.KD_PENYAKIT != "") {
            tmp_diagnosa            = dataKunjunganCell_Select.KD_PENYAKIT;
        }
        tmp_kunjungan_tgl_masuk = dataKunjunganCell_Select.TGL_MASUK;
    }
    var formPanel = new Ext.form.FormPanel({
        id: "myformpanel",
        anchor : '100% 100%',
        height: 450,
        bodyStyle: "padding:5px",
        labelAlign: "right",
        columns  : 2,
        defaults:
        { anchor: "100%" },
        items:[   
            {
                xtype       : 'textfield',
                fieldLabel  : 'Medrec',
                name        : 'txtPasien_medrec',
                id          : 'txtPasien_medrec',
                disabled    : true,
                value       : dataPasienCell_Select.KD_PASIEN,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Nama Pasien',
                name        : 'txtPasien_nama',
                id          : 'txtPasien_nama',
                disabled    : true,
                value       : dataPasienCell_Select.NAMA,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Tgl Lahir',
                name        : 'txtPasien_tgl_lahir',
                id          : 'txtPasien_tgl_lahir',
                disabled    : true,
                value       : dataPasienCell_Select.TGL_LAHIR,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Jenis Kelamin',
                name        : 'txtPasien_jenis_kelamin',
                id          : 'txtPasien_jenis_kelamin',
                disabled    : true,
                value       : dataPasienCell_Select.DESC_JENIS_KELAMIN,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Telepon',
                name        : 'txtPasien_telepon',
                id          : 'txtPasien_telepon',
                disabled    : true,
                value       : dataPasienCell_Select.TELEPON,
            },{
                xtype: 'box',
                autoEl: {tag: 'hr'}
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Customer',
                name        : 'txtPasien_customer',
                id          : 'txtPasien_customer',
                disabled    : true,
                value       : tmp_kunjungan_customer,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Unit',
                name        : 'txtPasien_nama_unit',
                id          : 'txtPasien_nama_unit',
                disabled    : true,
                value       : tmp_kunjungan_unit,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'Tgl Masuk',
                name        : 'txtPasien_tgl_masuk',
                id          : 'txtPasien_tgl_masuk',
                disabled    : true,
                value       : tmp_kunjungan_tgl_masuk,
            },{
                xtype           : 'textfield',
                fieldLabel      : 'Diagnosa',
                name            : 'txtPasien_diagnosa',
                id              : 'txtPasien_diagnosa',
                disabled        : false,
                value           : tmp_kunjungan_diagnosa,
                enableKeyEvents : true,
                listeners:{
                    keydown:function(c,e){
                        if (e.keyCode == 13){
                            params = {
                                text : Ext.getCmp('txtPasien_diagnosa').getValue(),
                            };
                            // loaddatastoreListDiagnosa(params);
                            PilihDiagnosaLookUp_BPJS(params);
                        }   
                    }
                }
            },{
                xtype: 'box',
                autoEl: {tag: 'hr'}
            },{
                xtype       : 'textfield',
                fieldLabel  : 'No Asuransi',
                name        : 'txtPasien_asuransi',
                id          : 'txtPasien_asuransi',
                disabled    : false,
                value       : dataPasienCell_Select.NO_ASURANSI,
                listeners: {
                    specialkey: function(f,e){
                        if (e.getKey() == e.ENTER) {
                            loadMask.show();
                            // update_kunjungan();
                            get_data_bpjs(dataKunjunganCell_Select.KD_UNIT, dataPasienCell_Select.NO_ASURANSI);
                            // if (response_bpjs.data.metaData.code == '200') {
                            //     console.log(response_bpjs);
                            //     Ext.MessageBox.confirm('Informasi', 'SEP di temukan atas nama '+response_bpjs.data.response.peserta.nama+', mulai generate SEP ?', function(btn){
                            //         if (btn == 'yes') {
                            //             generate_bpjs();
                            //         }
                            //     });
                            //     loadMask.hide();
                            // }else{
                            //     ShowPesanInfo_viCreateSEP(response_bpjs.data.metaData.message, "Error");
                            // }
                        }
                    }
                }
            },{
                xtype       : 'checkbox',
                fieldLabel  : 'Lakalantas',
                name        : 'chckPasien_lakalantas',
                id          : 'chckPasien_lakalantas',
                listeners: {
                    check: function (checkbox, isChecked) {
                        if (isChecked === true) {
                            formLookup_Lakalantas_BPJS();
                        }
                        // console.log(isChecked);
                    }
                }
            },{
                xtype       : 'textfield',
                fieldLabel  : 'PPK Rujukan',
                name        : 'txt_ppk_rujukan',
                id          : 'txt_ppk_rujukan',
                disabled    : true,
            },mComboFaskes(),
            {
                xtype       : 'textfield',
                fieldLabel  : 'Catatan',
                name        : 'txtPasien_catatan',
                id          : 'txtPasien_catatan',
                hidden    	: true,
            },{
                xtype       : 'textfield',
                fieldLabel  : 'No SEP',
                name        : 'txtPasien_no_sep',
                id          : 'txtPasien_no_sep',
                value       : dataKunjunganCell_Select.NO_SEP,
                disabled    : true,
            },
        ],
        buttons:
        [
            {
                text: "Cetak SEP",
                handler: function()
                {
                    _bpjs.cetakSep(Ext.getCmp('txtPasien_no_sep').getValue(),undefined,"");
                }
            },
            {
                text: "Pengajuan",
                handler: function()
                {
                    Ext.MessageBox.prompt('Keterangan', 'Keterangan pengajuan : ', function(btn, combo){
                    console.log(btn); 
                        if (btn == 'ok') {
                            pengajuan_sep(combo);
                        }
                    });
                }
            },
            {
                text: "Keluar",
                handler: function()
                {
                    formLookups_CreateSEP.close(); 
                }
            }
        ]
    });
    return formPanel;
}


function mComboFaskes(){
	var cboFaskes = new Ext.form.ComboBox({
		id: 'cbFaskes',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		//allowBlank: false,
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Faskes',
		fieldLabel: 'Faskes',
		width: 50,
		anchor: '95%',
		store: new Ext.data.ArrayStore
		(
			{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				data: [[1, 'Faskes 1'], [2, 'Faskes 2'],]
			}
		),
		valueField: 'Id',
		displayField: 'displayText',
		value: 1,
	});
	return cboFaskes;
};
/* 
    ===============================================================================================================
    ======================================================== Form Lakalantas
    ===============================================================================================================
*/
function formLookup_Lakalantas_BPJS(rowdata){
    var lebar = 500;
    formLookups_Lakalantas = new Ext.Window({
        id: 'formLookup_Lakalantas',
        name: 'formLookup_Lakalantas',
        title: 'Formulir Lakalantas',
        closeAction: 'destroy',
        width: lebar,
        height: 240, //575,
        resizable: false,
        constrain: true,
        autoScroll: false,
        // iconCls: 'Studi_Lanjut',
        modal: true,
        items: panel_form_lookup_lakalantas_BPJS(), 
        listeners:{
            afterShow: function ()
            {
                this.activate();
            }
        }
    });
    formLookups_Lakalantas.show();
}

function panel_form_lookup_lakalantas_BPJS(){
    var formPanel = new Ext.form.FormPanel({
        id: "myformpanel",
        anchor : '100% 100%',
        height: 210,
        bodyStyle: "padding:5px",
        labelAlign: "top",
        defaults:
        {
            anchor: "100%"
        },
        items:
        [   
            {
                // 1=Jasa raharja PT, 2=BPJS Ketenagakerjaan, 3=TASPEN PT, 4=ASABRI PT} jika lebih dari 1 isi -> 1,2 (pakai delimiter koma)
                xtype: 'checkboxgroup',
                fieldLabel: 'Penjamin',
                itemId: 'chck_lakalantas_BPJS',
                columns: 2,
                vertical: true,
                items: [
                    { 
                        boxLabel: 'Jasa Raharja', 
                        name: 'rb1', 
                        id: 'chck_lakalantas_BPJS_1', 
                        inputValue: '1', 
                        checked : lakalantas_checkbox_1_BPJS, 
                        listeners: {
                            check: function (checkbox, isChecked) {
                                lakalantas_checkbox_1_BPJS = isChecked;
                            }
                        }

                    },
                    { 
                        boxLabel: 'BPJS Ketenagakerjaan', 
                        name: 'rb1', 
                        id: 'chck_lakalantas_BPJS_2', 
                        inputValue: '2', 
                        checked : lakalantas_checkbox_2_BPJS,
                        listeners: {
                            check: function (checkbox, isChecked) {
                                lakalantas_checkbox_2_BPJS = isChecked;
                            }
                        }
                    },
                    { 
                        boxLabel    : 'TASPEN PT', 
                        name        : 'rb1', 
                        id          : 'chck_lakalantas_BPJS_3', 
                        inputValue  : '3', 
                        checked     : lakalantas_checkbox_3_BPJS,
                        listeners   : {
                            check   : function (checkbox, isChecked) {
                                lakalantas_checkbox_3_BPJS = isChecked;
                            }
                        }
                    },
                    { 
                        boxLabel    : 'ASABRI PT', 
                        name        : 'rb1', 
                        id          : 'chck_lakalantas_BPJS_4', 
                        inputValue  : '4', 
                        checked     : lakalantas_checkbox_4_BPJS,
                        listeners   : {
                            check   : function (checkbox, isChecked) {
                                lakalantas_checkbox_4_BPJS = isChecked;
                            }
                        }
                    },
                ]
            },{
                xtype       : 'textfield',
                fieldLabel  : "Lokasi",
                name        : 'txtLokasi_Lakalantas',
                id          : 'txtLokasi_Lakalantas',
            }
        ],
        bbar:
        [
            {
                text: "Simpan",
                handler: function()
                {
                    txtLokasi_Lakalantas = Ext.getCmp('txtLokasi_Lakalantas').getValue();
                    formLookups_Lakalantas.close(); 
                }
            }
        ]
    });
    return formPanel;
}


/* 
    ===============================================================================================================
    ======================================================== UPDATE KUNJUNGAN
    ===============================================================================================================
*/
function update_kunjungan(){
                // value       : dataPasienCell_Select.KD_PASIEN,
    Ext.Ajax.request({
        method:'POST',
        url: baseURL+"index.php/rawat_jalan/functionRWJ/update_sjp",
        params:{
        	kd_pasien 	: dataPasienCell_Select.KD_PASIEN,
        	tgl_masuk 	: dataKunjunganCell_Select.TGL_MASUK_BPJS,
        	kd_unit  	: dataKunjunganCell_Select.KD_UNIT,
            urut_masuk  : dataKunjunganCell_Select.URUT,
        	kd_penyakit : tmp_diagnosa,
        	no_sjp 		: Ext.getCmp('txtPasien_no_sep').getValue(),
        },
        success: function(o){
            response = Ext.decode(o.responseText);
			Ext.MessageBox.alert('Informasi', response.message );
            loadMask.hide();
            // if (response.status == true) {
            // }else{
            //     Ext.MessageBox.alert('Informasi', response.message );
            // }

        }
    });
}


/* 
    ===============================================================================================================
    ======================================================== Get Data BPJS
    ===============================================================================================================
*/
function get_data_bpjs(klinik = null, no_kartu = null){
    Ext.Ajax.request({
        method:'POST',
        url: baseURL+"index.php/rawat_jalan/functionRWJ/getDataBpjs",
        params:{
            klinik      : klinik,
            no_kartu    : no_kartu 
        },
        success: function(o){
            response_bpjs = Ext.decode(o.responseText);
            if (response_bpjs.data.metaData.code == '200') {
                console.log(response_bpjs);
                Ext.getCmp('txt_ppk_rujukan').setValue(response_bpjs.data.response.peserta.provUmum.nmProvider);
                Ext.MessageBox.prompt('Informasi', 'BPJS di temukan atas nama '+response_bpjs.data.response.peserta.nama+', mulai generate SEP ?<br>Catatan : ', function(btn, combo){
                // Ext.MessageBox.confirm('Informasi', 'SEP di temukan atas nama '+response_bpjs.data.response.peserta.nama+', mulai generate SEP ?', function(btn){
                    if (btn == 'yes' || btn == 'ok') {
                        generate_bpjs(combo);
                    }
                });
            }else{
                ShowPesanInfo_viCreateSEP(response_bpjs.data.metaData.message, "Error");
            }
            loadMask.hide();
        }
    });
}

/* 
    ===============================================================================================================
    ======================================================== Generate SEP BPJS
    ===============================================================================================================
*/
function generate_bpjs(catatan = ''){
    loadMask.show();
    response_bpjs = response_bpjs.data.response.peserta;

    var lakalan           = 0;
    var pelayanan         = "2";
    var penjamin          = "-";
    var lokasi_Lakalantas = "-";
    var telepon           = "0";
    if (Ext.getCmp('chckPasien_lakalantas').getValue()===true){
        lakalan=1;
        
        penjamin = "";
        if (lakalantas_checkbox_1_BPJS === true || lakalantas_checkbox_1_BPJS == 'true' ) {
            penjamin += "1,";
        }
        
        if (lakalantas_checkbox_2_BPJS === true || lakalantas_checkbox_2_BPJS == 'true' ) {
        penjamin += "2,";
        }
        
        if (lakalantas_checkbox_3_BPJS === true || lakalantas_checkbox_3_BPJS == 'true' ) {
        penjamin += "3,";
        }
        
        if (lakalantas_checkbox_4_BPJS === true || lakalantas_checkbox_4_BPJS == 'true' ) {
        penjamin += "4,";
        }
        
        penjamin          = penjamin.substring(0, (penjamin.length - 1));
        if (txtLokasi_Lakalantas != "" || txtLokasi_Lakalantas != null) {
            lokasi_Lakalantas = txtLokasi_Lakalantas;
        }
    }

    if (dataKunjunganCell_Select.PARENT == '1') {
        pelayanan = "1";
    }

    if (dataPasienCell_Select.TELEPON != "" || dataPasienCell_Select.TELEPON != " ") {
        telepon = dataPasienCell_Select.TELEPON;
    }

    if (dataPasienCell_Select.TELEPON != "" || dataPasienCell_Select.TELEPON != " ") {
        telepon = dataPasienCell_Select.TELEPON;
    }

    // if (dataKunjunganCell_Select.KD_PENYAKIT != "" || dataKunjunganCell_Select.KD_PENYAKIT != null) {
    //     tmp_diagnosa = dataKunjunganCell_Select.KD_PENYAKIT;
    // }

    var datanya='<data>'+
        '<request>' +
            '<t_sep>' +
                '<noKartu>' + dataPasienCell_Select.NO_ASURANSI + '</noKartu>' +
                '<tglSep>' + dataKunjunganCell_Select.TGL_MASUK_BPJS + '</tglSep>' +
                //'<tglSep>2018-03-26</tglSep>' +
                '<ppkPelayanan>' + dataKunjunganCell_Select.PPK_PELAYANAN + '</ppkPelayanan>' +
                '<jnsPelayanan>'+pelayanan+'</jnsPelayanan>' +
                '<klsRawat>'+response_bpjs.hakKelas.kode+'</klsRawat>' +
                '<noMR>'+dataPasienCell_Select.KD_PASIEN+'</noMR>' +
                '<rujukan>'+
                '<asalRujukan>'+Ext.getCmp('cbFaskes').getValue()+'</asalRujukan>'+
                '<ppkRujukan>'+ response_bpjs.provUmum.kdProvider +'</ppkRujukan>'+
                '<noRujukan>0</noRujukan>'+
                '<tglRujukan>'+ dataKunjunganCell_Select.TGL_MASUK_BPJS +'</tglRujukan>'+
                '</rujukan>' +
                '<catatan>dari WS, '+catatan+'</catatan>'+
                '<diagAwal>'+tmp_diagnosa+'</diagAwal>'+
                '<poli>'+
                    '<tujuan>'+dataKunjunganCell_Select.UNIT_BPJS+'</tujuan>'+
                    '<eksekutif>0</eksekutif>'+
                '</poli>'+
                '<cob>'+
                '<cob>0</cob>'+
                '</cob>'+
                '<jaminan>'+
                '<lakaLantas>'+lakalan+'</lakaLantas>'+
                '<penjamin>'+penjamin+'</penjamin>'+
                '<lokasiLaka>'+lokasi_Lakalantas+'</lokasiLaka>'+
                '</jaminan>'+
                '<noTelp>'+telepon+'</noTelp>'+
                '<user>WS</user>'+
            '</t_sep>'+
        '</request>' +
    '</data>';
    var param={
        data:datanya
    }
    $.ajax({
    type: 'POST',
        dataType:'JSON',
        crossDomain: true,
        url: baseURL + "index.php/rawat_jalan/functionRWJ/createSEP/",
        data:param,
        success: function(resp1){
            loadMask.hide();
            if (resp1.metaData.code == '200') {
                Ext.getCmp('txtPasien_no_sep').setValue(resp1.response.sep.noSep);
                update_kunjungan();
            }else{
                Ext.MessageBox.alert('Gagal', resp1.metaData.message );
            }
            loadMask.hide();
        },
        error: function(jqXHR, exception) {
            loadMask.hide();
            Ext.MessageBox.alert('Gagal', 'Nomor SEP tidak ditemukan.');
            loadMask.hide();
            Ext.Ajax.request({
                url:baseURL +"index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                params:{
                    respon:jqXHR.responseText,
                    keterangan:'Error create SEP'
                }
            });
        }
    });
}

function pengajuan_sep(keterangan = ""){
    var pelayanan         = "2";
    if (dataKunjunganCell_Select.PARENT == '1') {
        pelayanan = "1";
    }

    var datanya='<data>'+
        '<request>' +
            '<t_sep>' +
                '<noKartu>' + dataPasienCell_Select.NO_ASURANSI + '</noKartu>' +
                '<tglSep>' + dataKunjunganCell_Select.TGL_MASUK_BPJS + '</tglSep>' +
                '<jnsPelayanan>'+pelayanan+'</jnsPelayanan>' +
                '<keterangan>'+keterangan+'</keterangan>' +
                '<user>WS</user>'+
            '</t_sep>'+
        '</request>' +
    '</data>';

    var param={
        data:datanya
    }

    $.ajax({
    type: 'POST',
        dataType:'JSON',
        crossDomain: true,
        url: baseURL + "index.php/rawat_jalan/functionRWJ/pengajuan_sep/",
        data:param,
        success: function(resp1){
            loadMask.hide();
            if (resp1.metaData.code == '200') {
                Ext.MessageBox.alert('Informasi', resp1.metaData.message );
            }else{
                Ext.MessageBox.alert('Gagal', resp1.metaData.message );
            }
        },
        error: function(jqXHR, exception) {
            loadMask.hide();
            Ext.MessageBox.alert('Gagal', 'Nomor SEP tidak ditemukan.');
            loadMask.hide();
            Ext.Ajax.request({
                url:baseURL +"index.php/rawat_jalan/functionRWJ/save_log_bpjs",
                params:{
                    respon:jqXHR.responseText,
                    keterangan:'Error create SEP'
                }
            });
        }
    });
}


function PilihDiagnosaLookUp_BPJS(data) 
{
    //console.log(dataRowIndexDetail);
    //
        dsDataStoreGridDiagnosa.removeAll(); 
        loaddatastoreListDiagnosa(data);
        varPilihDiagnosaLookUp_BPJS = new Ext.Window
        (
            {
                id: 'idFormDiagnosa_BPJS',
                title: 'Daftar diagnosa',
                closeAction: 'destroy',
                width: 600,
                height: 400,
                border: false,
                resizable: false,
                plain: true,
                layout: 'fit',
                iconCls: 'Request',
                modal: true,
                bodyStyle:'padding: 3px;',
                items: [
                    GridDiagnosa_BPJS(),
                    // paneltotal
                ],
                listeners:
                {         
                    activate: function()
                    {
                            
                    },
                    afterShow: function()
                    {
                        this.activate();

                    },
                    deactivate: function()
                    {
                        
                    },
                    close: function (){
                        /* if(FocusExitResepRWJ == false){
                            var line    = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
                            AptResepRWJ.form.Grid.a.startEditing(line, 4);  
                        } */
                    }
                }
            }
        );
        varPilihDiagnosaLookUp_BPJS.show();
};

    function GridDiagnosa_BPJS(){
        var rowLine = 0;
        var rowData = "";      
        var currentRowSelectionListKunjunganResepRWJ;
        var Field = ['kd_penyakit','penyakit'];
        DataStoreListDiagnosa = new WebApp.DataStore({ fields: Field });
        //loaddatastoreProdukComponent();

        var cm = new Ext.grid.ColumnModel({
            // specify any defaults for each column
            defaults: {
                sortable: false // columns are not sortable by default           
            },
            columns: [
                {
                    header: 'Kode Penyakit',
                    dataIndex: 'kd_penyakit',
                    width: 25,
                }, 
                {
                    header: 'Penyakit',
                    dataIndex: 'penyakit',
                    width: 75,
                }, 
            ]
        });

        gridDiagnosa_BPJS = new Ext.grid.EditorGridPanel
        (
            {
                title: '',
                id: 'gridDiagnosa_BPJS',
                store: dsDataStoreGridDiagnosa,
                clicksToEdit: 1, 
                editable: true,
                border: true,
                columnLines: true,
                frame: false,
                stripeRows       : true,
                trackMouseOver   : true,
                height: 250,
                width: 560,
                autoScroll: true,
                sm: new Ext.grid.CellSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                        }
                    }
                ),
                sm: new Ext.grid.RowSelectionModel
                (
                    {
                        singleSelect: true,
                        listeners:
                        {
                            rowselect: function (sm, row, rec)
                            {
                                rowLine = row;
                                currentRowSelectionListKunjunganResepRWJ = undefined;
                                currentRowSelectionListKunjunganResepRWJ = dsDataStoreGridDiagnosa.getAt(row);
                            }
                        }
                    }
                ),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },
                listeners: {
                    'keydown' : function(e, d){
                        if(e.getKey() == 13){
                            tmp_diagnosa = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
                            console.log(tmp_diagnosa);
                            // console.log(currentRowSelectionListKunjunganResepRWJ.data);
                            // autocomdiagnosa_masterrwi = currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit;
                            Ext.getCmp('txtPasien_diagnosa').setValue(currentRowSelectionListKunjunganResepRWJ.data.kd_penyakit+" - "+currentRowSelectionListKunjunganResepRWJ.data.penyakit);
                            varPilihDiagnosaLookUp_BPJS.close();
                            Ext.getCmp('txtPasien_diagnosa').focus();
                        }
                    }
                }  
            }
        );
        return gridDiagnosa_BPJS;
    };


    function loaddatastoreListDiagnosa(params){
        // console.log(params);
        // console.log(params);
        Ext.Ajax.request({
            url: baseURL +  "index.php/main/functionRWJ/getPenyakit",
            params: params,
            success: function(response) {
                var cst  = Ext.decode(response.responseText); 
                // PilihDiagnosaLookUp_BPJS();
                if (cst['listData'].length > 0) {
                    for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                        var recs    = [],recType = DataStoreListDiagnosa.recordType;
                        var o       = cst['listData'][i];
                        recs.push(new recType(o));
                        dsDataStoreGridDiagnosa.add(recs);
                    }

                    Ext.getCmp('gridDiagnosa_BPJS').getView().refresh();
                    Ext.getCmp('gridDiagnosa_BPJS').getSelectionModel().selectRow(0);
                    Ext.getCmp('gridDiagnosa_BPJS').getView().focusRow(0);
                }else{
                    varPilihDiagnosaLookUp_BPJS.close();
                    Ext.MessageBox.alert('Informasi', 'Tindakan '+params.text+' tidak ada');
                    Ext.getCmp('txtPasien_diagnosa').focus();
                }
            },
        });
    }
