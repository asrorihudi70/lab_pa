var NamaForm_viSetupPenyebabKematian="Setup Penyebab Kematian";
var mod_name_viSetupPenyebabKematian="Setup Penyebab Kematian";
var selectCount_viSetupPenyebabKematian=50;
var now_viSetupPenyebabKematian= new Date();
var tanggal = now_viSetupPenyebabKematian.format("d/M/Y");
var jam = now_viSetupPenyebabKematian.format("H/i/s");

var dataSource_viSetupPenyebabKematian;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupPenyebabKematian;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupPenyebabKematian;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;

var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;

var CurrentData_viSetupPenyebabKematian =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupPenyebabKematian={};
SetupPenyebabKematian.form={};
SetupPenyebabKematian.func={};
SetupPenyebabKematian.vars={};
SetupPenyebabKematian.func.parent=SetupPenyebabKematian;
SetupPenyebabKematian.form.ArrayStore={};
SetupPenyebabKematian.form.ComboBox={};
SetupPenyebabKematian.form.DataStore={};
SetupPenyebabKematian.form.Record={};
SetupPenyebabKematian.form.Form={};
SetupPenyebabKematian.form.Grid={};
SetupPenyebabKematian.form.Panel={};
SetupPenyebabKematian.form.TextField={};
SetupPenyebabKematian.form.Button={};

SetupPenyebabKematian.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupPenyebabKematian.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupPenyebabKematian(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupPenyebabKematian(mod_id_viSetupPenyebabKematian){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupPenyebabKematian = 
	[
		'kd_item','item_hd','type_item','type_item_des'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupPenyebabKematian = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupPenyebabKematian
    });
	dataGridItemPenyebabKematian();
//	dataGridRefHasil();
	//dataGridHasilDialisa(0);
	//loadDataComboItemHasil();
	var FieldItem=['kd_item', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupPenyebabKematian = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupPenyebabKematian,
			autoScroll: false,
			columnLines: true,
			border:true,
			flex:1,
			border:true,
			height:570,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupPenyebabKematian = undefined;
							rowSelected_viSetupPenyebabKematian = dataSource_viSetupPenyebabKematian.getAt(row);
							CurrentData_viSetupPenyebabKematian
							CurrentData_viSetupPenyebabKematian.row = row;
							CurrentData_viSetupPenyebabKematian.data = rowSelected_viSetupPenyebabKematian.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupPenyebabKematian = dataSource_viSetupPenyebabKematian.getAt(ridx);
					if (rowSelected_viSetupPenyebabKematian != undefined)
					{
						DataInitSetupPenyebabKematian(rowSelected_viSetupPenyebabKematian.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viPenyebabKematian',
						header: 'Kode',
						dataIndex: 'kd_penyebab', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viPenyebabKematian',
						header: 'Penyebab',
						dataIndex: 'penyebab',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMerkAmbulance',
				items: 
				[
					/*{
						xtype: 'button',
						text: 'Edit Dataa',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupDokterAmbulance',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupPenyebabKematian != undefined)
							{
								DataInitSetupPenyebabKematian(rowSelected_viSetupPenyebabKematian.data);
							}
							
						}
					}*/
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPenyebabKematian, selectCount_viSetupPenyebabKematian, dataSource_viSetupPenyebabKematian),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:250,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitSetupPenyebabKematian(rowSelected_viSetupPenyebabKematian.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupPenyebabKematian(rowSelected_viSetupPenyebabKematian.data);
					}
					else
					{
						//setLookUp_viSetupPenyebabKematian();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupReferensiHasil',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPenyebabKematian, selectCount_viSetupPenyebabKematian, dataSource_viSetupPenyebabKematian),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupPenyebabKematian.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPenyebabKematian, selectCount_viSetupPenyebabKematian, dataSource_viSetupPenyebabKematian),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupHasil = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupPenyebabKematian()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupPenyebabKematian',
						handler: function(){
							AddNewSetupPenyebabKematian();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupPenyebabKematian',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupPenyebabKematian();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupPenyebabKematian',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupPenyebabKematian();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					/*{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnCari_SetupPenyebabKematian',
						handler: function()
						{
							dataSource_viSetupPenyebabKematian.removeAll();
							dataGridItemPenyebabKematian();
						}
					},*/

					{
						xtype: 'button',
						text: 'Cari',
						iconCls: 'find',
						id: 'btnRefresh_SetupHasil',
						handler: function()
						{
							loadMask.show();
							pencatian_viSetupPenyebabKematian();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	
	
	
	
	/*
	var FrmHasilTest= new Ext.Panel(
	{
        layout: {
        type: 'accordion',
        titleCollapse: true,
        multi: true,
        fill: false,
        animate: false, 
		id: 'accordionNavigationContainerHemodialisa',
        flex: 1
        },

        
		width: 1000,
		height: 900,
		defaults: {
		},
		layoutConfig: {
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [{
			title: 'Dokter Ambulance',
			height: 750,
			items:[
				PanelInputSetupHasil,
				GridDataView_viSetupPenyebabKematian
			]
		}]
	}
	);*/
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupPenyebabKematian = new Ext.Panel
    (
		{
			title: 'Setup Penyebab Kematian',
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupPenyebabKematian,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [{
			title: '',
			height: 750,
				items:[
					PanelInputSetupHasil,
					GridDataView_viSetupPenyebabKematian
				]
		}]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupPenyebabKematian;
    //-------------- # End form filter # --------------
}

function GetFormSetupPenyebabKematian(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 60,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdPenyebabKematian',
								name: 'txtKdPenyebabKematian',
								width: 100,
								allowBlank: false,
								readOnly: true,
								disable:true,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Penyebab'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtnama_penyebab_kematian',
								name: 'txtnama_penyebab_kematian',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							}
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};


function dataGridItemPenyebabKematian(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_jenazah/setup_penyebab_kematian/getGridPenyebabKematian",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupPenyebabKematian.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupPenyebabKematian.add(recs);
					GridDataView_viSetupPenyebabKematian.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupPenyebabKematian(){
	Ext.getCmp('txtKdPenyebabKematian').setValue('');
	Ext.getCmp('txtnama_penyebab_kematian').setValue('');
};

function dataSave_viSetupPenyebabKematian(){
	if (ValidasiSaveSetupMerkAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_jenazah/setup_penyebab_kematian/save",
				params: getParamSaveSetupPenyebabKematian(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information')
						dataSource_viSetupPenyebabKematian.removeAll();
						dataGridItemPenyebabKematian();
						loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupPenyebabKematian.removeAll();
						dataGridItemPenyebabKematian();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupPenyebabKematian(){
	var	params =
	{
		kd_penyebab:Ext.getCmp('txtKdPenyebabKematian').getValue(),
		penyebab:Ext.getCmp('txtnama_penyebab_kematian').getValue(),		
	}
    return params
};

function ValidasiSaveSetupMerkAmbulance(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtnama_penyebab_kematian').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Penyebab masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupHasil(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupPenyebabKematian(){
	if (ValidasiSaveSetupMerkAmbulance(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_jenazah/setup_penyebab_kematian/delete",
				params: getParamDeleteSetupMerkAmbulance(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupPenyebabKematian()
						dataSource_viSetupPenyebabKematian.removeAll();
						dataGridItemPenyebabKematian();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupPenyebabKematian.removeAll();
						dataGridItemPenyebabKematian();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupMerkAmbulance(){
	var	params =
	{
		kd_penyebab:Ext.getCmp('txtKdPenyebabKematian').getValue()
	}
   
    return params
};

function DataInitSetupPenyebabKematian(rowdata){
	console.log(rowdata);
	Ext.getCmp('txtKdPenyebabKematian').setValue(rowdata.kd_penyebab);
	Ext.getCmp('txtnama_penyebab_kematian').setValue(rowdata.penyebab);
};


function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupHasil('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupPenyebabKematian/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params  
};
function pencatian_viSetupPenyebabKematian(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_jenazah/setup_penyebab_kematian/search",
				params: getParamSaveSetupPenyebabKematian(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) {
				var cst = Ext.decode(o.responseText);
				console.log(cst);
					if (cst.success === true) 
					{
						dataSource_viSetupPenyebabKematian.removeAll();
						var recs=[],
							recType=dataSource_viSetupPenyebabKematian.recordType;
							
						for(var i=0; i<cst.listData.length; i++){
							recs.push(new recType(cst.listData[i]));
						}
						dataSource_viSetupPenyebabKematian.add(recs);
						GridDataView_viSetupPenyebabKematian.getView().refresh();
						loadMask.hide();
					}
					else 
					{
						ShowPesanErrorSetupHasil('Gagal membaca data', 'Error');
					}
			}
			}
			
		)
	
}

