// Data Source ExtJS

/**
 *	Nama File 		: TRInformasiTarif.js
 *	Menu 			: PenGroupUseran
 *	Model id 		: 030103
 *	Keterangan 		: Untuk View Informasi Tarif
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 */

// Deklarasi Variabel pada ExtJS
var dsDataGroup_list;
var kode_tarif;
var addNew_viUser;
var rowSelected_user;
var Today_PenGroupUseran = new Date;
var kode_produk;
var kode_user;
var tgl_berlaku;
var dsInfotarifList;
var dsInfoGroupUser;
var rowSelected_viUsergroup;
var rowSelected_viGroup;
var selectCountInfoGroupUser = 100;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_viGrUser;
var dataSource_viGrUser;
var selectCount_viGrUser = 50;
var NamaForm_viGrUser = "Informasi Tarif";
var icons_viGrUser = "Gaji";
var addNew_viGrUser;
var rowSelected_viGrUser;
var rowSelected_viGrUserList;
var setLookUps_viGrUser;
var setLookUps_viGrUserList;
var now_viGrUser = new Date();
var dataSource_viInformasiGroupUser;
var dsDataStoreGridSetupGroup = new Ext.data.JsonStore();

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_viGrUser =
        {
            data: Object,
            details: Array,
            row: 0
        };

CurrentPage.page = dataGrid_viGrUser(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// End Shortcut Key

// Start Project Informasi Tarif

/**
 *	Function : dataGrid_viGrUser
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */
function dataGrid_viGrUser(mod_id)
{

    // Field kiriman dari Project Net.
    var Field = ['GROUP_ID', 'GROUP_DESC', 'GROUP_NAME'];
    dataSource_viInformasiGroupUser = new WebApp.DataStore({fields: Field});
    datarefresh_viGroupUser();

    var grListSetTypeBayar = new Ext.grid.EditorGridPanel
            (
                    {
                        id: 'grListSetTypeBayar',
                        stripeRows: false,
                        title: 'Group Name',
                        store: dataSource_viInformasiGroupUser,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        //height:300,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        rowSelected_viGroup = dataSource_viInformasiGroupUser.getAt(ridx);

                                        group_id = rowSelected_viGroup.data.GROUP_ID;
                                        loaddatastore_combo_module(group_id);
                                        RefreshDataGroupTrustee();
                                    },
                                    rowdblclick: function (sm, ridx, cidx)
                                    {
                                        rowSelected_viGroup = dataSource_viInformasiGroupUser.getAt(ridx);
                                        if (rowSelected_viGroup != undefined)
                                        {
                                            setLookUp_viGroup(rowSelected_viGroup.data);
                                        } else
                                        {
                                            setLookUp_viGroup();
                                        }
                                    }
                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                id: 'colnamaUsermane',
                                                header: 'Group ID',
                                                dataIndex: 'GROUP_ID',
                                                width: 150,
                                                menuDisabled: true,
                                                sortable: true,
                                                filter: {}
                                            },
                                            {
                                                id: 'colGroupName',
                                                header: 'Group Name',
                                                dataIndex: 'GROUP_NAME',
                                                width: 200,
                                                menuDisabled: true,
                                                sortable: true,
                                                filter: {}
                                            },
                                            {
                                                //'','','','','','TGL_BERLAKU',''
                                                id: 'colgroupdesc',
                                                header: 'Group Desc',
                                                dataIndex: 'GROUP_DESC',
                                                hidden: false,
                                                width: 220,
                                                menuDisabled: true,
                                                sortable: true,
                                                filter: {}
                                            },
                                        ]
                                        //'','','','','','',''
                                        ),
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    id: 'toolbar_viGroupUser',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Group Baru',
                                                    iconCls: 'Edit_Tr',
                                                    hidden: false,
                                                    tooltip: 'Edit Data',
                                                    id: 'btntambah_viGroupUser',
                                                    handler: function (sm, row, rec)
                                                    {

                                                        setLookUp_viGroup();

                                                    }
                                                },
												{
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnDelete_UserDepan',
                                                   // enable: false,
                                                    handler: function ()
                                                    {
                                                        SetUserGroupDepan();
                                                        //RefreshDataSetJadwalDokter();
                                                    }
                                                },
//                                                {
//                                                    xtype: 'button',
//                                                    text: 'Refresh',
//                                                    iconCls: 'Edit_Tr',
//                                                    hidden: false,
//                                                    tooltip: 'Segarkan',
//                                                    id: 'btnsegarkan_viGroupUser',
//                                                    handler: function (sm, row, rec)
//                                                    {
//                                                        datarefresh_viGroupUser();
//                                                    }
//                                                },
                                            ]},
                        viewConfig: {forceFit: false}
                    }
            );
    // Kriteria filter pada Grid
    var Field = ['MOD_ID', 'MOD_NAME'];
    dsInfogroupGroupTrustee = new WebApp.DataStore({fields: Field});


    var grListSetTrusteeGrupUser = new Ext.grid.EditorGridPanel
            (
                    {
                        id: 'grListSetTrusteeGrupUser',
                        stripeRows: false,
                        title: 'Trustee',
                        store: dsInfogroupGroupTrustee,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        //height:310,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                    }
                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                id: 'colsetTrustee',
                                                header: 'Trustee',
                                                dataIndex: 'MOD_NAME',
                                                width: 550,
                                                menuDisabled: true,
                                                sortable: true,
                                                filter: {}
                                            },
                                            {
                                                id: 'colsetgroupId',
                                                header: 'Group',
                                                dataIndex: 'MOD_ID',
                                                menuDisabled: true,
                                                sortable: true,
                                                hidden: true
                                            },
                                        ]
                                        //'','','','','','',''
                                        ),

                        tbar:{
                            xtype: 'toolbar',
                            items: 
                            [
                            ComboGroupTrusteeChecked(),
                        ]
                        }


                        , viewConfig: {forceFit: false}
                    }
            );
    // Kriteria filter pada Grid

    var FrmTabs_viGrUser = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        title: 'Group Users',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding:15px',
                        border: false,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        margins: '0 5 5 0',
                        anchor: '100% 100%',
                        iconCls: icons_viGrUser,
                        items:
                                [
                                    {
                                        columnWidth: .50,
                                        height: 600,
                                        layout: 'form',
                                        border: false,
                                        autoScroll: true,
                                        bodyStyle: 'padding:6px 3px 3px 6px',
                                        items:
                                                [
                                                    //grData_viGrUser
                                                    grListSetTypeBayar
                                                ]
                                    },
                                    {
                                        columnWidth: .50,
                                        height: 600,
                                        layout: 'form',
                                        bodyStyle: 'padding:6px 6px 3px 3px',
                                        border: false,
                                        anchor: '100% 100%',
                                        items:
                                                [
                                                    grListSetTrusteeGrupUser

                                                ]
                                    }
                                ]

                    }
            )
    // datarefresh_viGrUser();
    return FrmTabs_viGrUser;
}







function ShowPesanWarning_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}

function ShowPesanError_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            )
}

function ShowPesanInfo_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}










function datainit_viGrUser(rowdata)
{

}


function dataaddnew_viGrUser()
{

}
///---------------------------------------------------------------------------------------///


/**
 *	Function : gridDataForm_viGrUser
 *	
 *	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
 *	yang di pangil dari Function getFormItemEntry_viGrUser
 */
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_viGroupUser()
{
    dataSource_viInformasiGroupUser.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCount_viGrUser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'viInfoGroupUser',
                                    param: ''
                                }
                    }
            );

    return dataSource_viInformasiGroupUser;
    //alert("refersh")
}

function datarefresh_viGrUserFilter()
{
    var KataKunci = '';





    if (Ext.get('txtfilternama_viGrUser').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viGrUser').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viGrUser').getValue() + '%~)';
        }
        ;

    }
    ;

    if (Ext.get('txtfilpoli_viGrUser').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viGrUser').getValue() + '%~)';

        } else
        {

            KataKunci += ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viGrUser').getValue() + '%~)';
        }
        ;

    }
    ;




    if (KataKunci != undefined)
    {
        dataSource_viGrUser.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCount_viGrUser,
                                        Sort: 'KD_DOKTER',
                                        Sortdir: 'ASC',
                                        target: 'viInfoTarif',
                                        param: ''
                                    }
                        }
                );
    } else
    {
        datarefresh_viGroupUser();
    }
    ;
}
;

function RefreshDataUserGroup()
{
    dsInfoGroupUser.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountInfoGroupUser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewGroupUser',
                                    param: 'u.kd_user =~' + kode_user + '~'
                                }
                    }
            );

    rowSelectedInfotarif = undefined;
    return dsInfoGroupUser;
}
;

function RefreshDataGroupTrustee()
{
    dsInfogroupGroupTrustee.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountInfoGroupUser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewTrusteeGroup',
                                    param: 'g.group_id =~' + group_id + '~'
                                }
                    }
            );

    rowSelectedInfotarif = undefined;
    return dsInfogroupGroupTrustee;
}
;


function setLookUp_viGroup(rowdata)
{
    var lebar = 600;
    setLookUps_viUser = new Ext.Window
            (
                    {
                        id: 'SetLookUps_viGroupUser',
                        name: 'SetLookUps_viGroupUser',
                        title: 'Form User',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 250, //575,
                        resizable: false,
                        autoScroll: false,
                        iconCls: 'Studi_Lanjut',
                        modal: true,
                        items: getFormItemEntry_viGroupUser(lebar, rowdata), //1
                        listeners:
                                {
                                    activate: function ()
                                    {
                                        //alert(Ext.get('txtNamaPasien').getValue());
                                        //Ext.getCmp('txtNama').focus(true, 10);

                                    },
                                    afterShow: function ()
                                    {
                                        // this.activate();

                                    },
                                    deactivate: function ()
                                    {
                                    }
                                }
                    }
            );

    setLookUps_viUser.show();
    if (rowdata === undefined)
    {
        //alert(rowdata);
        dataaddnew_viGroup();
    } else
    {
        //console.log(rowdata);
        dataaddnew_viGroup(rowdata);
    }

}

function getFormItemEntry_viGroupUser(lebar, rowdata)
{
    var pnlFormDataBasic_viGroupUser = new Ext.FormPanel
            (
                    {
                        title: '',
                        region: 'north', //'center',
                        layout: 'form', //'anchor',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        anchor: '100%',
                        autoScroll: true,
                        width: lebar, //lebar-55,
                        height: 400,
                        border: false,
                        items: [
                            getItemPanelInputBiodata_viGroupUser(),
                                    //getPenelItemDataKunjungan_viGroupUser(lebar)
                        ],
                        fileUpload: true,
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Add',
                                                    iconCls: 'add',
                                                    id: 'btnAdd_viGroupUser',
                                                    handler: function () {
                                                        dataaddnew_viGroup();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viGroupUser',
                                                    handler: function ()
                                                    {
                                                        datasave_User(addNew_viUser);
                                                        datarefresh_viGroupUser();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Save & Close',
                                                    iconCls: 'saveexit',
                                                    id: 'btnSimpanExit_viGroupUser',
                                                    handler: function ()
                                                    {
														datasave_User(addNew_viUser);
														datarefresh_viGroupUser();
														setLookUps_viUser.close();
                                                        /*var x = datasave_viGroupUser(addNew_viGroupUser);
                                                        datarefresh_viGroupUser(tmpcriteriaRWJ);
                                                        if (x === undefined)
                                                        {
                                                        }*/
                                                    }
                                                },
                                                
                                            ]
                                }
//                        ,items:

                    }
            )


    return pnlFormDataBasic_viGroupUser;
}


//form view data biodata pasien
function getItemPanelInputBiodata_viGroupUser()
{

    var items =
            {
                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
                        [
                            {
                                columnWidth: 1.0,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Group ID ',
                                                name: 'txtgroupid',
                                                id: 'txtgroupid',
                                                readOnly: false,
                                                hidden: false,
                                                tabIndex: 1,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Group Name ',
                                                name: 'txtgroupname',
                                                id: 'txtgroupname',
                                                tabIndex: 2,
                                                readOnly: false,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Group Description ',
                                                name: 'txtgroupdesc',
                                                id: 'txtgroupdesc',
                                                tabIndex: 3,
                                                disabled: false,
                                                anchor: '95%',
                                            }

                                        ]
                            },
                        ]
            };
    return items;
}
;
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;


function dataaddnew_viGroup(rowdata)
{
    if (rowdata === undefined)
    {
        Ext.getCmp('txtgroupid').focus('false', 10);
        //addNew_viGroupUser = true;
        Ext.getCmp('txtgroupid').setValue('');
        Ext.getCmp('txtgroupname').setValue('');
        Ext.getCmp('txtgroupdesc').setValue('');
        Ext.getCmp('btnAdd_viGroupUser').enable();
        Ext.getCmp('btnSimpan_viGroupUser').enable();
        Ext.getCmp('btnSimpanExit_viGroupUser').enable();
        Ext.getCmp('btnDelete_User').disable();
        addNew_viUser = true;
    } else
    {
        Ext.getCmp('txtgroupid').focus('false', 10);
        //addNew_viGroupUser = true;
        Ext.getCmp('txtgroupid').setValue(rowdata.GROUP_ID);
        Ext.getCmp('txtgroupid').disable();
        Ext.getCmp('txtgroupname').setValue(rowdata.GROUP_NAME);
        Ext.getCmp('txtgroupdesc').setValue(rowdata.GROUP_DESC);
        Ext.getCmp('btnAdd_viGroupUser').enable();
        Ext.getCmp('btnSimpan_viGroupUser').enable();
        Ext.getCmp('btnSimpanExit_viGroupUser').enable();
        Ext.getCmp('btnDelete_User').enable();
        addNew_viUser = false;

    }

    // rowSelected_viGroupUser   = undefined;
}

function datasave_User(mBol)
{
    if (ValidasiEntry_Group('Simpan Data', false) == 1)
    {
        //alert('a')
        if (addNew_viUser == true)
        {
            Ext.Ajax.request
                    (
                            {
                                url: baseURL + "index.php/main/CreateDataObj",
                                params: dataparam_User(),
                                success: function (o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfo_viUserInfo('Data berhasil di simpan', 'Simpan Data');
                                        datarefresh_viGroupUser();
                                        //datarefresh_viGroupUser(tmpcriteriaRWJ);
                                        addNew_viGroupUser = false;
                                        //Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
                                        //                          printbill();
                                        Ext.getCmp('btnSimpan_viGroupUser').disable();
                                        Ext.getCmp('btnSimpanExit_viGroupUser').disable();
                                        //Ext.getCmp('btnDelete_User').enable();
                                    } else if (cst.success === false && cst.pesan === 0)
                                    {
                                        ShowPesanWarning_viUserInfo('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                                    } else if (cst.success === false && cst.cari === true)
                                    {
                                        ShowPesanWarning_viUserInfo('Anda telah berkunjung sebelumnya ', 'Simpan Data');
                                    } else
                                    {
                                        ShowPesanError_viUserInfo('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                                    }
                                }
                            }
                    )
        } else
        {
            Ext.Ajax.request
                    (
                            {
                                url: WebAppUrl.UrlUpdateData,
                                params: dataparam_User(),
                                success: function (o)
                                {
                                    //alert(o);
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfo_viUserInfo('Data berhasil disimpan', 'Edit Data');
                                        datarefresh_viGroupUser();
                                    } else if (cst.success === false && cst.pesan === 0)
                                    {
                                        ShowPesanWarning_viUserInfo('Data tidak berhasil disimpan ' + cst.pesan, 'Edit Data');
                                    } else
                                    {
                                        ShowPesanError_viUserInfo('Data tidak berhasil disimpan ' + cst.pesan, 'Edit Data');
                                    }
                                }
                            }
                    )
        }
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function ValidasiEntry_Group(modul, mBolHapus)
{
    var x;

    if (Ext.get('txtgroupid').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("group ID belum terisi...", modul);
        x = 0;
    } else if (Ext.get('txtgroupname').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("Group Name belum terisi...", modul);
        x = 0;
    } else
    {
        x = 1;
    }

    return x;
}

function dataparam_User()
{
    var params_ViPenGroupUseran =
            {
                Table: 'ViewGroupUser',
                group_id: Ext.getCmp('txtgroupid').getValue(),
                group_name: Ext.getCmp('txtgroupname').getValue(),
                group_desc: Ext.getCmp('txtgroupdesc').getValue(),
            };
    return params_ViPenGroupUseran
}


function SetUserGroupDepan()
{
    //if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
    //{
	var jumlahTrustee=dsInfogroupGroupTrustee.getCount();
	if (jumlahTrustee===0)
	{
		Ext.Msg.show
            (
                    {
                        title: nmHeaderHapusData,
                        msg: "Apakah yakin akan menghapus data ini ?",
                        buttons: Ext.MessageBox.YESNO,
                        width: 250,
                        fn: function (btn)
                        {
                            if (btn === 'yes')
                            {
                                Ext.Ajax.request
                                        (
                                                {
                                                    url: WebAppUrl.UrlDeleteData,
                                                    params: getParamRequest2(),
                                                    success: function (o)
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true)
                                                        {
                                                            ShowPesanInfo_viUserInfo("Data berhasil dihapus", "Hapus Data");
                                                            datarefresh_viGroupUser();
                                                            //setLookUps_viUser.close();
                                                            //dataaddnew_viGroup();

                                                        } else if (cst.success === false && cst.pesan === 0)
                                                        {
                                                            ShowPesanWarning_viUserInfo("Data gagal dihapus",  "Hapus Data");
                                                        } else
                                                        {
                                                            ShowPesanWarning_viUserInfo("Data gagal dihapus",  "Hapus Data");
                                                        }
                                                        ;
                                                    }
                                                }
                                        )
                            }
                            ;
                        }
                    }
            )
	}
	else
	{
		ShowPesanWarning_viUserInfo("Data tidak bisa dihapus",  "Hapus Data");
	}
    
}
;
function SetUserGroup()
{
    //if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
    //{
    Ext.Msg.show
            (
                    {
                        title: nmHeaderHapusData,
                        msg: "Apakah yakin akan menghapus data ini ?",
                        buttons: Ext.MessageBox.YESNO,
                        width: 250,
                        fn: function (btn)
                        {
                            if (btn === 'yes')
                            {
                                Ext.Ajax.request
                                        (
                                                {
                                                    url: WebAppUrl.UrlDeleteData,
                                                    params: getParamRequest2(),
                                                    success: function (o)
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true)
                                                        {
                                                            ShowPesanInfo_viUserInfo("Data berhasil dihapus", "Hapus Data");
                                                            datarefresh_viGroupUser();
                                                            setLookUps_viUser.close();
                                                            dataaddnew_viGroup();

                                                        } else if (cst.success === false && cst.pesan === 0)
                                                        {
                                                            ShowPesanWarning_viUserInfo("Data gagal dihapus", "Hapus Data");
                                                        } else
                                                        {
                                                            ShowPesanWarning_viUserInfo("Data gagal dihapus", "Hapus Data");
                                                        }
                                                        ;
                                                    }
                                                }
                                        )
                            }
                            ;
                        }
                    }
            )
}
;


function getParamRequest2()
{
    var params =
            {
                Table: 'ViewGroupUser',
                group_id: group_id//Ext.get('txtgroupid').getValue(),
            };
    return params
}
;



    function loaddatastore_combo_module(group_id){
        dsDataStoreGridSetupGroup.removeAll();
        var Field = ['mod_group','mod_img','mod_row'];
        var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
        Ext.Ajax.request({
            // url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
            url: baseURL +  "index.php/userman/setup_user/getComboSetTrustee",
            params: {
                group_id    : group_id,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst       = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                    var recs = [],recType = DataStoreGridKomponent.recordType;
                    var o    = cst['ListDataObj'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridSetupGroup.add(recs);
                }
            },
        });
    }


function ComboGroupTrusteeChecked(){
    dsDataGroup_list = new WebApp.DataStore({fields: ['MOD_GROUP']})
    // LoadGroupUser();
    cboKelasUnit_InfoPasienRWI = new Ext.form.ComboBox
    (
        {
            id: 'cboKelasUnit_InfoPasienRWI',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            fieldLabel: 'Kelas ',
            store: dsDataStoreGridSetupGroup,
            valueField: 'MOD_GROUP',
            displayField: 'MOD_GROUP',
            width:150,
            listeners:
            {
                'select': function(a, b, c)
                {
                    loaddatastore_set_zmodule(b.data.MOD_GROUP, group_id);
                    // console.log(group_id);
                    // console.log(b.data);
                }
            }
        }
    )
    return cboKelasUnit_InfoPasienRWI;
} 


    function loaddatastore_set_zmodule(mod_group, group_id){
        dsInfogroupGroupTrustee.removeAll();
        var Field = ['mod_group','mod_img','mod_row'];
        var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
        Ext.Ajax.request({
            // url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
            url: baseURL +  "index.php/userman/setup_user/getSetupZmodule",
            params: {
                mod_group   : mod_group,
                group_id    : group_id,
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst       = Ext.decode(response.responseText);
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
                    var recs = [],recType = DataStoreGridKomponent.recordType;
                    var o    = cst['ListDataObj'][i];
                    recs.push(new recType(o));
                    dsInfogroupGroupTrustee.add(recs);
                }
            },
        });
    }