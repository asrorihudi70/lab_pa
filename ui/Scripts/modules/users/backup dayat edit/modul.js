// Data Source ExtJS

/**
*	Nama File 		: TRInformasiTarif.js
*	Menu 			: PenMemberUseran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var group_id;
var kode_tarif;
var addNew_viUser;
var rowSelected_user;
var Today_PenMemberUseran = new Date;
var kode_produk;
var kode_user;
var tgl_berlaku;
var dsInfotarifList;
var dsInfomodul;
var rowSelected_viUsergroup;
var rowSelected_viGroup;
var selectCountInfomodul=100;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_vimodul;
var dataSource_vimodul;
var selectCount_vimodul=50;
var NamaForm_vimodul="Informasi Tarif";
var icons_vimodul="Gaji";
var dsInfomodulTrustee;
var mRecordmodul = Ext.data.Record.create
(
  [
   //','MOD_NAME
       {name: 'MOD_ID', mapping:'MOD_ID'},
       {name: 'MOD_NAME', mapping:'MOD_NAME'},
       {name: 'CHECK', mapping:'CHECK'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var mRecordmoduldelete = Ext.data.Record.create
(
  [
   
       {name: 'MOD_ID', mapping:'MOD_ID'},
       {name: 'MOD_NAME', mapping:'MOD_NAME'},
       {name: 'CHECK', mapping:'CHECK'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);




var now_vimodul = new Date();
var dataSource_viInformasiGroupMember;

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_vimodul =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGruopUser(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viGruopUser
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
function dataGrid_viGruopUser(mod_id)
{

    // Field kiriman dari Project Net.
   var Field =['GROUP_ID','GROUP_NAME','GROUP_DESC'];
     dataSource_viInformasiGroupMember = new WebApp.DataStore({ fields: Field });
     datarefresh_vimodul();
	
     var grListSetmodul = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetmodul',
            stripeRows: false,
			title: 'Group Name',
            store: dataSource_viInformasiGroupMember,
            autoScroll: true,
            columnLines: true,
            border: true,
			height:150,
            anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
						
							
                           
                        }
					}
				),
            listeners:
			{
			rowclick: function (sm, ridx, cidx)
				{
						rowSelected_viGroup = dataSource_viInformasiGroupMember.getAt(ridx);
                                       
						group_id =rowSelected_viGroup.data.GROUP_ID;
									
						RefreshDataUser();
							//pindahMOD_ID();
						 RefreshDataUser2();
						 Ext.getCmp('btnAdd_viMemberUser').enable();
						Ext.getCmp('btnDelete_User').enable();
				}
				
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
							
							{
							id: 'colgroupid',
							header: 'Group ID',
							dataIndex: 'GROUP_ID',
							menuDisabled:true,
							sortable: true,
							hidden:true,
							filter:{}
							},
							
							{
							id: 'colGroupName',
							header: 'Group Name',
							dataIndex: 'GROUP_NAME',
							width: 200,
							menuDisabled:true,
							sortable: true,
							filter:{}
							},
							{
							//'','','','','','TGL_BERLAKU',''
							id: 'colgroupdesc',
							header: 'Group Desc',
							dataIndex: 'GROUP_DESC',
							hidden:false,
							width: 220,
							menuDisabled:true,
							sortable: true,
							filter:{}
							},
						
							
		
				]
			//'','','','','','',''
            ),
			tbar:
                            {
				xtype: 'toolbar',
				id: 'toolbar_viMemberUser',
				items:
                                    [
                                          
                                        {
						xtype: 'button',
						text: 'Group Baru',
						iconCls: 'Edit_Tr',
                                                hidden : true,
						tooltip: 'Edit Data',
						id: 'btntambah_viMemberUser',
						handler: function(sm, row, rec)
						{

									setLookUp_viGroup();															
																																		 
						}
					},
                                    ]},
	
			
		    viewConfig: { forceFit: false}
		}
	); 
	// Kriteria filter pada Grid



   
	
	var Field =['MOD_ID','MOD_NAME','CHECK','URUT_MASUK'];
	
     dsInfomodulTrustee = new WebApp.DataStore({ fields: Field });
    
	var checkColumn = new Ext.grid.CheckColumn({
			   header: 'Pilih',
			   dataIndex: 'CHECK',
			   id: 'checkid',
			   width: 55
			});
	
     var grListSetTypeTrustee = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetTypeTrustee',
            stripeRows: true,
			title: 'Trustee',
            store: dsInfomodulTrustee,
            autoScroll: true,
            columnLines: true,
            border: true,
			//height:310,
            anchor: '100% 50%',
			plugins: [new Ext.ux.grid.FilterRow(),checkColumn],
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
					checkColumn,
							{
							id: 'colsetTrustee',
							//header: 'Trustee',
							dataIndex: 'MOD_NAME',
							width: 450,
							menuDisabled:true,
							sortable: true,
							filter:{}
							},
							{
							id: 'colsetKDuser',
							//header: 'Trustee',
							dataIndex: 'MOD_ID',
							width: 550,
							hidden:true,
								menuDisabled:true,
							sortable: true,
							filter:{}
							}								
		
				]
			//'','','','','','',''
            ),
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
										
					
						{
						xtype: 'button',
						text: 'Hapus',
						iconCls: 'remove',
						id: 'btnDelete_User',
						//enable:false,
						handler: function()
						{ 
						Ext.Msg.show
							(
								{
								   title:'Hapus',
								   msg: 'Anda yakin akan menghapus member',
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn)
								   {
									   if (btn =='yes')
										{
										
										 datadelete_User(addNew_viUser);
												
											
										};
								   },
								   icon: Ext.MessageBox.QUESTION
								}
							);
						
							//SetUserGroup();
							//RefreshDataSetJadwalDokter();
						}
					},
                                        
				]
			}
			
		    ,viewConfig: { forceFit: false}
		}
	); 
	// Kriteria filter pada Grid
	
var Field =['MOD_ID','MOD_NAME','CHECK','URUT_MASUK'];
var chkuser = new Ext.grid.CheckColumn
	(
		{
			id: 'chkuser',
			header: 'Pilih',
			align: 'center',
			disabled:false,
			
			dataIndex: 'CHECK',
		    anchor: '10% 100%'
	
		}
	);

     dsInfomodul = new WebApp.DataStore({ fields: Field });
	
     var grListSetlookupuser = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetlookupuser',
            stripeRows: false,
			title: 'Trustee',
            store: dsInfomodul,
            autoScroll: true,
            columnLines: true,
            border: true,
			//height:5,
            anchor: '100% 50%',
			plugins: [new Ext.ux.grid.FilterRow(),chkuser],
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
								chkuser,
						    {
							id: 'colsetUSER',
							dataIndex: 'MOD_NAME',
							width: 325,
							//header: 'Nama User',
					
						 //  anchor: '100% 100%',
							menuDisabled:true,
							sortable: true,
							filter:{}
							}	,
								{
							id: 'colsetKDUSER',
							dataIndex: 'MOD_ID',
							width: 10,
							hidden: true,
						  menuDisabled:true,
							sortable: true,
							filter:{}
							}	,
							{
							id: 'colseturut',
							dataIndex: 'URUT_MASUK',
							width: 100,
							hidden: true,
						  menuDisabled:true,
							sortable: true,
							filter:{}
							}								
		
				]
			//'','','','','','',''
            ),
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah',
						iconCls: 'add',
						id: 'btnAdd_viMemberUser',
						handler: function(){
							datasave_User(addNew_viUser);
							datarefresh_vimodul();
						}
					}
					
					
                                        
				]
			}
			
		    ,viewConfig: { forceFit: false}
		}
	); 
	var FrmTabs_viInformasimodul = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title:  'List Modul',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100% 100%',
		    iconCls: icons_vimodul,
		    items: 
			[
				{
					columnWidth: .50,
					height:600,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[
					//grData_vimodul
					grListSetmodul
					]
				},
				{
					columnWidth: .50,
					height:600,
					layout: 'form',
					bodyStyle: 'padding:6px 6px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[
					grListSetTypeTrustee,grListSetlookupuser
							
					]
				}
			]
		
    }
    )
    // datarefresh_vimodul();
    return FrmTabs_viInformasimodul;
}







function ShowPesanWarning_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}




/**
*	Function : gridDataForm_vimodul
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_vimodul
*/
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_vimodul()
{
    dataSource_viInformasiGroupMember.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_vimodul,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViewMemberUser',
				param: ''
			}
		}
    );
	
    return dataSource_viInformasiGroupMember;
    //alert("refersh")
}
	
	





function RefreshDataUser()
{
	dsInfomodulTrustee.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfomodul,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewCheckTrusteeModule',
					param : '~'+ group_id + '~'
				}
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfomodulTrustee;
};

function RefreshDataUser2()
{
	dsInfomodul.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfomodul,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewTrusteeModule',
					param : '~'+ group_id + '~'
					}			
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfomodul;
};

function setLookUp_viGroup(rowdata)
{
    var lebar = 500;
    setLookUps_viUser = new Ext.Window
    (
    {
        id: 'SetLookUps_viMemberUser',
        name: 'SetLookUps_viMemberUser',
        title: 'Form User', 
        closeAction: 'destroy',        
        width: lebar,
        height: 460,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viUSer(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				//alert(Ext.get('txtNamaPasien').getValue());
				//Ext.getCmp('txtNama').focus(true, 10);

            },
            afterShow: function()
            {
               // this.activate();
				
            },
            deactivate: function()
            {
            }
        }
    }
    );

    setLookUps_viUser.show();
    if (rowdata === undefined)
    {
		//alert(rowdata);
        //dataaddnew_viGroup();
    ShowPesanWarning_viUserInfo('silahkan pilih data ','Lookup')
	}
    else
    {
		//console.log(rowdata);
       dataaddnew_viGroup(rowdata);
    }
	

	
	
	}

function getFormItemEntry_viUSer(lebar,rowdata)
{
    var pnlFormDataBasic_viMemberUser = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                   autoScroll: false,
			width: lebar-15,//lebar-55,
                        height: 450,
			border: false,

                        items:[
                            getItemPanelInputUserr(),grListSetlookupuser
							
                            //getPenelItemDataKunjungan_viMemberUser(lebar)
                                ],
			fileUpload: true
			
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viMemberUser;
	
	
	
}


//form view data biodata pasien
function getItemPanelInputUserr() 
{
	
    var items =
	{
	    layout: 'column',
	    border: false,
        labelAlign: 'top',
	    items:
		[
                       
			{
			    columnWidth: 0.3,
			    layout: 'form',
                 labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group ID ',
					    name: 'txtgroupid',
					    id: 'txtgroupid',
                        readOnly:true,
						hidden:false,
						tabIndex:1,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group Name ',
					    name: 'txtgroupname',
					    id: 'txtgroupname',
						tabIndex:2,
                        readOnly:true,
					    anchor: '95%'
					}
					
				]
			},
			{
				columnWidth: 0.7,
			    layout: 'form',
                labelWidth:100,
			    border: false,
			    items:
				[
				
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group Description ',
					    name: 'txtgroupdesc',
					    id: 'txtgroupdesc',
						tabIndex:3,
						    readOnly:true,
                        disabled:false,
					    anchor: '95%',
                           
					}
				]
			}
			          
          ]
        };
    return items;
};
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;


function dataaddnew_viGroup(rowdata)
{
	if(rowdata === undefined)
	{
	Ext.getCmp('txtgroupid').focus('false', 10); 
    //addNew_viMemberUser = true;
	Ext.getCmp('txtgroupid').setValue('');
	Ext.getCmp('txtgroupname').setValue('');
	Ext.getCmp('txtgroupdesc').setValue('');
	Ext.getCmp('btnAdd_viMemberUser').enable();
	Ext.getCmp('btnSimpan_viMemberUser').enable();
	Ext.getCmp('btnSimpanExit_viMemberUser').enable();
	Ext.getCmp('btnDelete_User').disable();
	addNew_viUser= true;	
	}
	else
	{
	Ext.getCmp('txtgroupid').focus('false', 10); 
    //addNew_viMemberUser = true;
	Ext.getCmp('txtgroupid').setValue(rowdata.GROUP_ID);
	Ext.getCmp('txtgroupname').setValue(rowdata.GROUP_NAME);
	Ext.getCmp('txtgroupdesc').setValue(rowdata.GROUP_DESC);
	Ext.getCmp('btnAdd_viMemberUser').enable();
	Ext.getCmp('btnSimpan_viMemberUser').enable();
	Ext.getCmp('btnSimpanExit_viMemberUser').enable();
	Ext.getCmp('btnDelete_User').enable();
	addNew_viUser= false;	

	}
	
   // rowSelected_viMemberUser   = undefined;
}



function getParamDetailTransaksimodul() 
{
    var params =
	{
		//Table:'ViewTrmodul',
		
		
	
		Grup_Id: group_id,
		List:getArrDetailTrmodul(),
		JmlField: mRecordmodul.prototype.fields.length,
		JmlList:GetListCountDetailmodul(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamDetailTransaksideletermodul() 
{
    var params =
	{
		//Table:'ViewTrmodul',
		
		
	
		Grup_Id: group_id,
		List:getArrDetailTrdeletermodul(),
		JmlField: mRecordmoduldelete.prototype.fields.length,
		JmlList:GetListCountDetaildeletemodul(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetaildeletemodul()
{
	
	var x=0;
	for(var i = 0 ; i < dsInfomodulTrustee.getCount();i++)
	{
		if (dsInfomodulTrustee.data.items[i].data.MOD_ID != '' || dsInfomodulTrustee.data.items[i].data.MOD_NAME  != '')
		{
		if(dsInfomodulTrustee.data.items[i].data.CHECK == true)
					{
			x += 1;
			}
		};
	}
	return x;
	
};



function GetListCountDetailmodul()
{
	
	var x=0;
	for(var i = 0 ; i < dsInfomodul.getCount();i++)
	{
		if (dsInfomodul.data.items[i].data.MOD_ID != '' || dsInfomodul.data.items[i].data.MOD_NAME  != '')
		{
		if(dsInfomodul.data.items[i].data.CHECK == true)
					{
			x += 1;
			}
		};
	}
	return x;
	
};


function getArrDetailTrdeletermodul()
{
	var x='';
	for(var i = 0 ; i < dsInfomodulTrustee.getCount();i++)
	{

		if (dsInfomodulTrustee.data.items[i].data.MOD_ID != '' || dsInfomodulTrustee.data.items[i].data.MOD_NAME  != '')
		{
					if(dsInfomodulTrustee.data.items[i].data.CHECK == true)
					{
					
						var y='';
						var z='@@##$$@@';
						
						y = dsInfomodulTrustee.data.items[i].data.URUT_MASUK
						y += z + dsInfomodulTrustee.data.items[i].data.MOD_ID
						y += z + dsInfomodulTrustee.data.items[i].data.CHECK
						y += z + dsInfomodulTrustee.data.items[i].data.MOD_NAME
						
						if (i === (dsInfomodulTrustee.getCount()-1))
						{
							x += y 
						}
						else
						{
							x += y + '##[[]]##'
						};
					}
					}
		
		
	}	
	
	return x;
};

function getArrDetailTrmodul()
{
	var x='';
	for(var i = 0 ; i < dsInfomodul.getCount();i++)
	{

		if (dsInfomodul.data.items[i].data.MOD_ID != '' || dsInfomodul.data.items[i].data.MOD_NAME  != '')
		{
					if(dsInfomodul.data.items[i].data.CHECK == true)
					{
					
						var y='';
						var z='@@##$$@@';
						
						y = dsInfomodul.data.items[i].data.URUT_MASUK
						y += z + dsInfomodul.data.items[i].data.MOD_ID
						y += z + dsInfomodul.data.items[i].data.CHECK
						y += z + dsInfomodul.data.items[i].data.MOD_NAME
						
						if (i === (dsInfomodul.getCount()-1))
						{
							x += y 
						}
						else
						{
							x += y + '##[[]]##'
						};
					}
					}
		
		
	}	
	
	return x;
};




function datasave_User(mBol)
{	

        //alert('a')
    
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/functionTrustee/savetrustee",
					params: getParamDetailTransaksimodul(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viUserInfo('Data berhasil di simpan','Simpan Data');
							datarefresh_vimodul();
								RefreshDataUser();
								 RefreshDataUser2();
							addNew_viMemberUser = false;
//							Ext.getCmp('btnAdd_viMemberUser').disable();
//							Ext.getCmp('btnDelete_User').disable();
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viUserInfo('Data tidak berhasil di simpan ' ,'Simpan Data');
						}
						else if(cst.success === false && cst.cari===true)
						{
						ShowPesanWarning_viUserInfo('Anda telah berkunjung sebelumnya ','Simpan Data');
						}
						else
						{
							ShowPesanError_viUserInfo('Data tidak berhasil di simpan ' ,'Simpan Data');
						}
					}
				}
            )
        
        
    
}



function datadelete_User(mBol)
{	

        //alert('a')
    
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/functionTrustee/deletetrustee",
					params: getParamDetailTransaksideletermodul(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viUserInfo('Data berhasil di hapus','Hapus data');
							datarefresh_vimodul();
								RefreshDataUser();
								 RefreshDataUser2();
							addNew_viMemberUser = false;
//							Ext.getCmp('btnAdd_viMemberUser').disable();
//							Ext.getCmp('btnDelete_User').disable();
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viUserInfo('Data tidak berhasil di hapus ' ,'Hapus data');
						}
						
						else
						{
							ShowPesanError_viUserInfo('Data tidak berhasil di hapus ' ,'Hapus data');
						}
					}
				}
            )
        
        
    
}






