// Data Source ExtJS

/**
*	Nama File 		: TRInformasiTarif.js
*	Menu 			: PenMemberUseran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var group_id;
var dsDataGroup_list;
var kode_tarif;
var addNew_viUser;
var rowSelected_user;
var Today_PenMemberUseran = new Date;
var kode_produk;
var kode_user;
var tgl_berlaku;
var dsInfotarifList;
var dsInfomodul;
var rowSelected_viUsergroup;
var rowSelected_viGroup;
var selectCountInfomodul=100;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_vimodul;
var dataSource_vimodul;
var selectCount_vimodul=50;
var NamaForm_vimodul="Informasi Tarif";
var icons_vimodul="Gaji";
var dsInfomodulTrustee;
var dsDataStoreGridGoup       = new Ext.data.JsonStore();
var dsDataStoreGridSetupGroup = new Ext.data.JsonStore();
var mRecordmodul = Ext.data.Record.create
(
  [
   //','MOD_NAME
       {name: 'MOD_ID', mapping:'MOD_ID'},
       {name: 'MOD_NAME', mapping:'MOD_NAME'},
       {name: 'CHECK', mapping:'CHECK'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var mRecordmoduldelete = Ext.data.Record.create
(
  [
   
       {name: 'MOD_ID', mapping:'MOD_ID'},
       {name: 'MOD_NAME', mapping:'MOD_NAME'},
       {name: 'CHECK', mapping:'CHECK'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);




var now_vimodul = new Date();
var dataSource_viInformasiGroupMember;

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_vimodul =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGruopUser(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viGruopUser
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
loaddatastore_mod_group();
function dataGrid_viGruopUser(mod_id){
    // Field kiriman dari Project Net.
	var Field =['GROUP_ID','GROUP_NAME','GROUP_DESC'];
	dataSource_viInformasiGroupMember = new WebApp.DataStore({ fields: Field });
	datarefresh_vimodul();
	var grListSetmodul = new Ext.grid.EditorGridPanel({
		id: 'grListSetmodul',
		stripeRows: false,
		title: 'Group Name',
		store: dataSource_viInformasiGroupMember,
		autoScroll: true,
		columnLines: true,
		border: true,
		plugins: [new Ext.ux.grid.FilterRow()],
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{}
		}),
		listeners:{
			rowclick: function (sm, ridx, cidx){
				rowSelected_viGroup = dataSource_viInformasiGroupMember.getAt(ridx);
				group_id =rowSelected_viGroup.data.GROUP_ID;
				loaddatastore_combo_module(group_id);
				RefreshDataUser();
				//pindahMOD_ID();
				RefreshDataUser2();
				Ext.getCmp('btnAdd_viMemberUser').enable();
				Ext.getCmp('btnDelete_User').enable();
			}
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				id: 'colgroupid',
				header: 'Group ID',
				dataIndex: 'GROUP_ID',
				menuDisabled:true,
				sortable: true,
				hidden:true,
				filter:{}
			},{
				id: 'colGroupName',
				header: 'Group Name',
				dataIndex: 'GROUP_NAME',
				width: 200,
				menuDisabled:true,
				sortable: true,
				filter:{}
			},{
				//'','','','','','TGL_BERLAKU',''
				id: 'colgroupdesc',
				header: 'Group Desc',
				dataIndex: 'GROUP_DESC',
				hidden:false,
				width: 220,
				menuDisabled:true,
				sortable: true,
				filter:{}
			},
		]),
		tbar:[
			{
				xtype: 'button',
				text: 'Group Baru',
				iconCls: 'Edit_Tr',
				hidden : true,
				tooltip: 'Edit Data',
				id: 'btntambah_viMemberUser',
				handler: function(sm, row, rec){
					setLookUp_viGroup();															
				}
			}
		],
		viewConfig: { forceFit: false}
	}); 
	var Field =['MOD_ID','MOD_NAME','CHECK','URUT_MASUK'];
	dsInfomodulTrustee = new WebApp.DataStore({ fields: Field });
	var checkColumn = new Ext.grid.CheckColumn({
	   header: 'Pilih',
	   dataIndex: 'CHECK',
	   id: 'checkid',
	   width: 55
	});
	var grListSetTypeTrustee = new Ext.grid.EditorGridPanel({
		id: 'grListSetTypeTrustee',
		stripeRows: true,
		title: 'Trustee',
		store: dsInfomodulTrustee,
		autoScroll: true,
		columnLines: true,
		border: true,
		style:'padding-bottom: 4px;',
		flex:1,
		plugins: [new Ext.ux.grid.FilterRow(),checkColumn],
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){}
		},
		cm: new Ext.grid.ColumnModel ([
			new Ext.grid.RowNumberer(),
			checkColumn,
			{
				id: 'colsetTrustee',
				//header: 'Trustee',
				dataIndex: 'MOD_NAME',
				width: 450,
				menuDisabled:true,
				sortable: true,
				filter:{}
			},{
				id: 'colsetKDuser',
				//header: 'Trustee',
				dataIndex: 'MOD_ID',
				width: 550,
				hidden:true,
				menuDisabled:true,
				sortable: true,
				filter:{}
			}								
		]),
		tbar:[
			{
				xtype: 'button',
				text: 'Hapus',
				iconCls: 'remove',
				id: 'btnDelete_User',
				//enable:false,
				handler: function(){ 
					Ext.Msg.show({
					   title:'Hapus',
					   msg: 'Anda yakin akan menghapus member',
					   buttons: Ext.MessageBox.YESNO,
					   fn: function (btn){
							if (btn =='yes'){
								datadelete_User(addNew_viUser);
							}
					   },
					   icon: Ext.MessageBox.QUESTION
					});
					//SetUserGroup();
					//RefreshDataSetJadwalDokter();
				}
			},
			ComboGroupTrusteeChecked(),
		]
		,viewConfig: { forceFit: false}
	}); 
	// Kriteria filter pada Grid
	
	var Field =['MOD_ID','MOD_NAME','MOD_GROUP','CHECK','URUT_MASUK'];
	var chkuser = new Ext.grid.CheckColumn({
		id: 'chkuser',
		header: 'Pilih',
		align: 'center',
		disabled:false,
		dataIndex: 'CHECK',
		anchor: '10% 100%'
	});
	dsInfomodul = new WebApp.DataStore({ fields: Field });
	var grListSetlookupuser = new Ext.grid.EditorGridPanel({
		id: 'grListSetlookupuser',
		stripeRows: false,
		title: 'Trustee',
		store: dsInfomodul,
		autoScroll: true,
		columnLines: true,
		border: true,
		//height:5,
		// anchor: '100% 50%',
		flex:1,
		plugins: [new Ext.ux.grid.FilterRow(),chkuser],
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){}
		},
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			chkuser,
			{
				id: 'colsetUSERTrustee',
				dataIndex: 'MOD_NAME',
				width: 325,
				//header: 'Nama User',
				menuDisabled:true,
				sortable: true,
				filter:{}
			},{
				id: 'colsetKDUSERTrustee',
				dataIndex: 'MOD_ID',
				width: 10,
				hidden: true,
				menuDisabled:true,
				sortable: true,
				filter:{}
			},{
				id: 'colsetModGroupTrustee',
				dataIndex: 'MOD_GROUP',
				width: 100,
				hidden: true,
				menuDisabled:true,
				sortable: true,
				filter:{}
			}								
		]),
		tbar:[
			{
				xtype: 'button',
				text: 'Tambah',
				iconCls: 'add',
				id: 'btnAdd_viMemberUser',
				handler: function(){
					datasave_User(addNew_viUser);
					datarefresh_vimodul();
				}
			},
			ComboGroupTrustee(),
		]
		,viewConfig: { forceFit: false}
	}); 
	var FrmTabs_viInformasimodul = new Ext.Panel({
		id: mod_id,
		closable: true,
		layout: {
			type:'hbox',
			align:'stretch'
		},
		title:  'List Modul',
		border: false,
		shadhow: true,
		items:[
			{
				width: 480,
				layout: 'fit',
				border: false,
				autoScroll: true,
				bodyStyle: 'padding:4px;',
				items:[
					//grData_vimodul
					grListSetmodul
				]
			},{
				flex:1,
				layout: {
					type:'vbox',
					align:'stretch'
				},
				border: false,
				bodyStyle: 'padding:4px 4px 4px 0px;',
				items:[
					grListSetTypeTrustee,grListSetlookupuser
				]
			}
		]
    });
    // datarefresh_vimodul();
    return FrmTabs_viInformasimodul;
}







function ShowPesanWarning_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viUserInfo(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}




/**
*	Function : gridDataForm_vimodul
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_vimodul
*/
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_vimodul()
{
    dataSource_viInformasiGroupMember.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_vimodul,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViewMemberUser',
				param: ' group_id not in (~NULL~) order by group_name asc'
			}
		}
    );
	
    return dataSource_viInformasiGroupMember;
    //alert("refersh")
}


    function loaddatastore_mod_group(){
	    var Field = ['mod_group','mod_img','mod_row'];
	    var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
		Ext.Ajax.request({
			// url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
			url: baseURL +  "index.php/userman/setup_user/getModGroup",
			params: {
				null : null,
			},
			success: function(response) {
				//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
				var cst       = Ext.decode(response.responseText);
				//dsDataPerawatPenindak_KASIR_RWI.load(myData);
				for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
					var recs = [],recType = DataStoreGridKomponent.recordType;
					var o    = cst['ListDataObj'][i];
					recs.push(new recType(o));
					dsDataStoreGridGoup.add(recs);
				}
			},
		});
	}

    function loaddatastore_zmodule(criteria){
		dsInfomodul.removeAll();
	    var Field = ['mod_group','mod_img','mod_row'];
	    var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
		Ext.Ajax.request({
			// url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
			url: baseURL +  "index.php/userman/setup_user/getZmodule",
			params: {
				mod_group : criteria,
			},
			success: function(response) {
				//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
				var cst       = Ext.decode(response.responseText);
				//dsDataPerawatPenindak_KASIR_RWI.load(myData);
				for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
					var recs = [],recType = DataStoreGridKomponent.recordType;
					var o    = cst['ListDataObj'][i];
					recs.push(new recType(o));
					dsInfomodul.add(recs);
				}
			},
		});
	}

    function loaddatastore_set_zmodule(mod_group, group_id){
		dsInfomodulTrustee.removeAll();
	    var Field = ['mod_group','mod_img','mod_row'];
	    var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
		Ext.Ajax.request({
			// url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
			url: baseURL +  "index.php/userman/setup_user/getSetupZmodule",
			params: {
				mod_group 	: mod_group,
				group_id 	: group_id,
			},
			success: function(response) {
				//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
				var cst       = Ext.decode(response.responseText);
				//dsDataPerawatPenindak_KASIR_RWI.load(myData);
				for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
					var recs = [],recType = DataStoreGridKomponent.recordType;
					var o    = cst['ListDataObj'][i];
					recs.push(new recType(o));
					dsInfomodulTrustee.add(recs);
				}
			},
		});
	}

    function loaddatastore_combo_module(group_id){
		dsDataStoreGridSetupGroup.removeAll();
	    var Field = ['mod_group','mod_img','mod_row'];
	    var DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
		Ext.Ajax.request({
			// url: baseURL +  "index.php/rawat_inap/viewgridinfopasienrwi/listKamar",
			url: baseURL +  "index.php/userman/setup_user/getComboSetTrustee",
			params: {
				group_id 	: group_id,
			},
			success: function(response) {
				//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
				var cst       = Ext.decode(response.responseText);
				//dsDataPerawatPenindak_KASIR_RWI.load(myData);
				for(var i     =0,iLen=cst['ListDataObj'].length; i<iLen; i++){
					var recs = [],recType = DataStoreGridKomponent.recordType;
					var o    = cst['ListDataObj'][i];
					recs.push(new recType(o));
					dsDataStoreGridSetupGroup.add(recs);
				}
			},
		});
	}

function RefreshDataUser()
{
	dsInfomodulTrustee.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfomodul,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewCheckTrusteeModule',
					param : '~'+ group_id + '~'
				}
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfomodulTrustee;
};

function RefreshDataUser2()
{
	dsInfomodul.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfomodul,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewTrusteeModule',
					param : '~'+ group_id + '~'
					}			
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfomodul;
};

function setLookUp_viGroup(rowdata)
{
    var lebar = 500;
    setLookUps_viUser = new Ext.Window
    (
    {
        id: 'SetLookUps_viMemberUser',
        name: 'SetLookUps_viMemberUser',
        title: 'Form User', 
        closeAction: 'destroy',        
        width: lebar,
        height: 460,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viUSer(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				//alert(Ext.get('txtNamaPasien').getValue());
				//Ext.getCmp('txtNama').focus(true, 10);

            },
            afterShow: function()
            {
               // this.activate();
				
            },
            deactivate: function()
            {
            }
        }
    }
    );

    setLookUps_viUser.show();
    if (rowdata === undefined)
    {
		//alert(rowdata);
        //dataaddnew_viGroup();
    ShowPesanWarning_viUserInfo('silahkan pilih data ','Lookup')
	}
    else
    {
		//console.log(rowdata);
       dataaddnew_viGroup(rowdata);
    }
	

	
	
	}

function getFormItemEntry_viUSer(lebar,rowdata)
{
    var pnlFormDataBasic_viMemberUser = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                   autoScroll: false,
			width: lebar-15,//lebar-55,
                        height: 450,
			border: false,

                        items:[
                            getItemPanelInputUserr(),grListSetlookupuser
							
                            //getPenelItemDataKunjungan_viMemberUser(lebar)
                                ],
			fileUpload: true
			
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viMemberUser;
	
	
	
}


//form view data biodata pasien
function getItemPanelInputUserr() 
{
	
    var items =
	{
	    layout: 'column',
	    border: false,
        labelAlign: 'top',
	    items:
		[
                       
			{
			    columnWidth: 0.3,
			    layout: 'form',
                 labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group ID ',
					    name: 'txtgroupid',
					    id: 'txtgroupid',
                        readOnly:true,
						hidden:false,
						tabIndex:1,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group Name ',
					    name: 'txtgroupname',
					    id: 'txtgroupname',
						tabIndex:2,
                        readOnly:true,
					    anchor: '95%'
					}
					
				]
			},
			{
				columnWidth: 0.7,
			    layout: 'form',
                labelWidth:100,
			    border: false,
			    items:
				[
				
					{
					    xtype: 'textfield',
					    fieldLabel: 'Group Description ',
					    name: 'txtgroupdesc',
					    id: 'txtgroupdesc',
						tabIndex:3,
						    readOnly:true,
                        disabled:false,
					    anchor: '95%',
                           
					}
				]
			}
			          
          ]
        };
    return items;
};
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;


function dataaddnew_viGroup(rowdata)
{
	if(rowdata === undefined)
	{
	Ext.getCmp('txtgroupid').focus('false', 10); 
    //addNew_viMemberUser = true;
	Ext.getCmp('txtgroupid').setValue('');
	Ext.getCmp('txtgroupname').setValue('');
	Ext.getCmp('txtgroupdesc').setValue('');
	Ext.getCmp('btnAdd_viMemberUser').enable();
	Ext.getCmp('btnSimpan_viMemberUser').enable();
	Ext.getCmp('btnSimpanExit_viMemberUser').enable();
	Ext.getCmp('btnDelete_User').disable();
	addNew_viUser= true;	
	}
	else
	{
	Ext.getCmp('txtgroupid').focus('false', 10); 
    //addNew_viMemberUser = true;
	Ext.getCmp('txtgroupid').setValue(rowdata.GROUP_ID);
	Ext.getCmp('txtgroupname').setValue(rowdata.GROUP_NAME);
	Ext.getCmp('txtgroupdesc').setValue(rowdata.GROUP_DESC);
	Ext.getCmp('btnAdd_viMemberUser').enable();
	Ext.getCmp('btnSimpan_viMemberUser').enable();
	Ext.getCmp('btnSimpanExit_viMemberUser').enable();
	Ext.getCmp('btnDelete_User').enable();
	addNew_viUser= false;	

	}
	
   // rowSelected_viMemberUser   = undefined;
}



function getParamDetailTransaksimodul() 
{
    var params =
	{
		//Table:'ViewTrmodul',
		
		
	
		Grup_Id: group_id,
		List:getArrDetailTrmodul(),
		JmlField: mRecordmodul.prototype.fields.length,
		JmlList:GetListCountDetailmodul(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamDetailTransaksideletermodul() 
{
    var params =
	{
		//Table:'ViewTrmodul',
		
		
	
		Grup_Id: group_id,
		List:getArrDetailTrdeletermodul(),
		JmlField: mRecordmoduldelete.prototype.fields.length,
		JmlList:GetListCountDetaildeletemodul(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetaildeletemodul()
{
	
	var x=0;
	for(var i = 0 ; i < dsInfomodulTrustee.getCount();i++)
	{
		if (dsInfomodulTrustee.data.items[i].data.MOD_ID != '' || dsInfomodulTrustee.data.items[i].data.MOD_NAME  != '')
		{
		if(dsInfomodulTrustee.data.items[i].data.CHECK == true)
					{
			x += 1;
			}
		};
	}
	return x;
	
};



function GetListCountDetailmodul()
{
	
	var x=0;
	for(var i = 0 ; i < dsInfomodul.getCount();i++)
	{
		if (dsInfomodul.data.items[i].data.MOD_ID != '' || dsInfomodul.data.items[i].data.MOD_NAME  != '')
		{
		if(dsInfomodul.data.items[i].data.CHECK == true)
					{
			x += 1;
			}
		};
	}
	return x;
	
};


function getArrDetailTrdeletermodul()
{
	var x='';
	for(var i = 0 ; i < dsInfomodulTrustee.getCount();i++)
	{

		if (dsInfomodulTrustee.data.items[i].data.MOD_ID != '' || dsInfomodulTrustee.data.items[i].data.MOD_NAME  != '')
		{
					if(dsInfomodulTrustee.data.items[i].data.CHECK == true)
					{
					
						var y='';
						var z='@@##$$@@';
						
						y = dsInfomodulTrustee.data.items[i].data.URUT_MASUK
						y += z + dsInfomodulTrustee.data.items[i].data.MOD_ID
						y += z + dsInfomodulTrustee.data.items[i].data.CHECK
						y += z + dsInfomodulTrustee.data.items[i].data.MOD_NAME
						
						if (i === (dsInfomodulTrustee.getCount()-1))
						{
							x += y 
						}
						else
						{
							x += y + '##[[]]##'
						};
					}
					}
		
		
	}	
	
	return x;
};

function getArrDetailTrmodul()
{
	var x='';
	for(var i = 0 ; i < dsInfomodul.getCount();i++)
	{

		if (dsInfomodul.data.items[i].data.MOD_ID != '' || dsInfomodul.data.items[i].data.MOD_NAME  != '')
		{
					if(dsInfomodul.data.items[i].data.CHECK == true)
					{
					
						var y='';
						var z='@@##$$@@';
						
						y = dsInfomodul.data.items[i].data.URUT_MASUK
						y += z + dsInfomodul.data.items[i].data.MOD_ID
						y += z + dsInfomodul.data.items[i].data.CHECK
						y += z + dsInfomodul.data.items[i].data.MOD_NAME
						
						if (i === (dsInfomodul.getCount()-1))
						{
							x += y 
						}
						else
						{
							x += y + '##[[]]##'
						};
					}
					}
		
		
	}	
	
	return x;
};




function datasave_User(mBol)
{	

        //alert('a')
    
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/functionTrustee/savetrustee",
					params: getParamDetailTransaksimodul(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viUserInfo('Data berhasil di simpan','Simpan Data');
							datarefresh_vimodul();
								RefreshDataUser();
								 RefreshDataUser2();
							addNew_viMemberUser = false;
//							Ext.getCmp('btnAdd_viMemberUser').disable();
//							Ext.getCmp('btnDelete_User').disable();
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viUserInfo('Data tidak berhasil di simpan ' ,'Simpan Data');
						}
						else if(cst.success === false && cst.cari===true)
						{
						ShowPesanWarning_viUserInfo('Anda telah berkunjung sebelumnya ','Simpan Data');
						}
						else
						{
							ShowPesanError_viUserInfo('Data tidak berhasil di simpan ' ,'Simpan Data');
						}
					}
				}
            )
        
        
    
}



function datadelete_User(mBol)
{	

        //alert('a')
    
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/functionTrustee/deletetrustee",
					params: getParamDetailTransaksideletermodul(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viUserInfo('Data berhasil di hapus','Hapus data');
							datarefresh_vimodul();
								RefreshDataUser();
								 RefreshDataUser2();
							addNew_viMemberUser = false;
//							Ext.getCmp('btnAdd_viMemberUser').disable();
//							Ext.getCmp('btnDelete_User').disable();
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viUserInfo('Data tidak berhasil di hapus ' ,'Hapus data');
						}
						
						else
						{
							ShowPesanError_viUserInfo('Data tidak berhasil di hapus ' ,'Hapus data');
						}
					}
				}
            )
        
        
    
}


/*function LoadGroupUser(param){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/main/get_data_group_menu",
		params: {
			text:'',
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dsDataGroup_list.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsDataGroup_list.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsDataGroup_list.add(recs);
				// console.log(o);
			}
		}
	});
};*/

function ComboGroupTrustee(){
	dsDataGroup_list = new WebApp.DataStore({fields: ['mod_group','mod_img','mod_row']})
	// LoadGroupUser();
	cboKelasUnit_InfoPasienRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboGroupChecked_InfoPasienRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			fieldLabel: 'Kelas ',
			store: dsDataStoreGridGoup,
			valueField: 'mod_group',
			displayField: 'mod_group',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{

					loaddatastore_zmodule(b.data.mod_group);
					console.log(b.data);
				}
			}
		}
	)
	return cboKelasUnit_InfoPasienRWI;
} 

function ComboGroupTrusteeChecked(){
	dsDataGroup_list = new WebApp.DataStore({fields: ['MOD_GROUP']})
	// LoadGroupUser();
	cboKelasUnit_InfoPasienRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboGroup_InfoPasienRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			fieldLabel: 'Kelas ',
			store: dsDataStoreGridSetupGroup,
			valueField: 'MOD_GROUP',
			displayField: 'MOD_GROUP',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{
					loaddatastore_set_zmodule(b.data.MOD_GROUP, group_id);
					// console.log(group_id);
					// console.log(b.data);
				}
			}
		}
	)
	return cboKelasUnit_InfoPasienRWI;
} 



function RefreshDataUser_Module(group_id = null)
{
	dsInfomodul.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfomodul,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewCheckTrusteeModule',
					param : '~'+ group_id + '~'
				}
			}
		);

	// rowSelectedInfotarif = undefined;
	return dsInfomodul;
};