var CurrentKasirupload =
{
    data: Object,
    details: Array,
    row: 0
};

var tampunguploadsekarang;
var tampunguploadnanti;
var dsTRKasiruploadList;
var dsTRDetailKasiruploadList;
var AddNewKasirupload = true;
var selectCountKasirupload = 50;
var now = new Date();
var rowSelectedKasirupload;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRupload;
var valueStatusCMuploadView='All';
var nowTglTransaksi = new Date();
CurrentPage.page = getPanelupload(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelupload(mod_id) 
{
    var FormDepanupload = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir upload',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [uploadLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanupload

};









function uploadLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRupload = new Ext.Window
    (
        {
            id: 'gridupload',
            title: 'Tutup upload',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRupload(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirupload=undefined;
                    //RefreshDataFilterKasirupload();
                }
            }
        }
    );

    FormLookUpsdetailTRupload.show();
    if (rowdata == undefined) 
	{
        uploadAddNew();
    }
    else 
	{
        
    }

};
function getFormEntryTRupload(lebar) 
{
    var pnlTRupload = new Ext.FormPanel
    (
        {
            fileUpload: true,
			width: 500,
			frame: true,
			title: 'File Upload Form',
			bodyStyle: 'padding: 10px 10px 0 10px;',
			labelWidth: 50,
			defaults: {
				anchor: '95%',
				allowBlank: false,
				msgTarget: 'side'
    },
    items: [{
        xtype: 'fileuploadfield',
        id: 'form-file',
        emptyText: 'Pilih Foto',
        fieldLabel: 'Foto',
        name: 'image-upload_lr',
        buttonText: 'Upload'
    }], 
	  buttons: [{
        text: 'Save',
        handler: function(){
            if(pnlTRupload.getForm().isValid()){
                    pnlTRupload.getForm().submit({
                        url: baseURL + "index.php/main/functionRWJ/upload_foto",
                        waitMsg: 'Uploading your photo...',
                        success: function( o){
							ShowPesanInfoupload('Upload berhasil','upload foto');
                            //msg('Success', 'Processed file');
                        },failure: function(o)
											{
                            ShowPesanErrorupload('Upload gagal','upload foto');
											}
                    });
            }
        }
    },{
        text: 'Reset',
        handler: function(){
            pnlTRupload.getForm().reset();
        }
    }]

	}
    );

    return pnlTRupload
};
function ShowPesanWarningupload(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorupload(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoupload(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};










