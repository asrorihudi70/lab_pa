// Data Source ExtJS

/**
 *	Nama File 		: TRInformasiTarif.js
 *	Menu 			: Pendaftaran
 *	Model id 		: 030103
 *	Keterangan 		: Untuk View Informasi Tarif
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 */

// Deklarasi Variabel pada ExtJS
var idxgrid = '';
var kode_tarif;
var addNew_viUser;
var rowSelected_user;
var Today_Pendaftaran = new Date;
var kode_produk;
var kode_user;
var tgl_berlaku;
var dsInfotarifList;
var dsInfoGroupUser;
var rowSelected_viUsergroup;
var rowSelected_viGroup;
var selectCountInfoGroupUser = 100;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_viuser;
var dataSource_viuser;
var selectCount_viuser = 50;
var NamaForm_viuser = "Informasi Tarif";
var icons_viuser = "Gaji";
var addNew_viuser;
var rowSelected_viuser;
var rowSelected_viuserList;
var ssetLookUps_viUser;
var setLookUps_viuserList;
var now_viuser = new Date();
var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi
var dsUserDokter;
var iddokter;
var dsUserUnit;
var idunit;
var ds_Poli_viDaftar;
var idpoli;
var dsUsermilik;
var idmilik;


var CurrentData_viuser =
        {
            data: Object,
            details: Array,
            row: 0
        };
CurrentPage.page = dataGrid_viInformasiUser(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
// Start Shortcut Key
// Kegunaan : Tombol Cepat untuk Keyboard
Ext.getDoc().on('keypress', function (event, target) {
    if (event.altKey && !event.shiftKey)
    {
        event.stopEvent();
        /*if (Ext.EventObject.getKey() === 18 && !Ext.EventObject.getKey() === 16) {
         Ext.EventObject.stopEvent();*/

        switch (event.getKey()) {
//switch(Ext.EventObject.getKey()) {

            case event.F1 :
                dataaddnew_viuser();
                break;
            case event.F2 :
                datasave_viUsers(false);
                datarefresh_viUsers();
                break;
            case event.F3 : // Alt + F3 untuk Edit
                if (rowSelected_viuser === undefined)
                {
                    setLookUp_viuser();
                } else
                {
                    setLookUp_viuser(rowSelected_viuser.data);
                }
                break;
            case event.F5 :
                var x = datasave_viUsers(true);
                datarefresh_viUsers();
                if (x === undefined)
                {
                    ssetLookUps_viUser.close();
                }
                break;
            case event.F10 :
                alert("Delete")
                break;
                // other cases...
        }

    }
});
// End Shortcut Key

// Start Project Informasi Tarif

/**
 *	Function : dataGrid_viuser
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */
function dataGrid_viInformasiUser(mod_id)
{

    // Field kiriman dari Project Net.
    var FieldMaster = ['KD_USER', 'USER_NAMES', 'DESCRIPTION', 'FULL_NAME', 'PASSWORD', 'TAG1', 'TAG2',
        'KDPOLI', 'KDMILIK', 'KDUNIT', 'KDDOKTER', 'NAMADOKTER', 'PEMILIK', 'UNIT', 'POLI'];
    // Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viInformasiUser = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    datarefresh_viUsers();
    // Grid Informasi Tarif


    var FrmTabs_viUserss = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        title: 'List Users',
                        store: dataSource_viInformasiUser,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        height: 570,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    // Function saat ada event double klik maka akan muncul form view
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        idxgrid = ridx;
                                        rowSelected_viUsergroup = dataSource_viInformasiUser.getAt(ridx);
                                        kode_user = rowSelected_viUsergroup.data.KD_USER;
                                        RefreshDataUserGroup();
                                    },
                                    rowdblclick: function (sm, ridx, cidx)
                                    {
                                        rowSelected_user = dataSource_viInformasiUser.getAt(ridx);
                                        if (rowSelected_user != undefined)
                                        {
                                            setLookUp_viUser(rowSelected_user.data);
                                        } else
                                        {
                                            setLookUp_viUser();
                                        }
                                    }

                                },
                        /**
                         *	Mengatur tampilan pada Grid Informasi Tarif
                         *	Terdiri dari : Judul, Isi dan Event
                         *	Isi pada Grid di dapat dari pemangilan dari Net.
                         *	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viuser
                         *	didapat dari Function datarefresh_viUsers()
                         */
                        colModel: new Ext.grid.ColumnModel
                                (
                                        [
                                            //-------------- ## --------------  
                                            {
                                                id: 'colNama_viuser',
                                                header: 'Kode',
                                                dataIndex: 'KD_USER',
                                                sortable: false,
                                                hidden: true


                                            },
                                            //-------------- ## --------------
                                            {
                                                id: 'col_username',
                                                header: 'Username',
                                                dataIndex: 'USER_NAMES',
                                                sortable: true,
                                                menuDisabled: true,
                                                width: 150,
                                                filter: {}

                                            },
                                            {
                                                id: 'col_fullname',
                                                header: 'Full Name',
                                                dataIndex: 'FULL_NAME',
                                                sortable: true,
                                                menuDisabled: true,
                                                width: 200,
                                                filter: {}

                                            },
                                            {
                                                id: 'colPoli_viuser',
                                                header: 'Deskirpsi',
                                                dataIndex: 'DESCRIPTION',
                                                sortable: true,
                                                menuDisabled: true,
                                                hidden: false,
                                                width: 133,
                                                filter: {}

                                            },
                                            {
                                                id: 'colpass',
                                                header: 'Password',
                                                dataIndex: 'PASSWORD',
                                                sortable: true,
                                                menuDisabled: true,
                                                hidden: true,
                                            },
                                            {
                                                id: 'coltag1',
                                                header: 'Tag 1',
                                                dataIndex: 'TAG1',
                                                sortable: true,
                                                menuDisabled: true,
                                                hidden: true,
                                            },
                                            {
                                                id: 'coltag2',
                                                header: 'Tag 2',
                                                dataIndex: 'TAG2',
                                                sortable: true,
                                                menuDisabled: true,
                                                hidden: true,
                                            },
                                                    //-------------- ## --------------     

                                                    //-------------- ## --------------
                                        ]
                                        ),
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    id: 'toolbar_viUser',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'User Baru',
                                                    iconCls: 'Edit_Tr',
                                                    hidden: false,
                                                    tooltip: 'Edit Data',
                                                    id: 'btntambah_viDaftar',
                                                    handler: function (sm, row, rec)
                                                    {

                                                        setLookUp_viUser();
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Edit User',
                                                    iconCls: 'Edit_Tr',
                                                    hidden: false,
                                                    tooltip: 'Edit Data User',
                                                    id: 'btnedit_user',
                                                    handler: function (sm, row, rec)
                                                    {

                                                        rowSelected_user = dataSource_viInformasiUser.getAt(idxgrid);
                                                        if (rowSelected_user != undefined)
                                                        {
                                                            setLookUp_viUser(rowSelected_user.data);
                                                        } else
                                                        {
                                                            ShowPesanWarning_viUserInfo('Pilih Data Terlebih dahulu ', 'Edit Data');
                                                        }

                                                    }
                                                },
                                            ]},
                    }
            )
    /*
     public $;
     public $;
     public $KD_UNIT;
     */

    var checkColumn = new Ext.grid.CheckColumn({
        header: 'Pilih',
        dataIndex: 'CHECK',
        id: 'checkid_users',
        width: 55
    });

    var Field = ['GROUP_ID', 'USER_NAMES', 'GROUP_NAME', 'CHECK'];
    dsInfoGroupUser = new WebApp.DataStore({fields: Field});
    var grListUSer = new Ext.grid.EditorGridPanel
            (
                    {
                        id: 'grListUSer',
                        stripeRows: false,
                        title: 'Group Name',
                        store: dsInfoGroupUser,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        height: 300,
                        plugins: [new Ext.ux.grid.FilterRow()],
                        //anchor: '100% 100%',
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        rowSelected_viGroup = dsInfoGroupUser.getAt(ridx);
                                        group_id = rowSelected_viGroup.data.GROUP_ID;
                                        RefreshDataTrustee();
                                    }
                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            new Ext.grid.RowNumberer(),
                                            checkColumn,
                                            {
                                                id: 'colnamaUsermane',
                                                header: 'Username',
                                                dataIndex: 'USER_NAMES',
                                                width: 150,
                                                menuDisabled: true,
                                                sortable: true
                                            },
                                            {
                                                id: 'colGroupName',
                                                header: 'Group Name',
                                                dataIndex: 'GROUP_NAME',
                                                width: 200,
                                                menuDisabled: true,
                                                sortable: true
                                            },
                                            {
                                                //'','','','','','TGL_BERLAKU',''
                                                id: 'colkdUser',
                                                dataIndex: 'GROUP_ID',
                                                hidden: true,
                                                menuDisabled: true,
                                                sortable: true
                                            },
                                        ]
                                        //'','','','','','',''
                                        ),
                        tbar:
                        {
                            xtype: 'toolbar',
                            items:
                            [
                                {
                                    xtype: 'button',
                                    text: 'Hapus',
                                    iconCls: 'remove',
                                    id: 'btnDelete_Users',
                                    //enable:false,
                                    handler: function (){
                                        Ext.Msg.show({
                                            title: 'Hapus',
                                            msg: 'Anda yakin akan menghapus member',
                                            buttons: Ext.MessageBox.YESNO,
                                            fn: function (btn){
                                                if (btn == 'yes'){
                                                    Ext.Ajax.request({
                                                        url: baseURL + "index.php/userman/setup_user/delete_user",
                                                        params: {
                                                            array_data : getArrDetailTrdeletergroupuser(),
                                                        },
                                                        success: function (o)
                                                        {
                                                            var cst = Ext.decode(o.responseText);
                                                            if (cst.status === true){
                                                                ShowPesanInfo_viUserInfo('Data berhasil di hapus', 'Hapus Data');
                                                            }else{
                                                                ShowPesanWarning_viUserInfo('Data gagal di hapus', 'Hapus Data');
                                                            }
                                                            RefreshDataUserGroup();
                                                        }
                                                    });
                                                };
                                            },
                                            icon: Ext.MessageBox.QUESTION
                                        });
                                    }
                                },
                            ]
                        }, 
                        viewConfig: {forceFit: false}
                    }
            );
    // Kriteria filter pada Grid


    var Field = ['MOD_ID', 'MOD_NAME'];
    dsInfoTrustee = new WebApp.DataStore({fields: Field});
    var grListSetTypeTrustee2 = new Ext.grid.EditorGridPanel
            (
                    {
                        id: 'grListSetTypeTrustee2',
                        stripeRows: false,
                        title: 'Trustee',
                        store: dsInfoTrustee,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        height: 310,
                        //anchor: '100% 100%',
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                    }
                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                id: 'colsetTrustee2',
                                                header: 'Trustee',
                                                dataIndex: 'MOD_NAME',
                                                width: 550,
                                                menuDisabled: true,
                                                sortable: true
                                            },
                                            {
                                                id: 'colsetgroupId2',
                                                header: 'Group',
                                                dataIndex: 'MOD_ID',
                                                menuDisabled: true,
                                                sortable: true,
                                                hidden: true
                                            },
                                        ]
                                        //'','','','','','',''
                                        )

                        , viewConfig: {forceFit: false}
                    }
            );
    // Kriteria filter pada Grid

    var FrmTabs_viUser = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'column',
                        title: 'Users',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding:15px',
                        border: false,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        margins: '0 5 5 0',
                        anchor: '100% 100%',
                        iconCls: icons_viuser,
                        items:
                                [
                                    {
                                        columnWidth: .50,
                                        layout: 'form',
                                        border: false,
                                        autoScroll: true,
                                        bodyStyle: 'padding:6px 3px 3px 6px',
                                        items:
                                                [
                                                    FrmTabs_viUserss

                                                ]
                                    },
                                    {
                                        columnWidth: .50,
                                        height: 500,
                                        layout: 'form',
                                        bodyStyle: 'padding:6px 6px 3px 3px',
                                        border: false,
                                        anchor: '100% 100%',
                                        items:
                                                [
                                                    grListUSer,
                                                    {
                                                        xtype: 'tbspacer',
                                                        width: 15,
                                                        height: 10
                                                    },
                                                    grListSetTypeTrustee2

                                                ]
                                    }
                                ]

                    }
            )
    // datarefresh_viUsers();
    return FrmTabs_viUser;
}







function ShowPesanWarning_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}

function ShowPesanError_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            )
}

function ShowPesanInfo_viUserInfo(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}










///---------------------------------------------------------------------------------------///


/**
 *	Function : gridDataForm_viuser
 *	
 *	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
 *	yang di pangil dari Function getFormItemEntry_viuser
 */
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_viUsers()
{
    dataSource_viInformasiUser.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCount_viuser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'viInfoUser',
                                    param: "kd_user is NOT NULL order by user_names"
                                }
                    }
            );
    return dataSource_viuser;
    //alert("refersh")
}

function datarefresh_viUsersFilter()
{
    var KataKunci = '';
    if (Ext.get('txtfilternama_viuser').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viuser').getValue() + '%~)';
        } else
        {

            KataKunci += ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viuser').getValue() + '%~)';
        }
        ;
    }
    ;
    if (Ext.get('txtfilpoli_viuser').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viuser').getValue() + '%~)';
        } else
        {

            KataKunci += ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viuser').getValue() + '%~)';
        }
        ;
    }
    ;
    if (KataKunci != undefined)
    {
        dataSource_viuser.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCount_viuser,
                                        Sort: 'KD_DOKTER',
                                        Sortdir: 'ASC',
                                        target: 'viInfoTarif',
                                        param: ''
                                    }
                        }
                );
    } else
    {
        datarefresh_viUsers();
    }
    ;
}
;
function RefreshDataUserGroup()
{
    dsInfoGroupUser.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountInfoGroupUser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewGroupUser',
                                    param: 'u.kd_user =~' + kode_user + '~'
                                }
                    }
            );
    rowSelectedInfotarif = undefined;
    return dsInfoGroupUser;
}
;
function RefreshDataTrustee()
{
    dsInfoTrustee.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountInfoGroupUser,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'ViewTrusteeGroup',
                                    param: 'g.group_id =~' + group_id + '~'
                                }
                    }
            );
    rowSelectedInfotarif = undefined;
    return dsInfoTrustee;
}
;
function setLookUp_viUser(rowdata)
{
    var lebar = 600;
    setLookUps_viUser = new Ext.Window
            (
                    {
                        id: 'SetLookUps_viUser',
                        name: 'SetLookUps_viUser',
                        title: 'Form User',
                        closeAction: 'destroy',
                        width: 535,
                        height: 530, //575,
                        resizable: false,
                        autoScroll: false,
                        iconCls: 'Studi_Lanjut',
                        modal: true,
                        items: getFormItemEntry_viDaftar(lebar, rowdata), //1
                        listeners:
                                {
                                    activate: function ()
                                    {
                                        //alert(Ext.get('txtNamaPasien').getValue());
                                        //Ext.getCmp('txtNama').focus(true, 10);

                                    },
                                    afterShow: function ()
                                    {
                                        // this.activate();

                                    },
                                    deactivate: function ()
                                    {
                                    }
                                }
                    }
            );
    setLookUps_viUser.show();
    if (rowdata === undefined)
    {
        //alert(rowdata);
        dataaddnew_viUser();
    } else
    {
        //console.log(rowdata);
        dataaddnew_viUser(rowdata);
    }

}

function getFormItemEntry_viDaftar(lebar, rowdata)
{
    var pnlFormDataBasic_viDaftar = new Ext.FormPanel
            (
                    {
                        title: '',
                        region: 'north', //'center',
                        layout: 'form', //'anchor',
                        bodyStyle: 'padding:10px 10px 10px 10px',
//                        anchor: '100%',
                        autoScroll: true,
                        width: 700, //lebar-55,
                        height: 500,
                        border: false,
                        items: [
                            getItemPanelInputBiodata_viDaftar(),
                            getItemPanelInputBiodata2_viDaftar()
                                    //getPenelItemDataKunjungan_viDaftar(lebar)
                        ],
                        fileUpload: true,
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Add',
                                                    iconCls: 'add',
                                                    id: 'btnAdd_viDaftar',
                                                    handler: function () {
                                                        dataaddnew_viUser();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    handler: function ()
                                                    {
														//alert(addNew_viUser);
                                                        datasave_User(addNew_viUser);
                                                        datarefresh_viUsers();
                                                    }
                                                },
                                                /* {
                                                    xtype: 'tbseparator'
                                                }, */
                                                {
                                                    xtype: 'button',
                                                    text: 'Save & Close',
                                                    iconCls: 'saveexit',
                                                    id: 'btnSimpanExit_viDaftar',
													hidden:true,
                                                    handler: function ()
                                                    {
                                                        var x = datasave_viDaftar(addNew_viDaftar);
                                                        datarefresh_viDaftar(tmpcriteriaRWJ);
                                                        if (x === undefined)
                                                        {
                                                            SetLookUps_viUser.close();
                                                        }
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnDelete_User',
                                                    enable: false,
                                                    handler: function ()
                                                    {
                                                        SetUserDelete();
                                                        RefreshDataSetJadwalDokter();
                                                        /*var x = datasave_viJadwal(addNew_viDaftar);
                                                         datarefresh_viDaftar();
                                                         if (x===undefined)
                                                         {
                                                         datasave_viJadwal(addNew_viDaftar);
                                                         SetJadwalAddNew.close();
                                                         }*/
                                                    }
                                                },
                                            ]
                                }
//                        ,items:

                    }
            )


    return pnlFormDataBasic_viDaftar;
}


//form view data biodata pasien
function getItemPanelInputBiodata_viDaftar()
{

    var items =
            {
                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
                        [
                            {
                                columnWidth: 1.0,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'kd_user ',
                                                name: 'txtkduser',
                                                id: 'txtkduser',
                                                readOnly: true,
                                                hidden: true,
                                                anchor: '50%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Username ',
                                                name: 'txtusername',
                                                id: 'txtusername',
                                                tabIndex: 1,
                                                readOnly: false,
                                                anchor: '50%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Full Name ',
                                                name: 'txtfullName',
                                                id: 'txtfullName',
                                                tabIndex: 2,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Description',
                                                name: 'txtdeskripsi',
                                                id: 'txtdeskripsi',
                                                tabIndex: 3,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                            {
                                                xtype: 'textfield',
                                                inputType: 'password',
                                                fieldLabel: 'Password',
                                                name: 'txtpassword',
                                                id: 'txtpassword',
                                                tabIndex: 4,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                            {
                                                xtype: 'textfield',
                                                inputType: 'password',
                                                fieldLabel: 'Re-type Password',
                                                name: 'txtrepassword',
                                                id: 'txtrepassword',
                                                tabIndex: 5,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Tag 1',
                                                name: 'txttagsatu',
                                                id: 'txttagsatu',
                                                tabIndex: 6,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Tag 2',
                                                name: 'txttagdua',
                                                id: 'txttagdua',
                                                tabIndex: 7,
                                                disabled: false,
                                                anchor: '50%',
                                            },
                                        ]
                            },
                        ]
            };
    return items;
}
;
function getItemPanelInputBiodata2_viDaftar()
{

    var items =
            {
                layout: 'absolute',
                border: true,
                height: 100,
                width: 500,
                items:
                        [
                            {
                                x: 10,
                                y: 10,
                                xtype: 'checkbox',
                                name: 'cbdokter',
                                id: 'cbdokter',
                                boxLabel: 'Dokter',
                                handler: function ()
                                {
                                    if (this.getValue() === true)
                                    {
                                        Ext.getCmp('cbouserdokter').enable();
                                    } else
                                    {
                                        Ext.getCmp('cbouserdokter').disable();
                                    }
                                }
                            },
                            mCombodokter(),
                            {
                                x: 10,
                                y: 40,
                                xtype: 'checkbox',
                                name: 'cbpetugasapotek',
                                id: 'cbpetugasapotek',
                                boxLabel: 'Petugas Apotek',
                                handler: function ()
                                {
                                    if (this.getValue() === true)
                                    {
                                        Ext.getCmp('cbouserunit').enable();
                                        Ext.getCmp('cbousermilik').enable();
                                    } else
                                    {
                                        Ext.getCmp('cbouserunit').disable();
                                        Ext.getCmp('cbousermilik').disable();
                                    }
                                }
                            },
                            mComboUnit(), mCombomilik(),
                            {
                                x: 10,
                                y: 70,
                                xtype: 'checkbox',
                                name: 'cbpetugaspoli',
                                id: 'cbpetugaspoli',
                                boxLabel: 'Petugas Poli',
                                handler: function ()
                                {
                                    if (this.getValue() === true)
                                    {
                                        Ext.getCmp('cboPoliklinik').enable();
                                    } else
                                    {
                                        Ext.getCmp('cboPoliklinik').disable();
                                    }
                                }
                            },
                            mComboPoliklinik()
                        ]
            };
    return items;
}
;
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;
function dataaddnew_viUser(rowdata)
{
    Ext.getCmp('cboPoliklinik').disable();
    Ext.getCmp('cbousermilik').disable();
    Ext.getCmp('cbouserunit').disable();
    Ext.getCmp('cbouserdokter').disable();
    if (rowdata === undefined)
    {
        Ext.getCmp('txtusername').focus('false', 10);
        //addNew_viDaftar = true;
        Ext.getCmp('txtusername').setValue('');
        Ext.getCmp('txtfullName').setValue('');
        Ext.getCmp('txtdeskripsi').setValue('');
        Ext.getCmp('txtpassword').setValue('');
        Ext.getCmp('txtrepassword').setValue('');
        Ext.getCmp('txttagsatu').setValue('');
        Ext.getCmp('txttagdua').setValue('');
		Ext.getCmp('txtpassword').enable();
        Ext.getCmp('txtrepassword').enable();
        Ext.getCmp('btnAdd_viDaftar').enable();
        Ext.getCmp('btnSimpan_viDaftar').enable();
        Ext.getCmp('btnSimpanExit_viDaftar').enable();
        Ext.getCmp('btnDelete_User').disable();
        addNew_viUser = true;


    } else
    {
//        'KDPOLI', 'KDMILIK', 'KDUNIT', 'KDDOKTER', 'NAMADOKTER', 'PEMILIK', 'UNIT', 'POLI'
        Ext.getCmp('txtusername').focus('false', 10);
        //addNew_viDaftar = true;
        Ext.getCmp('txtkduser').setValue(rowdata.KD_USER);
        Ext.getCmp('txtusername').setValue(rowdata.USER_NAMES);
        Ext.getCmp('txtfullName').setValue(rowdata.FULL_NAME);
        Ext.getCmp('txtdeskripsi').setValue(rowdata.DESCRIPTION);
        Ext.getCmp('txtpassword').setValue(rowdata.PASSWORD);
        Ext.getCmp('txtrepassword').setValue(rowdata.PASSWORD);
        Ext.getCmp('txttagsatu').setValue(rowdata.TAG1);
        Ext.getCmp('txttagdua').setValue(rowdata.TAG2);

        if (rowdata.NAMADOKTER !== '')
        {
            Ext.getCmp('cbdokter').setValue(true);
            Ext.getCmp('cbouserdokter').enable();
            Ext.getCmp('cbouserdokter').setValue(rowdata.NAMADOKTER);
            iddokter = rowdata.KDDOKTER;
        }
        if (rowdata.UNIT !== '')
        {
            Ext.getCmp('cbpetugasapotek').setValue(true);
            Ext.getCmp('cbousermilik').enable();
            Ext.getCmp('cbouserunit').enable();
            Ext.getCmp('cbousermilik').setValue(rowdata.PEMILIK);
            Ext.getCmp('cbouserunit').setValue(rowdata.UNIT);
            idunit = rowdata.KDUNIT;
            idmilik = rowdata.KDMILIK;
        }
        if (rowdata.KDPOLI !== '')
        {
            Ext.getCmp('cbpetugaspoli').setValue(true);
            Ext.getCmp('cbpetugaspoli').enable();
            Ext.getCmp('cboPoliklinik').setValue(rowdata.POLI);
            idpoli = rowdata.KDPOLI;
        }

		// Ext.getCmp('txtpassword').disable();
        // Ext.getCmp('txtrepassword').disable();
        Ext.getCmp('btnAdd_viDaftar').enable();
        Ext.getCmp('btnSimpan_viDaftar').enable();
        Ext.getCmp('btnSimpanExit_viDaftar').enable();
        Ext.getCmp('btnDelete_User').enable();
        addNew_viUser = false;
    }

    // rowSelected_viDaftar   = undefined;
}

function datasave_User(mBol)
{
    if (ValidasiEntry_User('Simpan Data', false) == 1)
    {
        //alert('a')
        if (addNew_viUser == true)
        {
            Ext.Ajax.request
                    (
                            {
                                url: baseURL + "index.php/main/CreateDataObj",
                                params: dataparam_User(),
                                success: function (o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfo_viUserInfo('Data berhasil di simpan', 'Simpan Data');
                                        //datarefresh_viDaftar(tmpcriteriaRWJ);
                                        addNew_viDaftar = false;
                                        //Ext.get('txtNoRequest').dom.value = cst.KD_PASIEN;
                                        //                          printbill();
                                        Ext.getCmp('btnSimpan_viDaftar').disable();
                                        Ext.getCmp('btnSimpanExit_viDaftar').disable();
                                        //Ext.getCmp('btnDelete_User').enable();
                                    } else if (cst.success === false && cst.pesan === 0)
                                    {
                                        ShowPesanWarning_viUserInfo('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                                    } else if (cst.success === false && cst.cari === true)
                                    {
                                        ShowPesanWarning_viUserInfo('Anda telah berkunjung sebelumnya ', 'Simpan Data');
                                    } else
                                    {
                                        ShowPesanError_viUserInfo('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                                    }
                                    datarefresh_viUsers();
                                }
                            }
                    )
        } else
        {
            Ext.Ajax.request
                    (
                            {
                                url: WebAppUrl.UrlUpdateData,
                                params: dataparam_User(),
                                success: function (o)
                                {
                                    //alert(o);
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfo_viUserInfo('Data berhasil disimpan', 'Edit Data');
                                        datarefresh_viUsers();
                                    } else if (cst.success === false && cst.pesan === 0)
                                    {
                                        ShowPesanWarning_viUserInfo('Data tidak berhasil disimpan ' + cst.pesan, 'Edit Data');
                                    } else
                                    {
                                        ShowPesanError_viUserInfo('Data tidak berhasil disimpan ' + cst.pesan, 'Edit Data');
                                    }
                                }
                            }
                    )
        }
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function ValidasiEntry_User(modul, mBolHapus)
{
    var x;
    if (Ext.get('txtusername').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("Username belum terisi...", modul);
        x = 0;
    } else if (Ext.get('txtfullName').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("Full Name belum terisi...", modul);
        x = 0;
    } else if (Ext.get('txtpassword').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("Password belum terisi...", modul);
        x = 0;
    } else if (Ext.get('txtrepassword').getValue() == '')
    {
        ShowPesanWarning_viUserInfo("Ketikan Kembali Password...", modul);
        x = 0;
    } else if (Ext.get('txtpassword').getValue() != Ext.get('txtrepassword').getValue())
    {
        ShowPesanWarning_viUserInfo("Password Tidak Sama...", modul);
        x = 0;
    } else
    {
        x = 1;
    }

    return x;
}

function dataparam_User()
{
    var dokter;
    var unit;
    var pemilik;
    var poli;
    if (Ext.getCmp('cbdokter').getValue() === true)
    {
        dokter = iddokter;
    } else
    {
        dokter = null;
    }
    if (Ext.getCmp('cbpetugasapotek').getValue() === true)
    {
        unit = idunit;
        pemilik = idmilik;
    } else
    {
        unit = null;
        pemilik = 0;
    }
    if (Ext.getCmp('cbpetugaspoli').getValue() === true)
    {
        poli = idpoli;
    } else
    {
        poli = null;
    }
    var params_ViPendaftaran =
            {
                Table: 'ViewUserSetup',
                kd_user: Ext.getCmp('txtkduser').getValue(),
                user_names: Ext.getCmp('txtusername').getValue(),
                full_name: Ext.getCmp('txtfullName').getValue(),
                password: Ext.getCmp('txtpassword').getValue(),
                description2: Ext.getCmp('txtdeskripsi').getValue(),
                tag1: Ext.getCmp('txttagsatu').getValue(),
                tag2: Ext.getCmp('txttagdua').getValue(),
                language_id: 2,
                kd_dokter: dokter,
                kd_unit_far: unit,
                kd_milik: pemilik,
                kd_unit: poli,
            };
    return params_ViPendaftaran
}


function SetUserDelete()
{
    //if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
    //{
    Ext.Msg.show
            (
                    {
                        title: nmHeaderHapusData,
                        msg: 'Apakah anda yakin akan menghapus data ini ?', //nmGetValidasiHapus(nmEmp2),
                        buttons: Ext.MessageBox.YESNO,
                        width: 250,
                        fn: function (btn)
                        {
                            if (btn === 'yes')
                            {
                                Ext.Ajax.request
                                        (
                                                {
                                                    url: WebAppUrl.UrlDeleteData,
                                                    params: getParamRequest2(),
                                                    success: function (o)
                                                    {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true)
                                                        {
                                                            datarefresh_viUsers();
                                                            ShowPesanInfo_viUserInfo("Data berhasil dihapus", "Hapus Data");
                                                            setLookUps_viUser.close();
                                                            dataaddnew_viUser();
                                                        } else if (cst.success === false && cst.pesan === 0)
                                                        {
                                                            ShowPesanWarning_viUserInfo(nmPesanHapusGagal, nmHeaderHapusData);
                                                        } else
                                                        {
                                                            ShowPesanWarning_viUserInfo(nmPesanHapusError, nmHeaderHapusData);
                                                        }
                                                        ;
                                                    }
                                                }
                                        )
                            }
                            ;
                        }
                    }
            )
}
;
function getParamRequest2()
{
    var params =
            {
                Table: 'ViewUserSetup',
                kd_user: Ext.get('txtkduser').getValue(),
            };
    return params
}
;

function mCombodokter()
{
    var Field = ['KODE', 'NAMA'];
    dsUserDokter = new WebApp.DataStore({fields: Field});
    dsUserDokter.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'viUserDokter',
                                    param: ''
                                }
                    }
            );
    var cbouserdokter = new Ext.form.ComboBox
            (
                    {
                        x: 150,
                        y: 10,
                        id: 'cbouserdokter',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Dokter...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsUserDokter,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        anchor: '99%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        iddokter = b.data.KODE;
                                    },
                                }
                    }
            );
    return cbouserdokter;
}
;

function mComboUnit()
{
    var Field = ['KODE', 'NAMA'];
    dsUserUnit = new WebApp.DataStore({fields: Field});
    dsUserUnit.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'viUserUnit',
                                    param: ''
                                }
                    }
            );
    var cbouserunit = new Ext.form.ComboBox
            (
                    {
                        x: 150,
                        y: 40,
                        id: 'cbouserunit',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih Unit...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsUserUnit,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        width: 200,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        idunit = b.data.KODE;
                                    },
                                }
                    }
            );
    return cbouserunit;
}
;

function mCombomilik()
{
    var Field = ['KODE', 'NAMA'];
    dsUsermilik = new WebApp.DataStore({fields: Field});
    dsUsermilik.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100,
                                    Sort: 'NAMA',
                                    Sortdir: 'ASC',
                                    target: 'viUserMilik',
                                    param: ''
                                }
                    }
            );
    var cbousermilik = new Ext.form.ComboBox
            (
                    {
                        x: 360,
                        y: 40,
                        id: 'cbousermilik',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'Pilih milik...',
                        selectOnFocus: true,
                        align: 'Right',
                        store: dsUsermilik,
                        valueField: 'KODE',
                        displayField: 'NAMA',
//                        anchor: '99%',
                        width: 133,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        idmilik = b.data.KODE;
                                    },
                                }
                    }
            );
    return cbousermilik;
}
;

function mComboPoliklinik()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

    ds_Poli_viDaftar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'NAMA_UNIT',
                                    Sortdir: 'ASC',
                                    target: 'ViewSetupUnit',
                                    param: "kd_bagian=2 and type=0"
                                }
                    }
            )

    var cboPoliklinik = new Ext.form.ComboBox
            (
                    {
                        x: 150,
                        y: 70,
                        id: 'cboPoliklinik',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Poliklinik...',
                        fieldLabel: 'Poliklinik ',
                        align: 'Right',
                        store: ds_Poli_viDaftar,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        anchor: '99%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        idpoli = b.data.KD_UNIT;
                                    }


                                }
                    }
            )

    return cboPoliklinik;
};

function getArrDetailTrdeletergroupuser(){
    var x = '';
    console.log(dsInfoGroupUser.modified[0]);
    // console.log(dsInfoGroupUser.modified[i].data.GROUP_ID);
    for (var i = 0; i < dsInfoGroupUser.modified.length; i++)
    {
        var y = '';
        var z = '@@##$$@@';

        y = kode_user
        y += z + dsInfoGroupUser.modified[i].data.GROUP_ID

        if (i === (dsInfoGroupUser.modified.length - 1)){
            x += y
        }else{
            x += y + '##[[]]##'
        };
    }
    return x;
};