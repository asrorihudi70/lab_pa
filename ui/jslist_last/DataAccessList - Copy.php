LookupEmployee;general/viewemployee
LookupVendor;general/viewlookupvendor
LookupAsset;general/viewlookupasset
LookupPart;general/viewlookuppart
LookupPartCategory;general/viewlookuppartcategory
LookupPartService;general/viewlookuppartservice
LookupService;general/viewlookupservice
LookupServiceCategory;general/viewlookupservicecategory
ViewComboDept;general/viewcombodept
ViewComboDeptAll;general/viewcombodeptall
ViewComboStatusVw;general/viewcombostatusvw
ViewComboCategoryVw;general/viewcombocategoryvw
ViewSetupLocation;setup/viewsetuplocation
ViewSetupDept;setup/viewsetupdept
ViewSetupPart;setup/viewsetuppart
ViewSetupVendor;setup/viewsetupvendor
ViewSetupEmployee;setup/viewsetupemployee
ViewSetupServicePMType;setup/viewsetupservicepmtype
ViewSetupCategory;setup/viewsetupcategory
ViewSetupCategoryAddField;setup/viewsetupcategoryaddfield
ViewRequestCM;cm/viewrequestcm
ViewRequestCMDetail;cm/viewrequestcmdetail
ViewApproveCM;cm/viewapprovecm
ViewScheduleCM;cm/viewschedulecm
ViewWorkOrderCM;cm/viewworkordercm
ViewWorkOrderPM;pm/viewworkorderpm
ViewSchServiceCM;cm/viewschservicecm
ViewSchPersonCM;cm/viewschpersoncm
ViewSchPartCM;cm/viewschpartcm
ViewSchServicePM;pm/viewschservicepm
ViewWOServicePM;pm/viewwoservicepm
ViewWOPersonPM;pm/viewwopersonpm
ViewWOPartPM;pm/viewwopartpm
ViewResultCM;cm/viewresultcm
ViewResultServiceCM;cm/viewresultservicecm
ViewResultServicePM;pm/viewresultservicepm
ViewResultPersonCM;cm/viewresultpersoncm
ViewResultPartCM;cm/viewresultpartcm
ViewResultPM;pm/viewresultpm
ViewResultPersonPM;pm/viewresultpersonpm
ViewResultPartPM;pm/viewresultpartpm
ComboDepartment;general/viewcombodeptall
viComboCompany;general/vicombocompany
viComboLocation;general/vicombolocation
ViewSetupAsset;setup/viewsetupasset
ViewAsset;setup/viviewasset
viComboServicePM;general/vicomboservicepm
viewSetServicePMParts;setup/viewsetservicepmparts
ViewServicePMParts;pm/viewservicepmparts
viServicePMParts;setup/viservicepmparts
SchedulePM;pm/vischedulepm
viViewSchedulePM;pm/viviewschedulepm
ViewDashboardCategory;dashboard/viewdashboardcategory
ViewDashboardWO;dashboard/viewdashboardwo
ViewDashboardCost;dashboard/viewdashboardcost
ViewMonitoring;main/ViewMonitoring
ViewSetupUnit;setup/viewsetupunit
ViewBagian;masterdata/viewbagian
ViewSetupBagian;master/viewsetupbagian
ViewSetupKelas;master/viewsetupkelas
ViewSetupUnit;master/viewsetupunit
ViewSetupCompany;main/viewsetupcompany
viAdditionalField;setup/viadditionalfield
viAdditionalFieldBlank;setup/viadditionalfieldblank
viServiceCategory;setup/viservicecategory
ViewSetupServiceCategory;setup/viewsetupservicecategory
viAssetService;setup/viassetservice
viHistoryAsset;general/vihistoryasset
ViewSetupService;setup/viewsetupservice
ViewSetupReference;setup/viewsetupreference
InputLogMetering;setup/viewinputlogmetering
viEarlyWarning;dashboard/vimonitoringearlywarning
viMonitoringEarlyWarningDetail;dashboard/vimonitoringearlywarningdetail
viMaintenance;dashboard/vimonitoringmaintenance
viMonitoringMaintenanceRequest;main/vimonitoringmaintenancerequest
viMonitoringMaintenanceSchedule;main/vimonitoringmaintenanceschedule
viMonitoringMaintenanceWO;main/vimonitoringmaintenancewo
viMonitoringMaintenanceResult;main/vimonitoringmaintenanceresult
ViewSetupAssetRef;setup/viewsetupassetref
viComboAssetService;general/vicomboassetservice
viPartCategory;setup/vipartcategory
viHistoryInputLog;general/vihistoryinputlog
ViewComboBahasaUtility;main/viewcombobahasautility
ViewComboGroupLanguage;main/viewcombogrouplanguage
ViewSettingBahasa;main/viewsettingbahasa
ViewSetupBahasa;setup/viewsetupbahasa
ViewDashboardPerform;dashboard/viewdashboardperform
ViewGetFieldRepGenCMRequest;main/viewgetfieldrepgencmrequest
ViewGetFieldQueryRepGen;main/viewgetfieldqueryrepgen
ViewWarningLogMetering;dashboard/viewwarninglogmetering
ViewTracking;dashboard/viewtracking
ViewTrackingReqInfo;dashboard/viewtrackingreqinfo
ViewTrackingAppInfo;dashboard/viewtrackingappinfo
ViewTrackingWOInfo;dashboard/viewtrackingwoinfo
ViewTrackingCloseInfo;dashboard/viewtrackingcloseinfo
ViewSetupStatusAsset;setup/viewsetupstatusasset
viComboStatusAsset;general/viewcombostatusasset