<?php
include_once 'php-ofc-library/open-flash-chart.php';
 
echo '{
      "title":{
        "text":  "Chart Transaksi Umum",
        "style": "{font-size: 20px; color:#0000ff; font-family: Verdana; text-align: center;}"
      },

      "y_legend":{
        "text": "---",
        "style": "{color: #736AFF; font-size: 12px;}"
      },
     "x_legend":{
        "text": "'.$bln[$expSD[1]-1].' '.$expSD[0].'",
        "style": "{color: #736AFF; font-size: 12px;}"
      },

      "elements":[
        {
          "type":      "bar_glass",
          "alpha":     0.4,
          "colour":    "#9933CC",
          "tip":       "Tip for purple bars<br>val=#val#, top=#top#",
          "text":      "Total Amount",
          "font-size": 10,
          "values" :   [7,8,9]
        },{
          "type":      "bar_glass",
          "alpha":     0.4,
          "colour":    "#036ce2",
          "tip":       "Tip for purple bars<br>val=#val#, top=#top#",
          "text":      "Total Bulan",
          "font-size": 10,
          "values" :   [7,8,9]
        },{
          "type":      "bar_glass",
          "alpha":     0.4,
          "colour":    "#fd0701",
          "tip":       "Tip for purple bars<br>val=#val#, top=#top#",
          "text":      "Total Transaksi",
          "font-size": 10,
          "values" :   [5,6,7],

      "x_axis":{
        "stroke":       1,
        "tick_height":  10,
        "colour":      "#d000d0",
        "grid_colour": "#00ff00",
        "labels": {
          "labels": [24,25,26]
        }
       },

      "y_axis":{
        "stroke":      4,
        "tick_length": 3,
        "colour":      "#d000d0",
        "grid_colour": "#00ff00",
        "offset":      0,
        "max":         10,
        "visible":      true,
        "steps":        1
      },

      "tooltip":{
        "text": "Global Tooltip<br>val=#val#, top=#top#"
      }'

?>